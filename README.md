# InterVu2-Rooms-BE

### Starting locally in Docker
Go to `/scripts` folder and make a copy of a `sample.env` file and name it `.env`.
Populate it with proper Twilio credentials. After that, just execute `./start.sh`.
To stop the service, just execute `./stop.sh`.
