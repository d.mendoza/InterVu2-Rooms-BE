FROM maven:3.8.2-amazoncorretto-17 AS build
COPY pom.xml /usr/src/app/pom.xml
RUN mvn -f /usr/src/app/pom.xml clean verify --fail-never
COPY src /usr/src/app/src
RUN mvn -f /usr/src/app/pom.xml clean package -DskipTests

FROM amazoncorretto:17-alpine
MAINTAINER Branko Ostojic <bostojic@itekako.com>

COPY --from=build /usr/src/app/target/*.jar /usr/app/intervu-room-api.jar

ENTRYPOINT ["java", "-Dlog4j2.formatMsgNoLookups=true", "-jar","/usr/app/intervu-room-api.jar"]

EXPOSE 10000
