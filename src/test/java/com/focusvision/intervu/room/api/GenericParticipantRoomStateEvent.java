package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the generic room state event.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
public class GenericParticipantRoomStateEvent {

    private RoomStateChangeType eventType;
    protected String destination;
    private ParticipantRoomStateDto data;

}
