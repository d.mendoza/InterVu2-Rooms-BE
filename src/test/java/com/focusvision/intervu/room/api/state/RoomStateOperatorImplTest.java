package com.focusvision.intervu.room.api.state;

import static com.focusvision.intervu.room.api.RoomStateFixture.generateRoomState;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.RESPONDENTS_CHAT_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.RESPONDENTS_CHAT_ENABLED;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.focusvision.intervu.room.api.event.room.publisher.RoomStateChangedEventPublisher;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusStateService;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RoomStateOperatorImplTest {

  private RoomStateOperatorImpl operator;

  @Mock private RoomStateProvider roomStateProvider;
  @Mock private CommunicationChannelService communicationChannelService;
  @Mock private StimulusStateService stimulusStateService;
  @Mock private RoomStateChangedEventPublisher roomStateChangedEventPublisher;
  @Mock private RoomOffsetProvider roomOffsetProvider;

  @BeforeEach
  public void setup() {
    operator =
        new RoomStateOperatorImpl(
            roomStateProvider,
            communicationChannelService,
            stimulusStateService,
            roomStateChangedEventPublisher,
            roomOffsetProvider);
  }

  @Test
  @DisplayName("Enable respondents chat - chat currently disabled")
  void enableRespondentsChat_chatDisabled() {
    UUID roomId = randomUUID();
    var currentRoomState = generateRoomState(roomId).respondentsChatEnabled(false).build();
    when(roomStateProvider.findCurrentForRoom(roomId))
        .thenReturn(Optional.of(new RoomStateLog().setRoomState(currentRoomState)));
    when(roomStateProvider.save(any(), any()))
        .thenAnswer(invocation -> new RoomStateLog().setRoomState(invocation.getArgument(0)));

    RoomState roomState = operator.enableRespondentsChat(roomId).orElseThrow();

    assertTrue(roomState.isRespondentsChatEnabled());
    verify(roomStateProvider).save(currentRoomState, RESPONDENTS_CHAT_ENABLED);
    verify(roomStateChangedEventPublisher)
        .publish(new RoomStateChangedEvent(RESPONDENTS_CHAT_ENABLED, roomState));
  }

  @Test
  @DisplayName("Enable respondents chat - chat already enabled")
  void enableRespondentsChat_chatAlreadyEnabled() {
    UUID roomId = randomUUID();
    var currentRoomState = generateRoomState(roomId).respondentsChatEnabled(true).build();
    when(roomStateProvider.findCurrentForRoom(roomId))
        .thenReturn(Optional.of(new RoomStateLog().setRoomState(currentRoomState)));

    var roomState = operator.enableRespondentsChat(roomId);

    assertThat(roomState).isNotPresent();
    verify(roomStateProvider, never()).save(any(), any());
    verifyNoInteractions(roomStateChangedEventPublisher);
  }

  @Test
  @DisplayName("Disable respondents chat - chat currently enabled")
  void disableRespondentsChat_chatEnabled() {
    UUID roomId = randomUUID();
    var currentRoomState = generateRoomState(roomId).respondentsChatEnabled(true).build();
    when(roomStateProvider.findCurrentForRoom(roomId))
        .thenReturn(Optional.of(new RoomStateLog().setRoomState(currentRoomState)));
    when(roomStateProvider.save(any(), any()))
        .thenAnswer(invocation -> new RoomStateLog().setRoomState(invocation.getArgument(0)));

    RoomState roomState = operator.disableRespondentsChat(roomId).orElseThrow();

    assertFalse(roomState.isRespondentsChatEnabled());
    verify(roomStateProvider).save(currentRoomState, RESPONDENTS_CHAT_DISABLED);
    verify(roomStateChangedEventPublisher)
        .publish(new RoomStateChangedEvent(RESPONDENTS_CHAT_DISABLED, roomState));
  }

  @Test
  @DisplayName("Disable respondents chat - chat already disabled")
  void disableRespondentsChat_chatAlreadyDisabled() {
    UUID roomId = randomUUID();
    var currentRoomState = generateRoomState(roomId).respondentsChatEnabled(false).build();
    when(roomStateProvider.findCurrentForRoom(roomId))
        .thenReturn(Optional.of(new RoomStateLog().setRoomState(currentRoomState)));

    var roomState = operator.disableRespondentsChat(roomId);

    assertThat(roomState).isNotPresent();
    verify(roomStateProvider, never()).save(any(), any());
    verifyNoInteractions(roomStateChangedEventPublisher);
  }
}
