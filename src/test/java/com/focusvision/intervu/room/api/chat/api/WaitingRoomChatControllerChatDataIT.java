package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.HttpBasedIT;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.common.model.ChatType.RESPONDENTS;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link WaitingRoomChatController#chatMetadata}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Waiting Room chat channels data")
class WaitingRoomChatControllerChatDataIT extends HttpBasedIT {

    @Test
    @DisplayName("Get chat channels forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(WAITING_ROOM_CHAT_DATA_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Get chat channels forbidden for InterVu users")
    void forbiddenIntervuUsers() {
        given()
                .when()
                .headers(intervuUserHeaders(getModerator(setUpFullRoom())))
                .get(url(WAITING_ROOM_CHAT_DATA_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Get chat channels forbidden for banned participants")
    void forbiddenForBannedParticipants() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(WAITING_ROOM_CHAT_DATA_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Respondent gets metadata")
    void respondentGetsMetadata() {
        var respondent = getRespondent(setUpFullRoom());
        var handle = respondent.getResearchSession().getId().toString();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(WAITING_ROOM_CHAT_DATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal", nullValue())
                .body("respondents.size()", is(8))
                .body("respondents.handle", equalTo(handle))
                .body("respondents.type", equalTo(RESPONDENTS.name()))
                .body("respondents.readOnly", equalTo(false))
                .body("respondents.enabled", equalTo(true))
                .body("respondents.control", nullValue())
                .body("respondents.destination", equalTo("/app/waiting-room/chat/respondents"))
                .body("respondents.destinationForSeen", equalTo("/app/waiting-room/chat/respondents/seen"))
                .body("respondents.messagesUrl", equalTo("/waiting-room/chat/respondents"))
                .body("direct.size()", is(0));
    }

    @Test
    @DisplayName("Moderator gets metadata")
    void moderatorGetsMetadata() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var handle = researchSession.getId().toString();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(WAITING_ROOM_CHAT_DATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal.size()", is(8))
                .body("internal.handle", equalTo(handle))
                .body("internal.type", equalTo(INTERNAL.name()))
                .body("internal.readOnly", equalTo(false))
                .body("internal.enabled", equalTo(true))
                .body("internal.control", nullValue())
                .body("internal.destination", equalTo("/app/waiting-room/chat/internal"))
                .body("internal.destinationForSeen", equalTo("/app/waiting-room/chat/internal/seen"))
                .body("internal.messagesUrl", equalTo("/waiting-room/chat/internal"))
                .body("respondents.size()", is(8))
                .body("respondents.handle", equalTo(handle))
                .body("respondents.type", equalTo(RESPONDENTS.name()))
                .body("respondents.readOnly", equalTo(false))
                .body("respondents.enabled", equalTo(true))
                .body("respondents.control", nullValue())
                .body("respondents.destination", equalTo("/app/waiting-room/chat/respondents"))
                .body("respondents.destinationForSeen", equalTo("/app/waiting-room/chat/respondents/seen"))
                .body("respondents.messagesUrl", equalTo("/waiting-room/chat/respondents"))
                .body("direct.size()", is(0));
    }

    @Test
    @DisplayName("Translator gets metadata")
    void translatorGetsMetadata() {
        var researchSession = setUpFullRoom();
        var translator = getTranslator(researchSession);
        var handle = researchSession.getId().toString();

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(WAITING_ROOM_CHAT_DATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal.size()", is(8))
                .body("internal.handle", equalTo(handle))
                .body("internal.type", equalTo(INTERNAL.name()))
                .body("internal.readOnly", equalTo(false))
                .body("internal.enabled", equalTo(true))
                .body("internal.control", nullValue())
                .body("internal.destination", equalTo("/app/waiting-room/chat/internal"))
                .body("internal.destinationForSeen", equalTo("/app/waiting-room/chat/internal/seen"))
                .body("internal.messagesUrl", equalTo("/waiting-room/chat/internal"))
                .body("respondents", nullValue())
                .body("direct.size()", is(0));
    }

    @Test
    @DisplayName("Observer gets metadata")
    void observerGetsMetadata() {
        var researchSession = setUpFullRoom();
        var observer = getObserver(researchSession);
        var handle = researchSession.getId().toString();

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(WAITING_ROOM_CHAT_DATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal.size()", is(8))
                .body("internal.handle", equalTo(handle))
                .body("internal.type", equalTo(INTERNAL.name()))
                .body("internal.readOnly", equalTo(false))
                .body("internal.enabled", equalTo(true))
                .body("internal.control", nullValue())
                .body("internal.destination", equalTo("/app/waiting-room/chat/internal"))
                .body("internal.destinationForSeen", equalTo("/app/waiting-room/chat/internal/seen"))
                .body("internal.messagesUrl", equalTo("/waiting-room/chat/internal"))
                .body("respondents", nullValue())
                .body("direct.size()", is(0));
    }

}
