package com.focusvision.intervu.room.api.ws;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedEvent;
import com.focusvision.intervu.room.api.event.participant.publisher.ParticipantEventPublisher;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.scroll.model.ScrollLogDto;
import com.focusvision.intervu.room.api.scroll.model.StimulusScrollMessage;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * IT tests for WS scroll events.
 */
public class ScrollStimulusIT extends HttpBasedIT {

  @Autowired
  private ParticipantEventPublisher publisher;

  @Test
  @DisplayName("Scroll on stimulus")
  void scrollOnStimulus() throws ExecutionException, InterruptedException {
    var researchSession = setUpFullRoom();
    var stimulus = researchSession.getStimuli().get(0);
    var moderator = getModerator(researchSession);
    var respondent = getRespondent(researchSession);
    var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();

    var moderatorSession = connectRoomWs(moderator);
    var content1 = make();
    var content2 = make();

    connectToRoom(researchSession.getId(), moderator);
    connectToRoom(researchSession.getId(), respondent);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_STIMULUS_ACTIVATE), stimulus.getId())
        .then()
        .statusCode(OK.value());

    var moderatorRoomState = given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .extract().body().as(ParticipantRoomStateDto.class);

    var handler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
    subscribe(moderatorSession, moderatorRoomState.getParticipantState().getChannel().getRoom(),
        handler);

    var scrollAction1 = new StimulusScrollMessage()
        .setContent(content1)
        .setTimestamp(now().toEpochMilli());
    moderatorSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction1);
    waitForWs();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(SCROLL_STATE_LOG), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", Matchers.equalTo(1))
        .body("content", equalTo(content1))
        .extract().as(ScrollLogDto.class);

    var scrollAction2 = new StimulusScrollMessage()
        .setContent(content2)
        .setTimestamp(now().toEpochMilli());
    moderatorSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction2);
    waitForWs();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(SCROLL_STATE_LOG), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", Matchers.equalTo(1))
        .body("content", equalTo(content2))
        .extract().as(ScrollLogDto.class);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());
    waitForWs();

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_ROOM_SCROLL), researchSession.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("size()", Matchers.equalTo(2))
        .body("[0].size()", Matchers.equalTo(5))
        .body("[0].contextId", equalTo(stimulus.getId().toString()))
        .body("[0].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
        .body("[0].participantId", equalTo(moderator.getId().toString()))
        .body("[0].content", equalTo(content1))
        .body("[0].offset", greaterThan(0))
        .body("[1].size()", Matchers.equalTo(5))
        .body("[1].contextId", equalTo(stimulus.getId().toString()))
        .body("[1].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
        .body("[1].participantId", equalTo(moderator.getId().toString()))
        .body("[1].content", equalTo(content2))
        .body("[1].offset", greaterThan(0));
  }

  @Test
  @DisplayName("Scroll on stimulus - change moderator")
  void scrollOnStimulus_changeModerator() throws ExecutionException, InterruptedException {
    var researchSessionData = sendCreateRoomEvent();
    var adminObserverData = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName("admin")
        .setRole(OBSERVER)
        .setAdmin(true)
        .setBanable(false);
    var participants = new ArrayList<>(researchSessionData.getParticipants());
    participants.add(adminObserverData);
    researchSessionData.setParticipants(participants);
    researchSessionData.setVersion(researchSessionData.getVersion() + 1);
    sendUpdateRoomEvent(researchSessionData);
    var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId())
        .orElseThrow();
    var stimulus = researchSession.getStimuli().get(0);
    var moderator = getModerator(researchSession);
    var admin = researchSession.getParticipants().stream()
        .filter(participant -> participant.getPlatformId().equals("m0"))
        .findFirst().orElseThrow();
    var adminObserver = researchSession.getParticipants().stream()
        .filter(participant -> participant.getRole().equals(OBSERVER))
        .filter(participant -> participant.getPlatformId().equals(adminObserverData.getId()))
        .findFirst().orElseThrow();
    var respondent = getRespondent(researchSession);
    var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();

    var moderatorSession = connectRoomWs(moderator);
    var adminObserverSession = connectRoomWs(adminObserver);
    var content1 = make();
    var content2 = make();
    var content3 = make();
    var content4 = make();
    var content5 = make();
    var content6 = make();

    connectToRoom(researchSession.getId(), moderator);
    connectToRoom(researchSession.getId(), respondent);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_STIMULUS_ACTIVATE), stimulus.getId())
        .then()
        .statusCode(OK.value());

    var moderatorRoomState = given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .extract().body().as(ParticipantRoomStateDto.class);

    var handler1 = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
    subscribe(moderatorSession, moderatorRoomState.getParticipantState().getChannel().getRoom(),
        handler1);

    var adminSession = connectRoomWs(admin);
    connectToRoom(researchSession.getId(), admin);
    connectToRoom(researchSession.getId(), adminObserver);

    waitForWs();

    var scrollAction1 = new StimulusScrollMessage()
        .setContent(content1)
        .setTimestamp(now().toEpochMilli());
    moderatorSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction1);
    waitForWs();

    var scrollAction2 = new StimulusScrollMessage()
        .setContent(content2)
        .setTimestamp(now().toEpochMilli());
    adminSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction2);
    waitForWs();

    var scrollAction3 = new StimulusScrollMessage()
        .setContent(content3)
        .setTimestamp(now().toEpochMilli());
    adminObserverSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction3);
    waitForWs();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(SCROLL_STATE_LOG), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", Matchers.equalTo(1))
        .body("content", equalTo(content1))
        .extract().as(ScrollLogDto.class);

    publisher.publish(new ParticipantDisconnectedEvent(researchSession.getId(),
        moderator.getId()));
    waitForWs();
    var scrollAction4 = new StimulusScrollMessage()
        .setContent(content4)
        .setTimestamp(now().toEpochMilli());
    adminObserverSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction4);
    var scrollAction5 = new StimulusScrollMessage()
        .setContent(content5)
        .setTimestamp(now().toEpochMilli());
    adminSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction5);
    waitForWs();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(SCROLL_STATE_LOG), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", Matchers.equalTo(1))
        .body("content", equalTo(content5))
        .extract().as(ScrollLogDto.class);

    publisher.publish(new ParticipantDisconnectedEvent(researchSession.getId(), admin.getId()));
    waitForWs();
    var scrollAction6 = new StimulusScrollMessage()
        .setContent(content6)
        .setTimestamp(now().toEpochMilli());
    adminObserverSession.send(parseDestination(SCROLL_STIMULUS_DESTINATION, stimulus.getId()),
        scrollAction6);
    waitForWs();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(SCROLL_STATE_LOG), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", Matchers.equalTo(1))
        .body("content", equalTo(content6))
        .extract().as(ScrollLogDto.class);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());
    waitForWs();

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_ROOM_SCROLL), researchSession.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("size()", Matchers.equalTo(3))
        .body("[0].size()", Matchers.equalTo(5))
        .body("[0].contextId", equalTo(stimulus.getId().toString()))
        .body("[0].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
        .body("[0].participantId", equalTo(moderator.getId().toString()))
        .body("[0].content", equalTo(content1))
        .body("[0].offset", greaterThan(0))
        .body("[1].size()", Matchers.equalTo(5))
        .body("[1].contextId", equalTo(stimulus.getId().toString()))
        .body("[1].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
        .body("[1].participantId", equalTo(admin.getId().toString()))
        .body("[1].content", equalTo(content5))
        .body("[1].offset", greaterThan(0))
        .body("[2].size()", Matchers.equalTo(5))
        .body("[2].contextId", equalTo(stimulus.getId().toString()))
        .body("[2].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
        .body("[2].participantId", equalTo(adminObserver.getId().toString()))
        .body("[2].content", equalTo(content6))
        .body("[2].offset", greaterThan(0));
  }

}
