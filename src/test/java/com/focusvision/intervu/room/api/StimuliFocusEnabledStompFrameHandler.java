package com.focusvision.intervu.room.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.event.stimulus.model.StimuliFocusEnabledEvent;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@Getter
@RequiredArgsConstructor
public class StimuliFocusEnabledStompFrameHandler implements StompFrameHandler {

    private final ObjectMapper mapper;
    private final Consumer<StimuliFocusEnabledEvent> frameHandler;
    private final List<StimuliFocusEnabledEvent> states = new ArrayList<>();

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return StimuliFocusEnabledEvent.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info("received message: {} with headers: {}", payload, headers);

        StimuliFocusEnabledEvent dto = mapper.convertValue(payload, StimuliFocusEnabledEvent.class);
        frameHandler.accept(dto);
        states.add(dto);
    }
}
