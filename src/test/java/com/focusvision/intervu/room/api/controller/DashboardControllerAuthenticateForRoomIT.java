package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.model.dto.AuthDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link DashboardController#authenticate}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Dashboard room authentication")
class DashboardControllerAuthenticateForRoomIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(DASHBOARD_ROOM_TOKEN_URL), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for room participants")
    void forbiddenRoomParticipants() {
        given()
                .when()
                .headers(roomParticipantHeaders(getRespondent(setUpFullRoom())))
                .get(url(DASHBOARD_ROOM_TOKEN_URL), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Room not found")
    void roomNotFound() {
        given()
                .when()
                .headers(intervuUserHeaders(getModerator(setUpFullRoom())))
                .get(url(DASHBOARD_ROOM_TOKEN_URL), make())
                .then()
                .statusCode(BAD_REQUEST.value());
    }

    @Test
    @DisplayName("Participant not belonging to room")
    void participantNotBelongsToRoom() {
        given()
                .when()
                .headers(intervuUserHeaders(getModerator(setUpFullRoom())))
                .get(url(DASHBOARD_ROOM_TOKEN_URL), getModerator(setUpFullRoom())
                        .getResearchSession().getId().toString())
                .then()
                .statusCode(BAD_REQUEST.value());
    }

    @Test
    @DisplayName("Authenticated")
    void authenticated() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        AuthDto auth = given()
                .when()
                .headers(intervuUserHeaders(moderator))
                .get(url(DASHBOARD_ROOM_TOKEN_URL), moderator.getResearchSession().getId().toString())
                .then()
                .statusCode(OK.value())
                .extract().body().as(AuthDto.class);

        assertThat(auth.getToken()).isNotEmpty();

        given()
                .when()
                .headers("X-Room-Token", auth.getToken())
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value());
    }

}
