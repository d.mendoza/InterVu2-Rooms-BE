package com.focusvision.intervu.room.api.ws;

import static com.focusvision.intervu.room.api.common.model.DrawingActionType.CLEAR;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.DRAW;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.REDO;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.UNDO;
import static com.focusvision.intervu.room.api.common.model.DrawingContextType.STIMULUS;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.DrawingStompFrameHandler;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.draw.model.DrawingLogDto;
import com.focusvision.intervu.room.api.draw.model.StimulusDrawMessage;
import com.focusvision.intervu.room.api.export.model.RoomDrawingLogExportDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * IT tests for WS draw events.
 */
public class DrawOnStimulusIT extends HttpBasedIT {

    @Test
    @DisplayName("Draw on stimulus")
    void drawOnStimulus() {
        var researchSession = setUpFullRoom();
        var stimulus = researchSession.getStimuli().get(0);
        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        var drawContent = make();
        var drawContent2 = make();
        var drawUndoContent = make();
        var drawRedoContent = make();

        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);

        var respondentRoomState = given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.participants.findAll { p -> p.id == '" + respondent.getId() + "' }[0].online", equalTo(true))
                .extract().body().as(ParticipantRoomStateDto.class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_STIMULUS_ACTIVATE), stimulus.getId())
                .then()
                .statusCode(OK.value());

        var drawHandler = new DrawingStompFrameHandler(mapper);
        subscribe(respondentSession, respondentRoomState.getParticipantState().getChannel().getDraw(), drawHandler);

        var drawAction = new StimulusDrawMessage()
                .setType(DRAW)
                .setContent(drawContent)
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(parseDestination(DRAW_STIMULUS_DESTINATION, stimulus.getId()), drawAction);
        waitForWs();
        var drawUndoAction = new StimulusDrawMessage()
                .setType(UNDO)
                .setContent(drawUndoContent)
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(parseDestination(DRAW_STIMULUS_DESTINATION, stimulus.getId()), drawUndoAction);
        waitForWs();
        var drawRedoAction = new StimulusDrawMessage()
                .setType(REDO)
                .setContent(drawRedoContent)
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(parseDestination(DRAW_STIMULUS_DESTINATION, stimulus.getId()), drawRedoAction);
        waitForWs();
        var drawClearAction = new StimulusDrawMessage()
                .setType(CLEAR)
                .setContent(drawContent)
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(parseDestination(DRAW_STIMULUS_DESTINATION, stimulus.getId()), drawClearAction);
        waitForWs();
        var drawAction2 = new StimulusDrawMessage()
                .setType(DRAW)
                .setContent(drawContent2)
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(parseDestination(DRAW_STIMULUS_DESTINATION, stimulus.getId()), drawAction2);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(DRAWING_STATE_LOG), stimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(2))
                .body("[0].size()", Matchers.equalTo(3))
                .body("[0].participantId", equalTo(moderator.getId().toString()))
                .body("[0].type", equalTo(CLEAR.name()))
                .body("[0].content", nullValue())
                .body("[1].size()", Matchers.equalTo(3))
                .body("[1].participantId", equalTo(moderator.getId().toString()))
                .body("[1].type", equalTo(DRAW.name()))
                .body("[1].content", equalTo(drawContent2))
                .extract().as(DrawingLogDto[].class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_FINISH_URL))
                .then()
                .statusCode(OK.value());
        waitForWs();

        var drawMessages = drawHandler.getMessages();

        assertThat(drawMessages.size()).isEqualTo(5);

        assertAll(
                () -> assertThat(drawMessages.get(0).getParticipantId()).isEqualTo(moderator.getId()),
                () -> assertThat(drawMessages.get(0).getType()).isEqualTo(DRAW),
                () -> assertThat(drawMessages.get(0).getContextType()).isEqualTo(STIMULUS),
                () -> assertThat(drawMessages.get(0).getContextId()).isEqualTo(stimulus.getId()),
                () -> assertThat(drawMessages.get(0).getContent()).isEqualTo(drawContent),
                () -> assertThat(drawMessages.get(0).getOffset()).isPositive()
        );
        assertAll(
                () -> assertThat(drawMessages.get(1).getParticipantId()).isEqualTo(moderator.getId()),
                () -> assertThat(drawMessages.get(1).getType()).isEqualTo(UNDO),
                () -> assertThat(drawMessages.get(1).getContextType()).isEqualTo(STIMULUS),
                () -> assertThat(drawMessages.get(1).getContextId()).isEqualTo(stimulus.getId()),
                () -> assertThat(drawMessages.get(1).getContent()).isEqualTo(drawUndoContent),
                () -> assertThat(drawMessages.get(1).getOffset()).isPositive()
        );
        assertAll(
                () -> assertThat(drawMessages.get(2).getParticipantId()).isEqualTo(moderator.getId()),
                () -> assertThat(drawMessages.get(2).getType()).isEqualTo(REDO),
                () -> assertThat(drawMessages.get(2).getContextType()).isEqualTo(STIMULUS),
                () -> assertThat(drawMessages.get(2).getContextId()).isEqualTo(stimulus.getId()),
                () -> assertThat(drawMessages.get(2).getContent()).isEqualTo(drawRedoContent),
                () -> assertThat(drawMessages.get(2).getOffset()).isPositive()
        );
        assertAll(
                () -> assertThat(drawMessages.get(3).getParticipantId()).isEqualTo(moderator.getId()),
                () -> assertThat(drawMessages.get(3).getType()).isEqualTo(CLEAR),
                () -> assertThat(drawMessages.get(3).getContextType()).isEqualTo(STIMULUS),
                () -> assertThat(drawMessages.get(3).getContextId()).isEqualTo(stimulus.getId()),
                () -> assertThat(drawMessages.get(3).getContent()).isNullOrEmpty(),
                () -> assertThat(drawMessages.get(3).getOffset()).isPositive()
        );
        assertAll(
                () -> assertThat(drawMessages.get(4).getParticipantId()).isEqualTo(moderator.getId()),
                () -> assertThat(drawMessages.get(4).getType()).isEqualTo(DRAW),
                () -> assertThat(drawMessages.get(4).getContextType()).isEqualTo(STIMULUS),
                () -> assertThat(drawMessages.get(4).getContextId()).isEqualTo(stimulus.getId()),
                () -> assertThat(drawMessages.get(4).getContent()).isEqualTo(drawContent2),
                () -> assertThat(drawMessages.get(4).getOffset()).isPositive()
        );

        given()
                .when()
                .headers(intervuUserHeadersForSystemUser())
                .get(url(EXPORT_ROOM_DRAWINGS), researchSession.getPlatformId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(5))
                .body("[0].size()", Matchers.equalTo(7))
                .body("[0].participantId", equalTo(moderator.getId().toString()))
                .body("[0].type", equalTo(DRAW.name()))
                .body("[0].contextType", equalTo(STIMULUS.name()))
                .body("[0].contextId", equalTo(stimulus.getId().toString()))
                .body("[0].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
                .body("[0].content", equalTo(drawContent))
                .body("[0].offset", greaterThan(0))
                .body("[1].size()", Matchers.equalTo(7))
                .body("[1].participantId", equalTo(moderator.getId().toString()))
                .body("[1].type", equalTo(UNDO.name()))
                .body("[1].contextType", equalTo(STIMULUS.name()))
                .body("[1].contextId", equalTo(stimulus.getId().toString()))
                .body("[1].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
                .body("[1].content", equalTo(drawUndoContent))
                .body("[1].offset", greaterThan(0))
                .body("[2].size()", Matchers.equalTo(7))
                .body("[2].participantId", equalTo(moderator.getId().toString()))
                .body("[2].type", equalTo(REDO.name()))
                .body("[2].contextType", equalTo(STIMULUS.name()))
                .body("[2].contextId", equalTo(stimulus.getId().toString()))
                .body("[2].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
                .body("[2].content", equalTo(drawRedoContent))
                .body("[2].offset", greaterThan(0))
                .body("[3].size()", Matchers.equalTo(7))
                .body("[3].participantId", equalTo(moderator.getId().toString()))
                .body("[3].type", equalTo(CLEAR.name()))
                .body("[3].contextType", equalTo(STIMULUS.name()))
                .body("[3].contextId", equalTo(stimulus.getId().toString()))
                .body("[3].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
                .body("[3].content", nullValue())
                .body("[3].offset", greaterThan(0))
                .body("[4].size()", Matchers.equalTo(7))
                .body("[4].participantId", equalTo(moderator.getId().toString()))
                .body("[4].type", equalTo(DRAW.name()))
                .body("[4].contextType", equalTo(STIMULUS.name()))
                .body("[4].contextId", equalTo(stimulus.getId().toString()))
                .body("[4].stimulusPlatformId", equalTo(stimulus.getPlatformId()))
                .body("[4].content", equalTo(drawContent2))
                .body("[4].offset", greaterThan(0))
                .extract().as(RoomDrawingLogExportDto[].class);
    }

}
