package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.bookmark.api.BookmarkController;
import com.focusvision.intervu.room.api.bookmark.model.AddBookmarkDto;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

import static com.focusvision.intervu.room.api.common.model.BookmarkType.NOTE;
import static com.focusvision.intervu.room.api.common.model.BookmarkType.PII;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link BookmarkController#list}.
 */
@DisplayName("Bookmarks list")
class BookmarkControllerListIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for respondents")
    void forbiddenForRespondents() {
        given()
                .when()
                .headers(roomParticipantHeaders(getRespondent(setUpFullRoom())))
                .get(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(FORBIDDEN.value());
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"RESPONDENT"})
    @DisplayName("OK for backroom participants - no bookmarks")
    void okBackroomEmpty(ParticipantRole role) {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var participant = getParticipant(researchSession, role);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(roomParticipantHeaders(participant))
                .get(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(0));
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"RESPONDENT"})
    @DisplayName("OK for backroom participants")
    void okBackroom(ParticipantRole role) {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var participant = getParticipant(researchSession, role);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var startedRoom = researchSessionRepository.getById(researchSession.getId());

        var bookmark1 = generateBookmark();
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(bookmark1)
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(CREATED.value());

        waitForWs();
        var bookmark2 = generateBookmark();
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(bookmark2)
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(CREATED.value());

        waitForWs();
        var bookmark3 = generateBookmark();
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(bookmark3)
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .and().statusCode(CREATED.value());

        given()
                .when()
                .headers(roomParticipantHeaders(participant))
                .get(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("[0].size()", is(7))
                .body("[0].id", not(nullValue()))
                .body("[0].note", equalTo(bookmark1.getNote()))
                .body("[0].offset", equalTo(roomOffsetProvider
                        .calculateOffset(startedRoom.getId(), bookmark1.getTimestamp()).intValue()))
                .body("[0].addedBy", equalTo(moderator.getPlatformId()))
                .body("[0].addedAt", not(nullValue()))
                .body("[0].addedByDisplayName", equalTo(moderator.getDisplayName()))
                .body("[0].type", equalTo(NOTE.toString()))
                .body("[1].size()", is(7))
                .body("[1].id", not(nullValue()))
                .body("[1].note", equalTo(bookmark2.getNote()))
                .body("[1].offset", equalTo(roomOffsetProvider
                        .calculateOffset(startedRoom.getId(), bookmark2.getTimestamp()).intValue()))
                .body("[1].addedBy", equalTo(moderator.getPlatformId()))
                .body("[1].addedAt", not(nullValue()))
                .body("[1].addedByDisplayName", equalTo(moderator.getDisplayName()))
                .body("[1].type", equalTo(NOTE.toString()))
                .body("[2].size()", is(7))
                .body("[2].id", not(nullValue()))
                .body("[2].note", nullValue())
                .body("[2].offset", equalTo(roomOffsetProvider
                        .calculateOffset(startedRoom.getId(), bookmark3.getTimestamp()).intValue()))
                .body("[2].addedBy", equalTo(moderator.getPlatformId()))
                .body("[2].addedAt", not(nullValue()))
                .body("[2].addedByDisplayName", equalTo(moderator.getDisplayName()))
                .body("[2].type", equalTo(PII.toString()));
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"RESPONDENT"})
    @DisplayName("Bookmarks from other session not returned")
    void okBackroomOtherSessionNotReturned(ParticipantRole role) {
        var researchSession = setUpFullRoom();
        var moderator1 = getModerator(researchSession);
        var participant = getParticipant(researchSession, role);
        var researchSession2 = setUpFullRoom();
        var moderator2 = getModerator(researchSession2);

        // room 1
        given()
                .when()
                .headers(roomParticipantHeaders(moderator1))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var bookmark1 = generateBookmark();
        given()
                .headers(roomParticipantHeadersWithContent(moderator1))
                .body(bookmark1)
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(CREATED.value());

        var bookmark2 = generateBookmark();
        given()
                .headers(roomParticipantHeadersWithContent(moderator1))
                .body(bookmark2)
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .and().statusCode(CREATED.value());

        // room 2
        given()
                .when()
                .headers(roomParticipantHeaders(moderator2))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var bookmark3 = generateBookmark();
        given()
                .headers(roomParticipantHeadersWithContent(moderator2))
                .body(bookmark3)
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(CREATED.value());

        var bookmark4 = generateBookmark();
        given()
                .headers(roomParticipantHeadersWithContent(moderator2))
                .body(bookmark4)
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .and().statusCode(CREATED.value());

        given()
                .when()
                .headers(roomParticipantHeaders(participant))
                .get(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(2))
                .body("[0].note", equalTo(bookmark1.getNote()))
                .body("[0].type", equalTo(NOTE.toString()))
                .body("[1].note", nullValue())
                .body("[1].type", equalTo(PII.toString()));
    }

    private AddBookmarkDto generateBookmark() {
        return new AddBookmarkDto()
                .setNote(make())
                .setTimestamp(now().toEpochMilli());
    }
}
