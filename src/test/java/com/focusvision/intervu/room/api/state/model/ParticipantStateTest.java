package com.focusvision.intervu.room.api.state.model;

import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.dto.ChannelDto;
import com.focusvision.intervu.room.api.model.dto.ConferenceDto;
import java.util.UUID;
import org.junit.jupiter.api.Test;

public class ParticipantStateTest {

  @Test
  void markAsPresentOverPhone_previouslyOffline() {
    var participantState = constructParticipantState(false, false, false);

    participantState.markAsPresentOverPhone();

    assertTrue(participantState.isOnline());
    assertTrue(participantState.isPresentOverPhone());
    assertFalse(participantState.isPresentOverWeb());
  }

  @Test
  void markAsNotPresentOverPhone_previouslyPresentOverWebAndPhone() {
    var participantState = constructParticipantState(true, true, true);

    participantState.markAsNotPresentOverPhone();

    assertTrue(participantState.isOnline());
    assertFalse(participantState.isPresentOverPhone());
    assertTrue(participantState.isPresentOverWeb());
  }

  @Test
  void markAsNotPresentOverPhone_previouslyPresentOverPhone() {
    var participantState = constructParticipantState(true, false, true);

    participantState.markAsNotPresentOverPhone();

    assertFalse(participantState.isOnline());
    assertFalse(participantState.isPresentOverPhone());
    assertFalse(participantState.isPresentOverWeb());
  }

  @Test
  void markAsPresentOverPhone_previouslyPresentOverWeb() {
    var participantState = constructParticipantState(true, true, false);

    participantState.markAsPresentOverPhone();

    assertTrue(participantState.isOnline());
    assertTrue(participantState.isPresentOverPhone());
    assertTrue(participantState.isPresentOverWeb());
  }

  @Test
  void markAsPresentOverWeb_previouslyOffline() {
    var participantState = constructParticipantState(false, false, false);

    participantState.markAsPresentOverWeb();

    assertTrue(participantState.isOnline());
    assertFalse(participantState.isPresentOverPhone());
    assertTrue(participantState.isPresentOverWeb());
  }

  @Test
  void markAsPresentOverWeb_previouslyPresentOverPhone() {
    var participantState = constructParticipantState(true, false, true);

    participantState.markAsPresentOverWeb();

    assertTrue(participantState.isOnline());
    assertTrue(participantState.isPresentOverPhone());
    assertTrue(participantState.isPresentOverWeb());
  }

  @Test
  void markAsNotPresentOverWeb_previouslyPresentOverWebAndPhone() {
    var participantState = constructParticipantState(true, true, true);

    participantState.markAsNotPresentOverWeb();

    assertTrue(participantState.isOnline());
    assertTrue(participantState.isPresentOverPhone());
    assertFalse(participantState.isPresentOverWeb());
  }

  @Test
  void markAsNotPresentOverWeb_previouslyPresentOverWeb() {
    var participantState = constructParticipantState(true, true, false);

    participantState.markAsNotPresentOverWeb();

    assertFalse(participantState.isOnline());
    assertFalse(participantState.isPresentOverPhone());
    assertFalse(participantState.isPresentOverWeb());
  }

  private ParticipantState constructParticipantState(
      boolean online, boolean presentOverWeb, boolean presentOverPhone) {
    return new ParticipantState(
        UUID.randomUUID(),
        make(),
        make(),
        online,
        presentOverWeb,
        presentOverPhone,
        ParticipantRole.MODERATOR,
        false,
        false,
        true,
        false,
        false,
        false,
        false,
        false,
        false,
        0L,
        new ConferenceDto(),
        new ChannelDto());
  }
}
