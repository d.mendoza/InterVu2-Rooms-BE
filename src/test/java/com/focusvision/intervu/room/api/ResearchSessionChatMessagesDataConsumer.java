package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.model.messaging.ResearchSessionChatMessagesData;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class ResearchSessionChatMessagesDataConsumer {

    private SimpleMessageListenerContainer container;
    private CountDownLatch latch;
    private List<ResearchSessionChatMessagesData> messages = new ArrayList<>();

    public ResearchSessionChatMessagesDataConsumer(final ConnectionFactory connectionFactory,
                                                   MessageConverter messageConverter,
                                                   String queueName,
                                                   int messageCount) {
        var adapter = new MessageListenerAdapter(this, "receiveMessage");
        adapter.setMessageConverter(messageConverter);

        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setupMessageListener(adapter);

        latch = new CountDownLatch(messageCount);
    }

    @SuppressWarnings("unused")
    public void receiveMessage(ResearchSessionChatMessagesData message) {
        messages.add(message);
        latch.countDown();
    }

    public void stop() {
        container.stop();
    }

    public ResearchSessionChatMessagesDataConsumer start() {
        container.start();

        return this;
    }

    public boolean allMessagesReceived(long timeout, TimeUnit seconds) throws InterruptedException {
        return latch.await(timeout, seconds);
    }

    public Optional<ResearchSessionChatMessagesData> getMessage(String sessionId) {
        return messages.stream()
                .filter(chatMessagesData -> chatMessagesData.getId().equals(sessionId))
                .findFirst();
    }
}
