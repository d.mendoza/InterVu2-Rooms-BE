package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.model.messaging.ProjectSanitiseRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

@SuppressWarnings({"unused"})
@RequiredArgsConstructor
public class ProjectSanitiseRequestConsumer {

    private SimpleMessageListenerContainer container;
    private CountDownLatch latch;
    private List<ProjectSanitiseRequest> messages = new ArrayList<>();

    public ProjectSanitiseRequestConsumer(final ConnectionFactory connectionFactory,
                                          MessageConverter messageConverter,
                                          String queueName,
                                          int messageCount) {
        var adapter = new MessageListenerAdapter(this, "receiveMessage");
        adapter.setMessageConverter(messageConverter);

        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setupMessageListener(adapter);

        latch = new CountDownLatch(messageCount);
    }

    public void receiveMessage(ProjectSanitiseRequest message) {
        messages.add(message);
        latch.countDown();
    }

    public void stop() {
        container.stop();
    }

    public ProjectSanitiseRequestConsumer start() {
        container.start();

        return this;
    }

    public Optional<ProjectSanitiseRequest> getMessage(Long id) {
        return messages.stream()
                .filter(processed -> id.equals(processed.getPlatformId()))
                .findFirst();
    }
}
