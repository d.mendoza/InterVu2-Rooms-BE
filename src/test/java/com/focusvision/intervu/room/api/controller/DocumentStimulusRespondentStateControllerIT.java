package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.StimulusGlobalChannelStompFrameHandler;
import com.focusvision.intervu.room.api.exception.ErrorCode;
import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.stimulus.document.model.DocumentStimulusActionMessageDto;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

/**
 * Integration tests for
 * {@link DocumentSharingStimulusStateController#lastAction(String, AuthenticatedParticipant)}.
 */
@DisplayName("Document stimulus states for respondents")
public class DocumentStimulusRespondentStateControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(PARTICIPANT_DOCUMENT_STIMULUS_STATE), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned moderators")
    void unauthorizedBannedModerator() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        participantRepository.save(respondent.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(PARTICIPANT_DOCUMENT_STIMULUS_STATE), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden - not a document stimulus")
    void forbidden_notDocumentStimulus() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var pollingStimulus = getPollStimulus(researchSession);
        var dragAndDropStimulus = getDragAndDropStimulus(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(PARTICIPANT_DOCUMENT_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(FORBIDDEN.value())
                .body("code", equalTo(ErrorCode.FORBIDDEN.name()))
                .body("message", equalTo("Fetching states forbidden."));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(PARTICIPANT_DOCUMENT_STIMULUS_STATE), dragAndDropStimulus.getId())
                .then()
                .statusCode(FORBIDDEN.value())
                .body("code", equalTo(ErrorCode.FORBIDDEN.name()))
                .body("message", equalTo("Fetching states forbidden."));
    }

    @Test
    @DisplayName("Ok, states fetched for respondent")
    void okStatesFetchedForRespondent() {
        var researchSession = setUpFullRoom();
        var documentStimulus = getDocumentSharingStimulus(researchSession);
        var content = RandomString.make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorDocumentStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);
        var respondentDocumentStimulusActionHandler = new StimulusGlobalChannelStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, documentStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var globalChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorDocumentStimulusActionHandler);
        subscribe(respondentSession, globalChannel, respondentDocumentStimulusActionHandler);

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var pageChangeAction = new DocumentStimulusActionMessageDto()
                .setStimulusId(documentStimulus.getId())
                .setContent(content)
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(DOCUMENT_STIMULUS_PAGE_CHANGE_DESTINATION, pageChangeAction);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), documentStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(1))
                .body("[0].size()", Matchers.equalTo(3))
                .body("[0].participantId", equalTo(moderator.getId().toString()))
                .body("[0].content", equalTo(content))
                .body("[0].stimulusId", equalTo(documentStimulus.getId().toString()))
                .extract().as(StimulusStateDto[].class);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(PARTICIPANT_DOCUMENT_STIMULUS_STATE), documentStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(3))
                .body("participantId", equalTo(moderator.getId().toString()))
                .body("content", equalTo(content))
                .body("stimulusId", equalTo(documentStimulus.getId().toString()));
    }
}
