package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link HomeController#home}
 */
class HomeControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Home redirects to Swagger")
    void home() {
        given()
                .when()
                .get(url(HOME_URL))
                .then()
                .statusCode(OK.value())
                .body(CoreMatchers.containsString("Swagger UI"));
    }
}