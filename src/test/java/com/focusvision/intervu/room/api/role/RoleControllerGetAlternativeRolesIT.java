package com.focusvision.intervu.room.api.role;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link RoleController#getAlternativeRoles}.
 */
@DisplayName("Get participant alternative roles")
class RoleControllerGetAlternativeRolesIT extends HttpBasedIT {

  @Test
  @DisplayName("Forbidden for unauthorized")
  void forbiddenUnauthorized() {
    given()
        .when()
        .get(url(GET_ALTERNATIVE_ROLES_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE,
      names = {"MODERATOR"})
  @DisplayName("Forbidden for all roles except moderator")
  void forbiddenForAllExceptModerator(ParticipantRole role) {
    var researchSession = setUpFullRoom();

    given()
        .headers(roomParticipantHeadersWithContent(getParticipant(researchSession, role)))
        .when()
        .get(url(GET_ALTERNATIVE_ROLES_URL))
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Ok for moderator")
  void okForModerator() {
    var researchSession = setUpFullRoom();

    ParticipantAlternativeRolesDto[] response = given()
        .headers(roomParticipantHeaders(getModerator(researchSession)))
        .when()
        .get(url(GET_ALTERNATIVE_ROLES_URL))
        .then()
        .log().all()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(5))
        .body("[0].size()", is(4))
        .extract()
        .as(ParticipantAlternativeRolesDto[].class);

    researchSession.getParticipants().forEach(participant -> {
      ParticipantAlternativeRolesDto dto = Arrays.stream(
              response).filter(p -> p.getParticipantId().equals(participant.getId())).findFirst()
          .orElseThrow();
      assertThat(dto.getCurrentRole()).isEqualByComparingTo(participant.getRole());
      assertThat(dto.getDisplayName()).isEqualTo(participant.getDisplayName());
      switch (dto.getCurrentRole()) {
        case MODERATOR, TRANSLATOR -> assertThat(dto.getAlternativeRoles()).containsOnly(OBSERVER);
        case OBSERVER -> assertThat(dto.getAlternativeRoles()).containsOnly(MODERATOR);
        default -> assertThat(dto.getAlternativeRoles()).isEmpty();
      }
    });

  }
}
