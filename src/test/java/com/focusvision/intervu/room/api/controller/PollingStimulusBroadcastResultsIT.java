package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.GenericStimulusGlobalChannelEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.StimulusGlobalChannelStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.PollStimulusRespondentResultDto;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionNotificationMessage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.focusvision.intervu.room.api.event.stimulus.model.StimulusGlobalDataType.ACTION;
import static com.focusvision.intervu.room.api.event.stimulus.model.StimulusGlobalDataType.BROADCAST;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_BROADCAST_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_BROADCAST_ENABLED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_ANSWERED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_RANKED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_VOTED;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.junit.jupiter.api.Assertions.assertAll;

/**
 * IT tests for poling stimulus events {@link PollingStimulusActionController}.
 */
@DisplayName("Poling stimulus broadcast results")
class PollingStimulusBroadcastResultsIT extends HttpBasedIT {

    @Test
    @DisplayName("Vote")
    void vote() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var respondentResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);
        var respondentStimulusGlobalResultsHandler = new StimulusGlobalChannelStompFrameHandler(mapper);
        var moderatorRoomStateHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        var respondentRoomStateHandler = new RoomStateStompFrameHandler(mapper, respondentResultKeeper::complete);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var globalStimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());

        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);
        subscribe(respondentSession, globalStimulusChannel, respondentStimulusGlobalResultsHandler);
        subscribe(moderatorSession, roomChannel, moderatorRoomStateHandler);
        subscribe(respondentSession, roomChannel, respondentRoomStateHandler);

        waitForWs();
        var voteAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction);
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        disableStimulusInteraction(moderator);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isTrue();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isTrue();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(DISABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        enableStimulusInteraction(moderator);
        waitForWs();

        var voteAction2 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction2);
        waitForWs();

        var voteAction3 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction3);
        waitForWs();

        finishRoom(moderator);
        waitForWs();

        assertAll(
                () -> assertThat(moderatorRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_ENABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(true),
                () -> assertThat(moderatorRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_DISABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(false),
                () -> assertThat(respondentRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_ENABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(true),
                () -> assertThat(respondentRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_DISABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(false)
        );

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();
        assertAll(
                () -> assertThat(moderatorPollStimulusMessages.size()).isEqualTo(3),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getRoomId)
                        .containsOnly(researchSession.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getParticipantId)
                        .containsOnly(respondent.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getStimulusId)
                        .containsOnly(pollingStimulus.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getType)
                        .containsOnly(POLL_VOTED),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getContent)
                        .containsOnly(voteAction.getContent(), voteAction2.getContent(), voteAction3.getContent())
        );

        var messages = respondentStimulusGlobalResultsHandler.getMessages();
        var respondentBroadcastResults = messages.stream()
                .filter(event -> BROADCAST.equals(event.getEventType()))
                .toList();
        var respondentVoteMessages = messages.stream()
                .filter(event -> ACTION.equals(event.getEventType()))
                .toList();

        assertAll(
                () -> assertThat(respondentBroadcastResults.size()).isEqualTo(1),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getEventType)
                        .isEqualTo(BROADCAST),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getDestination)
                        .isEqualTo(globalStimulusChannel),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getData)
                        .isEqualToComparingOnlyGivenFields(new PollStimulusRespondentResultDto()
                                .setStimulusId(pollingStimulus.getId())
                                .setResults(List.of(voteAction.getContent()))),
                () -> assertThat(respondentVoteMessages.size()).isEqualTo(0));
    }

    @Test
    @DisplayName("Rank")
    void rank() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var respondentResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);
        var respondentStimulusGlobalResultsHandler = new StimulusGlobalChannelStompFrameHandler(mapper);
        var moderatorRoomStateHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        var respondentRoomStateHandler = new RoomStateStompFrameHandler(mapper, respondentResultKeeper::complete);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var globalStimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());

        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);
        subscribe(respondentSession, globalStimulusChannel, respondentStimulusGlobalResultsHandler);
        subscribe(moderatorSession, roomChannel, moderatorRoomStateHandler);
        subscribe(respondentSession, roomChannel, respondentRoomStateHandler);

        waitForWs();
        var rankAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_RANKING_DESTINATION, rankAction);
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        disableStimulusInteraction(moderator);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isTrue();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isTrue();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(DISABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        enableStimulusInteraction(moderator);
        waitForWs();

        var rankAction2 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_RANKING_DESTINATION, rankAction2);
        waitForWs();

        var rankAction3 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_RANKING_DESTINATION, rankAction3);
        waitForWs();

        finishRoom(moderator);
        waitForWs();

        assertAll(
                () -> assertThat(moderatorRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_ENABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(true),
                () -> assertThat(moderatorRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_DISABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(false),
                () -> assertThat(respondentRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_ENABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(true),
                () -> assertThat(respondentRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_DISABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(false)
        );

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();
        assertAll(
                () -> assertThat(moderatorPollStimulusMessages.size()).isEqualTo(3),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getRoomId)
                        .containsOnly(researchSession.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getParticipantId)
                        .containsOnly(respondent.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getStimulusId)
                        .containsOnly(pollingStimulus.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getType)
                        .containsOnly(POLL_RANKED),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getContent)
                        .containsOnly(rankAction.getContent(), rankAction2.getContent(), rankAction3.getContent())
        );

        var messages = respondentStimulusGlobalResultsHandler.getMessages();
        var respondentBroadcastResults = messages.stream()
                .filter(event -> BROADCAST.equals(event.getEventType()))
                .toList();
        var respondentRankMessages = messages.stream()
                .filter(event -> ACTION.equals(event.getEventType()))
                .toList();

        assertAll(
                () -> assertThat(respondentBroadcastResults.size()).isEqualTo(1),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getEventType)
                        .isEqualTo(BROADCAST),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getDestination)
                        .isEqualTo(globalStimulusChannel),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getData)
                        .isEqualToComparingOnlyGivenFields(new PollStimulusRespondentResultDto()
                                .setStimulusId(pollingStimulus.getId())
                                .setResults(List.of(rankAction.getContent()))),
                () -> assertThat(respondentRankMessages.size()).isEqualTo(0));
    }

    @Test
    @DisplayName("Answer")
    void answer() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var respondentResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);
        var respondentStimulusGlobalResultsHandler = new StimulusGlobalChannelStompFrameHandler(mapper);
        var moderatorRoomStateHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        var respondentRoomStateHandler = new RoomStateStompFrameHandler(mapper, respondentResultKeeper::complete);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var globalStimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());

        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);
        subscribe(respondentSession, globalStimulusChannel, respondentStimulusGlobalResultsHandler);
        subscribe(moderatorSession, roomChannel, moderatorRoomStateHandler);
        subscribe(respondentSession, roomChannel, respondentRoomStateHandler);

        waitForWs();
        var answerAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, answerAction);
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        disableStimulusInteraction(moderator);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isTrue();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isTrue();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(DISABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        enableStimulusInteraction(moderator);
        waitForWs();

        var answerAction2 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, answerAction2);
        waitForWs();

        var answerAction3 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, answerAction3);
        waitForWs();

        finishRoom(moderator);
        waitForWs();

        assertAll(
                () -> assertThat(moderatorRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_ENABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(true),
                () -> assertThat(moderatorRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_DISABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(false),
                () -> assertThat(respondentRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_ENABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(true),
                () -> assertThat(respondentRoomStateHandler.getStates())
                        .filteredOn(event -> STIMULUS_BROADCAST_DISABLED.equals(event.getEventType()))
                        .extracting(input -> input.getData().getStimulus().isBroadcastResults())
                        .contains(false)
        );

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();
        assertAll(
                () -> assertThat(moderatorPollStimulusMessages.size()).isEqualTo(3),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getRoomId)
                        .containsOnly(researchSession.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getParticipantId)
                        .containsOnly(respondent.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getStimulusId)
                        .containsOnly(pollingStimulus.getId()),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getType)
                        .containsOnly(POLL_ANSWERED),
                () -> assertThat(moderatorPollStimulusMessages)
                        .extracting(StimulusActionNotificationMessage::getContent)
                        .containsOnly(answerAction.getContent(), answerAction2.getContent(), answerAction3.getContent())
        );

        var messages = respondentStimulusGlobalResultsHandler.getMessages();
        var respondentBroadcastResults = messages.stream()
                .filter(event -> BROADCAST.equals(event.getEventType()))
                .toList();
        var respondentTextAnswerMessages = messages.stream()
                .filter(event -> ACTION.equals(event.getEventType()))
                .toList();

        assertAll(
                () -> assertThat(respondentBroadcastResults.size()).isEqualTo(1),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getEventType)
                        .isEqualTo(BROADCAST),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getDestination)
                        .isEqualTo(globalStimulusChannel),
                () -> assertThat(respondentBroadcastResults.get(0))
                        .extracting(GenericStimulusGlobalChannelEvent::getData)
                        .isEqualToComparingOnlyGivenFields(new PollStimulusRespondentResultDto()
                                .setStimulusId(pollingStimulus.getId())
                                .setResults(List.of(answerAction.getContent()))),
                () -> assertThat(respondentTextAnswerMessages.size()).isEqualTo(0));
    }

    @Test
    @DisplayName("Stimuli interaction enabled")
    void stimuliInteractionEnabled() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var respondentResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        connectRoomWs(moderator);
        connectRoomWs(respondent);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);
        var respondentStimulusGlobalResultsHandler = new StimulusGlobalChannelStompFrameHandler(mapper);
        var moderatorRoomStateHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        var respondentRoomStateHandler = new RoomStateStompFrameHandler(mapper, respondentResultKeeper::complete);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var globalStimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());

        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);
        subscribe(respondentSession, globalStimulusChannel, respondentStimulusGlobalResultsHandler);
        subscribe(moderatorSession, roomChannel, moderatorRoomStateHandler);
        subscribe(respondentSession, roomChannel, respondentRoomStateHandler);

        waitForWs();
        var voteAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction);
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        disableStimulusInteraction(moderator);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isTrue();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isTrue();

        enableStimulusInteraction(moderator);
        waitForWs();
        assertThat(getRoomState(moderator).getRoomState().getStimulus().getInteractionEnabled()).isFalse();
    }

    @Test
    @DisplayName("Empty broadcast")
    void emptyBroadcast() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_BROADCAST_RESULTS), pollingStimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        assertThat(getRoomState(moderator).getRoomState().getStimulus().isBroadcastResults()).isFalse();
        assertThat(getRoomState(respondent).getRoomState().getStimulus().isBroadcastResults()).isFalse();
    }

}
