package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingResult;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class RespondentTestingResultDataConsumer {

    private SimpleMessageListenerContainer container;
    private CountDownLatch latch;
    private List<ParticipantTestingResult> messages = new ArrayList<>();

    public RespondentTestingResultDataConsumer(final ConnectionFactory connectionFactory,
                                               MessageConverter messageConverter,
                                               String queueName,
                                               int messageCount) {
        var adapter = new MessageListenerAdapter(this, "receiveMessage");
        adapter.setMessageConverter(messageConverter);

        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setupMessageListener(adapter);

        latch = new CountDownLatch(messageCount);
    }

    @SuppressWarnings("unused")
    public void receiveMessage(ParticipantTestingResult message) {
        messages.add(message);
        latch.countDown();
    }

    public void stop() {
        container.stop();
    }

    public RespondentTestingResultDataConsumer start() {
        container.start();

        return this;
    }

    public boolean allMessagesReceived(long timeout, TimeUnit seconds) throws InterruptedException {
        return latch.await(timeout, seconds);
    }

    public Optional<ParticipantTestingResult> getMessage(Participant participant) {
        return messages.stream()
                .filter(processed -> participant.getPlatformId().equals(processed.getRespondentId()))
                .filter(processed -> participant.getResearchSession().getPlatformId()
                        .equals(processed.getSessionId()))
                .findAny();
    }
}
