package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.model.CompositionLayout.MOSAIC;
import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.entity.Recording.State;
import com.focusvision.intervu.room.api.repository.ConferenceRepository;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link RoomRecordingController#regenerateRecording}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Regenerate recording")
class RoomRecordingControllerRegenerateRecordingIT extends HttpBasedIT {

  @Autowired
  private ConferenceRepository conferenceRepository;

  @Test
  @DisplayName("Unauthorized")
  void regenerate_unauthorized() {
    given()
        .when()
        .put(url(REGENERATE_RECORDING_URL), make())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Forbidden for participant")
  void regenerate_forbiddenForParticipant(ParticipantRole role) {
    var room = setUpFullRoom();
    var caller = getParticipant(room, role);
    given()
        .when()
        .headers(roomParticipantHeaders(caller))
        .param("layout", MOSAIC.name())
        .put(url(REGENERATE_RECORDING_URL), RandomString.make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Forbidden for admin")
  void regenerate_forbiddenForAdmin() {
    var room = setUpFullRoom();
    var admin = getAdmin(room);
    given()
        .when()
        .headers(intervuUserHeaders(admin))
        .param("layout", MOSAIC.name())
        .put(url(REGENERATE_RECORDING_URL), RandomString.make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Conference not found")
  void regenerate_conferenceNotFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .param("layout", MOSAIC.name())
        .put(url(REGENERATE_RECORDING_URL), RandomString.make())
        .then()
        .statusCode(NOT_FOUND.value());
  }

  @Test
  @DisplayName("Invalid layout")
  void regenerate_invalidLayout() {
    var room = setUpFullRoom();
    conferenceRepository.save(new Conference()
        .setRoomSid(make())
        .setResearchSession(room));

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .param("layout", MOSAIC.name())
        .put(url(REGENERATE_RECORDING_URL), room.getPlatformId())
        .then()
        .statusCode(BAD_REQUEST.value())
        .body("message", containsString("Composition layout is not allowed"));
  }

  @Test
  @DisplayName("Ok")
  void regenerate_ok() {
    var room = setUpFullRoom();
    var roomSid = make();
    var conference = conferenceRepository.save(new Conference()
        .setRoomSid(roomSid)
        .setResearchSession(room));

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .param("layout", PICTURE_IN_PICTURE_WITH_COLUMN.name())
        .put(url(REGENERATE_RECORDING_URL), room.getPlatformId())
        .then()
        .statusCode(NO_CONTENT.value());

    conference = conferenceRepository.getById(conference.getId());
    assertThat(conference.getRecordings()).extracting(Recording::getState)
        .containsExactly(State.PENDING);
    assertThat(conference.getRecordings()).extracting(Recording::getCompositionLayout)
        .containsExactly(PICTURE_IN_PICTURE_WITH_COLUMN);
  }
}
