package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static com.focusvision.intervu.room.api.common.model.StimulusType.DOCUMENT_SHARING;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link StimulusController#enableSync(String, AuthenticatedParticipant)} endpoint.
 */
@DisplayName("Enable stimulus sync")
public class StimulusControllerEnableSyncIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .put(url(ENABLE_STIMULUS_SYNC), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned moderator")
    void unauthorizedBannedModerator() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_STIMULUS_SYNC), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for non moderators")
    void forbiddenForParticipant() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var stimulus = researchSession.getStimuli().get(0);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(ENABLE_STIMULUS_SYNC), stimulus.getId())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .put(url(ENABLE_STIMULUS_SYNC), stimulus.getId())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(ENABLE_STIMULUS_SYNC), stimulus.getId())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Disable/enable by moderator")
    void disableAndEnableByModerator() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulus = getDocumentSharingStimulus(researchSession);

        activateStimulus(moderator, stimulus.getId());
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(DISABLE_STIMULUS_SYNC), stimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        var stimulusState = roomStateOperator.getCurrentRoomState(researchSession.getId()).getActiveStimulus()
                .orElseThrow();

        assertThat(stimulusState.isSyncEnabled()).isFalse();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_STIMULUS_SYNC), stimulus.getId())
                .then()
                .statusCode(HttpStatus.OK.value());
        waitForWs();

        stimulusState = roomStateOperator.getCurrentRoomState(researchSession.getId()).getStimuli().stream()
                .filter(ss -> DOCUMENT_SHARING.equals(ss.getType()))
                .findFirst().orElseThrow();

        assertThat(stimulusState.isSyncEnabled()).isTrue();

    }
}
