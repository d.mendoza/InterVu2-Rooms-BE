package com.focusvision.intervu.room.api.chat;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;

import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * IT tests for chat functionality.
 *
 * @author Branko Ostojic
 */
class ChatMessagingIT extends HttpBasedIT {

    @Test
    @DisplayName("Connecting forbidden for unauthorized")
    void connect_fail() {
        String wsUrl = "ws://127.0.0.1:" + port + "/ws";
        stompClient = wsTestUtils.createWebSocketClient(mappingJackson2MessageConverter());

        ExecutionException e = assertThrows(ExecutionException.class,
                () -> stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get());
        assertThat(e.getMessage()).contains("401");
    }

    @Test
    @DisplayName("Connecting forbidden for invalid authentication")
    void connect_failInvalidAuth() {
        RuntimeException e = assertThrows(RuntimeException.class, () -> connectWs(make()));
        assertThat(e.getMessage()).contains("401");
    }

    @Test
    @DisplayName("Connecting forbidden for room token in dashboard")
    void connect_failInvalidToken() {
        RuntimeException e = assertThrows(RuntimeException.class,
                () -> connectWs(roomAuthToken(getModerator(setUpFullRoom()).getInvitationToken())));
        assertThat(e.getMessage()).contains("401");
    }

    @Test
    @DisplayName("Connecting success")
    void connect_success() {
        var session = connectWs(intervuAuthToken(getModerator(setUpFullRoom()).getPlatformId()));

        assertThat(session.isConnected()).isTrue();
    }
}
