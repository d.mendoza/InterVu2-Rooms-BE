package com.focusvision.intervu.room.api.export.api;

import static com.focusvision.intervu.room.api.common.model.BookmarkType.NOTE;
import static com.focusvision.intervu.room.api.common.model.BookmarkType.PII;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_ONLINE;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CREATED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_FINISHED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_STARTED;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.bookmark.model.AddBookmarkDto;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 * Integration test for {@link BookmarkExportController}.
 */
class BookmarkExportControllerIT extends HttpBasedIT {

  @Test
  @DisplayName("Bookmarks export - unauthorized")
  void exportBookmarks_unauthorized() {
    given()
        .when()
        .get(url(BOOKMARKS_EXPORT_URL), make())
        .then()
        .statusCode(UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Bookmarks export - forbidden for participants")
  void exportBookmarks_forbiddenForParticipants(ParticipantRole role) {
    var room = setUpFullRoom();
    var caller = getParticipant(room, role);

    given()
        .when()
        .headers(roomParticipantHeaders(caller))
        .get(url(BOOKMARKS_EXPORT_URL), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Bookmarks export - forbidden for admin")
  void exportBookmarks_forbiddenForAdmin() {
    var room = setUpFullRoom();
    var caller = getAdmin(room);

    given()
        .when()
        .headers(roomParticipantHeaders(caller))
        .get(url(BOOKMARKS_EXPORT_URL), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }
  
  @Test
  @DisplayName("Bookmarks export - conference not found")
  void exportBookmarks_conferenceNotFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(BOOKMARKS_EXPORT_URL), make())
        .then()
        .statusCode(NOT_FOUND.value());
  }

  @Test
  @DisplayName("Bookmarks export - ok, system user")
  void exportBookmarks_okSystemUser() {
    var room = setUpFullRoom();
    var moderator = getModerator(room);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    var addBookmarkDto1 = new AddBookmarkDto()
        .setNote(RandomString.make())
        .setTimestamp(now().plusMillis(5000).toEpochMilli());

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(addBookmarkDto1)
        .when()
        .post(url(ROOM_BOOKMARKS_URL))
        .then()
        .and().statusCode(CREATED.value());

    var addBookmarkDto2 = new AddBookmarkDto()
        .setNote(RandomString.make())
        .setTimestamp(now().plusMillis(5000).toEpochMilli());

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(addBookmarkDto2)
        .when()
        .post(url(ROOM_BOOKMARKS_PII_URL))
        .then()
        .and().statusCode(CREATED.value());

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(BOOKMARKS_EXPORT_URL), room.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("size()", equalTo(2))
        .body("[0].size()", equalTo(4))
        .body("[0].offset", notNullValue())
        .body("[0].note", equalTo(addBookmarkDto1.getNote()))
        .body("[0].createdBy", equalTo(moderator.getPlatformId()))
        .body("[0].type", equalTo(NOTE.name()))
        .body("[1].size()", equalTo(4))
        .body("[1].offset", notNullValue())
        .body("[1].note", equalTo(addBookmarkDto2.getNote()))
        .body("[1].createdBy", equalTo(moderator.getPlatformId()))
        .body("[1].type", equalTo(PII.name()));
  }
}
