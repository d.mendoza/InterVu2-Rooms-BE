package com.focusvision.intervu.room.api.role;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.model.event.EventType.PARTICIPANT_ROLE_CHANGED;
import static io.restassured.RestAssured.given;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.junit.jupiter.params.provider.EnumSource.Mode.INCLUDE;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantRoleChangedStompFrameHandler;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.event.ParticipantRoleChangedEvent;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link RoleController#setAlternativeRole}.
 */
@DisplayName("Set participant alternative role")
class RoleControllerSetAlternativeRoleIT extends HttpBasedIT {

  @Test
  @DisplayName("Forbidden for unauthorized")
  void forbiddenUnauthorized() {
    given()
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(MODERATOR))
        .post(url(SET_ALTERNATIVE_ROLES_URL), randomUUID())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE,
      names = {"MODERATOR"})
  @DisplayName("Forbidden for all roles except moderator")
  void forbiddenForAllExceptModerator(ParticipantRole role) {
    var researchSession = setUpFullRoom();

    given()
        .headers(roomParticipantHeadersWithContent(getParticipant(researchSession, role)))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(MODERATOR))
        .post(url(SET_ALTERNATIVE_ROLES_URL), getObserver(researchSession).getId())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = INCLUDE, names = {"MODERATOR"})
  @DisplayName("Observer allowed alternative roles")
  void observer_allowedAlternativeRoles(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var observer = getObserver(researchSession);
    var observerResultKeeper = new CompletableFuture<ParticipantRoleChangedEvent>();

    var participantHandler = new ParticipantRoleChangedStompFrameHandler(mapper,
        observerResultKeeper::complete);
    var session = connectRoomWs(observer);
    subscribe(
        session,
        communicationChannelService.getParticipantChannel(observer.getId()),
        participantHandler);

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(role))
        .post(url(SET_ALTERNATIVE_ROLES_URL), observer.getId())
        .then()
        .log().all()
        .statusCode(HttpStatus.OK.value());

    waitForWs();

    assertThat(participantHandler.getEvents()).hasSize(1);
    assertThat(participantHandler.getEvents().get(0).getEventType()).isEqualTo(
        PARTICIPANT_ROLE_CHANGED);
    assertThat(participantHandler.getEvents().get(0).getData().getRole()).isEqualTo(role);
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"MODERATOR"})
  @DisplayName("Observer forbidden alternative roles")
  void observer_forbiddenAlternativeRoles(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var observer = getObserver(researchSession);
    var observerResultKeeper = new CompletableFuture<ParticipantRoleChangedEvent>();

    var participantHandler = new ParticipantRoleChangedStompFrameHandler(mapper,
        observerResultKeeper::complete);
    var session = connectRoomWs(observer);
    subscribe(
        session,
        communicationChannelService.getParticipantChannel(observer.getId()),
        participantHandler);

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(role))
        .post(url(SET_ALTERNATIVE_ROLES_URL), observer.getId())
        .then()
        .log().all()
        .statusCode(HttpStatus.BAD_REQUEST.value());

    waitForWs();

    assertThat(participantHandler.getEvents()).hasSize(0);
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = INCLUDE, names = {"OBSERVER"})
  @DisplayName("Moderator allowed alternative roles")
  void moderator_allowedAlternativeRoles(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var resultKeeper = new CompletableFuture<ParticipantRoleChangedEvent>();

    var participantHandler = new ParticipantRoleChangedStompFrameHandler(mapper,
        resultKeeper::complete);
    var session = connectRoomWs(moderator);
    subscribe(
        session,
        communicationChannelService.getParticipantChannel(moderator.getId()),
        participantHandler);

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(role))
        .post(url(SET_ALTERNATIVE_ROLES_URL), moderator.getId())
        .then()
        .log().all()
        .statusCode(HttpStatus.OK.value());

    waitForWs();

    assertThat(participantHandler.getEvents()).hasSize(1);
    assertThat(participantHandler.getEvents().get(0).getEventType()).isEqualTo(
        PARTICIPANT_ROLE_CHANGED);
    assertThat(participantHandler.getEvents().get(0).getData().getRole()).isEqualTo(role);
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"OBSERVER"})
  @DisplayName("Moderator forbidden alternative roles")
  void moderator_forbiddenAlternativeRoles(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var resultKeeper = new CompletableFuture<ParticipantRoleChangedEvent>();

    var participantHandler = new ParticipantRoleChangedStompFrameHandler(mapper,
        resultKeeper::complete);
    var session = connectRoomWs(moderator);
    subscribe(
        session,
        communicationChannelService.getParticipantChannel(moderator.getId()),
        participantHandler);

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(role))
        .post(url(SET_ALTERNATIVE_ROLES_URL), moderator.getId())
        .then()
        .log().all()
        .statusCode(HttpStatus.BAD_REQUEST.value());

    waitForWs();

    assertThat(participantHandler.getEvents()).hasSize(0);
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = INCLUDE, names = {"OBSERVER"})
  @DisplayName("Translator allowed alternative roles")
  void translator_allowedAlternativeRoles(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var translator = getTranslator(researchSession);
    var resultKeeper = new CompletableFuture<ParticipantRoleChangedEvent>();

    var participantHandler = new ParticipantRoleChangedStompFrameHandler(mapper,
        resultKeeper::complete);
    var session = connectRoomWs(translator);
    subscribe(
        session,
        communicationChannelService.getParticipantChannel(translator.getId()),
        participantHandler);

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(role))
        .post(url(SET_ALTERNATIVE_ROLES_URL), translator.getId())
        .then()
        .log().all()
        .statusCode(HttpStatus.OK.value());

    waitForWs();

    assertThat(participantHandler.getEvents()).hasSize(1);
    assertThat(participantHandler.getEvents().get(0).getEventType()).isEqualTo(
        PARTICIPANT_ROLE_CHANGED);
    assertThat(participantHandler.getEvents().get(0).getData().getRole()).isEqualTo(role);
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"OBSERVER"})
  @DisplayName("Translator forbidden alternative roles")
  void translator_forbiddenAlternativeRoles(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var translator = getTranslator(researchSession);
    var resultKeeper = new CompletableFuture<ParticipantRoleChangedEvent>();

    var participantHandler = new ParticipantRoleChangedStompFrameHandler(mapper,
        resultKeeper::complete);
    var session = connectRoomWs(translator);
    subscribe(
        session,
        communicationChannelService.getParticipantChannel(translator.getId()),
        participantHandler);

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(role))
        .post(url(SET_ALTERNATIVE_ROLES_URL), translator.getId())
        .then()
        .log().all()
        .statusCode(HttpStatus.BAD_REQUEST.value());

    waitForWs();

    assertThat(participantHandler.getEvents()).hasSize(0);
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class)
  @DisplayName("Respondent forbidden alternative roles")
  void respondent_forbiddenAlternativeRoles(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var respondent = getRespondent(researchSession);
    var resultKeeper = new CompletableFuture<ParticipantRoleChangedEvent>();

    var participantHandler = new ParticipantRoleChangedStompFrameHandler(mapper,
        resultKeeper::complete);
    var session = connectRoomWs(respondent);
    subscribe(
        session,
        communicationChannelService.getParticipantChannel(respondent.getId()),
        participantHandler);

    given()
        .headers(roomParticipantHeadersWithContent(moderator))
        .when()
        .body(new SetAlternativeRoleRequestDto().setRole(role))
        .post(url(SET_ALTERNATIVE_ROLES_URL), respondent.getId())
        .then()
        .log().all()
        .statusCode(HttpStatus.BAD_REQUEST.value());

    waitForWs();

    assertThat(participantHandler.getEvents()).hasSize(0);
  }
}
