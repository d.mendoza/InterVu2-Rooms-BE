package com.focusvision.intervu.room.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.model.event.ParticipantStateEvent;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@Getter
@RequiredArgsConstructor
public class ParticipantStateStompFrameHandler implements StompFrameHandler {

    private final ObjectMapper mapper;
    private final Consumer<ParticipantStateEvent> frameHandler;
    private final List<ParticipantStateEvent> states = new ArrayList<>();

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ParticipantStateEvent.class;
    }


    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info("received message: {} with headers: {}", payload, headers);

        ParticipantStateEvent event = mapper.convertValue(payload, ParticipantStateEvent.class);
        frameHandler.accept(event);
        states.add(event);

    }
}
