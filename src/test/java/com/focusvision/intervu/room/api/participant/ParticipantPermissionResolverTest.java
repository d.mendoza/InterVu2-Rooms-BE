package com.focusvision.intervu.room.api.participant;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.BOOKMARK_PII_ADD;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.BOOKMARK_READ;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.BOOKMARK_REGULAR_ADD;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.DRAWING_READ;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.PARTICIPANT_TESTING_READ;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.PARTICIPANT_TESTING_REPORT;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.ROOM_JOIN;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.ROOM_READ;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.SCROLL_READ;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.STIMULI_READ;
import static com.focusvision.intervu.room.api.security.ParticipantPermission.STIMULI_STATE_READ;
import static org.assertj.core.api.Assertions.assertThat;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.security.ParticipantPermission;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ParticipantPermissionResolverTest {

  @Test
  void respondentPermissions() {
    var respondent = new Participant().setRole(RESPONDENT);
    var respondentPermissions = Set.of(
        DRAWING_READ,
        PARTICIPANT_TESTING_READ,
        PARTICIPANT_TESTING_REPORT,
        ROOM_READ,
        ROOM_JOIN,
        STIMULI_READ,
        SCROLL_READ);

    var permissions = ParticipantPermissionResolver.resolve(respondent);

    assertThat(permissions).containsExactlyInAnyOrderElementsOf(respondentPermissions);
  }

  @Test
  void translatorPermissions() {
    var translator = new Participant().setRole(TRANSLATOR);
    var translatorPermissions = Set.of(
        BOOKMARK_READ,
        BOOKMARK_PII_ADD,
        BOOKMARK_REGULAR_ADD,
        DRAWING_READ,
        PARTICIPANT_TESTING_READ,
        PARTICIPANT_TESTING_REPORT,
        ROOM_READ,
        ROOM_JOIN,
        STIMULI_READ,
        SCROLL_READ,
        STIMULI_STATE_READ);

    var permissions = ParticipantPermissionResolver.resolve(translator);

    assertThat(permissions).containsExactlyInAnyOrderElementsOf(translatorPermissions);
  }

}
