package com.focusvision.intervu.room.api.chat.api;

import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.REGULAR;
import static com.focusvision.intervu.room.api.common.model.ChatType.RESPONDENTS;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.chat.model.ChatMessageDto;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link MeetingRoomChatController#respondentChatMessages}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Meeting Room respondents chat history")
class MeetingRoomChatControllerRespondentChatHistoryIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("OK for respondent handle")
    void ok() throws Exception {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var question = make();
        var answer = make();
        var handle = researchSession.getId();

        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_ENABLE_URL))
            .then()
            .statusCode(HttpStatus.OK.value());

        sendMeetingRoomRespondentsChatMessage(respondent, question);
        sendMeetingRoomRespondentsChatMessage(moderator, answer);

        var chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        ChatMessageDto respondentMessage = chatMessages.get(0);
        ChatMessageDto moderatorMessage = chatMessages.get(1);
        assertAll(
                () -> assertThat(respondentMessage.getId()).isNotNull(),
                () -> assertThat(respondentMessage.getSenderId()).isEqualTo(respondent.getId()),
                () -> assertThat(respondentMessage.getSenderPlatformId()).isEqualTo(respondent.getPlatformId()),
                () -> assertThat(respondentMessage.getHandle()).isEqualTo(handle),
                () -> assertThat(respondentMessage.getType()).isEqualByComparingTo(RESPONDENTS),
                () -> assertThat(respondentMessage.getMessageType()).isEqualByComparingTo(REGULAR),
                () -> assertThat(respondentMessage.getMessage()).isEqualTo(question),
                () -> assertThat(respondentMessage.getSenderName()).isEqualTo(respondent.getDisplayName()),
                () -> assertThat(respondentMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS)),

                () -> assertThat(moderatorMessage.getId()).isNotNull(),
                () -> assertThat(moderatorMessage.getSenderId()).isEqualTo(moderator.getId()),
                () -> assertThat(moderatorMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId()),
                () -> assertThat(moderatorMessage.getHandle()).isEqualTo(handle),
                () -> assertThat(moderatorMessage.getType()).isEqualByComparingTo(RESPONDENTS),
                () -> assertThat(moderatorMessage.getMessageType()).isEqualByComparingTo(REGULAR),
                () -> assertThat(moderatorMessage.getMessage()).isEqualTo(answer),
                () -> assertThat(moderatorMessage.getSenderName()).isEqualTo(moderator.getDisplayName()),
                () -> assertThat(moderatorMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS))
        );
    }

    @Test
    @DisplayName("Forbidden for non moderator backroom participants")
    void respondentsChatData_forbiddenExceptModerator() throws Exception {
        var question = make();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        sendMeetingRoomRespondentsChatMessage(respondent, question);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(FORBIDDEN.value());
    }

    @Test
    @DisplayName("OK for moderator")
    void respondentsChatData_respondentSender() throws Exception {
        var question = make();
        var answer = make();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var handle = researchSession.getId();

        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_ENABLE_URL))
            .then()
            .statusCode(HttpStatus.OK.value());

        sendMeetingRoomRespondentsChatMessage(respondent, question);
        sendMeetingRoomRespondentsChatMessage(moderator, answer);

        List<ChatMessageDto> chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages.size()", is(2))
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);
        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages.size()", is(2))
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        ChatMessageDto respondentChatMessage = chatMessages.get(0);
        ChatMessageDto moderatorChatMessage = chatMessages.get(1);
        assertAll(
                () -> assertThat(respondentChatMessage.getId()).isNotNull(),
                () -> assertThat(respondentChatMessage.getSenderId()).isEqualTo(respondent.getId()),
                () -> assertThat(respondentChatMessage.getHandle()).isEqualTo(handle),
                () -> assertThat(respondentChatMessage.getType()).isEqualByComparingTo(RESPONDENTS),
                () -> assertThat(respondentChatMessage.getMessageType()).isEqualByComparingTo(REGULAR),
                () -> assertThat(respondentChatMessage.getMessage()).isEqualTo(question),
                () -> assertThat(respondentChatMessage.getSenderName()).isEqualTo(respondent.getDisplayName()),
                () -> assertThat(respondentChatMessage.getSentAt())
                        .isCloseTo(now(), within(2, ChronoUnit.SECONDS)),

                () -> assertThat(moderatorChatMessage.getId()).isNotNull(),
                () -> assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId()),
                () -> assertThat(moderatorChatMessage.getHandle()).isEqualTo(handle),
                () -> assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(RESPONDENTS),
                () -> assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR),
                () -> assertThat(moderatorChatMessage.getMessage()).isEqualTo(answer),
                () -> assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName()),
                () -> assertThat(moderatorChatMessage.getSentAt())
                        .isCloseTo(now(), within(2, ChronoUnit.SECONDS))
        );

        sendMeetingRoomRespondentsChatMessageSeen(moderator, respondentChatMessage.getId());
        sendMeetingRoomRespondentsChatMessageSeen(respondent, moderatorChatMessage.getId());
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(0))
                .body("latestSeenMessageId", equalTo(respondentChatMessage.getId().toString()))
                .body("messages.size()", is(2))
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);
        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(0))
                .body("latestSeenMessageId", equalTo(moderatorChatMessage.getId().toString()))
                .body("messages.size()", is(2))
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);
    }

}
