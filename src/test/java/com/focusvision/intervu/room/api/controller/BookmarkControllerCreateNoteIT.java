package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.common.model.BookmarkType.NOTE;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.BOOKMARK;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils;
import com.focusvision.intervu.room.api.bookmark.api.BookmarkController;
import com.focusvision.intervu.room.api.bookmark.model.BookmarkDetailsDto;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link BookmarkController#create}.
 */
@DisplayName("Bookmarks creation - NOTE")
class BookmarkControllerCreateNoteIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .body(addBookmarkDto())
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for not started room.")
    void roomNotStarted() {
        given()
                .when()
                .headers(roomParticipantHeadersWithContent(getModerator(setUpFullRoom())))
                .body(addBookmarkDto())
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(BAD_REQUEST.value())
                .body("message", containsString("Room not running."));
    }

    @Test
    @DisplayName("Bad request, invalid data")
    void invalidData() {
        var moderator = getModerator(setUpFullRoom());

        // note too long
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addBookmarkDto(make(BOOKMARK_NOTE_MAX_LENGTH + 1)))
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(BAD_REQUEST.value());

        // note null
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addBookmarkDto(null))
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(BAD_REQUEST.value());

        // note empty
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addBookmarkDto(""))
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(BAD_REQUEST.value());

        // timestamp null
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addBookmarkDto(randomBookmarkNote(), null))
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(BAD_REQUEST.value());
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class, mode = EXCLUDE,
        names = {"MODERATOR", "OBSERVER", "TRANSLATOR"})
    @DisplayName("Forbidden for all roles except moderator, observer and translator participants")
    void forbiddenForAllExceptModeratorObserverAndTranslator(ParticipantRole role) {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        given()
                .headers(roomParticipantHeadersWithContent(getParticipant(researchSession, role)))
                .body(addBookmarkDto())
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .statusCode(FORBIDDEN.value());
    }

    @Test
    @DisplayName("Create bookmark - ok")
    void ok() throws ExecutionException, InterruptedException, TimeoutException {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var moderatorChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        connectAllToRoom(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var moderatorSession = connectRoomWs(moderator);
        moderatorSession.subscribe(
                getChatChannel(moderator.getId()),
                new WsTestUtils.OutgoingChatMessageStompFrameHandler(moderatorChatResultKeeper::complete));

        var beforeBookmarkCreation = now();
        var addBookmarkDto = addBookmarkDto();

        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addBookmarkDto)
                .when()
                .post(url(ROOM_BOOKMARKS_URL))
                .then()
                .and().statusCode(CREATED.value())
                .body("size()", is(7))
                .body("id", notNullValue())
                .body("note", equalTo(addBookmarkDto.getNote()))
                .body("addedBy", equalTo(moderator.getPlatformId()))
                .body("addedAt", greaterThan(beforeBookmarkCreation.toString()))
                .body("addedByDisplayName", equalTo(moderator.getDisplayName()))
                .body("offset", equalTo(
                        roomOffsetProvider.calculateOffset(researchSession.getId(), addBookmarkDto.getTimestamp())))
                .body("type", equalTo(NOTE.toString()))
                .extract()
                .as(BookmarkDetailsDto.class);

        assertThat(moderatorChatResultKeeper.get(500, MILLISECONDS).getMessageType()).isEqualTo(BOOKMARK);
    }

    @Test
    @DisplayName("OK for admin")
    void okForAdmin() {
        var researchSession = setUpFullRoom();
        var admin = getAdmin(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(admin))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());
    }

    @Test
    @DisplayName("OK for PM")
    void okForPM() {
        var researchSession = setUpFullRoom();
        var manager = getProjectManager(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(manager))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());
    }

    @Test
    @DisplayName("OK for IPO")
    void okForIPO() {
        var researchSession = setUpFullRoom();
        var operator = getProjectOperator(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(operator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());
    }

}
