package com.focusvision.intervu.room.api.event;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.model.messaging.ChatMessageData;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionChatMessagesData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link ResearchSessionChatMessagesData} events.
 */
@DisplayName("Chat messages data for control panel")
class SendChatMessagesToControlPanelEventIT extends HttpBasedIT {

    @Test
    @DisplayName("Room chat messages sent on finish")
    void chatMessagesDataSent() throws Exception {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var consumer = researchSessionChatMessagesDataConsumer(1);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        String message1 = make();
        String message2 = make();
        String message3 = make();
        sendBackRoomInternalChatMessage(moderator, message1);
        sendBackRoomInternalChatMessage(moderator, message2);
        sendBackRoomInternalChatMessage(moderator, message3);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_FINISH_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        assertThat(consumer.allMessagesReceived(2, TimeUnit.SECONDS)).isTrue();
        var message = consumer.getMessage(researchSession.getPlatformId()).orElseThrow();
        consumer.stop();

        var senderPlatformId = moderator.getPlatformId();
        var senderDisplayName = moderator.getDisplayName();

        assertAll(
                () -> assertThat(message.getMessages()).extracting(ChatMessageData::getMessage)
                        .containsExactly(message1, message2, message3),
                () -> assertThat(message.getMessages()).extracting(ChatMessageData::getSenderPlatformId)
                        .containsExactly(senderPlatformId, senderPlatformId, senderPlatformId),
                () -> assertThat(message.getMessages()).extracting(ChatMessageData::getSenderDisplayName)
                        .containsExactly(senderDisplayName, senderDisplayName, senderDisplayName)
        );
    }
}
