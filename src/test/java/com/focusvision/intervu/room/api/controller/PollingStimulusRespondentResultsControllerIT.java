package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.StimulusState;
import com.focusvision.intervu.room.api.state.repository.RoomStateLogRepository;
import com.focusvision.intervu.room.api.state.stimulus.repository.StimulusStateRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static io.restassured.RestAssured.given;
import static java.util.UUID.randomUUID;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link PollStimulusController#results(String, AuthenticatedParticipant)} (String, AuthenticatedParticipant)}.
 */
@DisplayName("Polling stimulus results")
class PollingStimulusRespondentResultsControllerIT extends HttpBasedIT {

    @Autowired
    public StimulusStateRepository stimulusStateRepository;
    @Autowired
    public RoomStateLogRepository roomStateLogRepository;

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(POLL_STIMULUS_RESULTS), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned moderators")
    void unauthorizedBannedModerator() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        participantRepository.save(respondent.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(POLL_STIMULUS_RESULTS), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Results - ok")
    void results_ok() {
        var researchSession = setUpFullRoom();
        var respondent1 = getRespondent(researchSession);
        var stimulus = researchSession.getStimuli().get(0);
        var stimulusState = StimulusState.builder()
                .active(true)
                .broadcastResults(true)
                .id(stimulus.getId()).build();
        var roomState = RoomState.builder().roomId(researchSession.getId()).stimuli(List.of(stimulusState)).build();
        var respondent2Id = addUserToRoom(researchSession, RESPONDENT);
        var respondent1State = stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent1.getId()));
        var respondent2State = stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent2Id));
        roomStateLogRepository.save(roomStateLog(roomState));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent1))
                .get(url(POLL_STIMULUS_RESULTS), stimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("results", contains(respondent1State.getContent(), respondent2State.getContent()))
                .body("stimulusId", equalTo(stimulus.getId().toString()));
    }

    @Test
    @DisplayName("Results - broadcast disabled")
    void results_broadcastDisabled() {
        var researchSession = setUpFullRoom();
        var respondent1 = getRespondent(researchSession);
        var stimulus = researchSession.getStimuli().get(0);
        var stimulusState = StimulusState.builder().broadcastResults(false).id(stimulus.getId()).build();
        var roomState = RoomState.builder().roomId(researchSession.getId()).stimuli(List.of(stimulusState)).build();
        var respondent2Id = addUserToRoom(researchSession, RESPONDENT);
        stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent1.getId()));
        stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent2Id));
        roomStateLogRepository.save(roomStateLog(roomState));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent1))
                .get(url(POLL_STIMULUS_RESULTS), stimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("results", empty())
                .body("stimulusId", equalTo(stimulus.getId().toString()));
    }

    @Test
    @DisplayName("Results - stimulus not found or not currently active in room")
    void results_stimulusNotFoundOrNotActive() {
        var researchSession = setUpFullRoom();
        var respondent1 = getRespondent(researchSession);
        var stimulus = researchSession.getStimuli().get(0);
        var stimulusState = StimulusState.builder().broadcastResults(true).id(stimulus.getId()).build();
        var roomState = RoomState.builder().roomId(researchSession.getId()).stimuli(List.of(stimulusState)).build();
        var respondent2Id = addUserToRoom(researchSession, RESPONDENT);
        stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent1.getId()));
        stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent2Id));
        roomStateLogRepository.save(roomStateLog(roomState));
        var researchSession2 = setUpFullRoom();
        var stimulus2 = researchSession2.getStimuli().get(0);

        var id = randomUUID();
        given()
                .when()
                .headers(roomParticipantHeaders(respondent1))
                .get(url(POLL_STIMULUS_RESULTS), id)
                .then()
                .statusCode(OK.value())
                .body("results", empty())
                .body("stimulusId", equalTo(id.toString()));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent1))
                .get(url(POLL_STIMULUS_RESULTS), stimulus2.getId())
                .then()
                .statusCode(OK.value())
                .body("results", empty())
                .body("stimulusId", equalTo(stimulus2.getId().toString()));

        roomState.setStimuli(Collections.emptyList());
        roomStateLogRepository.save(roomStateLog(roomState));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent1))
                .get(url(POLL_STIMULUS_RESULTS), stimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("results", empty())
                .body("stimulusId", equalTo(stimulus.getId().toString()));
    }

}
