package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.model.messaging.BannedParticipantData;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class BannedParticipantDataConsumer {
    private SimpleMessageListenerContainer container;
    private CountDownLatch latch;
    private List<BannedParticipantData> messages = new ArrayList<>();

    public BannedParticipantDataConsumer(final ConnectionFactory connectionFactory,
                                               MessageConverter messageConverter,
                                               String queueName,
                                               int messageCount) {
        var adapter = new MessageListenerAdapter(this, "receiveMessage");
        adapter.setMessageConverter(messageConverter);

        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setupMessageListener(adapter);

        latch = new CountDownLatch(messageCount);
    }

    public void receiveMessage(BannedParticipantData message) {
        messages.add(message);
        latch.countDown();
    }

    public void stop() {
        container.stop();
    }

    public BannedParticipantDataConsumer start() {
        container.start();

        return this;
    }

    public boolean allMessagesReceived(long timeout, TimeUnit seconds) throws InterruptedException {
        return latch.await(timeout, seconds);
    }

    public Optional<BannedParticipantData> getMessage(String platformId) {
        return messages.stream()
                .filter(messageData -> messageData.getPlatformId().equals(platformId))
                .findFirst();
    }
}
