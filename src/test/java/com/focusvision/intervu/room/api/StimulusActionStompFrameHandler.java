package com.focusvision.intervu.room.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.draw.model.DrawingNotificationMessage;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionNotificationMessage;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@RequiredArgsConstructor
public class StimulusActionStompFrameHandler implements StompFrameHandler {

    private final ObjectMapper mapper;
    private final List<StimulusActionNotificationMessage> messages = new ArrayList<>();

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return StimulusActionNotificationMessage.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info("received message: {} with headers: {}", payload, headers);

        StimulusActionNotificationMessage dto = mapper.convertValue(payload, StimulusActionNotificationMessage.class);
        messages.add(dto);
    }
}
