package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.CANCELED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MICROS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link DashboardController#rooms}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Dashboard rooms")
class DashboardControllerRoomsIT extends HttpBasedIT {

  @Test
  @DisplayName("Forbidden for unauthorized")
  void forbiddenUnauthorized() {
    given()
        .when()
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Forbidden for room participants")
  void forbiddenRoomParticipants() {
    given()
        .when()
        .headers(roomParticipantHeaders(getRespondent(setUpFullRoom())))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Upcoming sessions fetched")
  void ok() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(1))
        .body("[0].size()", is(3))
        .body("[0].roomState.size()", is(17))
        .body("[0].roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("[0].roomState.version", notNullValue())
        .body("[0].roomState.roomName", equalTo(researchSession.getName()))
        .body("[0].roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("[0].roomState.state", equalTo(researchSession.getState().name()))
        .body("[0].roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("[0].roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("[0].roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("[0].roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("[0].roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("[0].roomState.participants.size()", is(0))
        .body("[0].roomState.activeStimulus", nullValue())
        .body("[0].roomState.stimulusInFocus", equalTo(false))
        .body("[0].roomState.drawingInSync", equalTo(false))
        .body("[0].roomState.respondentsChatEnabled", equalTo(false))
        .body("[0].roomState.projectType", equalTo(researchSession.getProject().getType().name()))
        .body("[0].roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("[0].participantState.size()", is(16))
        .body("[0].participantState.admin", equalTo(moderator.isAdmin()))
        .body("[0].participantState.moderator", equalTo(moderator.isModerator()))
        .body("[0].participantState.banable", equalTo(moderator.isBanable()))
        .body("[0].participantState.guest", equalTo(moderator.isGuest()))
        .body("[0].participantState.projectManager", equalTo(moderator.isProjectManager()))
        .body("[0].participantState.projectOperator", equalTo(moderator.isProjectOperator()))
        .body("[0].participantState.participantId", equalTo(moderator.getId().toString()))
        .body("[0].participantState.participantPlatformId", equalTo(moderator.getPlatformId()))
        .body("[0].participantState.displayName", equalTo(moderator.getDisplayName()))
        .body("[0].participantState.role", equalTo(moderator.getRole().name()))
        .body("[0].participantState.ready", equalTo(false))
        .body("[0].participantState.banned", equalTo(moderator.isBanned()))
        .body("[0].participantState.drawingEnabled", equalTo(false))
        .body("[0].participantState.conference", nullValue())
        .body("[0].participantState.channel.room",
            equalTo("/topic/room." + researchSession.getId()))
        .body("[0].participantState.channel.participant",
            equalTo("/topic/participant." + moderator.getId().toString()))
        .body("[0].dialInInfo", nullValue());
  }

  @Test
  @DisplayName("Next day session")
  void nextDaySessions() {
    var tomorrowSessionStart = now().plus(1, DAYS).truncatedTo(MICROS);
    var tomorrowSessionEnd = LocalDate.now(ZoneId.of("UTC"))
        .atTime(LocalTime.MAX)
        .atZone(ZoneId.of("UTC"))
        .truncatedTo(SECONDS)
        .toInstant();

    var researchSessionData = sendCreateRoomEvent();
    researchSessionData
        .setVersion(2)
        .setStartsAt(tomorrowSessionStart)
        .setEndsAt(tomorrowSessionEnd);
    sendUpdateRoomEvent(researchSessionData);
    var tomorrowScheduledSession = researchSessionRepository.findByPlatformId(
        researchSessionData.getId()).orElseThrow();
    var moderator = getModerator(tomorrowScheduledSession);

    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(1))
        .body("[0].size()", is(3))
        .body("[0].roomState.size()", is(17))
        .body("[0].roomState.roomId", equalTo(tomorrowScheduledSession.getId().toString()))
        .body("[0].roomState.version", notNullValue())
        .body("[0].roomState.roomName", equalTo(tomorrowScheduledSession.getName()))
        .body("[0].roomState.projectName", equalTo(tomorrowScheduledSession.getProject().getName()))
        .body("[0].roomState.state", equalTo(tomorrowScheduledSession.getState().name()))
        .body("[0].roomState.startsAt", equalTo(tomorrowScheduledSession.getStartsAt().toString()))
        .body("[0].roomState.endsAt", equalTo(tomorrowScheduledSession.getEndsAt().toString()))
        .body("[0].roomState.useWebcams", equalTo(tomorrowScheduledSession.isUseWebcams()))
        .body("[0].roomState.privacy", equalTo(tomorrowScheduledSession.isPrivacy()))
        .body("[0].roomState.projectNumber",
            equalTo(tomorrowScheduledSession.getProject().getProjectNumber()))
        .body("[0].roomState.participants.size()", is(0))
        .body("[0].roomState.activeStimulus", nullValue())
        .body("[0].roomState.stimulusInFocus", equalTo(false))
        .body("[0].roomState.drawingInSync", equalTo(false))
        .body("[0].roomState.respondentsChatEnabled", equalTo(false))
        .body("[0].roomState.projectType",
            equalTo(tomorrowScheduledSession.getProject().getType().name()))
        .body("[0].roomState.audioChannel",
            equalTo(tomorrowScheduledSession.getAudioChannel().name()))
        .body("[0].participantState.size()", is(16))
        .body("[0].participantState.admin", equalTo(moderator.isAdmin()))
        .body("[0].participantState.moderator", equalTo(moderator.isModerator()))
        .body("[0].participantState.banable", equalTo(moderator.isBanable()))
        .body("[0].participantState.guest", equalTo(moderator.isGuest()))
        .body("[0].participantState.projectManager", equalTo(moderator.isProjectManager()))
        .body("[0].participantState.projectOperator", equalTo(moderator.isProjectOperator()))
        .body("[0].participantState.participantId", equalTo(moderator.getId().toString()))
        .body("[0].participantState.participantPlatformId", equalTo(moderator.getPlatformId()))
        .body("[0].participantState.displayName", equalTo(moderator.getDisplayName()))
        .body("[0].participantState.role", equalTo(moderator.getRole().name()))
        .body("[0].participantState.ready", equalTo(false))
        .body("[0].participantState.banned", equalTo(moderator.isBanned()))
        .body("[0].participantState.drawingEnabled", equalTo(false))
        .body("[0].participantState.conference", nullValue())
        .body("[0].participantState.channel.room",
            equalTo("/topic/room." + tomorrowScheduledSession.getId()))
        .body("[0].participantState.channel.participant",
            equalTo("/topic/participant." + moderator.getId().toString()))
        .body("[0].dialInInfo", nullValue());
  }

  @Test
  @DisplayName("Not returned if participant is banned")
  void notReturnedBanned() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .delete(url(PARTICIPANT_URL), moderator.getId().toString())
        .then()
        .statusCode(NO_CONTENT.value());

    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(0));
  }

  @Test
  @DisplayName("Canceled session not returned")
  void canceledNotReturned() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(1))
        .body("[0].size()", is(3))
        .body("[0].roomState.size()", is(17))
        .body("[0].roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("[0].roomState.version", notNullValue())
        .body("[0].roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("[0].roomState.roomName", equalTo(researchSession.getName()))
        .body("[0].roomState.state", equalTo(researchSession.getState().name()))
        .body("[0].roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("[0].roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("[0].roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("[0].roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("[0].roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("[0].roomState.participants.size()", is(0))
        .body("[0].roomState.activeStimulus", nullValue())
        .body("[0].roomState.stimulusInFocus", equalTo(false))
        .body("[0].roomState.drawingInSync", equalTo(false))
        .body("[0].roomState.respondentsChatEnabled", equalTo(false))
        .body("[0].roomState.projectType", equalTo(researchSession.getProject().getType().name()))
        .body("[0].roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("[0].participantState.size()", is(16))
        .body("[0].participantState.admin", equalTo(moderator.isAdmin()))
        .body("[0].participantState.moderator", equalTo(moderator.isModerator()))
        .body("[0].participantState.banable", equalTo(moderator.isBanable()))
        .body("[0].participantState.guest", equalTo(moderator.isGuest()))
        .body("[0].participantState.projectManager", equalTo(moderator.isProjectManager()))
        .body("[0].participantState.projectOperator", equalTo(moderator.isProjectOperator()))
        .body("[0].participantState.drawingEnabled", equalTo(false))
        .body("[0].participantState.role", equalTo(moderator.getRole().name()))
        .body("[0].participantState.channel.room",
            equalTo("/topic/room." + researchSession.getId()))
        .body("[0].dialInInfo", nullValue());

    researchSessionRepository.save(researchSession.setState(CANCELED));
    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(0));
  }

  @Test
  @DisplayName("Finished session not returned")
  void finishedNotReturned() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(1))
        .body("[0].size()", is(3))
        .body("[0].roomState.size()", is(17))
        .body("[0].roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("[0].roomState.version", notNullValue())
        .body("[0].roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("[0].roomState.roomName", equalTo(researchSession.getName()))
        .body("[0].roomState.state", equalTo(researchSession.getState().name()))
        .body("[0].roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("[0].roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("[0].roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("[0].roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("[0].roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("[0].roomState.participants.size()", is(0))
        .body("[0].roomState.activeStimulus", nullValue())
        .body("[0].roomState.stimulusInFocus", equalTo(false))
        .body("[0].roomState.drawingInSync", equalTo(false))
        .body("[0].roomState.projectType", equalTo(researchSession.getProject().getType().name()))
        .body("[0].roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("[0].participantState.size()", is(16))
        .body("[0].roomState.respondentsChatEnabled", equalTo(false))
        .body("[0].participantState.admin", equalTo(moderator.isAdmin()))
        .body("[0].participantState.moderator", equalTo(moderator.isModerator()))
        .body("[0].participantState.banable", equalTo(moderator.isBanable()))
        .body("[0].participantState.guest", equalTo(moderator.isGuest()))
        .body("[0].participantState.projectManager", equalTo(moderator.isProjectManager()))
        .body("[0].participantState.projectOperator", equalTo(moderator.isProjectOperator()))
        .body("[0].participantState.drawingEnabled", equalTo(false))
        .body("[0].participantState.role", equalTo(moderator.getRole().name()))
        .body("[0].participantState.channel.room",
            equalTo("/topic/room." + researchSession.getId()))
        .body("[0].dialInInfo", nullValue());

    researchSessionRepository.save(researchSession.setState(FINISHED));
    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(0));
  }

  @Test
  @DisplayName("In progress session returned")
  void inProgressReturned() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    connectAllToRoom(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(DASHBOARD_UPCOMING_SESSIONS_URL))
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("size()", is(1))
        .body("[0].size()", is(3))
        .body("[0].roomState.size()", is(17))
        .body("[0].roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("[0].roomState.version", notNullValue())
        .body("[0].roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("[0].roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("[0].roomState.roomName", equalTo(researchSession.getName()))
        .body("[0].roomState.state", equalTo(ResearchSessionState.IN_PROGRESS.name()))
        .body("[0].roomState.activeStimulus", nullValue())
        .body("[0].roomState.stimulusInFocus", equalTo(false))
        .body("[0].roomState.drawingInSync", equalTo(false))
        .body("[0].roomState.respondentsChatEnabled", equalTo(false))
        .body("[0].roomState.projectType", equalTo(researchSession.getProject().getType().name()))
        .body("[0].roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("[0].participantState.size()", is(16))
        .body("[0].participantState.admin", equalTo(moderator.isAdmin()))
        .body("[0].participantState.moderator", equalTo(moderator.isModerator()))
        .body("[0].participantState.banable", equalTo(moderator.isBanable()))
        .body("[0].participantState.guest", equalTo(moderator.isGuest()))
        .body("[0].participantState.projectManager", equalTo(moderator.isProjectManager()))
        .body("[0].participantState.projectOperator", equalTo(moderator.isProjectOperator()))
        .body("[0].participantState.drawingEnabled", equalTo(false))
        .body("[0].participantState.role", equalTo(moderator.getRole().name()))
        .body("[0].participantState.channel.room",
            equalTo("/topic/room." + researchSession.getId()))
        .body("[0].roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("[0].roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("[0].roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("[0].roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("[0].roomState.participants[0].size()", is(12))
        .body("[0].dialInInfo", nullValue());
  }
}
