package com.focusvision.intervu.room.api.export.api;

import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.common.model.ChatType.RESPONDENTS;
import static com.focusvision.intervu.room.api.common.model.RecordingType.VIDEO;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.ChatSource.MEETING_ROOM;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.ChatSource.WAITING_ROOM;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.BOOKMARK;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.REGULAR;
import static io.restassured.RestAssured.given;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.adapter.RecordingCheckSenderAdapter;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.focusvision.intervu.room.api.service.StreamingService;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

/**
 * Integration tests for {@link ParticipantExportController}.
 */
class ParticipantExportControllerIT extends HttpBasedIT {

  @Autowired
  private RecordingCheckSenderAdapter adapter;
  @SpyBean
  private StreamingService streamingService;

  @Test
  @DisplayName("Respondent info - unauthorized")
  void respondentInfo_unauthorized() {
    given()
        .when()
        .get(url(PARTICIPANT_EXPORT_URL), make())
        .then()
        .statusCode(UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Respondent info - forbidden for participants")
  void respondentInfo_forbiddenForParticipants(ParticipantRole role) {
    var room = setUpFullRoom();
    var caller = getParticipant(room, role);

    given()
        .when()
        .headers(roomParticipantHeaders(caller))
        .get(url(PARTICIPANT_EXPORT_URL), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Respondent info - forbidden for admin")
  void respondentInfo_forbiddenForAdmin() {
    var room = setUpFullRoom();
    var admin = getAdmin(room);

    given()
        .when()
        .headers(intervuUserHeaders(admin))
        .get(url(PARTICIPANT_EXPORT_URL), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Respondent info - research sessions not found")
  void respondentInfo_researchSessionsNotFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(PARTICIPANT_EXPORT_URL), make())
        .then()
        .statusCode(OK.value())
        .body("$", empty());
  }

  @Test
  @DisplayName("Respondent info - ok, system user")
  void respondentInfo_okSystemUser() throws InterruptedException {
    var room = setUpFullRoom();
    var respondent = getRespondent(room);
    var moderator = getModerator(room);
    var chatMessage = new ChatMessage()
        .setMessage(make())
        .setHandle(room.getId())
        .setSenderId(respondent.getId())
        .setSenderPlatformId(respondent.getPlatformId())
        .setSenderDisplayName(respondent.getDisplayName())
        .setSource(WAITING_ROOM)
        .setSentAt(Instant.now())
        .setType(RESPONDENTS)
        .setMessageType(REGULAR)
        .setResearchSessionId(room.getId());
    var bookmarkChatMessage = new ChatMessage()
        .setMessage(make())
        .setHandle(room.getId())
        .setSenderId(respondent.getId())
        .setSenderPlatformId(respondent.getPlatformId())
        .setSenderDisplayName(respondent.getDisplayName())
        .setSource(MEETING_ROOM)
        .setSentAt(Instant.now())
        .setType(INTERNAL)
        .setMessageType(BOOKMARK)
        .setResearchSessionId(room.getId());
    chatMessageRepository.save(chatMessage);
    chatMessageRepository.save(bookmarkChatMessage);

    var consumer = researchSessionStateDataConsumer(3);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    var finalSession = researchSessionRepository.getById(room.getId());
    var conference = finalSession.getConference();


    when(streamingService.getParticipantRecordings(conference.getRoomSid(), respondent.getId()))
        .thenReturn(List.of(new ParticipantGroupRoomRecording()
            .setSid("recording-sid")
            .setDuration(100)
            .setParticipantId(respondent.getId().toString())
            .setParticipantType(ParticipantRole.RESPONDENT.name())
            .setRecordingType(VIDEO)));

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());

    var recording = conferenceService.get(conference.getId()).getRecordings().get(0);
    var processingConferenceCheck = new ProcessingRecordingCheck()
        .setId(recording.getId().toString());

    adapter.send(processingConferenceCheck);

    consumer.allMessagesReceived(1, TimeUnit.SECONDS);
    consumer.stop();

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(PARTICIPANT_EXPORT_URL), respondent.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("[0].chatMessages.size()", equalTo(1))
        .body("[0].sessionId", equalTo(room.getPlatformId()))
        .body("[0].chatMessages[0].message", equalTo(chatMessage.getMessage()))
        .body("[0].chatMessages[0].senderId", equalTo(respondent.getPlatformId()))
        .body("[0].chatMessages[0].senderName", equalTo(respondent.getDisplayName()))
        .body("[0].chatMessages[0].handle", equalTo(room.getId().toString()))
        .body("[0].chatMessages[0].type", equalTo(RESPONDENTS.name()))
        .body("[0].chatMessages[0].sentAt", equalTo(formatInstant(chatMessage.getSentAt())))
        .body("[0].roomRecordings.size()", equalTo(1))
        .body("[0].roomRecordings[0].sid", equalTo("recording-sid"))
        .body("[0].roomRecordings[0].type", equalTo(VIDEO.name()));
  }
}
