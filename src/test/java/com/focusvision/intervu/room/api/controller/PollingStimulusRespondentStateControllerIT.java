package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link PollStimulusController#respondentState(String, AuthenticatedParticipant)}.
 */
@DisplayName("Polling stimulus respondent state")
class PollingStimulusRespondentStateControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned moderators")
    void unauthorizedBannedModerator() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        participantRepository.save(respondent.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Stimuli state empty - ok for respondent")
    void stimuliBarData_okForBackroomUsersModerator() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var stimulus = researchSession.getStimuli().get(0);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), stimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", nullValue())
                .body("stimulusId", equalTo(stimulus.getId().toString()));
    }

    @Test
    @DisplayName("Stimuli state empty - ok for respondent, with content")
    void stimuliBarData_okForBackroomUsersRespondentWithContent() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);
        var pollingStimulus = getPollStimulus(researchSession);
        var answerContent1 = make();
        var answerContent2 = make();
        var respondentSession = connectRoomWs(respondent);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var action1 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(answerContent1)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, action1);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(answerContent1))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()));

        var action2 = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(answerContent2)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, action2);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(answerContent2))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()));
    }

}
