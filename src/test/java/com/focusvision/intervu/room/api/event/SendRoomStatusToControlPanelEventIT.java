package com.focusvision.intervu.room.api.event;

import static com.focusvision.intervu.room.api.common.model.AudioChannel.NATIVE;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;
import static com.focusvision.intervu.room.api.model.CompositionLayout.GRID;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.TestStreamingServiceImpl;
import com.focusvision.intervu.room.api.adapter.RecordingCheckSenderAdapter;
import com.focusvision.intervu.room.api.model.messaging.ProcessPendingRecordingRequest;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionStateData;
import com.focusvision.intervu.room.api.recording.infra.ProcessPendingRecordingRequestPublisher;
import com.focusvision.intervu.room.api.repository.ConferenceRepository;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * IT tests for {@link ResearchSessionStateData} events.
 *
 * @author Branko Ostojic
 */
@DisplayName("Room status updates for control panel")
class SendRoomStatusToControlPanelEventIT extends HttpBasedIT {

  @Autowired
  private RecordingCheckSenderAdapter adapter;

  @Autowired
  private ConferenceRepository conferenceRepository;

  @Autowired
  private ProcessPendingRecordingRequestPublisher processPendingRecordingRequestPublisher;

  @Autowired
  private TestStreamingServiceImpl testStreamingService;

  @Test
  @DisplayName("Room status updates sent on start, finish and processing finish")
  void statusUpdatesSent() throws InterruptedException {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
    var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());
    var handler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);

    var consumer = researchSessionStateDataConsumer(3);
    var session = connectRoomWs(moderator);
    subscribe(session, roomChannel, handler);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    session.disconnect();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());

    waitForWs();

    var conference = researchSessionRepository.getById(researchSession.getId()).getConference();
    var recording = conference.getRecordings().get(0);
    processPendingRecordingRequestPublisher.publish(new ProcessPendingRecordingRequest()
        .setRecordingId(recording.getId().toString()));
    waitForWs();

    var processingRecordingCheck = new ProcessingRecordingCheck()
        .setId(recording.getId().toString());
    adapter.send(processingRecordingCheck);

    consumer.allMessagesReceived(5, TimeUnit.SECONDS);

    var startedMessage = consumer.getMessage(0).orElseThrow();
    var finishedMessage = consumer.getMessage(1).orElseThrow();
    var recordingFinishedMessage = consumer.getMessage(2).orElseThrow();
    consumer.stop();

    conference = researchSessionRepository.getById(researchSession.getId()).getConference();

    assertThat(startedMessage.getId()).isEqualTo(researchSession.getPlatformId());
    assertThat(startedMessage.getState()).isEqualByComparingTo(IN_PROGRESS);
    assertThat(startedMessage.getConferenceId()).isEqualTo(conference.getRoomSid());
    assertThat(startedMessage.getRecordingId()).isNullOrEmpty();
    assertThat(startedMessage.getStartedAt()).isNotNull();
    assertThat(startedMessage.getEndedAt()).isNull();
    assertThat(startedMessage.isError()).isFalse();

    assertThat(finishedMessage.getId()).isEqualTo(researchSession.getPlatformId());
    assertThat(finishedMessage.getState()).isEqualByComparingTo(FINISHED);
    assertThat(finishedMessage.getConferenceId()).isEqualTo(conference.getRoomSid());
    assertThat(finishedMessage.getRecordingId()).isNullOrEmpty();
    assertThat(finishedMessage.getStartedAt()).isNotNull();
    assertThat(finishedMessage.getEndedAt()).isNotNull();
    assertThat(finishedMessage.isError()).isFalse();

    assertThat(recordingFinishedMessage.getId()).isEqualTo(researchSession.getPlatformId());
    assertThat(recordingFinishedMessage.getState()).isEqualByComparingTo(FINISHED);
    assertThat(recordingFinishedMessage.getConferenceId()).isEqualTo(conference.getRoomSid());
    assertThat(recordingFinishedMessage.getRecordingId())
        .isEqualTo(conference.getRecordings().get(0).getRecordingSid());
    assertThat(recordingFinishedMessage.getStartedAt()).isNotNull();
    assertThat(recordingFinishedMessage.getEndedAt()).isNotNull();
    assertThat(recordingFinishedMessage.getCompositionLayout()).isEqualTo(GRID);
    assertThat(recordingFinishedMessage.getAudioChannel()).isEqualTo(NATIVE);
    assertThat(recordingFinishedMessage.isError()).isFalse();
  }

  @Test
  @DisplayName("Room status updates sent on start, finish and processing error")
  void statusUpdatesSent_error() throws InterruptedException, ExecutionException {
    testStreamingService.setRecordingError(true);
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
    var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());
    var handler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
    var consumer = researchSessionStateDataConsumer(3);
    var session = connectRoomWs(moderator);
    subscribe(session, roomChannel, handler);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    session.disconnect();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());

    waitForWs();

    var conference = conferenceRepository
        .save(researchSessionRepository.getById(researchSession.getId()).getConference());
    var recording = conference.getRecordings().get(0);

    processPendingRecordingRequestPublisher.publish(new ProcessPendingRecordingRequest()
        .setRecordingId(recording.getId().toString()));
    waitForWs();

    var processingRecordingCheck = new ProcessingRecordingCheck()
        .setId(recording.getId().toString());

    adapter.send(processingRecordingCheck);

    consumer.allMessagesReceived(5, TimeUnit.SECONDS);

    var startedMessage = consumer.getMessage(0).orElseThrow();
    var finishedMessage = consumer.getMessage(1).orElseThrow();
    var recordingErrorMessage = consumer.getMessage(2).orElseThrow();
    consumer.stop();

    conference = researchSessionRepository.getById(researchSession.getId()).getConference();

    assertThat(startedMessage.getId()).isEqualTo(researchSession.getPlatformId());
    assertThat(startedMessage.getState()).isEqualByComparingTo(IN_PROGRESS);
    assertThat(startedMessage.getConferenceId()).isEqualTo(conference.getRoomSid());
    assertThat(startedMessage.getRecordingId()).isNullOrEmpty();
    assertThat(startedMessage.getStartedAt()).isNotNull();
    assertThat(startedMessage.getEndedAt()).isNull();
    assertThat(startedMessage.isError()).isFalse();

    assertThat(finishedMessage.getId()).isEqualTo(researchSession.getPlatformId());
    assertThat(finishedMessage.getState()).isEqualByComparingTo(FINISHED);
    assertThat(finishedMessage.getConferenceId()).isEqualTo(conference.getRoomSid());
    assertThat(finishedMessage.getRecordingId()).isNullOrEmpty();
    assertThat(finishedMessage.getStartedAt()).isNotNull();
    assertThat(finishedMessage.getEndedAt()).isNotNull();
    assertThat(finishedMessage.isError()).isFalse();

    assertThat(recordingErrorMessage.getId()).isEqualTo(researchSession.getPlatformId());
    assertThat(recordingErrorMessage.getState()).isEqualByComparingTo(FINISHED);
    assertThat(recordingErrorMessage.getConferenceId()).isEqualTo(conference.getRoomSid());
    assertThat(recordingErrorMessage.getRecordingId()).isNull();
    assertThat(recordingErrorMessage.getStartedAt()).isNotNull();
    assertThat(recordingErrorMessage.getEndedAt()).isNotNull();
    assertThat(recordingErrorMessage.isError()).isTrue();
    assertThat(recordingErrorMessage.getAudioChannel()).isEqualTo(NATIVE);
    testStreamingService.setRecordingError(false);
  }
}
