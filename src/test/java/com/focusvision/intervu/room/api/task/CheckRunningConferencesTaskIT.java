package com.focusvision.intervu.room.api.task;

import com.focusvision.intervu.room.api.adapter.ConferenceCheckSenderAdapter;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;
import com.focusvision.intervu.room.api.service.ConferenceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import static java.lang.Thread.sleep;
import static java.util.List.of;
import static java.util.UUID.randomUUID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * IT tests for {@link CheckRunningConferencesTask#run}
 */
@DirtiesContext
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {
        "app.tasks.check-running-conferences.enabled=true",
        "app.tasks.check-running-conferences.initial-delay=500",
        "app.tasks.check-running-conferences.period=1000",
})
@DisplayName("Check running conferences task")
class CheckRunningConferencesTaskIT {

    @MockBean
    private ConferenceService conferenceService;
    @MockBean
    private ConferenceCheckSenderAdapter adapter;

    @BeforeEach
    void setup() {
        when(conferenceService.getAllRunningConferences())
                .thenReturn(of(new Conference().setId(randomUUID()), new Conference().setId(randomUUID())));
    }

    @Test
    @DisplayName("Task executed")
    void taskExecuted() throws InterruptedException {
        sleep(1000);
        verify(conferenceService, times(1)).getAllRunningConferences();
        verify(adapter, times(2)).send(any(RunningConferenceCheck.class));
    }
}