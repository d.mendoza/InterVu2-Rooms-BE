package com.focusvision.intervu.room.api.event;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ResearchSessionProcessResultConsumer;
import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.repository.ResearchSessionRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult.Status.FAILED;
import static com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult.Status.SUCCESS;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

/**
 * IT tests for {@link ResearchSessionData} event.
 * Various new research session tests.
 */
@DisplayName("New research session event")
public class NewResearchSessionEventIT extends HttpBasedIT {

    @Autowired
    private MessagingProperties messagingProperties;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ResearchSessionRepository researchSessionRepository;

    private ResearchSessionProcessResultConsumer consumer;

    @Test
    @DisplayName("Fails for invalid data")
    void invalidDataFail() {
        var researchSessionData = new ResearchSessionData()
                .setId(make())
                .setVersion(1);

        consumer = researchSessionProcessResultConsumer(1);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var message = consumer.getMessage(researchSessionData.getId(), researchSessionData.getVersion()).orElseThrow();
        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isNotPresent();
        assertThat(message.getStatus()).isEqualByComparingTo(FAILED);
        assertThat(message.getMessage()).isNotEmpty();
        assertThat(message.getMessage()).contains("NullPointer");
        assertThat(message.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Created")
    void created() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(1);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var message = consumer.getMessage(researchSessionData.getId(), researchSessionData.getVersion()).orElseThrow();
        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(researchSession.get().getParticipants()).hasSize(2);
        assertThat(researchSession.get().getStimuli()).hasSize(1);
        assertThat(message.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(message.getMessage()).isNull();
        assertThat(message.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Created without participants and stimuli")
    void createdWithoutParticipantsAndStimuli() {
        var researchSessionData = generate()
                .setParticipants(null)
                .setStimuli(null);

        consumer = researchSessionProcessResultConsumer(1);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var message = consumer.getMessage(researchSessionData.getId(), researchSessionData.getVersion()).orElseThrow();
        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(researchSession.get().getParticipants()).isEmpty();
        assertThat(researchSession.get().getStimuli()).isEmpty();
        assertThat(message.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(message.getMessage()).isNull();
        assertThat(message.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }
}
