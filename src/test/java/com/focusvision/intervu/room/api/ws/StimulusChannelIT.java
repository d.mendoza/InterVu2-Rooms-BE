package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

/**
 * Integration tests for WS stimulus channel.
 */
@DisplayName("Stimulus channel")
class StimulusChannelIT extends HttpBasedIT {

    @Test
    @DisplayName("Subscribing forbidden for intervu users")
    void forbiddenForIntervuUsers() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectWs(respondent);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for wrong room")
    void messageForbiddenForWrongRoom() {
        var researchSession = setUpFullRoom();
        var researchSession1 = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession1.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for respondent")
    void forbiddenForRespondent() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var respondentSession = connectRoomWs(respondent);
        var translatorSessions = connectRoomWs(translator);

        subscribe(respondentSession, stimulusChannel, handler);
        subscribe(translatorSessions, stimulusChannel, handler);

        assertThat(respondentSession.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing for moderator - ok")
    void okForModerator() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isTrue();

        session.disconnect();
    }

    @Test
    @DisplayName("Subscribing for observer - ok")
    void okForObserver() {
        var researchSession = setUpFullRoom();
        var observer = getObserver(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(observer);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isTrue();

        session.disconnect();
    }

    @Test
    @DisplayName("Subscribing for translator - ok")
    void okForTranslator() {
        var researchSession = setUpFullRoom();
        var translator = getTranslator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(translator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isTrue();

        session.disconnect();
    }
}
