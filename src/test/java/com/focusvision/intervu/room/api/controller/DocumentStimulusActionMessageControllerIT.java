package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.StimulusGlobalChannelStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.stimulus.document.DocumentStimulusActionMessageController;
import com.focusvision.intervu.room.api.stimulus.document.model.DocumentStimulusActionMessageDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;

import static com.focusvision.intervu.room.api.common.model.StimulusActionType.DOCUMENT_PAGE_CHANGED;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

/**
 * Integration tests for {@link DocumentStimulusActionMessageController}.
 */
@DisplayName("Document stimulus actions")
public class DocumentStimulusActionMessageControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Subscribing forbidden for respondent")
    void forbiddenForRespondent() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(respondent);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for intervu users")
    void forbiddenForIntervuUsers() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for wrong room")
    void messageForbiddenForWrongRoom() {
        var researchSession = setUpFullRoom();
        var researchSession1 = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession1.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Change page - forbidden for respondent")
    void changePage_forbiddenForRespondent() {
        var researchSession = setUpFullRoom();
        var documentStimulus = getDocumentSharingStimulus(researchSession);
        var content = make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorDocumentStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, documentStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorDocumentStimulusActionHandler);

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var voteAction = new DocumentStimulusActionMessageDto()
                .setStimulusId(documentStimulus.getId())
                .setContent(content)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(DOCUMENT_STIMULUS_PAGE_CHANGE_DESTINATION, voteAction);
        waitForWs();

        var moderatorDocumentStimulusMessages = moderatorDocumentStimulusActionHandler.getMessages();

        assertThat(moderatorDocumentStimulusMessages).isEmpty();
    }

    @Test
    @DisplayName("Change page")
    void changePage() {
        var researchSession = setUpFullRoom();
        var documentStimulus = getDocumentSharingStimulus(researchSession);

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorDocumentStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);
        var respondentDocumentStimulusActionHandler = new StimulusGlobalChannelStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, documentStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var globalChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorDocumentStimulusActionHandler);
        subscribe(respondentSession, globalChannel, respondentDocumentStimulusActionHandler);

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var pageChangeAction1 = new DocumentStimulusActionMessageDto()
                .setStimulusId(documentStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(DOCUMENT_STIMULUS_PAGE_CHANGE_DESTINATION, pageChangeAction1);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), documentStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(1))
                .body("[0].size()", Matchers.equalTo(3))
                .body("[0].participantId", equalTo(moderator.getId().toString()))
                .body("[0].content", equalTo(pageChangeAction1.getContent()))
                .body("[0].stimulusId", equalTo(documentStimulus.getId().toString()))
                .extract().as(StimulusStateDto[].class);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(PARTICIPANT_DOCUMENT_STIMULUS_STATE), documentStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(3))
                .body("participantId", equalTo(moderator.getId().toString()))
                .body("content", equalTo(pageChangeAction1.getContent()))
                .body("stimulusId", equalTo(documentStimulus.getId().toString()));

        var pageChangeAction2 = new DocumentStimulusActionMessageDto()
                .setStimulusId(documentStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        moderatorSession.send(DOCUMENT_STIMULUS_PAGE_CHANGE_DESTINATION, pageChangeAction2);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(PARTICIPANT_DOCUMENT_STIMULUS_STATE), documentStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(3))
                .body("participantId", equalTo(moderator.getId().toString()))
                .body("content", equalTo(pageChangeAction2.getContent()))
                .body("stimulusId", equalTo(documentStimulus.getId().toString()));

        finishRoom(moderator);
        waitForWs();

        var moderatorDocumentStimulusMessages = moderatorDocumentStimulusActionHandler.getMessages();
        var respondentDocumentStimulusMessages = respondentDocumentStimulusActionHandler.getMessages();
        var respondentData1 = (LinkedHashMap<String, String>) respondentDocumentStimulusMessages.get(0).getData();
        var respondentData2 = (LinkedHashMap<String, String>) respondentDocumentStimulusMessages.get(1).getData();

        assertAll(
                () -> assertThat(moderatorDocumentStimulusMessages.size()).isEqualTo(2),
                () -> assertThat(respondentDocumentStimulusMessages.size()).isEqualTo(2),
                () -> assertThat(moderatorDocumentStimulusMessages.get(0).getRoomId()).isEqualTo(researchSession.getId()),
                () -> assertThat(moderatorDocumentStimulusMessages.get(0).getParticipantId()).isEqualTo(moderator.getId()),
                () -> assertThat(moderatorDocumentStimulusMessages.get(0).getStimulusId()).isEqualTo(documentStimulus.getId()),
                () -> assertThat(moderatorDocumentStimulusMessages.get(0).getType()).isEqualTo(DOCUMENT_PAGE_CHANGED),
                () -> assertThat(moderatorDocumentStimulusMessages.get(0).getContent()).isEqualTo(pageChangeAction1.getContent()),
                () -> assertThat(moderatorDocumentStimulusMessages.get(1).getRoomId()).isEqualTo(researchSession.getId()),
                () -> assertThat(moderatorDocumentStimulusMessages.get(1).getParticipantId()).isEqualTo(moderator.getId()),
                () -> assertThat(moderatorDocumentStimulusMessages.get(1).getStimulusId()).isEqualTo(documentStimulus.getId()),
                () -> assertThat(moderatorDocumentStimulusMessages.get(1).getType()).isEqualTo(DOCUMENT_PAGE_CHANGED),
                () -> assertThat(moderatorDocumentStimulusMessages.get(1).getContent()).isEqualTo(pageChangeAction2.getContent()),
                () -> assertThat(respondentData1.get("roomId")).isEqualTo(researchSession.getId().toString()),
                () -> assertThat(respondentData1.get("stimulusId")).isEqualTo(documentStimulus.getId().toString()),
                () -> assertThat(respondentData1.get("content")).isEqualTo(pageChangeAction1.getContent()),
                () -> assertThat(respondentData1.get("type")).isEqualTo(DOCUMENT_PAGE_CHANGED.toString()),
                () -> assertThat(respondentData2.get("roomId")).isEqualTo(researchSession.getId().toString()),
                () -> assertThat(respondentData2.get("stimulusId")).isEqualTo(documentStimulus.getId().toString()),
                () -> assertThat(respondentData2.get("content")).isEqualTo(pageChangeAction2.getContent()),
                () -> assertThat(respondentData2.get("type")).isEqualTo(DOCUMENT_PAGE_CHANGED.toString())
        );
    }

}
