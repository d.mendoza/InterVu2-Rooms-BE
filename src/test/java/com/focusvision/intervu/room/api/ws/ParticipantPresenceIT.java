package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static io.restassured.RestAssured.given;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for WS participant presence.
 *
 * @author Branko Ostojic
 */
class ParticipantPresenceIT extends HttpBasedIT {

    @Test
    @DisplayName("Presence status updated on connect/disconnect")
    void presenceStatusUpdated() throws Exception {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);

        var moderatorRoomState = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.participants.size()", equalTo(0))
                .extract().body().as(ParticipantRoomStateDto.class);

        var moderatorSession = connectRoomWs(moderator);
        var handler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, moderatorRoomState.getParticipantState().getChannel().getRoom(), handler);
        await().atMost(1, SECONDS).until(() -> handler.getStates().size() == 1);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.participants.findAll { p -> p.id == '" + moderator.getId() + "' }[0].online", equalTo(true));

        moderatorSession.disconnect();
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.participants.findAll { p -> p.id == '" + moderator.getId() + "' }[0].online", equalTo(false));
    }

    @Test
    @DisplayName("Presence status updated WS events")
    void presenceStatusUpdated_eventsSent() throws Exception {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var respondentResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);
        var moderatorState = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.state", equalTo(SCHEDULED.name()))
                .extract().body().as(ParticipantRoomStateDto.class);
        var respondentState = given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.state", equalTo(SCHEDULED.name()))
                .extract().body().as(ParticipantRoomStateDto.class);


        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        var moderatorSession = connectRoomWs(moderator);
        subscribe(moderatorSession, moderatorState.getParticipantState()
                .getChannel().getRoom(), moderatorHandler);
        await().atMost(500, MILLISECONDS).until(() -> moderatorHandler.getStates().size() >= 1);

        var respondentHandler = new RoomStateStompFrameHandler(mapper, respondentResultKeeper::complete);
        var respondentSession = connectRoomWs(respondent);
        subscribe(respondentSession, respondentState.getParticipantState().getChannel().getRoom(), respondentHandler);
        assertThat(respondentSession.isConnected()).isTrue();
        await().atMost(500, MILLISECONDS).until(() -> respondentHandler.getStates().size() >= 1);

        respondentSession.disconnect();
        assertThat(respondentSession.isConnected()).isFalse();

        await().atMost(500, MILLISECONDS).until(() -> moderatorHandler.getStates().size() == 3);
        assertThat(moderatorHandler.getStates()).hasSize(3);
        var respondentPresenceBeforeConnected = moderatorHandler.getStates().get(0).getData().getParticipants().stream()
                .filter(p -> p.getId().equals(respondent.getId())).findFirst();
        var respondentPresenceBeforeDisconnect = moderatorHandler.getStates().get(1).getData().getParticipants()
                .stream()
                .filter(p -> p.getId().equals(respondent.getId())).findFirst().orElseThrow();
        var respondentPresenceAfterDisconnect = moderatorHandler.getStates().get(2).getData().getParticipants()
                .stream()
                .filter(p -> p.getId().equals(respondent.getId())).findFirst().orElseThrow();
        assertThat(respondentPresenceBeforeConnected).isNotPresent();
        assertThat(respondentPresenceBeforeDisconnect.isOnline()).isTrue();
        assertThat(respondentPresenceAfterDisconnect.isOnline()).isFalse();
    }
}
