package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

/**
 * Integration tests for WS drawing sync events.
 */
@DisplayName("Stimulus global channel")
class StimulusGlobalChannelIT extends HttpBasedIT {

    @Test
    @DisplayName("Subscribing forbidden for intervu users")
    void forbiddenForIntervuUsers() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for wrong room")
    void messageForbiddenForWrongRoom() {
        var researchSession = setUpFullRoom();
        var researchSession1 = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession1.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for non respondent")
    void forbiddenForNonRespondent() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var moderatorSession = connectRoomWs(moderator);
        var translatorSessions = connectRoomWs(translator);
        var observerSession = connectRoomWs(observer);

        subscribe(moderatorSession, stimulusChannel, handler);
        subscribe(translatorSessions, stimulusChannel, handler);
        subscribe(observerSession, stimulusChannel, handler);

        assertAll(
                () -> assertThat(moderatorSession.isConnected()).isFalse(),
                () -> assertThat(translatorSessions.isConnected()).isFalse(),
                () -> assertThat(observerSession.isConnected()).isFalse()
        );
    }

    @Test
    @DisplayName("Subscribing for respondent - ok")
    void okForRespondent() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusGlobalChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(respondent);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isTrue();

        session.disconnect();
    }
}
