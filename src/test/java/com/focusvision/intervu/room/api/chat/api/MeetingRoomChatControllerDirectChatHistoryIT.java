package com.focusvision.intervu.room.api.chat.api;

import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.REGULAR;
import static com.focusvision.intervu.room.api.common.model.ChatType.PRIVATE;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.chat.model.ChatMessageDto;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * IT tests for {@link MeetingRoomChatController#privateChatMessages}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Meeting Room private chat history")
class MeetingRoomChatControllerDirectChatHistoryIT extends HttpBasedIT {

    @Test
    @DisplayName("OK for moderator/respondent")
    void respondentsChatData_respondentSender() throws Exception {
        var question = make();
        var answer = make();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);

        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);
        sendMeetingRoomPrivateChatMessage(respondent, moderator, question);
        sendMeetingRoomPrivateChatMessage(moderator, respondent, answer);
        sendMeetingRoomPrivateChatMessage(respondent, moderator, question);
        sendMeetingRoomPrivateChatMessage(moderator, respondent, answer);
        waitForWs();

        List<ChatMessageDto> chatMessages = given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .get(url(MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL), respondent.getId())
            .then()
            .statusCode(OK.value())
            .body("size()", is(3))
            .body("unreadMessages", equalTo(2))
            .body("latestSeenMessageId", nullValue())
            .body("messages.size()", is(4))
            .body("messages[0].size()", is(9))
            .extract().body().jsonPath().getList("messages", ChatMessageDto.class);
        given()
            .when()
            .headers(roomParticipantHeaders(respondent))
            .get(url(MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL), moderator.getId())
            .then()
            .statusCode(OK.value())
            .body("size()", is(3))
            .body("unreadMessages", equalTo(2))
            .body("latestSeenMessageId", nullValue())
            .body("messages.size()", is(4))
            .body("messages[0].size()", is(9))
            .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        ChatMessageDto respondentChatMessage = chatMessages.get(0);
        ChatMessageDto moderatorChatMessage = chatMessages.get(1);
        assertAll(
            () -> assertThat(respondentChatMessage.getId()).isNotNull(),
            () -> assertThat(respondentChatMessage.getSenderId()).isEqualTo(respondent.getId()),
            () -> assertThat(respondentChatMessage.getHandle()).isEqualTo(moderator.getId()),
            () -> assertThat(respondentChatMessage.getType()).isEqualByComparingTo(PRIVATE),
            () -> assertThat(respondentChatMessage.getMessageType()).isEqualByComparingTo(REGULAR),
            () -> assertThat(respondentChatMessage.getMessage()).isEqualTo(question),
            () -> assertThat(respondentChatMessage.getSenderName()).isEqualTo(respondent.getDisplayName()),
            () -> assertThat(respondentChatMessage.getSentAt()).isNotNull(),

            () -> assertThat(moderatorChatMessage.getId()).isNotNull(),
            () -> assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId()),
            () -> assertThat(moderatorChatMessage.getHandle()).isEqualTo(respondent.getId()),
            () -> assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(PRIVATE),
            () -> assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR),
            () -> assertThat(moderatorChatMessage.getMessage()).isEqualTo(answer),
            () -> assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName()),
            () -> assertThat(moderatorChatMessage.getSentAt()).isNotNull()
        );

        sendMeetingRoomPrivateChatMessageSeen(moderator, respondent, respondentChatMessage.getId());
        sendMeetingRoomPrivateChatMessageSeen(respondent, moderator, respondentChatMessage.getId());
        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .get(url(MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL), respondent.getId())
            .then()
            .statusCode(OK.value())
            .body("size()", is(3))
            .body("unreadMessages", equalTo(1))
            .body("latestSeenMessageId", equalTo(respondentChatMessage.getId().toString()))
            .body("messages.size()", is(4))
            .body("messages[0].size()", is(9))
            .extract().body().jsonPath().getList("messages", ChatMessageDto.class);
        given()
            .when()
            .headers(roomParticipantHeaders(respondent))
            .get(url(MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL), moderator.getId())
            .then()
            .statusCode(OK.value())
            .body("size()", is(3))
            .body("unreadMessages", equalTo(2))
            .body("latestSeenMessageId", equalTo(respondentChatMessage.getId().toString()))
            .body("messages.size()", is(4))
            .body("messages[0].size()", is(9))
            .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        sendMeetingRoomPrivateChatMessageSeen(moderator, respondent, chatMessages.get(3).getId());
        sendMeetingRoomPrivateChatMessageSeen(respondent, moderator, chatMessages.get(3).getId());
        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .get(url(MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL), respondent.getId())
            .then()
            .statusCode(OK.value())
            .body("size()", is(3))
            .body("unreadMessages", equalTo(0))
            .body("latestSeenMessageId", equalTo(chatMessages.get(3).getId().toString()))
            .body("messages.size()", is(4))
            .body("messages[0].size()", is(9))
            .extract().body().jsonPath().getList("messages", ChatMessageDto.class);
        given()
            .when()
            .headers(roomParticipantHeaders(respondent))
            .get(url(MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL), moderator.getId())
            .then()
            .statusCode(OK.value())
            .body("size()", is(3))
            .body("unreadMessages", equalTo(0))
            .body("latestSeenMessageId", equalTo(chatMessages.get(3).getId().toString()))
            .body("messages.size()", is(4))
            .body("messages[0].size()", is(9))
            .extract().body().jsonPath().getList("messages", ChatMessageDto.class);
    }

}
