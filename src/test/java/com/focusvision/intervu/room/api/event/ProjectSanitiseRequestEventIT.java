package com.focusvision.intervu.room.api.event;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.model.messaging.ProjectSanitiseRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Long.valueOf;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

/**
 * IT tests for processing {@link ProjectSanitiseRequest} events.
 */
@DisplayName("Project Sanitise event")
class ProjectSanitiseRequestEventIT extends HttpBasedIT {

    @Autowired
    private MessagingProperties messagingProperties;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @BeforeEach
    void setUp() {
        purgeQueue(messagingProperties.getProjectSanitiseQueue());
    }

    @Test
    @DisplayName("Sanitise success - respondent chat history cleared.")
    void success() throws Exception {
        var session = setUpFullRoom();
        var projectSanitiseRequest = new ProjectSanitiseRequest()
                .setPlatformId(valueOf(session.getProject().getPlatformId()));
        var respondent = getRespondent(session);
        var moderator = getModerator(session);

        sendWaitingRoomRespondentsChatMessage(respondent, respondent.getDisplayName());
        sendBackRoomInternalChatMessage(moderator, make());
        sendWaitingRoomInternalChatMessage(moderator, make());
        sendWaitingRoomRespondentsChatMessage(moderator, make());

        var consumer = projectSanitiseRequestConsumer(1);
        rabbitTemplate.convertAndSend(messagingProperties.getProjectSanitiseQueue(), projectSanitiseRequest);
        waitForWs();

        assertAll(
                () -> assertThat(chatMessageRepository.findAllBySenderPlatformId(respondent.getPlatformId())).isEmpty(),
                () -> assertThat(chatMessageRepository.findAllBySenderPlatformId(moderator.getPlatformId())).hasSize(3)
        );
        consumer.stop();
    }

}
