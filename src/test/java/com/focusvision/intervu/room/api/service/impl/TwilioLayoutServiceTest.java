package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.RecordingType.SCREENSHARE;
import static com.focusvision.intervu.room.api.common.model.RecordingType.VIDEO;
import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static java.lang.String.format;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import com.focusvision.intervu.room.api.common.model.RecordingType;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.focusvision.intervu.room.api.twilio.service.TwilioLayoutService;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TwilioLayoutServiceTest {

  private static final String VIDEO_SOURCES = "video_sources";
  private static final String MAX_ROWS = "max_rows";
  private static final String Z_POS = "z_pos";
  private static final String X_POS = "x_pos";
  private static final String Y_POS = "y_pos";
  private static final String WIDTH = "width";
  private static final String HEIGHT = "height";
  private static final String MAX_COLUMNS = "max_columns";

  private static final int VIDEO_HEIGHT = 720;
  private static final double CAMERA_RATIO = 4.0 / 3;

  // PICTURE_IN_PICTURE_WITH_COLUMN layout parameters
  private static final int SINGLE_CAMERA_HEIGHT = VIDEO_HEIGHT / 5;
  private static final int REDUCED_COLUMN_CAMERA_HEIGHT = SINGLE_CAMERA_HEIGHT * 3 / 4;
  private static final int MAX_SINGLE_COLUMN_CAMERAS =
      VIDEO_HEIGHT / REDUCED_COLUMN_CAMERA_HEIGHT;
  private static final int REDUCED_CAMERA_HEIGHT_OFFSET =
      VIDEO_HEIGHT / MAX_SINGLE_COLUMN_CAMERAS;
  private static final int BASE_COLUMN_CAMERA_WIDTH =
      Double.valueOf(SINGLE_CAMERA_HEIGHT * CAMERA_RATIO).intValue();
  private static final int REDUCED_COLUMN_CAMERA_WIDTH = BASE_COLUMN_CAMERA_WIDTH * 3 / 4;
  private static final int MAX_VIDEOS_PER_SINGLE_CELL = 1;
  private static final String COLUMN_VIDEOS_WILDCARD = "RESPONDENT_camera_%s*";
  private static final String MODERATOR_VIDEOS_WILDCARD = "MODERATOR_camera_%s*";
  private static final int Z_ORDER_COLUMNS_REGION = 2;

  private TwilioLayoutService service;

  @BeforeEach
  public void setup() {
    service = new TwilioLayoutService();
  }

  @Test
  @DisplayName("Create picture in picture with column layout")
  void createPictureInPictureWithColumnLayout() {
    var moderator = participantRecording(VIDEO, MODERATOR.name());
    var screenshare = participantRecording(SCREENSHARE, null);
    var respondent = participantRecording(VIDEO, "RESPONDENT");

    var result = service.createLayoutData(
        List.of(moderator, screenshare, respondent),
        PICTURE_IN_PICTURE_WITH_COLUMN,
        Set.of()).get();

    assertAll(
        () -> assertThat(result.getLayout().containsKey("pip")).isFalse(),
        () -> assertThat(result.getLayout().get("main")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().get("CELL_0")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().get("CELL_1")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().size()).isEqualTo(3),
        () -> assertThat(result.getScreenshareBounds().getScreenshareArea()).isNotNull(),
        () -> assertThat(result.getScreenshareBounds().getExcludedAreas()).isNull()
    );
  }

  @Test
  @DisplayName("Create picture in picture with column layout, no moderator")
  void createPictureInPictureWithColumnLayout_noModerator() {
    var screenshare = participantRecording(SCREENSHARE, null);
    var respondent = participantRecording(VIDEO, "RESPONDENT");

    var result = service.createLayoutData(
        List.of(screenshare, respondent),
        PICTURE_IN_PICTURE_WITH_COLUMN,
        Set.of()).get();

    assertAll(
        () -> assertThat(result.getLayout().containsKey("pip")).isFalse(),
        () -> assertThat(result.getLayout().get("main")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().get("CELL_0")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().size()).isEqualTo(2),
        () -> assertThat(result.getScreenshareBounds().getScreenshareArea()).isNotNull(),
        () -> assertThat(result.getScreenshareBounds().getExcludedAreas()).isNull()
    );
  }

  @Test
  @DisplayName("Create picture in picture with column layout")
  void createPictureInPictureWithColumnLayout_noScreenshare() {
    var moderator = participantRecording(VIDEO, MODERATOR.name());
    var respondent = participantRecording(VIDEO, "RESPONDENT");

    var result = service.createLayoutData(
        List.of(moderator, respondent),
        PICTURE_IN_PICTURE_WITH_COLUMN,
        Set.of()).get();

    assertAll(
        () -> assertThat(result.getLayout().containsKey("pip")).isFalse(),
        () -> assertThat(result.getLayout().containsKey("main")).isFalse(),
        () -> assertThat(result.getLayout().get("CELL_0")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().get("CELL_1")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().size()).isEqualTo(2),
        () -> assertThat(result.getScreenshareBounds().getScreenshareArea()).isNotNull(),
        () -> assertThat(result.getScreenshareBounds().getExcludedAreas()).isNull()
    );
  }

  @Test
  @DisplayName("Create picture in picture with column layout - multiple respondents")
  void createPictureInPictureWithColumnLayout_multipleRespondents() {
    var moderator = participantRecording(VIDEO, MODERATOR.name());
    var screenshare = participantRecording(SCREENSHARE, null);
    var respondent1 = participantRecording(VIDEO, "RESPONDENT");
    var respondent2 = participantRecording(VIDEO, "RESPONDENT");

    var result = service.createLayoutData(
        List.of(moderator, screenshare, respondent1, respondent2),
        PICTURE_IN_PICTURE_WITH_COLUMN,
        Set.of()).get();

    assertAll(
        () -> assertThat(result.getLayout().containsKey("pip")).isFalse(),
        () -> assertThat(result.getLayout().get("main")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().get("CELL_0")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().get("CELL_1")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().get("CELL_2")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().size()).isEqualTo(4),
        () -> assertThat(result.getScreenshareBounds().getScreenshareArea()).isNotNull(),
        () -> assertThat(result.getScreenshareBounds().getExcludedAreas()).isNull()
    );
  }

  @Test
  @DisplayName("Create picture in picture with column layout - two columns")
  void createPictureInPictureWithColumnLayout_twoColumns() {
    var moderator1 = participantRecording(VIDEO, MODERATOR.name());
    var moderator2 = participantRecording(VIDEO, MODERATOR.name());
    var moderator3 = participantRecording(VIDEO, MODERATOR.name());
    var moderator4 = participantRecording(VIDEO, MODERATOR.name());
    var screenshare = participantRecording(SCREENSHARE, null);
    var respondent1 = participantRecording(VIDEO, "RESPONDENT");
    var respondent2 = participantRecording(VIDEO, "RESPONDENT");
    var respondent3 = participantRecording(VIDEO, "RESPONDENT");
    var respondent4 = participantRecording(VIDEO, "RESPONDENT");
    var respondent5 = participantRecording(VIDEO, "RESPONDENT");

    var result = service.createLayoutData(
        List.of(moderator1, moderator2, moderator3, moderator4, screenshare, respondent1,
            respondent2, respondent3, respondent4, respondent5),
        PICTURE_IN_PICTURE_WITH_COLUMN,
        Set.of()).get();

    var participantVideos = List.of(moderator1, moderator2, moderator3, moderator4, respondent1,
            respondent2, respondent3, respondent4, respondent5).stream()
        .map(participant -> format(
            participant.isModerator() ? MODERATOR_VIDEOS_WILDCARD : COLUMN_VIDEOS_WILDCARD,
            participant.getParticipantId()))
        .toList();

    assertAll(
        () -> assertThat(result.getLayout().containsKey("pip")).isFalse(),
        () -> assertThat(result.getLayout().get("main")).isInstanceOf(Map.class),
        () -> assertThat(result.getLayout().size()).isEqualTo(10),
        () -> assertThat(result.getScreenshareBounds().getScreenshareArea()).isNotNull(),
        () -> assertThat(result.getScreenshareBounds().getExcludedAreas()).isNull()
    );

    for (int i = 0; i < 9; i++) {
      var columnNum = i / MAX_SINGLE_COLUMN_CAMERAS;
      var columnPos = i % MAX_SINGLE_COLUMN_CAMERAS;
      assertThat(result.getLayout().get("CELL_" + i)).isInstanceOf(Map.class)
          .extracting(layout -> ((Map<String, String>) layout))
          .isEqualTo(Map.of(X_POS, columnNum * REDUCED_COLUMN_CAMERA_WIDTH,
              Y_POS, columnPos * REDUCED_CAMERA_HEIGHT_OFFSET,
              Z_POS, Z_ORDER_COLUMNS_REGION,
              HEIGHT, REDUCED_CAMERA_HEIGHT_OFFSET,
              WIDTH, REDUCED_COLUMN_CAMERA_WIDTH,
              MAX_COLUMNS, MAX_VIDEOS_PER_SINGLE_CELL,
              MAX_ROWS, MAX_VIDEOS_PER_SINGLE_CELL,
              VIDEO_SOURCES, List.of(participantVideos.get(i))));
    }
  }

  private ParticipantGroupRoomRecording participantRecording(RecordingType recordingType,
                                                             String participantType) {
    return new ParticipantGroupRoomRecording().setStartedAt(Instant.now())
        .setRecordingType(recordingType)
        .setParticipantType(participantType)
        .setParticipantId(make())
        .setTrackName(make())
        .setSid(make());
  }

}
