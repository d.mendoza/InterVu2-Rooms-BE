package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static io.restassured.RestAssured.given;
import static org.springframework.http.HttpStatus.FORBIDDEN;

/**
 * Integration tests for {@link StimuliController} focus endpoints.
 */
class StimuliControllerFocusIT extends HttpBasedIT {

    @Test
    @DisplayName("Enable stimuli focus - unauthorized")
    void enableStimuliFocus_unauthorized() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(STIMULI_FOCUS_ENABLE_URL))
                .then()
                .statusCode(FORBIDDEN.value());
    }

    @Test
    @DisplayName("Disable stimuli focus - unauthorized")
    void disableStimuliFocus_unauthorized() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(STIMULI_FOCUS_DISABLE_URL))
                .then()
                .statusCode(FORBIDDEN.value());
    }
}
