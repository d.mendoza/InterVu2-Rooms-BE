package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.common.model.AudioChannel.NATIVE;
import static com.focusvision.intervu.room.api.common.model.AudioChannel.NATIVE_AND_TRANSLATOR;
import static com.focusvision.intervu.room.api.common.model.AudioChannel.TRANSLATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ProjectType.FULL_SERVICE;
import static com.focusvision.intervu.room.api.common.model.ProjectType.SELF_SERVICE;
import static com.focusvision.intervu.room.api.model.CompositionLayout.GRID;
import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static com.focusvision.intervu.room.api.model.entity.Recording.State.PENDING;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.ProjectType;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.entity.Recording.State;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.focusvision.intervu.room.api.repository.ConferenceRepository;
import com.focusvision.intervu.room.api.service.StreamingService;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.StimulusState;
import com.focusvision.intervu.room.api.state.repository.RoomStateLogRepository;
import com.focusvision.intervu.room.api.state.stimulus.repository.StimulusStateRepository;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link RoomController#finish}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Finish room")
class RoomControllerFinishRoomIT extends HttpBasedIT {

  @SpyBean
  protected StreamingService streamingService;
  @Autowired
  private ConferenceRepository conferenceRepository;

  @Autowired
  private StimulusStateRepository stimulusStateRepository;

  @Autowired
  private RoomStateLogRepository roomStateLogRepository;

  @Test
  @DisplayName("Forbidden for unauthorized")
  void forbiddenUnauthorized() {
    given()
        .when()
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Unauthorized, banned participant")
  void unauthorizedBannedParticipant() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    participantRepository.save(moderator.setBanned(true));

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(ProjectType.class)
  @DisplayName("Forbidden for non moderators")
  void forbiddenForNonModerators(ProjectType projectType) {
    project = projectRepository.save(project.setType(projectType));
    var researchSession = setUpFullRoom();
    var respondent = getRespondent(researchSession);
    var translator = getTranslator(researchSession);
    var observer = getObserver(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(respondent))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.FORBIDDEN.value());

    given()
        .when()
        .headers(roomParticipantHeaders(translator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.FORBIDDEN.value());

    given()
        .when()
        .headers(roomParticipantHeaders(observer))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.FORBIDDEN.value());
  }

  @ParameterizedTest
  @EnumSource(ProjectType.class)
  @DisplayName("Admin can finnish the room")
  void okForAdmin(ProjectType projectType) {
    project = projectRepository.save(project.setType(projectType));
    var admin = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(MODERATOR)
        .setAdmin(true)
        .setBanable(false)
        .setGuest(false)
        .setProjectManager(false)
        .setProjectOperator(false)
        .setModerator(true);
    var researchSession = setUpRoom(admin);
    var participant = getParticipant(researchSession, MODERATOR);

    startRoom(participant);
    finishRoom(participant);

    verifyThatRoomHasFinished(participant);
  }

  @ParameterizedTest
  @EnumSource(ProjectType.class)
  @DisplayName("Project manager can finish the room")
  void okForProjectManager(ProjectType projectType) {
    project = projectRepository.save(project.setType(projectType));
    var projectManager = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(MODERATOR)
        .setAdmin(false)
        .setBanable(true)
        .setGuest(false)
        .setProjectManager(true)
        .setProjectOperator(false)
        .setModerator(true);
    var researchSession = setUpRoom(projectManager);
    var participant = getParticipant(researchSession, MODERATOR);

    startRoom(participant);
    finishRoom(participant);

    verifyThatRoomHasFinished(participant);
  }

  @ParameterizedTest
  @EnumSource(ProjectType.class)
  @DisplayName("Project operator can finish the room")
  void okForProjectOperator(ProjectType projectType) {
    project = projectRepository.save(project.setType(projectType));
    var projectOperator = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(MODERATOR)
        .setAdmin(false)
        .setBanable(true)
        .setGuest(false)
        .setProjectManager(false)
        .setProjectOperator(true)
        .setModerator(true);
    var researchSession = setUpRoom(projectOperator);
    var participant = getParticipant(researchSession, MODERATOR);

    startRoom(participant);
    finishRoom(participant);

    verifyThatRoomHasFinished(participant);
  }

  @Test
  @DisplayName("Moderator can finish the room - self service project")
  void okForModerator_selfServiceProject() {
    project = projectRepository.save(project.setType(SELF_SERVICE));
    var room = setUpFullRoom();
    var moderator = getModerator(room);
    var conference = conferenceRepository.save(new Conference()
        .setRoomSid(make())
        .setResearchSession(room));

    room.setConference(conference);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.OK.value());

    conference = conferenceRepository.getById(conference.getId());
    assertThat(conference.getRecordings()).extracting(Recording::getState)
        .containsExactly(PENDING);
    assertThat(conference.getRecordings()).extracting(Recording::getCompositionLayout)
        .containsExactly(GRID);
    assertThat(conference.getRecordings()).extracting(Recording::getAudioChannel)
        .containsExactly(NATIVE);
  }

  @Test
  @DisplayName("Forbidden for non admin - full service project")
  void forbiddenForNonAdmin_fullServiceProject() {
    project = projectRepository.save(project.setType(FULL_SERVICE));
    var admin = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(OBSERVER)
        .setAdmin(true)
        .setBanable(false)
        .setGuest(false)
        .setProjectManager(false)
        .setProjectOperator(false)
        .setModerator(true);
    var moderator = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(MODERATOR)
        .setAdmin(false)
        .setBanable(true)
        .setGuest(false)
        .setProjectManager(false)
        .setProjectOperator(false)
        .setModerator(true);
    var researchSession = setUpRoom(admin, moderator);
    var adminParticipant = getParticipant(researchSession, OBSERVER);
    var moderatorParticipant = getParticipant(researchSession, MODERATOR);

    startRoom(adminParticipant);

    given()
        .when()
        .headers(roomParticipantHeaders(moderatorParticipant))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("When using stimuli in room - then layout 5")
  void whenUsingStimuliInRoom_ThenLayoutFive() {
    project = projectRepository.save(project.setType(SELF_SERVICE));
    var room = setUpFullRoom();
    var moderator = getModerator(room);
    var conference = conferenceRepository.save(new Conference()
        .setRoomSid(make())
        .setResearchSession(room));

    room.setConference(conference);

    var respondent1 = getRespondent(room);
    var stimulus = room.getStimuli().get(0);
    var stimulusState = StimulusState.builder().broadcastResults(false).id(stimulus.getId())
        .build();
    var roomState = RoomState.builder().roomId(room.getId()).stimuli(List.of(stimulusState))
        .build();
    stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent1.getId()));
    roomStateLogRepository.save(stimulusActivated(roomState));

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.OK.value());

    conference = conferenceRepository.getById(conference.getId());
    assertThat(conference.getRecordings()).extracting(Recording::getState)
        .containsExactly(PENDING, PENDING);
    assertThat(conference.getRecordings()).extracting(Recording::getCompositionLayout)
        .containsExactlyInAnyOrder(PICTURE_IN_PICTURE_WITH_COLUMN, GRID);
    assertThat(conference.getRecordings()).extracting(Recording::getAudioChannel)
        .containsExactly(NATIVE, NATIVE);
  }

  @Test
  @DisplayName("Ok - translator audio channel, missing translator")
  void ok_translatorAudioChannelMissingTranslator() {
    project = projectRepository.save(project.setType(SELF_SERVICE));
    var room = researchSessionRepository.save(setUpFullRoom().setAudioChannel(TRANSLATOR));
    var moderator = getModerator(room);
    var conference = conferenceRepository.save(new Conference()
        .setRoomSid(make())
        .setResearchSession(room));

    room.setConference(conference);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.OK.value());

    conference = conferenceRepository.getById(conference.getId());
    assertThat(conference.getRecordings()).extracting(Recording::getState)
        .containsExactly(PENDING);
    assertThat(conference.getRecordings()).extracting(Recording::getCompositionLayout)
        .containsExactly(GRID);
    assertThat(conference.getRecordings()).extracting(Recording::getAudioChannel)
        .containsExactly(NATIVE);
  }

  @Test
  @DisplayName("Ok - translator audio channel")
  void ok_translatorAudioChannel() {
    project = projectRepository.save(project.setType(SELF_SERVICE));
    var room = researchSessionRepository.save(setUpFullRoom().setAudioChannel(TRANSLATOR));
    addUserToRoom(room, ParticipantRole.TRANSLATOR);
    var moderator = getModerator(room);

    var conference = conferenceRepository.save(new Conference()
        .setRoomSid(make())
        .setResearchSession(room));
    when(streamingService.getParticipantsRecordings(conference.getRoomSid()))
        .thenReturn(List.of(new ParticipantGroupRoomRecording()
            .setParticipantType(ParticipantRole.TRANSLATOR.name())));

    room.setConference(conference);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.OK.value());

    conference = conferenceRepository.getById(conference.getId());
    assertThat(conference.getRecordings()).extracting(Recording::getState)
        .containsExactly(PENDING, PENDING);
    assertThat(conference.getRecordings()).extracting(Recording::getCompositionLayout)
        .containsExactly(GRID, GRID);
    assertThat(conference.getRecordings()).extracting(Recording::getAudioChannel)
        .containsExactlyInAnyOrder(TRANSLATOR, NATIVE);
  }

  @Test
  @DisplayName("Ok - two channel audio")
  void ok_twoChannelAudio() {
    project = projectRepository.save(project.setType(SELF_SERVICE));
    var room = setUpFullRoom();
    addUserToRoom(room, ParticipantRole.TRANSLATOR);
    var moderator = getModerator(room);

    var conference = conferenceRepository.save(new Conference()
        .setRoomSid(make())
        .setResearchSession(room));
    when(streamingService.getParticipantsRecordings(conference.getRoomSid()))
        .thenReturn(List.of(new ParticipantGroupRoomRecording()
            .setParticipantType(ParticipantRole.TRANSLATOR.name())));

    room.setConference(conference);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(HttpStatus.OK.value());

    conference = conferenceRepository.getById(conference.getId());
    assertThat(conference.getRecordings()).extracting(Recording::getState)
        .containsExactly(PENDING, PENDING);
    assertThat(conference.getRecordings()).extracting(Recording::getCompositionLayout)
        .containsExactly(GRID, GRID);
    assertThat(conference.getRecordings()).extracting(Recording::getAudioChannel)
        .containsExactlyInAnyOrder(TRANSLATOR, NATIVE);
  }
}
