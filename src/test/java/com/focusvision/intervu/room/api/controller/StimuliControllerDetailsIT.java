package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.model.dto.StimulusDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.UUID;

import static com.focusvision.intervu.room.api.common.model.StimulusType.POLL;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for fetching stimulus details in {@link StimuliController}.
 */
@DisplayName("Stimulus details")
public class StimuliControllerDetailsIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(ROOM_STIMULUS_DETAILS), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participant")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .get(url(ROOM_STIMULUS_DETAILS), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for non moderators")
    void forbiddenForNonModerators() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(ROOM_STIMULUS_DETAILS), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(ROOM_STIMULUS_DETAILS), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(ROOM_STIMULUS_DETAILS), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Fetch stimulus details")
    void fetchStimulusDetails() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = researchSession.getStimuli().stream()
                .filter(stimulus -> POLL.equals(stimulus.getType()))
                .findFirst()
                .orElseThrow();

        var moderator = getModerator(researchSession);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_STIMULUS_DETAILS), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("id", equalTo(pollingStimulus.getId().toString()))
                .body("platformId", equalTo(pollingStimulus.getPlatformId()))
                .body("name", equalTo(pollingStimulus.getName()))
                .body("type", equalTo(pollingStimulus.getType().toString()))
                .body("position", equalTo(pollingStimulus.getPosition()))
                .extract().as(StimulusDto.class);
    }
}
