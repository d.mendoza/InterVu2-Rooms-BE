package com.focusvision.intervu.room.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.model.event.ParticipantRoleChangedEvent;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

@Slf4j
@Getter
@RequiredArgsConstructor
public class ParticipantRoleChangedStompFrameHandler implements StompFrameHandler {

    private final ObjectMapper mapper;
    private final Consumer<ParticipantRoleChangedEvent> frameHandler;
    private final List<ParticipantRoleChangedEvent> events = new ArrayList<>();

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ParticipantRoleChangedEvent.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info("received message: {} with headers: {}", payload, headers);

        var event = mapper.convertValue(payload, ParticipantRoleChangedEvent.class);
        frameHandler.accept(event);
        events.add(event);
    }
}
