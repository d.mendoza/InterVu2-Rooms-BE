package com.focusvision.intervu.room.api.common.event;

import static java.util.UUID.randomUUID;
import static org.mockito.Mockito.verify;

import com.focusvision.intervu.room.api.event.room.model.RoomCanceledEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

@ExtendWith(MockitoExtension.class)
class EventPublisherTest {

  private EventPublisher eventPublisher;

  @Mock private ApplicationEventPublisher applicationEventPublisher;

  @BeforeEach
  public void setup() {
    eventPublisher = new EventPublisher(applicationEventPublisher);
  }

  @Test
  @DisplayName("Event published with application event publisher")
  void publishedWithApplicationEventPublisher() {
    var event = new RoomCanceledEvent(randomUUID());

    eventPublisher.publish(event);

    verify(applicationEventPublisher).publishEvent(event);
  }
}
