package com.focusvision.intervu.room.api.draw.api;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_DRAWING_ENABLED;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.RoomParticipantDto;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

/**
 * * IT tests for {@link DrawingController#enableDrawingForAll}}.
 */
@DisplayName("Enable drawing for participant in room")
class DrawingControllerEnableDrawingForAllControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .put(url(ROOM_PARTICIPANTS_DRAW_ENABLE))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participant")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANTS_DRAW_ENABLE))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for non moderators")
    void forbiddenForNonModerators() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(ROOM_PARTICIPANTS_DRAW_ENABLE))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .put(url(ROOM_PARTICIPANTS_DRAW_ENABLE))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(ROOM_PARTICIPANTS_DRAW_ENABLE))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("All")
    void all() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        var stimulus = researchSession.getStimuli().get(0);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_STIMULUS_ACTIVATE), stimulus.getId())
                .then()
                .statusCode(OK.value());

        var moderatorSession = connectRoomWs(moderator);
        subscribe(moderatorSession, roomChannel, moderatorHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("participantState.drawingEnabled", equalTo(false))
                .root("roomState.participants")
                .body("drawingEnabled", hasItems(false,false,false,false,false,false));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANTS_DRAW_ENABLE))
                .then()
                .statusCode(HttpStatus.OK.value());

        waitForWs();
        var states = moderatorHandler.getStates().stream()
                .filter(genericRoomStateEvent -> genericRoomStateEvent.getEventType().equals(PARTICIPANT_DRAWING_ENABLED))
                .toList();

        assertAll(
                () -> assertThat(states.size()).isEqualTo(1),
                () -> assertThat(states.get(0).getDestination()).isEqualTo(roomChannel),
                () -> assertThat(states.get(0).getData().getParticipants())
                        .filteredOn(roomParticipantDto -> roomParticipantDto.getId().equals(moderator.getId()))
                        .extracting(RoomParticipantDto::isDrawingEnabled).contains(true,true,true,true,true,true),
                () -> assertThat(states.get(0).getEventType()).isEqualTo(PARTICIPANT_DRAWING_ENABLED)
        );
    }
}
