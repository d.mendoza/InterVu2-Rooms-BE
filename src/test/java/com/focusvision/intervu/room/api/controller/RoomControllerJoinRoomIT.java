package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantStateStompFrameHandler;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.model.event.ParticipantStateEvent;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.concurrent.CompletableFuture;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_ONLINE;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_READY;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link RoomController#join}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Join room")
class RoomControllerJoinRoomIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(ROOM_JOIN_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participant")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_JOIN_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Respondent joined")
    void respondentJoined() {
        var roomResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var participantResultKeeper = new CompletableFuture<ParticipantStateEvent>();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);

        var state = given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(ROOM_INFO_URL))
                .then()
                 .statusCode(OK.value())
                .body("roomState.state", equalTo(SCHEDULED.name()))
                .body("participantState.ready", equalTo(false))
                .extract().body().as(ParticipantRoomStateDto.class);

        var roomHandler = new RoomStateStompFrameHandler(mapper, roomResultKeeper::complete);
        var participantHandler = new ParticipantStateStompFrameHandler(mapper, participantResultKeeper::complete);
        var session = connectRoomWs(respondent);
        subscribe(session, state.getParticipantState().getChannel().getRoom(), roomHandler);
        subscribe(session, state.getParticipantState().getChannel().getParticipant(), participantHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(ROOM_JOIN_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.state", equalTo(SCHEDULED.name()))
                .body("participantState.ready", equalTo(true))
                .extract().body().as(ParticipantRoomStateDto.class);

        assertThat(roomHandler.getStates()).hasSize(2);
        assertThat(participantHandler.getStates()).hasSize(1);
        assertThat(roomHandler.getStates().get(0).getEventType()).isEqualByComparingTo(PARTICIPANT_ONLINE);
        assertThat(roomHandler.getStates().get(1).getEventType()).isEqualByComparingTo(PARTICIPANT_READY);
        assertThat(participantHandler.getStates().get(0).getData().isReady()).isTrue();
    }

}
