package com.focusvision.intervu.room.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
public class WsTestUtils {

    private static final ObjectMapper mapper = new ObjectMapper();

    public WebSocketStompClient createWebSocketClient(MessageConverter messageConverter) {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(messageConverter);
        return stompClient;
    }

    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }

    public static class MyStompSessionHandler extends StompSessionHandlerAdapter {
        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            log.info("Stomp client is connected");
            super.afterConnected(session, connectedHeaders);
        }

        @Override
        public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
            log.info("Exception: " + exception);
            super.handleException(session, command, headers, payload, exception);
        }
    }

    public static class OutgoingChatMessageStompFrameHandler implements StompFrameHandler {

        private final Consumer<OutgoingChatMessage> frameHandler;

        public OutgoingChatMessageStompFrameHandler(Consumer<OutgoingChatMessage> frameHandler) {
            this.frameHandler = frameHandler;
        }

        @Override
        public Type getPayloadType(StompHeaders headers) {
            return OutgoingChatMessage.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
            log.info("received message: {} with headers: {}", payload, headers);
            frameHandler.accept(mapper.convertValue(payload, OutgoingChatMessage.class));
        }
    }
}
