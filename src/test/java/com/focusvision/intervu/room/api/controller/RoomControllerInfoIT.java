package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link RoomController#info}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Room info")
class RoomControllerInfoIT extends HttpBasedIT {

  @Test
  @DisplayName("Forbidden for unauthorized")
  void forbiddenUnauthorized() {
    given()
        .when()
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Unauthorized, banned participant")
  void unauthorizedBanned() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    participantRepository.save(moderator.setBanned(true));

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("OK for respondent")
  void okForRespondent() {
    var researchSession = setUpFullRoom();
    var respondent = getRespondent(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(respondent))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("roomState.size()", is(17))
        .body("roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("roomState.version", notNullValue())
        .body("roomState.roomName", equalTo(researchSession.getName()))
        .body("roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("roomState.state", equalTo(researchSession.getState().name()))
        .body("roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("roomState.stimulusInFocus", equalTo(false))
        .body("roomState.drawingInSync", equalTo(false))
        .body("roomState.respondentsChatEnabled", equalTo(false))
        .body("roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("roomState.projectType",
            equalTo(researchSession.getProject().getType().name()))
        .body("roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("roomState.participants.size()", is(0))
        .body("roomState.activeStimulus", nullValue())
        .body("participantState.size()", is(16))
        .body("participantState.admin", equalTo(respondent.isAdmin()))
        .body("participantState.moderator", equalTo(respondent.isModerator()))
        .body("participantState.banable", equalTo(respondent.isBanable()))
        .body("participantState.guest", equalTo(respondent.isGuest()))
        .body("participantState.projectManager", equalTo(respondent.isProjectManager()))
        .body("participantState.projectManager", equalTo(respondent.isProjectOperator()))
        .body("participantState.participantId", equalTo(respondent.getId().toString()))
        .body("participantState.participantPlatformId", equalTo(respondent.getPlatformId()))
        .body("participantState.displayName", equalTo(respondent.getDisplayName()))
        .body("participantState.ready", equalTo(false))
        .body("participantState.banned", equalTo(respondent.isBanned()))
        .body("participantState.drawingEnabled", equalTo(false))
        .body("participantState.role", equalTo(respondent.getRole().name()))
        .body("participantState.conference", nullValue())
        .body("participantState.channel.size()", is(6))
        .body("participantState.channel.room",
            equalTo("/topic/room." + respondent.getResearchSession().getId().toString()))
        .body("participantState.channel.draw",
            equalTo("/topic/draw." + respondent.getResearchSession().getId().toString()))
        .body("participantState.channel.participant",
            equalTo("/topic/participant." + respondent.getId().toString()))
        .body("participantState.channel.stimulus",
            equalTo("/topic/stimulus." + respondent.getResearchSession().getId().toString()))
        .body("participantState.channel.globalStimulus",
            equalTo("/topic/global-stimulus." + respondent.getResearchSession().getId().toString()))
        .body("participantState.permissions.size()", is(7))
        .body("participantState.permissions",
            hasItems("ROOM_READ",
                "SCROLL_READ",
                "PARTICIPANT_TESTING_REPORT",
                "PARTICIPANT_TESTING_READ",
                "ROOM_JOIN",
                "STIMULI_READ",
                "DRAWING_READ"))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        .body("dialInInfo.operatorPin", nullValue())
        .body("dialInInfo.listenerPin", nullValue())
        .body("dialInInfo.speakerPin", equalTo(respondent.getSpeakerPin()));
  }

  @Test
  @DisplayName("OK for moderator")
  void okForModerator() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("roomState.size()", is(17))
        .body("roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("roomState.version", notNullValue())
        .body("roomState.roomName", equalTo(researchSession.getName()))
        .body("roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("roomState.state", equalTo(researchSession.getState().name()))
        .body("roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("roomState.stimulusInFocus", equalTo(false))
        .body("roomState.drawingInSync", equalTo(false))
        .body("roomState.respondentsChatEnabled", equalTo(false))
        .body("roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("roomState.projectType",
            equalTo(researchSession.getProject().getType().name()))
        .body("roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("roomState.participants.size()", is(0))
        .body("roomState.activeStimulus", nullValue())
        .body("participantState.size()", is(16))
        .body("participantState.admin", equalTo(moderator.isAdmin()))
        .body("participantState.moderator", equalTo(moderator.isModerator()))
        .body("participantState.banable", equalTo(moderator.isBanable()))
        .body("participantState.guest", equalTo(moderator.isGuest()))
        .body("participantState.projectManager", equalTo(moderator.isProjectManager()))
        .body("participantState.projectOperator", equalTo(moderator.isProjectOperator()))
        .body("participantState.participantId", equalTo(moderator.getId().toString()))
        .body("participantState.participantPlatformId", equalTo(moderator.getPlatformId()))
        .body("participantState.displayName", equalTo(moderator.getDisplayName()))
        .body("participantState.ready", equalTo(false))
        .body("participantState.banned", equalTo(moderator.isBanned()))
        .body("participantState.drawingEnabled", equalTo(false))
        .body("participantState.role", equalTo(moderator.getRole().name()))
        .body("participantState.conference", nullValue())
        .body("participantState.channel.size()", is(6))
        .body("participantState.channel.room",
            equalTo("/topic/room." + moderator.getResearchSession().getId().toString()))
        .body("participantState.channel.draw",
            equalTo("/topic/draw." + moderator.getResearchSession().getId().toString()))
        .body("participantState.channel.participant",
            equalTo("/topic/participant." + moderator.getId().toString()))
        .body("participantState.channel.stimulus",
            equalTo("/topic/stimulus." + moderator.getResearchSession().getId().toString()))
        .body("participantState.channel.globalStimulus",
            equalTo("/topic/global-stimulus." + moderator.getResearchSession().getId().toString()))
        .body("participantState.permissions.size()", is(18))
        .body("participantState.permissions",
            hasItems("STIMULI_READ",
                "DRAWING_READ",
                "PARTICIPANT_CONTROL",
                "PARTICIPANT_TESTING_REPORT",
                "PIN_CODES_READ",
                "STIMULI_STATE_READ",
                "ROOM_JOIN",
                "ROOM_READ",
                "STIMULI_CONTROL",
                "DRAWING_CONTROL",
                "STIMULI_SNAPSHOT_ADD",
                "BOOKMARK_REGULAR_ADD",
                "PARTICIPANT_TESTING_READ",
                "SCROLL_READ",
                "BOOKMARK_READ",
                "ROOM_CONTROL",
                "BOOKMARK_PII_ADD",
                "ROLE_CONTROL"))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        /* This moderator is marked as project manager and project operator as well,
        thus they can see all session PINs. */
        .body("dialInInfo.operatorPin", equalTo(researchSession.getOperatorPin()))
        .body("dialInInfo.listenerPin", equalTo(researchSession.getListenerPin()))
        .body("dialInInfo.speakerPin", equalTo(researchSession.getSpeakerPin()));
  }

  @Test
  @DisplayName("OK for observer")
  void okForObserver() {
    var researchSession = setUpFullRoom();
    var observer = getObserver(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(observer))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("roomState.size()", is(17))
        .body("roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("roomState.version", notNullValue())
        .body("roomState.roomName", equalTo(researchSession.getName()))
        .body("roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("roomState.state", equalTo(researchSession.getState().name()))
        .body("roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("roomState.stimulusInFocus", equalTo(false))
        .body("roomState.drawingInSync", equalTo(false))
        .body("roomState.respondentsChatEnabled", equalTo(false))
        .body("roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("roomState.projectType",
            equalTo(researchSession.getProject().getType().name()))
        .body("roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("roomState.participants.size()", is(0))
        .body("roomState.activeStimulus", nullValue())
        .body("participantState.size()", is(16))
        .body("participantState.admin", equalTo(observer.isAdmin()))
        .body("participantState.moderator", equalTo(observer.isModerator()))
        .body("participantState.banable", equalTo(observer.isBanable()))
        .body("participantState.guest", equalTo(observer.isGuest()))
        .body("participantState.projectManager", equalTo(observer.isProjectManager()))
        .body("participantState.projectOperator", equalTo(observer.isProjectOperator()))
        .body("participantState.participantId", equalTo(observer.getId().toString()))
        .body("participantState.participantPlatformId", equalTo(observer.getPlatformId()))
        .body("participantState.displayName", equalTo(observer.getDisplayName()))
        .body("participantState.ready", equalTo(false))
        .body("participantState.banned", equalTo(observer.isBanned()))
        .body("participantState.drawingEnabled", equalTo(false))
        .body("participantState.role", equalTo(observer.getRole().name()))
        .body("participantState.conference", nullValue())
        .body("participantState.channel.size()", is(6))
        .body("participantState.channel.room",
            equalTo("/topic/room." + observer.getResearchSession().getId().toString()))
        .body("participantState.channel.draw",
            equalTo("/topic/draw." + observer.getResearchSession().getId().toString()))
        .body("participantState.channel.participant",
            equalTo("/topic/participant." + observer.getId().toString()))
        .body("participantState.channel.stimulus",
            equalTo("/topic/stimulus." + observer.getResearchSession().getId().toString()))
        .body("participantState.channel.globalStimulus",
            equalTo("/topic/global-stimulus." + observer.getResearchSession().getId().toString()))
        .body("participantState.permissions.size()", is(12))
        .body("participantState.permissions",
            hasItems("SCROLL_READ",
                "BOOKMARK_PII_ADD",
                "ROOM_JOIN",
                "BOOKMARK_READ",
                "DRAWING_READ",
                "PARTICIPANT_TESTING_READ",
                "BOOKMARK_REGULAR_ADD",
                "STIMULI_STATE_READ",
                "PARTICIPANT_TESTING_REPORT",
                "STIMULI_READ",
                "ROOM_READ",
                "STIMULI_SNAPSHOT_ADD"))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        .body("dialInInfo.operatorPin", nullValue())
        .body("dialInInfo.listenerPin", equalTo(researchSession.getListenerPin()))
        .body("dialInInfo.speakerPin", nullValue());
  }

  @Test
  @DisplayName("OK for translator")
  void okForTranslator() {
    var researchSession = setUpFullRoom();
    var translator = getTranslator(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(translator))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("roomState.size()", is(17))
        .body("roomState.roomId", equalTo(researchSession.getId().toString()))
        .body("roomState.version", notNullValue())
        .body("roomState.roomName", equalTo(researchSession.getName()))
        .body("roomState.projectName", equalTo(researchSession.getProject().getName()))
        .body("roomState.state", equalTo(researchSession.getState().name()))
        .body("roomState.startsAt", equalTo(researchSession.getStartsAt().toString()))
        .body("roomState.endsAt", equalTo(researchSession.getEndsAt().toString()))
        .body("roomState.useWebcams", equalTo(researchSession.isUseWebcams()))
        .body("roomState.privacy", equalTo(researchSession.isPrivacy()))
        .body("roomState.stimulusInFocus", equalTo(false))
        .body("roomState.drawingInSync", equalTo(false))
        .body("roomState.respondentsChatEnabled", equalTo(false))
        .body("roomState.projectNumber",
            equalTo(researchSession.getProject().getProjectNumber()))
        .body("roomState.projectType",
            equalTo(researchSession.getProject().getType().name()))
        .body("roomState.audioChannel", equalTo(researchSession.getAudioChannel().name()))
        .body("roomState.participants.size()", is(0))
        .body("roomState.activeStimulus", nullValue())
        .body("participantState.size()", is(16))
        .body("participantState.admin", equalTo(translator.isAdmin()))
        .body("participantState.moderator", equalTo(translator.isModerator()))
        .body("participantState.banable", equalTo(translator.isBanable()))
        .body("participantState.guest", equalTo(translator.isGuest()))
        .body("participantState.projectManager", equalTo(translator.isProjectManager()))
        .body("participantState.projectOperator", equalTo(translator.isProjectOperator()))
        .body("participantState.participantId", equalTo(translator.getId().toString()))
        .body("participantState.participantPlatformId", equalTo(translator.getPlatformId()))
        .body("participantState.displayName", equalTo(translator.getDisplayName()))
        .body("participantState.ready", equalTo(false))
        .body("participantState.banned", equalTo(translator.isBanned()))
        .body("participantState.drawingEnabled", equalTo(false))
        .body("participantState.role", equalTo(translator.getRole().name()))
        .body("participantState.conference", nullValue())
        .body("participantState.channel.size()", is(6))
        .body("participantState.channel.room",
            equalTo("/topic/room." + translator.getResearchSession().getId().toString()))
        .body("participantState.channel.draw",
            equalTo("/topic/draw." + translator.getResearchSession().getId().toString()))
        .body("participantState.channel.participant",
            equalTo("/topic/participant." + translator.getId().toString()))
        .body("participantState.channel.stimulus",
            equalTo("/topic/stimulus." + translator.getResearchSession().getId().toString()))
        .body("participantState.channel.globalStimulus",
            equalTo(
                "/topic/global-stimulus." + translator.getResearchSession().getId().toString()))
        .body("participantState.permissions.size()", is(11))
        .body("participantState.permissions",
            hasItems("STIMULI_READ",
                "BOOKMARK_PII_ADD",
                "BOOKMARK_REGULAR_ADD",
                "SCROLL_READ",
                "PARTICIPANT_TESTING_REPORT",
                "ROOM_JOIN",
                "STIMULI_STATE_READ",
                "BOOKMARK_READ",
                "ROOM_READ",
                "DRAWING_READ",
                "PARTICIPANT_TESTING_READ"))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        .body("dialInInfo.operatorPin", nullValue())
        .body("dialInInfo.listenerPin", equalTo(researchSession.getListenerPin()))
        .body("dialInInfo.speakerPin", nullValue());
  }

  @Test
  @DisplayName("OK for project manager")
  void okForProjectManager() {
    var participant = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(OBSERVER)
        .setAdmin(false)
        .setBanable(true)
        .setGuest(false)
        .setProjectManager(true)
        .setProjectOperator(false)
        .setModerator(true);
    var researchSession = setUpRoom(participant);
    var projectManager = getParticipant(researchSession, OBSERVER);

    given()
        .when()
        .headers(roomParticipantHeaders(projectManager))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        .body("dialInInfo.operatorPin", equalTo(researchSession.getOperatorPin()))
        .body("dialInInfo.listenerPin", equalTo(researchSession.getListenerPin()))
        .body("dialInInfo.speakerPin", equalTo(researchSession.getSpeakerPin()));
  }

  @Test
  @DisplayName("OK for project operator")
  void okForProjectOperator() {
    var participant = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(OBSERVER)
        .setAdmin(false)
        .setBanable(true)
        .setGuest(false)
        .setProjectManager(false)
        .setProjectOperator(true)
        .setModerator(true);
    var researchSession = setUpRoom(participant);
    var projectOperator = getParticipant(researchSession, OBSERVER);

    given()
        .when()
        .headers(roomParticipantHeaders(projectOperator))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        .body("dialInInfo.operatorPin", equalTo(researchSession.getOperatorPin()))
        .body("dialInInfo.listenerPin", equalTo(researchSession.getListenerPin()))
        .body("dialInInfo.speakerPin", equalTo(researchSession.getSpeakerPin()));
  }

  @Test
  @DisplayName("OK for admin")
  void okForAdmin() {
    var participant = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(OBSERVER)
        .setAdmin(true)
        .setBanable(false)
        .setGuest(false)
        .setProjectManager(false)
        .setProjectOperator(false)
        .setModerator(true);
    var researchSession = setUpRoom(participant);
    var admin = getParticipant(researchSession, OBSERVER);

    given()
        .when()
        .headers(roomParticipantHeaders(admin))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        .body("dialInInfo.operatorPin", equalTo(researchSession.getOperatorPin()))
        .body("dialInInfo.listenerPin", equalTo(researchSession.getListenerPin()))
        .body("dialInInfo.speakerPin", equalTo(researchSession.getSpeakerPin()));
  }

  @Test
  @DisplayName("OK for moderator, non-admin, non-project manager, non-project operator")
  void okForModerator_nonAdmin_nonProjectManager_nonProjectOperator() {
    var participant = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(MODERATOR)
        .setAdmin(false)
        .setBanable(true)
        .setGuest(false)
        .setProjectManager(false)
        .setProjectOperator(false)
        .setModerator(true);
    var researchSession = setUpRoom(participant);
    var moderator = getParticipant(researchSession, MODERATOR);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("dialInInfo.size()", is(4))
        .body("dialInInfo.dialInNumbers.size()", is(1))
        .body("dialInInfo.dialInNumbers[0]", startsWith(DIAL_IN_NUMBER_PREFIX))
        .body("dialInInfo.operatorPin", nullValue())
        .body("dialInInfo.listenerPin", equalTo(researchSession.getListenerPin()))
        .body("dialInInfo.speakerPin", equalTo(researchSession.getSpeakerPin()));
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Forbidden for canceled session")
  void forbiddenForCancelledSession(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var participant = getParticipant(researchSession, role);

    var headers = roomParticipantHeaders(participant);
    given()
        .when()
        .headers(headers)
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value());

    researchSession.setState(ResearchSessionState.CANCELED);
    researchSessionRepository.save(researchSession);

    given()
        .when()
        .headers(headers)
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(FORBIDDEN.value())
        .body("code", equalTo("ROOM_CANCELED"))
        .body("message", equalTo("Room is canceled."));
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Forbidden for finished session")
  void forbiddenForFinishedSession(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var participant = getParticipant(researchSession, role);

    var headers = roomParticipantHeaders(participant);
    given()
        .when()
        .headers(headers)
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value());

    researchSession.setState(ResearchSessionState.FINISHED);
    researchSessionRepository.save(researchSession);

    given()
        .when()
        .headers(headers)
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(FORBIDDEN.value())
        .body("code", equalTo("ROOM_FINISHED"))
        .body("message", equalTo("Room is finished."));
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Forbidden for expired session")
  void forbiddenForExpiredSession(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var participant = getParticipant(researchSession, role);

    var headers = roomParticipantHeaders(participant);
    given()
        .when()
        .headers(headers)
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value());

    researchSession.setState(ResearchSessionState.EXPIRED);
    researchSessionRepository.save(researchSession);

    given()
        .when()
        .headers(headers)
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(FORBIDDEN.value())
        .body("code", equalTo("ROOM_EXPIRED"))
        .body("message", equalTo("Room has expired."));
  }

}
