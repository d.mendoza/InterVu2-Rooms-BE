package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.chat.model.ChatMessageDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.REGULAR;
import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link WaitingRoomChatController#internalChatMessages}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Waiting Room internal chat history")
class WaitingRoomChatControllerInternalChatHistoryIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(WAITING_ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for respondent")
    void forbiddenForRespondent() {
        given()
                .when()
                .headers(roomParticipantHeaders(getRespondent(setUpFullRoom())))
                .get(url(WAITING_ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("OK for backroom users")
    void okForBackroomUsers() throws Exception {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var handle = researchSession.getId();
        var question = make();

        sendWaitingRoomInternalChatMessage(moderator, question);

        List<ChatMessageDto> chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(WAITING_ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(0))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        ChatMessageDto moderatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(moderatorChatMessage.getId()).isNotNull();
        assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId());
        assertThat(moderatorChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(moderatorChatMessage.getHandle()).isEqualTo(handle);
        assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(moderatorChatMessage.getMessage()).isEqualTo(question);
        assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName());
        assertThat(moderatorChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(WAITING_ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        moderatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(moderatorChatMessage.getId()).isNotNull();
        assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId());
        assertThat(moderatorChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(moderatorChatMessage.getHandle()).isEqualTo(handle);
        assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(moderatorChatMessage.getMessage()).isEqualTo(question);
        assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName());
        assertThat(moderatorChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(WAITING_ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        moderatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(moderatorChatMessage.getId()).isNotNull();
        assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId());
        assertThat(moderatorChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(moderatorChatMessage.getHandle()).isEqualTo(handle);
        assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(moderatorChatMessage.getMessage()).isEqualTo(question);
        assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName());
        assertThat(moderatorChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));
    }

}
