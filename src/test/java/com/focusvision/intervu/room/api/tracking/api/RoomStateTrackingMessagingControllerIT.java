package com.focusvision.intervu.room.api.tracking.api;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.tracking.model.RoomStateAckMessage;
import com.focusvision.intervu.room.api.tracking.model.entity.RoomStateAcknowledgement;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class RoomStateTrackingMessagingControllerIT extends HttpBasedIT {

  @Test
  @DisplayName("ACK room state - ok")
  void ackRoomState() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var respondent = getRespondent(researchSession);
    var moderatorSession = connectRoomWs(moderator);
    var respondentSession = connectRoomWs(respondent);
    connectToRoom(researchSession.getId(), moderator);
    connectToRoom(researchSession.getId(), respondent);
    var respondentRoomState =
        given()
            .when()
            .headers(roomParticipantHeaders(respondent))
            .get(url(ROOM_INFO_URL))
            .then()
            .statusCode(OK.value())
            .extract()
            .body()
            .as(ParticipantRoomStateDto.class);
    var moderatorRoomState =
        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .get(url(ROOM_INFO_URL))
            .then()
            .statusCode(OK.value())
            .extract()
            .body()
            .as(ParticipantRoomStateDto.class);

    moderatorSession.send(
        parseDestination(ACK_ROOM_STATE_DESTINATION), new RoomStateAckMessage(1L));
    respondentSession.send(
        parseDestination(ACK_ROOM_STATE_DESTINATION), new RoomStateAckMessage(2L));
    waitForWs();

    List<RoomStateAcknowledgement> moderatorAckList =
        roomStateAcknowledgementRepository.findAll().stream()
            .filter(e -> e.getParticipantId().equals(moderator.getId()))
            .toList();
    List<RoomStateAcknowledgement> respondentAckList =
        roomStateAcknowledgementRepository.findAll().stream()
            .filter(e -> e.getParticipantId().equals(respondent.getId()))
            .toList();

    assertThat(moderatorAckList).hasSize(1);
    assertThat(moderatorAckList.get(0).getVersion()).isEqualTo(1L);
    assertThat(respondentAckList).hasSize(1);
    assertThat(respondentAckList.get(0).getVersion()).isEqualTo(2L);
  }
}
