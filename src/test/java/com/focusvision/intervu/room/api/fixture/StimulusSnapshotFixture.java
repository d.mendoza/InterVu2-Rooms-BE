package com.focusvision.intervu.room.api.fixture;

import static net.bytebuddy.utility.RandomString.make;

import com.focusvision.intervu.room.api.stimulus.snapshot.AddStimulusSnapshotDto;
import java.io.IOException;
import java.time.Instant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class StimulusSnapshotFixture {

  @Value("classpath:assets/forsta.png")
  private Resource forstaLogoResource;

  public AddStimulusSnapshotDto addStimulusSnapshotDto() {
    return new AddStimulusSnapshotDto()
        .setName(make())
        .setData(getForstaLogoData())
        .setMimeType(make())
        .setTimestamp(Instant.now().toEpochMilli());
  }

  private byte[] getForstaLogoData() {
    try {
      return forstaLogoResource.getInputStream().readAllBytes();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
