package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.HttpBasedIT;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static com.focusvision.intervu.room.api.chat.model.ChatType.PRIVATE;
import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.common.model.ChatType.RESPONDENTS;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link MeetingRoomChatController#chatMetadata}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Meeting Room chat metadata")
class MeetingRoomChatControllerMetadataIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(MEETING_ROOM_CHAT_METADATA_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for InterVu users")
    void forbiddenIntervuUsers() {
        given()
                .when()
                .headers(intervuUserHeaders(getModerator(setUpFullRoom())))
                .get(url(MEETING_ROOM_CHAT_METADATA_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for banned participants")
    void forbiddenForBannedParticipants() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(MEETING_ROOM_CHAT_METADATA_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Respondent gets metadata")
    void respondentGetsMetadata() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var handle = researchSession.getId().toString();
        connectAllToRoom(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(MEETING_ROOM_CHAT_METADATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal", nullValue())
                .body("respondents.size()", is(8))
                .body("respondents.handle", equalTo(handle))
                .body("respondents.type", equalTo(RESPONDENTS.name()))
                .body("respondents.readOnly", equalTo(false))
                .body("respondents.enabled", equalTo(false))
                .body("respondents.control", nullValue())
                .body("respondents.destination", equalTo("/app/meeting-room/chat/respondents"))
                .body("respondents.destinationForSeen", equalTo("/app/meeting-room/chat/respondents/seen"))
                .body("respondents.messagesUrl", equalTo("/meeting-room/chat/respondents"))
                .body("direct.size()", is(2))
                .body("direct[0].size()", is(9))
                .body("direct[0].handle", notNullValue())
                .body("direct[0].type", equalTo(PRIVATE.name()))
                .body("direct[0].readOnly", equalTo(false))
                .body("direct[0].enabled", equalTo(true))
                .body("direct[0].control", nullValue())
                .body("direct[0].destination", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/seen"))
                .body("direct[0].messagesUrl", containsString("/meeting-room/chat/private/"))
                .body("direct[0].participant", notNullValue())
                .body("direct[0].participant.role", equalTo(MODERATOR.name()))
                .body("direct[1].participant.role", equalTo(MODERATOR.name()))
                .body("direct[1].enabled", equalTo(true))
                .body("direct[1].control", nullValue());

    }

    @Test
    @DisplayName("Moderator gets metadata")
    void moderatorGetsMetadata() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var handle = researchSession.getId().toString();
        connectAllToRoom(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(MEETING_ROOM_CHAT_METADATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal.size()", is(8))
                .body("internal.handle", equalTo(handle))
                .body("internal.type", equalTo(INTERNAL.name()))
                .body("internal.readOnly", equalTo(false))
                .body("internal.enabled", equalTo(true))
                .body("internal.control", nullValue())
                .body("internal.destination", equalTo("/app/meeting-room/chat/internal"))
                .body("internal.destinationForSeen", equalTo("/app/meeting-room/chat/internal/seen"))
                .body("internal.messagesUrl", equalTo("/meeting-room/chat/internal"))
                .body("respondents.size()", is(8))
                .body("respondents.handle", equalTo(handle))
                .body("respondents.type", equalTo(RESPONDENTS.name()))
                .body("respondents.readOnly", equalTo(false))
                .body("respondents.enabled", equalTo(false))
                .body("respondents.control", notNullValue())
                .body("respondents.control.size()", is(2))
                .body("respondents.control.enable.size()", is(2))
                .body("respondents.control.enable.method", equalTo("PUT"))
                .body("respondents.control.enable.url", equalTo("/meeting-room/chat/respondents/control/enable"))
                .body("respondents.control.disable.size()", is(2))
                .body("respondents.control.disable.method", equalTo("PUT"))
                .body("respondents.control.disable.url", equalTo("/meeting-room/chat/respondents/control/disable"))
                .body("respondents.destination", equalTo("/app/meeting-room/chat/respondents"))
                .body("respondents.destinationForSeen", equalTo("/app/meeting-room/chat/respondents/seen"))
                .body("respondents.messagesUrl", equalTo("/meeting-room/chat/respondents"))
                .body("direct.size()", is(4))
                .body("direct[0].size()", is(9))
                .body("direct[0].handle", notNullValue())
                .body("direct[0].type", equalTo(PRIVATE.name()))
                .body("direct[0].readOnly", equalTo(false))
                .body("direct[0].enabled", equalTo(true))
                .body("direct[0].control", nullValue())
                .body("direct[0].destination", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/seen"))
                .body("direct[0].messagesUrl", containsString("/meeting-room/chat/private/"))
                .body("direct[0].participant", notNullValue());
    }

    @Test
    @DisplayName("Translator gets metadata")
    void translatorGetsMetadata() {
        var researchSession = setUpFullRoom();
        var translator = getTranslator(researchSession);
        var handle = researchSession.getId().toString();
        connectAllToRoom(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(MEETING_ROOM_CHAT_METADATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal.size()", is(8))
                .body("internal.handle", equalTo(handle))
                .body("internal.type", equalTo(INTERNAL.name()))
                .body("internal.readOnly", equalTo(false))
                .body("internal.destination", equalTo("/app/meeting-room/chat/internal"))
                .body("internal.destinationForSeen", equalTo("/app/meeting-room/chat/internal/seen"))
                .body("internal.messagesUrl", equalTo("/meeting-room/chat/internal"))
                .body("respondents", nullValue())
                .body("direct.size()", is(3))
                .body("direct[0].size()", is(9))
                .body("direct[0].handle", notNullValue())
                .body("direct[0].type", equalTo(PRIVATE.name()))
                .body("direct[0].readOnly", equalTo(false))
                .body("direct[0].enabled", equalTo(true))
                .body("direct[0].control", nullValue())
                .body("direct[0].destination", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/seen"))
                .body("direct[0].messagesUrl", containsString("/meeting-room/chat/private/"))
                .body("direct[0].participant", notNullValue());
    }

    @Test
    @DisplayName("Observer gets metadata")
    void observerGetsMetadata() {
        var researchSession = setUpFullRoom();
        var observer = getObserver(researchSession);
        var handle = researchSession.getId().toString();
        connectAllToRoom(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(MEETING_ROOM_CHAT_METADATA_URL))
                .then()
                .and().statusCode(OK.value())
                .body("size()", is(3))
                .body("internal.size()", is(8))
                .body("internal.handle", equalTo(handle))
                .body("internal.type", equalTo(INTERNAL.name()))
                .body("internal.readOnly", equalTo(false))
                .body("internal.enabled", equalTo(true))
                .body("internal.control", nullValue())
                .body("internal.destination", equalTo("/app/meeting-room/chat/internal"))
                .body("internal.destinationForSeen", equalTo("/app/meeting-room/chat/internal/seen"))
                .body("internal.messagesUrl", equalTo("/meeting-room/chat/internal"))
                .body("respondents", nullValue())
                .body("direct.size()", is(3))
                .body("direct[0].size()", is(9))
                .body("direct[0].handle", notNullValue())
                .body("direct[0].type", equalTo(PRIVATE.name()))
                .body("direct[0].readOnly", equalTo(false))
                .body("direct[0].enabled", equalTo(true))
                .body("direct[0].control", nullValue())
                .body("direct[0].destination", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/app/meeting-room/chat/private/"))
                .body("direct[0].destinationForSeen", containsString("/seen"))
                .body("direct[0].messagesUrl", containsString("/meeting-room/chat/private/"))
                .body("direct[0].participant", notNullValue());
    }

}
