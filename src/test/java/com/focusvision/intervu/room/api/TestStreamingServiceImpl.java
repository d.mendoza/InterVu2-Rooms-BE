package com.focusvision.intervu.room.api;

import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.List.of;
import static net.bytebuddy.utility.RandomString.make;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.ScreenArea;
import com.focusvision.intervu.room.api.model.ScreenshareBounds;
import com.focusvision.intervu.room.api.model.streaming.GroupRoom;
import com.focusvision.intervu.room.api.model.streaming.GroupRoomParticipant;
import com.focusvision.intervu.room.api.model.streaming.GroupRoomRecording;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.focusvision.intervu.room.api.service.StreamingService;
import com.twilio.rest.video.v1.room.Participant;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
public class TestStreamingServiceImpl implements StreamingService {

  @Setter
  private boolean recordingError = false;

  @Override
  public GroupRoom createGroupRoom(UUID id) {
    return new GroupRoom()
        .setSid(make())
        .setDateCreatedMillisGmt(now().plus(1, HOURS).plus(2, SECONDS).toEpochMilli())
        .setDuration(20000);
  }

  @Override
  public GroupRoom getGroupRoom(String roomSid) {
    return new GroupRoom()
        .setSid(roomSid)
        .setDateCreatedMillisGmt(now().plus(1, HOURS).plus(2, SECONDS).toEpochMilli())
        .setDuration(20000);
  }

  @Override
  public void completeGroupRoom(String roomSid) {

  }

  @Override
  public GroupRoomParticipant createGroupRoomParticipant(UUID participantId, String roomSid) {
    return new GroupRoomParticipant(make(), make());
  }

  @Override
  public void kickParticipantFromRoom(String roomSid, UUID participantId) {

  }

  @Override
  public GroupRoomRecording createGroupRoomRecording(String roomSid,
                                                     boolean isPrivacyOn,
                                                     CompositionLayout layout,
                                                     AudioChannel audioChannel) {
    var hasScreenshare = PICTURE_IN_PICTURE_WITH_COLUMN.equals(layout);
    var screenshareBounds = !hasScreenshare ? null
        : ScreenshareBounds.builder()
            .screenshareArea(ScreenArea.builder()
                .offsetX(0L)
                .offsetY(0L)
                .width(1280L)
                .height(720L)
                .build())
            .build();
    return new GroupRoomRecording()
        .setSid(roomSid)
        .setProcessed(false)
        .setDuration(0)
        .setScreenshareBounds(screenshareBounds);
  }

  @Override
  public GroupRoomRecording getGroupRoomRecording(String recordingSid) throws IntervuRoomException {
    return new GroupRoomRecording()
        .setSid(recordingSid)
        .setProcessed(true)
        .setDuration(18000)
        .setError(recordingError)
        .setScreenshareBounds(ScreenshareBounds.builder()
            .screenshareArea(ScreenArea.builder()
                .offsetX(0L)
                .offsetY(0L)
                .width(1280L)
                .height(720L)
                .build())
            .build());
  }

  @Override
  public String getGroupRoomRecordingLink(String recordingSid) throws IntervuRoomException {
    return make();
  }

  @Override
  public void deleteGroupRoomRecordings(String roomSid) {

  }

  @Override
  public List<ParticipantGroupRoomRecording> getParticipantsRecordings(String roomSid) {
    return of();
  }

  @Override
  public List<ParticipantGroupRoomRecording> getParticipantRecordings(String roomSid,
                                                                      UUID participantId) {
    return of();
  }

  @Override
  public Set<Participant> getRoomParticipants(String roomSid) {
    return Set.of();
  }

}
