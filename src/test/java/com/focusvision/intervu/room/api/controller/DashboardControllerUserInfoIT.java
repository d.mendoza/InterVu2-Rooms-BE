package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.configuration.domain.JwtProperties;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

/**
 * IT tests for {@link DashboardController#dashboard}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Dashboard user info")
class DashboardControllerUserInfoIT extends HttpBasedIT {

    @Autowired
    private JwtProperties jwtProperties;

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(DASHBOARD_USER_INFO_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for room participants")
    void forbiddenRoomParticipants() {
        given()
                .when()
                .headers(roomParticipantHeaders(getRespondent(setUpFullRoom())))
                .get(url(DASHBOARD_USER_INFO_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("User info fetched")
    void ok() {
        String platformId = make();
        String email = make();
        String firstName = make();
        String lastName = make();
        Map<String, Object> claims = new HashMap<>();
        claims.put("platformId", platformId);
        claims.put("email", email);
        claims.put("firstName", firstName);
        claims.put("lastName", lastName);

        var token = Jwts.builder()
                .setSubject(email)
                .addClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(new Date(
                        now().plusMillis(jwtProperties.getTokenExpirationHours() * 60 * 60 * 1000).toEpochMilli()))
                .signWith(SignatureAlgorithm.HS512, jwtProperties.getTokenSecret())
                .compact();

        given()
                .when()
                .headers("Authorization", "Bearer " + token)
                .get(url(DASHBOARD_USER_INFO_URL))
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("size()", is(5))
                .body("platformId", equalTo(platformId))
                .body("firstName", equalTo(firstName))
                .body("lastName", equalTo(lastName))
                .body("email", equalTo(email))
                .body("channel", equalTo("/topic/participant." + platformId));
    }
}
