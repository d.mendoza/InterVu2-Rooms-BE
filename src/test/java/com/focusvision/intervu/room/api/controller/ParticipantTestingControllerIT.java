package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingResult;
import com.focusvision.intervu.room.api.participant.testing.api.ParticipantTestingController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * IT tests for {@link ParticipantTestingController}.
 */
class ParticipantTestingControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Participant testing failure - forbidden for unauthorized")
    void participantTestingFailure_unauthorized() {
        given()
                .when()
                .get(url(PARTICIPANTS_TESTING_FAILURE_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Participant testing failure - ok")
    void participantTestingFailure_ok() throws InterruptedException {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);

        var consumer = respondentTestingResultsDataConsumer(1);
        given()
                .headers(roomParticipantHeadersWithContent(respondent))
                .when()
                .post(url(PARTICIPANTS_TESTING_FAILURE_URL))
                .then()
                .statusCode(HttpStatus.OK.value());
        assertThat(consumer.allMessagesReceived(2, TimeUnit.SECONDS)).isTrue();
        var message = consumer.getMessage(respondent).orElseThrow();
        assertAll(
                () -> assertThat(message).extracting(ParticipantTestingResult::getSessionId)
                        .isEqualTo(respondent.getResearchSession().getPlatformId()),
                () -> assertThat(message).extracting(ParticipantTestingResult::getRespondentId)
                        .isEqualTo(respondent.getPlatformId()),
                () -> assertFalse(message.isSuccess())
        );
        consumer.stop();
    }

    @Test
    @DisplayName("Participant testing success - forbidden for unauthorized")
    void participantTestingFSuccess_unauthorized() {
        given()
                .when()
                .get(url(PARTICIPANTS_TESTING_PASS_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Participant testing success - ok")
    void participantTestingSuccess_ok() throws InterruptedException {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);

        var consumer = respondentTestingResultsDataConsumer(1);
        given()
                .headers(roomParticipantHeadersWithContent(respondent))
                .when()
                .post(url(PARTICIPANTS_TESTING_PASS_URL))
                .then()
                .statusCode(HttpStatus.OK.value());
        assertThat(consumer.allMessagesReceived(2, TimeUnit.SECONDS)).isTrue();
        var message = consumer.getMessage(respondent);
        assertAll(
                () -> assertThat(message).isPresent(),
                () -> assertThat(message).get().extracting(ParticipantTestingResult::getSessionId)
                        .isEqualTo(respondent.getResearchSession().getPlatformId()),
                () -> assertThat(message).get().extracting(ParticipantTestingResult::getRespondentId)
                        .isEqualTo(respondent.getPlatformId())
        );
        consumer.stop();
    }

    @Test
    @DisplayName("Research session info - forbidden for unauthorized")
    void researchSessionInfo_unauthorized() {
        given()
                .when()
                .get(url(PARTICIPANTS_TESTING_SESSION_INFO_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Research session info - ok")
    void researchSessionInfo_ok() {
        var room = setUpFullRoom();
        var respondent = getRespondent(room);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(PARTICIPANTS_TESTING_SESSION_INFO_URL))
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("useWebcams", equalTo(room.isUseWebcams()));
    }

}
