package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ProjectType;
import com.focusvision.intervu.room.api.exception.ErrorCode;
import com.focusvision.intervu.room.api.exception.StreamingApiException;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.service.StreamingService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import static com.focusvision.intervu.room.api.common.model.ProjectType.FULL_SERVICE;
import static com.focusvision.intervu.room.api.common.model.ProjectType.SELF_SERVICE;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 * IT tests for {@link RoomController#start}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Start room")
@DirtiesContext
class RoomControllerStartRoomIT extends HttpBasedIT {

    @SpyBean
    protected StreamingService streamingService;

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participant")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Start room - streaming api error")
    void startRoom_streamingApiError() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var mockException = mock(StreamingApiException.class);
        when(streamingService.createGroupRoom(moderator.getResearchSession().getId())).thenThrow(mockException);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(SERVICE_UNAVAILABLE.value())
                .body("code", equalTo(ErrorCode.STREAMING_API_ERROR.name()));
    }

    @ParameterizedTest
    @EnumSource(ProjectType.class)
    @DisplayName("Forbidden for non moderators")
    void forbiddenForNonModerators(ProjectType projectType) {
        project = projectRepository.save(project.setType(projectType));
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @ParameterizedTest
    @EnumSource(ProjectType.class)
    @DisplayName("Admin can start the room")
    void okForAdmin(ProjectType projectType) {
        project = projectRepository.save(project.setType(projectType));
        var admin = new ResearchSessionData.Participant()
                .setId(make())
                .setInvitationToken(make())
                .setName(make())
                .setRole(MODERATOR)
                .setAdmin(true)
                .setBanable(false)
                .setGuest(false)
                .setProjectManager(false)
                .setProjectOperator(false)
                .setModerator(true);
        var researchSession = setUpRoom(admin);
        var participant = getParticipant(researchSession, MODERATOR);

        startRoom(participant);

        verifyThatRoomHasStarted(participant);
    }

    @ParameterizedTest
    @EnumSource(ProjectType.class)
    @DisplayName("Project manager can start the room")
    void okForProjectManager(ProjectType projectType) {
        project = projectRepository.save(project.setType(projectType));
        var projectManager = new ResearchSessionData.Participant()
                .setId(make())
                .setInvitationToken(make())
                .setName(make())
                .setRole(MODERATOR)
                .setAdmin(false)
                .setBanable(true)
                .setGuest(false)
                .setProjectManager(true)
                .setProjectOperator(false)
                .setModerator(true);
        var researchSession = setUpRoom(projectManager);
        var participant = getParticipant(researchSession, MODERATOR);

        startRoom(participant);

        verifyThatRoomHasStarted(participant);
    }

    @ParameterizedTest
    @EnumSource(ProjectType.class)
    @DisplayName("Project operator can start the room")
    void okForProjectOperator(ProjectType projectType) {
        project = projectRepository.save(project.setType(projectType));
        var projectOperator = new ResearchSessionData.Participant()
                .setId(make())
                .setInvitationToken(make())
                .setName(make())
                .setRole(MODERATOR)
                .setAdmin(false)
                .setBanable(true)
                .setGuest(false)
                .setProjectManager(false)
                .setProjectOperator(true)
                .setModerator(true);
        var researchSession = setUpRoom(projectOperator);
        var participant = getParticipant(researchSession, MODERATOR);

        startRoom(participant);

        verifyThatRoomHasStarted(participant);
    }

    @Test
    @DisplayName("Moderator can start the room - self service project")
    void okForModerator_selfServiceProject() {
        project = projectRepository.save(project.setType(SELF_SERVICE));
        var moderator = new ResearchSessionData.Participant()
                .setId(make())
                .setInvitationToken(make())
                .setName(make())
                .setRole(MODERATOR)
                .setAdmin(false)
                .setBanable(true)
                .setGuest(false)
                .setProjectManager(false)
                .setProjectOperator(false)
                .setModerator(true);
        var researchSession = setUpRoom(moderator);
        var participant = getParticipant(researchSession, MODERATOR);

        startRoom(participant);

        verifyThatRoomHasStarted(participant);
    }

    @Test
    @DisplayName("Forbidden for moderator - full service project")
    void forbiddenForModerator_fullServiceProject() {
        project = projectRepository.save(project.setType(FULL_SERVICE));
        var moderator = new ResearchSessionData.Participant()
                .setId(make())
                .setInvitationToken(make())
                .setName(make())
                .setRole(MODERATOR)
                .setAdmin(false)
                .setBanable(true)
                .setGuest(false)
                .setProjectManager(false)
                .setProjectOperator(false)
                .setModerator(true);
        var researchSession = setUpRoom(moderator);
        var participant = getParticipant(researchSession, MODERATOR);

        given()
                .when()
                .headers(roomParticipantHeaders(participant))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(FORBIDDEN.value());
    }

    // Fail already started

    // Fail finished
}
