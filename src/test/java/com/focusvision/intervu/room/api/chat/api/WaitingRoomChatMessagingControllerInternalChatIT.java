package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils.OutgoingChatMessageStompFrameHandler;
import com.focusvision.intervu.room.api.chat.model.IncomingChatMessage;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.time.Instant.now;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * IT tests for waiting room chat functionality {@link WaitingRoomChatMessagingController#internal}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Waiting Room internal chat messaging")
class WaitingRoomChatMessagingControllerInternalChatIT extends HttpBasedIT {

    @Test
    @DisplayName("Message not received for forbidden topic")
    void messageNotReceivedForForWrongChatTopic() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var session = connectRoomWs(moderator);
        var auth = AuthenticatedParticipant.builder()
                .id(moderator.getId())
                .role(ParticipantRole.MODERATOR)
                .roomId(setUpFullRoom().getId())
                .build();
        var forbiddenChatTopic = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getInternal();

        subscribe(
                session,
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        var outgoingChatMessage = new OutgoingChatMessage()
                .setMessage(make())
                .setSenderId(make())
                .setSenderName(make())
                .setHandle(randomUUID())
                .setSentAt(now().toEpochMilli());

        messagingTemplate.convertAndSend(forbiddenChatTopic.getDestination(), outgoingChatMessage);
        assertThrows(TimeoutException.class, () -> resultKeeper.get(500, MILLISECONDS));
    }

    @Test
    @DisplayName("Messaging forbidden for respondent")
    void forbiddenForRespondent() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);

        var session = connectRoomWs(respondent);
        var auth = AuthenticatedParticipant.builder()
                .id(respondent.getId())
                .roomId(researchSession.getId())
                .build();
        var handle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getInternal();

        subscribe(
                session,
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        session.send(handle.getDestination(), new OutgoingChatMessage());
        assertThrows(TimeoutException.class, () -> resultKeeper.get(500, MILLISECONDS));
    }

    @Test
    @DisplayName("Message received for allowed topic")
    void messageReceivedForProperTopic() throws ExecutionException, InterruptedException, TimeoutException {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var session = connectRoomWs(moderator);
        var auth = AuthenticatedParticipant.builder()
                .id(moderator.getId())
                .roomId(researchSession.getId())
                .build();
        var handle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getInternal();

        connectToRoom(researchSession.getId(), moderator);

        subscribe(
                session,
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        var message = new OutgoingChatMessage()
                .setMessage(make())
                .setSenderId(make())
                .setSenderName(make())
                .setHandle(randomUUID())
                .setSentAt(now().toEpochMilli());

        session.send(handle.getDestination(), message);
        assertThat(resultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
    }

    @Test
    @DisplayName("Moderator sends internal message")
    void moderatorSendInternalMessage() throws ExecutionException, InterruptedException, TimeoutException {
        var moderatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var respondentResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var observerResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorFromOtherProjectResultKeeper = new CompletableFuture<OutgoingChatMessage>();

        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var translator2 = getTranslator(setUpFullRoom());

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        var translatorSession = connectRoomWs(translator);
        var observerSession = connectRoomWs(observer);
        var translatorFromOtherProjectSession = connectRoomWs(translator2);

        moderatorSession.subscribe(
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(moderatorResultKeeper::complete));
        respondentSession.subscribe(
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(respondentResultKeeper::complete));
        translatorSession.subscribe(
                getChatChannel(translator.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorResultKeeper::complete));
        translatorFromOtherProjectSession.subscribe(
                getChatChannel(translator2.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorFromOtherProjectResultKeeper::complete));
        observerSession.subscribe(
                getChatChannel(observer.getId()),
                new OutgoingChatMessageStompFrameHandler(observerResultKeeper::complete));
        waitForWs();

        var auth = AuthenticatedParticipant.builder()
                .id(moderator.getId())
                .roomId(researchSession.getId())
                .build();
        var waitingRoomInternalChatHandle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getInternal();
        var message = new IncomingChatMessage(make());
        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);
        connectToRoom(researchSession.getId(), translator);
        connectToRoom(researchSession.getId(), observer);

        moderatorSession.send(waitingRoomInternalChatHandle.getDestination(), message);

        assertThat(moderatorResultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(translatorResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(observerResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThrows(TimeoutException.class, () -> respondentResultKeeper.get(500, MILLISECONDS));
        assertThrows(TimeoutException.class, () -> translatorFromOtherProjectResultKeeper.get(100, MILLISECONDS));

        moderatorSession.disconnect();
        respondentSession.disconnect();
        translatorSession.disconnect();
        observerSession.disconnect();
        translatorFromOtherProjectSession.disconnect();
    }

    @Test
    @DisplayName("Translator sends internal message")
    void translatorSendsInternalMessage() throws ExecutionException, InterruptedException, TimeoutException {
        var moderatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var respondentResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var observerResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorFromOtherProjectResultKeeper = new CompletableFuture<OutgoingChatMessage>();

        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var translator2 = getTranslator(setUpFullRoom());

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        var translatorSession = connectRoomWs(translator);
        var observerSession = connectRoomWs(observer);
        var translatorFromOtherProjectSession = connectRoomWs(translator2);

        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);
        connectToRoom(researchSession.getId(), translator);
        connectToRoom(researchSession.getId(), observer);

        moderatorSession.subscribe(
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(moderatorResultKeeper::complete));
        respondentSession.subscribe(
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(respondentResultKeeper::complete));
        translatorSession.subscribe(
                getChatChannel(translator.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorResultKeeper::complete));
        translatorFromOtherProjectSession.subscribe(
                getChatChannel(translator2.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorFromOtherProjectResultKeeper::complete));
        observerSession.subscribe(
                getChatChannel(observer.getId()),
                new OutgoingChatMessageStompFrameHandler(observerResultKeeper::complete));
        waitForWs();

        var auth = AuthenticatedParticipant.builder()
                .id(translator.getId())
                .roomId(moderator.getResearchSession().getId())
                .build();
        var waitingRoomInternalChatHandle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getInternal();
        var message = new IncomingChatMessage(make());

        translatorSession.send(waitingRoomInternalChatHandle.getDestination(), message);
        waitForWs();
        var chatMessages = chatMessageRepository
                .findAllByHandle(waitingRoomInternalChatHandle.getHandle());
        var lastChatMessage = chatMessages.get(chatMessages.size() - 1);

        assertThat(lastChatMessage.getMessage()).isEqualTo(message.getMessage());
        assertThat(lastChatMessage.getHandle()).isEqualTo(waitingRoomInternalChatHandle.getHandle());
        assertThat(lastChatMessage.getSenderDisplayName()).isEqualTo(translator.getDisplayName());
        assertThat(lastChatMessage.getSenderPlatformId()).isEqualTo(translator.getPlatformId());
        assertThat(lastChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));
        assertThat(lastChatMessage.getSource()).isEqualTo(ChatMessage.ChatSource.WAITING_ROOM);
        assertThat(lastChatMessage.getType()).isEqualTo(ChatType.INTERNAL);

        var moderatorReceivedMessage = moderatorResultKeeper.get(500, MILLISECONDS);
        assertThat(moderatorReceivedMessage.getMessage()).isEqualTo(message.getMessage());
        assertThat(moderatorReceivedMessage.getHandle()).isEqualTo(waitingRoomInternalChatHandle.getHandle());
        assertThat(moderatorReceivedMessage.getSenderName()).isEqualTo(translator.getDisplayName());
        assertThat(moderatorReceivedMessage.getSenderId()).isEqualTo(translator.getId().toString());
        assertThat(moderatorReceivedMessage.getSentAt()).isEqualTo(lastChatMessage.getSentAt().toEpochMilli());
        assertThat(moderatorReceivedMessage.getType()).isEqualTo(ChatType.INTERNAL);

        assertThat(moderatorResultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(observerResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(translatorResultKeeper.get(100, MILLISECONDS).getMessage())
                .isEqualTo(message.getMessage());
        assertThrows(TimeoutException.class, () -> respondentResultKeeper.get(100, MILLISECONDS));
        assertThrows(TimeoutException.class,
                () -> translatorFromOtherProjectResultKeeper.get(100, MILLISECONDS));

        moderatorSession.disconnect();
        respondentSession.disconnect();
        translatorSession.disconnect();
        observerSession.disconnect();
        translatorFromOtherProjectSession.disconnect();
    }

    @Test
    @DisplayName("Observer sends internal message")
    void observerSendsInternalMessage() throws ExecutionException, InterruptedException, TimeoutException {
        var moderatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var respondentResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var observerResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorFromOtherProjectResultKeeper = new CompletableFuture<OutgoingChatMessage>();

        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var translator2 = getTranslator(setUpFullRoom());

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        var translatorSession = connectRoomWs(translator);
        var observerSession = connectRoomWs(observer);
        var translatorFromOtherProjectSession = connectRoomWs(translator2);
        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);
        connectToRoom(researchSession.getId(), translator);
        connectToRoom(researchSession.getId(), observer);

        moderatorSession.subscribe(
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(moderatorResultKeeper::complete));
        respondentSession.subscribe(
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(respondentResultKeeper::complete));
        translatorSession.subscribe(
                getChatChannel(translator.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorResultKeeper::complete));
        translatorFromOtherProjectSession.subscribe(
                getChatChannel(translator2.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorFromOtherProjectResultKeeper::complete));
        observerSession.subscribe(
                getChatChannel(observer.getId()),
                new OutgoingChatMessageStompFrameHandler(observerResultKeeper::complete));
        waitForWs();

        var auth = AuthenticatedParticipant.builder()
                .id(observer.getId())
                .roomId(researchSession.getId())
                .build();
        var waitingRoomInternalChatHandle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getInternal();
        var message = new IncomingChatMessage(make());

        observerSession.send(waitingRoomInternalChatHandle.getDestination(), message);
        waitForWs();
        var chatMessages = chatMessageRepository
                .findAllByHandle(waitingRoomInternalChatHandle.getHandle());
        var lastChatMessage = chatMessages.get(chatMessages.size() - 1);

        assertThat(lastChatMessage.getMessage()).isEqualTo(message.getMessage());
        assertThat(lastChatMessage.getHandle()).isEqualTo(waitingRoomInternalChatHandle.getHandle());
        assertThat(lastChatMessage.getSenderDisplayName()).isEqualTo(observer.getDisplayName());
        assertThat(lastChatMessage.getSenderPlatformId()).isEqualTo(observer.getPlatformId());
        assertThat(lastChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));
        assertThat(lastChatMessage.getSource()).isEqualTo(ChatMessage.ChatSource.WAITING_ROOM);
        assertThat(lastChatMessage.getType()).isEqualTo(ChatType.INTERNAL);

        var observerReceivedMessage = observerResultKeeper.get(500, MILLISECONDS);
        assertThat(observerReceivedMessage.getMessage()).isEqualTo(message.getMessage());
        assertThat(observerReceivedMessage.getHandle()).isEqualTo(waitingRoomInternalChatHandle.getHandle());
        assertThat(observerReceivedMessage.getSenderName()).isEqualTo(observer.getDisplayName());
        assertThat(observerReceivedMessage.getSenderId()).isEqualTo(observer.getId().toString());
        assertThat(observerReceivedMessage.getSentAt()).isEqualTo(lastChatMessage.getSentAt().toEpochMilli());
        assertThat(observerReceivedMessage.getType()).isEqualTo(ChatType.INTERNAL);

        assertThat(moderatorResultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(observerResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(translatorResultKeeper.get(100, MILLISECONDS).getMessage())
                .isEqualTo(message.getMessage());
        assertThrows(TimeoutException.class, () -> respondentResultKeeper.get(100, MILLISECONDS));
        assertThrows(TimeoutException.class,
                () -> translatorFromOtherProjectResultKeeper.get(100, MILLISECONDS));

        moderatorSession.disconnect();
        respondentSession.disconnect();
        translatorSession.disconnect();
        observerSession.disconnect();
        translatorFromOtherProjectSession.disconnect();
    }

}
