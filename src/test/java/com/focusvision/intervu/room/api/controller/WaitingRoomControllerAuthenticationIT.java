package com.focusvision.intervu.room.api.controller;

import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.chat.api.WaitingRoomChatController;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.dto.AuthDto;
import java.util.Random;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link WaitingRoomChatController}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Waiting Room authentication")
class WaitingRoomControllerAuthenticationIT extends HttpBasedIT {

  @Test
  @DisplayName("Error authenticating")
  void errorAuthenticating() {
    given()
        .when()
        .get(url(WAITING_ROOM_AUTHENTICATION_URL), make())
        .then()
        .statusCode(HttpStatus.BAD_REQUEST.value())
        .body("message", equalTo("Error authenticating."));
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Success")
  void success(ParticipantRole role) {
    var participant = getParticipant(setUpFullRoom(), role);
    given()
        .when()
        .get(url(WAITING_ROOM_AUTHENTICATION_URL), participant.getInvitationToken())
        .then()
        .statusCode(HttpStatus.OK.value())
        .body("token", notNullValue());
  }

  @Test
  @DisplayName("Forbidden for unauthorized")
  void forbiddenUnauthorized() {
    given()
        .when()
        .get(url(WAITING_ROOM_USER_AUTHENTICATION_URL), make())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Forbidden for room participants")
  void forbiddenRoomParticipants() {
    given()
        .when()
        .headers(roomParticipantHeaders(getRespondent(setUpFullRoom())))
        .get(url(WAITING_ROOM_USER_AUTHENTICATION_URL), new Random().nextLong())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Room not found")
  void roomNotFound() {
    given()
        .when()
        .headers(intervuUserHeaders(getModerator(setUpFullRoom())))
        .get(url(WAITING_ROOM_USER_AUTHENTICATION_URL), new Random().nextLong())
        .then()
        .statusCode(BAD_REQUEST.value());
  }

  @Test
  @DisplayName("Participant not belonging to room")
  void participantNotBelongsToRoom() {
    given()
        .when()
        .headers(intervuUserHeaders(getModerator(setUpFullRoom())))
        .get(url(WAITING_ROOM_USER_AUTHENTICATION_URL), getModerator(setUpFullRoom())
            .getResearchSession().getPlatformId())
        .then()
        .statusCode(BAD_REQUEST.value());
  }

  @Test
  @DisplayName("Authenticated")
  void authenticated() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    AuthDto auth = given()
        .when()
        .headers(intervuUserHeaders(moderator))
        .get(url(WAITING_ROOM_USER_AUTHENTICATION_URL),
            moderator.getResearchSession().getPlatformId())
        .then()
        .statusCode(OK.value())
        .extract().body().as(AuthDto.class);

    assertThat(auth.getToken()).isNotEmpty();

    given()
        .when()
        .headers("X-Room-Token", auth.getToken())
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value());
  }

}
