package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.chat.model.ChatMessageDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.REGULAR;
import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link MeetingRoomChatController#internalHistory}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Meeting Room internal chat history")
class MeetingRoomChatControllerInternalChatHistoryIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for respondents")
    void forbiddenForRespondent() {
        given()
                .when()
                .headers(roomParticipantHeaders(getRespondent(setUpFullRoom())))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participants")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Moderator is sender")
    void okForBackroomUsersModerator() throws Exception {
        var moderatorMessage = make();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var roomId = researchSession.getId();

        sendBackRoomInternalChatMessage(moderator, moderatorMessage);

        var chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(0))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        ChatMessageDto moderatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(moderatorChatMessage.getId()).isNotNull();
        assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId());
        assertThat(moderatorChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(moderatorChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(moderatorChatMessage.getMessage()).isEqualTo(moderatorMessage);
        assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName());
        assertThat(moderatorChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        moderatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(moderatorChatMessage.getId()).isNotNull();
        assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId());
        assertThat(moderatorChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(moderatorChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(moderatorChatMessage.getMessage()).isEqualTo(moderatorMessage);
        assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName());
        assertThat(moderatorChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        moderatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(moderatorChatMessage.getId()).isNotNull();
        assertThat(moderatorChatMessage.getSenderId()).isEqualTo(moderator.getId());
        assertThat(moderatorChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(moderatorChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(moderatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(moderatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(moderatorChatMessage.getMessage()).isEqualTo(moderatorMessage);
        assertThat(moderatorChatMessage.getSenderName()).isEqualTo(moderator.getDisplayName());
        assertThat(moderatorChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));
    }

    @Test
    @DisplayName("Translator is sender")
    void okForBackroomUsersTranslator() throws Exception {
        var translatorMessage = make();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var roomId = researchSession.getId();

        sendBackRoomInternalChatMessage(translator, translatorMessage);

        var chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);;

        ChatMessageDto translatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(translatorChatMessage.getId()).isNotNull();
        assertThat(translatorChatMessage.getSenderId()).isEqualTo(translator.getId());
        assertThat(translatorChatMessage.getSenderPlatformId()).isEqualTo(translator.getPlatformId());
        assertThat(translatorChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(translatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(translatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(translatorChatMessage.getMessage()).isEqualTo(translatorMessage);
        assertThat(translatorChatMessage.getSenderName()).isEqualTo(translator.getDisplayName());
        assertThat(translatorChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(0))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        translatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(translatorChatMessage.getId()).isNotNull();
        assertThat(translatorChatMessage.getSenderId()).isEqualTo(translator.getId());
        assertThat(translatorChatMessage.getSenderPlatformId()).isEqualTo(translator.getPlatformId());
        assertThat(translatorChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(translatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(translatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(translatorChatMessage.getMessage()).isEqualTo(translatorMessage);
        assertThat(translatorChatMessage.getSenderName()).isEqualTo(translator.getDisplayName());
        assertThat(translatorChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);;

        translatorChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(translatorChatMessage.getId()).isNotNull();
        assertThat(translatorChatMessage.getSenderId()).isEqualTo(translator.getId());
        assertThat(translatorChatMessage.getSenderPlatformId()).isEqualTo(translator.getPlatformId());
        assertThat(translatorChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(translatorChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(translatorChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(translatorChatMessage.getMessage()).isEqualTo(translatorMessage);
        assertThat(translatorChatMessage.getSenderName()).isEqualTo(translator.getDisplayName());
        assertThat(translatorChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));
    }

    @Test
    @DisplayName("Observer is sender")
    void okForBackroomUsersObserver() throws Exception {
        var observerMessage = make();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var roomId = researchSession.getId();

        sendBackRoomInternalChatMessage(observer, observerMessage);

        var chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        ChatMessageDto observerChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(observerChatMessage.getId()).isNotNull();
        assertThat(observerChatMessage.getSenderId()).isEqualTo(observer.getId());
        assertThat(observerChatMessage.getSenderPlatformId()).isEqualTo(observer.getPlatformId());
        assertThat(observerChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(observerChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(observerChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(observerChatMessage.getMessage()).isEqualTo(observerMessage);
        assertThat(observerChatMessage.getSenderName()).isEqualTo(observer.getDisplayName());
        assertThat(observerChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(1))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        observerChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(observerChatMessage.getId()).isNotNull();
        assertThat(observerChatMessage.getSenderId()).isEqualTo(observer.getId());
        assertThat(observerChatMessage.getSenderPlatformId()).isEqualTo(observer.getPlatformId());
        assertThat(observerChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(observerChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(observerChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(observerChatMessage.getMessage()).isEqualTo(observerMessage);
        assertThat(observerChatMessage.getSenderName()).isEqualTo(observer.getDisplayName());
        assertThat(observerChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));

        chatMessages = given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(ROOM_INTERNAL_CHAT_MESSAGES_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("unreadMessages", equalTo(0))
                .body("latestSeenMessageId", nullValue())
                .body("messages[0].size()", is(9))
                .extract().body().jsonPath().getList("messages", ChatMessageDto.class);

        observerChatMessage = chatMessages.get(chatMessages.size() - 1);
        assertThat(observerChatMessage.getId()).isNotNull();
        assertThat(observerChatMessage.getSenderId()).isEqualTo(observer.getId());
        assertThat(observerChatMessage.getSenderPlatformId()).isEqualTo(observer.getPlatformId());
        assertThat(observerChatMessage.getHandle()).isEqualTo(roomId);
        assertThat(observerChatMessage.getType()).isEqualByComparingTo(INTERNAL);
        assertThat(observerChatMessage.getMessageType()).isEqualByComparingTo(REGULAR);
        assertThat(observerChatMessage.getMessage()).isEqualTo(observerMessage);
        assertThat(observerChatMessage.getSenderName()).isEqualTo(observer.getDisplayName());
        assertThat(observerChatMessage.getSentAt()).isCloseTo(now(), within(2, SECONDS));
    }
}
