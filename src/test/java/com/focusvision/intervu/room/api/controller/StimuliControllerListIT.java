package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static com.focusvision.intervu.room.api.common.model.StimulusType.DOCUMENT_SHARING;
import static com.focusvision.intervu.room.api.common.model.StimulusType.DRAG_AND_DROP;
import static com.focusvision.intervu.room.api.common.model.StimulusType.POLL;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link StimuliController#list}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Room stimuli list")
class StimuliControllerListIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(ROOM_STIMULI_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned moderators")
    void unauthorizedBannedModerator() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_STIMULI_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for non moderators")
    void forbiddenForParticipant() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(ROOM_STIMULI_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .get(url(ROOM_STIMULI_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .get(url(ROOM_STIMULI_URL))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Stimuli bar data - ok for moderator")
    void stimuliBarData_okForBackroomUsersModerator() {
        given()
                .when()
                .headers(roomParticipantHeaders(getModerator(setUpFullRoom())))
                .get(url(ROOM_STIMULI_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(3))
                .body("[0].size()", is(9))
                .body("[0].id", not(nullValue()))
                .body("[0].platformId", not(nullValue()))
                .body("[0].position", is(1))
                .body("[0].data", notNullValue())
                .body("[0].name", containsString("Stimulus 3"))
                .body("[0].type", equalTo(DOCUMENT_SHARING.name()))
                .body("[1].size()", is(9))
                .body("[1].id", not(nullValue()))
                .body("[1].platformId", not(nullValue()))
                .body("[1].position", is(2))
                .body("[1].data", notNullValue())
                .body("[1].name", containsString("Stimulus 2"))
                .body("[1].type", equalTo(DRAG_AND_DROP.name()))
                .body("[2].size()", is(9))
                .body("[2].id", not(nullValue()))
                .body("[2].platformId", not(nullValue()))
                .body("[2].position", is(3))
                .body("[2].data", notNullValue())
                .body("[2].name", containsString("Stimulus 1"))
                .body("[2].type", equalTo(POLL.name()));
    }

}
