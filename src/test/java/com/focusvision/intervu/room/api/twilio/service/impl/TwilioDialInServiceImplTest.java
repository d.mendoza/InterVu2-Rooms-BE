package com.focusvision.intervu.room.api.twilio.service.impl;

import static java.util.Optional.of;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.focusvision.intervu.room.api.common.provider.ResearchSessionProvider;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.twilio.api.TwilioDialInRequest;
import com.focusvision.intervu.room.api.twilio.model.DialInPin;
import com.focusvision.intervu.room.api.twilio.service.TwilioIdentityProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TwilioDialInServiceImplTest {

  private TwilioDialInServiceImpl service;

  @Mock
  private ResearchSessionProvider researchSessionProvider;

  @Mock
  private ParticipantService participantService;

  @Mock
  private TwilioIdentityProvider twilioIdentityProvider;

  @BeforeEach
  public void setup() {
    service = new TwilioDialInServiceImpl(
        researchSessionProvider, participantService, twilioIdentityProvider);
  }

  @Test
  @DisplayName("Process dial in - gather response returned")
  void processDialIn() {
    var request = new TwilioDialInRequest();

    var response = service.processDialIn(request);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Gather action=\"/twilio/dial-in/pin\" finishOnKey=\"#\" input=\"dtmf\" timeout=\"10\"><Say>Welcome. Please enter the conference PIN followed by #.</Say></Gather><Say>We didn't receive any input. Goodbye!</Say></Response>");
  }

  @Test
  @DisplayName("Process dial in PIN - invalid PIN")
  void processDialInPin_invalidPin() {
    var dialInPin = new DialInPin("123456");

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>You've entered invalid conference PIN number.</Say></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider).findByOperatorPin(dialInPin.pin());
    verify(participantService).findBySpeakerPin(dialInPin.pin());
  }

  @Test
  @DisplayName("Process dial in PIN - listener PIN, session not started")
  void processDialInPin_listenerPinSessionNotStarted() {
    var dialInPin = new DialInPin("111111");
    var researchSession = mock(ResearchSession.class);
    when(researchSessionProvider.findByListenerPin(dialInPin.pin())).thenReturn(of(researchSession));

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>The conference has not started yet. Please try again later.</Say></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider, never()).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider, never()).findByOperatorPin(dialInPin.pin());
    verify(participantService, never()).findBySpeakerPin(dialInPin.pin());
    verify(researchSession).getConference();
  }

  @Test
  @DisplayName("Process dial in PIN - listener PIN, session started")
  void processDialInPin_listenerPinSessionStarted() {
    var dialInPin = new DialInPin("111111");
    var conference = new Conference()
        .setRoomSid("RM111111")
        .setCompleted(false);
    var researchSession = new ResearchSession()
        .setId(randomUUID())
        .setConference(conference);

    when(researchSessionProvider.findByListenerPin(dialInPin.pin())).thenReturn(of(researchSession));
    when(twilioIdentityProvider.generateAnonymousListenerIdentity(researchSession.getId()))
        .thenReturn("listener_identity");

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>You will be joined to the conference now.</Say><Connect><Room participantIdentity=\"listener_identity\">RM111111</Room></Connect></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider, never()).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider, never()).findByOperatorPin(dialInPin.pin());
    verify(participantService, never()).findBySpeakerPin(dialInPin.pin());
  }

  @Test
  @DisplayName("Process dial in PIN - speaker PIN, session not started")
  void processDialInPin_speakerPinSessionNotStarted() {
    var dialInPin = new DialInPin("222222");
    var researchSession = mock(ResearchSession.class);
    when(researchSessionProvider.findBySpeakerPin(dialInPin.pin())).thenReturn(of(researchSession));

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>The conference has not started yet. Please try again later.</Say></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider, never()).findByOperatorPin(dialInPin.pin());
    verify(participantService, never()).findBySpeakerPin(dialInPin.pin());
    verify(researchSession).getConference();
  }

  @Test
  @DisplayName("Process dial in PIN - speaker PIN, session started")
  void processDialInPin_speakerPinSessionStarted() {
    var dialInPin = new DialInPin("222222");
    var conference = new Conference()
        .setRoomSid("RM222222")
        .setCompleted(false);
    var researchSession = new ResearchSession()
        .setId(randomUUID())
        .setConference(conference);

    when(researchSessionProvider.findBySpeakerPin(dialInPin.pin())).thenReturn(of(researchSession));
    when(twilioIdentityProvider.generateAnonymousSpeakerIdentity(researchSession.getId()))
        .thenReturn("speaker_identity");

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>You will be joined to the conference now.</Say><Connect><Room participantIdentity=\"speaker_identity\">RM222222</Room></Connect></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider, never()).findByOperatorPin(dialInPin.pin());
    verify(participantService, never()).findBySpeakerPin(dialInPin.pin());
  }

  @Test
  @DisplayName("Process dial in PIN - operator PIN, session not started")
  void processDialInPin_operatorPinSessionNotStarted() {
    var dialInPin = new DialInPin("333333");
    var researchSession = mock(ResearchSession.class);
    when(researchSessionProvider.findByOperatorPin(dialInPin.pin())).thenReturn(of(researchSession));

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>The conference has not started yet. Please try again later.</Say></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider).findByOperatorPin(dialInPin.pin());
    verify(participantService, never()).findBySpeakerPin(dialInPin.pin());
    verify(researchSession).getConference();
  }

  @Test
  @DisplayName("Process dial in PIN - operator PIN, session started")
  void processDialInPin_operatorPinSessionStarted() {
    var dialInPin = new DialInPin("333333");
    var conference = new Conference()
        .setRoomSid("RM333333")
        .setCompleted(false);
    var researchSession = new ResearchSession()
        .setId(randomUUID())
        .setConference(conference);

    when(researchSessionProvider.findByOperatorPin(dialInPin.pin())).thenReturn(of(researchSession));
    when(twilioIdentityProvider.generateAnonymousOperatorIdentity(researchSession.getId()))
        .thenReturn("operator_identity");

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>You will be joined to the conference now.</Say><Connect><Room participantIdentity=\"operator_identity\">RM333333</Room></Connect></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider).findByOperatorPin(dialInPin.pin());
    verify(participantService, never()).findBySpeakerPin(dialInPin.pin());
  }

  @Test
  @DisplayName("Process dial in PIN - participant speaker PIN, session not started")
  void processDialInPin_participantSpeakerPinSessionNotStarted() {
    var dialInPin = new DialInPin("444444");
    var participant = new Participant()
        .setResearchSession(new ResearchSession()
            .setConference(new Conference()));
    when(participantService.findBySpeakerPin(dialInPin.pin())).thenReturn(of(participant));

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>The conference has not started yet. Please try again later.</Say></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider).findByOperatorPin(dialInPin.pin());
    verify(participantService).findBySpeakerPin(dialInPin.pin());
  }

  @Test
  @DisplayName("Process dial in PIN - participant speaker PIN, session started")
  void processDialInPin_participantSpeakerPinSessionStarted() {
    var dialInPin = new DialInPin("444444");
    var conference = new Conference()
        .setRoomSid("RM444444")
        .setCompleted(false);
    var researchSession = new ResearchSession()
        .setId(randomUUID())
        .setConference(conference);
    var participant = new Participant()
        .setId(randomUUID())
        .setResearchSession(researchSession);
    when(participantService.findBySpeakerPin(dialInPin.pin())).thenReturn(of(participant));

    when(twilioIdentityProvider.generateSpeakerIdentity(participant.getId()))
        .thenReturn("speaker_participant_identity");

    var response = service.processDialInPin(dialInPin);

    assertThat(response)
        .isEqualTo(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response><Say>You will be joined to the conference now.</Say><Connect><Room participantIdentity=\"speaker_participant_identity\">RM444444</Room></Connect></Response>");
    verify(researchSessionProvider).findByListenerPin(dialInPin.pin());
    verify(researchSessionProvider).findBySpeakerPin(dialInPin.pin());
    verify(researchSessionProvider).findByOperatorPin(dialInPin.pin());
    verify(participantService).findBySpeakerPin(dialInPin.pin());
  }

}
