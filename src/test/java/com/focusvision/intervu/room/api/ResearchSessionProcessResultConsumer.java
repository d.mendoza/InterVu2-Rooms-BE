package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

@RequiredArgsConstructor
public class ResearchSessionProcessResultConsumer {

    private SimpleMessageListenerContainer container;
    private CountDownLatch latch;
    private List<ResearchSessionProcessResult> messages = new ArrayList<>();

    public ResearchSessionProcessResultConsumer(final ConnectionFactory connectionFactory,
                                                MessageConverter messageConverter,
                                                String queueName,
                                                int messageCount) {
        var adapter = new MessageListenerAdapter(this, "receiveMessage");
        adapter.setMessageConverter(messageConverter);

        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setupMessageListener(adapter);

        latch = new CountDownLatch(messageCount);
    }

    @SuppressWarnings({"unused"})
    public void receiveMessage(ResearchSessionProcessResult message) {
        messages.add(message);
        latch.countDown();
    }

    public void stop() {
        container.stop();
    }

    public ResearchSessionProcessResultConsumer start() {
        container.start();

        return this;
    }

    public Optional<ResearchSessionProcessResult> getMessage(String id, Integer version) {
        return messages.stream()
                .filter(processed -> id.equals(processed.getResearchSessionData().getId()))
                .filter(processed -> version.equals(processed.getResearchSessionData().getVersion()))
                .findFirst();
    }
}
