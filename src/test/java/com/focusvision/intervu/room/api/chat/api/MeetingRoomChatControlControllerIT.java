package com.focusvision.intervu.room.api.chat.api;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.RESPONDENTS_CHAT_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.RESPONDENTS_CHAT_ENABLED;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link MeetingRoomChatControlController}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Meeting Room chat control")
class MeetingRoomChatControlControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Enable forbidden for unauthorized")
    void enableForbiddenUnauthorized() {
        given()
            .when()
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_ENABLE_URL))
            .then()
            .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"MODERATOR"})
    @DisplayName("Enable forbidden for all except moderator")
    void enableForbiddenForAllExceptModerator(ParticipantRole role) {
        var room = setUpFullRoom();

        given()
            .when()
            .headers(roomParticipantHeaders(getParticipant(room, role)))
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_ENABLE_URL))
            .then()
            .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Disable forbidden for unauthorized")
    void disableForbiddenUnauthorized() {
        given()
            .when()
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_DISABLE_URL))
            .then()
            .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = {"MODERATOR"})
    @DisplayName("Disable forbidden for all except moderator")
    void disableForbiddenForAllExceptModerator(ParticipantRole role) {
        var room = setUpFullRoom();

        given()
            .when()
            .headers(roomParticipantHeaders(getParticipant(room, role)))
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_DISABLE_URL))
            .then()
            .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Moderator can enable/disable")
    void moderatorCanEnable() {
        var room = setUpFullRoom();
        var moderator = getModerator(room);
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var moderatorSession = connectRoomWs(moderator);

        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, communicationChannelService.getRoomChannel(room.getId()), moderatorHandler);

        waitForWs();

        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .get(url(MEETING_ROOM_CHAT_METADATA_URL))
            .then()
            .and().statusCode(OK.value())
            .body("respondents.enabled", equalTo(false));

        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_ENABLE_URL))
            .then()
            .statusCode(HttpStatus.OK.value());

        waitForWs();
        var enabledStates = moderatorHandler.getStates().stream()
            .filter(genericRoomStateEvent -> genericRoomStateEvent.getEventType().equals(RESPONDENTS_CHAT_ENABLED))
            .toList();

        assertAll(
            () -> assertThat(enabledStates.size()).isEqualTo(1),
            () -> assertThat(enabledStates.get(0).getData().isRespondentsChatEnabled()).isTrue(),
            () -> assertThat(enabledStates.get(0).getEventType()).isEqualTo(RESPONDENTS_CHAT_ENABLED)
        );

        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .get(url(MEETING_ROOM_CHAT_METADATA_URL))
            .then()
            .and().statusCode(OK.value())
            .body("respondents.enabled", equalTo(true));

        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .put(url(MEETING_ROOM_RESPONDENTS_CHAT_DISABLE_URL))
            .then()
            .statusCode(HttpStatus.OK.value());

        waitForWs();
        var disabledStates = moderatorHandler.getStates().stream()
            .filter(genericRoomStateEvent -> genericRoomStateEvent.getEventType().equals(RESPONDENTS_CHAT_DISABLED))
            .toList();

        assertAll(
            () -> assertThat(disabledStates.size()).isEqualTo(1),
            () -> assertThat(disabledStates.get(0).getData().isRespondentsChatEnabled()).isFalse(),
            () -> assertThat(disabledStates.get(0).getEventType()).isEqualTo(RESPONDENTS_CHAT_DISABLED)
        );

        given()
            .when()
            .headers(roomParticipantHeaders(moderator))
            .get(url(MEETING_ROOM_CHAT_METADATA_URL))
            .then()
            .and().statusCode(OK.value())
            .body("respondents.enabled", equalTo(false));
    }

}
