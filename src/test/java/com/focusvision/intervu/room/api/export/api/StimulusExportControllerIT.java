package com.focusvision.intervu.room.api.export.api;

import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_VOTED;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import org.apache.commons.codec.binary.Base64;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import java.util.UUID;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 * Integration tests for {@link StimulusExportController}.
 */
class StimulusExportControllerIT extends HttpBasedIT {

  @Test
  @DisplayName("Unauthorized")
  void stimulusActionLogs_unauthorized() {
    given()
        .when()
        .get(url(EXPORT_STIMULUS_ACTIONS), make())
        .then()
        .statusCode(UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Forbidden for room participant")
  void forbiddenForParticipant(ParticipantRole role) {
    var researchSession = setUpFullRoom();

    given()
        .when()
        .headers(roomParticipantHeaders(getParticipant(researchSession, role)))
        .get(url(EXPORT_STIMULUS_ACTIONS), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Forbidden for room admin")
  void forbiddenForAdmin() {
    var researchSession = setUpFullRoom();
    var admin = getAdmin(researchSession);

    given()
        .when()
        .headers(intervuUserHeaders(admin))
        .get(url(EXPORT_STIMULUS_ACTIONS), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Room not found")
  void roomNotFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_STIMULUS_ACTIONS), make())
        .then()
        .statusCode(NOT_FOUND.value());
  }

  @Test
  @DisplayName("Stimulus action logs")
  void stimulusActionLogs() {
    var researchSession = setUpFullRoom();
    var pollingStimulus = getPollStimulus(researchSession);
    var voteContent = RandomString.make();

    var moderator = getModerator(researchSession);
    var respondent = getRespondent(researchSession);

    var moderatorSession = connectRoomWs(moderator);
    var respondentSession = connectRoomWs(respondent);

    var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

    startRoom(moderator);
    waitForWs();
    activateStimulus(moderator, pollingStimulus.getId());
    waitForWs();

    var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
    subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

    waitForWs();
    enableStimulusInteraction(moderator);
    waitForWs();
    var voteAction = new PollStimulusActionMessage()
        .setStimulusId(pollingStimulus.getId())
        .setContent(voteContent)
        .setTimestamp(now().toEpochMilli());
    respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction);
    waitForWs();

    finishRoom(moderator);
    waitForWs();

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_STIMULUS_ACTIONS), researchSession.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("[0].size()", Matchers.equalTo(9))
        .body("[0].actionType", equalTo(POLL_VOTED.name()))
        .body("[0].stimulusId", equalTo(pollingStimulus.getId().toString()))
        .body("[0].stimulusPlatformId", equalTo(pollingStimulus.getPlatformId()))
        .body("[0].participantId", equalTo(respondent.getId().toString()))
        .body("[0].participantPlatformId", equalTo(respondent.getPlatformId()))
        .body("[0].addedAt", notNullValue())
        .body("[0].content", equalTo(voteContent))
        .body("[0].offset", notNullValue())
        .body("[0].version", notNullValue());
  }

  @Test
  @DisplayName("Stimuli snapshots - Unauthorized")
  void stimuliSnapshots_unauthorized() {
    given()
        .when()
        .get(url(EXPORT_STIMULI_SNAPSHOTS), make())
        .then()
        .statusCode(UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Used stimuli - unauthorized")
  void usedStimuli_unauthorized() {
    given()
        .when()
        .get(url(EXPORT_ROOM_STIMULI_USED), make())
        .then()
        .statusCode(UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Used stimuli - not found")
  void usedStimuli_notFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_ROOM_STIMULI_USED), make())
        .then()
        .statusCode(NOT_FOUND.value());
  }

  @Test
  @DisplayName("Used stimuli - forbidden for admin")
  void usedStimuli_forbiddenForAdmin() {
    var researchSession = setUpFullRoom();
    var admin = getAdmin(researchSession);

    given()
        .when()
        .headers(intervuUserHeaders(admin))
        .get(url(EXPORT_ROOM_STIMULI_USED), researchSession.getPlatformId())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Used stimuli - forbidden for participant")
  void usedStimuli_forbiddenForParticipant(ParticipantRole role) {
    var researchSession = setUpFullRoom();

    given()
        .when()
        .headers(intervuUserHeaders(getParticipant(researchSession, role)))
        .get(url(EXPORT_ROOM_STIMULI_USED), researchSession.getPlatformId())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Used stimuli - ok")
  void usedStimuli_ok() {
    var researchSession = setUpFullRoom();
    var stimuli = researchSession.getStimuli();
    var ids = stimuli.stream()
        .map(Stimulus::getId)
        .map(UUID::toString)
        .toList()
        .toArray(new String[0]);
    var platformIds = stimuli.stream()
        .map(Stimulus::getPlatformId)
        .toList()
        .toArray(new String[0]);

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_ROOM_STIMULI_USED), researchSession.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("size()", equalTo(stimuli.size()))
        .body("[0].size()", equalTo(2))
        .body("id", containsInAnyOrder(ids))
        .body("platformId", containsInAnyOrder(platformIds));
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Stimuli snapshots - forbidden for room participant")
  void stimuliSnapshots_forbiddenForParticipant(ParticipantRole role) {
    var researchSession = setUpFullRoom();

    given()
        .when()
        .headers(roomParticipantHeaders(getParticipant(researchSession, role)))
        .get(url(EXPORT_STIMULI_SNAPSHOTS), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Stimuli snapshots - forbidden for room admin")
  void stimuliSnapshots_forbiddenForAdmin() {
    var researchSession = setUpFullRoom();
    var admin = getAdmin(researchSession);

    given()
        .when()
        .headers(intervuUserHeaders(admin))
        .get(url(EXPORT_STIMULI_SNAPSHOTS), make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Stimuli snapshots - room not found")
  void stimuliSnapshots_roomNotFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_STIMULI_SNAPSHOTS), make())
        .then()
        .statusCode(NOT_FOUND.value());
  }

  @Test
  @DisplayName("Stimuli snapshots - ok")
  void stimuliSnapshots_ok() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var stimulus1Id = researchSession.getStimuli().stream().map(Stimulus::getId).findFirst()
        .orElseThrow();
    var stimulus2Id = researchSession.getStimuli().stream().map(Stimulus::getId).skip(1).findFirst()
        .orElseThrow();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    var addStimulusSnapshotDto1 = stimulusSnapshotFixture.addStimulusSnapshotDto();
    var addStimulusSnapshotDto2 = stimulusSnapshotFixture.addStimulusSnapshotDto();
    var addStimulusSnapshotDto3 = stimulusSnapshotFixture.addStimulusSnapshotDto();
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(addStimulusSnapshotDto1)
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), stimulus1Id)
        .then()
        .statusCode(CREATED.value());
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(addStimulusSnapshotDto2)
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), stimulus1Id)
        .then()
        .statusCode(CREATED.value());
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(addStimulusSnapshotDto3)
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), stimulus2Id)
        .then()
        .statusCode(CREATED.value());

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(EXPORT_STIMULI_SNAPSHOTS), researchSession.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("size()", is(3))
        .body("[0].size()", is(5))
        .body("[0].stimulusId", equalTo(stimulus1Id.toString()))
        .body("[0].name", equalTo(addStimulusSnapshotDto1.getName() + " - 1"))
        .body("[0].mimeType", equalTo(addStimulusSnapshotDto1.getMimeType()))
        .body("[0].data", equalTo(Base64.encodeBase64String(addStimulusSnapshotDto1.getData())))
        .body("[0].offset", greaterThan(0))
        .body("[1].size()", is(5))
        .body("[1].stimulusId", equalTo(stimulus1Id.toString()))
        .body("[1].name", equalTo(addStimulusSnapshotDto2.getName() + " - 2"))
        .body("[1].mimeType", equalTo(addStimulusSnapshotDto2.getMimeType()))
        .body("[1].data", equalTo(Base64.encodeBase64String(addStimulusSnapshotDto2.getData())))
        .body("[1].offset", greaterThan(0))
        .body("[2].size()", is(5))
        .body("[2].stimulusId", equalTo(stimulus2Id.toString()))
        .body("[2].name", equalTo(addStimulusSnapshotDto3.getName() + " - 1"))
        .body("[2].mimeType", equalTo(addStimulusSnapshotDto3.getMimeType()))
        .body("[2].data", equalTo(Base64.encodeBase64String(addStimulusSnapshotDto3.getData())))
        .body("[2].offset", greaterThan(0));
  }

}
