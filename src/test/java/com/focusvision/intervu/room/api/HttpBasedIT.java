package com.focusvision.intervu.room.api;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;
import static com.focusvision.intervu.room.api.common.model.ProjectType.FULL_SERVICE;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_FINISHED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_STARTED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_ACTIVATED;
import static com.focusvision.intervu.room.api.common.model.StimulusType.DOCUMENT_SHARING;
import static com.focusvision.intervu.room.api.common.model.StimulusType.DRAG_AND_DROP;
import static com.focusvision.intervu.room.api.common.model.StimulusType.POLL;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static java.lang.Thread.sleep;
import static java.time.Instant.now;
import static java.time.temporal.ChronoField.MICRO_OF_SECOND;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.util.Map.of;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toList;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.focusvision.intervu.room.api.bookmark.model.AddBookmarkDto;
import com.focusvision.intervu.room.api.chat.model.IncomingChatMessage;
import com.focusvision.intervu.room.api.chat.model.SeenChatMessage;
import com.focusvision.intervu.room.api.chat.service.ChatDataService;
import com.focusvision.intervu.room.api.chat.service.ChatMetadataService;
import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.configuration.domain.JwtProperties;
import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.fixture.StimulusSnapshotFixture;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.dto.AddPiiBookmarkDto;
import com.focusvision.intervu.room.api.model.dto.AuthDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.Project;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.repository.ChatMessageRepository;
import com.focusvision.intervu.room.api.repository.ParticipantRepository;
import com.focusvision.intervu.room.api.repository.ProjectRepository;
import com.focusvision.intervu.room.api.repository.ResearchSessionRepository;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.security.TokenProvider;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.service.ConferenceService;
import com.focusvision.intervu.room.api.service.ProjectService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.RoomStateNotificationService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.state.RoomStateOperator;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import com.focusvision.intervu.room.api.tracking.repository.ParticipantActionLogRepository;
import com.focusvision.intervu.room.api.tracking.repository.RoomStateAcknowledgementRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.restassured.RestAssured;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.socket.messaging.WebSocketStompClient;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class HttpBasedIT {

  @LocalServerPort
  protected int port;
  @Value("${spring.rabbitmq.management-port}")
  private int rabbitMngPort;
  @Value("${spring.rabbitmq.username}")
  private String rmqUsername;
  @Value("${spring.rabbitmq.password}")
  private String rmqPassword;
  protected WsTestUtils wsTestUtils = new WsTestUtils();
  protected WebSocketStompClient stompClient;

  @Autowired
  protected SimpMessagingTemplate messagingTemplate;
  @Autowired
  protected ProjectService projectService;
  @Autowired
  protected ProjectRepository projectRepository;
  @Autowired
  protected ResearchSessionRepository researchSessionRepository;
  @Autowired
  protected ResearchSessionService researchSessionService;
  @Autowired
  protected ParticipantRepository participantRepository;
  @Autowired
  protected RoomStateNotificationService roomStateNotificationService;
  @Autowired
  protected ChatMessageRepository chatMessageRepository;
  @Autowired
  private ConnectionFactory connectionFactory;
  @Autowired
  private Jackson2JsonMessageConverter producerJackson2MessageConverter;
  @Autowired
  protected ObjectMapper mapper;
  @Autowired
  private MessagingProperties messagingProperties;
  @Autowired
  protected CommunicationChannelService communicationChannelService;
  @Autowired
  protected TokenProvider tokenProvider;
  @Autowired
  protected ChatDataService chatDataService;
  @Autowired
  protected ChatMetadataService chatMetadataService;
  @Autowired
  protected JwtProperties jwtProperties;
  @Autowired
  protected ConferenceService conferenceService;
  @Autowired
  protected RoomStateOperator roomStateOperator;
  @Autowired
  private RabbitTemplate rabbitTemplate;
  @Autowired
  protected RoomOffsetProvider roomOffsetProvider;
  @Autowired
  protected RoomStateAcknowledgementRepository roomStateAcknowledgementRepository;
  @Autowired
  protected ParticipantActionLogRepository participantActionLogRepository;

  @Autowired
  protected StimulusSnapshotFixture stimulusSnapshotFixture;

  protected final static String DASHBOARD_USER_INFO_URL = "dashboard";
  protected final static String DASHBOARD_UPCOMING_SESSIONS_URL = "dashboard/rooms";
  protected final static String DASHBOARD_ROOM_TOKEN_URL = "dashboard/rooms/{roomId}";
  protected final static String WAITING_ROOM_CHAT_DATA_URL = "waiting-room/chat/meta";
  protected final static String WAITING_ROOM_INTERNAL_CHAT_MESSAGES_URL = "waiting-room/chat/internal";
  protected final static String WAITING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL = "waiting-room/chat/respondents";
  protected final static String MEETING_ROOM_RESPONDENTS_CHAT_MESSAGES_URL = "meeting-room/chat/respondents";
  protected final static String MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL = "meeting-room/chat/private/{handle}";
  protected final static String WAITING_ROOM_AUTHENTICATION_URL = "waiting-room/{token}";
  protected final static String WAITING_ROOM_USER_AUTHENTICATION_URL = "waiting-room/session/{platformId}/auth";
  protected final static String ROOM_INTERNAL_CHAT_MESSAGES_URL = "meeting-room/chat/internal";
  protected final static String WAITING_ROOM_RESPONDENTS_CHAT_ENABLE_URL = "waiting-room/chat/respondents/control/enable";
  protected final static String MEETING_ROOM_RESPONDENTS_CHAT_ENABLE_URL = "meeting-room/chat/respondents/control/enable";
  protected final static String MEETING_ROOM_RESPONDENTS_CHAT_DISABLE_URL = "meeting-room/chat/respondents/control/disable";
  protected final static String PARTICIPANTS_TESTING_FAILURE_URL = "participants/testing/fail";
  protected final static String PARTICIPANTS_TESTING_SESSION_INFO_URL = "participants/testing/info";
  protected final static String PARTICIPANTS_TESTING_PASS_URL = "participants/testing/pass";
  protected final static String HOME_URL = "home";
  protected final static String MEETING_ROOM_CHAT_METADATA_URL = "meeting-room/chat/meta";
  protected final static String ROOM_STIMULI_URL = "room/stimuli";
  protected final static String ROOM_STIMULI_THUMBNAILS_URL = "room/stimuli/thumbnails";
  protected final static String ROOM_INFO_URL = "room/info";
  protected final static String ROOM_PIN_CODES_URL = "room/pins";
  protected final static String ROOM_START_URL = "room/start";
  protected final static String ROOM_FINISH_URL = "room/finish";
  protected final static String ROOM_JOIN_URL = "room/join";
  protected final static String ROOM_BOOKMARKS_URL = "room/bookmarks";
  protected final static String ROOM_BOOKMARKS_PII_URL = "room/bookmarks/pii";
  protected final static String ROOM_PARTICIPANT_DRAW_ENABLE = "room/drawing/{id}/enable";
  protected final static String ROOM_PARTICIPANTS_DRAW_ENABLE = "room/drawing/enable";
  protected final static String ROOM_PARTICIPANT_DRAW_DISABLE = "room/drawing/{id}/disable";
  protected final static String ROOM_PARTICIPANTS_DRAW_DISABLE = "room/drawing/disable";
  protected final static String PARTICIPANT_URL = "room/participants/{id}";
  protected final static String PARTICIPANT_EXPORT_URL = "export/participants/{platformId}";
  protected final static String RECORDING_DATA = "recordings/{roomPlatformId}";
  protected final static String REGENERATE_RECORDING_URL = "recordings/{roomPlatformId}/regenerate";
  protected final static String REGENERATE_ALL_RECORDINGS_URL = "recordings/{roomPlatformId}/regenerate/all";
  protected final static String ROOM_STIMULUS_ACTIVATE = "room/stimuli/{id}/activate";
  protected final static String ROOM_STIMULUS_DEACTIVATE = "room/stimuli/{id}/deactivate";
  protected final static String ROOM_STIMULUS_DETAILS = "room/stimuli/{id}";
  protected final static String EXPORT_ROOM_STATE = "export/room/{id}/state";
  protected final static String EXPORT_ROOM_DRAWINGS = "export/room/{id}/drawings";
  protected final static String BOOKMARKS_EXPORT_URL = "export/room/{id}/bookmarks";
  protected final static String EXPORT_ROOM_SCROLL = "export/room/{id}/scroll";
  protected final static String EXPORT_STIMULUS_ACTIONS = "export/room/{id}/stimuli";
  protected final static String EXPORT_STIMULI_SNAPSHOTS = "export/room/{id}/stimuli/snapshots";
  protected final static String EXPORT_ROOM_STIMULI_USED = "export/room/{id}/stimuli/used";
  protected final static String STIMULI_FOCUS_ENABLE_URL = ROOM_STIMULI_URL + "/enable";
  protected final static String STIMULI_FOCUS_DISABLE_URL = ROOM_STIMULI_URL + "/disable";
  protected final static String DRAWING_SYNC_ENABLE_URL = "room/drawing/sync/enable";
  protected final static String DRAWING_SYNC_DISABLE_URL = "room/drawing/sync/disable";
  protected final static String DRAWING_STATE_LOG = "room/drawing/state/{contextId}";
  protected final static String SCROLL_STATE_LOG = "room/scroll/state/{contextId}";

  protected final static String DRAW_STIMULUS_DESTINATION = "/app/meeting-room/stimulus/%s/draw";
  protected final static String SCROLL_STIMULUS_DESTINATION = "/app/meeting-room/stimulus/%s/scroll";
  protected final static String ACK_ROOM_STATE_DESTINATION = "/app/tracking/room-state/acknowledgment";
  protected final static String TRACK_PARTICIPANT_DESTINATION = "/app/tracking/participant";

  protected final static String POLL_STIMULUS_VOTE_DESTINATION = "/app/stimulus.poll.vote";
  protected final static String POLL_STIMULUS_RANKING_DESTINATION = "/app/stimulus.poll.ranking";
  protected final static String POLL_STIMULUS_ANSWER_DESTINATION = "/app/stimulus.poll.answer";
  protected final static String DOCUMENT_STIMULUS_PAGE_CHANGE_DESTINATION = "/app/stimulus.document.page";
  protected final static String RESPONDENT_POLL_STIMULUS_STATE = "room/stimuli/poll/{id}/state";
  protected final static String POLL_STIMULUS_RESULTS = "room/stimuli/poll/{id}/results";
  protected final static String POLL_STIMULUS_STATE = "room/stimuli/poll/{id}/states";
  protected final static String PARTICIPANT_DOCUMENT_STIMULUS_STATE = "room/stimuli/document/{id}/state";
  protected final static String STIMULUS_INTERACTION_ENABLE = "room/stimuli/interaction/enable";
  protected final static String STIMULUS_INTERACTION_DISABLE = "room/stimuli/interaction/disable";
  protected final static String STIMULUS_RESET = "room/stimuli/reset";
  protected final static String STIMULI_ADD_SNAPSHOT_URL = "room/stimuli/{id}/snapshot";

  protected final static String ENABLE_BROADCAST_RESULTS = "room/stimuli/{id}/broadcast/enable";
  protected final static String DISABLE_BROADCAST_RESULTS = "room/stimuli/{id}/broadcast/disable";
  protected final static String ENABLE_STIMULUS_SYNC = "room/stimuli/{id}/sync/enable";
  protected final static String DISABLE_STIMULUS_SYNC = "room/stimuli/{id}/sync/disable";

  protected final static String GET_ALTERNATIVE_ROLES_URL = "room/participants/roles";
  protected final static String SET_ALTERNATIVE_ROLES_URL = "room/participants/{id}/roles";

  /**
   * RabbitMQ management API urls
   */
  protected static final String RMQ_PURGE_QUEUE = "api/queues/%2F/{queueName}/contents";

  /**
   * Twilio dial-in number prefix
   */
  protected static final String DIAL_IN_NUMBER_PREFIX = "(US)+1 ";

  protected final static int BOOKMARK_NOTE_MAX_LENGTH = 1000;

  protected Project project;

  static {
    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
  }

  @BeforeEach
  void setup() {
    project = projectRepository.save(
        new Project()
            .setPlatformId(String.valueOf(Math.abs(new Random().nextLong())))
            .setProjectNumber(make())
            .setName(make())
            .setType(FULL_SERVICE));
    mapper.registerModule(new JavaTimeModule());

    purgeQueue(messagingProperties.getResearchSessionUpdatesQueue());
    purgeQueue(messagingProperties.getResearchSessionProcessedQueue());
    purgeQueue(messagingProperties.getResearchSessionStateUpdatesQueue());
    purgeQueue(messagingProperties.getRespondentTestingQueue());
    purgeQueue(messagingProperties.getConferencesProcessingQueue());
    purgeQueue(messagingProperties.getConferencesRunningQueue());
  }

  @AfterEach
  void tearDown() {
    if (stompClient != null) {
      stompClient.stop();
    }
  }

  @Bean
  public MappingJackson2MessageConverter mappingJackson2MessageConverter() {
    MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
    messageConverter.setObjectMapper(mapper);
    return messageConverter;
  }

  protected String url(String route) {
    return format("http://localhost:%s/%s", port, route);
  }

  protected String url(String route, Integer port) {
    return format("http://localhost:%s/%s", port, route);
  }

  protected String roomAuthToken(String invitationLink) {
    return RestAssured.given()
        .when()
        .get(url(WAITING_ROOM_AUTHENTICATION_URL), invitationLink)
        .thenReturn().as(AuthDto.class).getToken();
  }

  protected String intervuAuthToken(String invitationLink) {
    return generate(invitationLink).getToken();
  }

  private String intervuAuthTokenForSystemUser() {
    return generateForSystemUser().getToken();
  }

  private AuthDto generate(String platformId) {

    Participant participant = participantRepository.findAll().stream()
        .filter(p -> p.getPlatformId().equals(platformId))
        .findFirst()
        .orElseThrow();

    var email = participant.getId().toString();

    return generate(
        participant.getPlatformId(),
        email,
        participant.getDisplayName(),
        participant.getDisplayName(),
        false,
        email);
  }

  private AuthDto generateForSystemUser() {
    var platformId = make();

    return generate(platformId, make(), make(), make(), true, platformId);
  }

  private AuthDto generate(
      String platformId,
      String email,
      String firstName,
      String lastName,
      boolean isSystemUser,
      String subject) {

    Map<String, Object> claims = new HashMap<>();
    claims.put("platformId", platformId);
    claims.put("email", email);
    claims.put("firstName", firstName);
    claims.put("lastName", lastName);
    claims.put("isSystemUser", isSystemUser);

    var token = Jwts.builder()
        .setSubject(subject)
        .addClaims(claims)
        .setIssuedAt(new Date())
        .setExpiration(new Date(
            now().plusMillis(jwtProperties.getTokenExpirationHours() * 60 * 60 * 1000)
                .toEpochMilli()))
        .signWith(SignatureAlgorithm.HS512, jwtProperties.getTokenSecret())
        .compact();

    return new AuthDto().setToken(token);
  }

  protected Map<String, String> intervuUserHeaders(Participant participant) {
    return intervuUserHeaders(participant.getPlatformId());
  }

  protected Map<String, String> intervuUserHeaders(String token) {
    return of("Authorization", "Bearer " + intervuAuthToken(token));
  }

  protected Map<String, String> intervuUserHeadersForSystemUser() {
    return of("Authorization", "Bearer " + intervuAuthTokenForSystemUser());
  }

  protected Map<String, String> roomParticipantHeaders(Participant participant) {
    return roomParticipantHeaders(participant.getInvitationToken());
  }

  protected Map<String, String> roomParticipantHeadersWithContent(Participant participant) {
    return of("X-Room-Token", roomAuthToken(participant.getInvitationToken()),
        HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
  }

  protected Map<String, String> roomParticipantHeaders(String token) {
    return of("X-Room-Token", roomAuthToken(token));
  }

  protected void sendWaitingRoomInternalChatMessage(Participant sender, String message)
      throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    waitForWs();
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .build();
    var handle = chatMetadataService.getWaitingRoomChatMetadata(auth)
        .getInternal();

    session.send(handle.getDestination(), new IncomingChatMessage(message));
    waitForWs();
  }

  protected void sendWaitingRoomRespondentsChatMessage(Participant sender, String message)
      throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    waitForWs();
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .role(sender.getRole())
        .build();
    var handle = chatMetadataService.getWaitingRoomChatMetadata(auth)
        .getRespondents();

    session.send(handle.getDestination(), new IncomingChatMessage(message));
    waitForWs();
  }

  protected void sendWaitingRoomRespondentsChatMessageSeen(Participant sender, UUID messageId)
      throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    waitForWs();
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .role(sender.getRole())
        .build();
    var handle = chatMetadataService.getWaitingRoomChatMetadata(auth)
        .getRespondents();

    session.send(handle.getDestinationForSeen(), new SeenChatMessage(messageId));
    waitForWs();
  }

  protected void sendMeetingRoomRespondentsChatMessageSeen(Participant sender, UUID messageId)
      throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    waitForWs();
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .role(sender.getRole())
        .build();
    var handle = chatMetadataService.getMeetingRoomChatMetadata(auth)
        .getRespondents();

    session.send(handle.getDestinationForSeen(), new SeenChatMessage(messageId));
    waitForWs();
  }

  protected void sendMeetingRoomPrivateChatMessageSeen(Participant sender,
                                                       Participant recipient,
                                                       UUID messageId) throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    waitForWs();
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .role(sender.getRole())
        .build();
    var handle = chatMetadataService.getMeetingRoomChatMetadata(auth)
        .getDirect().stream()
        .filter(metadata -> metadata.getParticipant().getId().equals(recipient.getId()))
        .findFirst().orElseThrow();

    session.send(handle.getDestinationForSeen(), new SeenChatMessage(messageId));
    waitForWs();
  }

  protected void sendMeetingRoomRespondentsChatMessage(Participant sender, String message)
      throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    waitForWs();
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .role(sender.getRole())
        .build();
    var handle = chatMetadataService.getMeetingRoomChatMetadata(auth)
        .getRespondents();

    session.send(handle.getDestination(), new IncomingChatMessage(message));
    waitForWs();
  }

  protected void sendMeetingRoomPrivateChatMessage(Participant sender,
                                                   Participant recipient,
                                                   String message) throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    waitForWs();
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .role(sender.getRole())
        .build();
    var handle = chatMetadataService.getMeetingRoomChatMetadata(auth)
        .getDirect().stream()
        .filter(metadata -> metadata.getParticipant().getId().equals(recipient.getId()))
        .findFirst().orElseThrow();

    session.send(handle.getDestination(), new IncomingChatMessage(message));
    waitForWs();
  }

  protected void waitForWs() {
    try {
      sleep(400);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  protected void sendBackRoomInternalChatMessage(Participant sender, String message)
      throws Exception {
    var researchSession = sender.getResearchSession();
    var session = connectRoomWs(sender);
    var auth = AuthenticatedParticipant.builder()
        .id(sender.getId())
        .roomId(researchSession.getId())
        .role(sender.getRole())
        .build();
    var handle = chatMetadataService.getMeetingRoomChatMetadata(auth)
        .getInternal();
    session.send(handle.getDestination(), new IncomingChatMessage(message));
    waitForWs();
  }

  protected StompSession connectWs(Participant participant) {
    String token = intervuAuthToken(participant.getPlatformId());
    String wsUrl = "ws://127.0.0.1:" + port + "/ws?access_token=" + token;

    stompClient = wsTestUtils.createWebSocketClient(mappingJackson2MessageConverter());
    try {
      return stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  protected StompSession connectWs(String accessToken) {
    String wsUrl = "ws://127.0.0.1:" + port + "/ws?access_token=" + accessToken;
    stompClient = wsTestUtils.createWebSocketClient(mappingJackson2MessageConverter());
    try {
      return stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  protected StompSession connectRoomWs(Participant participant) {
    String token = roomAuthToken(participant.getInvitationToken());
    String wsUrl = "ws://127.0.0.1:" + port + "/ws?x-room-token=" + token;
    stompClient = wsTestUtils.createWebSocketClient(mappingJackson2MessageConverter());
    try {
      return stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  protected void subscribe(StompSession session, String topic, StompFrameHandler handler) {
    session.subscribe(topic, handler);
    waitForWs();
  }

  protected ResearchSessionStateDataConsumer researchSessionStateDataConsumer(int messageNum) {
    return new ResearchSessionStateDataConsumer(
        connectionFactory,
        producerJackson2MessageConverter,
        messagingProperties.getResearchSessionStateUpdatesQueue(),
        messageNum)
        .start();
  }

  protected ResearchSession setUpFullRoom() {
    ResearchSessionData researchSessionData = sendCreateRoomEvent();
    return researchSessionRepository.findByPlatformId(researchSessionData.getId()).orElseThrow();
  }

  protected List<Participant> getAllParticipants(ResearchSession researchSession) {
    return List.of(
        getModerator(researchSession),
        getTranslator(researchSession),
        getObserver(researchSession),
        getRespondent(researchSession));
  }

  protected Participant getModerator(ResearchSession researchSession) {
    return getParticipant(researchSession, MODERATOR);
  }

  protected Participant getTranslator(ResearchSession researchSession) {
    return getParticipant(researchSession, TRANSLATOR);
  }

  protected Participant getObserver(ResearchSession researchSession) {
    return getParticipant(researchSession, OBSERVER);
  }

  protected Participant getRespondent(ResearchSession researchSession) {
    return getParticipant(researchSession, RESPONDENT);
  }

  protected Participant getAdmin(ResearchSession researchSession) {
    return researchSession.getParticipants().stream()
        .filter(Participant::isAdmin)
        .findFirst().orElseThrow();
  }

  protected Participant getProjectManager(ResearchSession researchSession) {
    return researchSession.getParticipants().stream()
        .filter(Participant::isProjectManager)
        .findFirst().orElseThrow();
  }

  protected Participant getProjectOperator(ResearchSession researchSession) {
    return researchSession.getParticipants().stream()
        .filter(Participant::isProjectOperator)
        .findFirst().orElseThrow();
  }

  protected Participant getParticipant(ResearchSession researchSession, ParticipantRole role) {
    return researchSession.getParticipants().stream()
        .filter(participant -> !participant.getPlatformId().equals("m0"))
        .filter(participant -> participant.getRole().equals(role))
        .findFirst().orElseThrow();
  }

  protected ResearchSessionProcessResultConsumer researchSessionProcessResultConsumer(
      int messageNum) {
    return new ResearchSessionProcessResultConsumer(
        connectionFactory,
        producerJackson2MessageConverter,
        messagingProperties.getResearchSessionProcessedQueue(),
        messageNum)
        .start();
  }

  protected ProjectSanitiseRequestConsumer projectSanitiseRequestConsumer(int messageNum) {
    return new ProjectSanitiseRequestConsumer(
        connectionFactory,
        producerJackson2MessageConverter,
        messagingProperties.getProjectSanitiseQueue(),
        messageNum)
        .start();
  }

  protected RespondentTestingResultDataConsumer respondentTestingResultsDataConsumer(
      int messageNum) {
    return new RespondentTestingResultDataConsumer(
        connectionFactory,
        producerJackson2MessageConverter,
        messagingProperties.getRespondentTestingQueue(),
        messageNum)
        .start();
  }

  protected ResearchSessionChatMessagesDataConsumer researchSessionChatMessagesDataConsumer(
      int messageNum) {
    return new ResearchSessionChatMessagesDataConsumer(
        connectionFactory,
        producerJackson2MessageConverter,
        messagingProperties.getResearchSessionChatMessagesQueue(),
        messageNum)
        .start();
  }

  protected BannedParticipantDataConsumer bannedParticipantDataConsumer(int messageNum) {
    return new BannedParticipantDataConsumer(
        connectionFactory,
        producerJackson2MessageConverter,
        messagingProperties.getParticipantBannedQueue(),
        messageNum)
        .start();
  }

  protected void purgeQueue(String queueName) {
    given()
        .urlEncodingEnabled(false)
        .auth()
        .basic(rmqUsername, rmqPassword)
        .contentType("application/json")
        .when()
        .delete(url(RMQ_PURGE_QUEUE, rabbitMngPort), queueName);
  }

  protected ResearchSessionData generate() {
    var moderator = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(MODERATOR)
        .setModerator(true);

    var respondent = new ResearchSessionData.Participant()
        .setId(make())
        .setInvitationToken(make())
        .setName(make())
        .setRole(RESPONDENT);
    var stimulus = new ResearchSessionData.Stimulus()
        .setId(make())
        .setName(make())
        .setPosition(1)
        .setThumbnail(make())
        .setType(DRAG_AND_DROP)
        .setData(make());
    var projectId = make();

    return new ResearchSessionData()
        .setId(make())
        .setName(make())
        .setState(SCHEDULED)
        .setStartsAt(now())
        .setEndsAt(now().plus(1, ChronoUnit.HOURS))
        .setVersion(1)
        .setPrivacy(false)
        .setCompositionLayout(CompositionLayout.GRID)
        .setAudioChannel(AudioChannel.NATIVE_AND_TRANSLATOR)
        .setProject(new ResearchSessionData.Project()
            .setId(projectId)
            .setProjectNumber(projectId)
            .setName(make())
            .setType(FULL_SERVICE))
        .setParticipants(List.of(moderator, respondent))
        .setStimuli(List.of(stimulus));
  }

  protected ResearchSessionData generateForUpdate(ResearchSessionData researchSessionData) {
    return researchSessionData
        .setName(make())
        .setVersion(researchSessionData.getVersion() + 1)
        .setParticipants(
            researchSessionData.getParticipants().stream()
                .map(participant -> participant.setName(make()))
                .collect(toList()))
        .setStimuli(
            researchSessionData.getStimuli().stream()
                .map(stimulus -> stimulus.setName(make()))
                .collect(toList()));
  }

  protected ResearchSessionData mapToResearchSessionData(ResearchSession researchSession) {
    return new ResearchSessionData()
        .setId(researchSession.getPlatformId())
        .setName(researchSession.getName())
        .setState(researchSession.getState())
        .setStartsAt(researchSession.getStartsAt())
        .setEndsAt(researchSession.getEndsAt())
        .setVersion(researchSession.getVersion())
        .setPrivacy(researchSession.isPrivacy())
        .setCompositionLayout(CompositionLayout.GRID)
        .setAudioChannel(AudioChannel.NATIVE_AND_TRANSLATOR)
        .setProject(new ResearchSessionData.Project()
            .setId(researchSession.getProject().getPlatformId())
            .setProjectNumber(researchSession.getProject().getProjectNumber())
            .setName(researchSession.getProject().getName())
            .setType(researchSession.getProject().getType()))
        .setParticipants(researchSession.getParticipants().stream()
            .map(participant -> new ResearchSessionData.Participant()
                .setId(participant.getPlatformId())
                .setName(participant.getDisplayName())
                .setInvitationToken(participant.getInvitationToken())
                .setRole(participant.getRole())
                .setModerator(participant.isModerator())
                .setBanable(participant.isBanable())
                .setAdmin(participant.isAdmin())).collect(toList()))
        .setStimuli(
            researchSession.getStimuli().stream().map(stimulus -> new ResearchSessionData.Stimulus()
                .setId(stimulus.getPlatformId())
                .setName(stimulus.getName())
                .setThumbnail(stimulus.getThumbnail())
                .setType(stimulus.getType())
                .setData(stimulus.getData())
                .setPosition(stimulus.getPosition())).collect(toList()));
  }

  protected ResearchSessionData sendCreateRoomEvent() {
    return sendCreateRoomEvent(make());
  }

  protected ResearchSessionData sendCreateRoomEvent(String moderatorPlatformId) {
    return sendCreateRoomEvent(moderatorPlatformId, CompositionLayout.GRID);
  }

  protected ResearchSessionData sendCreateRoomEvent(String moderatorPlatformId,
                                                    CompositionLayout layout) {
    var start = now().truncatedTo(HOURS);
    var end = now().plus(1, HOURS).truncatedTo(HOURS);

    var moderator0 = new ResearchSessionData.Participant()
        .setRole(MODERATOR)
        .setId("m0")
        .setInvitationToken(make())
        .setAdmin(true)
        .setModerator(true)
        .setBanable(false)
        .setName("Moderator 0");

    var moderator = new ResearchSessionData.Participant()
        .setId(moderatorPlatformId)
        .setInvitationToken(make())
        .setName(make())
        .setRole(MODERATOR)
        .setAdmin(false)
        .setBanable(true)
        .setGuest(true)
        .setProjectManager(true)
        .setProjectOperator(true)
        .setModerator(true);
    var id = make();
    var respondent = new ResearchSessionData.Participant()
        .setId(id)
        .setInvitationToken(id)
        .setName(make())
        .setRole(RESPONDENT)
        .setSpeakerPin(make());
    id = make();
    var translator = new ResearchSessionData.Participant()
        .setId(id)
        .setInvitationToken(id)
        .setName(make())
        .setRole(TRANSLATOR);
    id = make();
    var observer = new ResearchSessionData.Participant()
        .setId(id)
        .setInvitationToken(id)
        .setName(make())
        .setRole(OBSERVER);
    id = make();

    var stimulus1 = new ResearchSessionData.Stimulus()
        .setPosition(3)
        .setId(make())
        .setName("Stimulus 1 " + make())
        .setType(POLL)
        .setThumbnail("t1")
        .setData(format("{ key: %s }", make()));

    var stimulus2 = new ResearchSessionData.Stimulus()
        .setPosition(2)
        .setId(make())
        .setName("Stimulus 2 " + make())
        .setType(DRAG_AND_DROP)
        .setThumbnail("t2")
        .setData(format("{ key: %s }", make()));

    var stimulus3 = new ResearchSessionData.Stimulus()
        .setPosition(1)
        .setId(make())
        .setName("Stimulus 3 " + make())
        .setType(DOCUMENT_SHARING)
        .setThumbnail("t3")
        .setData(format("{ key: %s }", make()));

    var researchSessionData = new ResearchSessionData()
        .setId(make())
        .setName(make())
        .setState(SCHEDULED)
        .setStartsAt(start)
        .setEndsAt(end)
        .setVersion(1)
        .setPrivacy(false)
        .setCompositionLayout(layout)
        .setAudioChannel(AudioChannel.NATIVE_AND_TRANSLATOR)
        .setProject(new ResearchSessionData.Project()
            .setId(project.getPlatformId())
            .setProjectNumber(project.getProjectNumber())
            .setName(project.getName())
            .setType(project.getType()))
        .setParticipants(List.of(moderator0, moderator, respondent, translator, observer))
        .setStimuli(List.of(stimulus1, stimulus2, stimulus3))
        .setListenerPin(make())
        .setSpeakerPin(make())
        .setOperatorPin(make());

    rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(),
        researchSessionData);
    waitForWs();

    return researchSessionData;
  }

  protected ResearchSessionData sendUpdateRoomEvent(ResearchSessionData researchSessionData) {
    rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(),
        researchSessionData);
    waitForWs();

    return researchSessionData;
  }

  protected void startRoom(Participant participant) {
    given()
        .when()
        .headers(roomParticipantHeaders(participant))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());
  }

  protected void activateStimulus(Participant participant, UUID stimulusId) {
    given()
        .when()
        .headers(roomParticipantHeaders(participant))
        .put(url(ROOM_STIMULUS_ACTIVATE), stimulusId)
        .then()
        .statusCode(OK.value());
  }

  protected void deactivateStimulus(Participant participant, UUID stimulusId) {
    given()
        .when()
        .headers(roomParticipantHeaders(participant))
        .put(url(ROOM_STIMULUS_DEACTIVATE), stimulusId)
        .then()
        .statusCode(OK.value());
  }

  protected ParticipantRoomStateDto getRoomState(Participant participant) {
    return given()
        .when()
        .headers(roomParticipantHeaders(participant))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .extract().body().as(ParticipantRoomStateDto.class);
  }

  protected void finishRoom(Participant participant) {
    given()
        .when()
        .headers(roomParticipantHeaders(participant))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());
  }

  protected void enableStimulusInteraction(Participant caller) {
    given()
        .when()
        .headers(roomParticipantHeaders(caller))
        .put(url(STIMULUS_INTERACTION_ENABLE))
        .then()
        .statusCode(OK.value());
  }

  protected void disableStimulusInteraction(Participant caller) {
    given()
        .when()
        .headers(roomParticipantHeaders(caller))
        .put(url(STIMULUS_INTERACTION_DISABLE))
        .then()
        .statusCode(OK.value());
  }

  protected UUID addUserToRoom(ResearchSession researchSession, ParticipantRole role) {
    var researchSessionData = mapToResearchSessionData(researchSession);
    var id = randomUUID();
    var participant = new ResearchSessionData.Participant()
        .setId(id.toString())
        .setInvitationToken(id.toString())
        .setName(make())
        .setRole(role);

    researchSessionData.getParticipants().add(participant);
    sendUpdateRoomEvent(generateForUpdate(researchSessionData));
    return id;
  }

  protected StimulusActionState stimulusActionState(UUID stimulusId, UUID participantId) {
    return new StimulusActionState()
        .setStimulusId(stimulusId)
        .setParticipantId(participantId)
        .setContent(make());
  }

  protected RoomStateLog stimulusActivated(RoomState roomState) {
    return roomStateLog(roomState, STIMULUS_ACTIVATED);
  }

  protected RoomStateLog roomStateLog(RoomState roomState) {
    return roomStateLog(roomState, ROOM_STARTED);
  }

  protected RoomStateLog roomStateLog(RoomState roomState, RoomStateChangeType changeType) {
    return new RoomStateLog().setRoomId(roomState.getRoomId())
        .setRoomState(roomState)
        .setOffset(0L)
        .setVersion(now().toEpochMilli())
        .setAddedAt(now())
        .setChangeType(changeType);
  }

  protected String formatInstant(Instant instant) {
    var nano = instant.getNano();
    var newMicro = Math.round(nano / 1000.0);
    var newInstant = instant.with(MICRO_OF_SECOND, newMicro);

    return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'")
        .format(OffsetDateTime.ofInstant(newInstant, ZoneId.of("UTC")));
  }

  protected String getChatChannel(UUID handle) {
    return "/topic/chat." + handle.toString();
  }

  protected void connectToRoom(UUID roomId, Participant participant) {
    var roomChannel = communicationChannelService.getRoomChannel(roomId);
    var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
    var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
    var moderatorSession = connectRoomWs(participant);
    subscribe(moderatorSession, roomChannel, moderatorHandler);
  }

  protected void connectAllToRoom(ResearchSession researchSession) {
    researchSession.getParticipants().forEach(p -> {
      try {
        connectToRoom(researchSession.getId(), p);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });
  }

  protected Stimulus getPollStimulus(ResearchSession researchSession) {
    return researchSession.getStimuli().stream()
        .filter(stimulus -> POLL.equals(stimulus.getType()))
        .findFirst()
        .orElseThrow();
  }

  protected Stimulus getDragAndDropStimulus(ResearchSession researchSession) {
    return researchSession.getStimuli().stream()
        .filter(stimulus -> DRAG_AND_DROP.equals(stimulus.getType()))
        .findFirst()
        .orElseThrow();
  }

  protected Stimulus getDocumentSharingStimulus(ResearchSession researchSession) {
    return researchSession.getStimuli().stream()
        .filter(Stimulus::isDocumentSharing)
        .findFirst()
        .orElseThrow();
  }

  private ResearchSessionData sendCreateRoomEvent(ResearchSessionData.Participant... participants) {
    var start = now().truncatedTo(HOURS);
    var end = now().plus(1, HOURS).truncatedTo(HOURS);

    Arrays.stream(participants).forEach(p -> p.setSpeakerPin(make()));

    var stimulus = new ResearchSessionData.Stimulus()
        .setPosition(1)
        .setId(make())
        .setName("Stimulus " + make())
        .setType(POLL)
        .setThumbnail("t1")
        .setData(format("{ key: %s }", make()));

    var researchSessionData = new ResearchSessionData()
        .setId(make())
        .setName(make())
        .setState(SCHEDULED)
        .setStartsAt(start)
        .setEndsAt(end)
        .setVersion(1)
        .setPrivacy(false)
        .setCompositionLayout(CompositionLayout.GRID)
        .setListenerPin("" + Instant.now().toEpochMilli())
        .setOperatorPin("" + Instant.now().toEpochMilli() + 1)
        .setSpeakerPin("" + Instant.now().toEpochMilli() + 2)
        .setAudioChannel(AudioChannel.NATIVE_AND_TRANSLATOR)
        .setProject(new ResearchSessionData.Project()
            .setId(project.getPlatformId())
            .setProjectNumber(project.getProjectNumber())
            .setName(project.getName())
            .setType(project.getType()))
        .setParticipants(Arrays.asList(participants))
        .setStimuli(List.of(stimulus));

    rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(),
        researchSessionData);
    waitForWs();

    return researchSessionData;
  }

  protected ResearchSession setUpRoom(ResearchSessionData.Participant... participants) {
    ResearchSessionData researchSessionData = sendCreateRoomEvent(participants);

    return researchSessionRepository.findByPlatformId(researchSessionData.getId()).orElseThrow();
  }

  protected String parseDestination(String url, Object... args) {
    return String.format(url, args);
  }

  protected static String randomBookmarkNote() {
    return make(new Random().nextInt(BOOKMARK_NOTE_MAX_LENGTH) + 1);
  }

  protected AddBookmarkDto addBookmarkDto(String note, Long timestamp) {
    return new AddBookmarkDto().setNote(note).setTimestamp(timestamp);
  }

  protected AddBookmarkDto addBookmarkDto(String note) {
    return addBookmarkDto(note, new Random().nextLong());
  }

  protected AddBookmarkDto addBookmarkDto() {
    return addBookmarkDto(randomBookmarkNote());
  }

  protected AddPiiBookmarkDto addPiiBookmarkDto(String note, Long timestamp) {
    return new AddPiiBookmarkDto().setNote(note).setTimestamp(timestamp);
  }

  protected AddPiiBookmarkDto addPiiBookmarkDto(String note) {
    return addPiiBookmarkDto(note, new Random().nextLong());
  }

  protected AddPiiBookmarkDto addPiiBookmarkDto() {
    return addPiiBookmarkDto(randomBookmarkNote());
  }

  protected void verifyThatRoomHasStarted(Participant participant) {
    given()
        .when()
        .headers(roomParticipantHeaders(participant))
        .get(url(ROOM_INFO_URL))
        .then()
        .statusCode(OK.value())
        .body("roomState.state", equalTo(IN_PROGRESS.name()));
  }

  protected void verifyThatRoomHasFinished(Participant participant) {
    given()
        .when()
        .get(url(WAITING_ROOM_AUTHENTICATION_URL), participant.getInvitationToken())
        .then()
        .statusCode(FORBIDDEN.value())
        .body("code", equalTo(ROOM_FINISHED.name()));
  }

}
