package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.configuration.domain.TwilioProperties;
import com.focusvision.intervu.room.api.model.dto.ParticipantDialInInfoDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.restassured.common.mapper.TypeRef;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

/**
 * Integration tests for {@link RoomController#pinCodes(AuthenticatedParticipant)}.
 */
@DisplayName("Participants' PIN codes")
public class RoomControllerPinCodesIT extends HttpBasedIT {

  @Autowired
  private TwilioProperties twilioProperties;

  @Test
  @DisplayName("Unauthorized")
  void unauthorized() {
    given()
        .when()
        .get(url(ROOM_PIN_CODES_URL))
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE, names = "MODERATOR")
  @DisplayName("Forbidden for room participant")
  void forbiddenForParticipant(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    given()
        .when()
        .headers(roomParticipantHeaders(getParticipant(researchSession, role)))
        .get(url(ROOM_PIN_CODES_URL))
        .then()
        .statusCode(HttpStatus.FORBIDDEN.value());
  }

  @Test
  @DisplayName("Forbidden for unprivileged moderator")
  void forbiddenForUnprivilegedModerator() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    participantRepository.save(moderator
        .setAdmin(false)
        .setProjectOperator(false)
        .setProjectManager(false));

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(ROOM_PIN_CODES_URL))
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Ok, as admin")
  void okAsAdmin() {
    var researchSession = setUpFullRoom();
    var admin = getAdmin(researchSession);

    var result = given()
        .when()
        .headers(roomParticipantHeaders(admin))
        .get(url(ROOM_PIN_CODES_URL))
        .as(new TypeRef<List<ParticipantDialInInfoDto>>() {
        });

    var respondent = getRespondent(researchSession);
    var observer = getObserver(researchSession);
    var translator = getTranslator(researchSession);
    var moderator = getModerator(researchSession);

    String speakerPin = researchSession.getSpeakerPin();
    String listenerPin = researchSession.getListenerPin();

    assertAll(
        () -> assertThat(result).hasSize(5),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(respondent.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin)
            .containsExactly(respondent.getSpeakerPin()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::isAdmin).containsExactlyInAnyOrder(false),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::isProjectManager)
            .containsExactlyInAnyOrder(false),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::isProjectOperator)
            .containsExactlyInAnyOrder(false),

        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(observer.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isObserver)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsExactly(listenerPin),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::isAdmin).containsExactlyInAnyOrder(false),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::isProjectManager)
            .containsExactlyInAnyOrder(false),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::isProjectOperator)
            .containsExactlyInAnyOrder(false),

        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(translator.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsExactly(listenerPin),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::isAdmin).containsExactlyInAnyOrder(false),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::isProjectManager)
            .containsExactlyInAnyOrder(false),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::isProjectOperator)
            .containsExactlyInAnyOrder(false),

        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactlyInAnyOrder(moderator.getDisplayName(), "Moderator 0"),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin)
            .containsExactly(speakerPin, speakerPin),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getDialInNumbers)
            .containsExactly(
                twilioProperties.getDialInNumbers(),
                twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::isAdmin)
            .containsExactlyInAnyOrder(false, true),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::isProjectManager)
            .containsExactlyInAnyOrder(false, true),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::isProjectOperator)
            .containsExactlyInAnyOrder(false, true)
    );
  }

  @Test
  @DisplayName("Ok, as project manager")
  void okAsProjectManager() {
    var researchSession = setUpFullRoom();
    var projectManager = getProjectManager(researchSession);

    //Setting project operator flag to false in order for participant to be only PM
    participantRepository.save(projectManager.setProjectOperator(false));
    projectManager = participantRepository.getById(projectManager.getId());

    var result = given()
        .when()
        .headers(roomParticipantHeaders(projectManager))
        .get(url(ROOM_PIN_CODES_URL))
        .as(new TypeRef<List<ParticipantDialInInfoDto>>() {
        });

    var respondent = getRespondent(researchSession);
    var observer = getObserver(researchSession);
    var translator = getTranslator(researchSession);

    String speakerPin = researchSession.getSpeakerPin();
    String listenerPin = researchSession.getListenerPin();
    var finalProjectManager = projectManager;

    assertAll(
        () -> assertThat(result).hasSize(5),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(respondent.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin)
            .containsExactly(respondent.getSpeakerPin()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),

        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(observer.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isObserver)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsExactly(listenerPin),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),

        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(translator.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsExactly(listenerPin),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),

        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactlyInAnyOrder(finalProjectManager.getDisplayName(), "Moderator 0"),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin)
            .containsExactly(speakerPin, speakerPin),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getDialInNumbers)
            .containsExactly(
                twilioProperties.getDialInNumbers(),
                twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls()
    );
  }

  @Test
  @DisplayName("Ok, as project operator")
  void okAsProjectOperator() {
    var researchSession = setUpFullRoom();
    var projectOperator = getProjectOperator(researchSession);

    //Setting PM flag to false in order for participant to be only project operator
    participantRepository.save(projectOperator.setProjectManager(false));
    projectOperator = participantRepository.getById(projectOperator.getId());
    var result = given()
        .when()
        .headers(roomParticipantHeaders(projectOperator))
        .get(url(ROOM_PIN_CODES_URL))
        .as(new TypeRef<List<ParticipantDialInInfoDto>>() {
        });

    var respondent = getRespondent(researchSession);
    var observer = getObserver(researchSession);
    var translator = getTranslator(researchSession);

    String speakerPin = researchSession.getSpeakerPin();
    String listenerPin = researchSession.getListenerPin();
    var finalProjectOperator = projectOperator;

    assertAll(
        () -> assertThat(result).hasSize(5),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(respondent.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin)
            .containsExactly(respondent.getSpeakerPin()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isRespondent)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),

        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(observer.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isObserver)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsExactly(listenerPin),
        () -> assertThat(result).filteredOn(this::isObserver)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),

        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactly(translator.getDisplayName()),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .flatExtracting(ParticipantDialInInfoDto::getDialInNumbers)
            .isEqualTo(twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsExactly(listenerPin),
        () -> assertThat(result).filteredOn(this::isTranslator)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls(),

        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getDisplayName)
            .containsExactlyInAnyOrder(finalProjectOperator.getDisplayName(), "Moderator 0"),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getSpeakerPin)
            .containsExactly(speakerPin, speakerPin),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getDialInNumbers)
            .containsExactly(
                twilioProperties.getDialInNumbers(),
                twilioProperties.getDialInNumbers()),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getListenerPin).containsOnlyNulls(),
        () -> assertThat(result).filteredOn(this::isModerator)
            .extracting(ParticipantDialInInfoDto::getOperatorPin).containsOnlyNulls()
    );
  }

  private boolean isRespondent(ParticipantDialInInfoDto dto) {
    return hasRole(dto, RESPONDENT);
  }

  private boolean isModerator(ParticipantDialInInfoDto dto) {
    return hasRole(dto, MODERATOR);
  }

  private boolean isTranslator(ParticipantDialInInfoDto dto) {
    return hasRole(dto, TRANSLATOR);
  }

  private boolean isObserver(ParticipantDialInInfoDto dto) {
    return hasRole(dto, OBSERVER);
  }

  private boolean hasRole(ParticipantDialInInfoDto dto, ParticipantRole role) {
    return role.equals(dto.getRole());
  }

}
