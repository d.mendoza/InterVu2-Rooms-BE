package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantMicrophoneToggleStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.ParticipantMicrophoneToggleDto;
import com.focusvision.intervu.room.api.model.event.ParticipantMicrophoneToggleEvent;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.focusvision.intervu.room.api.model.event.EventType.PARTICIPANT_MICROPHONE_TOGGLE;
import static io.restassured.RestAssured.given;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NO_CONTENT;

/**
 * IT tests for {@link ParticipantController}.
 */
@DisplayName("Toggle participant's microphone")
class ParticipantControllerToggleMicIT extends HttpBasedIT {

    @Test
    @DisplayName("Caller not moderator")
    void callerNotModerator() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(PARTICIPANT_URL), respondent.getId().toString())
                .then()
                .statusCode(FORBIDDEN.value())
                .body("message", equalTo("Access is denied"));
    }

    @Test
    @DisplayName("Participant and moderator not on the same session")
    void participantAndModeratorNotOnSameSession() {
        given()
                .when()
                .headers(roomParticipantHeaders(getModerator(setUpFullRoom())))
                .put(url(PARTICIPANT_URL), getObserver(setUpFullRoom()).getId())
                .then()
                .statusCode(BAD_REQUEST.value())
                .body("message", equalTo("Moderator and participant are not in the same room."));
    }

    @Test
    @DisplayName("Participant not found")
    void participantNotFound() {
        given()
                .when()
                .headers(roomParticipantHeaders(getModerator(setUpFullRoom())))
                .put(url(PARTICIPANT_URL), randomUUID())
                .then()
                .statusCode(BAD_REQUEST.value())
                .body("message", equalTo("Moderator and participant are not in the same room."));
    }

    @Test
    @DisplayName("Toggle success")
    void success() throws InterruptedException, ExecutionException {
        var resultKeeper = new CompletableFuture<ParticipantMicrophoneToggleEvent>();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);

        var participantHandler = new ParticipantMicrophoneToggleStompFrameHandler(mapper, resultKeeper::complete);
        var session = connectRoomWs(respondent);
        subscribe(session, communicationChannelService.getParticipantChannel(respondent.getId()), participantHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(PARTICIPANT_URL), respondent.getId().toString())
                .then()
                .statusCode(NO_CONTENT.value());

        waitForWs();

        assertThat(participantHandler.getStates()).hasSize(1);
        assertThat(participantHandler.getStates().get(0).getEventType()).isEqualTo(PARTICIPANT_MICROPHONE_TOGGLE);
        assertThat(participantHandler.getStates().get(0).getData())
                .isEqualToComparingFieldByField(new ParticipantMicrophoneToggleDto());
    }
}
