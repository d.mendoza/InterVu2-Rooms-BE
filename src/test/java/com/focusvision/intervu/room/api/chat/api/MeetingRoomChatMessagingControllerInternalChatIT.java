package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils.OutgoingChatMessageStompFrameHandler;
import com.focusvision.intervu.room.api.chat.model.IncomingChatMessage;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.time.Instant.now;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * IT tests for meeting room internal chat functionality.
 *
 * @author Branko Ostojic
 */
@DisplayName("Meeting room internal chat")
class MeetingRoomChatMessagingControllerInternalChatIT extends HttpBasedIT {

    @Test
    @DisplayName("Subscribing forbidden for wrong room")
    void forbiddenForWrongRoom() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var session = connectRoomWs(moderator);
        var auth = AuthenticatedParticipant.builder()
                .id(moderator.getId())
                .roomId(setUpFullRoom().getId())
                .build();
        var handle = chatMetadataService.getMeetingRoomChatMetadata(auth)
                .getInternal();

        subscribe(
                session,
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        messagingTemplate.convertAndSend(handle.getDestination(), new OutgoingChatMessage());
        assertThrows(TimeoutException.class, () -> resultKeeper.get(500, MILLISECONDS));
    }

    @Test
    @DisplayName("Subscribing forbidden for intervu users")
    void forbiddenForIntervuUsers() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);

        var session = connectWs(moderator);

        subscribe(
                session,
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for respondent")
    void forbiddenForRespondent() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);

        var session = connectRoomWs(respondent);
        var auth = AuthenticatedParticipant.builder()
                .id(respondent.getId())
                .roomId(researchSession.getId())
                .build();
        var handle = chatMetadataService.getMeetingRoomChatMetadata(auth)
                .getInternal();

        subscribe(
                session,
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        session.send(handle.getDestination(), new OutgoingChatMessage());
        assertThrows(TimeoutException.class, () -> resultKeeper.get(500, MILLISECONDS));
    }

    @Test
    @DisplayName("Subscribing/messaging OK for internal users")
    void success() throws ExecutionException, InterruptedException, TimeoutException {
        var moderator1ResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var respondentResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var observerResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorFromOtherProjectResultKeeper = new CompletableFuture<OutgoingChatMessage>();

        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var researchSession2 = setUpFullRoom();
        var translator_2 = getTranslator(researchSession2);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        var translatorSession = connectRoomWs(translator);
        var observerSession = connectRoomWs(observer);
        var translatorFromOtherProjectSession = connectRoomWs(translator_2);

        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);
        connectToRoom(researchSession.getId(), translator);
        connectToRoom(researchSession.getId(), observer);

        moderatorSession.subscribe(
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(moderator1ResultKeeper::complete));
        respondentSession.subscribe(
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(respondentResultKeeper::complete));
        translatorSession.subscribe(
                getChatChannel(translator.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorResultKeeper::complete));
        translatorFromOtherProjectSession.subscribe(
                getChatChannel(translator_2.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorFromOtherProjectResultKeeper::complete));
        observerSession.subscribe(
                getChatChannel(observer.getId()),
                new OutgoingChatMessageStompFrameHandler(observerResultKeeper::complete));
        waitForWs();

        var message = new IncomingChatMessage(make());
        var auth = AuthenticatedParticipant.builder()
                .id(moderator.getId())
                .roomId(moderator.getResearchSession().getId())
                .build();
        var roomInternalChatHandle = chatMetadataService.getMeetingRoomChatMetadata(auth)
                .getInternal();
        moderatorSession.send(roomInternalChatHandle.getDestination(), message);
        waitForWs();
        var chatMessages = chatMessageRepository.findAllByHandle(roomInternalChatHandle.getHandle());
        var lastChatMessage = chatMessages.get(chatMessages.size() - 1);

        assertThat(lastChatMessage.getMessage()).isEqualTo(message.getMessage());
        assertThat(lastChatMessage.getHandle()).isEqualTo(roomInternalChatHandle.getHandle());
        assertThat(lastChatMessage.getSenderDisplayName()).isEqualTo(moderator.getDisplayName());
        assertThat(lastChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(lastChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));
        assertThat(lastChatMessage.getSource()).isEqualTo(ChatMessage.ChatSource.MEETING_ROOM);
        assertThat(lastChatMessage.getType()).isEqualTo(ChatType.INTERNAL);

        assertThat(moderator1ResultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(translatorResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(observerResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThrows(TimeoutException.class, () -> respondentResultKeeper.get(100, MILLISECONDS));
        assertThrows(TimeoutException.class,
                () -> translatorFromOtherProjectResultKeeper.get(100, MILLISECONDS));

        moderatorSession.disconnect();
        respondentSession.disconnect();
        translatorSession.disconnect();
        observerSession.disconnect();
        translatorFromOtherProjectSession.disconnect();
    }
}
