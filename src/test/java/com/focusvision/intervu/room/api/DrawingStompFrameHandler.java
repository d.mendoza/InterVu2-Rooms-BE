package com.focusvision.intervu.room.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.draw.model.DrawingNotificationMessage;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@RequiredArgsConstructor
public class DrawingStompFrameHandler implements StompFrameHandler {

    private final ObjectMapper mapper;
    private final List<DrawingNotificationMessage> messages = new ArrayList<>();

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return DrawingNotificationMessage.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info("received message: {} with headers: {}", payload, headers);

        DrawingNotificationMessage dto = mapper.convertValue(payload, DrawingNotificationMessage.class);
        messages.add(dto);
    }
}
