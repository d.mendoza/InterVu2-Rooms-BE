package com.focusvision.intervu.room.api.stimulus.snapshot;

import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.SNAPSHOT;
import static io.restassured.RestAssured.given;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link StimulusSnapshotController#createSnapshot}.
 */
@DisplayName("Stimulus snapshot")
class StimulusSnapshotControllerIT extends HttpBasedIT {

  @Test
  @DisplayName("Add snapshot - forbidden for unauthorized")
  void addSnapshot_forbiddenUnauthorized() {
    given()
        .when()
        .body(stimulusSnapshotFixture.addStimulusSnapshotDto())
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Add snapshot - forbidden for not started room")
  void addSnapshot_roomNotStarted() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(stimulusSnapshotFixture.addStimulusSnapshotDto())
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(BAD_REQUEST.value())
        .body("message", containsString("Room not running."));
  }

  @Test
  @DisplayName("Add snapshot - stimulus not found")
  void addSnapshot_stimulusNotFound() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(stimulusSnapshotFixture.addStimulusSnapshotDto())
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(NOT_FOUND.value());
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE,
      names = {"MODERATOR", "OBSERVER"})
  @DisplayName("Add snapshot - forbidden for role")
  void addSnapshot_forbiddenForRole(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    given()
        .when()
        .headers(roomParticipantHeadersWithContent(getParticipant(researchSession, role)))
        .body(stimulusSnapshotFixture.addStimulusSnapshotDto())
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE,
      names = {"RESPONDENT", "TRANSLATOR"})
  @DisplayName("Add snapshot - ok for role")
  void addSnapshot_okForRole(ParticipantRole role)
      throws ExecutionException, InterruptedException, TimeoutException {
    var addStimulusSnapshotDto = stimulusSnapshotFixture.addStimulusSnapshotDto();
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var observer = getObserver(researchSession);
    var translator = getTranslator(researchSession);
    var respondent = getRespondent(researchSession);
    var moderatorChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
    var observerChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
    var translatorChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
    var respondentChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();

    connectAllToRoom(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    var moderatorSession = connectRoomWs(moderator);
    moderatorSession.subscribe(
        getChatChannel(moderator.getId()),
        new WsTestUtils.OutgoingChatMessageStompFrameHandler(moderatorChatResultKeeper::complete));

    var observerSession = connectRoomWs(observer);
    observerSession.subscribe(
        getChatChannel(observer.getId()),
        new WsTestUtils.OutgoingChatMessageStompFrameHandler(observerChatResultKeeper::complete));

    var translatorSession = connectRoomWs(translator);
    translatorSession.subscribe(
        getChatChannel(translator.getId()),
        new WsTestUtils.OutgoingChatMessageStompFrameHandler(translatorChatResultKeeper::complete));

    var respondentSession = connectRoomWs(respondent);
    respondentSession.subscribe(
        getChatChannel(respondent.getId()),
        new WsTestUtils.OutgoingChatMessageStompFrameHandler(respondentChatResultKeeper::complete));

    given()
        .when()
        .headers(roomParticipantHeadersWithContent(getParticipant(researchSession, role)))
        .body(addStimulusSnapshotDto)
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL),
            researchSession.getStimuli().stream().map(Stimulus::getId).findFirst().orElseThrow())
        .then()
        .statusCode(CREATED.value())
        .body("size()", is(3))
        .body("id", notNullValue())
        .body("name", equalTo(addStimulusSnapshotDto.getName() + " - 1"))
        .body("timestamp", equalTo(addStimulusSnapshotDto.getTimestamp()));

    var moderatorChatMessage = moderatorChatResultKeeper.get(500, MILLISECONDS);
    var observerChatMessage = observerChatResultKeeper.get(500, MILLISECONDS);
    var translatorChatMessage = translatorChatResultKeeper.get(500, MILLISECONDS);

    assertAll(
        () -> assertThrows(TimeoutException.class,
            () -> respondentChatResultKeeper.get(500, MILLISECONDS)),
        () -> assertThat(moderatorChatMessage.getMessageType()).isEqualTo(SNAPSHOT),
        () -> assertThat(moderatorChatMessage.getMessage()).isEqualTo(
            addStimulusSnapshotDto.getName() + " - 1"),
        () -> assertThat(observerChatMessage.getMessageType()).isEqualTo(SNAPSHOT),
        () -> assertThat(observerChatMessage.getMessage()).isEqualTo(
            addStimulusSnapshotDto.getName() + " - 1"),
        () -> assertThat(translatorChatMessage.getMessageType()).isEqualTo(SNAPSHOT),
        () -> assertThat(translatorChatMessage.getMessage()).isEqualTo(
            addStimulusSnapshotDto.getName() + " - 1")
    );
  }

  @Test
  @DisplayName("Add snapshot - invalid data")
  void addSnapshot_invalidData() {
    var moderator = getModerator(setUpFullRoom());

    // empty name
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(new AddStimulusSnapshotDto())
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(BAD_REQUEST.value());

    // too long name
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(new AddStimulusSnapshotDto().setName(make(250)))
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(BAD_REQUEST.value());

    // empty data
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(new AddStimulusSnapshotDto().setName(make()))
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(BAD_REQUEST.value());

    // empty mimeType
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(new AddStimulusSnapshotDto().setName(make()).setData(make().getBytes()))
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(BAD_REQUEST.value());

    // mimeType too long
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(new AddStimulusSnapshotDto().setName(make()).setData(make().getBytes())
            .setMimeType(make(31)))
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(BAD_REQUEST.value());

    // empty timestamp
    given()
        .when()
        .headers(roomParticipantHeadersWithContent(moderator))
        .body(new AddStimulusSnapshotDto().setName(make()).setData(make().getBytes())
            .setMimeType(make()))
        .when()
        .post(url(STIMULI_ADD_SNAPSHOT_URL), randomUUID())
        .then()
        .statusCode(BAD_REQUEST.value());
  }
}
