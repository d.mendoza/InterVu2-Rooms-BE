package com.focusvision.intervu.room.api.twilio.service.impl;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.UUID.randomUUID;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.publisher.ParticipantPresenceEventPublisher;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.twilio.service.TwilioIdentityProvider;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TwilioStatusServiceImplTest {

  private TwilioStatusServiceImpl service;

  @Mock
  private ParticipantService participantService;

  @Mock
  private ParticipantPresenceEventPublisher presenceEventPublisher;

  @Mock
  private TwilioIdentityProvider twilioIdentityProvider;

  @BeforeEach
  public void setup() {
    service = new TwilioStatusServiceImpl(
        participantService,
        twilioIdentityProvider,
        presenceEventPublisher);
  }

  @Test
  @Disabled
  @DisplayName("Participant connected - handled for web participant")
  void handleParticipantConnected_handledForWebUser() {
    var participant = participant();
    var status = Map.of(
        "StatusCallbackEvent", "participant-connected",
        "RoomSid", "RM123",
        "ParticipantIdentity", participant.getId().toString()
    );

    when(participantService.findById(participant.getId()))
        .thenReturn(of(participant));

    service.processStatus(status);

    verify(presenceEventPublisher).publish(new ParticipantConnectedWithPhoneEvent(
        participant.getResearchSession().getId(), participant.getId()));
  }

  @Test
  @Disabled
  @DisplayName("Participant connected - handled for speaker (phone) participant")
  void handleParticipantConnected_handledForSpeaker() {
    var participant = participant();
    var status = Map.of(
        "StatusCallbackEvent", "participant-connected",
        "RoomSid", "RM123",
        "ParticipantIdentity", "speaker_" + participant.getId()
    );

    when(participantService.findById(participant.getId()))
        .thenReturn(of(participant));

    service.processStatus(status);

    verify(presenceEventPublisher).publish(new ParticipantConnectedWithPhoneEvent(
        participant.getResearchSession().getId(), participant.getId()));
  }

  @Test
  @Disabled
  @DisplayName("Participant connected - ignored for unknown participant")
  void handleParticipantConnected_ignoredForUnknown() {
    var participant = participant();
    var status = Map.of(
        "StatusCallbackEvent", "participant-connected",
        "RoomSid", "RM123",
        "ParticipantIdentity", participant.getId().toString()
    );

    when(participantService.findById(participant.getId()))
        .thenReturn(empty());

    service.processStatus(status);

    verifyNoInteractions(presenceEventPublisher);
  }

  @Test
  @Disabled
  @DisplayName("Participant connected - ignored for listener participant")
  void handleParticipantConnected_ignoredForListener() {
    var participant = participant();
    var status = Map.of(
        "StatusCallbackEvent", "participant-connected",
        "RoomSid", "RM123",
        "ParticipantIdentity", "listener_" + participant.getId()
    );

    service.processStatus(status);

    verifyNoInteractions(participantService, presenceEventPublisher);
  }

  @Test
  @DisplayName("Participant disconnected - not handled for web participant")
  void handleParticipantDisconnected_notHandledForWebParticipant() {
    var participant = participant();
    var status = Map.of(
        "StatusCallbackEvent", "participant-disconnected",
        "RoomSid", "RM123",
        "ParticipantIdentity", participant.getId().toString()
    );

    service.processStatus(status);

    verifyNoInteractions(presenceEventPublisher);
  }

  @Test
  @DisplayName("Participant disconnected - handled for speaker (phone) participant")
  void handleParticipantDisconnected_handledForSpeakerParticipant() {
    var participant = participant();
    var participantIdentity = "speaker_" + participant.getId();
    var status = Map.of(
        "StatusCallbackEvent", "participant-disconnected",
        "RoomSid", "RM123",
        "ParticipantIdentity", participantIdentity
    );

    when(participantService.findById(participant.getId()))
        .thenReturn(of(participant));
    when(twilioIdentityProvider.extractSpeakerIdentity(participantIdentity))
        .thenReturn(of(participant.getId()));

    service.processStatus(status);

    verify(presenceEventPublisher).publish(new ParticipantDisconnectedWithPhoneEvent(
        participant.getResearchSession().getId(), participant.getId()));
  }

  @Test
  @DisplayName("Participant disconnected - ignored for unknown participant")
  void handleParticipantDisconnected_ignoredForUnknownParticipant() {
    var participant = participant();
    var status = Map.of(
        "StatusCallbackEvent", "participant-disconnected",
        "RoomSid", "RM123",
        "ParticipantIdentity", participant.getId().toString()
    );

    service.processStatus(status);

    verifyNoInteractions(presenceEventPublisher);
  }

  @Test
  @DisplayName("Participant disconnected - ignored for listener (phone) participant")
  void handleParticipantDisconnected_ignoredForListenerParticipant() {
    var participant = participant();
    var status = Map.of(
        "StatusCallbackEvent", "participant-disconnected",
        "RoomSid", "RM123",
        "ParticipantIdentity", "listener_" + participant.getId()
    );

    service.processStatus(status);

    verifyNoInteractions(participantService, presenceEventPublisher);
  }

  private Participant participant() {
    return new Participant()
        .setId(randomUUID())
        .setResearchSession(
            new ResearchSession().setId(randomUUID()));
  }
}
