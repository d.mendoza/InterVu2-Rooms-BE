package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.model.CompositionLayout.GRID;
import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static com.focusvision.intervu.room.api.model.entity.Recording.State.PENDING;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.entity.Recording.State;
import com.focusvision.intervu.room.api.repository.ConferenceRepository;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.StimulusState;
import com.focusvision.intervu.room.api.state.repository.RoomStateLogRepository;
import com.focusvision.intervu.room.api.state.stimulus.repository.StimulusStateRepository;
import java.util.List;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link RoomRecordingController#regenerateAll}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Regenerate recordings")
class RoomRecordingControllerRegenerateRecordingsIT extends HttpBasedIT {

  @Autowired
  private ConferenceRepository conferenceRepository;
  @Autowired
  private StimulusStateRepository stimulusStateRepository;
  @Autowired
  private RoomStateLogRepository roomStateLogRepository;

  @Test
  @DisplayName("Unauthorized")
  void regenerateAll_unauthorized() {
    given()
        .when()
        .put(url(REGENERATE_ALL_RECORDINGS_URL), make())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Forbidden for participant")
  void regenerateAll_forbiddenForParticipant(ParticipantRole role) {
    var room = setUpFullRoom();
    var caller = getParticipant(room, role);
    given()
        .when()
        .headers(roomParticipantHeaders(caller))
        .put(url(REGENERATE_ALL_RECORDINGS_URL), RandomString.make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Forbidden for admin")
  void regenerateAll_forbiddenForAdmin() {
    var room = setUpFullRoom();
    var admin = getAdmin(room);
    given()
        .when()
        .headers(intervuUserHeaders(admin))
        .put(url(REGENERATE_ALL_RECORDINGS_URL), RandomString.make())
        .then()
        .statusCode(FORBIDDEN.value());
  }

  @Test
  @DisplayName("Conference not found")
  void regenerateAll_conferenceNotFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .put(url(REGENERATE_ALL_RECORDINGS_URL), RandomString.make())
        .then()
        .statusCode(NOT_FOUND.value());
  }

  @Test
  @DisplayName("Ok")
  void regenerateAll_ok() {
    var room = setUpFullRoom();
    var roomSid = make();
    var conference = conferenceRepository.save(new Conference()
        .setRoomSid(roomSid)
        .setResearchSession(room));

    room.setConference(conference);

    var respondent1 = getRespondent(room);
    var stimulus = room.getStimuli().get(0);
    var stimulusState = StimulusState.builder().broadcastResults(false).id(stimulus.getId())
        .build();
    var roomState = RoomState.builder().roomId(room.getId()).stimuli(List.of(stimulusState))
        .build();
    stimulusStateRepository.save(stimulusActionState(stimulus.getId(), respondent1.getId()));
    roomStateLogRepository.save(stimulusActivated(roomState));

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .put(url(REGENERATE_ALL_RECORDINGS_URL), room.getPlatformId())
        .then()
        .statusCode(NO_CONTENT.value());

    conference = conferenceRepository.getById(conference.getId());
    assertThat(conference.getRecordings()).extracting(Recording::getState)
        .containsExactly(PENDING, PENDING);
    assertThat(conference.getRecordings()).extracting(Recording::getCompositionLayout)
        .containsExactlyInAnyOrder(PICTURE_IN_PICTURE_WITH_COLUMN, GRID);
  }
}
