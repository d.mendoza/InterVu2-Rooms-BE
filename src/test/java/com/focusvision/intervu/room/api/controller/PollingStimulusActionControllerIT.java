package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_ANSWERED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_RANKED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_VOTED;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for poling stimulus events {@link PollingStimulusActionController}.
 */
@DisplayName("Poling stimulus events")
class PollingStimulusActionControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Subscribing forbidden for respondent")
    void forbiddenForRespondent() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectRoomWs(respondent);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for intervu users")
    void forbiddenForIntervuUsers() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Subscribing forbidden for wrong room")
    void messageForbiddenForWrongRoom() {
        var researchSession = setUpFullRoom();
        var researchSession1 = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession1.getId());
        var handler = new StimulusActionStompFrameHandler(mapper);

        var session = connectWs(moderator);

        subscribe(session, stimulusChannel, handler);

        assertThat(session.isConnected()).isFalse();
    }

    @Test
    @DisplayName("Vote")
    void vote() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);
        var voteContent = make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var voteAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(voteContent)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(voteContent))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(1))
                .body("[0].size()", Matchers.equalTo(3))
                .body("[0].participantId", equalTo(respondent.getId().toString()))
                .body("[0].content", equalTo(voteContent))
                .body("[0].stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto[].class);

        finishRoom(moderator);
        waitForWs();

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();

        assertThat(moderatorPollStimulusMessages.size()).isEqualTo(1);
        assertAll(
                () -> assertThat(moderatorPollStimulusMessages.get(0).getRoomId()).isEqualTo(researchSession.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getParticipantId()).isEqualTo(respondent.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getStimulusId()).isEqualTo(pollingStimulus.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getType()).isEqualTo(POLL_VOTED),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getContent()).isEqualTo(voteContent)
        );
    }

    @Test
    @DisplayName("Ranking")
    void ranking() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);
        var rankingContent = make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var rankingAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(rankingContent)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_RANKING_DESTINATION, rankingAction);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(rankingContent))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(1))
                .body("[0].size()", Matchers.equalTo(3))
                .body("[0].participantId", equalTo(respondent.getId().toString()))
                .body("[0].content", equalTo(rankingContent))
                .body("[0].stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto[].class);

        finishRoom(moderator);
        waitForWs();

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();

        assertThat(moderatorPollStimulusMessages.size()).isEqualTo(1);
        assertAll(
                () -> assertThat(moderatorPollStimulusMessages.get(0).getRoomId()).isEqualTo(researchSession.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getParticipantId()).isEqualTo(respondent.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getStimulusId()).isEqualTo(pollingStimulus.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getType()).isEqualTo(POLL_RANKED),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getContent()).isEqualTo(rankingContent)
        );
    }

    @Test
    @DisplayName("Answer")
    void answer() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);
        var answerContent = make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var action = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(answerContent)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, action);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(answerContent))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(1))
                .body("[0].size()", Matchers.equalTo(3))
                .body("[0].participantId", equalTo(respondent.getId().toString()))
                .body("[0].content", equalTo(answerContent))
                .body("[0].stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto[].class);

        finishRoom(moderator);
        waitForWs();

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();

        assertThat(moderatorPollStimulusMessages.size()).isEqualTo(1);
        assertAll(
                () -> assertThat(moderatorPollStimulusMessages.get(0).getRoomId()).isEqualTo(researchSession.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getParticipantId()).isEqualTo(respondent.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getStimulusId()).isEqualTo(pollingStimulus.getId()),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getType()).isEqualTo(POLL_ANSWERED),
                () -> assertThat(moderatorPollStimulusMessages.get(0).getContent()).isEqualTo(answerContent)
        );
    }

    @Test
    @DisplayName("Answer - validation errors")
    void answer_validationErrors() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);
        var answerContent = "";

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();

        // empty content
        var action = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(answerContent)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, action);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(null))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        // stimulus id empty
        action = new PollStimulusActionMessage()
                .setStimulusId(null)
                .setContent(make(3000))
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, action);
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(null))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        // timestamp empty
        action = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make(3000))
                .setTimestamp(null);
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, action);
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", equalTo(null))
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);
    }

    @Test
    @DisplayName("Vote - stimulus without interaction")
    void vote_stimulusWithoutInteraction() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);
        var voteContent = make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

        waitForWs();
        var voteAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(voteContent)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", nullValue())
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(0));

        finishRoom(moderator);
        waitForWs();

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();

        assertThat(moderatorPollStimulusMessages.size()).isEqualTo(0);
    }

    @Test
    @DisplayName("Rank - stimulus without interaction")
    void rank_stimulusWithoutInteraction() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);
        var rankingContent = make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

        waitForWs();
        var rankingAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(rankingContent)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_RANKING_DESTINATION, rankingAction);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", nullValue())
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(0));

        finishRoom(moderator);
        waitForWs();

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();

        assertThat(moderatorPollStimulusMessages.size()).isEqualTo(0);
    }

    @Test
    @DisplayName("answer - stimulus without interaction")
    void answer_stimulusWithoutInteraction() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);
        var answerContent = make();

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);

        waitForWs();
        var action = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(answerContent)
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_ANSWER_DESTINATION, action);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(RESPONDENT_POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", Matchers.equalTo(3))
                .body("participantId", equalTo(respondent.getId().toString()))
                .body("content", nullValue())
                .body("stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto.class);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(0));

        finishRoom(moderator);
        waitForWs();

        var moderatorPollStimulusMessages = moderatorPollStimulusActionHandler.getMessages();

        assertThat(moderatorPollStimulusMessages.size()).isEqualTo(0);
    }
}
