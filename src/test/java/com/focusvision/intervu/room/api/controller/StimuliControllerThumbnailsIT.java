package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasValue;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link StimuliController#thumbnails}.
 */
@DisplayName("Room stimuli list")
class StimuliControllerThumbnailsIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .get(url(ROOM_STIMULI_THUMBNAILS_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Stimuli bar data - ok for moderator")
    void stimuliThumbnails_ok() {
        var session = setUpFullRoom();
        given()
                .when()
                .headers(roomParticipantHeaders(getModerator(session)))
                .get(url(ROOM_STIMULI_THUMBNAILS_URL))
                .then()
                .statusCode(OK.value())
                .body("size()", is(1))
                .body("thumbnails", allOf(hasValue("t1"), hasValue("t2"), hasValue("t3")));
    }

}
