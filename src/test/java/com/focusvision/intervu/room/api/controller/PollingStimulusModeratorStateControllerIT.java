package com.focusvision.intervu.room.api.controller;

import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link PollStimulusController#stimulusStates(String, AuthenticatedParticipant)}.
 */
@DisplayName("Polling stimulus moderator state")
class PollingStimulusModeratorStateControllerIT extends HttpBasedIT {

  @Test
  @DisplayName("Forbidden for unauthorized")
  void forbiddenUnauthorized() {
    given()
        .when()
        .get(url(POLL_STIMULUS_STATE), make())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Unauthorized, banned moderators")
  void unauthorizedBannedModerator() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    participantRepository.save(moderator.setBanned(true));

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(POLL_STIMULUS_STATE), make())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @Test
  @DisplayName("Forbidden for respondent")
  void forbiddenForRespondent() {
    var researchSession = setUpFullRoom();
    var respondent = getRespondent(researchSession);
    var stimulus = researchSession.getStimuli().get(0);

    given()
        .when()
        .headers(roomParticipantHeaders(respondent))
        .get(url(POLL_STIMULUS_STATE), stimulus.getId())
        .then()
        .statusCode(HttpStatus.FORBIDDEN.value());
  }

  @Test
  @DisplayName("Stimuli state empty - ok for moderator")
  void stimuliBarData_okForBackroomUsersModerator() {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var stimulus = researchSession.getStimuli().get(0);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .get(url(POLL_STIMULUS_STATE), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", is(0));
  }

  @Test
  @DisplayName("Stimuli state empty - ok for observer")
  void stimuliBarData_okForBackroomUsersObserver() {
    var researchSession = setUpFullRoom();
    var observer = getObserver(researchSession);
    var stimulus = researchSession.getStimuli().get(0);

    given()
        .when()
        .headers(roomParticipantHeaders(observer))
        .get(url(POLL_STIMULUS_STATE), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", is(0));
  }

  @Test
  @DisplayName("Stimuli state empty - ok for translator")
  void stimuliBarData_okForBackroomUsersTranslator() {
    var researchSession = setUpFullRoom();
    var translator = getTranslator(researchSession);
    var stimulus = researchSession.getStimuli().get(0);

    given()
        .when()
        .headers(roomParticipantHeaders(translator))
        .get(url(POLL_STIMULUS_STATE), stimulus.getId())
        .then()
        .statusCode(OK.value())
        .body("size()", is(0));
  }

}
