package com.focusvision.intervu.room.api.draw.api;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_DRAWING_DISABLED;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.RoomParticipantDto;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link DrawingController#disable}.
 */
@DisplayName("Disable drawing for participant in room")
class DrawingControllerDisableDrawingIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participant")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for non moderators")
    void forbiddenForNonModerators() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Participant not in room")
    void participantNotInRoom() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var moderatorSession = connectRoomWs(moderator);
        var stimulus = researchSession.getStimuli().get(0);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_STIMULUS_ACTIVATE), stimulus.getId())
                .then()
                .statusCode(OK.value());

        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, communicationChannelService.getRoomChannel(researchSession.getId()), moderatorHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), UUID.randomUUID())
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    @DisplayName("Moderator")
    void enabled() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var stimulus = researchSession.getStimuli().get(0);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_STIMULUS_ACTIVATE), stimulus.getId())
                .then()
                .statusCode(OK.value());

        var moderatorSession = connectRoomWs(moderator);
        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, roomChannel, moderatorHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.participants.findAll { p -> p.id == '" + moderator.getId() + "' }[0].online", equalTo(true))
                .body("roomState.participants.findAll { p -> p.id == '" + moderator.getId() + "' }[0].drawingEnabled", equalTo(false))
                .body("participantState.drawingEnabled", equalTo(false));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANT_DRAW_ENABLE), moderator.getId())
                .then()
                .statusCode(HttpStatus.OK.value());

        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), moderator.getId())
                .then()
                .statusCode(HttpStatus.OK.value());

        waitForWs();
        var states = moderatorHandler.getStates().stream()
                .filter(genericRoomStateEvent -> genericRoomStateEvent.getEventType().equals(PARTICIPANT_DRAWING_DISABLED))
                .toList();

        assertAll(
                () -> assertThat(states.size()).isEqualTo(1),
                () -> assertThat(states.get(0).getDestination()).isEqualTo(roomChannel),
                () -> assertThat(states.get(0).getData().getParticipants())
                        .filteredOn(roomParticipantDto -> roomParticipantDto.getId().equals(moderator.getId()))
                        .extracting(RoomParticipantDto::isDrawingEnabled).containsOnly(false),
                () -> assertThat(states.get(0).getEventType()).isEqualTo(PARTICIPANT_DRAWING_DISABLED)
        );
    }

    @Test
    @DisplayName("Respondent")
    void respondent() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);
        var roomChannel = communicationChannelService.getRoomChannel(researchSession.getId());
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var respondentResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        var respondentHandler = new RoomStateStompFrameHandler(mapper, respondentResultKeeper::complete);
        var stimulus = researchSession.getStimuli().get(0);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_STIMULUS_ACTIVATE), stimulus.getId())
                .then()
                .statusCode(OK.value());

        var moderatorSession = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.participants.findAll { p -> p.id == '" + respondent.getId() + "' }[0].online", equalTo(true))
                .body("roomState.participants.findAll { p -> p.id == '" + respondent.getId() + "' }[0].drawingEnabled", equalTo(false))
                .body("participantState.drawingEnabled", equalTo(false));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANT_DRAW_ENABLE), respondent.getId())
                .then()
                .statusCode(HttpStatus.OK.value());

        waitForWs();

        subscribe(moderatorSession, roomChannel, moderatorHandler);
        subscribe(respondentSession, roomChannel, respondentHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_PARTICIPANT_DRAW_DISABLE), respondent.getId())
                .then()
                .statusCode(HttpStatus.OK.value());

        waitForWs();
        var states = respondentHandler.getStates().stream()
                .filter(genericRoomStateEvent -> genericRoomStateEvent.getEventType().equals(PARTICIPANT_DRAWING_DISABLED))
                .toList();

        assertAll(
                () -> assertThat(states.size()).isEqualTo(1),
                () -> assertThat(states.get(0).getDestination()).isEqualTo(roomChannel),
                () -> assertThat(states.get(0).getData().getParticipants())
                        .filteredOn(roomParticipantDto -> roomParticipantDto.getId().equals(respondent.getId()))
                        .extracting(RoomParticipantDto::isDrawingEnabled).containsOnly(false),
                () -> assertThat(states.get(0).getEventType()).isEqualTo(PARTICIPANT_DRAWING_DISABLED)
        );
    }
}
