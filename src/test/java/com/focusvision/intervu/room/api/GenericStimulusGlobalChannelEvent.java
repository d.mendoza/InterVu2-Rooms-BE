package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusGlobalDataType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the generic event on stimulus global channel.
 *
 */
@Getter
@Setter
@ToString
public class GenericStimulusGlobalChannelEvent {

    private StimulusGlobalDataType eventType;
    private String destination;
    private Object data;
}
