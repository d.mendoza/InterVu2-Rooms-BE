package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.GenericParticipantRoomStateEvent;
import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantRoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.IntervuUserDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CANCELED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CREATED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.CANCELED;
import static io.restassured.RestAssured.given;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for cancel room event WS integration.
 */
class RoomCanceledUpdateIT extends HttpBasedIT {

    @Test
    @DisplayName("Room canceled - InterVu user gets the WS event")
    void roomRoomCanceled_eventSentToDashboard() throws ExecutionException, InterruptedException, TimeoutException {
        var dummyRoom = setUpFullRoom();
        var moderator = getModerator(dummyRoom);
        var roomResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var newRoomResultKeeper = new CompletableFuture<GenericParticipantRoomStateEvent>();

        var intervuUser = given()
                .when()
                .headers(intervuUserHeaders(moderator.getPlatformId()))
                .get(url(DASHBOARD_USER_INFO_URL))
                .then()
                .statusCode(OK.value())
                .extract().body().as(IntervuUserDto.class);

        var participantGlobalHandler = new ParticipantRoomStateStompFrameHandler(mapper, newRoomResultKeeper::complete);
        var roomHandler = new RoomStateStompFrameHandler(mapper, roomResultKeeper::complete);
        var session = connectWs(intervuAuthToken(moderator.getPlatformId()));
        subscribe(session, intervuUser.getChannel(), participantGlobalHandler);

        var researchSessionData = sendCreateRoomEvent(moderator.getPlatformId());
        var newRoomEvent = newRoomResultKeeper.get(1, SECONDS);
        subscribe(session, newRoomEvent.getData().getParticipantState().getChannel().getRoom(), roomHandler);

        researchSessionData.setState(CANCELED);
        researchSessionData.setVersion(2);
        sendUpdateRoomEvent(researchSessionData);

        assertThat(roomHandler.getStates()).hasSize(1);
        assertThat(participantGlobalHandler.getStates()).hasSize(1);
        assertThat(participantGlobalHandler.getStates().get(0).getEventType()).isEqualByComparingTo(ROOM_CREATED);
        assertThat(roomHandler.getStates().get(0).getEventType()).isEqualByComparingTo(ROOM_CANCELED);
    }
}
