package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.common.model.AudioChannel.NATIVE;
import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.adapter.RecordingCheckSenderAdapter;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.messaging.ProcessPendingRecordingRequest;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.recording.infra.ProcessPendingRecordingRequestPublisher;
import com.focusvision.intervu.room.api.state.repository.RoomStateLogRepository;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

/**
 * IT tests for {@link RoomRecordingController#data}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Room recording data")
public class RoomRecordingControllerRecordingDataIT extends HttpBasedIT {

  @Autowired
  private RecordingCheckSenderAdapter adapter;

  @Autowired
  private ProcessPendingRecordingRequestPublisher processPendingRecordingRequestPublisher;

  @Autowired
  private RoomStateLogRepository roomStateLogRepository;

  @Test
  @DisplayName("Unauthorized")
  void unauthorized() {
    given()
        .when()
        .get(url(RECORDING_DATA), make())
        .then()
        .statusCode(HttpStatus.UNAUTHORIZED.value());
  }

  @ParameterizedTest
  @EnumSource(ParticipantRole.class)
  @DisplayName("Forbidden for room participant")
  void forbiddenForParticipant(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    given()
        .when()
        .headers(roomParticipantHeaders(getParticipant(researchSession, role)))
        .get(url(RECORDING_DATA), make())
        .then()
        .statusCode(HttpStatus.FORBIDDEN.value());
  }

  @Test
  @DisplayName("Forbidden for admin")
  void forbiddenForAdmin() {
    var researchSession = setUpFullRoom();
    var admin = getAdmin(researchSession);

    given()
        .when()
        .headers(intervuUserHeaders(admin))
        .get(url(RECORDING_DATA), make())
        .then()
        .statusCode(HttpStatus.FORBIDDEN.value());
  }

  @Test
  @DisplayName("Room not found")
  void roomNotFound() {
    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(RECORDING_DATA), make())
        .then()
        .statusCode(HttpStatus.NOT_FOUND.value());
  }

  @Test
  @DisplayName("Recording data not found for non-finished room")
  void notFoundForNonFinishedRoom() {
    var room = setUpFullRoom();

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(RECORDING_DATA), room.getPlatformId())
        .then()
        .statusCode(HttpStatus.NOT_FOUND.value());
  }

  @Test
  @DisplayName("Get room recording data - ok")
  void ok() throws InterruptedException, ExecutionException {
    var room = setUpFullRoom();
    var moderator = getModerator(room);
    var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
    var roomChannel = communicationChannelService.getRoomChannel(room.getId());
    var handler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);

    var consumer = researchSessionStateDataConsumer(3);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    var session = connectRoomWs(moderator);
    subscribe(session, roomChannel, handler);

    session.disconnect();
    waitForWs();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());

    waitForWs();

    var finalSession = researchSessionRepository.getById(room.getId());
    var conference = finalSession.getConference();
    var recording = conference.getRecordings().get(0);

    assertThat(recording.getAudioChannel()).isEqualTo(NATIVE);

    processPendingRecordingRequestPublisher.publish(new ProcessPendingRecordingRequest()
        .setRecordingId(recording.getId().toString()));
    waitForWs();

    var processingRecordingCheck = new ProcessingRecordingCheck()
        .setId(recording.getId().toString());
    adapter.send(processingRecordingCheck);

    consumer.allMessagesReceived(1, TimeUnit.SECONDS);
    consumer.stop();

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(RECORDING_DATA), finalSession.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("size()", is(1))
        .body("recordings.size()", is(1))
        .body("recordings[0].recordingUrl", notNullValue())
        .body("recordings[0].recordingDuration", equalTo(18000))
        .body("recordings[0].screensharePosition", nullValue())
        .body("recordings[0].audioChannel", equalTo("NATIVE"))
        .body("recordings[0].layout", equalTo("GRID"))
        .body("recordings[0].state", equalTo("COMPLETED"));
  }

  @Test
  @DisplayName("Get room recording data - ok, with screenshareBounds")
  void ok_withScreenShareBounds() throws InterruptedException {
    var researchSessionData = sendCreateRoomEvent(make(), PICTURE_IN_PICTURE_WITH_COLUMN);
    waitForWs();
    waitForWs();
    waitForWs();
    var room = researchSessionRepository.findByPlatformId(researchSessionData.getId())
        .orElseThrow();
    var moderator = getModerator(room);
    var documentStimulus = getDocumentSharingStimulus(room);

    var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
    var roomChannel = communicationChannelService.getRoomChannel(room.getId());
    var handler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
    var roomState = roomStateOperator.getCurrentRoomState(room.getId());
    roomStateLogRepository.save(stimulusActivated(roomState));

    var consumer = researchSessionStateDataConsumer(3);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    activateStimulus(moderator, documentStimulus.getId());
    waitForWs();

    var session = connectRoomWs(moderator);
    subscribe(session, roomChannel, handler);

    session.disconnect();
    waitForWs();

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_FINISH_URL))
        .then()
        .statusCode(OK.value());

    waitForWs();

    var finalSession = researchSessionRepository.getById(room.getId());
    var conference = finalSession.getConference();
    var recording = conference.getRecordings().stream()
        .filter(rec -> PICTURE_IN_PICTURE_WITH_COLUMN.equals(rec.getCompositionLayout()))
        .findAny()
        .get();

    assertThat(recording.getAudioChannel()).isEqualTo(NATIVE);

    processPendingRecordingRequestPublisher.publish(new ProcessPendingRecordingRequest()
        .setRecordingId(recording.getId().toString()));
    waitForWs();

    var processingRecordingCheck = new ProcessingRecordingCheck()
        .setId(recording.getId().toString());

    adapter.send(processingRecordingCheck);

    consumer.allMessagesReceived(1, TimeUnit.SECONDS);
    consumer.stop();

    given()
        .when()
        .headers(intervuUserHeadersForSystemUser())
        .get(url(RECORDING_DATA), finalSession.getPlatformId())
        .then()
        .statusCode(OK.value())
        .body("size()", is(1))
        .body("recordings.size()", is(2))
        .body("recordings[0].recordingUrl", nullValue())
        .body("recordings[0].recordingDuration", equalTo(0))
        .body("recordings[0].screensharePosition", nullValue())
        .body("recordings[0].layout", equalTo("GRID"))
        .body("recordings[0].state", equalTo("PENDING"))
        .body("recordings[0].audioChannel", equalTo("NATIVE"))
        .body("recordings[1].recordingUrl", notNullValue())
        .body("recordings[1].recordingDuration", equalTo(18000))
        .body("recordings[1].screensharePosition", notNullValue())
        .body("recordings[1].layout", equalTo("PICTURE_IN_PICTURE_WITH_COLUMN"))
        .body("recordings[1].state", equalTo("COMPLETED"))
        .body("recordings[1].audioChannel", equalTo("NATIVE"))
        .rootPath("recordings[1].screensharePosition.screenshareArea")
        .body("offsetX", equalTo(0))
        .body("offsetY", equalTo(0))
        .body("height", equalTo(720))
        .body("width", equalTo(1280))
        .detachRootPath("screenshareArea")
        .body("excludedAreas", nullValue())
        .detachRootPath("screensharePosition");
  }
}
