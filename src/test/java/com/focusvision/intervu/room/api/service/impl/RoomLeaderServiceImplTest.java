package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static java.util.UUID.randomUUID;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.state.RoomLeaderServiceImpl;
import com.focusvision.intervu.room.api.state.RoomStateProvider;
import com.focusvision.intervu.room.api.state.model.ParticipantState;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Tests for {@link RoomLeaderServiceImpl}.
 */
@ExtendWith(MockitoExtension.class)
@DisplayName("Room leader service")
class RoomLeaderServiceImplTest {

  private RoomLeaderServiceImpl service;

  @Mock
  private RoomStateProvider roomStateProvider;

  @BeforeEach
  public void setup() {
    service = new RoomLeaderServiceImpl(roomStateProvider);
  }

  @Test
  @DisplayName("Is room leader - no participants")
  void IsRoomLeader_noParticipants() {
    var roomId = UUID.randomUUID();
    var participantId = UUID.randomUUID();

    var roomStateLog = new RoomStateLog()
        .setRoomState(RoomState.builder().participants(List.of()).build());

    when(roomStateProvider.getCurrentForRoom(roomId)).thenReturn(roomStateLog);

    var result = service.isRoomLeader(roomId, participantId);

    assertThat(result).isFalse();
  }

  @Test
  @DisplayName("Is room leader - no moderators")
  void IsRoomLeader_noModerators() {
    var roomId = UUID.randomUUID();
    var participantId = UUID.randomUUID();

    var respondent = participantState(OBSERVER, false, 0L);
    var roomStateLog = new RoomStateLog().setRoomState(
        RoomState.builder().participants(List.of(respondent)).build());

    when(roomStateProvider.getCurrentForRoom(roomId)).thenReturn(roomStateLog);

    var result = service.isRoomLeader(roomId, participantId);

    assertThat(result).isFalse();
  }

  @Test
  @DisplayName("Is room leader - no moderator role")
  void IsRoomLeader_noModeratorRole() {
    var roomId = randomUUID();

    var respondent = participantState(OBSERVER, false, 10L);
    var observerWithMod1 = participantState(OBSERVER, true, 20L);
    var observerWithMod2 = participantState(OBSERVER, true, 10L);
    var observerWithMod3 = participantState(OBSERVER, true, 0L).setOnline(false);
    var observerWithMod4 = participantState(OBSERVER, true, 0L).setBanned(true);
    var participantList = List.of(respondent, observerWithMod1, observerWithMod2, observerWithMod3,
        observerWithMod4);
    var roomStateLog = new RoomStateLog()
        .setRoomState(RoomState.builder()
            .participants(participantList)
            .build());

    when(roomStateProvider.getCurrentForRoom(roomId)).thenReturn(roomStateLog);

    assertAll(
        () -> assertThat(service.isRoomLeader(roomId, observerWithMod1.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, observerWithMod2.getId())).isTrue(),
        () -> assertThat(service.isRoomLeader(roomId, observerWithMod3.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, observerWithMod4.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, respondent.getId())).isFalse()
    );
  }

  @Test
  @DisplayName("Is room leader - with moderator")
  void IsRoomLeader_withModerator() {
    var roomId = randomUUID();

    var respondent = participantState(OBSERVER, false, 0L);
    var observerWithMod1 = participantState(OBSERVER, true, 10L);
    var observerWithMod2 = participantState(OBSERVER, true, 0L);
    var moderator1 = participantState(MODERATOR, true, 20L);
    var moderator2 = participantState(MODERATOR, true, 30L);
    var moderator3 = participantState(MODERATOR, true, 0L).setOnline(false);
    var moderator4 = participantState(MODERATOR, true, 0L).setBanned(true);
    var participantList = List.of(respondent, observerWithMod1, observerWithMod2, moderator1,
        moderator2, moderator3, moderator4);
    var roomStateLog = new RoomStateLog().setRoomState(
        RoomState.builder().participants(participantList).build());

    when(roomStateProvider.getCurrentForRoom(roomId)).thenReturn(roomStateLog);

    assertAll(
        () -> assertThat(service.isRoomLeader(roomId, observerWithMod2.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, observerWithMod1.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, respondent.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, moderator1.getId())).isTrue(),
        () -> assertThat(service.isRoomLeader(roomId, moderator2.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, moderator3.getId())).isFalse(),
        () -> assertThat(service.isRoomLeader(roomId, moderator4.getId())).isFalse()
    );
  }

  private ParticipantState participantState(ParticipantRole role, boolean isModerator,
                                            Long firstEntryOffset) {
    return ParticipantState.builder()
        .role(role)
        .moderator(isModerator)
        .firstEntryOffset(firstEntryOffset)
        .id(randomUUID())
        .online(true)
        .build();
  }

}
