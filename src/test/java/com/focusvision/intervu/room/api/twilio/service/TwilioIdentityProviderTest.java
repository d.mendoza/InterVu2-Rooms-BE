package com.focusvision.intervu.room.api.twilio.service;

import static java.util.UUID.randomUUID;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class TwilioIdentityProviderTest {

  private TwilioIdentityProvider provider;

  @BeforeEach
  public void setup() {
    provider = new TwilioIdentityProvider();
  }

  @Test
  @DisplayName("Generate anonymous listener identity")
  void generateAnonymousListenerIdentity() {
    UUID sessionId = randomUUID();

    var identity = provider.generateAnonymousListenerIdentity(sessionId);

    assertThat(identity).startsWith("listener_" + sessionId + "_");
  }

  @Test
  @DisplayName("Generate anonymous listener identity - non repeating")
  void generateAnonymousListenerIdentity_nonRepeating() {
    UUID sessionId = randomUUID();

    var identity1 = provider.generateAnonymousListenerIdentity(sessionId);
    var identity2 = provider.generateAnonymousListenerIdentity(sessionId);

    assertThat(identity1).isNotEqualTo(identity2);
  }

  @Test
  @DisplayName("Generate anonymous speaker identity")
  void generateAnonymousSpeakerIdentity() {
    UUID sessionId = randomUUID();

    var identity = provider.generateAnonymousSpeakerIdentity(sessionId);

    assertThat(identity).startsWith("speaker_" + sessionId + "_");
  }

  @Test
  @DisplayName("Generate anonymous speaker identity - non repeating")
  void generateAnonymousSpeakerIdentity_nonRepeating() {
    UUID sessionId = randomUUID();

    var identity1 = provider.generateAnonymousSpeakerIdentity(sessionId);
    var identity2 = provider.generateAnonymousSpeakerIdentity(sessionId);

    assertThat(identity1).isNotEqualTo(identity2);
  }

  @Test
  @DisplayName("Generate anonymous operator identity")
  void generateAnonymousOperatorIdentity() {
    UUID sessionId = randomUUID();

    var identity = provider.generateAnonymousOperatorIdentity(sessionId);

    assertThat(identity).startsWith("operator_" + sessionId + "_");
  }

  @Test
  @DisplayName("Generate anonymous operator identity - non repeating")
  void generateAnonymousOperatorIdentity_nonRepeating() {
    UUID sessionId = randomUUID();

    var identity1 = provider.generateAnonymousOperatorIdentity(sessionId);
    var identity2 = provider.generateAnonymousOperatorIdentity(sessionId);

    assertThat(identity1).isNotEqualTo(identity2);
  }

  @Test
  @DisplayName("Generate speaker identity")
  void generateParticipantSpeakerIdentity() {
    UUID speakerId = randomUUID();

    var identity = provider.generateSpeakerIdentity(speakerId);

    assertThat(identity).startsWith("speaker_" + speakerId);
  }

  @Test
  @DisplayName("Generate speaker identity - consistent identity")
  void generateParticipantSpeakerIdentity_consistentIdentity() {
    UUID speakerId = randomUUID();

    var identity1 = provider.generateSpeakerIdentity(speakerId);
    var identity2 = provider.generateSpeakerIdentity(speakerId);

    assertThat(identity1).isEqualTo(identity2);
  }

  @Test
  @DisplayName("Participant is listener")
  void participantIsListener() {
    String participantIdentity = "listener_" + randomUUID();

    var isListener = provider.isListener(participantIdentity);

    assertThat(isListener).isTrue();
  }

  @Test
  @DisplayName("Participant is not a listener")
  void participantIsNotListener() {
    String participantIdentity = randomUUID().toString();

    var isListener = provider.isListener(participantIdentity);

    assertThat(isListener).isFalse();
  }

  @ParameterizedTest
  @ValueSource(strings = {"speaker_", "listener_", "operator_"})
  @DisplayName("Participant is a phone participant")
  void participantIsNotPhoneParticipant(String prefix) {
    String participantIdentity = prefix + randomUUID();

    var isListener = provider.isPhoneParticipant(participantIdentity);

    assertThat(isListener).isTrue();
  }

  @Test
  @DisplayName("Participant is not a phone participant")
  void participantIsNotPhoneParticipant() {
    String participantIdentity = randomUUID().toString();

    var isListener = provider.isPhoneParticipant(participantIdentity);

    assertThat(isListener).isFalse();
  }

  @Test
  @DisplayName("Extract speaker identity - not a speaker")
  void extractSpeakerIdentity_notSpeaker() {
    var identity = provider.extractSpeakerIdentity(make());

    assertThat(identity).isNotPresent();
  }

  @Test
  @DisplayName("Extract speaker identity - anonymous speaker")
  void extractSpeakerIdentity_anonymousSpeaker() {
    String participantIdentity = "speaker_" + randomUUID() + "_" + randomUUID();

    var identity = provider.extractSpeakerIdentity(participantIdentity);

    assertThat(identity).isNotPresent();
  }

  @Test
  @DisplayName("Extract speaker identity - speaker")
  void extractSpeakerIdentity_speaker() {
    UUID participantId = randomUUID();
    String participantIdentity = "speaker_" + participantId;

    var identity = provider.extractSpeakerIdentity(participantIdentity);

    assertThat(identity).contains(participantId);
  }

}
