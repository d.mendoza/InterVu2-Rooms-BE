package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;

import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * IT tests for WS integration.
 *
 * @author Branko Ostojic
 */
class WSIntegrationIT extends HttpBasedIT {

    @Test
    @DisplayName("Connecting forbidden for unauthorized")
    void connect_fail() {
        String wsUrl = "ws://127.0.0.1:" + port + "/ws";
        stompClient = wsTestUtils.createWebSocketClient(mappingJackson2MessageConverter());

        assertThrows(ExecutionException.class,
                () -> stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get());
    }

    @Test
    @DisplayName("Connecting forbidden for banned participant")
    void connect_unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        assertThrows(RuntimeException.class, () -> connectRoomWs(moderator));
    }

    @Test
    @DisplayName("Connecting forbidden for invalid authentication")
    void connect_failInvalidAuth() {
        String wsUrl = "ws://127.0.0.1:" + port + "/ws?x-room-token=" + make();
        stompClient = wsTestUtils.createWebSocketClient(mappingJackson2MessageConverter());
        assertThrows(ExecutionException.class,
                () -> stompClient.connect(wsUrl, new WsTestUtils.MyStompSessionHandler()).get());
    }

    @Test
    @DisplayName("Connecting success")
    void connect_success() throws Exception {
        var session1 = connectRoomWs(getModerator(setUpFullRoom()));

        assertThat(session1.isConnected()).isTrue();
    }
}
