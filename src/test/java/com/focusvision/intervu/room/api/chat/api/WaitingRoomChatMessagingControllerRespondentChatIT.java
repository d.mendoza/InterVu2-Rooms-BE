package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils.OutgoingChatMessageStompFrameHandler;
import com.focusvision.intervu.room.api.chat.model.IncomingChatMessage;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static java.time.Instant.now;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * IT tests for waiting room chat functionality {@link WaitingRoomChatMessagingController#respondents}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Waiting Room respondent chat messaging")
class WaitingRoomChatMessagingControllerRespondentChatIT extends HttpBasedIT {

    @Test
    @DisplayName("Message not received for forbidden topic")
    void messageNotReceivedForForWrongChatTopic() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var session = connectRoomWs(moderator);
        var auth = AuthenticatedParticipant.builder()
                .id(moderator.getId())
                .role(ParticipantRole.MODERATOR)
                .canModerate(true)
                .roomId(setUpFullRoom().getId())
                .build();
        var handle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getRespondents();

        subscribe(
                session,
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        messagingTemplate.convertAndSend(handle.getDestination(), new OutgoingChatMessage());
        assertThrows(TimeoutException.class, () -> resultKeeper.get(500, MILLISECONDS));
    }

    @Test
    @DisplayName("Messaging forbidden for translator")
    void forbiddenForTranslator() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var translator = getTranslator(researchSession);
        var session = connectRoomWs(translator);

        subscribe(
                session,
                getChatChannel(translator.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        session.send("/app/chat.waiting-room.respondents", new OutgoingChatMessage());
        assertThrows(TimeoutException.class, () -> resultKeeper.get(500, MILLISECONDS));
    }

    @Test
    @DisplayName("Messaging forbidden for observer")
    void forbiddenForObserver() {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var observer = getTranslator(researchSession);
        var session = connectRoomWs(observer);

        subscribe(
                session,
                getChatChannel(observer.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        session.send("/app/chat.waiting-room.respondents", new OutgoingChatMessage());
        assertThrows(TimeoutException.class, () -> resultKeeper.get(500, MILLISECONDS));
    }

    @Test
    @DisplayName("Message received for proper topic")
    void messageReceivedForProperTopic() throws ExecutionException, InterruptedException, TimeoutException {
        var resultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var session = connectRoomWs(respondent);
        var auth = AuthenticatedParticipant.builder()
                .id(respondent.getId())
                .role(RESPONDENT)
                .roomId(researchSession.getId())
                .build();
        var chatTopic = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getRespondents();

        connectToRoom(researchSession.getId(), respondent);

        subscribe(
                session,
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(resultKeeper::complete));

        var message = new OutgoingChatMessage()
                .setMessage(make())
                .setSenderId(make())
                .setSenderName(make())
                .setHandle(randomUUID())
                .setSentAt(now().toEpochMilli());

        session.send(chatTopic.getDestination(), message);
        assertThat(resultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
    }

    @Test
    @DisplayName("Respondent send message to moderators")
    void respondentSendsMessageToModerators() throws ExecutionException, InterruptedException, TimeoutException {
        var moderator1ResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var respondentResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var observerResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorFromOtherProjectResultKeeper = new CompletableFuture<OutgoingChatMessage>();

        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var translator2 = getTranslator(setUpFullRoom());

        var moderator1Session = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        var translatorSession = connectRoomWs(translator);
        var observerSession = connectRoomWs(observer);
        var translatorFromOtherProjectSession = connectRoomWs(translator2);

        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);
        connectToRoom(researchSession.getId(), translator);
        connectToRoom(researchSession.getId(), observer);

        moderator1Session.subscribe(
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(moderator1ResultKeeper::complete));
        respondentSession.subscribe(
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(respondentResultKeeper::complete));
        translatorSession.subscribe(
                getChatChannel(translator.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorResultKeeper::complete));
        translatorFromOtherProjectSession.subscribe(
                getChatChannel(translator2.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorFromOtherProjectResultKeeper::complete));
        observerSession.subscribe(
                getChatChannel(observer.getId()),
                new OutgoingChatMessageStompFrameHandler(observerResultKeeper::complete));
        waitForWs();

        var auth = AuthenticatedParticipant.builder()
                .id(respondent.getId())
                .roomId(researchSession.getId())
                .role(RESPONDENT)
                .build();
        var waitingRoomRespondentsChatHandle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getRespondents();
        var message = new IncomingChatMessage(make());

        respondentSession.send(waitingRoomRespondentsChatHandle.getDestination(), message);
        waitForWs();
        var chatMessages = chatMessageRepository.findAllByHandle(waitingRoomRespondentsChatHandle.getHandle());
        var lastChatMessage = chatMessages.get(chatMessages.size() - 1);

        assertThat(lastChatMessage.getMessage()).isEqualTo(message.getMessage());
        assertThat(lastChatMessage.getHandle()).isEqualTo(waitingRoomRespondentsChatHandle.getHandle());
        assertThat(lastChatMessage.getSenderDisplayName()).isEqualTo(respondent.getDisplayName());
        assertThat(lastChatMessage.getSenderPlatformId()).isEqualTo(respondent.getPlatformId());
        assertThat(lastChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));
        assertThat(lastChatMessage.getSource()).isEqualTo(ChatMessage.ChatSource.WAITING_ROOM);
        assertThat(lastChatMessage.getType()).isEqualTo(ChatType.RESPONDENTS);

        assertThat(moderator1ResultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(respondentResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThrows(TimeoutException.class, () -> observerResultKeeper.get(100, MILLISECONDS));
        assertThrows(TimeoutException.class, () -> translatorResultKeeper.get(100, MILLISECONDS));
        assertThrows(TimeoutException.class,
                () -> translatorFromOtherProjectResultKeeper.get(100, MILLISECONDS));

        moderator1Session.disconnect();
        respondentSession.disconnect();
        translatorSession.disconnect();
        observerSession.disconnect();
        translatorFromOtherProjectSession.disconnect();
    }

    @Test
    @DisplayName("Moderator send message to respondent")
    void moderatorSendsMessageToRespondent() throws ExecutionException, InterruptedException, TimeoutException {
        var moderator1ResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var respondentResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var observerResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var translatorFromOtherProjectResultKeeper = new CompletableFuture<OutgoingChatMessage>();

        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var moderator = getModerator(researchSession);
        var translator = getTranslator(researchSession);
        var translator2 = getTranslator(setUpFullRoom());
        var observer = getObserver(researchSession);

        var moderator1Session = connectRoomWs(moderator);
        var respondentSession = connectRoomWs(respondent);
        var translatorSession = connectRoomWs(translator);
        var observerSession = connectRoomWs(observer);
        var translatorFromOtherProjectSession = connectRoomWs(translator2);

        connectToRoom(researchSession.getId(), moderator);
        connectToRoom(researchSession.getId(), respondent);
        connectToRoom(researchSession.getId(), translator);
        connectToRoom(researchSession.getId(), observer);

        moderator1Session.subscribe(
                getChatChannel(moderator.getId()),
                new OutgoingChatMessageStompFrameHandler(moderator1ResultKeeper::complete));
        respondentSession.subscribe(
                getChatChannel(respondent.getId()),
                new OutgoingChatMessageStompFrameHandler(respondentResultKeeper::complete));
        translatorSession.subscribe(
                getChatChannel(translator.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorResultKeeper::complete));
        translatorFromOtherProjectSession.subscribe(
                getChatChannel(translator2.getId()),
                new OutgoingChatMessageStompFrameHandler(translatorFromOtherProjectResultKeeper::complete));
        observerSession.subscribe(
                getChatChannel(observer.getId()),
                new OutgoingChatMessageStompFrameHandler(observerResultKeeper::complete));
        waitForWs();

        var auth = AuthenticatedParticipant.builder()
                .id(moderator.getId())
                .role(ParticipantRole.MODERATOR)
                .canModerate(true)
                .roomId(researchSession.getId())
                .build();
        var waitingRoomRespondentsChatHandle = chatMetadataService.getWaitingRoomChatMetadata(auth)
                .getRespondents();
        var message = new IncomingChatMessage(make());

        moderator1Session.send(waitingRoomRespondentsChatHandle.getDestination(), message);
        waitForWs();
        var chatMessages = chatMessageRepository.findAllByHandle(waitingRoomRespondentsChatHandle.getHandle());
        var lastChatMessage = chatMessages.get(chatMessages.size() - 1);

        assertThat(lastChatMessage.getMessage()).isEqualTo(message.getMessage());
        assertThat(lastChatMessage.getHandle()).isEqualTo(waitingRoomRespondentsChatHandle.getHandle());
        assertThat(lastChatMessage.getSenderDisplayName()).isEqualTo(moderator.getDisplayName());
        assertThat(lastChatMessage.getSenderPlatformId()).isEqualTo(moderator.getPlatformId());
        assertThat(lastChatMessage.getSentAt()).isCloseTo(now(), within(2, ChronoUnit.SECONDS));
        assertThat(lastChatMessage.getSource()).isEqualTo(ChatMessage.ChatSource.WAITING_ROOM);
        assertThat(lastChatMessage.getType()).isEqualTo(ChatType.RESPONDENTS);

        // All moderators and respondents should receive message.
        assertThat(respondentResultKeeper.get(500, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThat(moderator1ResultKeeper.get(100, MILLISECONDS).getMessage()).isEqualTo(message.getMessage());
        assertThrows(TimeoutException.class, () -> translatorResultKeeper.get(100, MILLISECONDS));
        assertThrows(TimeoutException.class,
                () -> translatorFromOtherProjectResultKeeper.get(100, MILLISECONDS));

        moderator1Session.disconnect();
        respondentSession.disconnect();
        translatorSession.disconnect();
        observerSession.disconnect();
        translatorFromOtherProjectSession.disconnect();
    }
}
