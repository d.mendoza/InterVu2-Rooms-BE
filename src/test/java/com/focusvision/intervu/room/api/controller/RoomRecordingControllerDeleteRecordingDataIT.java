package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.adapter.RecordingCheckSenderAdapter;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for {@link RoomRecordingController#deleteRecordings}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Delete room recording data")
public class RoomRecordingControllerDeleteRecordingDataIT extends HttpBasedIT {

    @Autowired
    private RecordingCheckSenderAdapter adapter;

    @Test
    @DisplayName("Unauthorized")
    void unauthorized() {
        given()
                .when()
                .delete(url(RECORDING_DATA), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @ParameterizedTest
    @EnumSource(ParticipantRole.class)
    @DisplayName("Forbidden for room participant")
    void forbiddenForParticipant(ParticipantRole role) {
        var researchSession = setUpFullRoom();
        given()
                .when()
                .headers(roomParticipantHeaders(getParticipant(researchSession, role)))
                .delete(url(RECORDING_DATA), make())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Forbidden for admin")
    void forbiddenForAdmin() {
        var researchSession = setUpFullRoom();
        var admin = getAdmin(researchSession);
        given()
                .when()
                .headers(intervuUserHeaders(admin))
                .delete(url(RECORDING_DATA), make())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Room not found")
    void roomNotFound() {
        given()
                .when()
                .headers(intervuUserHeadersForSystemUser())
                .delete(url(RECORDING_DATA), make())
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    @DisplayName("Room not started, nothing deleted")
    void roomNotStarted() {
        var room = setUpFullRoom();

        given()
                .when()
                .headers(intervuUserHeadersForSystemUser())
                .delete(url(RECORDING_DATA), room.getPlatformId())
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    @DisplayName("Room not finished, nothing deleted")
    void roomNotFinished() {
        var room = setUpFullRoom();
        var moderator = getModerator(room);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(intervuUserHeadersForSystemUser())
                .delete(url(RECORDING_DATA), room.getPlatformId())
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    @DisplayName("Delete room recording data - ok")
    void ok() throws InterruptedException {
        var room = setUpFullRoom();
        var moderator = getModerator(room);

        var consumer = researchSessionStateDataConsumer(3);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_FINISH_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        var finalSession = researchSessionRepository.getById(room.getId());
        var conference = finalSession.getConference();
        var recording = conference.getRecordings().get(0);
        var processingRecordingCheck = new ProcessingRecordingCheck()
            .setId(recording.getId().toString());

        adapter.send(processingRecordingCheck);

        consumer.allMessagesReceived(1, TimeUnit.SECONDS);
        consumer.stop();

        given()
                .when()
                .headers(intervuUserHeadersForSystemUser())
                .delete(url(RECORDING_DATA), finalSession.getPlatformId())
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }
}
