package com.focusvision.intervu.room.api.export.api;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_ONLINE;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CREATED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_FINISHED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_STARTED;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.focusvision.intervu.room.api.HttpBasedIT;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

/**
 * IT tests for {@link RoomExportController}.
 */
class RoomExportControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Unauthorized")
    void roomStateLogs_unauthorized() {
        given()
                .when()
                .get(url(EXPORT_ROOM_STATE), make())
                .then()
                .statusCode(UNAUTHORIZED.value());
    }

    @ParameterizedTest
    @EnumSource(ParticipantRole.class)
    @DisplayName("Forbidden for room participant")
    void forbiddenForParticipant(ParticipantRole role) {
        var researchSession = setUpFullRoom();

        given()
                .when()
                .headers(roomParticipantHeaders(getParticipant(researchSession, role)))
                .get(url(EXPORT_ROOM_STATE), make())
                .then()
                .statusCode(FORBIDDEN.value());
    }

    @Test
    @DisplayName("Forbidden for admin")
    void forbiddenForAdmin() {
        var researchSession = setUpFullRoom();
        var admin = getAdmin(researchSession);

        given()
                .when()
                .headers(intervuUserHeaders(admin))
                .get(url(EXPORT_ROOM_STATE), make())
                .then()
                .statusCode(FORBIDDEN.value());
    }

    @Test
    @DisplayName("Room not found")
    void roomNotFound() {
        given()
                .when()
                .headers(intervuUserHeadersForSystemUser())
                .get(url(EXPORT_ROOM_STATE), make())
                .then()
                .statusCode(NOT_FOUND.value());
    }

    @Test
    @DisplayName("State logs")
    void stateLogs() {
        var room = setUpFullRoom();
        var moderator = getModerator(room);

        connectToRoom(room.getId(), moderator);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.participants.findAll { p -> p.id == '" + moderator.getId() + "' }[0].online", CoreMatchers.equalTo(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_FINISH_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(intervuUserHeadersForSystemUser())
                .get(url(EXPORT_ROOM_STATE), room.getPlatformId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(4))
                .body("[0].size()", equalTo(5))
                .body("[0].offset", equalTo(0))
                .body("[0].changeType", equalTo(ROOM_CREATED.name()))
                .body("[0].version", notNullValue())
                .body("[0].addedAt", notNullValue())
                .body("[1].changeType", equalTo(PARTICIPANT_ONLINE.name()))
                .body("[2].changeType", equalTo(ROOM_STARTED.name()))
                .body("[3].changeType", equalTo(ROOM_FINISHED.name()));
    }

}
