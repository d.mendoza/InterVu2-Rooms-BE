package com.focusvision.intervu.room.api.waitingroom.authentication.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.exception.ErrorCode;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.CANCELED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;

/**
 * IT tests for {@link WaitingRoomAuthenticationController}.
 *
 * @author Branko Ostojic
 */
@DisplayName("Waiting Room authentication")
class WaitingRoomAuthenticationControllerIT extends HttpBasedIT {

    @Test
    @DisplayName("Error authenticating")
    void errorAuthenticating() {
        given()
                .when()
                .get(url(WAITING_ROOM_AUTHENTICATION_URL), make())
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("message", equalTo("Error authenticating."));
    }

    @ParameterizedTest
    @EnumSource(ParticipantRole.class)
    @DisplayName("Success")
    void success(ParticipantRole role) {
        var participant = getParticipant(setUpFullRoom(), role);
        given()
                .when()
                .get(url(WAITING_ROOM_AUTHENTICATION_URL), participant.getInvitationToken())
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("token", notNullValue());
    }

    @Test
    @DisplayName("Error - room canceled")
    void canceled() {
        var dummyRoom = setUpFullRoom();
        var moderator = getModerator(dummyRoom);

        var researchSessionData = sendCreateRoomEvent(moderator.getPlatformId());
        researchSessionData.setState(CANCELED);
        researchSessionData.setVersion(2);
        sendUpdateRoomEvent(researchSessionData);

        var participant = researchSessionData.getParticipants().stream()
                .filter(p -> p.getRole().equals(ParticipantRole.RESPONDENT))
                .findFirst().orElseThrow();

        given()
                .when()
                .get(url(WAITING_ROOM_AUTHENTICATION_URL), participant.getInvitationToken())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value())
                .body("code", equalTo(ErrorCode.ROOM_CANCELED.name()))
                .body("message", equalTo("Room is canceled."));
    }

    @Test
    @DisplayName("Error - room finished")
    void finished() {
        var dummyRoom = setUpFullRoom();
        var moderator = getModerator(dummyRoom);

        var researchSessionData = sendCreateRoomEvent(moderator.getPlatformId());
        researchSessionData.setState(FINISHED);
        researchSessionData.setVersion(2);
        sendUpdateRoomEvent(researchSessionData);

        var participant = researchSessionData.getParticipants().stream()
                .filter(p -> p.getRole().equals(ParticipantRole.RESPONDENT))
                .findFirst().orElseThrow();

        given()
                .when()
                .get(url(WAITING_ROOM_AUTHENTICATION_URL), participant.getInvitationToken())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value())
                .body("code", equalTo(ErrorCode.ROOM_FINISHED.name()))
                .body("message", equalTo("Room is finished."));
    }
}