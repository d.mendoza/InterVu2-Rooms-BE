package com.focusvision.intervu.room.api.tracking.service;

import static java.util.UUID.randomUUID;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.tracking.repository.RoomStateAcknowledgementRepository;
import java.util.UUID;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RoomStateTrackingServiceTest {

  private RoomStateTrackingService stateTrackingService;

  @Mock private RoomOffsetProvider roomOffsetProvider;

  @Mock private RoomStateAcknowledgementRepository roomStateAcknowledgementRepository;

  @BeforeEach
  public void setup() {
    stateTrackingService =
        new RoomStateTrackingService(roomOffsetProvider, roomStateAcknowledgementRepository);
  }

  @Test
  @DisplayName("ACK room state - ok")
  void ackRoomState_ok() {
    UUID roomId = randomUUID();
    UUID participantId = randomUUID();
    Long version = RandomUtils.nextLong();
    Long offset = RandomUtils.nextLong();
    when(roomOffsetProvider.calculateOffset(roomId)).thenReturn(offset);

    stateTrackingService.ackRoomState(participantId, roomId, version);

    verify(roomStateAcknowledgementRepository)
        .save(
            argThat(
                entity ->
                    entity.getRoomId().equals(roomId)
                        && entity.getParticipantId().equals(participantId)
                        && entity.getOffset().equals(offset)
                        && entity.getVersion().equals(version)));
  }
}
