package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.concurrent.CompletableFuture;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.DRAWING_SYNC_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.DRAWING_SYNC_ENABLED;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

/**
 * Integration tests for WS drawing sync events.
 */
class DrawingSyncIT extends HttpBasedIT {

    @Test
    @DisplayName("Enable drawing sync")
    void enableDrawingSync() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var roomId = researchSession.getId();
        var moderator = getModerator(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var roomStateHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, communicationChannelService.getRoomChannel(roomId), roomStateHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(DRAWING_SYNC_ENABLE_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        var states = roomStateHandler.getStates();

        assertAll(
                () -> assertThat(states.size()).isEqualTo(2),
                () -> assertThat(states.get(1).getDestination())
                        .isEqualTo(communicationChannelService.getRoomChannel(roomId)),
                () -> assertThat(states.get(1).getEventType()).isEqualTo(DRAWING_SYNC_ENABLED),
                () -> assertThat(states.get(1).getData().isDrawingInSync()).isTrue()
        );
    }

    @Test
    @DisplayName("Disable stimuli focus")
    void disableStimuliFocus() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var roomId = researchSession.getId();
        var moderator = getModerator(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var roomStateHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, communicationChannelService.getRoomChannel(roomId), roomStateHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(DRAWING_SYNC_ENABLE_URL))
                .then()
                .statusCode(OK.value());

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(DRAWING_SYNC_DISABLE_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        var states = roomStateHandler.getStates();

        assertAll(
                () -> assertThat(states.size()).isEqualTo(3),
                () -> assertThat(states.get(1).getDestination())
                        .isEqualTo(communicationChannelService.getRoomChannel(roomId)),
                () -> assertThat(states.get(1).getEventType()).isEqualTo(DRAWING_SYNC_ENABLED),
                () -> assertThat(states.get(1).getData().isDrawingInSync()).isTrue(),
                () -> assertThat(states.get(2).getDestination())
                        .isEqualTo(communicationChannelService.getRoomChannel(roomId)),
                () -> assertThat(states.get(2).getEventType()).isEqualTo(DRAWING_SYNC_DISABLED),
                () -> assertThat(states.get(2).getData().isDrawingInSync()).isFalse()
        );
    }
}
