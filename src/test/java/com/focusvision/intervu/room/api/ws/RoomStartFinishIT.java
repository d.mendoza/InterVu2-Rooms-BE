package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantStateStompFrameHandler;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.exception.ErrorCode;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.model.event.ParticipantStateEvent;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.concurrent.CompletableFuture;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_ONLINE;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_FINISHED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_STARTED;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

/**
 * IT tests for room start/finish WS integration.
 *
 * @author Branko Ostojic
 */
class RoomStartFinishIT extends HttpBasedIT {

    @Test
    @DisplayName("Presence status updated on connect/disconnect")
    void presenceStatusUpdated() throws Exception {
        var resultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var participantResultKeeper = new CompletableFuture<ParticipantStateEvent>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);

        var state = given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.state", equalTo(SCHEDULED.name()))
                .extract().body().as(ParticipantRoomStateDto.class);

        var handler = new RoomStateStompFrameHandler(mapper, resultKeeper::complete);
        var participantHandler = new ParticipantStateStompFrameHandler(mapper, participantResultKeeper::complete);
        var session = connectRoomWs(moderator);
        subscribe(session, state.getParticipantState().getChannel().getRoom(), handler);
        subscribe(session, state.getParticipantState().getChannel().getParticipant(), participantHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        verifyThatRoomHasStarted(moderator);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_FINISH_URL))
                .then()
                .statusCode(OK.value());

        verifyThatRoomHasFinished(moderator);

        given()
                .when()
                .get(url(WAITING_ROOM_AUTHENTICATION_URL), moderator.getInvitationToken())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value())
                .body("code", equalTo(ErrorCode.ROOM_FINISHED.name()))
                .body("message", equalTo("Room is finished."));

        waitForWs();

        assertThat(handler.getStates()).hasSize(3);
        assertThat(handler.getStates().get(0).getEventType()).isEqualByComparingTo(PARTICIPANT_ONLINE);
        assertThat(handler.getStates().get(0).getData().getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(handler.getStates().get(1).getData().getState()).isEqualByComparingTo(IN_PROGRESS);
        assertThat(handler.getStates().get(1).getEventType()).isEqualByComparingTo(ROOM_STARTED);
        assertThat(handler.getStates().get(2).getData().getState()).isEqualByComparingTo(FINISHED);
        assertThat(handler.getStates().get(2).getEventType()).isEqualByComparingTo(ROOM_FINISHED);

        assertThat(participantHandler.getStates().get(participantHandler.getStates().size() - 1)
                .getData().getConference().getToken()).isNotNull();
    }

    @Test
    @DisplayName("Start room - forbidden for banned participant")
    void startRoom_forbiddenBanned() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Finish room - forbidden for banned participant")
    void finishRoom_forbiddenBanned() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_FINISH_URL))
                .then()
                .statusCode(UNAUTHORIZED.value());
    }
}
