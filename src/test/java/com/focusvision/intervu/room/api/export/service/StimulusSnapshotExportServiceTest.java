package com.focusvision.intervu.room.api.export.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;

class StimulusSnapshotExportServiceTest {

  @Test
  void test() {
    assertThat(countRepeatingCharacters("tata")).isEqualTo(2);
    assertThat(countRepeatingCharacters("tataratira")).isEqualTo(4);
    assertThat(countRepeatingCharacters("aaaaaa")).isEqualTo(1);
  }

  public Integer countRepeatingCharacters(String text) {
    int counter = 0;
    if (StringUtils.hasText(text)) {
      Set<Character> uniqueChars = new HashSet<>();
      for (int i = 0; i < text.length(); i++) {
        if (uniqueChars.add(text.charAt(i))) {
          counter++;
        }
      }
    }

    return counter;
  }
}