package com.focusvision.intervu.room.api;

import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionStateData;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@SuppressWarnings({"unused"})
@RequiredArgsConstructor
public class ResearchSessionStateDataConsumer {

    private SimpleMessageListenerContainer container;

    private CountDownLatch latch;

    private List<ResearchSessionStateData> messages = new ArrayList<>();

    public ResearchSessionStateDataConsumer(final ConnectionFactory connectionFactory,
                                            MessageConverter messageConverter,
                                            String queueName,
                                            int messageCount) {
        var adapter = new MessageListenerAdapter(this, "receiveMessage");
        adapter.setMessageConverter(messageConverter);

        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setupMessageListener(adapter);

        latch = new CountDownLatch(messageCount);
    }

    public void receiveMessage(ResearchSessionStateData message) {
        messages.add(message);
        latch.countDown();
    }

    public void stop() {
        container.stop();
    }

    public ResearchSessionStateDataConsumer start() {
        container.start();

        return this;
    }

    public boolean allMessagesReceived(long timeout, TimeUnit seconds) throws InterruptedException {
        return latch.await(timeout, seconds);
    }

    public boolean containsMessage(String id, ResearchSessionState state) {
        return messages.stream()
                .anyMatch(sessionRoomData -> id.equals(sessionRoomData.getId())
                        && state.equals(sessionRoomData.getState()));
    }

    public Optional<ResearchSessionStateData> getMessage(String id, ResearchSessionState state) {
        return messages.stream()
                .filter(sessionRoomData -> id.equals(sessionRoomData.getId()))
                .filter(sessionRoomData -> state.equals(sessionRoomData.getState()))
                .findFirst();
    }

    public Optional<ResearchSessionStateData> getMessage(Integer number) {
        return messages.size() > number ? of(messages.get(number)) : empty();
    }
}
