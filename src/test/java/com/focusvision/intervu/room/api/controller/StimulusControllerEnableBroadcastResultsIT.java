package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static io.restassured.RestAssured.given;
import static net.bytebuddy.utility.RandomString.make;
import static org.springframework.http.HttpStatus.FORBIDDEN;

/**
 * Integration tests for {@link StimulusController#enableBroadcast(String, AuthenticatedParticipant)} focus endpoints.
 */
@DisplayName("Enable broadcast results")
class StimulusControllerEnableBroadcastResultsIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .put(url(ENABLE_BROADCAST_RESULTS), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned moderators")
    void unauthorizedBannedModerator() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ENABLE_BROADCAST_RESULTS), make())
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for non moderators")
    void forbiddenForParticipant() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);
        var stimulus = researchSession.getStimuli().get(0);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(ENABLE_BROADCAST_RESULTS), stimulus.getId())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .put(url(ENABLE_BROADCAST_RESULTS), stimulus.getId())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(ENABLE_BROADCAST_RESULTS), stimulus.getId())
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

}