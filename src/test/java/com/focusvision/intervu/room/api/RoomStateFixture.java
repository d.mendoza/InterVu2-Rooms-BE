package com.focusvision.intervu.room.api;

import static java.util.List.of;
import static net.bytebuddy.utility.RandomString.make;

import com.focusvision.intervu.room.api.common.model.ProjectType;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.state.model.RoomState;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

public class RoomStateFixture {

  public static RoomState.RoomStateBuilder generateRoomState(UUID roomId) {
    return RoomState.builder()
        .version(1L)
        .previousVersion(null)
        .roomId(roomId)
        .roomName(make())
        .projectName(make())
        .startsAt(Instant.now().minus(1L, ChronoUnit.MINUTES))
        .endsAt(Instant.now().plus(59L, ChronoUnit.MINUTES))
        .startedAt(Instant.now())
        .endedAt(null)
        .useWebcams(true)
        .projectNumber(make())
        .privacy(false)
        .roomChannel(make())
        .stimulusInFocus(false)
        .drawingInSync(false)
        .respondentsChatEnabled(false)
        .state(ResearchSessionState.IN_PROGRESS)
        .projectType(ProjectType.SELF_SERVICE)
        .participants(of())
        .stimuli(of());
  }
}
