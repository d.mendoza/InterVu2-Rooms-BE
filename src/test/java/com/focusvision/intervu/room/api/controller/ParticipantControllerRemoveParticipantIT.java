package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantStateStompFrameHandler;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.event.ParticipantStateEvent;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static java.util.UUID.randomUUID;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

/**
 * IT tests for {@link ParticipantController#banParticipant}.
 */
class ParticipantControllerRemoveParticipantIT extends HttpBasedIT {

    @Test
    @DisplayName("Remove participant from session - participant and moderator not on the same session")
    void removeParticipantFromSession_participantAndModeratorNotOnSameSession() {
        var observer = getObserver(setUpFullRoom());
        given()
                .when()
                .headers(roomParticipantHeaders(getModerator(setUpFullRoom())))
                .delete(url(PARTICIPANT_URL), observer.getId())
                .then()
                .statusCode(BAD_REQUEST.value())
                .body("message", equalTo("Moderator and participant are not in the same room."));
    }

    @Test
    @DisplayName("Remove participant from session - participant not found")
    void removeParticipantFromSession_participantNotFound() {
        given()
                .when()
                .headers(roomParticipantHeaders(getModerator(setUpFullRoom())))
                .delete(url(PARTICIPANT_URL), randomUUID())
                .then()
                .statusCode(BAD_REQUEST.value())
                .body("message", equalTo("Moderator and participant are not in the same room."));
    }

    @Test
    @DisplayName("Remove participant from session - caller not moderator")
    void removeParticipantFromSession_callerNotModerator() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .delete(url(PARTICIPANT_URL), respondent.getId().toString())
                .then()
                .statusCode(FORBIDDEN.value())
                .body("message", equalTo("Access is denied"));
    }

    @Test
    @DisplayName("Remove participant from session - unauthorized, banned caller")
    void removeParticipantFromSession_bannedCaller() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        participantRepository.save(respondent.setBanned(true));

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .delete(url(PARTICIPANT_URL), respondent.getId().toString())
                .then()
                .statusCode(UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Remove participant from session - ban admin by non admin")
    void removeParticipantFromSession_banAdminByNonAdmin() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var moderator2 = researchSession.getParticipants().stream()
                .filter(participant -> participant.getPlatformId().equals("m0"))
                .findFirst().orElseThrow();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .delete(url(PARTICIPANT_URL), moderator2.getId().toString())
                .then()
                .statusCode(BAD_REQUEST.value())
                .body("message", equalTo("Only administrators can ban other administrators."));
    }

    @Test
    @DisplayName("Remove participant from session - ok, ban admin")
    void removeParticipantFromSession_okBanAdmin() {
        var researchSession = setUpFullRoom();
        var moderator = participantRepository.save(getModerator(researchSession).setAdmin(true));
        var moderator2 = researchSession.getParticipants().stream()
                .filter(participant -> participant.getPlatformId().equals("m0"))
                .findFirst().orElseThrow();

        var conference = new Conference()
                .setRoomSid(make());
        researchSession.setConference(conference);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .delete(url(PARTICIPANT_URL), moderator2.getId().toString())
                .then()
                .statusCode(NO_CONTENT.value());
    }

    @Test
    @DisplayName("Remove participant from session - ok")
    void removeParticipantFromSession_ok() throws InterruptedException {
        var roomResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var participantResultKeeper = new CompletableFuture<ParticipantStateEvent>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);
        var bannedParticipantDataConsumer = bannedParticipantDataConsumer(1);

        var conference = new Conference()
                .setRoomSid(make());
        researchSession.setConference(conference);

        var state = given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .extract().body().as(ParticipantRoomStateDto.class);

        var handler = new RoomStateStompFrameHandler(mapper, roomResultKeeper::complete);
        var participantHandler = new ParticipantStateStompFrameHandler(mapper, participantResultKeeper::complete);
        var session = connectRoomWs(respondent);

        subscribe(session, state.getParticipantState().getChannel().getRoom(), handler);
        subscribe(session, state.getParticipantState().getChannel().getParticipant(), participantHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .delete(url(PARTICIPANT_URL), respondent.getId().toString())
                .then()
                .statusCode(NO_CONTENT.value());
        waitForWs();

        assertThat(handler.getStates().get(0).getData().getParticipants()
                .stream()
                .filter(p -> p.getId().equals(respondent.getId()))
                .findFirst().orElseThrow().isBanned()).isFalse();
        assertThat(handler.getStates().get(1).getData().getParticipants()
                .stream()
                .filter(p -> p.getId().equals(respondent.getId()))
                .findFirst().orElseThrow().isBanned()).isTrue();

        assertThat(participantHandler.getStates()).hasSize(1);

        assertThat(participantHandler.getStates().get(0).getData().isBanned()).isTrue();

        bannedParticipantDataConsumer.allMessagesReceived(2, TimeUnit.SECONDS);
        var message = bannedParticipantDataConsumer.getMessage(respondent.getPlatformId()).orElseThrow();

        assertAll(
                () -> assertThat(message.getPlatformId()).isEqualTo(respondent.getPlatformId()),
                () -> assertThat(message.getRole()).isEqualTo(respondent.getRole().name()),
                () -> assertThat(message.getSessionPlatformId()).isEqualTo(researchSession.getPlatformId())
        );
    }
}
