package com.focusvision.intervu.room.api.tracking.api;

import static org.assertj.core.api.Assertions.assertThat;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.tracking.model.ParticipantActionTrackingMessage;
import com.focusvision.intervu.room.api.tracking.model.ParticipantActionType;
import com.focusvision.intervu.room.api.tracking.model.entity.ParticipantActionLog;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class ParticipantTrackingMessagingControllerIT extends HttpBasedIT {

  @ParameterizedTest
  @EnumSource(ParticipantActionType.class)
  @DisplayName("Track participant action - ok")
  void track(ParticipantActionType type) {
    var researchSession = setUpFullRoom();
    var allParticipants = getAllParticipants(researchSession);

    allParticipants.forEach(participant -> sendTrackingMessage(participant, researchSession, type));

    waitForWs();

    allParticipants.forEach(participant -> verify(participant, researchSession, type));
  }

  private void sendTrackingMessage(
      Participant participant, ResearchSession researchSession, ParticipantActionType type) {
    var session = connectRoomWs(participant);
    connectToRoom(researchSession.getId(), participant);

    session.send(
        parseDestination(TRACK_PARTICIPANT_DESTINATION),
        new ParticipantActionTrackingMessage(type));
  }

  private void verify(
      Participant participant, ResearchSession researchSession, ParticipantActionType type) {
    List<ParticipantActionLog> actionLogs =
        participantActionLogRepository.findAll().stream()
            .filter(e -> e.getParticipantId().equals(participant.getId()))
            .toList();

    assertThat(actionLogs).hasSize(1);
    assertThat(actionLogs.get(0).getRoomId()).isEqualTo(researchSession.getId());
    assertThat(actionLogs.get(0).getOffset()).isZero();
    assertThat(actionLogs.get(0).getType()).isEqualTo(type);
  }
}
