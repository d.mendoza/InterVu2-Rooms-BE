package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for stimulus interaction in {@link StimuliController}.
 */
@DisplayName("Stimulus interaction")
class StimuliControllerInteractionIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .put(url(STIMULUS_INTERACTION_ENABLE))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
        given()
                .when()
                .put(url(STIMULUS_INTERACTION_DISABLE))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participant")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));
        given()
                .when()
                .put(url(STIMULUS_INTERACTION_ENABLE))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
        given()
                .when()
                .put(url(STIMULUS_INTERACTION_DISABLE))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @ParameterizedTest
    @ValueSource(strings = {STIMULUS_INTERACTION_ENABLE, STIMULUS_INTERACTION_DISABLE})
    @DisplayName("Forbidden for non moderators")
    void forbiddenForNonModerators(String url) {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(url))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .put(url(url))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(url))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Stimulus interaction")
    void stimulusInteraction() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);

        var moderatorSession = connectRoomWs(moderator);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.stimulus.interactionEnabled", equalTo(false))
                .extract().as(ParticipantRoomStateDto.class);
        enableStimulusInteraction(moderator);
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.stimulus.interactionEnabled", equalTo(true))
                .extract().as(ParticipantRoomStateDto.class);
        disableStimulusInteraction(moderator);
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.stimulus.interactionEnabled", equalTo(false))
                .extract().as(ParticipantRoomStateDto.class);
    }

    @Test
    @DisplayName("Stimulus reactivation")
    void stimulusReactivation() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);

        var moderatorSession = connectRoomWs(moderator);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.stimulus.interactionEnabled", equalTo(false))
                .extract().as(ParticipantRoomStateDto.class);
        enableStimulusInteraction(moderator);
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.stimulus.interactionEnabled", equalTo(true))
                .extract().as(ParticipantRoomStateDto.class);

        deactivateStimulus(moderator, pollingStimulus.getId());
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(ROOM_INFO_URL))
                .then()
                .statusCode(OK.value())
                .body("roomState.stimulus.interactionEnabled", equalTo(true))
                .extract().as(ParticipantRoomStateDto.class);
    }

}
