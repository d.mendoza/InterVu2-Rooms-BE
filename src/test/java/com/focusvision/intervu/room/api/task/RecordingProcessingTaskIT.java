package com.focusvision.intervu.room.api.task;

import static java.lang.Thread.sleep;
import static java.util.List.of;
import static java.util.UUID.randomUUID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import com.focusvision.intervu.room.api.adapter.RecordingCheckSenderAdapter;
import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.recording.service.RecordingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

/**
 * IT tests for {@link RecordingProcessingTask#run}
 */
@DirtiesContext
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {
    "app.tasks.check-processing-recordings.enabled=true",
    "app.tasks.check-processing-recordings.initial-delay=500",
    "app.tasks.check-processing-recordings.period=1000",
})
@DisplayName("Check processing recordings task")
class RecordingProcessingTaskIT {

  @MockBean
  private RecordingService recordingService;
  @MockBean
  private RecordingCheckSenderAdapter adapter;

  @BeforeEach
  void setup() {
    when(recordingService.getAllUnprocessedRecordings())
        .thenReturn(of(new Recording().setId(randomUUID()), new Recording().setId(randomUUID())));
  }

  @Test
  @DisplayName("Task executed")
  void taskExecuted() throws InterruptedException {
    sleep(1000);
    verify(recordingService, times(1)).getAllUnprocessedRecordings();
    verify(adapter, times(2)).send(any(ProcessingRecordingCheck.class));
  }
}
