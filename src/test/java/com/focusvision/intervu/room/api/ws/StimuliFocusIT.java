package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.concurrent.CompletableFuture;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_FOCUS_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_FOCUS_ENABLED;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.HttpStatus.OK;

/**
 * Integration tests for WS stimuli focus events.
 */
class StimuliFocusIT extends HttpBasedIT {

    @Test
    @DisplayName("Enable stimuli focus")
    void enableStimuliFocus() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, communicationChannelService.getRoomChannel(researchSession.getId()), moderatorHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(STIMULI_FOCUS_ENABLE_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        var states = moderatorHandler.getStates();

        assertAll(
                () -> assertThat(states.size()).isEqualTo(2),
                () -> assertThat(states.get(1).getDestination()).isEqualTo(communicationChannelService.getRoomChannel(researchSession.getId())),
                () -> assertThat(states.get(1).getEventType()).isEqualTo(STIMULUS_FOCUS_ENABLED),
                () -> assertThat(states.get(1).getData().isStimulusInFocus()).isTrue()
        );
    }

    @Test
    @DisplayName("Disable stimuli focus")
    void disableStimuliFocus() {
        var moderatorResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);

        var moderatorSession = connectRoomWs(moderator);
        var moderatorHandler = new RoomStateStompFrameHandler(mapper, moderatorResultKeeper::complete);
        subscribe(moderatorSession, communicationChannelService.getRoomChannel(researchSession.getId()), moderatorHandler);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(STIMULI_FOCUS_ENABLE_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .put(url(STIMULI_FOCUS_DISABLE_URL))
                .then()
                .statusCode(OK.value());

        waitForWs();

        var states = moderatorHandler.getStates();

        assertAll(
                () -> assertThat(states.size()).isEqualTo(3),
                () -> assertThat(states.get(2).getDestination()).isEqualTo(communicationChannelService.getRoomChannel(researchSession.getId())),
                () -> assertThat(states.get(2).getEventType()).isEqualTo(STIMULUS_FOCUS_DISABLED),
                () -> assertThat(states.get(2).getData().isStimulusInFocus()).isFalse()
        );
    }
}
