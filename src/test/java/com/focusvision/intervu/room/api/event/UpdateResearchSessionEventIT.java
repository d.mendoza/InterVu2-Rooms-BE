package com.focusvision.intervu.room.api.event;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantRemovedStompFrameHandler;
import com.focusvision.intervu.room.api.ResearchSessionProcessResultConsumer;
import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.model.event.ParticipantRemovedEvent;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.repository.ConferenceRepository;
import com.focusvision.intervu.room.api.repository.ResearchSessionRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.CompletableFuture;

import static com.focusvision.intervu.room.api.common.model.StimulusType.DRAG_AND_DROP;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.CANCELED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.EXPIRED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static com.focusvision.intervu.room.api.model.event.EventType.PARTICIPANT_REMOVED;
import static com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult.Status.FAILED;
import static com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult.Status.SUCCESS;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

/**
 * IT tests for {@link ResearchSessionData} event.
 * Various update research session tests.
 */
@DisplayName("Update research session event")
public class UpdateResearchSessionEventIT extends HttpBasedIT {

    @Autowired
    private MessagingProperties messagingProperties;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ResearchSessionRepository researchSessionRepository;
    @Autowired
    private ConferenceRepository conferenceRepository;
    private ResearchSessionProcessResultConsumer consumer;

    @Test
    @DisplayName("Failed, session version up to date")
    void failedSessionVersion() {
        var researchSessionData = generate().setVersion(3);

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate
                .convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var researchSessionDataOutdated = generate().setId(researchSessionData.getId()).setVersion(2);
        rabbitTemplate
                .convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionDataOutdated);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 3)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(researchSession.get().getVersion()).isEqualTo(3);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(FAILED);
        assertThat(updatedMessage.getMessage()).contains("Research session is not editable");
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Update with fresh data")
    void updatedFreshData() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        var updatedResearchSessionData = generate()
                .setId(researchSessionData.getId())
                .setVersion(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), updatedResearchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();
        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(researchSession.get().getVersion()).isEqualTo(2);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(updatedMessage.getMessage()).isNull();
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Update with renames")
    void updatedWithRenames() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        var updatedResearchSessionData = generateForUpdate(researchSessionData);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), updatedResearchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(researchSession.get().getVersion()).isEqualTo(2);
        assertThat(researchSession.get().getName()).isEqualTo(updatedResearchSessionData.getName());
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(updatedMessage.getMessage()).isNull();
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Failed, session finished")
    void failedSessionFinished() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        researchSessionRepository.findByPlatformId(researchSessionData.getId())
                .map(session -> session.setState(ResearchSessionState.FINISHED))
                .map(researchSessionRepository::save)
                .orElseThrow();
        researchSessionData.setVersion(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(FINISHED);
        assertThat(researchSession.get().getVersion()).isEqualTo(1);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(FAILED);
        assertThat(updatedMessage.getMessage()).contains("Research session is not editable");
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Failed, session cancelled")
    void failedSessionCancelled() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        researchSessionRepository.findByPlatformId(researchSessionData.getId())
                .map(session -> session.setState(ResearchSessionState.CANCELED))
                .map(researchSessionRepository::save)
                .orElseThrow();
        researchSessionData.setVersion(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(CANCELED);
        assertThat(researchSession.get().getVersion()).isEqualTo(1);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(FAILED);
        assertThat(updatedMessage.getMessage()).contains("Research session is not editable");
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Failed, session expired")
    void failedSessionExpired() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        researchSessionRepository.findByPlatformId(researchSessionData.getId())
                .map(session -> session.setState(ResearchSessionState.EXPIRED))
                .map(researchSessionRepository::save)
                .orElseThrow();
        researchSessionData.setVersion(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(EXPIRED);
        assertThat(researchSession.get().getVersion()).isEqualTo(1);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(FAILED);
        assertThat(updatedMessage.getMessage()).contains("Research session is not editable");
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Update started session - only project data is updated")
    void startedSession() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        researchSessionRepository.findByPlatformId(researchSessionData.getId())
                .map(session -> session.setState(ResearchSessionState.IN_PROGRESS))
                .map(researchSessionRepository::save)
                .ifPresent(researchSession -> {
                    conferenceRepository.save(new Conference().setResearchSession(researchSession).setRoomSid(make()));
                    researchSessionRepository.save(researchSession);
                });
        var updatedResearchSessionData = generateForUpdate(researchSessionData);
        updatedResearchSessionData.getStimuli().add(new ResearchSessionData.Stimulus()
                .setId(make())
                .setName(make())
                .setPosition(1)
                .setThumbnail(make())
                .setType(DRAG_AND_DROP)
                .setData(make()));
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), updatedResearchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(IN_PROGRESS);
        assertThat(researchSession.get().getVersion()).isEqualTo(2);
        assertThat(researchSession.get().getName()).isEqualTo(researchSessionData.getName());
        assertThat(researchSession.get().getProject().getName())
                .isEqualTo(updatedResearchSessionData.getProject().getName());
        assertThat(researchSession.get().getProject().getProjectNumber())
                .isEqualTo(updatedResearchSessionData.getProject().getProjectNumber());
        assertThat(researchSession.get().getStimuli()).hasSize(1);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(updatedMessage.getMessage()).isNull();
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Started session - participants are not removed")
    void startedSessionParticipantsNotRemoved() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        researchSessionRepository.findByPlatformId(researchSessionData.getId())
                .map(session -> session.setState(ResearchSessionState.IN_PROGRESS))
                .map(researchSessionRepository::save)
                .ifPresent(researchSession -> {
                    conferenceRepository.save(new Conference().setResearchSession(researchSession).setRoomSid(make()));
                    researchSessionRepository.save(researchSession);
                });
        var updatedResearchSessionData = generateForUpdate(researchSessionData);
        updatedResearchSessionData.setParticipants(null);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), updatedResearchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1).orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2).orElseThrow();
        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId());
        assertThat(researchSession).isPresent();
        assertThat(researchSession.get().getState()).isEqualByComparingTo(IN_PROGRESS);
        assertThat(researchSession.get().getVersion()).isEqualTo(2);
        assertThat(researchSession.get().getParticipants()).hasSize(2);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(createdMessage.getMessage()).isNull();
        assertThat(createdMessage.getProcessedAt()).isCloseTo(now(), within(2, SECONDS));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(updatedMessage.getMessage()).isNull();
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Participants removed - WS notified")
    void participantsRemoved_wsNotified() {
        var resultKeeper = new CompletableFuture<ParticipantRemovedEvent>();
        var researchSession = setUpFullRoom();
        var researchSessionData = mapToResearchSessionData(researchSession);

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        conferenceRepository.save(new Conference().setResearchSession(researchSession).setRoomSid(make()));
        researchSessionRepository.save(researchSession);

        var updatedResearchSessionData = generateForUpdate(researchSessionData);
        updatedResearchSessionData.setParticipants(null);

        var moderator = getModerator(researchSession);
        var observer = getObserver(researchSession);
        var respondent = getRespondent(researchSession);
        var participantHandler = new ParticipantRemovedStompFrameHandler(mapper, resultKeeper::complete);
        var moderatorSession = connectWs(moderator);
        var observerSession = connectWs(observer);
        var respondentSession = connectWs(respondent);
        subscribe(moderatorSession, communicationChannelService
                .getParticipantGlobalChannel(moderator.getPlatformId()), participantHandler);
        subscribe(observerSession, communicationChannelService
                .getParticipantGlobalChannel(observer.getPlatformId()), participantHandler);
        subscribe(respondentSession, communicationChannelService
                .getParticipantGlobalChannel(respondent.getPlatformId()), participantHandler);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), updatedResearchSessionData);

        waitForWs();

        consumer.stop();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2).orElseThrow();

        var updatedResearchSession = researchSessionRepository.getById(researchSession.getId());

        assertThat(updatedResearchSession.getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(updatedResearchSession.getVersion()).isEqualTo(2);
        assertThat(updatedResearchSession.getParticipants()).hasSize(0);
        assertThat(participantHandler.getStates()).hasSize(3);
        assertThat(participantHandler.getStates()).extracting(ParticipantRemovedEvent::getRoomId)
                .containsExactly(researchSession.getId(), researchSession.getId(), researchSession.getId());
        assertThat(participantHandler.getStates()).extracting(ParticipantRemovedEvent::getEventType)
                .containsExactly(PARTICIPANT_REMOVED, PARTICIPANT_REMOVED, PARTICIPANT_REMOVED);
        assertThat(participantHandler.getStates()).extracting(ParticipantRemovedEvent::getDestination)
                .containsExactlyInAnyOrder(
                        communicationChannelService.getParticipantGlobalChannel(moderator.getPlatformId()),
                        communicationChannelService.getParticipantGlobalChannel(observer.getPlatformId()),
                        communicationChannelService.getParticipantGlobalChannel(respondent.getPlatformId()));
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(updatedMessage.getMessage()).isNull();
        assertThat(updatedMessage.getProcessedAt()).isCloseTo(now(), within(1, SECONDS));
    }

    @Test
    @DisplayName("Remove all stimuli and participants")
    void removeAllParticipantsAndStimuli() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        researchSessionData
                .setVersion(2)
                .setParticipants(null)
                .setStimuli(null);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId()).orElseThrow();

        assertThat(researchSession.getState()).isEqualByComparingTo(SCHEDULED);
        assertThat(researchSession.getVersion()).isEqualTo(2);
        assertThat(researchSession.getParticipants()).hasSize(0);
        assertThat(researchSession.getStimuli()).hasSize(0);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(researchSession.getParticipants()).isEmpty();
        assertThat(researchSession.getStimuli()).isEmpty();
    }

    @Test
    @DisplayName("Set state to cancelled")
    void setStateToCancelled() {
        var researchSessionData = generate();

        consumer = researchSessionProcessResultConsumer(2);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();
        researchSessionData
                .setVersion(2)
                .setState(CANCELED);
        rabbitTemplate.convertAndSend(messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
        waitForWs();

        var createdMessage = consumer.getMessage(researchSessionData.getId(), 1)
                .orElseThrow();
        var updatedMessage = consumer.getMessage(researchSessionData.getId(), 2)
                .orElseThrow();

        consumer.stop();

        var researchSession = researchSessionRepository.findByPlatformId(researchSessionData.getId()).orElseThrow();

        assertThat(researchSession.getState()).isEqualByComparingTo(CANCELED);
        assertThat(researchSession.getVersion()).isEqualTo(2);
        assertThat(createdMessage.getStatus()).isEqualByComparingTo(SUCCESS);
        assertThat(updatedMessage.getStatus()).isEqualByComparingTo(SUCCESS);
    }
}
