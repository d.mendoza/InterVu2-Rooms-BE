package com.focusvision.intervu.room.api.ws;

import com.focusvision.intervu.room.api.GenericParticipantRoomStateEvent;
import com.focusvision.intervu.room.api.GenericRoomStateEvent;
import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.ParticipantRoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.RoomStateStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.IntervuUserDto;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CREATED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_UPDATED;
import static io.restassured.RestAssured.given;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.utility.RandomString.make;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for create/edit room event WS integration.
 *
 * @author Branko Ostojic
 */
class RoomCreateUpdateIT extends HttpBasedIT {

    @Test
    @DisplayName("Room created and then updated - InterVu user gets the WS events")
    void eventSentToDashboard() throws ExecutionException, InterruptedException, TimeoutException {
        var dummyRoom = setUpFullRoom();
        var moderator = getModerator(dummyRoom);
        var roomResultKeeper = new CompletableFuture<GenericRoomStateEvent>();
        var newRoomResultKeeper = new CompletableFuture<GenericParticipantRoomStateEvent>();

        var intervuUser = given()
                .when()
                .headers(intervuUserHeaders(moderator.getPlatformId()))
                .get(url(DASHBOARD_USER_INFO_URL))
                .then()
                .statusCode(OK.value())
                .extract().body().as(IntervuUserDto.class);

        var participantGlobalHandler = new ParticipantRoomStateStompFrameHandler(mapper, newRoomResultKeeper::complete);
        var session = connectWs(intervuAuthToken(moderator.getPlatformId()));
        subscribe(session, intervuUser.getChannel(), participantGlobalHandler);

        var researchSessionData = sendCreateRoomEvent(moderator.getPlatformId());
        var newRoomEvent = newRoomResultKeeper.get(1, SECONDS);

        var roomHandler = new RoomStateStompFrameHandler(mapper, roomResultKeeper::complete);
        subscribe(session, newRoomEvent.getData().getParticipantState().getChannel().getRoom(), roomHandler);

        researchSessionData.setName(make());
        researchSessionData.setVersion(2);
        sendUpdateRoomEvent(researchSessionData);

        roomResultKeeper.get(1, SECONDS);
        assertThat(roomHandler.getStates()).hasSize(1);
        assertThat(participantGlobalHandler.getStates()).hasSize(1);
        assertThat(participantGlobalHandler.getStates().get(0).getEventType()).isEqualByComparingTo(ROOM_CREATED);
        assertThat(roomHandler.getStates().get(0).getEventType()).isEqualByComparingTo(ROOM_UPDATED);
        assertThat(roomHandler.getStates().get(0).getData().getRoomName()).isEqualTo(researchSessionData.getName());
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class)
    @DisplayName("Room created, then started and then updated - add new participant after session start")
    void addParticipantAfterSessionStart(ParticipantRole participantRole) {
        var researchSession = setUpFullRoom();
        var admin = getAdmin(researchSession);

        startRoom(admin);

        var startedResearchSession = researchSessionService.get(researchSession.getId());

        var researchSessionData = mapToResearchSessionData(startedResearchSession);

        researchSessionData.setVersion(researchSession.getVersion() + 1);
        var newParticipant = new ResearchSessionData.Participant()
                .setId(make())
                .setName(make())
                .setInvitationToken(make())
                .setRole(participantRole);
        researchSessionData.getParticipants().add(newParticipant);

        researchSessionService.createOrUpdate(researchSessionData);

        var updatedResearchSession = researchSessionService.get(researchSession.getId());
        var updatedParticipant = updatedResearchSession.getParticipants().stream()
                .filter(participant -> participant.getPlatformId().equals(newParticipant.getId()))
                .findFirst()
                .orElseThrow();

        assertThat(updatedParticipant.getConferenceToken()).isNotEmpty();
    }

    @ParameterizedTest
    @EnumSource(value = ParticipantRole.class)
    @DisplayName("Room created, then started and then updated - add new participant before session start")
    void addParticipantBeforeSessionStart(ParticipantRole participantRole) {
        var researchSession = setUpFullRoom();
        var admin = getAdmin(researchSession);

        var researchSessionData = mapToResearchSessionData(researchSession);

        researchSessionData.setVersion(researchSession.getVersion() + 1);
        var newParticipant = new ResearchSessionData.Participant()
                .setId(make())
                .setName(make())
                .setInvitationToken(make())
                .setRole(participantRole);
        researchSessionData.getParticipants().add(newParticipant);

        researchSessionService.createOrUpdate(researchSessionData);

        startRoom(admin);

        var startedResearchSession = researchSessionService.get(researchSession.getId());
        var updatedParticipant = startedResearchSession.getParticipants().stream()
                .filter(participant -> participant.getPlatformId().equals(newParticipant.getId())).findFirst().orElseThrow();

        assertThat(updatedParticipant.getConferenceToken()).isNotEmpty();
    }

}
