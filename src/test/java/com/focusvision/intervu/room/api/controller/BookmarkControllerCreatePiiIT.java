package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.common.model.BookmarkType.PII;
import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static net.bytebuddy.utility.RandomString.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.WsTestUtils;
import com.focusvision.intervu.room.api.bookmark.api.BookmarkController;
import com.focusvision.intervu.room.api.bookmark.model.BookmarkDetailsDto;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.dto.AddPiiBookmarkDto;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

/**
 * IT tests for
 * {@link BookmarkController#createPiiBookmark(AddPiiBookmarkDto, AuthenticatedParticipant)}.
 */
@DisplayName("Bookmarks creation - PII")
class BookmarkControllerCreatePiiIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .body(addPiiBookmarkDto())
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for not started room.")
    void roomNotStarted() {
        given()
                .when()
                .headers(roomParticipantHeadersWithContent(getModerator(setUpFullRoom())))
                .body(addPiiBookmarkDto())
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .statusCode(BAD_REQUEST.value())
                .body("message", containsString("Room not running."));
    }

    @Test
    @DisplayName("Bad request, invalid data")
    void invalidData() {
        var moderator = getModerator(setUpFullRoom());

        // note too long
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addPiiBookmarkDto(make(BOOKMARK_NOTE_MAX_LENGTH + 1)))
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .statusCode(BAD_REQUEST.value());

        // note null
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addPiiBookmarkDto(null))
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .statusCode(BAD_REQUEST.value());

        // note empty
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addPiiBookmarkDto(""))
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .statusCode(BAD_REQUEST.value());

        // timestamp null
        given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addPiiBookmarkDto(randomBookmarkNote(), null))
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .statusCode(BAD_REQUEST.value());
    }

  @ParameterizedTest
  @EnumSource(value = ParticipantRole.class, mode = EXCLUDE,
      names = {"MODERATOR", "OBSERVER", "TRANSLATOR"})
  @DisplayName("Forbidden for all roles except moderator, observer and translator participants")
  void forbiddenForAllExceptModeratorObserverAndTranslator(ParticipantRole role) {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    given()
        .headers(roomParticipantHeadersWithContent(getParticipant(researchSession, role)))
        .body(addPiiBookmarkDto())
        .when()
        .post(url(ROOM_BOOKMARKS_PII_URL))
        .then()
        .statusCode(FORBIDDEN.value());
  }

    @Test
    @DisplayName("Ok, as moderator")
    void ok_asModerator() throws ExecutionException, InterruptedException, TimeoutException {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var observer = getObserver(researchSession);
        var respondent = getRespondent(researchSession);
        var moderatorChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var observerChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
        var respondentChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();

        connectAllToRoom(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var moderatorSession = connectRoomWs(moderator);
        moderatorSession.subscribe(
                getChatChannel(moderator.getId()),
                new WsTestUtils.OutgoingChatMessageStompFrameHandler(moderatorChatResultKeeper::complete));

        var observerSession = connectRoomWs(observer);
        observerSession.subscribe(
                getChatChannel(observer.getId()),
                new WsTestUtils.OutgoingChatMessageStompFrameHandler(observerChatResultKeeper::complete));

        var respondentSession = connectRoomWs(respondent);
        respondentSession.subscribe(
                getChatChannel(respondent.getId()),
                new WsTestUtils.OutgoingChatMessageStompFrameHandler(respondentChatResultKeeper::complete));

        var beforeBookmarkCreation = now();
        var addPiiBookmarkDto = addPiiBookmarkDto();

         given()
                .headers(roomParticipantHeadersWithContent(moderator))
                .body(addPiiBookmarkDto)
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .and().statusCode(CREATED.value())
                .body("size()", is(7))
                .body("id", notNullValue())
                .body("note", nullValue())
                .body("addedBy", equalTo(moderator.getPlatformId()))
                .body("addedAt", greaterThan(beforeBookmarkCreation.toString()))
                .body("addedByDisplayName", equalTo(moderator.getDisplayName()))
                .body("offset", equalTo(
                        roomOffsetProvider.calculateOffset(researchSession.getId(), addPiiBookmarkDto.getTimestamp())))
                .body("type", equalTo(PII.toString()))
                .extract()
                .as(BookmarkDetailsDto.class);

        var moderatorChatMessage = moderatorChatResultKeeper.get(500, MILLISECONDS);
        var observerChatMessage = observerChatResultKeeper.get(500, MILLISECONDS);

        assertAll(
                () -> assertThrows(TimeoutException.class, () -> respondentChatResultKeeper.get(500, MILLISECONDS)),
                () -> assertThat(moderatorChatMessage.getMessageType()).isEqualTo(ChatMessage.MessageType.PII),
                () -> assertThat(moderatorChatMessage.getMessage()).isNull(),
                () -> assertThat(observerChatMessage.getMessageType()).isEqualTo(ChatMessage.MessageType.PII),
                () -> assertThat(observerChatMessage.getMessage()).isNull()
        );
    }

  @Test
  @DisplayName("Ok, as translator")
  void ok_asTranslator() throws ExecutionException, InterruptedException, TimeoutException {
    var researchSession = setUpFullRoom();
    var moderator = getModerator(researchSession);
    var respondent = getRespondent(researchSession);
    var translator = getTranslator(researchSession);
    var moderatorChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
    var translatorChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();
    var respondentChatResultKeeper = new CompletableFuture<OutgoingChatMessage>();

    connectAllToRoom(researchSession);

    given()
        .when()
        .headers(roomParticipantHeaders(moderator))
        .put(url(ROOM_START_URL))
        .then()
        .statusCode(OK.value());

    var moderatorSession = connectRoomWs(moderator);
    moderatorSession.subscribe(
        getChatChannel(moderator.getId()),
        new WsTestUtils.OutgoingChatMessageStompFrameHandler(moderatorChatResultKeeper::complete));

    var translatorSession = connectRoomWs(moderator);
    translatorSession.subscribe(
        getChatChannel(moderator.getId()),
        new WsTestUtils.OutgoingChatMessageStompFrameHandler(translatorChatResultKeeper::complete));

    var respondentSession = connectRoomWs(respondent);
    respondentSession.subscribe(
        getChatChannel(respondent.getId()),
        new WsTestUtils.OutgoingChatMessageStompFrameHandler(respondentChatResultKeeper::complete));

    var beforeBookmarkCreation = now();
    var addPiiBookmarkDto = addPiiBookmarkDto();

    given()
        .headers(roomParticipantHeadersWithContent(translator))
        .body(addPiiBookmarkDto)
        .when()
        .post(url(ROOM_BOOKMARKS_PII_URL))
        .then()
        .and().statusCode(CREATED.value())
        .body("size()", is(7))
        .body("id", notNullValue())
        .body("note", nullValue())
        .body("addedBy", equalTo(translator.getPlatformId()))
        .body("addedAt", greaterThan(beforeBookmarkCreation.toString()))
        .body("addedByDisplayName", equalTo(translator.getDisplayName()))
        .body("offset", equalTo(
            roomOffsetProvider.calculateOffset(researchSession.getId(),
                addPiiBookmarkDto.getTimestamp())))
        .body("type", equalTo(PII.toString()))
        .extract()
        .as(BookmarkDetailsDto.class);

    var moderatorChatMessage = moderatorChatResultKeeper.get(500, MILLISECONDS);
    var translatorChatMessage = translatorChatResultKeeper.get(500, MILLISECONDS);

    assertAll(
        () -> assertThrows(TimeoutException.class,
            () -> respondentChatResultKeeper.get(500, MILLISECONDS)),
        () -> assertThat(moderatorChatMessage.getMessageType()).isEqualTo(
            ChatMessage.MessageType.PII),
        () -> assertThat(moderatorChatMessage.getMessage()).isNull(),
        () -> assertThat(translatorChatMessage.getMessageType()).isEqualTo(
            ChatMessage.MessageType.PII),
        () -> assertThat(translatorChatMessage.getMessage()).isNull()
    );
  }

    @Test
    @DisplayName("Ok, as admin")
    void ok_asAdmin() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var admin = getAdmin(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var beforeBookmarkCreation = now();
        var addPiiBookmarkDto = addPiiBookmarkDto();

        given()
                .headers(roomParticipantHeadersWithContent(admin))
                .body(addPiiBookmarkDto)
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .and().statusCode(CREATED.value())
                .body("size()", is(7))
                .body("id", notNullValue())
                .body("note", nullValue())
                .body("addedBy", equalTo(admin.getPlatformId()))
                .body("addedAt", greaterThan(beforeBookmarkCreation.toString()))
                .body("addedByDisplayName", equalTo(admin.getDisplayName()))
                .body("offset", equalTo(
                        roomOffsetProvider.calculateOffset(researchSession.getId(), addPiiBookmarkDto.getTimestamp())))
                .body("type", equalTo(PII.toString()));
    }

    @Test
    @DisplayName("Ok, as project manager")
    void ok_asProjectManager() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var projectManager = getProjectManager(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var beforeBookmarkCreation = now();
        var addPiiBookmarkDto = addPiiBookmarkDto();

        given()
                .headers(roomParticipantHeadersWithContent(projectManager))
                .body(addPiiBookmarkDto)
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .and().statusCode(CREATED.value())
                .body("size()", is(7))
                .body("id", notNullValue())
                .body("note", nullValue())
                .body("addedBy", equalTo(projectManager.getPlatformId()))
                .body("addedAt", greaterThan(beforeBookmarkCreation.toString()))
                .body("addedByDisplayName", equalTo(projectManager.getDisplayName()))
                .body("offset", equalTo(
                        roomOffsetProvider.calculateOffset(researchSession.getId(), addPiiBookmarkDto.getTimestamp())))
                .body("type", equalTo(PII.toString()));
    }

    @Test
    @DisplayName("Ok, as project operator")
    void ok_asProjectOperator() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        var projectOperator = getProjectOperator(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(ROOM_START_URL))
                .then()
                .statusCode(OK.value());

        var beforeBookmarkCreation = now();
        var addPiiBookmarkDto = addPiiBookmarkDto();

        given()
                .headers(roomParticipantHeadersWithContent(projectOperator))
                .body(addPiiBookmarkDto)
                .when()
                .post(url(ROOM_BOOKMARKS_PII_URL))
                .then()
                .and().statusCode(CREATED.value())
                .body("size()", is(7))
                .body("id", notNullValue())
                .body("note", nullValue())
                .body("addedBy", equalTo(projectOperator.getPlatformId()))
                .body("addedAt", greaterThan(beforeBookmarkCreation.toString()))
                .body("addedByDisplayName", equalTo(projectOperator.getDisplayName()))
                .body("offset", equalTo(
                        roomOffsetProvider.calculateOffset(researchSession.getId(), addPiiBookmarkDto.getTimestamp())))
                .body("type", equalTo(PII.toString()));
    }

}
