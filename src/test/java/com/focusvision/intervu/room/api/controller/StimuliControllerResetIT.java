package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.HttpBasedIT;
import com.focusvision.intervu.room.api.StimulusActionStompFrameHandler;
import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static java.time.Instant.now;
import static net.bytebuddy.utility.RandomString.make;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.http.HttpStatus.OK;

/**
 * IT tests for resetting stimulus in {@link StimuliController}.
 */
@DisplayName("Resetting stimulus")
class StimuliControllerResetIT extends HttpBasedIT {

    @Test
    @DisplayName("Forbidden for unauthorized")
    void forbiddenUnauthorized() {
        given()
                .when()
                .put(url(STIMULUS_RESET))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Unauthorized, banned participant")
    void unauthorizedBannedParticipant() {
        var researchSession = setUpFullRoom();
        var moderator = getModerator(researchSession);
        participantRepository.save(moderator.setBanned(true));
        given()
                .when()
                .put(url(STIMULUS_RESET))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @DisplayName("Forbidden for non moderators")
    void forbiddenForNonModerators() {
        var researchSession = setUpFullRoom();
        var respondent = getRespondent(researchSession);
        var translator = getTranslator(researchSession);
        var observer = getObserver(researchSession);

        given()
                .when()
                .headers(roomParticipantHeaders(respondent))
                .put(url(STIMULUS_RESET))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(translator))
                .put(url(STIMULUS_RESET))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        given()
                .when()
                .headers(roomParticipantHeaders(observer))
                .put(url(STIMULUS_RESET))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    @DisplayName("Reset stimulus")
    void resetStimulus() {
        var researchSession = setUpFullRoom();
        var pollingStimulus = getPollStimulus(researchSession);

        var moderator = getModerator(researchSession);
        var respondent = getRespondent(researchSession);
        var respondentSession = connectRoomWs(respondent);

        var moderatorSession = connectRoomWs(moderator);

        var moderatorPollStimulusActionHandler = new StimulusActionStompFrameHandler(mapper);

        startRoom(moderator);
        waitForWs();
        activateStimulus(moderator, pollingStimulus.getId());
        waitForWs();

        var stimulusChannel = communicationChannelService.getStimulusChannel(researchSession.getId());
        subscribe(moderatorSession, stimulusChannel, moderatorPollStimulusActionHandler);
        waitForWs();
        enableStimulusInteraction(moderator);
        waitForWs();
        var voteAction = new PollStimulusActionMessage()
                .setStimulusId(pollingStimulus.getId())
                .setContent(make())
                .setTimestamp(now().toEpochMilli());
        respondentSession.send(POLL_STIMULUS_VOTE_DESTINATION, voteAction);
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(1))
                .body("[0].size()", Matchers.equalTo(3))
                .body("[0].participantId", equalTo(respondent.getId().toString()))
                .body("[0].content", equalTo(voteAction.getContent()))
                .body("[0].stimulusId", equalTo(pollingStimulus.getId().toString()))
                .extract().as(StimulusStateDto[].class);
        disableStimulusInteraction(moderator);
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .put(url(STIMULUS_RESET))
                .then()
                .statusCode(OK.value());
        waitForWs();
        given()
                .when()
                .headers(roomParticipantHeaders(moderator))
                .get(url(POLL_STIMULUS_STATE), pollingStimulus.getId())
                .then()
                .statusCode(OK.value())
                .body("size()", equalTo(0));
    }

}
