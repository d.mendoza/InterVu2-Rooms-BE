package com.focusvision.intervu.room.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@Getter
@RequiredArgsConstructor
public class RoomStateStompFrameHandler implements StompFrameHandler {

    private final ObjectMapper mapper;
    private final Consumer<GenericRoomStateEvent> frameHandler;
    private final List<GenericRoomStateEvent> states = new ArrayList<>();

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return GenericRoomStateEvent.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info("received message: {} with headers: {}", payload, headers);
        GenericRoomStateEvent dto = mapper.convertValue(payload, GenericRoomStateEvent.class);
        frameHandler.accept(dto);
        states.add(dto);
    }
}
