package com.focusvision.intervu.room.api.event.participant.publisher;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantBannedEvent;

/**
 * Participant ban event publisher.
 */
public interface ParticipantBannedEventPublisher {

  /**
   * Publishes participant ban event.
   *
   * @param event Event data.
   */
  void publish(ParticipantBannedEvent event);
}
