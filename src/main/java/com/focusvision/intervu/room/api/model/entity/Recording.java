package com.focusvision.intervu.room.api.model.entity;

import static com.focusvision.intervu.room.api.common.model.AudioChannel.NATIVE;
import static com.focusvision.intervu.room.api.model.entity.Recording.State.PENDING;
import static com.focusvision.intervu.room.api.model.entity.Recording.State.PROCESSING;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.ScreenshareBounds;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.LastModifiedDate;

/**
 * Entity representing room recording.
 */
@Entity
@Table(name = "recordings")
@Getter
@Setter
@Accessors(chain = true)
public class Recording extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private String recordingSid;

  @Column
  @Enumerated(EnumType.STRING)
  private CompositionLayout compositionLayout;
  @Type(type = "jsonb")
  @Column(columnDefinition = "jsonb")
  private ScreenshareBounds screenshareBounds;
  @Column
  @Enumerated(EnumType.STRING)
  private State state = PROCESSING;
  @Column
  private Integer roomDuration = 0;
  @Column
  private Integer recordingDuration = 0;
  @Column
  private Instant processingStartedAt;
  @Column
  private Instant processingFinishedAt;
  @Column
  @Enumerated(EnumType.STRING)
  private AudioChannel audioChannel = NATIVE;

  @ManyToOne
  private Conference conference;

  public boolean isPending() {
    return PENDING.equals(state);
  }

  public boolean isProcessing() {
    return PROCESSING.equals(state);
  }

  /**
   * Recording states.
   */
  public enum State {
    /**
     * Indicates that recording has pending processing.
     */
    PENDING,
    /**
     * Indicates that recording has been processing.
     */
    PROCESSING,
    /**
     * Indicates that recording has been processed and completed.
     */
    COMPLETED,
    /**
     * Indicates that recording has not been processed due to an error.
     */
    ERROR
  }
}
