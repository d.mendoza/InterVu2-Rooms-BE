package com.focusvision.intervu.room.api.service.impl;

import static java.util.Optional.ofNullable;

import com.focusvision.intervu.room.api.export.model.BookmarkExportDto;
import com.focusvision.intervu.room.api.export.service.BookmarkExportService;
import com.focusvision.intervu.room.api.model.ScreenArea;
import com.focusvision.intervu.room.api.model.ScreenshareBounds;
import com.focusvision.intervu.room.api.model.dto.RoomRecordingDto;
import com.focusvision.intervu.room.api.model.dto.RoomRecordingDto.RecordingDto;
import com.focusvision.intervu.room.api.model.dto.RoomRecordingDto.ScreenAreaDto;
import com.focusvision.intervu.room.api.model.dto.RoomRecordingDto.ScreenshareBoundsDto;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.recording.service.RecordingService;
import com.focusvision.intervu.room.api.service.ConferenceService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.RoomRecordingService;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomRecordingService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomRecordingServiceImpl implements RoomRecordingService {

  private final ResearchSessionService researchSessionService;
  private final ConferenceService conferenceService;
  private final BookmarkExportService bookmarkExportService;
  private final RecordingService recordingService;

  @Override
  public Optional<RoomRecordingDto> getRecordingData(String platformId) {
    log.debug("Collecting recording data for room platform ID {}.", platformId);

    return researchSessionService.fetch(platformId)
        .filter(ResearchSession::isCompleted)
        .filter(room -> room.getConference() != null)
        .map(researchSession -> {
          var conference = researchSession.getConference();
          var recordings = getRecordingsData(conference);

          return new RoomRecordingDto()
              .setRecordings(recordings);
        });
  }

  private List<BookmarkExportDto> getBookmarksData(ResearchSession researchSession) {
    return bookmarkExportService.export(researchSession.getPlatformId())
        .orElse(Collections.emptyList());
  }

  private List<RecordingDto> getRecordingsData(Conference conference) {
    return conference.getRecordings().stream()
        .map(recording -> {
          var recordingLink = recordingService.getRecordingLink(recording.getId())
              .orElse(null);
          var screensharePosition = ofNullable(recording.getScreenshareBounds())
              .map(bounds -> new ScreenshareBoundsDto()
                  .setScreenshareArea(mapToAreaDto(bounds.getScreenshareArea()))
                  .setExcludedAreas(getExcludedAreas(bounds)))
              .orElse(null);
          return new RecordingDto()
              .setState(recording.getState())
              .setLayout(recording.getCompositionLayout())
              .setRecordingDuration(recording.getRecordingDuration())
              .setRecordingUrl(recordingLink)
              .setScreensharePosition(screensharePosition)
              .setAudioChannel(recording.getAudioChannel());
        }).toList();
  }

  private List<ScreenAreaDto> getExcludedAreas(ScreenshareBounds bounds) {
    return ofNullable(bounds.getExcludedAreas())
        .map(areas -> areas
            .stream()
            .map(this::mapToAreaDto)
            .toList())
        .orElse(null);
  }

  @Override
  public void deleteRecordingData(String roomPlatformId) {
    log.debug("Deleting recording data for room platform ID {}.", roomPlatformId);

    researchSessionService.fetch(roomPlatformId)
        .filter(ResearchSession::isCompleted)
        .map(ResearchSession::getConference)
        .map(Conference::getId)
        .ifPresent(conferenceService::deleteRecordings);
  }

  private ScreenAreaDto mapToAreaDto(ScreenArea area) {
    return new ScreenAreaDto()
        .setOffsetX(area.getOffsetX())
        .setOffsetY(area.getOffsetY())
        .setHeight(area.getHeight())
        .setWidth(area.getWidth());
  }

}
