package com.focusvision.intervu.room.api.adapter.impl;

import static java.lang.String.format;
import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.adapter.ConferenceCheckDataInputAdapter;
import com.focusvision.intervu.room.api.advisor.ClusterLock;
import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;
import com.focusvision.intervu.room.api.service.ConferenceService;
import com.focusvision.intervu.room.api.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ConferenceCheckDataInputAdapter}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ConferenceCheckDataInputAdapterImpl implements ConferenceCheckDataInputAdapter {

  private final ConferenceService conferenceService;
  private final RoomService roomService;


  @Override
  @ClusterLock
  public void process(RunningConferenceCheck runningConferenceCheck) {
    log.debug("Running conference check incoming data: [{}]", runningConferenceCheck);

    try {
      var conference = conferenceService.get(fromString(runningConferenceCheck.getId()));
      roomService.finishIfBroken(conference.getResearchSession().getId());
    } catch (Exception e) {
      log.error(format("Error running conference check %s", runningConferenceCheck), e);
    }
  }
}
