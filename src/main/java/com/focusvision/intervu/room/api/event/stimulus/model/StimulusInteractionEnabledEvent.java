package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus interaction enabled event.
 */
public record StimulusInteractionEnabledEvent(UUID roomId) {

}
