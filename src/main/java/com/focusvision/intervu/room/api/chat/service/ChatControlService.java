package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;

/**
 * Service used for chat control related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatControlService {

  /**
   * Enables respondents meeting room chat.
   *
   * @param participant Participant enabling the chat.
   */
  void enableMeetingRoomRespondentsChat(AuthenticatedParticipant participant);

  /**
   * Disables respondents meeting room chat.
   *
   * @param participant Participant disabling the chat.
   */
  void disableMeetingRoomRespondentsChat(AuthenticatedParticipant participant);

}
