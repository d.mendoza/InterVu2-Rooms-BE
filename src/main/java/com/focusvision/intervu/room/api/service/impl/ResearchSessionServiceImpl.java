package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.CANCELED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.EXPIRED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;
import static com.focusvision.intervu.room.api.model.CompositionLayout.GRID;
import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantRemovedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomCanceledEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomCreatedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomExpiredEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomUpdatedEvent;
import com.focusvision.intervu.room.api.event.room.publisher.ParticipantsRemovedEventPublisher;
import com.focusvision.intervu.room.api.event.room.publisher.RoomCanceledEventPublisher;
import com.focusvision.intervu.room.api.event.room.publisher.RoomCreatedEventPublisher;
import com.focusvision.intervu.room.api.event.room.publisher.RoomExpiredEventPublisher;
import com.focusvision.intervu.room.api.event.room.publisher.RoomUpdatedEventPublisher;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.Project;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.repository.ResearchSessionRepository;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.ProjectService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.StreamingService;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of a {@link ResearchSessionService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ResearchSessionServiceImpl implements ResearchSessionService {

  private final ResearchSessionRepository researchSessionRepository;
  private final ProjectService projectService;
  private final StreamingService streamingService;
  private final ParticipantService participantService;
  private final RoomCreatedEventPublisher roomCreatedEventPublisher;
  private final RoomUpdatedEventPublisher roomUpdatedEventPublisher;
  private final RoomCanceledEventPublisher roomCanceledEventPublisher;
  private final RoomExpiredEventPublisher roomExpiredEventPublisher;
  private final ParticipantsRemovedEventPublisher participantsRemovedEventPublisher;

  private static final Set<CompositionLayout> ALLOWED_COMPOSITION_LAYOUTS = Set.of(GRID,
      PICTURE_IN_PICTURE_WITH_COLUMN);

  @Override
  public Optional<ResearchSession> fetch(String platformId) {
    log.debug("Fetching details for research session with platform ID {}.", platformId);

    return researchSessionRepository.findByPlatformId(platformId);
  }

  @Override
  public ResearchSession get(UUID id) {
    log.debug("Fetching research session {} details.", id);

    return researchSessionRepository.getById(id);
  }

  @Override
  public Optional<ResearchSession> findByListenerPin(String listenerPin) {
    log.debug("Finding research session (listenerPin: {})", listenerPin);

    return researchSessionRepository.findByListenerPin(listenerPin);
  }

  @Override
  public Optional<ResearchSession> findBySpeakerPin(String speakerPin) {
    log.debug("Finding research session (speakerPin: {})", speakerPin);

    return researchSessionRepository.findBySpeakerPin(speakerPin);
  }

  @Override
  public Optional<ResearchSession> findByOperatorPin(String operatorPin) {
    log.debug("Finding research session (operatorPin: {})", operatorPin);

    return researchSessionRepository.findByOperatorPin(operatorPin);
  }

  @Override
  public ResearchSession markAsStarted(UUID id) {
    log.debug("Marking research session {} as started.", id);

    var researchSession = this.get(id)
        .setState(IN_PROGRESS)
        .setStartedAt(now());
    return researchSessionRepository.save(researchSession);
  }

  @Override
  public ResearchSession markAsFinished(UUID id) {
    log.debug("Marking research session {} as finished.", id);

    var researchSession = this.get(id)
        .setState(FINISHED)
        .setEndedAt(now());
    return researchSessionRepository.save(researchSession);
  }

  @Override
  public List<ResearchSession> getTodayUpcomingSessions(String platformId) {
    log.debug("Fetching intervu user {} today's sessions.", platformId);

    var from = now();
    var to = now().plus(2, DAYS).truncatedTo(DAYS);

    return researchSessionRepository.findParticipantSessions(platformId, from, to);
  }

  @Override
  public List<ResearchSession> getResearchSessionsForParticipant(String participantPlatformId) {
    log.debug("Getting research sessions for participant {}", participantPlatformId);

    return researchSessionRepository.findByParticipantsPlatformIdAndState(participantPlatformId,
        FINISHED);
  }

  @Override
  @Transactional
  public void createOrUpdate(ResearchSessionData researchSessionData) {
    log.debug("Creating/updating research session {}", researchSessionData);

    researchSessionRepository.findByPlatformId(researchSessionData.getId())
        .ifPresentOrElse(
            researchSession -> this.updateResearchSession(researchSessionData, researchSession),
            () -> this.createResearchSession(researchSessionData));
  }

  @Override
  public void validateCompositionLayout(CompositionLayout layout) {
    log.debug("Validating composition layout {}.", layout);

    if (!ALLOWED_COMPOSITION_LAYOUTS.contains(layout)) {
      log.warn("Composition layout {} is not allowed.", layout);
      throw new IntervuRoomException("Composition layout is not allowed");
    }
  }

  private void createResearchSession(ResearchSessionData researchSessionData) {
    log.debug("Creating research session: {}", researchSessionData);

    var project = this.createOrUpdateProject(researchSessionData.getProject());

    validateCompositionLayout(researchSessionData.getCompositionLayout());

    ResearchSession researchSession = new ResearchSession()
        .setPlatformId(researchSessionData.getId())
        .setName(researchSessionData.getName())
        .setProject(project)
        .setState(researchSessionData.getState())
        .setStartsAt(researchSessionData.getStartsAt())
        .setEndsAt(researchSessionData.getEndsAt())
        .setVersion(researchSessionData.getVersion())
        .setPrivacy(researchSessionData.isPrivacy())
        .setUseWebcams(researchSessionData.isUseWebcams())
        .setListenerPin(researchSessionData.getListenerPin())
        .setSpeakerPin(researchSessionData.getSpeakerPin())
        .setOperatorPin(researchSessionData.getOperatorPin())
        .setAudioChannel(researchSessionData.getAudioChannel());

    if (researchSessionData.getParticipants() != null) {
      researchSessionData.getParticipants().stream()
          .map(this::construct)
          .forEach(researchSession::addParticipant);
    }

    if (researchSessionData.getStimuli() != null) {
      researchSessionData.getStimuli().stream()
          .map(this::construct)
          .forEach(researchSession::addStimulus);
    }
    researchSessionRepository.save(researchSession);

    roomCreatedEventPublisher.publish(new RoomCreatedEvent(
        researchSession.getId(),
        project,
        researchSession,
        researchSession.getParticipants()));
  }

  private void cancelResearchSession(ResearchSessionData researchSessionData,
                                     ResearchSession researchSession) {
    log.info("Canceling research session {}.", researchSession.getId());

    researchSession
        .setVersion(researchSessionData.getVersion())
        .setState(researchSessionData.getState());
    researchSessionRepository.save(researchSession);

    roomCanceledEventPublisher.publish(new RoomCanceledEvent(researchSession.getId()));
  }

  private void expireResearchSession(ResearchSessionData researchSessionData,
                                     ResearchSession researchSession) {
    log.info("Expiring research session {}.", researchSession.getId());

    researchSession
        .setVersion(researchSessionData.getVersion())
        .setState(researchSessionData.getState());
    researchSessionRepository.save(researchSession);

    roomExpiredEventPublisher.publish(new RoomExpiredEvent(researchSession.getId()));
  }

  private void updateResearchSession(ResearchSessionData researchSessionData,
                                     ResearchSession researchSession) {
    log.debug("Updating research session: {}", researchSessionData);

    if (!isResearchSessionEditable(researchSession, researchSessionData)) {
      log.warn("Research session {} is not editable.", researchSession.getId());
      throw new IntervuRoomException("Research session is not editable");
    } else if (researchSession.isPending() && CANCELED.equals(researchSessionData.getState())) {
      this.cancelResearchSession(researchSessionData, researchSession);
      return;
    } else if (researchSession.isPending() && EXPIRED.equals(researchSessionData.getState())) {
      this.expireResearchSession(researchSessionData, researchSession);
      return;
    }

    validateCompositionLayout(researchSessionData.getCompositionLayout());

    var project = this.createOrUpdateProject(researchSessionData.getProject());

    researchSession
        .setVersion(researchSessionData.getVersion())
        .setName(researchSessionData.getName())
        .setProject(project);

    if (researchSession.isPending()) {
      researchSession
          .setState(researchSessionData.getState())
          .setStartsAt(researchSessionData.getStartsAt())
          .setEndsAt(researchSessionData.getEndsAt())
          .setPrivacy(researchSessionData.isPrivacy())
          .setUseWebcams(researchSessionData.isUseWebcams())
          .setListenerPin(researchSessionData.getListenerPin())
          .setSpeakerPin(researchSessionData.getSpeakerPin())
          .setOperatorPin(researchSessionData.getOperatorPin())
          .setAudioChannel(researchSessionData.getAudioChannel());

      this.removeNonExistingStimulus(researchSession, researchSessionData.getStimuli());
      if (researchSessionData.getStimuli() != null) {
        researchSessionData.getStimuli()
            .forEach(stimulus -> this.constructOrUpdate(researchSession, stimulus));
      }

      this.removeNonExistingParticipants(researchSession, researchSessionData.getParticipants());
      if (researchSessionData.getParticipants() != null) {
        researchSessionData.getParticipants()
            .forEach(participant -> this.constructOrUpdate(researchSession, participant));
      }
    }

    if (researchSession.isStarted()) {
      if (researchSessionData.getParticipants() != null) {
        this.getNewParticipants(researchSession, researchSessionData.getParticipants())
            .forEach(participant -> this.constructOrUpdate(researchSession, participant));
      }
    }

    researchSessionRepository.save(researchSession);

    if (researchSession.isStarted()) {
      populateMissingConferenceTokens(researchSession);
    }

    var updatedResearchSession = researchSessionRepository.getById(researchSession.getId());

    roomUpdatedEventPublisher.publish(new RoomUpdatedEvent(
        updatedResearchSession.getId(),
        project,
        updatedResearchSession,
        updatedResearchSession.getConference(),
        updatedResearchSession.getParticipants(),
        updatedResearchSession.getStimuli()));
  }

  private void populateMissingConferenceTokens(ResearchSession researchSession) {
    log.info("Populate missing conference tokens for the session {}.", researchSession.getId());

    researchSession.getParticipants().stream()
        .filter(p -> p.getConferenceToken() == null)
        .forEach(p -> {
          var groupRoomParticipant =
              streamingService.createGroupRoomParticipant(p.getId(),
                  researchSession.getConference().getRoomSid());
          participantService.updateConferenceToken(p.getId(), groupRoomParticipant.getToken());
        });
  }

  private List<ResearchSessionData.Participant> getNewParticipants(
      ResearchSession researchSession,
      List<ResearchSessionData.Participant> participants) {
    return participants.stream().filter(participant ->
            researchSession.getParticipants().stream()
                .noneMatch(rsParticipant ->
                    participant.getId().equals(rsParticipant.getPlatformId())))
        .toList();
  }

  private Project createOrUpdateProject(ResearchSessionData.Project projectData) {
    log.info("Creating/updating project {}", projectData.getId());

    var project = projectService.findByPlatformId(projectData.getId())
        .orElseGet(() -> new Project()
            .setPlatformId(projectData.getId())
            .setProjectNumber(projectData.getProjectNumber())
            .setType(projectData.getType()))
        .setName(projectData.getName());

    return projectService.save(project);
  }

  private void removeNonExistingParticipants(ResearchSession researchSession,
                                             List<ResearchSessionData.Participant> participants) {
    var removed = researchSession.getParticipants().stream()
        .filter(participant -> participants == null || participants.stream()
            .noneMatch(p -> p.getId().equals(participant.getPlatformId())))
        .collect(toSet());

    researchSession.getParticipants().removeAll(removed);

    removed.forEach(
        participant -> participantsRemovedEventPublisher
            .publish(new ParticipantRemovedEvent(
                researchSession.getId(), participant.getId(), participant.getPlatformId())));
  }

  private void removeNonExistingStimulus(ResearchSession researchSession,
                                         List<ResearchSessionData.Stimulus> stimuli) {
    var removed = researchSession.getStimuli().stream()
        .filter(stimulus -> stimuli == null || stimuli.stream()
            .noneMatch(s -> s.getId().equals(stimulus.getPlatformId())))
        .collect(toSet());

    researchSession.getStimuli().removeAll(removed);
  }

  private void constructOrUpdate(ResearchSession researchSession,
                                 ResearchSessionData.Participant participant) {
    researchSession.getParticipants().stream()
        .filter(existing -> existing.getPlatformId().equals(participant.getId()))
        .findFirst()
        .map(existing -> existing
            .setDisplayName(participant.getName())
            .setRole(participant.getRole())
            .setAdmin(participant.isAdmin())
            .setBanable(participant.isBanable())
            .setModerator(participant.isModerator())
            .setGuest(participant.isGuest())
            .setProjectManager(participant.isProjectManager())
            .setProjectOperator(participant.isProjectOperator())
            .setSpeakerPin(participant.getSpeakerPin()))
        .ifPresentOrElse(existing -> {
        }, () -> researchSession.addParticipant(this.construct(participant)));
  }

  private void constructOrUpdate(ResearchSession researchSession,
                                 ResearchSessionData.Stimulus stimulus) {
    researchSession.getStimuli().stream()
        .filter(existing -> existing.getPlatformId().equals(stimulus.getId()))
        .findFirst()
        .map(existing -> this.update(existing, stimulus))
        .ifPresentOrElse(existing -> {
        }, () -> researchSession.addStimulus(this.construct(stimulus)));
  }

  private Participant construct(ResearchSessionData.Participant participant) {
    return new Participant()
        .setPlatformId(participant.getId())
        .setDisplayName(participant.getName())
        .setRole(participant.getRole())
        .setInvitationToken(participant.getInvitationToken())
        .setAdmin(participant.isAdmin())
        .setBanable(participant.isBanable())
        .setModerator(participant.isModerator())
        .setGuest(participant.isGuest())
        .setProjectManager(participant.isProjectManager())
        .setProjectOperator(participant.isProjectOperator())
        .setSpeakerPin(participant.getSpeakerPin())
        .setBanned(false);
  }

  private Stimulus construct(ResearchSessionData.Stimulus stimulusData) {
    return new Stimulus()
        .setPlatformId(stimulusData.getId())
        .setName(stimulusData.getName())
        .setThumbnail(stimulusData.getThumbnail())
        .setType(stimulusData.getType())
        .setData(stimulusData.getData())
        .setPosition(stimulusData.getPosition());
  }

  private Stimulus update(Stimulus stimulus, ResearchSessionData.Stimulus stimulusData) {
    return stimulus
        .setName(stimulusData.getName())
        .setThumbnail(stimulusData.getThumbnail())
        .setType(stimulusData.getType())
        .setData(stimulusData.getData())
        .setPosition(stimulusData.getPosition());
  }

  private boolean isResearchSessionEditable(ResearchSession researchSession,
                                            ResearchSessionData researchSessionData) {
    return !researchSession.isCompleted()
        && researchSession.getVersion() < researchSessionData.getVersion();
  }

}
