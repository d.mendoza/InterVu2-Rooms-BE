package com.focusvision.intervu.room.api.chat.model;

import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the chat message details.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Chat Message")
public class ChatMessageDto {

  @ApiModelProperty("Chat message ID")
  private UUID id;

  @ApiModelProperty("Chat message content")
  private String message;

  @ApiModelProperty("Chat message sender ID")
  private UUID senderId;

  @ApiModelProperty("Chat message sender platform ID")
  private String senderPlatformId;

  @ApiModelProperty("Chat message sender name")
  private String senderName;

  @ApiModelProperty("Chat message handle")
  private UUID handle;

  @ApiModelProperty("Chat type")
  private ChatType type;

  @ApiModelProperty("Chat message type")
  private ChatMessage.MessageType messageType;

  @ApiModelProperty("Chat message sent time")
  private Instant sentAt;
}
