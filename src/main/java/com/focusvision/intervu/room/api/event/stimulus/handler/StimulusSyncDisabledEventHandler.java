package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusSyncDisabledEvent;

/**
 * Handler for {@link StimulusSyncDisabledEvent}.
 */
public interface StimulusSyncDisabledEventHandler {

  /**
   * Handles the provided {@link StimulusSyncDisabledEvent}.
   *
   * @param event Stimulus sync disabled event.
   */
  void handle(StimulusSyncDisabledEvent event);
}
