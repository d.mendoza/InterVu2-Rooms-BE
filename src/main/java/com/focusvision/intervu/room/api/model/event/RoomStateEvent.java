package com.focusvision.intervu.room.api.model.event;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * DTO representing the room state event.
 *
 * @author Branko Ostojic
 */
@Getter
@Builder
@ToString
@ApiModel("RoomStateEvent")
public class RoomStateEvent {

  @ApiModelProperty("Event Type")
  private final RoomStateChangeType eventType;

  /**
   * Event destination.
   */
  @ApiModelProperty("Event destination")
  private final String destination;

  /**
   * Event data payload.
   */
  @ApiModelProperty("Event data")
  private final RoomStateDto data;

}
