package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;

/**
 * Room finished event publisher.
 */
public interface RoomFinishedEventPublisher {

  void publish(RoomFinishedEvent event);
}
