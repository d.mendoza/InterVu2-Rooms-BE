package com.focusvision.intervu.room.api.draw.api;

import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.draw.model.DrawingLogDto;
import com.focusvision.intervu.room.api.draw.provider.DrawingStateProvider;
import com.focusvision.intervu.room.api.draw.service.DrawingControlService;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room drawing control related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/drawing", produces = MediaType.APPLICATION_JSON_VALUE)
public class DrawingController {

  private final DrawingControlService drawingControlService;
  private final DrawingStateProvider drawingStateProvider;

  /**
   * Enable drawing sync.
   *
   * @param caller User performing action.
   */
  @PutMapping("sync/enable")
  @ApiOperation(
      value = "Enables drawing sync",
      notes = "Enables drawing sync")
  @PreAuthorize("hasAuthority('DRAWING_CONTROL')")
  public void enable(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enabling drawing sync by {}", caller.getId());

    drawingControlService.enableSync(caller.getRoomId());
  }

  /**
   * Disable drawing sync.
   *
   * @param caller User performing action.
   */
  @PutMapping("sync/disable")
  @ApiOperation(
      value = "Disables drawing sync",
      notes = "Disables drawing sync")
  @PreAuthorize("hasAuthority('DRAWING_CONTROL')")
  public void disable(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Disabling stimuli focus by {}", caller.getId());

    drawingControlService.disableSync(caller.getRoomId());
  }

  /**
   * Get drawing state for specified context.
   *
   * @param contextId Context ID.
   * @param caller    User performing action.
   * @return Drawing state.
   */
  @GetMapping("state/{contextId}")
  @ApiOperation(
      value = "Retrieves the drawing state",
      notes = "Retrieves the drawing state for specific context")
  @PreAuthorize("hasAuthority('DRAWING_READ')")
  public List<DrawingLogDto> state(
      @PathVariable String contextId,
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Retrieving current drawing state for context {}.", contextId);

    return drawingStateProvider.getCurrentState(caller.getRoomId(), fromString(contextId));
  }

  /**
   * Enable drawing for specific participant.
   *
   * @param id     Participant ID.
   * @param caller User performing action.
   */
  @PutMapping("{id}/enable")
  @ApiOperation(
      value = "Enable drawing in room for participant",
      notes = "Enable drawing in room for participant")
  @PreAuthorize("hasAuthority('DRAWING_CONTROL')")
  public void enableDrawing(@ApiParam("Participant ID") @PathVariable String id,
                            @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enable drawing in room {} for participant {}.", caller.getRoomId(), id);

    drawingControlService.enableDrawingForParticipant(caller.getRoomId(), fromString(id));
  }

  /**
   * Enable drawing for specific participant.
   *
   * @param id     Participant ID.
   * @param caller User performing action.
   */
  @PutMapping("{id}/disable")
  @ApiOperation(
      value = "Disable drawing in room for participant",
      notes = "Disable drawing in room for participant")
  @PreAuthorize("hasAuthority('DRAWING_CONTROL')")
  public void disableDrawing(@ApiParam("Participant ID") @PathVariable String id,
                             @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Disable drawing in room {} for participant {}.", caller.getRoomId(), id);

    drawingControlService.disableDrawingForParticipant(caller.getRoomId(), fromString(id));
  }

  /**
   * Enable drawing for all participants.
   *
   * @param caller User performing action.
   */
  @PutMapping("enable")
  @ApiOperation(
      value = "Enable drawing in room for participants",
      notes = "Enable drawing in room for participants")
  @PreAuthorize("hasAuthority('DRAWING_CONTROL')")
  public void enableDrawingForAll(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enable drawing in room {} for participants {}.", caller.getRoomId(), caller.getId());

    drawingControlService.enableDrawingForParticipants(caller.getRoomId());
  }

  /**
   * Disable drawing for all participants.
   *
   * @param caller User performing action.
   */
  @PutMapping("disable")
  @ApiOperation(
      value = "Disable drawing in room for participants",
      notes = "Disable drawing in room for participants")
  @PreAuthorize("hasAuthority('DRAWING_CONTROL')")
  public void disableDrawingForAll(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enable drawing in room {} for participants {}.", caller.getRoomId(), caller.getId());

    drawingControlService.disableDrawingForParticipants(caller.getRoomId());
  }
}
