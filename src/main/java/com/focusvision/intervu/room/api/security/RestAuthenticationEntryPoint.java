package com.focusvision.intervu.room.api.security;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * Rest authentication entry point handler.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

  /**
   * This is invoked when user tries to access a secured REST resource without supplying any
   * credentials. We should just send a 401 Unauthorized response because there is no 'login page'
   * to redirect to.
   *
   * @param request       HTTP Request
   * @param response      HTTP Response
   * @param authException AuthenticationException
   * @throws IOException IOException
   */
  @Override
  public void commence(HttpServletRequest request,
                       HttpServletResponse response,
                       AuthenticationException authException) throws IOException {
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
  }
}

