package com.focusvision.intervu.room.api.twilio.service.impl;

import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;
import static com.focusvision.intervu.room.api.twilio.util.TwilioTrackNamingUtil.RESPONDENT_CAMERA_WILDCARD;
import static com.focusvision.intervu.room.api.twilio.util.TwilioTrackNamingUtil.isOldTrackNamingUsed;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.ScreenArea;
import com.focusvision.intervu.room.api.model.ScreenshareBounds;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.focusvision.intervu.room.api.model.streaming.RecordingLayoutData;
import com.focusvision.intervu.room.api.twilio.service.TwilioLayoutService;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link TwilioLayoutService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TwilioLayoutServiceImpl extends TwilioLayoutService {

  private static final String EXCLUDED_VIDEOS = "video_sources_excluded";
  private static final String VIDEO_SOURCES = "video_sources";
  private static final String MAX_ROWS = "max_rows";
  private static final String REUSE = "reuse";
  private static final String GRID = "grid";
  private static final String MAIN = "main";
  private static final String Z_POS = "z_pos";
  private static final String X_POS = "x_pos";
  private static final String Y_POS = "y_pos";
  private static final String WIDTH = "width";
  private static final String HEIGHT = "height";
  private static final String PIP = "pip";
  private static final String MAX_COLUMNS = "max_columns";
  private static final String SHOW_NEWEST = "show_newest";

  private static final int VIDEO_HEIGHT = 720;
  private static final int VIDEO_WIDTH = 1280;
  private static final double CAMERA_RATIO = 4.0 / 3;
  private static final int MAX_ROWS_SCREEN_SHARE = 1;
  private static final int MAX_COLUMNS_SCREEN_SHARE = 1;
  private static final String SCREEN_SHARE_WILDCARD = "%s_screenshare*";
  private static final String SCREEN_SHARE_WILDCARD_DEPRECATED = "*screenshare";

  // PICTURE_IN_PICTURE layout parameters
  private static final int PIP_X_POS = VIDEO_WIDTH * 3 / 4;
  private static final int PIP_WIDTH = VIDEO_WIDTH / 5;
  private static final int PIP_HEIGHT = PIP_WIDTH * 3 / 4;

  // PICTURE_IN_PICTURE_WITH_COLUMN layout parameters
  private static final int SINGLE_CAMERA_HEIGHT = VIDEO_HEIGHT / 5;
  private static final int MAX_FULL_SIZE_COLUMN_CAMERAS = VIDEO_HEIGHT / SINGLE_CAMERA_HEIGHT;
  private static final int REDUCED_COLUMN_CAMERA_HEIGHT = SINGLE_CAMERA_HEIGHT * 3 / 4;
  private static final int MAX_SINGLE_COLUMN_CAMERAS =
      VIDEO_HEIGHT / REDUCED_COLUMN_CAMERA_HEIGHT;
  private static final int REDUCED_CAMERA_HEIGHT_OFFSET =
      VIDEO_HEIGHT / MAX_SINGLE_COLUMN_CAMERAS;
  private static final int BASE_COLUMN_CAMERA_WIDTH =
      Double.valueOf(SINGLE_CAMERA_HEIGHT * CAMERA_RATIO).intValue();
  private static final int REDUCED_COLUMN_CAMERA_WIDTH = BASE_COLUMN_CAMERA_WIDTH * 3 / 4;
  private static final int MAX_VIDEOS_PER_SINGLE_CELL = 1;
  private static final String COLUMN_VIDEOS_WILDCARD = "RESPONDENT_camera_%s*";
  private static final String MODERATOR_VIDEOS_WILDCARD = "MODERATOR_camera_%s*";
  private static final String COLUMN_VIDEOS_WILDCARD_DEPRECATED = "RESPONDENT_%s_camera*";
  private static final String MODERATOR_VIDEOS_WILDCARD_DEPRECATED = "MODERATOR_%s_camera*";
  // The archive is limited to a maximum of three participant columns
  private static final int MAX_PARTICIPANTS_IN_ARCHIVE = 3 * MAX_SINGLE_COLUMN_CAMERAS;
  private static final int Z_ORDER_SCREEN_SHARE_REGION = 0;
  private static final int Z_ORDER_MODERATOR_REGION = 1;
  private static final int Z_ORDER_COLUMNS_REGION = 2;

  @Override
  public Optional<RecordingLayoutData> createLayoutData(
      List<ParticipantGroupRoomRecording> recordings,
      CompositionLayout compositionLayout,
      Set<String> excludedVideos) {
    log.debug("Creating layout data for {} composition layout", compositionLayout);

    if (!hasRecordingsForLayout(recordings, excludedVideos)) {
      return Optional.empty();
    }
    if (PICTURE_IN_PICTURE_WITH_COLUMN.equals(compositionLayout)) {
      return of(createPictureInPictureWithColumnLayout(recordings, excludedVideos));
    } else {
      return of(createGridLayout(excludedVideos));
    }
  }

  private RecordingLayoutData createGridLayout(Set<String> excludedVideos) {
    return RecordingLayoutData.builder()
        .layout(Map.of(GRID, Map.of(VIDEO_SOURCES, List.of("*"), EXCLUDED_VIDEOS, excludedVideos)))
        .build();
  }

  private RecordingLayoutData createPictureInPictureWithColumnLayout(
      List<ParticipantGroupRoomRecording> recordings, Set<String> excludedTrackNames) {
    var screenVideos = extractScreenShareSids(recordings);
    var moderators = extractModerators(recordings);
    var moderatorStartTimeMap = extractModeratorStartTimeMap(moderators);
    var respondentStartTimeMap = extractRespondentStartTimeMap(recordings, excludedTrackNames);
    var numberOfParticipants = moderatorStartTimeMap.size() + respondentStartTimeMap.size();

    var screenshareArea = calculateScreenshareArea(numberOfParticipants);

    boolean oldTrackNamingUsed = isOldTrackNamingUsed(recordings);

    Map<String, Object> layout = new HashMap<>();
    screenShareRegion(screenVideos, screenshareArea, oldTrackNamingUsed)
        .ifPresent(region -> layout.put(MAIN, region));
    getColumnRegion(moderatorStartTimeMap, respondentStartTimeMap, numberOfParticipants,
        oldTrackNamingUsed)
        .ifPresent(layout::putAll);

    var screenshareBoundsBuilder = ScreenshareBounds.builder()
        .screenshareArea(screenshareArea);

    return RecordingLayoutData.builder()
        .layout(layout)
        .screenshareBounds(screenshareBoundsBuilder.build())
        .build();
  }

  private Map<String, Instant> extractModeratorStartTimeMap(
      Set<ParticipantGroupRoomRecording> moderators) {
    return moderators.stream()
        .collect(
            groupingBy(
                ParticipantGroupRoomRecording::getParticipantId,
                collectingAndThen(
                    toList(),
                    list ->
                        list.stream()
                            .map(ParticipantGroupRoomRecording::getStartedAt)
                            .sorted()
                            .findFirst()
                            .orElse(null))));
  }

  private Optional<ScreenArea> calculateModeratorArea(
      List<ParticipantGroupRoomRecording> moderators) {
    if (!moderators.isEmpty()) {
      return of(ScreenArea.builder()
          .offsetX((long) PIP_X_POS)
          .offsetY(0L)
          .width((long) PIP_WIDTH)
          .height((long) PIP_HEIGHT)
          .build());
    }

    return empty();
  }

  private ScreenArea calculateScreenshareArea(int size) {
    var columnCnt = (size + MAX_SINGLE_COLUMN_CAMERAS - 1) / MAX_SINGLE_COLUMN_CAMERAS;

    if (columnCnt == 0) {
      return ScreenArea.builder()
          .offsetX(0L)
          .offsetY(0L)
          .width((long) VIDEO_WIDTH)
          .height((long) VIDEO_HEIGHT)
          .build();
    } else {
      var cameraWidth = resolveCameraWidth(size);
      var width = VIDEO_WIDTH - (columnCnt * cameraWidth);
      int x =
          size >= MAX_SINGLE_COLUMN_CAMERAS
              ? REDUCED_COLUMN_CAMERA_WIDTH * columnCnt
              : cameraWidth;

      return ScreenArea.builder()
          .offsetX((long) x)
          .offsetY(0L)
          .width((long) width)
          .height((long) VIDEO_HEIGHT)
          .build();
    }
  }

  private Map<String, Instant> extractRespondentStartTimeMap(
      List<ParticipantGroupRoomRecording> recordings, Set<String> excludedTrackNames) {
    return extractRespondentVideos(recordings).stream()
        .filter(recording -> isNotExcluded(recording, excludedTrackNames))
        .collect(
            groupingBy(
                ParticipantGroupRoomRecording::getParticipantId,
                collectingAndThen(
                    toList(),
                    list ->
                        list.stream()
                            .map(ParticipantGroupRoomRecording::getStartedAt)
                            .sorted()
                            .findFirst()
                            .orElse(null))));
  }

  private boolean isNotExcluded(ParticipantGroupRoomRecording recording,
                                Set<String> excludedTrackNames) {
    var respondentTrackName = isOldTrackNamingUsed(recording.getTrackName())
        ? recording.getTrackName() : RESPONDENT_CAMERA_WILDCARD;

    return excludedTrackNames.stream().noneMatch(e -> e.equals(respondentTrackName));
  }

  private Optional<Map<String, Object>> getColumnRegion(Map<String, Instant> moderatorMap,
                                                        Map<String, Instant> respondentMap,
                                                        int participantNumber,
                                                        boolean oldTrackNamingUsed) {
    var width = resolveCameraWidth(participantNumber);
    var height = resolveCameraHeight(participantNumber);

    var ids = extractParticipantIds(moderatorMap, respondentMap);

    Map<String, Object> layout = null;
    if (!ids.isEmpty()) {
      layout = new HashMap<>();
      var x = 0;
      var y = 0;
      var firstRespondent = moderatorMap.size();
      for (int i = 0; i < ids.size() && i < MAX_PARTICIPANTS_IN_ARCHIVE; i++) {
        var region = cellRegion(x, y, height, width, ids.get(i),
            oldTrackNamingUsed, i >= firstRespondent);
        layout.put("CELL_" + i, region);
        y += height;
        if ((i + 1) % MAX_SINGLE_COLUMN_CAMERAS == 0) {
          x += width;
          y = 0;
        }
      }
    }

    return ofNullable(layout);
  }

  private List<String> extractParticipantIds(Map<String, Instant> moderatorMap,
                                             Map<String, Instant> respondentMap) {
    return Stream.of(extractIds(moderatorMap), extractIds(respondentMap))
        .flatMap(Collection::stream)
        .toList();
  }

  private List<String> extractIds(Map<String, Instant> participantMap) {
    return participantMap.entrySet().stream()
        .sorted(Entry.comparingByValue())
        .map(Entry::getKey)
        .toList();
  }

  private Map<String, Object> cellRegion(int x, int y, int height, int width, String video,
                                         boolean oldTrackNamingUsed, boolean isRespondent) {
    return Map.of(
        X_POS, x,
        Y_POS, y,
        Z_POS, Z_ORDER_COLUMNS_REGION,
        HEIGHT, height,
        WIDTH, width,
        MAX_COLUMNS, MAX_VIDEOS_PER_SINGLE_CELL,
        MAX_ROWS, MAX_VIDEOS_PER_SINGLE_CELL,
        VIDEO_SOURCES,
        List.of(format(resolveColumnVideosWildcard(isRespondent, oldTrackNamingUsed), video)));
  }

  private int resolveCameraWidth(int size) {
    return size > MAX_FULL_SIZE_COLUMN_CAMERAS
        ? REDUCED_COLUMN_CAMERA_WIDTH
        : BASE_COLUMN_CAMERA_WIDTH;
  }

  private int resolveCameraHeight(int size) {
    return size > MAX_FULL_SIZE_COLUMN_CAMERAS
        ? REDUCED_CAMERA_HEIGHT_OFFSET
        : SINGLE_CAMERA_HEIGHT;
  }

  private Optional<Map<String, Object>> screenShareRegion(Set<String> videos,
                                                          ScreenArea screenArea,
                                                          boolean oldTrackNamingUsed) {

    if (!videos.isEmpty()) {
      return of(Map.of(
          X_POS, screenArea.getOffsetX(),
          Y_POS, screenArea.getOffsetY(),
          Z_POS, Z_ORDER_SCREEN_SHARE_REGION,
          WIDTH, screenArea.getWidth(),
          HEIGHT, screenArea.getHeight(),
          MAX_COLUMNS, MAX_COLUMNS_SCREEN_SHARE,
          MAX_ROWS, MAX_ROWS_SCREEN_SHARE,
          REUSE, SHOW_NEWEST,
          VIDEO_SOURCES, resolveScreenShareWildcard(oldTrackNamingUsed)));
    }

    return Optional.empty();
  }

  private Set<String> extractScreenShareSids(List<ParticipantGroupRoomRecording> recordings) {
    return extractRecordingSids(recordings, ParticipantGroupRoomRecording::isScreenShare);
  }

  private Set<ParticipantGroupRoomRecording> extractModerators(
      List<ParticipantGroupRoomRecording> recordings) {
    Predicate<ParticipantGroupRoomRecording> isModerator =
        ParticipantGroupRoomRecording::isModerator;
    Predicate<ParticipantGroupRoomRecording> isVideo = ParticipantGroupRoomRecording::isVideo;

    return recordings.stream()
        .filter(isModerator.and(isVideo))
        .collect(Collectors.toSet());
  }

  private Set<ParticipantGroupRoomRecording> extractRespondentVideos(
      List<ParticipantGroupRoomRecording> recordings) {
    Predicate<ParticipantGroupRoomRecording> isRespondent =
        ParticipantGroupRoomRecording::isRespondent;
    Predicate<ParticipantGroupRoomRecording> isVideo = ParticipantGroupRoomRecording::isVideo;
    Predicate<ParticipantGroupRoomRecording> isScreenShare =
        ParticipantGroupRoomRecording::isScreenShare;

    return recordings.stream()
        .filter(isRespondent.and(isVideo).and(not(isScreenShare)))
        .collect(Collectors.toSet());
  }

  private Set<String> extractRecordingSids(
      List<ParticipantGroupRoomRecording> recordings,
      Predicate<ParticipantGroupRoomRecording> type) {
    return recordings.stream()
        .filter(type)
        .map(ParticipantGroupRoomRecording::getSid)
        .collect(Collectors.toSet());
  }

  private String resolveModeratorVideosWildcard(boolean oldTrackNamingUsed) {
    return oldTrackNamingUsed ? MODERATOR_VIDEOS_WILDCARD_DEPRECATED : MODERATOR_VIDEOS_WILDCARD;
  }

  private String resolveColumnVideosWildcard(boolean oldTrackNamingUsed) {
    return oldTrackNamingUsed ? COLUMN_VIDEOS_WILDCARD_DEPRECATED : COLUMN_VIDEOS_WILDCARD;
  }

  private String resolveColumnVideosWildcard(boolean isRespondent, boolean oldTrackNamingUsed) {
    return isRespondent ? resolveColumnVideosWildcard(oldTrackNamingUsed)
        : resolveModeratorVideosWildcard(oldTrackNamingUsed);
  }

  private List<String> resolveScreenShareWildcard(boolean oldTrackNamingUsed) {
    return oldTrackNamingUsed
        ? List.of(SCREEN_SHARE_WILDCARD_DEPRECATED)
        : Arrays.stream(ParticipantRole.values())
            .map(role -> format(SCREEN_SHARE_WILDCARD, role))
            .toList();
  }

  private boolean hasRecordingsForLayout(List<ParticipantGroupRoomRecording> recordings,
                                         Set<String> excludedVideos) {
    return recordings.stream()
        .filter(not(ParticipantGroupRoomRecording::isAudio))
        .map(ParticipantGroupRoomRecording::getSid)
        .anyMatch(not(excludedVideos::contains));
  }

}
