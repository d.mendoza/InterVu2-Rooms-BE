package com.focusvision.intervu.room.api.event.participant.publisher;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantReadyEvent;

/**
 * Participant ready event publisher.
 */
public interface ParticipantReadyEventPublisher {

  void publish(ParticipantReadyEvent event);
}
