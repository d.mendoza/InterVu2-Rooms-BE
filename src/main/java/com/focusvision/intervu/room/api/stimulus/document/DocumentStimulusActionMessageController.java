package com.focusvision.intervu.room.api.stimulus.document;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.stimulus.document.model.DocumentStimulusActionMessageDto;
import com.focusvision.intervu.room.api.stimulus.document.service.DocumentStimulusActionService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller responsible for room document stimuli action related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class DocumentStimulusActionMessageController {

  private final DocumentStimulusActionService documentStimulusActionService;

  /**
   * Performs stimulus page change.
   *
   * @param message   Message payload.
   * @param principal User performing action.
   */
  @MessageMapping("stimulus.document.page")
  public void changePage(@Payload @Validated Message<DocumentStimulusActionMessageDto> message,
                         Principal principal) {
    log.debug("Receiving document page change stimulus action event [{}], from [{}].",
        message, principal.getName());

    var caller = extractParticipant(principal).orElseThrow();

    if (!caller.canModerate()) {
      throw new ActionForbiddenException("Changing document page forbidden.");
    }
    var action = message.getPayload();
    documentStimulusActionService
        .setActivePage(action.getStimulusId(), action.getContent(), action.getTimestamp(), caller);
  }

}
