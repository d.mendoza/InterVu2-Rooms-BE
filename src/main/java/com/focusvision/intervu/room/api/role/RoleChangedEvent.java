package com.focusvision.intervu.room.api.role;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.UUID;

/**
 * Model representing the role changed event.
 *
 * @author Branko Ostojic
 */
public record RoleChangedEvent(
    UUID roomId,
    UUID participantId,
    ParticipantRole role) {

}
