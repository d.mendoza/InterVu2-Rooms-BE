package com.focusvision.intervu.room.api.stimulus.event.handler;

import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawClearEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawRedoEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawUndoEvent;

/**
 * Handler contract for stimulus drawing events.
 *
 * @author Branko Ostojic
 */
public interface StimulusDrawEventHandler {

  /**
   * Handles the provided stimulus drawing event.
   *
   * @param event Drawing event.
   */
  void handle(StimulusDrawEvent event);

  /**
   * Handles the provided stimulus drawing undo event.
   *
   * @param event Drawing event.
   */
  void handle(StimulusDrawUndoEvent event);

  /**
   * Handles the provided stimulus drawing redo event.
   *
   * @param event Drawing event.
   */
  void handle(StimulusDrawRedoEvent event);

  /**
   * Handles the provided stimulus drawing clear event.
   *
   * @param event Drawing event.
   */
  void handle(StimulusDrawClearEvent event);
}
