package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.Optional;
import java.util.UUID;

/**
 * Resolver for chat handles.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatHandleResolver {

  /**
   * Resolves handle for meeting room private chat.
   *
   * @param sender    Sender participant.
   * @param recipient Recipient participant.
   * @return Chat handle or empty {@link Optional} if it is not available for sender.
   */
  Optional<UUID> resolveMeetingRoomPrivateChatHandle(AuthenticatedParticipant sender,
                                                     UUID recipient);

  /**
   * Resolves handle for waiting room private chat.
   *
   * @param sender    Sender participant.
   * @param recipient Recipient participant.
   * @return Chat handle or empty {@link Optional} if it is not available for sender.
   */
  Optional<UUID> resolveWaitingRoomPrivateChatHandle(AuthenticatedParticipant sender,
                                                     UUID recipient);

  /**
   * Resolves handle for meeting room internal chat.
   *
   * @param sender Sender participant.
   * @return Chat handle or empty {@link Optional} if it is not available for sender.
   */
  Optional<UUID> resolveMeetingRoomInternalChatHandle(AuthenticatedParticipant sender);

  /**
   * Resolves handle for waiting room internal chat.
   *
   * @param sender Sender participant.
   * @return Chat handle or empty {@link Optional} if it is not available for sender.
   */
  Optional<UUID> resolveWaitingRoomInternalChatHandle(AuthenticatedParticipant sender);

  /**
   * Resolves handle for meeting room respondents chat.
   *
   * @param sender Sender participant.
   * @return Chat handle or empty {@link Optional} if it is not available for sender.
   */
  Optional<UUID> resolveMeetingRoomRespondentsChatHandle(AuthenticatedParticipant sender);

  /**
   * Resolves handle for waiting room respondents chat.
   *
   * @param sender Sender participant.
   * @return Chat handle or empty {@link Optional} if it is not available for sender.
   */
  Optional<UUID> resolveWaitingRoomRespondentsChatHandle(AuthenticatedParticipant sender);

}
