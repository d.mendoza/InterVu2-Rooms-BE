package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.recording.event.RecordingProcessingFinishedEvent;

/**
 * Room recording finished event handler.
 */
public interface RoomRecordingFinishedEventHandler {

  void handle(RecordingProcessingFinishedEvent event);
}
