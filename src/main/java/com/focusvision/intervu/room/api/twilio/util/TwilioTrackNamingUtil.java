package com.focusvision.intervu.room.api.twilio.util;

import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import java.util.List;

/**
 * Util class for Twilio Track Naming operations.
 */
public class TwilioTrackNamingUtil {

  private static final String AUDIO_TRACK_NAME_PATTERN_DEPRECATED = ".+_.+_microphone";
  private static final String SCREEN_SHARE_TRACK_NAME_PATTERN_DEPRECATED = ".+_.+_screenshare";
  private static final String VIDEO_TRACK_NAME_PATTERN_DEPRECATED = ".+_.+_camera.*";
  public static final String RESPONDENT_CAMERA_WILDCARD = "RESPONDENT_camera_*";

  /**
   * Checks if track name uses old track naming pattern.
   *
   * @param trackName Track Name.
   * @return True if track name uses old track naming pattern.
   */
  public static boolean isOldTrackNamingUsed(String trackName) {
    return trackName.matches(AUDIO_TRACK_NAME_PATTERN_DEPRECATED)
        || trackName.matches(VIDEO_TRACK_NAME_PATTERN_DEPRECATED)
        || trackName.matches(SCREEN_SHARE_TRACK_NAME_PATTERN_DEPRECATED);
  }

  /**
   * Checks if any of the recordings uses old track naming pattern.
   *
   * @param recordings Recordings metadata.
   * @return True if any of the recordings uses old track naming pattern.
   */
  public static boolean isOldTrackNamingUsed(List<ParticipantGroupRoomRecording> recordings) {
    return recordings.stream()
        .map(ParticipantGroupRoomRecording::getTrackName)
        .anyMatch(TwilioTrackNamingUtil::isOldTrackNamingUsed);
  }

}
