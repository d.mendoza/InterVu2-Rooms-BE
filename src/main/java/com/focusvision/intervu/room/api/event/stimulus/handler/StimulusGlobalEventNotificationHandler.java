package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusBroadcastResultsEnableEvent;
import com.focusvision.intervu.room.api.service.StimulusNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/** {@link ApplicationEvent} specific implementation of a stimulus global event's handler. */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class StimulusGlobalEventNotificationHandler
    implements StimulusBroadcastResultsEnableEventHandler {

  private final StimulusNotificationService stimulusNotificationService;

  @Override
  @EventListener
  public void handle(StimulusBroadcastResultsEnableEvent event) {
    log.debug("Handling enable stimulus broadcast notification event (event: {})", event);

    stimulusNotificationService.sendStimulusBroadcastResultsNotification(
        event.roomId(), event.stimulusId());
  }
}
