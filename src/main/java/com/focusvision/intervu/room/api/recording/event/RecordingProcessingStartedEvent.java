package com.focusvision.intervu.room.api.recording.event;

import com.focusvision.intervu.room.api.model.CompositionLayout;
import java.time.Instant;
import java.util.UUID;

/**
 * Model for room recording processing started event.
 *
 * @author Branko Ostojic
 */
public record RecordingProcessingStartedEvent(UUID roomId,
                                              String roomPlatformId,
                                              String conferenceId,
                                              CompositionLayout layout,
                                              Instant roomStartedAt,
                                              Instant roomFinishedAt) {

}
