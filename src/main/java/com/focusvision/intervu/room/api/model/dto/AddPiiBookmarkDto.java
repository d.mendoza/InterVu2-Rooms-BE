package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

/**
 * DTO representing PII bookmark details to be added.
 */
@Data
@Accessors(chain = true)
@ApiModel("PII Bookmark")
public class AddPiiBookmarkDto {

  @NotNull
  @Length(max = 1000)
  @ApiModelProperty("PII bookmark note")
  private String note;
  @NotNull
  @ApiModelProperty("PII bookmark timestamp")
  private Long timestamp;
}
