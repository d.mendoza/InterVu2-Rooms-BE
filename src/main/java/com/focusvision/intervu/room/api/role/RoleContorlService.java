package com.focusvision.intervu.room.api.role;

import com.focusvision.intervu.room.api.common.event.EventPublisher;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.stimulus.snapshot.SnapshotAddedEvent;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Service for role control related operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoleContorlService {

  private final ResearchSessionService researchSessionService;
  private final ParticipantService participantService;
  private final EventPublisher eventPublisher;

  /**
   * Gets alternative roles for all room participants.
   *
   * @param roomId Room ID.
   * @return Participants alternative roles.
   */
  public List<ParticipantAlternativeRolesDto> getAlternativeRoles(UUID roomId) {
    return researchSessionService.get(roomId).getParticipants().stream()
        .map(participant -> ParticipantAlternativeRolesDto.builder()
            .participantId(participant.getId())
            .displayName(participant.getDisplayName())
            .currentRole(participant.getRole())
            .alternativeRoles(ParticipantAlternativeRoleResolver.resolve(participant))
            .build())
        .toList();
  }

  /**
   * Sets alternative role to participant.
   *
   * @param roomId        Room ID.
   * @param participantId Participant ID.
   * @param role          Alternative role.
   */
  public void setAlternativeRole(UUID roomId, UUID participantId, ParticipantRole role) {
    log.info("Setting alternative role (roomId: {}, participantId: {}, role: {})",
        roomId,
        participantId,
        role);

    var participant = participantService.findRoomParticipant(roomId, participantId)
        .orElseThrow(() -> new IntervuRoomException("Participant not in room."));
    if (!ParticipantAlternativeRoleResolver.resolve(participant).contains(role)) {
      throw new IntervuRoomException("Participant not applicable for alternative role.");
    }

    // TODO: implement actual role change
    var roleChangedEvent = new RoleChangedEvent(roomId, participantId, role);
    eventPublisher.publish(roleChangedEvent);
  }

}
