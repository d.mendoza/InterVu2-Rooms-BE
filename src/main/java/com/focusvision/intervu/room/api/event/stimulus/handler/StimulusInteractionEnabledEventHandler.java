package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusInteractionEnabledEvent;

/**
 * Handler for the {@link StimulusInteractionEnabledEvent}.
 */
public interface StimulusInteractionEnabledEventHandler {

  /**
   * Handles the provided {@link StimulusInteractionEnabledEvent}.
   *
   * @param event Stimulus interaction enabled event.
   */
  void handle(StimulusInteractionEnabledEvent event);
}
