package com.focusvision.intervu.room.api.waitingroom.authentication.controller;

import com.focusvision.intervu.room.api.model.dto.AuthDto;
import com.focusvision.intervu.room.api.security.IntervuUser;
import com.focusvision.intervu.room.api.waitingroom.authentication.service.ParticipantAuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller for Waiting Room authentication operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "waiting-room", produces = MediaType.APPLICATION_JSON_VALUE)
public class WaitingRoomAuthenticationController {

  private final ParticipantAuthenticationService participantAuthenticationService;

  /**
   * This method should check and authenticate participant. This is the LANDING point.
   *
   * @param token Unique invitation ID which identifies participant.
   * @return Room details and WS configuration.
   */
  @GetMapping("{token}")
  @ApiOperation(
      value = "Generate authentication token",
      notes = "Returns generated JWT token if invitation token is valid")
  public AuthDto authenticate(
      @ApiParam("Participant's invitation token.") @PathVariable String token) {
    log.info("Authenticating with invitation token: {}", token);

    // Send event to invalidate any existing WS connection?
    // (In case we want to force only one open session per participant)
    // Where to keep JWT tokens? In memory is not possible because of the cluster. Redis...?
    // Fetch invitation details - if not found throw 404 exception.
    // Generate JWT token with expiration of 1 year
    /// Store token maybe - this will help us with limiting access from one device?
    return participantAuthenticationService.authenticate(token);
  }

  /**
   * Authenticate for specific room.
   *
   * @param platformId Room ID.
   * @param caller     User performing action.
   * @return Authentication details.
   */
  @GetMapping("session/{platformId}/auth")
  @ApiOperation(
      value = "Generate authentication token for specified room",
      notes = "Returns generated JWT token if user has access to specified room")
  public AuthDto authenticate(@PathVariable String platformId,
                              @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Authenticating user {} for room {}", caller.getPlatformId(), platformId);

    return participantAuthenticationService.authenticate(platformId, caller.getPlatformId());
  }

}
