package com.focusvision.intervu.room.api.configuration;

import com.focusvision.intervu.room.api.configuration.domain.RetryProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

/**
 * Retry configuration class.
 *
 * @author Branko Ostojic
 */
@Configuration
@EnableRetry
@RequiredArgsConstructor
public class RetryConfiguration {

  private final RetryProperties properties;

  /**
   * Provides retry template for streaming service.
   *
   * @return Retry template.
   */
  @Bean
  public RetryTemplate streamingServiceRetryTemplate() {
    RetryTemplate retryTemplate = new RetryTemplate();

    var backOffPolicy = new FixedBackOffPolicy();
    backOffPolicy.setBackOffPeriod(
        properties.getStreamingServiceOperations().getBackOffIntervalMillis());
    retryTemplate.setBackOffPolicy(backOffPolicy);

    var retryPolicy = new SimpleRetryPolicy();
    retryPolicy.setMaxAttempts(properties.getStreamingServiceOperations().getMaxAttempts());
    retryTemplate.setRetryPolicy(retryPolicy);

    return retryTemplate;
  }
}
