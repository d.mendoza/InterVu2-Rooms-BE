package com.focusvision.intervu.room.api.event.room.model;

import java.util.UUID;

/**
 * Model for room expired event.
 */
public record RoomExpiredEvent(UUID roomId) {

}
