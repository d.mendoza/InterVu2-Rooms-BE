package com.focusvision.intervu.room.api.export.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing used stimulus ID values.
 */
@Getter
@Setter
@ToString
@ApiModel("Used Stimulus Export")
public class UsedStimulusExportDto {

  @ApiModelProperty("Stimulus ID")
  private UUID id;
  @ApiModelProperty("Stimulus platform ID")
  private String platformId;

}
