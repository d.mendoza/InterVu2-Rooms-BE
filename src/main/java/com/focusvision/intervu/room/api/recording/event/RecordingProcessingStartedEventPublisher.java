package com.focusvision.intervu.room.api.recording.event;

/**
 * Room recording processing started event publisher.
 */
public interface RecordingProcessingStartedEventPublisher {

  /**
   * Publishes provided event data.
   *
   * @param event Event data.
   */
  void publish(RecordingProcessingStartedEvent event);
}
