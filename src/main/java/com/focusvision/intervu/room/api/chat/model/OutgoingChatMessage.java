package com.focusvision.intervu.room.api.chat.model;

import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import java.util.UUID;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Model representing outgoing chat message.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ToString
public class OutgoingChatMessage {

  /**
   * Message ID.
   */
  private UUID id;
  /**
   * Message content.
   */
  @ToString.Exclude
  private String message;
  /**
   * Message sender platform ID.
   */
  private String senderId;
  /**
   * Message sender name.
   */
  private String senderName;
  /**
   * Message handle.
   */
  private UUID handle;
  /**
   * Chat source.
   */
  private ChatMessage.ChatSource source;
  /**
   * Chat type.
   */
  private ChatType type;
  /**
   * Message type.
   */
  private ChatMessage.MessageType messageType;
  /**
   * Message sent time.
   */
  private Long sentAt;
}
