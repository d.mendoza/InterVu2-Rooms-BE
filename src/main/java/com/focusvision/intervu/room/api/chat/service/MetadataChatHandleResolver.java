package com.focusvision.intervu.room.api.chat.service;

import static java.util.Optional.ofNullable;

import com.focusvision.intervu.room.api.chat.model.GroupChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ParticipantChatMetadataDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Metadata based chat handle resolver implementation of {@link ChatHandleResolver}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MetadataChatHandleResolver implements ChatHandleResolver {

  private final ChatMetadataService chatMetadataService;

  @Override
  public Optional<UUID> resolveMeetingRoomPrivateChatHandle(AuthenticatedParticipant sender,
                                                            UUID recipient) {
    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    return ofNullable(metadata.getDirect())
        .stream()
        .flatMap(Collection::stream)
        .map(ParticipantChatMetadataDto::getHandle)
        .filter(recipient::equals)
        .findFirst();
  }

  @Override
  public Optional<UUID> resolveWaitingRoomPrivateChatHandle(AuthenticatedParticipant sender,
                                                            UUID recipient) {
    var metadata = chatMetadataService.getWaitingRoomChatMetadata(sender);
    return ofNullable(metadata.getDirect())
        .stream()
        .flatMap(Collection::stream)
        .map(ParticipantChatMetadataDto::getHandle)
        .filter(recipient::equals)
        .findFirst();
  }

  @Override
  public Optional<UUID> resolveMeetingRoomInternalChatHandle(AuthenticatedParticipant sender) {
    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    return ofNullable(metadata.getInternal())
        .map(GroupChatMetadataDto::getHandle);
  }

  @Override
  public Optional<UUID> resolveWaitingRoomInternalChatHandle(AuthenticatedParticipant sender) {
    var metadata = chatMetadataService.getWaitingRoomChatMetadata(sender);
    return ofNullable(metadata.getInternal())
        .map(GroupChatMetadataDto::getHandle);
  }

  @Override
  public Optional<UUID> resolveMeetingRoomRespondentsChatHandle(AuthenticatedParticipant sender) {
    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    return ofNullable(metadata.getRespondents())
        .map(GroupChatMetadataDto::getHandle);
  }

  @Override
  public Optional<UUID> resolveWaitingRoomRespondentsChatHandle(AuthenticatedParticipant sender) {
    var metadata = chatMetadataService.getWaitingRoomChatMetadata(sender);
    return ofNullable(metadata.getRespondents())
        .map(GroupChatMetadataDto::getHandle);
  }
}
