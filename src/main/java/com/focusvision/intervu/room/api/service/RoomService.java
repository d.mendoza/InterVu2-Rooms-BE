package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.UUID;

/**
 * Service used for room related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface RoomService {

  /**
   * Starts specified room if user invoking this action is a moderator of that room and room is not
   * started or finished yet.
   *
   * @param roomId        Room to start.
   * @param participantId Participant starting the room.
   */
  void startRoom(UUID roomId, UUID participantId);

  /**
   * Joins specified room if user invoking this action is a participant of that room.
   *
   * @param roomId        Room ID.
   * @param participantId User platform ID.
   */
  void joinRoom(UUID roomId, UUID participantId);

  /**
   * Finishes the room in progress. Method is responsible for performing all actions related to the
   * session ending. That includes changing state, closing streaming services and sending
   * notifications.
   *
   * @param roomId        Room to start.
   * @param participantId Participant ID.
   */
  void finish(UUID roomId, UUID participantId);

  /**
   * Finishes the room in progress for which corresponding conference is in invalid state.
   *
   * @param roomId Room ID.
   */
  void finishIfBroken(UUID roomId);

  /**
   * Removes participant from research session. Both moderator and participant must be in the same
   * room.
   *
   * @param moderator     Moderator invoking the ban.
   * @param participantId Participant ID.
   */
  void banParticipant(AuthenticatedParticipant moderator, UUID participantId);

  /**
   * Toggle participant's microphone in the research session. Both moderator and participant must be
   * in the same room.
   *
   * @param moderator     Moderator.
   * @param participantId Participant.
   */
  void toggleParticipantMicrophone(AuthenticatedParticipant moderator, UUID participantId);

}
