package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * DTO representing the participant disconnected event.
 *
 * @author Branko Ostojic
 */
public record ParticipantDisconnectedEvent(UUID roomId,
                                           UUID participantId) {

}
