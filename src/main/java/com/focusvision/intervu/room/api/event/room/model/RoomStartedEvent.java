package com.focusvision.intervu.room.api.event.room.model;

import com.focusvision.intervu.room.api.model.entity.Participant;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

/**
 * Model for room started event.
 *
 * @author Branko Ostojic
 */
public record RoomStartedEvent(UUID roomId,
                               String roomPlatformId,
                               String conferenceId,
                               Instant startedAt,
                               Set<Participant> participants) {

}
