package com.focusvision.intervu.room.api.export.service;

import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.chat.service.ChatDataService;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.export.mapper.ChatMessageExportMapper;
import com.focusvision.intervu.room.api.export.mapper.ParticipantRecordingExportMapper;
import com.focusvision.intervu.room.api.export.model.ChatMessageExportDto;
import com.focusvision.intervu.room.api.export.model.ParticipantDataExportDto;
import com.focusvision.intervu.room.api.export.model.ParticipantRecordingExportDto;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.StreamingService;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ParticipantExportService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantExportServiceImpl implements ParticipantExportService {

  private final ResearchSessionService researchSessionService;
  private final ChatDataService chatDataService;
  private final StreamingService streamingService;
  private final ChatMessageExportMapper chatMessageExportMapper;
  private final ParticipantRecordingExportMapper participantRecordingExportMapper;

  @Override
  public List<ParticipantDataExportDto> export(String platformId) {
    log.debug("Exporting room personal data for participant {}", platformId);

    return researchSessionService.getResearchSessionsForParticipant(platformId)
        .stream()
        .map(researchSession -> export(researchSession, platformId))
        .toList();
  }

  private ParticipantDataExportDto export(ResearchSession researchSession, String platformId) {
    var chatMessages = exportChatMessages(researchSession, platformId);
    var recordings = exportRecordings(researchSession, platformId);

    return new ParticipantDataExportDto(researchSession.getPlatformId(), chatMessages, recordings);
  }

  private List<ChatMessageExportDto> exportChatMessages(ResearchSession researchSession,
                                                        String participantPlatformId) {
    return chatDataService
        .getRespondentChatMessages(researchSession.getId(), participantPlatformId)
        .stream()
        .map(chatMessageExportMapper::mapToExportDto)
        .toList();
  }

  private List<ParticipantRecordingExportDto> exportRecordings(ResearchSession researchSession,
                                                               String participantPlatformId) {

    Participant participant = researchSession.getParticipants().stream()
        .filter(p -> p.getPlatformId().equals(participantPlatformId))
        .findFirst()
        .orElseThrow(() -> new IntervuRoomException("Participant data for export not found"));

    return Optional.ofNullable(researchSession.getConference())
        .stream()
        .map(Conference::getRoomSid)
        .map(sid -> streamingService.getParticipantRecordings(sid, participant.getId()))
        .flatMap(Collection::stream)
        .map(participantRecordingExportMapper::mapToDto)
        .toList();
  }
}
