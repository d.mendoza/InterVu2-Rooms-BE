package com.focusvision.intervu.room.api.participant.testing.application;

import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingResult;

/**
 * Adapter for sending participant testing result notifications.
 */
public interface RespondentTestingNotificationSenderAdapter {

  /**
   * Sends the provided participant testing results.
   *
   * @param results Testing results.
   */
  void send(ParticipantTestingResult results);
}
