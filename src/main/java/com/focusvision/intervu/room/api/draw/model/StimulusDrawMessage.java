package com.focusvision.intervu.room.api.draw.model;

import com.focusvision.intervu.room.api.common.model.DrawingActionType;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Model representing stimulus draw action message.
 */
@Data
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class StimulusDrawMessage {

  /**
   * Drawing action type (e.g. draw, undo, redo, clear)
   */
  @NotNull
  private DrawingActionType type;

  /**
   * Drawing action content.
   */
  private String content;

  /**
   * Drawing action UTC timestamp.
   */
  @NotNull
  private Long timestamp;
}
