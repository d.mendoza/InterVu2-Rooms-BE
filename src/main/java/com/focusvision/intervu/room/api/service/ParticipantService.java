package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.entity.Participant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service used for participant related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ParticipantService {

  /**
   * Finds participant by it's ID.
   *
   * @param id Participant ID.
   * @return Participant details or empty {@link Optional}.
   */
  Optional<Participant> findById(UUID id);

  /**
   * Add conference token details to the participant profile.
   *
   * @param id              Participant UUID.
   * @param conferenceToken Conference token.
   * @return Updated participant.
   */
  Participant updateConferenceToken(UUID id, String conferenceToken);

  /**
   * Finds participant by its speaker PIN.
   *
   * @param pin Participant speaker PIN.
   * @return Participant details or empty {@link Optional}.
   */
  Optional<Participant> findBySpeakerPin(String pin);

  /**
   * Finds participant with provided platform ID and belonging to the provided research session.
   *
   * @param roomId     Research Session ID.
   * @param platformId Participant's platform ID.
   * @return Matching participant or empty {@link Optional}.
   */
  Optional<Participant> findRoomParticipant(UUID roomId, String platformId);

  /**
   * Finds participant with provided platform ID and belonging to the provided research session.
   *
   * @param roomPlatformId Research Session ID.
   * @param platformId     Participant's platform ID.
   * @return Matching participant or empty {@link Optional}.
   */
  Optional<Participant> findRoomParticipant(String roomPlatformId, String platformId);

  /**
   * Finds participant with provided participant ID and belonging to the provided research session.
   *
   * @param roomId        Research Session ID.
   * @param participantId Participant's ID.
   * @return Matching participant or empty {@link Optional}.
   */
  Optional<Participant> findRoomParticipant(UUID roomId, UUID participantId);

  /**
   * Finds participants belonging to the provided research session.
   *
   * @param roomId Research Session ID.
   * @return Matching participants.
   */
  List<Participant> findRoomParticipants(UUID roomId);

  /**
   * Removes participant from session by setting banned status to true.
   *
   * @param participantId Participant ID.
   */
  void banParticipant(UUID participantId);

  /**
   * Get participant by its ID.
   *
   * @param id Participant ID
   * @return Participant details
   */
  Participant get(UUID id);

}
