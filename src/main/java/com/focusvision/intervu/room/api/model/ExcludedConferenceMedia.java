package com.focusvision.intervu.room.api.model;

import java.util.Set;

/**
 * Record containing excluded conference video and audio data.
 */
public record ExcludedConferenceMedia(Set<String> video, Set<String> audio) {

}
