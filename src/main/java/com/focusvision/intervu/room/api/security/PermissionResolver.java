package com.focusvision.intervu.room.api.security;

import static com.focusvision.intervu.room.api.security.IntervuAuthority.ROOM_USER;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.participant.ParticipantPermissionResolver;
import java.util.HashSet;
import java.util.Set;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Service for resolving security permissions.
 *
 * @author Branko Ostojic
 */
public class PermissionResolver {

  /**
   * Resolves security permissions for provided participant.
   *
   * @param participant Participant.
   * @return Set of permissions.
   */
  public static Set<SimpleGrantedAuthority> resolve(Participant participant) {
    var permissions = new HashSet<SimpleGrantedAuthority>();
    permissions.add(new SimpleGrantedAuthority(ROOM_USER.name()));

    ParticipantPermissionResolver.resolve(participant).stream()
        .map(permission -> new SimpleGrantedAuthority(permission.name()))
        .forEach(permissions::add);

    return permissions;
  }

}
