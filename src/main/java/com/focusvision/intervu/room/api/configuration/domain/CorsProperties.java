package com.focusvision.intervu.room.api.configuration.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * CORS properties.
 **/
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "app.cors")
public class CorsProperties {
  /**
   * The list of allowed origins that be specific origins.
   */
  private final String[] allowedOrigins;

  /**
   * The list of allowed headers.
   */
  private final String[] allowedHeaders;
}
