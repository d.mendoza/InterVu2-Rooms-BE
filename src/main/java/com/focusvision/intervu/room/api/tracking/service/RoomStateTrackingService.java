package com.focusvision.intervu.room.api.tracking.service;

import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.tracking.model.entity.RoomStateAcknowledgement;
import com.focusvision.intervu.room.api.tracking.repository.RoomStateAcknowledgementRepository;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Service for room state tracking operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomStateTrackingService {

  private final RoomOffsetProvider roomOffsetProvider;
  private final RoomStateAcknowledgementRepository repository;

  /**
   * Stores room state ACK.
   *
   * @param participantId Participant ID.
   * @param roomId Room ID.
   * @param roomStateVersion Room state version.
   */
  public void ackRoomState(UUID participantId, UUID roomId, Long roomStateVersion) {
    log.debug(
        "Acknowledging room state (participantId: {}, roomId: {}, roomStateVersion: {})",
        participantId,
        roomId,
        roomStateVersion);

    Long offset = roomOffsetProvider.calculateOffset(roomId);
    var roomStateAcknowledgement =
        new RoomStateAcknowledgement()
            .setRoomId(roomId)
            .setParticipantId(participantId)
            .setVersion(roomStateVersion)
            .setOffset(offset);

    repository.save(roomStateAcknowledgement);
  }
}
