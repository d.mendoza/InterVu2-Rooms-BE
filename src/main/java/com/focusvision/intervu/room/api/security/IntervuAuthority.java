package com.focusvision.intervu.room.api.security;

/**
 * Application authorities.
 *
 * @author Branko Ostojic
 */
public enum IntervuAuthority {
  /**
   * InterVu resources authority.
   */
  INTERVU_USER,
  /**
   * Room resources authority.
   */
  ROOM_USER
}
