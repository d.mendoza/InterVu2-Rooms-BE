package com.focusvision.intervu.room.api.participant;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.security.ParticipantPermission;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for resolving participant permissions.
 *
 * @author Branko Ostojic
 */
public class ParticipantPermissionResolver {

  /**
   * Resolves permissions for provided participant.
   *
   * @param participant Participant.
   * @return Set of applicable permissions.
   */
  public static Set<ParticipantPermission> resolve(Participant participant) {
    return Arrays.stream(ParticipantPermission.values())
        .filter(permission -> hasPermission(permission, participant))
        .collect(Collectors.toUnmodifiableSet());
  }

  private static boolean hasPermission(ParticipantPermission permission,
                                       Participant participant) {
    return switch (permission) {
      case BOOKMARK_PII_ADD, BOOKMARK_REGULAR_ADD -> canAddBookmark(participant);
      case BOOKMARK_READ -> participant.isInternal();
      case DRAWING_CONTROL, PARTICIPANT_CONTROL, STIMULI_CONTROL -> participant.canModerate();
      case DRAWING_READ,
          PARTICIPANT_TESTING_READ,
          PARTICIPANT_TESTING_REPORT,
          ROOM_READ,
          ROOM_JOIN,
          STIMULI_READ,
          SCROLL_READ -> true;
      case ROOM_CONTROL -> canControlRoom(participant);
      case STIMULI_STATE_READ -> canReadStimuliState(participant);
      case STIMULI_SNAPSHOT_ADD -> canAddStimuliSnapshot(participant);
      case PIN_CODES_READ -> participant.isForstaStaffMember();
      case ROLE_CONTROL -> canControlRoles(participant);
    };
  }

  private static boolean canAddBookmark(Participant participant) {
    return participant.isModerator()
        || participant.getModerator()
        || participant.isObserver()
        || participant.isAdmin()
        || participant.isProjectManager()
        || participant.isProjectOperator()
        || participant.isTranslator();
  }

  private static boolean canControlRoom(Participant participant) {
    return participant.isAdmin()
        || participant.isProjectManager()
        || participant.isProjectOperator()
        || (participant.isModerator()
        && participant.getResearchSession().getProject().isSelfService());
  }

  private static boolean canReadStimuliState(Participant participant) {
    return participant.canModerate()
        || participant.isObserver()
        || participant.isTranslator();
  }

  private static boolean canAddStimuliSnapshot(Participant participant) {
    return participant.isModerator()
        || participant.getModerator()
        || participant.isObserver()
        || participant.isAdmin()
        || participant.isProjectManager()
        || participant.isProjectOperator();
  }

  private static boolean canControlRoles(Participant participant) {
    return participant.isAdmin()
        || participant.isProjectManager()
        || participant.isProjectOperator()
        || participant.isModerator();
  }
}
