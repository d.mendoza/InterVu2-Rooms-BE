package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.room.model.RoomStartedEvent;

/**
 * Room started event publisher.
 */
public interface RoomStartedEventPublisher {

  void publish(RoomStartedEvent event);
}
