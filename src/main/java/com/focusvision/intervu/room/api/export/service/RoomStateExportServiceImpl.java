package com.focusvision.intervu.room.api.export.service;

import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.export.mapper.RoomStateLogExportMapper;
import com.focusvision.intervu.room.api.export.model.RoomStateLogExportDto;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.state.RoomStateProvider;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomStateExportService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomStateExportServiceImpl implements RoomStateExportService {

  private final RoomStateProvider roomStateProvider;
  private final RoomOffsetProvider roomOffsetProvider;
  private final ResearchSessionService researchSessionService;
  private final RoomStateLogExportMapper roomStateLogExportMapper;

  @Override
  public Optional<List<RoomStateLogExportDto>> export(String platformId) {
    log.debug("Exporting room state logs for room with platform ID {}.", platformId);

    return researchSessionService.fetch(platformId)
        .map(this::getLogs);
  }

  private List<RoomStateLogExportDto> getLogs(ResearchSession room) {
    return roomStateProvider.getLogsForRoom(room.getId()).stream()
        .map(log -> map(log, room.getConference().getRecordingOffset()))
        .toList();
  }

  private RoomStateLogExportDto map(RoomStateLog roomStateLog,
                                    Integer roomRecordingOffset) {
    Long alignedOffset = roomOffsetProvider
        .alignWithRecordingOffset(roomStateLog.getOffset(), roomRecordingOffset);
    return roomStateLogExportMapper.mapToExportDto(roomStateLog, alignedOffset);
  }
}
