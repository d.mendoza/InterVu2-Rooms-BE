package com.focusvision.intervu.room.api.scroll.service;

import com.focusvision.intervu.room.api.scroll.model.StimulusScrollMessage;
import java.util.UUID;

/**
 * Service for handling scroll actions on stimulus context.
 */
public interface StimulusScrollService {

  /**
   * Handle scrolling on stimulus action.
   *
   * @param roomId                Room ID.
   * @param stimulusId            Stimulus ID.
   * @param participantId         Room participant performing the scroll action.
   * @param stimulusScrollMessage Scroll action message.
   */
  void scroll(UUID roomId, UUID stimulusId, UUID participantId,
              StimulusScrollMessage stimulusScrollMessage);
}
