package com.focusvision.intervu.room.api.common.model;

/**
 * Enum that represents drawing action type.
 *
 * @author Branko Ostojic
 */
public enum DrawingActionType {
  /**
   * Drawing.
   */
  DRAW,
  /**
   * Undo drawing.
   */
  UNDO,
  /**
   * Redo drawing.
   */
  REDO,
  /**
   * Clear drawing.
   */
  CLEAR
}
