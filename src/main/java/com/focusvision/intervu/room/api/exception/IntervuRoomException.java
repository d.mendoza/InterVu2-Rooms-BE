package com.focusvision.intervu.room.api.exception;


import static com.focusvision.intervu.room.api.exception.ErrorCode.BAD_REQUEST;

import lombok.Getter;

/**
 * General custom exception class used for various cases.
 *
 * @author Branko Ostojic
 */
@Getter
public class IntervuRoomException extends RuntimeException {

  private final ErrorCode code;

  public IntervuRoomException() {
    this.code = BAD_REQUEST;
  }

  public IntervuRoomException(ErrorCode code) {
    this.code = code;
  }

  public IntervuRoomException(String message) {
    super(message);
    this.code = BAD_REQUEST;
  }

  public IntervuRoomException(ErrorCode code, String message) {
    super(message);
    this.code = code;
  }

  public IntervuRoomException(ErrorCode code, Throwable cause) {
    super(cause);
    this.code = code;
  }

  public IntervuRoomException(ErrorCode code, String message, Throwable cause) {
    super(message, cause);
    this.code = code;
  }
}
