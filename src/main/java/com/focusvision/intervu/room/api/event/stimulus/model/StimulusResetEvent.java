package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus reset event.
 */
public record StimulusResetEvent(UUID roomId) {

}
