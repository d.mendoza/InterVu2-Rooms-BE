package com.focusvision.intervu.room.api.common.model;

/**
 * Drawing context type (e.g. white board, stimulus, etc...)
 *
 * @author Branko Ostojic
 */
public enum DrawingContextType {
  /**
   * Drawing on stimulus.
   */
  STIMULUS
}
