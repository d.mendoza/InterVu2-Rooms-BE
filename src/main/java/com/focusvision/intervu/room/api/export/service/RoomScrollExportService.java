package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.model.RoomScrollLogExportDto;
import java.util.List;
import java.util.Optional;

/**
 * Service for exporting room scroll data.
 */
public interface RoomScrollExportService {

  /**
   * Gets the scroll export data for provided room ID.
   *
   * @param platformId Room platform ID.
   * @return List of room scroll logs or empty {@link Optional} if room can't be found.
   */
  Optional<List<RoomScrollLogExportDto>> export(String platformId);
}
