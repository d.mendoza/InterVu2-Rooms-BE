package com.focusvision.intervu.room.api.chat.api;

import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.chat.model.ChatHistoryDto;
import com.focusvision.intervu.room.api.chat.model.ChatMetadataDto;
import com.focusvision.intervu.room.api.chat.service.ChatDataService;
import com.focusvision.intervu.room.api.chat.service.ChatMetadataService;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller for Waiting Room chat data related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "waiting-room/chat", produces = MediaType.APPLICATION_JSON_VALUE)
public class WaitingRoomChatController {

  private final ChatDataService chatDataService;
  private final ChatMetadataService chatMetadataService;

  /**
   * Gets waiting room chat metadata.
   *
   * @param caller Authenticated participant.
   * @return Chat metadata.
   */
  @GetMapping("meta")
  @ApiOperation(
      value = "Gets waiting room chat metadata.",
      notes = "Gets waiting room chat metadata used for creating chat UI")
  public ChatMetadataDto chatMetadata(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching waiting room chat metadata for participant {}.", caller.getId());

    return chatMetadataService.getWaitingRoomChatMetadata(caller);
  }

  /**
   * Gets waiting room respondents chat messages.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping("respondents")
  @ApiOperation(
      value = "Gets waiting room chat messages",
      notes = "Gets waiting room chat messages history for specified handle")
  public ChatHistoryDto respondentChatMessages(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching waiting room chat messages for participant {}.", caller.getId());

    return chatDataService.getWaitingRoomRespondentsChatMessages(caller);
  }

  /**
   * Gets waiting room internal chat messages.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping("internal")
  @ApiOperation(
      value = "Gets waiting room internal chat messages",
      notes = "Gets waiting room internal chat messages history for specified handle")
  public ChatHistoryDto internalChatMessages(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching all waiting room internal chat messages for participant {}.",
        caller.getId());

    return chatDataService.getWaitingRoomInternalChatMessages(caller);
  }

  /**
   * Gets waiting room private chat messages.
   *
   * @param handle Chat handle.
   * @param caller Authenticated participant.
   */
  @GetMapping("private/{handle}")
  @ApiOperation(
      value = "Gets waiting room private chat messages",
      notes = "Gets waiting room private chat messages history")
  public ChatHistoryDto privateChatMessages(
      @ApiParam("Chat messages handle.") @PathVariable String handle,
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching waiting room private chat messages for participant {}.", caller.getId());

    return chatDataService.getWaitingRoomPrivateChatMessages(caller, fromString(handle));
  }

}
