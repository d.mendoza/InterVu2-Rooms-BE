package com.focusvision.intervu.room.api.common.model;

/**
 * Stimulus action type.
 */
public enum StimulusActionType {
  /**
   * Vote action on poll stimulus.
   */
  POLL_VOTED,
  /**
   * Ranking action on poll stimulus.
   */
  POLL_RANKED,
  /**
   * Text answer action on poll stimulus.
   */
  POLL_ANSWERED,
  /**
   * Page changed on document stimulus.
   */
  DOCUMENT_PAGE_CHANGED
}
