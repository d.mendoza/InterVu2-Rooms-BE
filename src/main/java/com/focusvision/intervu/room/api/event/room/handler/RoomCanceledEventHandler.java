package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.event.room.model.RoomCanceledEvent;

/**
 * Room canceled event handler.
 */
public interface RoomCanceledEventHandler {

  void handle(RoomCanceledEvent event);
}
