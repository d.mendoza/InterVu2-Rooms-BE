package com.focusvision.intervu.room.api.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.messaging.RabbitMqProducer;
import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ConferenceCheckSenderAdapter}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqConferenceCheckSenderAdapter implements ConferenceCheckSenderAdapter {

  private final MessagingProperties messagingProperties;
  private final RabbitMqProducer rabbitMqProducer;

  @Override
  public void send(RunningConferenceCheck conference) {
    log.debug("Sending running conference check event: [{}]", conference);

    try {
      rabbitMqProducer.send(messagingProperties.getConferencesRunningQueue(), conference);
    } catch (MessagingException e) {
      log.error(format("Error sending running conference check event: %s.", conference), e);
    }
  }

}
