package com.focusvision.intervu.room.api.model.streaming;

import com.twilio.rest.video.v1.Room;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Model for group room.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@Accessors(chain = true)
public class GroupRoom {

  /**
   * Group room SID.
   */
  private String sid;
  /**
   * Group room duration.
   */
  private Integer duration;
  /**
   * GMT date and time in milliseconds when room was created.
   */
  private Long dateCreatedMillisGmt;

  /**
   * Group room status.
   */
  private Room.RoomStatus status;
}
