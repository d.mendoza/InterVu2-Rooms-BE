package com.focusvision.intervu.room.api.security;

import static java.util.Optional.of;

import com.focusvision.intervu.room.api.model.entity.Participant;
import java.security.Principal;
import java.util.Optional;

/**
 * Helper class for {@link Principal} related operations.
 *
 * @author Branko Ostojic
 */
public class PrincipalHelper {

  /**
   * Extracts {@link Participant} from the principal.
   *
   * @param caller Authenticated caller.
   * @return Participant if present, or empty {@link Optional}.
   */
  public static Optional<AuthenticatedParticipant> extractParticipant(Principal caller) {
    return of(caller)
        .filter(principal -> principal instanceof ParticipantTokenBasedAuthentication)
        .map(principal -> (AuthenticatedParticipant) ((ParticipantTokenBasedAuthentication) caller)
            .getPrincipal());
  }

  /**
   * Extracts {@link IntervuUser} from the principal.
   *
   * @param caller Authenticated caller.
   * @return IntervuUser if present, or empty {@link Optional}.
   */
  public static Optional<IntervuUser> extractIntervuUser(Principal caller) {
    return of(caller)
        .filter(principal -> principal instanceof IntervuUserTokenBasedAuthentication)
        .map(principal ->
            (IntervuUser) ((IntervuUserTokenBasedAuthentication) caller).getPrincipal());
  }
}
