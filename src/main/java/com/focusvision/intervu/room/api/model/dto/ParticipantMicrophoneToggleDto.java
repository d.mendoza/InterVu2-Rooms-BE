package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the participant microphone toggle event.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@ApiModel("ParticipantMicrophoneToggle")
public class ParticipantMicrophoneToggleDto {
}
