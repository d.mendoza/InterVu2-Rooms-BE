package com.focusvision.intervu.room.api.export.model;

import com.focusvision.intervu.room.api.common.model.StimulusActionType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.UUID;
import lombok.Value;

/**
 * DTO representing the stimulus action log for export.
 */
@Value
@ApiModel("Stimulus Action Log Export")
public class StimulusActionLogExportDto {

  @ApiModelProperty("Action type")
  StimulusActionType actionType;

  @ApiModelProperty("Stimulus ID")
  UUID stimulusId;

  @ApiModelProperty("Stimulus platform ID")
  String stimulusPlatformId;

  @ApiModelProperty("Participant ID")
  UUID participantId;

  @ApiModelProperty("Participant platform ID")
  String participantPlatformId;

  @ApiModelProperty("Event offset from session start")
  Long offset;

  @ApiModelProperty("Action version")
  Long version;

  @ApiModelProperty("Action content")
  String content;

  @ApiModelProperty("Action time")
  Instant addedAt;

}
