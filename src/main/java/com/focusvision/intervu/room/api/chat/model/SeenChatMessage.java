package com.focusvision.intervu.room.api.chat.model;

import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model representing latest seen chat message.
 *
 * @author Branko Ostojic
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeenChatMessage {

  /**
   * Seen chat message ID.
   */
  @NotNull
  private UUID id;
}
