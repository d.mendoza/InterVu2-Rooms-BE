package com.focusvision.intervu.room.api.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.messaging.RabbitMqProducer;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link RecordingCheckSenderAdapter}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqRecordingCheckSenderAdapter implements RecordingCheckSenderAdapter {

  private final MessagingProperties messagingProperties;
  private final RabbitMqProducer rabbitMqProducer;

  @Override
  public void send(ProcessingRecordingCheck recording) {
    log.debug("Sending processing recording check event: [{}]", recording);

    try {
      rabbitMqProducer.send(messagingProperties.getConferencesProcessingQueue(), recording);
    } catch (MessagingException e) {
      log.error(format("Error sending processing recording check event: %s.", recording), e);
    }
  }

}
