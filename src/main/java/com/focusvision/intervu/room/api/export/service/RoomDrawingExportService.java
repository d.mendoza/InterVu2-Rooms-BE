package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.model.RoomDrawingLogExportDto;
import java.util.List;
import java.util.Optional;

/**
 * Service for exporting room drawing data.
 *
 * @author Branko Ostojic
 */
public interface RoomDrawingExportService {

  /**
   * Gets the export data for provided room ID.
   *
   * @param platformId Room platform ID.
   * @return List of room drawing logs or empty {@link Optional} if room can't be found.
   */
  Optional<List<RoomDrawingLogExportDto>> export(String platformId);
}
