package com.focusvision.intervu.room.api.state.stimulus.publisher;

import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;

/**
 * Publisher for document stimulus page change event.
 */
public interface DocumentStimulusPageChangeEventPublisher {

  /**
   * Publishes the provided document stimulus page change event.
   *
   * @param event Document stimulus page change event.
   */
  void publish(DocumentStimulusPageChangeEvent event);
}
