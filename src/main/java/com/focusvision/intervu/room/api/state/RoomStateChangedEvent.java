package com.focusvision.intervu.room.api.state;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.state.model.RoomState;

/**
 * Room state changed event model.
 */
public record RoomStateChangedEvent(
    RoomStateChangeType eventType,
    RoomState roomState) {

}
