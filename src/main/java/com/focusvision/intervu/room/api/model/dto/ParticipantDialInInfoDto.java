package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing participant's telephony info.
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString
@ApiModel("Participant's dial-in Info")
public class ParticipantDialInInfoDto {

  @ApiModelProperty("Display name")
  private String displayName;
  @ApiModelProperty("Participant's role")
  private ParticipantRole role;
  @ApiModelProperty("Dial-in numbers")
  private List<String> dialInNumbers;
  @ApiModelProperty("Operator PIN")
  private String operatorPin;
  @ApiModelProperty("Listener PIN")
  private String listenerPin;
  @ApiModelProperty("Speaker PIN")
  private String speakerPin;
  @ApiModelProperty("Participant having admin permissions")
  private boolean admin;
  @ApiModelProperty("Participant having project manager permissions")
  private boolean projectManager;
  @ApiModelProperty("Participant having project operator permissions")
  private boolean projectOperator;
}
