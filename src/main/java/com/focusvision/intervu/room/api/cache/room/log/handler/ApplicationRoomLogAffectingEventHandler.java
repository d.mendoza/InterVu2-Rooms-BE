package com.focusvision.intervu.room.api.cache.room.log.handler;

import com.focusvision.intervu.room.api.cache.room.log.event.ClearRoomLogCacheEvent;
import com.focusvision.intervu.room.api.cache.room.log.publisher.RoomLogCacheEvictionEventPublisher;
import com.focusvision.intervu.room.api.event.room.handler.RoomFinishedEventHandler;
import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * {@link EventListener} specific implementation of a {@link RoomFinishedEventHandler}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class ApplicationRoomLogAffectingEventHandler implements RoomFinishedEventHandler {

  private final RoomLogCacheEvictionEventPublisher publisher;

  @Override
  @EventListener
  public void handle(RoomFinishedEvent event) {
    log.debug("Handling room finished event: {}", event);

    publisher.publish(new ClearRoomLogCacheEvent(event.roomId()));
  }
}
