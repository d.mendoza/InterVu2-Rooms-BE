package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusBroadcastResultsDisableEvent;

/**
 * Handler for the {@link StimulusBroadcastResultsDisableEvent}.
 */
public interface StimulusBroadcastResultsDisableEventHandler {

  /**
   * Handles the provided {@link StimulusBroadcastResultsDisableEvent}.
   *
   * @param event Stimulus broadcast results disable event.
   */
  void handle(StimulusBroadcastResultsDisableEvent event);

}
