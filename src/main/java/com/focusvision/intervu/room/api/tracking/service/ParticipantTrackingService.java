package com.focusvision.intervu.room.api.tracking.service;

import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.tracking.model.ParticipantActionType;
import com.focusvision.intervu.room.api.tracking.model.entity.ParticipantActionLog;
import com.focusvision.intervu.room.api.tracking.repository.ParticipantActionLogRepository;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Service for participant tracking operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantTrackingService {

  private final RoomOffsetProvider roomOffsetProvider;
  private final ParticipantActionLogRepository repository;

  /**
   * Stores participant tracking data.
   *
   * @param participantId Participant ID.
   * @param roomId Room ID.
   * @param type Tracking event type.
   */
  public void track(UUID participantId, UUID roomId, ParticipantActionType type) {
    log.debug(
        "Persisting participant action (participantId: {}, roomId: {}, type: {})",
        participantId,
        roomId,
        type);

    Long offset = roomOffsetProvider.calculateOffset(roomId);
    var participantActionLog =
        new ParticipantActionLog()
            .setRoomId(roomId)
            .setParticipantId(participantId)
            .setType(type)
            .setOffset(offset);

    repository.save(participantActionLog);
  }
}
