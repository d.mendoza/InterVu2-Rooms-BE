package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.common.model.StimulusType;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link Stimulus} entity.
 *
 * @author Branko Ostojic
 */
public interface StimulusRepository extends JpaRepository<Stimulus, UUID> {

  /**
   * Finds stimulus by ID and belonging research session ID.
   *
   * @param id                Stimulus ID.
   * @param researchSessionId Research session ID.
   * @return Matching stimulus od empty {@link Optional}.
   */
  Optional<Stimulus> findByIdAndResearchSessionId(UUID id, UUID researchSessionId);

  /**
   * Finds all stimuli belonging to research session.
   *
   * @param researchSessionId Research session ID.
   * @return All stimuli belonging to the research session.
   */
  List<Stimulus> findAllByResearchSessionIdOrderByPosition(UUID researchSessionId);

  /**
   * Finds stimulus by provided ID and type belonging to provided research session ID.
   *
   * @param stimulusId        Stimulus ID.
   * @param researchSessionId Research session ID
   * @param type              Stimulus type.
   * @return Matching stimulus or empty {@link Optional}.
   */
  Optional<Stimulus> findByIdAndResearchSessionIdAndType(UUID stimulusId,
                                                         UUID researchSessionId,
                                                         StimulusType type);
}
