package com.focusvision.intervu.room.api.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.focusvision.intervu.room.api.configuration.domain.TwilioProperties;
import com.focusvision.intervu.room.api.mapper.decorator.ParticipantMapperDecorator;
import com.focusvision.intervu.room.api.model.dto.DialInInfoDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantDialInInfoDto;
import com.focusvision.intervu.room.api.model.entity.Participant;
import org.mapstruct.BeforeMapping;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for {@link com.focusvision.intervu.room.api.model.entity.Participant} entity.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, uses = TwilioProperties.class)
@DecoratedWith(ParticipantMapperDecorator.class)
public interface ParticipantMapper {

  /**
   * Maps participant to dial-in DTO.
   *
   * @param participant Participant.
   * @return Resulting DTO.
   */
  @Mapping(target = "speakerPin", ignore = true)
  DialInInfoDto mapToDialInInfoDto(Participant participant);

  /**
   * Maps participant to participant dial-in resolver data.
   *
   * @param participant Participant.
   * @return Resulting DTO.
   */
  @Mapping(target = "speakerPin", ignore = true)
  ParticipantDialInInfoDto mapToParticipantDialInInfoDto(Participant participant);

  /**
   * Sets up mapping for {@link #mapToDialInInfoDto(Participant)}.
   *
   * @param target Dial-in info DTO.
   * @param source Participant.
   */
  @BeforeMapping
  default void setUpMapping(@MappingTarget DialInInfoDto target, Participant source) {
    if (source.isRespondent()) {
      target.setSpeakerPin(source.getSpeakerPin());
    } else {
      target.setListenerPin(source.getResearchSession().getListenerPin());
      if (source.isForstaStaffMember()) {
        target
            .setSpeakerPin(source.getResearchSession().getSpeakerPin())
            .setOperatorPin(source.getResearchSession().getOperatorPin());
      } else if (source.isModerator()) {
        target.setSpeakerPin(source.getResearchSession().getSpeakerPin());
      }
    }
  }

  /**
   * Sets up mapping for {@link #mapToParticipantDialInInfoDto(Participant)}.
   *
   * @param target Participant dial-in info DTO.
   * @param source Participant.
   */
  @BeforeMapping
  default void setUpMapping(@MappingTarget ParticipantDialInInfoDto target, Participant source) {
    var researchSession = source.getResearchSession();
    if (source.isRespondent()) {
      target.setSpeakerPin(source.getSpeakerPin());
    } else if (source.isModerator()) {
      target.setSpeakerPin(researchSession.getSpeakerPin());
    } else if (source.isForstaStaffMember()) {
      target.setOperatorPin(researchSession.getOperatorPin());
    } else if (source.isObserver() || source.isTranslator()) {
      target.setListenerPin(researchSession.getListenerPin());
    }
  }
}
