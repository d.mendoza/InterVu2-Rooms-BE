package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.model.entity.Conference;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link Conference} entity.
 *
 * @author Branko Ostojic
 */
public interface ConferenceRepository extends JpaRepository<Conference, UUID> {

  /**
   * Finds all conferences with matching completed flag.
   *
   * @param completed Conference completed flag.
   * @return List of matching conferences.
   */
  List<Conference> findByCompleted(boolean completed);

  /**
   * Finds conference by provided research session platform ID.
   *
   * @param platformId Research session platform ID.
   * @return Matching conference or empty {@link Optional}.
   */
  Optional<Conference> findByResearchSessionPlatformId(String platformId);
}
