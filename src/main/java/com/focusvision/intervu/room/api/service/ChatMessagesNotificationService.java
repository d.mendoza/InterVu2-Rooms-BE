package com.focusvision.intervu.room.api.service;

import java.util.UUID;

/**
 * Service used for sending research session's chat messages.
 */
public interface ChatMessagesNotificationService {

  /**
   * Sends chat messages of research session with specified id.
   *
   * @param id Research Session ID.
   */
  void sendChatMessages(UUID id);
}
