package com.focusvision.intervu.room.api.bookmark.event.model;

import com.focusvision.intervu.room.api.bookmark.model.BookmarkDetails;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.UUID;

/**
 * Model representing the bookmark added event.
 *
 * @author Branko Ostojic
 */
public record BookmarkAddedEvent(
    UUID roomId,
    AuthenticatedParticipant participant,
    BookmarkDetails bookmark) {
}
