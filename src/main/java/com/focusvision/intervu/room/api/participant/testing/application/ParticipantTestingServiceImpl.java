package com.focusvision.intervu.room.api.participant.testing.application;

import com.focusvision.intervu.room.api.common.provider.ResearchSessionProvider;
import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingInfo;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ParticipantTestingService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantTestingServiceImpl implements ParticipantTestingService {

  private final ParticipantTestNotificationService notificationService;
  private final ResearchSessionProvider researchSessionProvider;

  @Override
  public ParticipantTestingInfo getInfo(UUID roomId) {
    log.debug("Getting participant testing info for room {}.", roomId);

    var room = researchSessionProvider.get(roomId);
    return new ParticipantTestingInfo(room.isUseWebcams());
  }

  @Override
  public void reportSuccess(String participantPlatformId, String roomPlatformId) {
    log.debug("Participant {} passed testing.", participantPlatformId);

    notificationService
        .notifyTestingSuccess(participantPlatformId, roomPlatformId);
  }

  @Override
  public void reportFail(String participantPlatformId, String roomPlatformId) {
    log.debug("Participant {} failed testing.", participantPlatformId);

    notificationService
        .notifyTestingFailure(participantPlatformId, roomPlatformId);
  }
}
