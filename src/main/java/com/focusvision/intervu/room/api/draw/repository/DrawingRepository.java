package com.focusvision.intervu.room.api.draw.repository;

import com.focusvision.intervu.room.api.draw.model.DrawingLog;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for {@link DrawingLog} entity.
 *
 * @author Branko Ostojic
 */
public interface DrawingRepository extends JpaRepository<DrawingLog, UUID> {

  /**
   * Gets drawing logs for specified room ordered by the offset and creation date.
   *
   * @param roomId Room ID.
   * @return Matching drawing logs.
   */
  List<DrawingLog> findAllByRoomIdOrderByOffsetAscAddedAtAsc(UUID roomId);

  /**
   * Gets drawing logs for specified room and context ordered by the offset and creation date.
   *
   * @param roomId    Room ID.
   * @param contextId Context ID.
   * @return Matching drawing logs.
   */
  List<DrawingLog> findAllByRoomIdAndContextIdOrderByOffsetAscAddedAtAsc(UUID roomId,
                                                                         UUID contextId);
}
