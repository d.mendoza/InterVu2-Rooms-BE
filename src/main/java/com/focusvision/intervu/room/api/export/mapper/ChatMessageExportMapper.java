package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.export.model.ChatMessageExportDto;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Mapper for {@link ChatMessage} entity.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface ChatMessageExportMapper {

  /**
   * Maps chat message to export DTO.
   *
   * @param chatMessage Chat message to be mapped.
   * @return Resulting DTO.
   */
  @Mappings(value = {
      @Mapping(source = "senderPlatformId", target = "senderId"),
      @Mapping(source = "senderDisplayName", target = "senderName")
  })
  ChatMessageExportDto mapToExportDto(ChatMessage chatMessage);
}
