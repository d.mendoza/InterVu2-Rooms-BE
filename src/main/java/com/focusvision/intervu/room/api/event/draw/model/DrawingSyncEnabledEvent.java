package com.focusvision.intervu.room.api.event.draw.model;

import java.util.UUID;

/**
 * Model for drawing sync enabled event.
 */
public record DrawingSyncEnabledEvent(UUID roomId) {

}
