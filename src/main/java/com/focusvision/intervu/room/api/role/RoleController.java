package com.focusvision.intervu.room.api.role;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller for role control related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/participants", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleController {

  private final RoleContorlService roleContorlService;

  /**
   * Gets participants alternative roles data.
   *
   * @param caller Authenticated participant.
   * @return Participants alternative roles data.
   */
  @GetMapping("roles")
  @PreAuthorize("hasAuthority('ROLE_CONTROL')")
  public List<ParticipantAlternativeRolesDto> getAlternativeRoles(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Getting alternative roles (participant: {}).", caller.getId());

    return roleContorlService.getAlternativeRoles(caller.getRoomId());
  }

  /**
   * Sets participant alternative role.
   *
   * @param participantId Participant ID.
   * @param request       Request data.
   * @param caller        Authenticated participant.
   */
  @PostMapping("{participantId}/roles")
  @PreAuthorize("hasAuthority('ROLE_CONTROL')")
  public void setAlternativeRole(
      @PathVariable("participantId") UUID participantId,
      @RequestBody SetAlternativeRoleRequestDto request,
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Setting alternative role (participantId: {}).", caller.getId());

    roleContorlService.setAlternativeRole(caller.getRoomId(), participantId, request.getRole());
  }
}
