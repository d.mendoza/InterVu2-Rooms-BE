package com.focusvision.intervu.room.api.draw.service;

import com.focusvision.intervu.room.api.draw.model.StimulusDrawMessage;
import java.util.UUID;

/**
 * Service for handling drawing actions on stimulus context.
 *
 * @author Branko Ostojic
 */
public interface StimulusDrawingService {

  /**
   * Handle drawing on stimulus action.
   *
   * @param roomId              Room ID.
   * @param stimulusId          Stimulus ID.
   * @param participantId       Room participant performing the draw action.
   * @param stimulusDrawMessage Draw action message.
   */
  void draw(UUID roomId, UUID stimulusId, UUID participantId,
            StimulusDrawMessage stimulusDrawMessage);
}
