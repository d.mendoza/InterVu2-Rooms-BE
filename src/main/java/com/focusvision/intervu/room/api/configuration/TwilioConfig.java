package com.focusvision.intervu.room.api.configuration;

import com.focusvision.intervu.room.api.configuration.domain.TwilioProperties;
import com.twilio.Twilio;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

/**
 * Twilio service configuration.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Configuration
@RequiredArgsConstructor
public class TwilioConfig {

  private final TwilioProperties configuration;

  @PostConstruct
  public void init() {
    Twilio.init(configuration.getAccountSid(), configuration.getApiKey());
  }

}
