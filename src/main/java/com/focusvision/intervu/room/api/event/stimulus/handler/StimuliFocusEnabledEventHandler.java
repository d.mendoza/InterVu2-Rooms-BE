package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimuliFocusEnabledEvent;

/**
 * Stimulus focus enabled event handler.
 */
public interface StimuliFocusEnabledEventHandler {

  void handle(StimuliFocusEnabledEvent event);
}
