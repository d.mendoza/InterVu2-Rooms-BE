package com.focusvision.intervu.room.api.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.messaging.RabbitMqProducer;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionStateData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ResearchSessionStateUpdateSenderAdapter}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqResearchSessionStateUpdateSenderAdapter
    implements ResearchSessionStateUpdateSenderAdapter {

  private final MessagingProperties messagingProperties;
  private final RabbitMqProducer rabbitMqProducer;

  @Override
  public void send(ResearchSessionStateData researchSessionStateData) {
    log.debug("Sending research session state update: {}", researchSessionStateData);

    try {
      rabbitMqProducer.send(messagingProperties.getResearchSessionStateUpdatesQueue(),
          researchSessionStateData);
    } catch (MessagingException e) {
      log.error(
          format("Error sending research session state update: %s.", researchSessionStateData), e);
    }
  }
}
