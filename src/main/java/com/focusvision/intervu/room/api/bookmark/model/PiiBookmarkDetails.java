package com.focusvision.intervu.room.api.bookmark.model;

import java.time.Instant;
import java.util.UUID;

/**
 * Model representing the PII bookmark details.
 */
public record PiiBookmarkDetails(
    UUID id,
    String addedBy,
    Instant addedAt,
    Long offset,
    String addedByDisplayName
) {
}
