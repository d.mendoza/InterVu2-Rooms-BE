package com.focusvision.intervu.room.api.state.stimulus.model;

import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * DTO representing the poll stimulus vote event.
 */
@Getter
@Builder
@ToString
public class PollStimulusVoteEvent {

  private final UUID roomId;
  private final UUID stimulusId;
  private final UUID participantId;
  private final String content;
  private final Long offset;

}
