package com.focusvision.intervu.room.api.twilio.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * Model for Twilio dial-in.
 *
 * @author Branko Ostojic
 */
@Data
@ApiModel("Twilio Dial In Request")
@ToString(onlyExplicitlyIncluded = true)
public class TwilioDialInRequest {

  @ApiModelProperty("Account SID")
  private String accountSid;

  @ApiModelProperty("API version")
  private String apiVersion;

  @ApiModelProperty("Call SID")
  private String callSid;

  @ApiModelProperty("Call status")
  private String callStatus;

  @ApiModelProperty("Called number")
  private String called;

  @ApiModelProperty("Called number city")
  private String calledCity;

  @ApiModelProperty("Called number country")
  private String calledCountry;

  @ApiModelProperty("Called number state")
  private String calledState;

  @ApiModelProperty("Called number ZIP")
  private String calledZip;

  @ApiModelProperty("Caller identification")
  private String caller;

  @ToString.Include
  @ApiModelProperty("Call direction (inbound/outbound)")
  private String direction;

  @ApiModelProperty("Calling number")
  private String from;

  @ApiModelProperty("Called number")
  private String to;

  @ApiModelProperty("Called number city")
  private String toCity;

  @ApiModelProperty("Called number country")
  private String toCountry;

  @ApiModelProperty("Called number state")
  private String toState;

  @ApiModelProperty("Called number ZIP")
  private String toZip;

  // Gather data
  @ToString.Include
  @ApiModelProperty("Entered digits (PIN)")
  private String digits;

  @ToString.Include
  @ApiModelProperty("Gathering message")
  private String msg;

  @ApiModelProperty("Gathering finish key")
  private String finishedOnKey;
}
