package com.focusvision.intervu.room.api.controller;

import static org.springframework.http.HttpStatus.NO_CONTENT;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.dto.RoomRecordingDto;
import com.focusvision.intervu.room.api.security.IntervuUser;
import com.focusvision.intervu.room.api.service.ConferenceService;
import com.focusvision.intervu.room.api.service.RoomRecordingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room recording data related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "recordings", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomRecordingController {

  private final RoomRecordingService roomRecordingService;
  private final ConferenceService conferenceService;

  /**
   * Gets room recording data.
   *
   * @param roomPlatformId Room platform ID.
   * @param caller         Authenticated participant.
   * @return Room recording data.
   */
  @GetMapping("{roomPlatformId}")
  public RoomRecordingDto data(@PathVariable("roomPlatformId") String roomPlatformId,
                               @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Fetching room {} recording details.", roomPlatformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return roomRecordingService.getRecordingData(roomPlatformId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  /**
   * Deletes room recording data.
   *
   * @param roomPlatformId Room platform ID.
   * @param caller         Authenticated participant.
   */
  @ResponseStatus(NO_CONTENT)
  @DeleteMapping("{roomPlatformId}")
  public void deleteRecordings(@PathVariable String roomPlatformId,
                               @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Deleting room {} recordings.", roomPlatformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    roomRecordingService.deleteRecordingData(roomPlatformId);
  }

  /**
   * Regenerates room recording for provided layout and composition.
   *
   * @param roomPlatformId Room platform ID.
   * @param layout         Composition layout.
   * @param caller         Authenticated participant.
   */
  @PutMapping("{roomPlatformId}/regenerate")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void regenerateRecording(@PathVariable String roomPlatformId,
                                  @RequestParam CompositionLayout layout,
                                  @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Regenerating recording for room {}", roomPlatformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    var conference = conferenceService.findByResearchSession(roomPlatformId)
        .orElseThrow(ResourceNotFoundException::new);

    conferenceService.regenerateGroupRoomRecordings(conference, layout);
  }

  /**
   * Regenerates room recordings for composition.
   *
   * @param roomPlatformId Room platform ID.
   * @param caller         Authenticated participant.
   */
  @PutMapping("{roomPlatformId}/regenerate/all")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void regenerateAll(@PathVariable String roomPlatformId,
                            @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Regenerating recordings for room {}", roomPlatformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    conferenceService.regenerateGroupRoomRecordings(roomPlatformId);
  }

}
