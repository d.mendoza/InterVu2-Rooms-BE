package com.focusvision.intervu.room.api.export.model;

import com.focusvision.intervu.room.api.common.model.RecordingType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

/**
 * DTO representing the participant recording brief details.
 */
@Value
@ApiModel("ParticipantRecordingBrief")
public class ParticipantRecordingExportDto {

  @ApiModelProperty("Recording twilio SID")
  String sid;
  @ApiModelProperty("Recording type")
  RecordingType type;
}
