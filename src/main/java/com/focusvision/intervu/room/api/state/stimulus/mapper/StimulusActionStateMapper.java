package com.focusvision.intervu.room.api.state.stimulus.mapper;

import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import org.mapstruct.Mapper;

/**
 * Mapper for {@link StimulusActionState} entity.
 */
@Mapper(componentModel = "spring")
public interface StimulusActionStateMapper {

  /**
   * Maps stimulus state to DTO.
   *
   * @param stimulusActionState Stimulus action state.
   * @return Resulting DTO.
   */
  StimulusStateDto mapToDto(StimulusActionState stimulusActionState);

}
