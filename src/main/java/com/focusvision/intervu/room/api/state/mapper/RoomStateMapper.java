package com.focusvision.intervu.room.api.state.mapper;

import com.focusvision.intervu.room.api.model.dto.ParticipantStateDto;
import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import com.focusvision.intervu.room.api.model.dto.StimulusDto;
import com.focusvision.intervu.room.api.state.model.ParticipantState;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.StimulusState;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link RoomState} entity.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring")
public interface RoomStateMapper {

  /**
   * Maps room state to DTO.
   *
   * @param roomState Room state to be mapped.
   * @return Resulting DTO.
   */
  @Mapping(target = "stimulus", expression = "java(map(extractActiveStimulusState(roomState)))")
  RoomStateDto mapToDto(RoomState roomState);

  /**
   * Maps participant state to DTO.
   *
   * @param participantState Participant state to be mapped.
   * @return Resulting DTO.
   */
  @Mapping(source = "id", target = "participantId")
  @Mapping(source = "platformId", target = "participantPlatformId")
  ParticipantStateDto mapToDto(ParticipantState participantState);

  /**
   * Maps stimuli state to DTO.
   *
   * @param stimulusState Room state to be mapped.
   * @return Resulting DTO.
   */
  StimulusDto map(StimulusState stimulusState);

  default StimulusState extractActiveStimulusState(RoomState roomState) {
    return roomState.getActiveStimulus().orElse(null);
  }

}
