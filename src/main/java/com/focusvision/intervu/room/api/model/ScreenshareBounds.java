package com.focusvision.intervu.room.api.model;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

/**
 * Class that contains screenshare position details.
 */
@Getter
@Builder
public class ScreenshareBounds {

  private ScreenArea screenshareArea;

  private List<ScreenArea> excludedAreas;

}
