package com.focusvision.intervu.room.api.chat.service;

import static java.util.Optional.ofNullable;

import com.focusvision.intervu.room.api.chat.event.model.RespondentsChatDisabledEvent;
import com.focusvision.intervu.room.api.chat.event.model.RespondentsChatEnabledEvent;
import com.focusvision.intervu.room.api.chat.model.ChatControlDto;
import com.focusvision.intervu.room.api.chat.model.GroupChatMetadataDto;
import com.focusvision.intervu.room.api.common.event.EventPublisher;
import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Event based implementation of a {@link ChatControlService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class EventBasedChatControlService implements ChatControlService {

  private final ChatMetadataService chatMetadataService;
  private final EventPublisher eventPublisher;

  @Override
  public void enableMeetingRoomRespondentsChat(AuthenticatedParticipant participant) {
    log.debug("Enabling meeting room respondents chat by {}.", participant.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(participant);
    ofNullable(metadata.getRespondents())
        .map(GroupChatMetadataDto::getControl)
        .map(ChatControlDto::getEnable)
        .orElseThrow(this::chatControlForbiddenException);

    eventPublisher
        .publish(new RespondentsChatEnabledEvent(participant.getRoomId()));
  }

  @Override
  public void disableMeetingRoomRespondentsChat(AuthenticatedParticipant participant) {
    log.debug("Disabling meeting room respondents chat by {}.", participant.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(participant);
    ofNullable(metadata.getRespondents())
        .map(GroupChatMetadataDto::getControl)
        .map(ChatControlDto::getDisable)
        .orElseThrow(this::chatControlForbiddenException);

    eventPublisher
        .publish(new RespondentsChatDisabledEvent(participant.getRoomId()));
  }

  private IntervuRoomException chatControlForbiddenException() {
    return new ActionForbiddenException("Chat control forbidden.");
  }
}
