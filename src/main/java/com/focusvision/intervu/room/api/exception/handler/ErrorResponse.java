package com.focusvision.intervu.room.api.exception.handler;

import com.focusvision.intervu.room.api.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Model for exception response.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Getter
@AllArgsConstructor
public class ErrorResponse {
  protected ErrorCode code;
  protected String message;

  public ErrorResponse() {
    this.code = ErrorCode.UNDEFINED;
  }
}
