package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.model.entity.Bookmark;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link Bookmark} entity.
 *
 * @author Branko Ostojic
 */
public interface BookmarkRepository extends JpaRepository<Bookmark, UUID> {

  /**
   * Finds all bookmarks belonging to the specified room.
   *
   * @param researchSession Research session.
   * @return Matching bookmarks
   */
  List<Bookmark> findAllByResearchSession(ResearchSession researchSession);
}
