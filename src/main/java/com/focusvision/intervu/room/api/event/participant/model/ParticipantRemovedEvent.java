package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * Model for participants removed event.
 */
public record ParticipantRemovedEvent(UUID roomId,
                                      UUID participantId,
                                      String platformId) {

}
