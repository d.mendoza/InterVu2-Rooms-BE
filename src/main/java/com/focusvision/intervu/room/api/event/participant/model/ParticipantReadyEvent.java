package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * Model for participant ready event.
 *
 * @author Branko Ostojic
 */
public record ParticipantReadyEvent(UUID roomId, UUID participantId) {

}
