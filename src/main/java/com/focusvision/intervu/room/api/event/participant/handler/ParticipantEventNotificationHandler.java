package com.focusvision.intervu.room.api.event.participant.handler;

import com.focusvision.intervu.room.api.adapter.BannedParticipantNotificationSenderAdapter;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantBannedEvent;
import com.focusvision.intervu.room.api.model.messaging.BannedParticipantData;
import com.focusvision.intervu.room.api.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Participant event notification handler.
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class ParticipantEventNotificationHandler implements ParticipantBannedEventHandler {

  private final ParticipantService participantService;
  private final BannedParticipantNotificationSenderAdapter
      bannedParticipantNotificationSenderAdapter;

  @Override
  @EventListener
  public void handle(ParticipantBannedEvent event) {
    log.info("Handling participant banned event notification: {}.", event);

    participantService.findById(event.participantId())
        .map(participant -> new BannedParticipantData()
            .setPlatformId(participant.getPlatformId())
            .setRole(participant.getRole().name())
            .setSessionPlatformId(participant.getResearchSession().getPlatformId()))
        .ifPresent(bannedParticipantNotificationSenderAdapter::send);
  }

}
