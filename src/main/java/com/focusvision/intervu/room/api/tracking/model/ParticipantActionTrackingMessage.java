package com.focusvision.intervu.room.api.tracking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model representing participant action tracking message.
 *
 * @author Branko Ostojic
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParticipantActionTrackingMessage {

  /**
   * Tracking action type.
   */
  private ParticipantActionType type;
}
