package com.focusvision.intervu.room.api.state.model;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_FINISHED;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

/**
 * Entity representing room state.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "room_state_logs")
@Getter
@Setter
@Accessors(chain = true)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class RoomStateLog {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  private UUID roomId;

  @Column(name = "start_offset")
  private Long offset;

  @Enumerated(EnumType.STRING)
  private RoomStateChangeType changeType;

  private Long version;

  private Long previousVersion;

  @Type(type = "jsonb")
  @Column(columnDefinition = "jsonb")
  private RoomState roomState;

  private Instant addedAt;

  public boolean isRoomFinished() {
    return ROOM_FINISHED.equals(changeType);
  }
}
