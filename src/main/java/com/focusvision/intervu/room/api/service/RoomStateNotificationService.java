package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.state.model.ParticipantState;
import com.focusvision.intervu.room.api.state.model.RoomState;
import java.util.List;
import java.util.UUID;

/**
 * Service used for room state notification related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface RoomStateNotificationService {

  /**
   * Sends fresh room state data to all interested parties.
   *
   * @param roomState           Room state.
   * @param roomStateChangeType Room state change type.
   */
  void sendRoomStateChangeNotification(RoomState roomState,
                                       RoomStateChangeType roomStateChangeType);

  /**
   * Sends fresh participant state to co-responding participant.
   *
   * @param participantState Participant state.
   */
  void sendParticipantStateNotification(ParticipantState participantState);

  /**
   * Sends microphone toggle notification to provided participant.
   *
   * @param participantId Room participant ID.
   */
  void sendParticipantMicrophoneToggleNotification(UUID participantId);

  /**
   * Sends new room data to all interested parties.
   *
   * @param roomState         Room state.
   * @param participantStates Room participants states.
   */
  void sendNewRoomNotification(RoomState roomState, List<ParticipantState> participantStates);

  /**
   * Sends participant removed notification to removed participant.
   *
   * @param roomId                Room ID.
   * @param participantId         Removed participant ID.
   * @param participantPlatformId Participant platform ID.
   */
  void sendParticipantRemovedNotification(UUID roomId, UUID participantId,
                                          String participantPlatformId);

}
