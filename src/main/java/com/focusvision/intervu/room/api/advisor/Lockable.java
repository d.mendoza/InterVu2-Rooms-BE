package com.focusvision.intervu.room.api.advisor;

/**
 * Marks an implementing class as a provider of a cluster lock key.
 *
 * @author Branko Ostojic
 */
public interface Lockable {

  /**
   * Should return a group level unique key for purpose of locking on a cluster level.
   *
   * @return Key to be used on cluster level locking.
   */
  String lockKey();

  /**
   * Should return a group of resource for purpose of locking on a cluster level.
   *
   * @return Group to be used on cluster level locking.
   */
  Group lockGroup();

  /**
   * Lock group.
   */
  enum Group {
    RESEARCH_SESSION,
    CONFERENCE,
    RECORDING
  }
}
