package com.focusvision.intervu.room.api.model.streaming;

import com.focusvision.intervu.room.api.model.ScreenshareBounds;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Model for group room recording.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@Accessors(chain = true)
public class GroupRoomRecording {

  /**
   * Group room recording SID.
   */
  private String sid;
  /**
   * Group room recording duration.
   */
  private Integer duration;
  /**
   * Group room recording processed flag.
   */
  private boolean processed = false;
  /**
   * Group room recording error flag.
   */
  private boolean error = false;
  /**
   * Group room recording screenshare position.
   */
  private ScreenshareBounds screenshareBounds;

}
