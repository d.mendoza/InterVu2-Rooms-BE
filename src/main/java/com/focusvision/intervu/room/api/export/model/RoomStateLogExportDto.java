package com.focusvision.intervu.room.api.export.model;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import lombok.Value;

/**
 * DTO representing the room state log for export.
 *
 * @author Branko Ostojic
 */
@Value
@ApiModel("Room State Log Export")
public class RoomStateLogExportDto {

  @ApiModelProperty("Event offset from session start")
  Long offset;

  @ApiModelProperty("State change type")
  RoomStateChangeType changeType;

  @ApiModelProperty("State version")
  Long version;

  @ApiModelProperty("Room state data")
  RoomStateExportDto roomState;

  @ApiModelProperty("Creation time")
  Instant addedAt;

}
