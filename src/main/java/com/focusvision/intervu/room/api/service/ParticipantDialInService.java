package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.dto.DialInInfoDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantDialInInfoDto;
import java.util.List;
import java.util.UUID;

/**
 * Service used for providing participant dial-in information.
 */
public interface ParticipantDialInService {

  /**
   * Provides dail-in information for given participant.
   *
   * @param id Participant ID.
   * @return Dial-in information.
   */
  DialInInfoDto getDialInInfo(UUID id);

  /**
   * Provides dial-in information for all participants in given room.
   *
   * @param roomId Room ID.
   * @return Dial-in information for participants.
   */
  List<ParticipantDialInInfoDto> getDialInfoForParticipants(UUID roomId);
}
