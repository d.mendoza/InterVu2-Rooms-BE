package com.focusvision.intervu.room.api.event.room.model;

import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.Project;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Model for room updated event.
 *
 * @author Branko Ostojic
 */
public record RoomUpdatedEvent(UUID roomId,
                               Project project,
                               ResearchSession researchSession,
                               Conference conference,
                               Set<Participant> participants,
                               List<Stimulus> stimuli) {

}
