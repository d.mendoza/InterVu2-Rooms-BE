package com.focusvision.intervu.room.api.export.model;

import com.focusvision.intervu.room.api.common.model.DrawingActionType;
import com.focusvision.intervu.room.api.common.model.DrawingContextType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Value;

/**
 * DTO for room drawing log export.
 *
 * @author Branko Ostojic
 */
@Value
@ApiModel("Room Drawing Log Export")
public class RoomDrawingLogExportDto {

  @ApiModelProperty("Participant's ID")
  UUID participantId;

  @ApiModelProperty("Event offset from session start")
  Long offset;

  @ApiModelProperty("Drawing action type")
  DrawingActionType type;

  @ApiModelProperty("Drawing context type")
  DrawingContextType contextType;

  @ApiModelProperty("Context ID")
  UUID contextId;

  @ApiModelProperty("Stimulus platform ID")
  String stimulusPlatformId;

  @ApiModelProperty("Action content")
  String content;
}
