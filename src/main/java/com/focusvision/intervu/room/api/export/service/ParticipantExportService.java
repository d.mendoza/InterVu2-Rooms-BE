package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.model.ParticipantDataExportDto;
import java.util.List;

/**
 * Service class used for exporting participant personal data.
 */
public interface ParticipantExportService {

  /**
   * Exports recordings and chat messages per session by provided participant platform ID.
   *
   * @param platformId Participant's platform ID.
   * @return List of matching participant personal data.
   */
  List<ParticipantDataExportDto> export(String platformId);
}
