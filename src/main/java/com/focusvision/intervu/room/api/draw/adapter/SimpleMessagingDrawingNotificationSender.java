package com.focusvision.intervu.room.api.draw.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.draw.model.DrawingNotificationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * {@link SimpMessagingTemplate} specific implementation of a {@link
 * DrawingNotificationSenderAdapter}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SimpleMessagingDrawingNotificationSender implements DrawingNotificationSenderAdapter {

  private final SimpMessagingTemplate messagingTemplate;

  @Override
  public void send(String destination, DrawingNotificationMessage message) {
    log.debug("Sending drawing action event [{}]", message);

    try {
      messagingTemplate.convertAndSend(destination, message);
    } catch (MessagingException e) {
      log.error(format("Error sending drawing action event %s", message), e);
    }
  }
}
