package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.room.model.RoomCanceledEvent;

/**
 * Room canceled event publisher.
 */
public interface RoomCanceledEventPublisher {

  void publish(RoomCanceledEvent event);
}
