package com.focusvision.intervu.room.api.cache.room.log.service;

import java.util.UUID;

/**
 * Contract for service providing authentication cache eviction functionality. Implementor may or
 * may not implement caching as well, but it will be responsible for eviction.
 *
 * @author Branko Ostojic
 */
public interface RoomLogCachingService {

  /**
   * Clears room log cache.
   *
   * @param roomId Room ID.
   */
  void clearRoomLogCache(UUID roomId);
}
