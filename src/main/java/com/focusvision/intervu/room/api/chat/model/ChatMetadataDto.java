package com.focusvision.intervu.room.api.chat.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * DTO representing the chat metadata details. It contains all metadata required for construction
 * chat UI.
 *
 * @author Branko Ostojic
 */
@Getter
@Builder
@ToString
@ApiModel("Chat metadata")
public class ChatMetadataDto {

  @ApiModelProperty("Internal group chat metadata")
  private final GroupChatMetadataDto internal;
  @ApiModelProperty("Respondents group chat metadata")
  private final GroupChatMetadataDto respondents;
  @ApiModelProperty("Direct (private) chats metadata")
  private final List<ParticipantChatMetadataDto> direct;
}
