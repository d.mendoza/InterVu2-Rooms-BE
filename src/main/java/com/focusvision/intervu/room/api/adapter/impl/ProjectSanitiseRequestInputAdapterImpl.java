package com.focusvision.intervu.room.api.adapter.impl;

import static java.lang.String.valueOf;

import com.focusvision.intervu.room.api.adapter.ProjectSanitiserRequestInputAdapter;
import com.focusvision.intervu.room.api.advisor.ClusterLock;
import com.focusvision.intervu.room.api.model.messaging.ProjectSanitiseRequest;
import com.focusvision.intervu.room.api.service.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ProjectSanitiserRequestInputAdapter}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectSanitiseRequestInputAdapterImpl implements ProjectSanitiserRequestInputAdapter {

  private final ProjectService projectService;

  @Override
  @ClusterLock
  public void process(ProjectSanitiseRequest projectSanitiseRequest) {
    log.debug("Processing project sanitise request: [{}]", projectSanitiseRequest);

    projectService.sanitiseProject(valueOf(projectSanitiseRequest.getPlatformId()));
  }
}
