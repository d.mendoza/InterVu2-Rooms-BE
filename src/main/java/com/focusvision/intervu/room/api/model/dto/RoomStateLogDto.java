package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import java.time.Instant;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the room state log.
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class RoomStateLogDto {

  private Long offset;
  private RoomStateChangeType changeType;
  private Long version;
  private RoomStateDto roomState;
  private Instant addedAt;

}
