package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the stimuli thumbnails in a room.
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@ApiModel("Stimuli thumbnails")
public class StimuliThumbnailsDto {

  @ApiModelProperty("Stimuli thumbnails")
  private Map<UUID, String> thumbnails;
}
