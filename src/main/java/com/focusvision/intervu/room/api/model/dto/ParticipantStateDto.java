package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.security.ParticipantPermission;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the participant state in room.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@ApiModel("ParticipantState")
public class ParticipantStateDto {

  @ApiModelProperty("Participant's ID")
  private UUID participantId;
  @ApiModelProperty("Participant's platform ID")
  private String participantPlatformId;
  @ApiModelProperty("Participant's display name")
  private String displayName;
  @ApiModelProperty("Participant's role in the room")
  private ParticipantRole role;
  @ApiModelProperty("Indicates whether participant is ready for joining the room")
  private boolean ready;
  @ApiModelProperty("Indicates whether participant is banned from room")
  private boolean banned;
  @ApiModelProperty("Indicates whether participant admin or not")
  private boolean admin;
  @ApiModelProperty("Indicates whether participant can moderate")
  private boolean moderator;
  @ApiModelProperty("Indicates whether participant can be banned by non-admins")
  private boolean banable;
  @ApiModelProperty("Indicates whether participant is guest")
  private boolean guest;
  @ApiModelProperty("Indicates whether participant is project manager")
  private boolean projectManager;
  @ApiModelProperty("Indicates whether participant is project operator")
  private boolean projectOperator;
  @ApiModelProperty("Indicates whether participant can draw in room")
  private boolean drawingEnabled;

  @ApiModelProperty("Room conference details")
  private ConferenceDto conference;

  @ApiModelProperty("Communication channels details")
  private ChannelDto channel;

  @ApiModelProperty("Participant action permissions")
  private Set<ParticipantPermission> permissions = new HashSet<>();

}
