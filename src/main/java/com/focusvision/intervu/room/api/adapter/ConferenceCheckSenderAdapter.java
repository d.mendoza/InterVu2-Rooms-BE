package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;

/**
 * Adapter for sending conference state check event.
 */
public interface ConferenceCheckSenderAdapter {

  /**
   * Sends event for checking running conference state.
   *
   * @param conference Running conference to check.
   */
  void send(RunningConferenceCheck conference);
}
