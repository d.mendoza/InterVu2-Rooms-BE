package com.focusvision.intervu.room.api.chat.service;

import java.util.UUID;

/**
 * Service used for communication channel related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatCommunicationChannelService {

  /**
   * Returns a chat channel data for a specified participant.
   *
   * @param participantId Participant ID.
   * @return Chat channel data.
   */
  String getParticipantChatChannel(UUID participantId);

  /**
   * Gets the chat channel for specified handle.
   *
   * @param handle Handle or destination.
   * @return Chat channel.
   */
  String getChatChannel(UUID handle);

}
