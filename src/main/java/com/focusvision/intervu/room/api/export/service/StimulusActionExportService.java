package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.model.StimulusActionLogExportDto;
import java.util.List;
import java.util.Optional;

/**
 * Service for exporting stimulus action log data.
 */
public interface StimulusActionExportService {

  /**
   * Gets the stimulus export data for provided room ID.
   *
   * @param platformId Room platform ID.
   * @return List of stimulus action logs or empty {@link Optional} if room can't be found.
   */
  Optional<List<StimulusActionLogExportDto>> export(String platformId);
}
