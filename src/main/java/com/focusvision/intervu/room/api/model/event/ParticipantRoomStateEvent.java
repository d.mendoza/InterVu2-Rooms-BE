package com.focusvision.intervu.room.api.model.event;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * DTO representing the participant room state event.
 *
 * @author Branko Ostojic
 */
@Getter
@Builder
@ToString
@ApiModel("ParticipantRoomStateEvent")
public class ParticipantRoomStateEvent {

  @ApiModelProperty("Event Type")
  private final RoomStateChangeType eventType;
  /**
   * Event destination.
   */
  @ApiModelProperty("Event destination")
  private final String destination;
  /**
   * Event data payload.
   */
  @ApiModelProperty("Event data")
  private final ParticipantRoomStateDto data;
}
