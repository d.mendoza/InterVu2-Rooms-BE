package com.focusvision.intervu.room.api.draw.service;

import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawClearEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawRedoEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawUndoEvent;

/**
 * Service for preparing and sending the drawing events notifications.
 *
 * @author Branko Ostojic
 */
public interface DrawingNotificationService {

  /**
   * Sends the drawing event notification.
   *
   * @param event Drawing event data.
   */
  void sendDrawNotification(StimulusDrawEvent event);

  /**
   * Sends the drawing undo event notification.
   *
   * @param event Drawing undo event data.
   */
  void sendDrawNotification(StimulusDrawUndoEvent event);

  /**
   * Sends the drawing redo event notification.
   *
   * @param event Drawing redo event data.
   */
  void sendDrawNotification(StimulusDrawRedoEvent event);

  /**
   * Sends the drawing clear event notification.
   *
   * @param event Drawing clear event data.
   */
  void sendDrawNotification(StimulusDrawClearEvent event);
}
