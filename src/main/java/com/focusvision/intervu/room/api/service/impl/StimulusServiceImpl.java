package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.StimulusType.DOCUMENT_SHARING;
import static java.util.Objects.isNull;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;

import com.focusvision.intervu.room.api.common.event.EventPublisher;
import com.focusvision.intervu.room.api.event.stimulus.model.StimuliFocusDisabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimuliFocusEnabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusActivatedEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusBroadcastResultsDisableEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusBroadcastResultsEnableEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusDeactivatedEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusInteractionDisabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusInteractionEnabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusResetEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusSyncDisabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusSyncEnabledEvent;
import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import com.focusvision.intervu.room.api.model.dto.StimulusDto;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.repository.StimulusRepository;
import com.focusvision.intervu.room.api.service.RoomStateService;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusStateService;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link StimulusService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusServiceImpl implements StimulusService {

  private final StimulusRepository stimulusRepository;
  private final RoomStateService roomStateService;
  private final StimulusStateService stimulusStateService;
  private final EventPublisher eventPublisher;

  @Override
  public List<Stimulus> getAll(UUID roomId) {
    log.info("Getting all room {} stimuli.", roomId);

    return stimulusRepository.findAllByResearchSessionIdOrderByPosition(roomId);
  }

  @Override
  public Optional<Stimulus> find(UUID id, UUID roomId) {
    log.info("Finding stimulus {} in room {}.", id, roomId);

    return stimulusRepository.findByIdAndResearchSessionId(id, roomId);
  }

  @Override
  public Optional<Stimulus> findStimulusWithBroadcastResults(UUID id) {
    log.info("Finding stimulus {} with broadcast results enabled.", id);

    var stimulus = stimulusRepository.findById(id);
    if (stimulus.isPresent()) {
      var state = roomStateService.getRoomState(stimulus.get().getResearchSession().getId());
      var stimulusState = state.getStimulus();
      if (isNull(stimulusState)
          || (!stimulusState.isBroadcastResults() && stimulusState.getId().equals(id))) {
        return empty();
      }
    }
    return stimulus;
  }

  @Override
  public void activate(UUID id, UUID roomId) {
    log.info("Activating stimulus {} in room {}.", id, roomId);

    stimulusRepository
        .findByIdAndResearchSessionId(id, roomId)
        .map(stimulus -> new StimulusActivatedEvent(roomId, stimulus))
        .ifPresent(eventPublisher::publish);
  }

  @Override
  public void deactivate(UUID id, UUID roomId) {
    log.info("Deactivating stimulus {} in room {}.", id, roomId);

    stimulusRepository
        .findByIdAndResearchSessionId(id, roomId)
        .map(s -> new StimulusDeactivatedEvent(roomId))
        .ifPresent(eventPublisher::publish);
  }

  @Override
  public void enableStimulusInteraction(UUID roomId) {
    log.info("Enabling current stimulus interaction in room {}.", roomId);

    ofNullable(roomStateService.getRoomState(roomId))
        .map(RoomStateDto::getStimulus)
        .filter(not(StimulusDto::isBroadcastResults))
        .map(s -> new StimulusInteractionEnabledEvent(roomId))
        .ifPresent(eventPublisher::publish);
  }

  @Override
  public void disableStimulusInteraction(UUID roomId) {
    log.info("Disabling current stimulus interaction  in room {}.", roomId);

    ofNullable(roomStateService.getRoomState(roomId))
        .map(RoomStateDto::getStimulus)
        .map(s -> new StimulusInteractionDisabledEvent(roomId))
        .ifPresent(eventPublisher::publish);
  }

  @Override
  public boolean isStimulusActive(UUID roomId) {
    log.debug("Checking if stimulus {} is active in room.", roomId);

    return roomStateService.getRoomState(roomId).getStimulus() != null;
  }

  @Override
  public void enableFocus(UUID roomId) {
    log.info("Enabling stimuli focus in room {}", roomId);

    eventPublisher.publish(new StimuliFocusEnabledEvent(roomId));
  }

  @Override
  public void disableFocus(UUID roomId) {
    log.info("Disabling stimuli focus in room {}", roomId);

    eventPublisher.publish(new StimuliFocusDisabledEvent(roomId));
  }

  @Override
  public void enableBroadcastResults(UUID roomId, UUID stimulusId) {
    log.debug("enabling broadcast polling stimulus {} results for room {}.", stimulusId, roomId);

    ofNullable(roomStateService.getRoomState(roomId))
        .map(RoomStateDto::getStimulus)
        .filter(stimulusDto -> stimulusDto.getId().equals(stimulusId))
        .filter(not(StimulusDto::getInteractionEnabled))
        .map(StimulusDto::getId)
        .map(stimulusStateService::getStimulusState)
        .filter(not(List::isEmpty))
        .map(s -> new StimulusBroadcastResultsEnableEvent(roomId, stimulusId))
        .ifPresent(eventPublisher::publish);
  }

  @Override
  public void disableBroadcastResults(UUID roomId, UUID stimulusId) {
    log.debug("Disabling broadcast polling stimulus {} results for room {}.", stimulusId, roomId);

    eventPublisher.publish(new StimulusBroadcastResultsDisableEvent(roomId, stimulusId));
  }

  @Override
  public boolean isStimulusActiveInRoom(UUID stimulusId, UUID roomId) {
    var stimulus = roomStateService.getRoomState(roomId).getStimulus();

    return Objects.nonNull(stimulus) && stimulusId.equals(stimulus.getId());
  }

  @Override
  public void resetStimulus(UUID roomId) {
    log.debug("Resetting room {} stimulus.", roomId);

    eventPublisher.publish(new StimulusResetEvent(roomId));
  }

  @Override
  public void enableStimulusSync(UUID roomId, UUID stimulusId) {
    log.debug("Enabling stimulus {} sync in room {}", stimulusId, roomId);

    eventPublisher.publish(new StimulusSyncEnabledEvent(roomId, stimulusId));
  }

  @Override
  public void disableStimulusSync(UUID roomId, UUID stimulusId) {
    log.debug("Disabling stimulus {} sync in room {}", stimulusId, roomId);

    eventPublisher.publish(new StimulusSyncDisabledEvent(roomId, stimulusId));
  }

  @Override
  public Optional<Stimulus> findDocumentSharingStimulus(UUID stimulusId, UUID roomId) {
    log.debug("Finding document sharing stimulus {} in room {}", stimulusId, roomId);

    return stimulusRepository.findByIdAndResearchSessionIdAndType(
        stimulusId, roomId, DOCUMENT_SHARING);
  }
}
