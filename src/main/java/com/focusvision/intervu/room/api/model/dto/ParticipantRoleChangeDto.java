package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the participant role change event.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("ParticipantRoleChange")
public class ParticipantRoleChangeDto {

  /**
   * New role.
   */
  private ParticipantRole role;
}
