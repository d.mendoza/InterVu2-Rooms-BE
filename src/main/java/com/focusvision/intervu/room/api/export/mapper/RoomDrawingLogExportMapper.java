package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.draw.model.DrawingLog;
import com.focusvision.intervu.room.api.export.model.RoomDrawingLogExportDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link DrawingLog} entity.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface RoomDrawingLogExportMapper {

  /**
   * Maps drawing log to export DTO.
   *
   * @param drawingLog         Drawing log to be mapped.
   * @param offset             Log offset.
   * @param stimulusPlatformId Stimulus platform ID.
   * @return Resulting DTO.
   */
  @Mapping(source = "drawingLog.actionType", target = "type")
  @Mapping(source = "offset", target = "offset")
  @Mapping(source = "stimulusPlatformId", target = "stimulusPlatformId")
  RoomDrawingLogExportDto mapToDto(DrawingLog drawingLog, Long offset, String stimulusPlatformId);

}
