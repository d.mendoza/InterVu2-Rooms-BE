package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus sync enabled event.
 */
public record StimulusSyncEnabledEvent(UUID roomId, UUID stimulusId) {

}
