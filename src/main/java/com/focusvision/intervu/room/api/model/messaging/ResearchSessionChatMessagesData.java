package com.focusvision.intervu.room.api.model.messaging;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * DTO representing research session's chat messages.
 */
@Getter
@Setter
@Accessors(chain = true)
public class ResearchSessionChatMessagesData {

  /**
   * Chat messages in research session.
   */
  List<ChatMessageData> messages;
  /**
   * Research session platform ID.
   */
  private String id;
}
