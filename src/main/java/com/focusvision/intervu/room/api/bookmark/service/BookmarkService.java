package com.focusvision.intervu.room.api.bookmark.service;

import com.focusvision.intervu.room.api.model.entity.Bookmark;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.List;
import java.util.UUID;

/**
 * Service used for bookmark related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface BookmarkService {

  /**
   * Adds a bookmark for specified room.
   *
   * @param caller    Participant adding the bookmark.
   * @param note      Bookmark note.
   * @param timestamp Bookmark timestamp.
   * @return Created bookmark.
   */
  Bookmark addBookmark(AuthenticatedParticipant caller,
                       String note,
                       Long timestamp);

  /**
   * Gets all research session bookmarks.
   *
   * @param roomId Room ID.
   * @return Collection of research session bookmarks.
   */
  List<Bookmark> getAllBookmarks(UUID roomId);

  /**
   * Gets all research session bookmarks with start offset aligned to be in sync with recording.
   *
   * @param researchSession Research session.
   * @param recordingOffset Recording offset.
   * @return Collection of research session bookmarks.
   */
  List<Bookmark> getAllBookmarksWithAlignedOffset(ResearchSession researchSession,
                                                  Integer recordingOffset);

  /**
   * Adds PII bookmark for specified room.
   *
   * @param caller    Participant adding the bookmark.
   * @param note      Bookmark note.
   * @param timestamp Bookmark timestamp.
   * @return Created bookmark.
   */
  Bookmark addPiiBookmark(AuthenticatedParticipant caller,
                          String note,
                          Long timestamp);

}
