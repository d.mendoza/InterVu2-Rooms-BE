package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusInteractionDisabledEvent;

/**
 * Handler for the {@link StimulusInteractionDisabledEvent}.
 */
public interface StimulusInteractionDisabledEventHandler {

  /**
   * Handles the provided {@link StimulusInteractionDisabledEvent}.
   *
   * @param event Stimulus interaction enabled event.
   */
  void handle(StimulusInteractionDisabledEvent event);
}
