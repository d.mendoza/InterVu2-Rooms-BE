package com.focusvision.intervu.room.api.stimulus.api;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;

import com.focusvision.intervu.room.api.draw.model.StimulusDrawMessage;
import com.focusvision.intervu.room.api.draw.service.StimulusDrawingService;
import com.focusvision.intervu.room.api.scroll.model.StimulusScrollMessage;
import com.focusvision.intervu.room.api.scroll.service.StimulusScrollService;
import java.security.Principal;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

/**
 * Controller responsible for meeting room stimulus messaging operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class MeetingRoomStimulusMessagingController {

  private final StimulusDrawingService stimulusDrawingService;
  private final StimulusScrollService stimulusScrollService;

  /**
   * Handle drawing on stimulus.
   *
   * @param stimulusId Stimulus ID.
   * @param message    Drawing message.
   * @param principal  Authenticated participant.
   */
  @MessageMapping("meeting-room/stimulus/{stimulusId}/draw")
  public void draw(@DestinationVariable UUID stimulusId,
                   @Payload @Validated StimulusDrawMessage message,
                   Principal principal) {
    log.debug("Receiving room stimulus draw event [{}], from [{}].", message, principal.getName());

    var caller = extractParticipant(principal).orElseThrow();

    stimulusDrawingService.draw(caller.getRoomId(), stimulusId, caller.getId(), message);
  }

  /**
   * Handle scroling on stimulus.
   *
   * @param stimulusId Stimulus ID.
   * @param message    Scroll message.
   * @param principal  Authenticated participant.
   */
  @MessageMapping("meeting-room/stimulus/{stimulusId}/scroll")
  public void scroll(@DestinationVariable UUID stimulusId,
                     @Payload @Validated StimulusScrollMessage message,
                     Principal principal) {
    log.debug("Receiving room stimulus scroll event [{}], from [{}].", message,
        principal.getName());

    var caller = extractParticipant(principal).orElseThrow();

    stimulusScrollService.scroll(caller.getRoomId(), stimulusId, caller.getId(), message);
  }
}
