package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * DTO representing the participant connected event.
 *
 * @author Branko Ostojic
 */
public record ParticipantConnectedEvent(UUID roomId,
                                        UUID participantId) {
}
