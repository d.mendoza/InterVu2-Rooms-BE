package com.focusvision.intervu.room.api.security;

import static java.util.Optional.ofNullable;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Custom room token authorization filter.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@RequiredArgsConstructor
public class IntervuUserAuthenticationFilter extends OncePerRequestFilter {

  private final TokenProvider tokenProvider;

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {
    return !(request.getRequestURI().startsWith("/dashboard")
        || request.getRequestURI().startsWith("/waiting-room/session")
        || request.getRequestURI().startsWith("/recordings")
        || request.getRequestURI().startsWith("/ws/")
        || request.getRequestURI().startsWith("/export/"));
  }

  @Override
  public void doFilterInternal(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain chain
  ) throws IOException, ServletException {

    getJwtFromRequest(request)
        .ifPresent(jwt ->
            tokenProvider.getIntervuUserFromToken(jwt)
                .ifPresent(intervuUser -> {
                  var authentication = new IntervuUserTokenBasedAuthentication(intervuUser, jwt);
                  SecurityContextHolder.getContext().setAuthentication(authentication);
                  this.setMdcContext(intervuUser);
                })
        );

    chain.doFilter(request, response);
  }

  private Optional<String> getJwtFromRequest(HttpServletRequest request) {
    return ofNullable(request.getHeader("Authorization"))
        .filter(StringUtils::hasText)
        .filter(token -> token.startsWith("Bearer "))
        .map(token -> token.substring(7))
        .or(() -> ofNullable(request.getParameter("access_token")));
  }

  private void setMdcContext(IntervuUser intervuUser) {
    MDC.put("user", "user:INTERVU");
    MDC.put("participantPlatformId", "participantPlatformId:" + intervuUser.getPlatformId());
    MDC.put("participantUsername", "participantUsername:" + intervuUser.getUsername());
  }
}
