package com.focusvision.intervu.room.api.chat.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the participant private (direct) chat metadata.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Participant chat metadata")
public class ParticipantChatMetadataDto {

  @ApiModelProperty("Handle for chat identification")
  private UUID handle;
  @ApiModelProperty("Type for chat identification")
  private ChatType type;
  @ApiModelProperty("Readonly chat permission indicator")
  private boolean readOnly;
  @ApiModelProperty("Destination for sending chat messages")
  private String destination;
  @ApiModelProperty("Destination for sending seen chat message ID")
  private String destinationForSeen;
  @ApiModelProperty("URL for fetching chat messages")
  private String messagesUrl;
  @ApiModelProperty("Participant data for chat")
  private ParticipantDto participant;
  @ApiModelProperty("Chat enabled/disabled indicator")
  private boolean enabled;
  @ApiModelProperty("Chat control")
  private ChatControlDto control;

}
