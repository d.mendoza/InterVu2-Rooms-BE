package com.focusvision.intervu.room.api.security;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import java.util.Set;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Model representing authenticated participant details.
 *
 * @author Branko Ostojic
 */
@Getter
@Builder
public class AuthenticatedParticipant implements UserDetails {

  private final UUID id;
  private final String platformId;
  private final String displayName;
  private final UUID roomId;
  private final String roomPlatformId;
  private final String roomName;
  private final ParticipantRole role;
  private final boolean banned;
  private final boolean canModerate;
  private final boolean admin;
  private final boolean banable;
  private final boolean guest;
  private final boolean projectManager;
  private final boolean projectOperator;

  private final Set<SimpleGrantedAuthority> authorities;

  public boolean canModerate() {
    return MODERATOR.equals(role) || canModerate;
  }

  public boolean isRespondent() {
    return RESPONDENT.equals(role);
  }

  public boolean isModeratorRole() {
    return MODERATOR.equals(role);
  }

  public boolean isObserver() {
    return OBSERVER.equals(role);
  }

  public boolean isTranslator() {
    return TRANSLATOR.equals(role);
  }

  public boolean isInternal() {
    return !isRespondent();
  }

  @Override
  public String getPassword() {
    return null;
  }

  @Override
  public String getUsername() {
    return id.toString();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  public boolean isAuthenticated() {
    return !this.banned;
  }
}
