package com.focusvision.intervu.room.api.recording.event;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import java.time.Instant;
import java.util.UUID;

/**
 * Model for room recording processing finished event.
 *
 * @author Branko Ostojic
 */
public record RecordingProcessingFinishedEvent(UUID roomId,
                                               String roomPlatformId,
                                               String conferenceId,
                                               String recordingId,
                                               Instant startedAt,
                                               Instant finishedAt,
                                               CompositionLayout compositionLayout,
                                               AudioChannel audioChannel) {

}
