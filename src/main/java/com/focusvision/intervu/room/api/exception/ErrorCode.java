package com.focusvision.intervu.room.api.exception;

/**
 * Error codes for various errors.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public enum ErrorCode {
  UNDEFINED,
  ACCESS_DENIED,
  FORBIDDEN,
  NOT_FOUND,
  BAD_REQUEST,
  INVALID_REQUEST_DATA,
  STREAMING_API_ERROR,
  ROOM_FINISHED,
  ROOM_CANCELED,
  ROOM_EXPIRED
}
