package com.focusvision.intervu.room.api.model.streaming;

import com.focusvision.intervu.room.api.model.ScreenshareBounds;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * POJO representing the recording layout data.
 */
@Getter
@Builder
public class RecordingLayoutData {

  private Map<String, Object> layout;
  private ScreenshareBounds screenshareBounds;

}
