package com.focusvision.intervu.room.api.cache.authentication.publisher;

import com.focusvision.intervu.room.api.cache.authentication.event.ClearAuthenticationCacheEvent;

/**
 * Publisher contract for clearing authentication cache events.
 *
 * @author Branko Ostojic
 */
public interface AuthenticationCacheEvictionEventPublisher {

  /**
   * Publish the provided clear authentication cache event.
   *
   * @param event Cache eviction event.
   */
  void publish(ClearAuthenticationCacheEvent event);
}
