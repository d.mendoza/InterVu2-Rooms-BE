package com.focusvision.intervu.room.api.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;

/**
 * Interceptor for inbound WebSocket messages.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RequiredArgsConstructor
public class WebSocketInboundChannelInterceptor implements ChannelInterceptor {

  private final WebSocketSubscriptionValidator subscriptionValidator;

  @Override
  public Message<?> preSend(Message<?> message, MessageChannel channel) {
    StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
    if (StompCommand.SUBSCRIBE.equals(headerAccessor.getCommand())) {
      subscriptionValidator.validateSubscription(headerAccessor.getDestination(),
          headerAccessor.getUser());
    }

    return message;
  }

}
