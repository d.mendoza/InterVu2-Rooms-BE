package com.focusvision.intervu.room.api.twilio.model;

/**
 * Model representing the dial in PIN details.
 */
public record DialInPin(String pin) {

}
