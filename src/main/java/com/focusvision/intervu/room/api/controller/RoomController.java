package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.model.dto.ParticipantDialInInfoDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.ParticipantDialInService;
import com.focusvision.intervu.room.api.service.RoomService;
import com.focusvision.intervu.room.api.service.RoomStateService;
import com.focusvision.intervu.room.api.service.impl.ParticipantStateService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomController {

  private final RoomService roomService;
  private final RoomStateService roomStateService;
  private final ParticipantStateService participantStateService;
  private final ParticipantDialInService participantDialInService;

  /**
   * Gets room info.
   *
   * @param caller Authenticated participant.
   * @return Participant room state.
   */
  @GetMapping("info")
  @ApiOperation(
      value = "Retrieve room info",
      notes = "Returns details for participant's belonging room")
  @PreAuthorize("hasAuthority('ROOM_READ')")
  public ParticipantRoomStateDto info(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching waiting room info for participant {}.", caller.getId());

    var roomId = caller.getRoomId();
    var callerId = caller.getId();

    return new ParticipantRoomStateDto()
        .setRoomState(roomStateService.getRoomState(roomId))
        .setParticipantState(participantStateService.getOrCreateParticipantState(callerId, roomId))
        .setDialInInfo(participantDialInService.getDialInInfo(callerId));
  }

  /**
   * Start room.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("start")
  @ApiOperation(
      value = "Starts the room",
      notes = "Starts the room if it's not already started")
  @PreAuthorize("hasAuthority('ROOM_CONTROL')")
  public void start(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Starting room {}", caller.getRoomPlatformId());

    roomService.startRoom(caller.getRoomId(), caller.getId());
  }

  /**
   * Joins room.
   *
   * @param caller Authenticated participant.
   */
  // TODO: Discuss need for this method?
  @PutMapping("join")
  @ApiOperation(
      value = "Join the room",
      notes = "Marks participant as ready to join the running room")
  @PreAuthorize("hasAuthority('ROOM_JOIN')")
  public void join(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Marking participant {} ready for room {}.", caller.getId(),
        caller.getRoomPlatformId());

    roomService.joinRoom(caller.getRoomId(), caller.getId());
  }

  /**
   * Finish room info.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("finish")
  @ApiOperation(
      value = "Marks room as finished",
      notes = "Marks research session as finished.")
  @PreAuthorize("hasAuthority('ROOM_CONTROL')")
  public void finish(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Participant {} marking room {} as finished.", caller.getId(),
        caller.getRoomPlatformId());

    roomService.finish(caller.getRoomId(), caller.getId());
  }

  /**
   * Gets participants' dial-in info for caller's room.
   *
   * @param caller Authenticated participant.
   * @return Participants' dial-in info.
   */
  @GetMapping(value = "pins")
  @ApiOperation(
      value = "Retrieve participants' dial-in information",
      notes = "Returns caller's room participants' dial-in information")
  @PreAuthorize("hasAuthority('PIN_CODES_READ')")
  public List<ParticipantDialInInfoDto> pinCodes(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Getting PIN codes for participants in room {}", caller.getRoomId());

    return participantDialInService.getDialInfoForParticipants(caller.getRoomId());
  }

}
