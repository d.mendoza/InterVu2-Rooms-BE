package com.focusvision.intervu.room.api.stimulus.event.handler;

import com.focusvision.intervu.room.api.draw.service.DrawingNotificationService;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawClearEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawRedoEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawUndoEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Drawing events handler responsible for sending the drawing notifications.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class StimulusDrawEventNotificationHandler implements StimulusDrawEventHandler {

  private final DrawingNotificationService drawingNotificationService;

  @Override
  @EventListener
  public void handle(StimulusDrawEvent event) {
    log.debug("Handling stimulus draw event: [{}].", event);

    drawingNotificationService.sendDrawNotification(event);
  }

  @Override
  @EventListener
  public void handle(StimulusDrawUndoEvent event) {
    log.debug("Handling stimulus draw undo event: [{}].", event);

    drawingNotificationService.sendDrawNotification(event);
  }

  @Override
  @EventListener
  public void handle(StimulusDrawRedoEvent event) {
    log.debug("Handling stimulus draw redo event: [{}].", event);

    drawingNotificationService.sendDrawNotification(event);
  }

  @Override
  @EventListener
  public void handle(StimulusDrawClearEvent event) {
    log.debug("Handling stimulus draw clear event: [{}].", event);

    drawingNotificationService.sendDrawNotification(event);
  }

}
