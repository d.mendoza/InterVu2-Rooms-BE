package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimuliFocusDisabledEvent;

/**
 * Stimulus focus disabled event handler.
 */
public interface StimuliFocusDisabledEventHandler {

  void handle(StimuliFocusDisabledEvent event);
}
