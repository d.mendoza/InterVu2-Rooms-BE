package com.focusvision.intervu.room.api.state.stimulus;

import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import java.util.UUID;

/**
 * Service for handling action on stimuli.
 */
public interface StimulusActionOperator {

  /**
   * Saves vote action on poll stimulus.
   *
   * @param roomId        Room ID.
   * @param participantId Participant ID.
   * @param stimulusId    Stimulus ID.
   * @param content       Action content.
   * @param offset        Action offset.
   * @return Stimulus action state.
   */
  StimulusActionState vote(UUID roomId, UUID participantId, UUID stimulusId, String content,
                           Long offset);

  /**
   * Saves ranking action on poll stimulus.
   *
   * @param roomId        Room ID.
   * @param participantId Participant ID.
   * @param stimulusId    Stimulus ID.
   * @param content       Action content.
   * @param offset        Action offset.
   * @return Stimulus action state.
   */
  StimulusActionState rank(UUID roomId, UUID participantId, UUID stimulusId, String content,
                           Long offset);

  /**
   * Saves answer action on poll stimulus.
   *
   * @param roomId        Room ID.
   * @param participantId Participant ID.
   * @param stimulusId    Stimulus ID.
   * @param content       Action content.
   * @param offset        Action offset.
   * @return Stimulus action state.
   */
  StimulusActionState answer(UUID roomId, UUID participantId, UUID stimulusId, String content,
                             Long offset);

  /**
   * Saves page change action on document stimulus.
   *
   * @param roomId        Room ID.
   * @param participantId Participant ID.
   * @param stimulusId    Stimulus ID.
   * @param content       Action content.
   * @param offset        Action offset.
   * @return Stimulus action state.
   */
  StimulusActionState changePage(UUID roomId, UUID participantId, UUID stimulusId, String content,
                                 Long offset);

  /**
   * Gets last stimulus action and populates content property of {@link StimulusStateDto} if
   * present.
   *
   * @param stimulusId Stimulus ID.
   * @return Stimulus state dto.
   */
  StimulusStateDto getLastAction(UUID stimulusId);
}
