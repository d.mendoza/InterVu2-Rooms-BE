package com.focusvision.intervu.room.api.export.service;


import com.focusvision.intervu.room.api.export.model.BookmarkExportDto;
import java.util.List;
import java.util.Optional;

/**
 * Service for exporting room bookmark data.
 */
public interface BookmarkExportService {

  /**
   * Gets the export of bookmark data for provided room ID.
   *
   * @param platformId Room platform ID.
   * @return List of room bookmarks or empty {@link Optional} if room can't be found.
   */
  Optional<List<BookmarkExportDto>> export(String platformId);
}
