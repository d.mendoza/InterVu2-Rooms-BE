package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessageTrack;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link ChatMessageTrack} entity.
 *
 * @author Branko Ostojic
 */
public interface ChatMessageTrackRepository extends JpaRepository<ChatMessageTrack, UUID> {

  Optional<ChatMessageTrack> findOneBySourceAndTypeAndParticipantIdAndHandle(
      ChatMessage.ChatSource source,
      ChatType type,
      UUID participantId,
      UUID handle);
}
