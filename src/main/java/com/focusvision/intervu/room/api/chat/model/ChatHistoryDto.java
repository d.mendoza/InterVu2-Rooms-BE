package com.focusvision.intervu.room.api.chat.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the chat history.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Chat History")
public class ChatHistoryDto {

  @ApiModelProperty("Number of unread chat messages")
  private long unreadMessages;

  @ApiModelProperty("Latest seen chat message ID")
  private UUID latestSeenMessageId;

  @ApiModelProperty("Chat messages")
  private List<ChatMessageDto> messages;
}
