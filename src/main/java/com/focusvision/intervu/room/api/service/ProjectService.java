package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.entity.Project;
import java.util.Optional;

/**
 * Service used for project related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ProjectService {

  /**
   * Finds project by it's platform ID.
   *
   * @param platformId Project platform ID.
   * @return Project details or empty {@link Optional}.
   */
  Optional<Project> findByPlatformId(String platformId);

  /**
   * Saves project to the persistent storage.
   *
   * @param project Project to be saved.
   * @return Saved project.
   */
  Project save(Project project);

  /**
   * Performs project sanitise operation. It will remove respondents chat history for GDPR
   * purposes.
   *
   * @param projectPlatformId Project to be saved.
   */
  void sanitiseProject(String projectPlatformId);

}
