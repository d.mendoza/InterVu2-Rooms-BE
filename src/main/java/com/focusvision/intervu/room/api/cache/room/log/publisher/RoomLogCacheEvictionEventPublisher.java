package com.focusvision.intervu.room.api.cache.room.log.publisher;

import com.focusvision.intervu.room.api.cache.room.log.event.ClearRoomLogCacheEvent;

/**
 * Publisher contract for clearing room log cache events.
 *
 * @author Branko Ostojic
 */
public interface RoomLogCacheEvictionEventPublisher {

  /**
   * Publish the provided clear room logs cache event.
   *
   * @param event Cache eviction event.
   */
  void publish(ClearRoomLogCacheEvent event);
}
