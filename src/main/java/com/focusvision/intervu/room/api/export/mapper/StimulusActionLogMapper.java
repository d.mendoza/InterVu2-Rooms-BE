package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.export.model.StimulusActionLogExportDto;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionLog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link StimulusActionLog} entity.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface StimulusActionLogMapper {

  /**
   * Maps stimulus action log to DTO for export.
   *
   * @param stimulusActionLog     Stimulus action log to be mapped.
   * @param offset                Log offset.
   * @param stimulusPlatformId    Stimulus platform ID.
   * @param participantPlatformId Participant platform ID.
   * @return Resulting DTO.
   */
  @Mapping(source = "offset", target = "offset")
  @Mapping(source = "stimulusPlatformId", target = "stimulusPlatformId")
  @Mapping(source = "participantPlatformId", target = "participantPlatformId")
  StimulusActionLogExportDto mapToDto(
      StimulusActionLog stimulusActionLog,
      Long offset,
      String stimulusPlatformId,
      String participantPlatformId);
}
