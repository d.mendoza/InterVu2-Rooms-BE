package com.focusvision.intervu.room.api.stimulus.document.model;

import java.util.UUID;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Model representing document stimulus action message.
 */
@Data
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class DocumentStimulusActionMessageDto {

  /**
   * ID of the stimulus on which action is performed.
   */
  @NotEmpty
  private UUID stimulusId;

  /**
   * Document stimulus action content.
   */
  @NotEmpty
  private String content;

  /**
   * Document stimulus action UTC timestamp.
   */
  @NotNull
  private Long timestamp;
}
