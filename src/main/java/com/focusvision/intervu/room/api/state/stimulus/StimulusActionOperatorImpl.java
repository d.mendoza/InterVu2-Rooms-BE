package com.focusvision.intervu.room.api.state.stimulus;

import static com.focusvision.intervu.room.api.common.model.StimulusActionType.DOCUMENT_PAGE_CHANGED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_ANSWERED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_RANKED;
import static com.focusvision.intervu.room.api.common.model.StimulusActionType.POLL_VOTED;

import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusActionLogService;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusStateService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation for {@link StimulusActionOperator}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusActionOperatorImpl implements StimulusActionOperator {

  private final StimulusStateService stimulusActionStateService;
  private final StimulusActionLogService stimulusActionLogService;

  @Override
  public StimulusActionState vote(UUID roomId, UUID participantId, UUID stimulusId, String content,
                                  Long offset) {
    log.debug(
        "Saving vote action in room {} for participant {} "
            + "on stimuli {} with content {} and offset {}.",
        roomId, participantId, stimulusId, content, offset);

    var stimulusAction =
        stimulusActionStateService.getStimulusStateForParticipant(stimulusId, participantId)
            .map(stimulusState -> updateActionState(stimulusState, content))
            .orElseGet(() -> createActionState(participantId, stimulusId, content));
    stimulusActionLogService.save(stimulusAction, POLL_VOTED, offset);

    return stimulusActionStateService.save(stimulusAction);
  }

  @Override
  public StimulusActionState rank(UUID roomId, UUID participantId, UUID stimulusId, String content,
                                  Long offset) {
    log.debug(
        "Saving ranking action in room {} for participant {} "
            + "on stimuli {} with content {} and offset {}.",
        roomId, participantId, stimulusId, content, offset);

    var stimulusAction =
        stimulusActionStateService.getStimulusStateForParticipant(stimulusId, participantId)
            .map(stimulusState -> updateActionState(stimulusState, content))
            .orElseGet(() -> createActionState(participantId, stimulusId, content));
    stimulusActionLogService.save(stimulusAction, POLL_RANKED, offset);

    return stimulusActionStateService.save(stimulusAction);
  }

  @Override
  public StimulusActionState answer(UUID roomId, UUID participantId, UUID stimulusId,
                                    String content, Long offset) {
    log.debug(
        "Saving answer action in room {} for participant {} "
            + "on stimuli {} with content {} and offset {}.",
        roomId, participantId, stimulusId, content, offset);

    var stimulusAction =
        stimulusActionStateService.getStimulusStateForParticipant(stimulusId, participantId)
            .map(stimulusState -> updateActionState(stimulusState, content))
            .orElseGet(() -> createActionState(participantId, stimulusId, content));
    stimulusActionLogService.save(stimulusAction, POLL_ANSWERED, offset);

    return stimulusActionStateService.save(stimulusAction);
  }

  @Override
  public StimulusActionState changePage(UUID roomId, UUID participantId, UUID stimulusId,
                                        String content, Long offset) {
    log.debug(
        "Saving page change action in room {} for participant {} "
            + "on stimulus {} with content {} and offset {}.",
        roomId, participantId, stimulusId, content, offset);

    var stimulusAction =
        stimulusActionStateService.getStimulusStateForParticipant(stimulusId, participantId)
            .map(stimulusState -> updateActionState(stimulusState, content))
            .orElseGet(() -> createActionState(participantId, stimulusId, content));
    stimulusActionLogService.save(stimulusAction, DOCUMENT_PAGE_CHANGED, offset);

    return stimulusActionStateService.save(stimulusAction);
  }

  @Override
  public StimulusStateDto getLastAction(UUID stimulusId) {
    log.debug("Getting last action for stimulus {}", stimulusId);

    var dto = new StimulusStateDto()
        .setStimulusId(stimulusId);

    stimulusActionLogService.findLastAction(stimulusId)
        .ifPresent(log -> dto.setParticipantId(log.getParticipantId())
            .setContent(log.getContent()));

    return dto;
  }

  private StimulusActionState createActionState(UUID participantId, UUID stimulusId,
                                                String content) {
    return new StimulusActionState()
        .setStimulusId(stimulusId)
        .setParticipantId(participantId)
        .setContent(content);
  }

  private StimulusActionState updateActionState(StimulusActionState stimulusActionState,
                                                String content) {
    return stimulusActionState.setContent(content);
  }
}
