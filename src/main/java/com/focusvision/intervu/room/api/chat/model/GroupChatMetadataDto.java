package com.focusvision.intervu.room.api.chat.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the group chat metadata.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Group chat metadata")
public class GroupChatMetadataDto {

  @ApiModelProperty("Handle for chat identification")
  private UUID handle;
  @ApiModelProperty("Type for chat identification")
  private ChatType type;
  @ApiModelProperty("Readonly chat permission indicator")
  private boolean readOnly;
  @ApiModelProperty("Destination for sending chat messages")
  private String destination;
  @ApiModelProperty("Destination for sending seen chat message ID")
  private String destinationForSeen;
  @ApiModelProperty("URL for fetching chat messages")
  private String messagesUrl;
  @ApiModelProperty("Chat enabled/disabled indicator")
  private boolean enabled;
  @ApiModelProperty("Chat control")
  private ChatControlDto control;

  @JsonIgnore
  private List<ParticipantChatMetadataDto> participants;
}
