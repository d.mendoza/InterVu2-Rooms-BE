package com.focusvision.intervu.room.api.state.stimulus.service;

import static java.time.Instant.now;

import com.focusvision.intervu.room.api.common.model.StimulusActionType;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionLog;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import com.focusvision.intervu.room.api.state.stimulus.repository.StimulusActionLogRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link StimulusActionLogService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusActionLogServiceImpl implements StimulusActionLogService {

  private final StimulusActionLogRepository stimulusActionLogRepository;

  @Override
  public StimulusActionLog save(StimulusActionState stimulusAction,
                                StimulusActionType actionType,
                                Long offset) {
    log.debug("Saving stimulus action log for action [{}] with type {} and offset {}.",
        stimulusAction, actionType, offset);

    return stimulusActionLogRepository.save(new StimulusActionLog()
        .setContent(stimulusAction.getContent())
        .setActionType(actionType)
        .setVersion(now().toEpochMilli())
        .setAddedAt(now())
        .setOffset(offset)
        .setParticipantId(stimulusAction.getParticipantId())
        .setStimulusId(stimulusAction.getStimulusId()));
  }

  @Override
  public Optional<StimulusActionLog> findLastAction(UUID stimulusId) {
    log.debug("Finding last action on stimulus {}", stimulusId);

    return stimulusActionLogRepository.findTop1ByStimulusIdOrderByOffsetDescAddedAtDesc(stimulusId);
  }

  @Override
  public List<StimulusActionLog> getLogsForStimulus(UUID stimulusId) {
    log.debug("Finding action logs for stimulus {}", stimulusId);

    return stimulusActionLogRepository.findByStimulusIdOrderByOffsetAscAddedAtAsc(stimulusId);
  }
}
