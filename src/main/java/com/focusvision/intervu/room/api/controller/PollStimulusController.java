package com.focusvision.intervu.room.api.controller;

import static java.util.UUID.fromString;
import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.model.dto.PollStimulusRespondentResultDto;
import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.stimulus.mapper.StimulusActionStateMapper;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusStateService;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room stimuli action state related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/stimuli/poll", produces = MediaType.APPLICATION_JSON_VALUE)
public class PollStimulusController {

  private final StimulusStateService stimulusStateService;
  private final StimulusService stimulusService;
  private final StimulusActionStateMapper stimulusActionStateMapper;

  /**
   * Gets stimulus state for respondents.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @GetMapping("{id}/state")
  @PreAuthorize("hasAuthority('STIMULI_READ')")
  public StimulusStateDto respondentState(@ApiParam("Stimulus ID") @PathVariable String id,
                                          @ApiIgnore @AuthenticationPrincipal
                                              AuthenticatedParticipant caller) {
    log.debug("Get stimulus {} state for participant {}", id, caller.getId());

    return stimulusStateService.getStimulusStateForParticipant(fromString(id), caller.getId())
        .map(stimulusActionStateMapper::mapToDto)
        .orElse(noStateOnStimulusForParticipant(fromString(id), caller.getId()));
  }

  /**
   * Gets stimulus states.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @GetMapping("{id}/states")
  @PreAuthorize("hasAuthority('STIMULI_STATE_READ')")
  public List<StimulusStateDto> stimulusStates(@ApiParam("Stimulus ID") @PathVariable String id,
                                               @ApiIgnore @AuthenticationPrincipal
                                                   AuthenticatedParticipant caller) {
    log.debug("Get stimulus {} state for participant {}", id, caller.getId());

    return stimulusStateService.getStimulusState(fromString(id)).stream()
        .map(stimulusActionStateMapper::mapToDto)
        .toList();
  }

  /**
   * Gets poll stimulus results.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @GetMapping("{id}/results")
  @PreAuthorize("hasAuthority('STIMULI_READ')")
  public PollStimulusRespondentResultDto results(@ApiParam("Stimulus ID") @PathVariable String id,
                                                 @ApiIgnore @AuthenticationPrincipal
                                                     AuthenticatedParticipant caller) {
    log.debug("Get stimulus {} results for participant {}", id, caller.getId());

    var uuid = fromString(id);
    var stimulus = stimulusService.findStimulusWithBroadcastResults(uuid);

    var results = stimulus.map(Stimulus::getId).map(stimulusStateService::getStimulusState)
        .map(states -> states.stream().map(StimulusActionState::getContent).toList())
        .orElse(List.of());

    return new PollStimulusRespondentResultDto()
        .setStimulusId(uuid)
        .setResults(results);
  }

  private StimulusStateDto noStateOnStimulusForParticipant(UUID stimulusId, UUID participantId) {
    return new StimulusStateDto()
        .setStimulusId(stimulusId)
        .setParticipantId(participantId);
  }
}
