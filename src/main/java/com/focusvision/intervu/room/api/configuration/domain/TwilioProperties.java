package com.focusvision.intervu.room.api.configuration.domain;

import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * Twilio properties.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "app.twilio")
public class TwilioProperties {

  /** Twilio account identification. */
  private final String accountSid;

  /** Twilio account API key. */
  private final String apiKey;

  /** Twilio key SID. */
  private final String keySid;

  /** Twilio key secret. */
  private final String keySecret;

  /** Twilio callback URL. */
  private final String callbackUrl;

  /** Twilio dial-in numbers. */
  private final List<String> dialInNumbers;
}
