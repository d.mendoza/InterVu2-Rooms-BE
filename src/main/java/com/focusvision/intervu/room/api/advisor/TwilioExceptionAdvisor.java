package com.focusvision.intervu.room.api.advisor;

import com.focusvision.intervu.room.api.exception.StreamingApiException;
import com.focusvision.intervu.room.api.service.impl.TwilioStreamingService;
import com.twilio.exception.TwilioException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Twilio exception handler adviser.
 * If any method in {@link TwilioStreamingService} throws a {@link TwilioException}
 * then translate that exception to {@link StreamingApiException}.
 */
@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class TwilioExceptionAdvisor {

  /**
   * Handles {@link TwilioException} and translates it to {@link StreamingApiException}.
   *
   * @param joinPoint Join point.
   * @return Method output.
   * @throws Throwable in case of any exceptions.
   */
  @Around(
      "execution(* com.focusvision.intervu.room.api.service.impl.TwilioStreamingService..*(..))")
  public Object translateToStreamingApiException(ProceedingJoinPoint joinPoint) throws Throwable {
    try {
      return joinPoint.proceed();
    } catch (TwilioException e) {
      throw new StreamingApiException(e.getMessage());
    }
  }

}
