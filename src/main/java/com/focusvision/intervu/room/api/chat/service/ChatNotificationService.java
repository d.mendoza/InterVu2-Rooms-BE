package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import java.util.UUID;

/**
 * Service used for chat message notification related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatNotificationService {

  /**
   * Sends the provided chat message to the specified recipient (destination).
   *
   * @param message   Chat message to be sent.
   * @param recipient Recipient of the message.
   */
  void send(ChatMessage message, UUID recipient);

}
