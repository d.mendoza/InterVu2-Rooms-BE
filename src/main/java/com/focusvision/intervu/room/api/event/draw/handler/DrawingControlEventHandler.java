package com.focusvision.intervu.room.api.event.draw.handler;

import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncDisabledEvent;
import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncEnabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingDisabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingEnabledEvent;

/**
 * Handler for the drawing control.
 *
 * @author Branko Ostojic
 */
public interface DrawingControlEventHandler {

  /**
   * Handles the provided drawing sync enabled event.
   *
   * @param event Drawing sync enabled event.
   */
  void handle(DrawingSyncEnabledEvent event);

  /**
   * Handles the provided drawing sync disabled event.
   *
   * @param event Drawing sync disabled event.
   */
  void handle(DrawingSyncDisabledEvent event);

  /**
   * Handles participant drawing enabled event.
   *
   * @param event {@link ParticipantDrawingEnabledEvent}.
   */
  void handle(ParticipantDrawingEnabledEvent event);

  /**
   * Handles participant drawing disabled event.
   *
   * @param event {@link ParticipantDrawingDisabledEvent}.
   */
  void handle(ParticipantDrawingDisabledEvent event);
}
