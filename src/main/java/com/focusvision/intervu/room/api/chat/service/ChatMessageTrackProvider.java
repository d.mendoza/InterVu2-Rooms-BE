package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessageTrack;
import java.util.Optional;
import java.util.UUID;

/**
 * Service used for chat tracking related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatMessageTrackProvider {

  /**
   * Find the latest track record matching provided parameters.
   *
   * @param source        Chat source.
   * @param type          Chat type.
   * @param participantId Chat participant.
   * @param handle        Chat handle.
   * @return matching chat track or empty {@link Optional}.
   */
  Optional<ChatMessageTrack> findLatestTrack(ChatMessage.ChatSource source,
                                             ChatType type,
                                             UUID participantId,
                                             UUID handle);

  /**
   * Adds new chat message track.
   *
   * @param chatMessageTrack Chat message track to add.
   * @return Added chat message track.
   **/
  ChatMessageTrack addTrack(ChatMessageTrack chatMessageTrack);
}
