package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.event.room.model.RoomCreatedEvent;

/**
 * Room created event handler.
 */
public interface RoomCreatedEventHandler {

  void handle(RoomCreatedEvent event);
}
