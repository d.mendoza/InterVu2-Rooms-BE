package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.ChatSource.MEETING_ROOM;
import static java.util.List.of;
import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.chat.adapter.ChatMessagesSenderAdapter;
import com.focusvision.intervu.room.api.common.provider.ResearchSessionProvider;
import com.focusvision.intervu.room.api.model.messaging.ChatMessageData;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionChatMessagesData;
import com.focusvision.intervu.room.api.repository.ChatMessageRepository;
import com.focusvision.intervu.room.api.service.ChatMessagesNotificationService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ChatMessagesNotificationService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
// TODO: Refactor this service
public class ChatMessagesSendingServiceImpl implements ChatMessagesNotificationService {

  private final ResearchSessionProvider researchSessionProvider;
  private final ChatMessageRepository chatMessageRepository;
  private final ChatMessagesSenderAdapter senderAdapter;

  @Override
  public void sendChatMessages(UUID id) {
    log.info("Sending chat messages for research session: {}", id);

    var researchSession = researchSessionProvider.get(id);

    var handle = researchSession.getId();
    var backroomInternalChatMessages =
        chatMessageRepository.findAllByHandleAndTypeAndSourceIsInOrderBySentAtAsc(handle, INTERNAL,
            of(MEETING_ROOM));

    var chatMessages = backroomInternalChatMessages.stream()
        .map(chatMessage -> new ChatMessageData()
            .setSenderPlatformId(chatMessage.getSenderPlatformId())
            .setSenderDisplayName(chatMessage.getSenderDisplayName())
            .setSentAt(chatMessage.getSentAt())
            .setMessage(chatMessage.getMessage()))
        .toList();

    var researchSessionChatMessagesData = new ResearchSessionChatMessagesData()
        .setId(researchSession.getPlatformId())
        .setMessages(chatMessages);

    senderAdapter.send(researchSessionChatMessagesData);
  }
}
