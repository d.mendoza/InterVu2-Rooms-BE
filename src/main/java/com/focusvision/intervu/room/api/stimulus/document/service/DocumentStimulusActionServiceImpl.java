package com.focusvision.intervu.room.api.stimulus.document.service;

import static java.util.Optional.ofNullable;

import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.RoomStateService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;
import com.focusvision.intervu.room.api.state.stimulus.publisher.DocumentStimulusPageChangeEventPublisher;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link DocumentStimulusActionService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentStimulusActionServiceImpl implements DocumentStimulusActionService {

  private final RoomOffsetProvider roomOffsetProvider;
  private final DocumentStimulusPageChangeEventPublisher documentStimulusPageChangeEventPublisher;
  private final RoomStateService roomStateService;

  @Override
  public void setActivePage(UUID stimulusId,
                            String content,
                            Long timestamp,
                            AuthenticatedParticipant caller) {
    log.debug("Handling participant {} document stimulus {} change page action: {}",
        caller.getId(), stimulusId, content);

    var canChangePage = ofNullable(roomStateService.getRoomState(caller.getRoomId()))
        .map(RoomStateDto::getStimulus)
        .isPresent();

    if (canChangePage) {
      Long offset = roomOffsetProvider.calculateOffset(caller.getRoomId(), timestamp);

      documentStimulusPageChangeEventPublisher
          .publish(DocumentStimulusPageChangeEvent.builder()
              .roomId(caller.getRoomId())
              .stimulusId(stimulusId)
              .participantId(caller.getId())
              .content(content)
              .offset(offset)
              .build());
    }
  }
}
