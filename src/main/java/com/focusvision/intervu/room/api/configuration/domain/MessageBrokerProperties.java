package com.focusvision.intervu.room.api.configuration.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * Message broker properties.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 **/
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "app.broker")
public class MessageBrokerProperties {
  /**
   * Message broker host address.
   */
  private final String host;
  /**
   * Message broker relay port.
   */
  private final Integer relayPort;
  /**
   * Message broker username.
   */
  private final String username;
  /**
   * Message broker password.
   */
  private final String password;

  /**
   * Message transport properties.
   */
  private final MessageBrokerTransportProperties transport;

  /**
   * Message broker transport properties.
   */
  @Getter
  @RequiredArgsConstructor
  @ConstructorBinding
  public static class MessageBrokerTransportProperties {
    /**
     * Maximum size of an inbound sub-protocol message, such as a STOMP frame which may be
     * aggregated from multiple WebSocket messages.
     */
    private final Integer messageSizeLimit;
  }
}
