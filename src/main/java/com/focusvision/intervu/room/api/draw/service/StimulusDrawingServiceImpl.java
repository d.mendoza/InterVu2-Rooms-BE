package com.focusvision.intervu.room.api.draw.service;

import com.focusvision.intervu.room.api.common.event.EventPublisher;
import com.focusvision.intervu.room.api.draw.model.StimulusDrawMessage;
import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawClearEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawRedoEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawUndoEvent;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link StimulusDrawingService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusDrawingServiceImpl implements StimulusDrawingService {

  private final RoomOffsetProvider roomOffsetProvider;
  private final EventPublisher publisher;

  @Override
  public void draw(UUID roomId,
                   UUID stimulusId,
                   UUID participantId,
                   StimulusDrawMessage stimulusDrawMessage) {
    log.debug("Handling stimulus {} drawing action: {}", stimulusId, stimulusDrawMessage);

    // TODO: disabling for performances reasons for now
    /*
    if (!stimulusService.isStimulusActive(roomId)) {
        throw drawForbiddenException();
    }*/

    String content = stimulusDrawMessage.getContent();
    Long offset = roomOffsetProvider.calculateOffset(roomId, stimulusDrawMessage.getTimestamp());

    //of(roomStateService.getRoomState(roomId).getStimulus())
    //        .ifPresent(stimulusDto -> {
    switch (stimulusDrawMessage.getType()) {
      case DRAW -> publisher.publish(
          new StimulusDrawEvent(roomId, stimulusId, participantId, content, offset));
      case UNDO -> publisher.publish(
          new StimulusDrawUndoEvent(roomId, stimulusId, participantId, content, offset));
      case REDO -> publisher.publish(
          new StimulusDrawRedoEvent(roomId, stimulusId, participantId, content, offset));
      case CLEAR -> publisher.publish(
          new StimulusDrawClearEvent(roomId, stimulusId, participantId, offset));
      default -> {
      }
    }
    //});
  }

  private IntervuRoomException drawForbiddenException() {
    return new ActionForbiddenException("Draw forbidden. Stimulus not active.");
  }
}
