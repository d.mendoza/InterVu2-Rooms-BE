package com.focusvision.intervu.room.api.event.draw.publisher;

import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncDisabledEvent;
import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncEnabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingDisabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingEnabledEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * {@link ApplicationEventPublisher} specific implementation of a drawing control events publisher.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ApplicationDrawingSyncEventPublisher implements
    DrawingControlEventPublisher {

  private final ApplicationEventPublisher publisher;

  @Override
  public void publish(DrawingSyncDisabledEvent event) {
    log.debug("Publishing drawing sync disabled event: [{}].", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(DrawingSyncEnabledEvent event) {
    log.debug("Publishing drawing sync enabled event: [{}].", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantDrawingEnabledEvent event) {
    log.debug("Publishing participant drawing enabled: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantDrawingDisabledEvent event) {
    log.debug("Publishing participant drawing disabled: {}.", event);

    publisher.publishEvent(event);
  }
}
