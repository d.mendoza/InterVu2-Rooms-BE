package com.focusvision.intervu.room.api.state;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.partitioningBy;

import com.focusvision.intervu.room.api.state.model.ParticipantState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomLeaderService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomLeaderServiceImpl implements RoomLeaderService {

  private final RoomStateProvider roomStateProvider;

  @Override
  public boolean isRoomLeader(UUID roomId, UUID participantId) {
    log.debug("Checking if participant {} is the room leader in room {}.", participantId, roomId);

    return getRoomLeader(roomId).map(participantId::equals).orElse(false);
  }

  /**
   * Finds the room leader. The room leader is the person with the moderator role that was in the
   * room for the longest time. If there is no one with the moderator role then the leader is the
   * person that can moderate that was in the room for the longest time .
   *
   * @param roomId Room id.
   * @return room leader id if present or empty {@link Optional}.
   */
  private Optional<UUID> getRoomLeader(UUID roomId) {
    var currentLog = roomStateProvider.getCurrentForRoom(roomId);
    var roomLeaderMap = extractRoomLeaderMap(currentLog);
    var moderatorsWithRole = roomLeaderMap.get(true);
    if (!moderatorsWithRole.isEmpty()) {
      return moderatorsWithRole.stream().min(comparing(ParticipantState::getFirstEntryOffset))
          .map(ParticipantState::getId);
    } else {
      var otherModerators = roomLeaderMap.get(false);
      return otherModerators.stream().min(comparing(ParticipantState::getFirstEntryOffset))
          .map(ParticipantState::getId);
    }
  }

  private Map<Boolean, List<ParticipantState>> extractRoomLeaderMap(RoomStateLog currentLog) {
    return currentLog.getRoomState().getParticipants().stream()
        .filter(this::canModerate)
        .filter(this::isOnline)
        .collect(partitioningBy(ParticipantState::isModeratorRole));
  }

  private boolean isOnline(ParticipantState participantState) {
    return participantState.isOnline() && !participantState.isBanned();
  }

  private boolean canModerate(ParticipantState participant) {
    return participant.isModerator() || participant.isAdmin() || participant.isProjectManager()
           || participant.isProjectOperator();
  }
}
