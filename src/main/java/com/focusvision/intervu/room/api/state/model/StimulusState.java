package com.focusvision.intervu.room.api.state.model;

import com.focusvision.intervu.room.api.common.model.StimulusType;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the stimulus state in room.
 */
@Data
@Builder
@Accessors(chain = true)
public class StimulusState {

  private UUID id;
  private String platformId;
  private String name;
  private String data;
  private StimulusType type;
  private boolean broadcastResults;
  private boolean interactionEnabled;
  private boolean active;
  private boolean syncEnabled;

}
