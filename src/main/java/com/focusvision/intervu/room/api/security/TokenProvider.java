package com.focusvision.intervu.room.api.security;

import static java.lang.Boolean.TRUE;
import static java.time.Instant.now;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.springframework.util.Assert.hasText;

import com.focusvision.intervu.room.api.configuration.domain.JwtProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * JWT token provider service.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TokenProvider {

  private final JwtProperties jwtConfiguration;

  /**
   * Generates the JWT token based on provided subject.
   *
   * @param subject User's username.
   * @return Generated JWT token.
   */
  public String createToken(String subject) {
    return Jwts.builder()
        .setSubject(subject)
        .setIssuedAt(new Date())
        .setExpiration(new Date(
            now().plus(jwtConfiguration.getTokenExpirationHours(), ChronoUnit.HOURS)
                .toEpochMilli()))
        .signWith(SignatureAlgorithm.HS512, jwtConfiguration.getTokenSecret())
        .compact();
  }

  /**
   * Extracts the subject from the provided JWT token.
   *
   * @param token Authentication JWT token.
   * @return Subject extracted from token.
   */
  public Optional<String> getSubjectFromToken(String token) {
    try {
      Claims claims = this.getClaims(token);
      return of(claims.getSubject());
    } catch (Exception e) {
      log.error("Error parsing JWT token.", e);
    }

    return empty();
  }

  /**
   * Extracts Intervu user from JWT token.
   *
   * @param token JWT token.
   * @return Intervu user.
   */
  public Optional<IntervuUser> getIntervuUserFromToken(String token) {
    log.debug("Extracting InterVu user from token {}.", token);

    try {
      Claims claims = this.getClaims(token);
      String platformId = claims.get("platformId", String.class);
      String email = claims.get("email", String.class);
      String firstName = claims.get("firstName", String.class);
      String lastName = claims.get("lastName", String.class);

      hasText(platformId, () -> "User platform ID not present.");
      hasText(email, () -> "User email not present.");

      var systemUser =
          claims.containsKey("isSystemUser") && TRUE.equals(claims.get("isSystemUser"));

      return of(new IntervuUser()
          .setPlatformId(platformId)
          .setEmail(email)
          .setFirstName(firstName)
          .setLastName(lastName)
          .setSystemUser(systemUser));
    } catch (Exception e) {
      log.error("Error extracting Intervu user from JWT token.", e);
    }

    return empty();
  }

  private Claims getClaims(String token) {
    return Jwts.parser()
        .setSigningKey(jwtConfiguration.getTokenSecret())
        .parseClaimsJws(token)
        .getBody();
  }

}
