package com.focusvision.intervu.room.api.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger configuration class.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Profile(value = {"!production"})
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Value("${spring.application.version}")
  private String version;

  /**
   * Configures Swagger.
   *
   * @return Swagger configuration.
   */
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.focusvision.intervu.room.api"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo());
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("InterVu Room API")
        .description("InterVu Room API")
        .version(version)
        .build();
  }

}
