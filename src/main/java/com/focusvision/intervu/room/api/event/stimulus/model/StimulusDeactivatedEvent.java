package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus deactivated event.
 *
 * @author Branko Ostojic
 */
public record StimulusDeactivatedEvent(UUID roomId) {

}
