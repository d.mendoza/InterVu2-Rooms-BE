package com.focusvision.intervu.room.api.recording.service;

import static com.focusvision.intervu.room.api.model.entity.Recording.State.COMPLETED;
import static com.focusvision.intervu.room.api.model.entity.Recording.State.ERROR;
import static com.focusvision.intervu.room.api.model.entity.Recording.State.PENDING;
import static com.focusvision.intervu.room.api.model.entity.Recording.State.PROCESSING;
import static java.lang.String.format;
import static java.time.Duration.between;
import static java.time.Instant.now;
import static java.time.Instant.ofEpochMilli;
import static java.util.Optional.ofNullable;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.entity.Recording.State;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.streaming.GroupRoom;
import com.focusvision.intervu.room.api.model.streaming.GroupRoomRecording;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingErrorEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingErrorEventPublisher;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingFinishedEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingFinishedEventPublisher;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingStartedEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingStartedEventPublisher;
import com.focusvision.intervu.room.api.repository.RecordingRepository;
import com.focusvision.intervu.room.api.service.StreamingService;
import com.focusvision.intervu.room.api.state.RoomStateProvider;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for room conference recording data related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RecordingService {

  private final RecordingRepository repository;
  private final StreamingService streamingService;
  private final RoomStateProvider roomStateProvider;
  private final RetryTemplate retryTemplate;
  private final RecordingProcessingStartedEventPublisher recordingProcessingStartedEventPublisher;
  private final RecordingProcessingFinishedEventPublisher recordingProcessingFinishedEventPublisher;
  private final RecordingProcessingErrorEventPublisher recordingProcessingErrorEventPublisher;

  private static final int MAX_LAST_ROOM_EVENTS_RANGE_MS = 2500;

  /**
   * Create pending room recording data.
   *
   * @param conference   Conference ID.
   * @param layout       Composition layout.
   * @param audioChannel Audio channel.
   * @return Created room recording data.
   */
  public Recording createPending(Conference conference,
                                 CompositionLayout layout,
                                 AudioChannel audioChannel) {
    var recording = new Recording()
        .setState(PENDING)
        .setConference(conference)
        .setCompositionLayout(layout)
        .setAudioChannel(audioChannel);

    return repository.save(recording);
  }

  /**
   * Marks existing pending room recording data or creates new one.
   *
   * @param conference   Conference ID.
   * @param layout       Composition layout.
   * @param audioChannel Audio channel.
   */
  public void markAsPendingOrCreateNew(Conference conference,
                                       CompositionLayout layout,
                                       AudioChannel audioChannel) {

    // TODO: Consider creating new record and marking old ones as deleted.
    // TODO: createPending should not be reachable here after the regenerate refactor
    repository.findByConferenceAndCompositionLayoutAndAudioChannel(conference, layout, audioChannel)
        .ifPresentOrElse(
            recording -> updateRecording(recording, conference, PENDING, layout, audioChannel),
            () -> createPending(conference, layout, audioChannel));
  }

  /**
   * Triggers processing of a recording. It will invoke streaming service to generate recording
   * (composition) and recording status will be transitioned to PROCESSING and recording processing
   * started event will be published.
   *
   * <p>In case of any errors recording status will be transitioned to ERROR and recording
   * processing error event will be published.
   *
   * @param recordingId Recording ID.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void startProcessing(UUID recordingId) {
    repository.findById(recordingId)
        .filter(Recording::isPending)
        .ifPresent(this::startProcessingWithRetry);
  }

  /**
   * Starts record processing. Streaming service will be called with retry to invoke composition
   * (recording) creation process.
   *
   * @param recording Recording to be processed.
   */
  private void startProcessingWithRetry(Recording recording) {
    var conference = recording.getConference();
    var roomSid = conference.getRoomSid();
    var isPrivacyOn = conference.getResearchSession().isPrivacy();
    var layout = recording.getCompositionLayout();

    try {
      var groupRoomRecording = retryTemplate.execute(state -> {
        log.info("Processing recording {} - attempt {}",
            recording.getId(),
            state.getRetryCount() + 1);

        return createGroupRoomRecording(roomSid, isPrivacyOn, layout, recording.getAudioChannel());
      });

      handleRecordingProcessingStarted(recording, groupRoomRecording);
    } catch (Exception e) {
      log.error(format("Error processing recording (recodingId: %s)", recording.getId()), e);

      handleRecordingProcessingFailed(recording);
    }
  }

  /**
   * Invokes streaming service to create composition (recording).
   *
   * @param roomSid      Room SID.
   * @param isPrivacyOn  Is privacy ON.
   * @param layout       Composition layout.
   * @param audioChannel Audio channel.
   * @return Recording (composition) data.
   */
  private GroupRoomRecording createGroupRoomRecording(String roomSid,
                                                      boolean isPrivacyOn,
                                                      CompositionLayout layout,
                                                      AudioChannel audioChannel) {
    try {
      return streamingService.createGroupRoomRecording(roomSid, isPrivacyOn, layout, audioChannel);
    } catch (Exception e) {
      log.error(
          format("Error creating group room recording for room SID %s.", roomSid), e);
      throw e;
    }
  }

  /**
   * Fetch all unprocessed recordings.
   *
   * @return List of Recordings.
   */
  public List<Recording> getAllUnprocessedRecordings() {
    log.debug("Fetching processing recordings.");

    return repository.findByState(PROCESSING);
  }

  /**
   * Fetch all recordings in PENDING status.
   *
   * @return List of Recordings.
   */
  public List<Recording> getAllPendingRecordings() {
    log.debug("Fetching pending recordings.");

    return repository.findByState(PENDING);
  }

  /**
   * Check whether specified recording is processed. If it is processed, recording offset from the
   * start of the conference will be calculated, event that recording processing is done will be
   * sent and status of recording will be set to COMPLETED.
   *
   * @param uuid Recording ID.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void checkProcessingRecording(UUID uuid) {
    log.debug("Checking processing recording (recordingId: {})", uuid);

    var recording = repository.getById(uuid);
    if (!recording.isProcessing()) {
      log.debug("Recording already processed (recordingId: {})", uuid);
      return;
    }

    var groupRoomRecording = streamingService.getGroupRoomRecording(recording.getRecordingSid());

    if (groupRoomRecording.isError()) {
      log.warn("Recording processing had an error (recordingId: {})", uuid);

      handleRecordingProcessingFailed(recording);
    } else if (groupRoomRecording.isProcessed()) {
      log.debug("Recording is processed (recordingId: {})", uuid);

      handleRecordingProcessingFinished(recording, groupRoomRecording);
    } else {
      log.debug("Recording still processing (recordingId: {})", uuid);
    }
  }

  /**
   * Returns a recording media link, if recording is processed and ready.
   *
   * @param id Recording ID.
   * @return Recording link or empty {@link Optional}.
   */
  public Optional<String> getRecordingLink(UUID id) {
    log.debug("Getting recording url (id: {})", id);

    return repository.findById(id)
        .map(Recording::getRecordingSid)
        .map(streamingService::getGroupRoomRecordingLink);
  }

  /**
   * Updates recording status to PROCESSING and start date to now. Publishes recording processing
   * started event.
   *
   * @param recording          Started  recording.
   * @param groupRoomRecording Streaming provider recording data.
   */
  private void handleRecordingProcessingStarted(Recording recording,
                                                GroupRoomRecording groupRoomRecording) {
    var conference = recording.getConference();
    var researchSession = conference.getResearchSession();

    recording
        .setProcessingStartedAt(now())
        .setRecordingSid(groupRoomRecording.getSid())
        .setState(PROCESSING)
        .setScreenshareBounds(groupRoomRecording.getScreenshareBounds());

    recordingProcessingStartedEventPublisher.publish(
        new RecordingProcessingStartedEvent(researchSession.getId(),
            researchSession.getPlatformId(),
            conference.getRoomSid(),
            recording.getCompositionLayout(),
            researchSession.getStartedAt(),
            researchSession.getEndedAt()));
  }

  /**
   * Updates recording status to COMPLETED and end date to now. Publishes recording processing
   * finished event.
   *
   * @param recording          Completed recording.
   * @param groupRoomRecording Streaming provider recording data.
   */
  private void handleRecordingProcessingFinished(Recording recording,
                                                 GroupRoomRecording groupRoomRecording) {
    var conference = recording.getConference();
    var groupRoom = streamingService.getGroupRoom(conference.getRoomSid());
    var researchSession = conference.getResearchSession();
    var recordingOffset = calculateRecordingOffset(researchSession, groupRoom, groupRoomRecording);

    repository.save(recording
        .setRoomDuration(groupRoom.getDuration())
        .setRecordingDuration(groupRoomRecording.getDuration())
        .setProcessingFinishedAt(now())
        .setState(COMPLETED));

    conference.setRecordingOffset(recordingOffset);

    recordingProcessingFinishedEventPublisher.publish(
        new RecordingProcessingFinishedEvent(
            researchSession.getId(),
            researchSession.getPlatformId(),
            conference.getRoomSid(),
            groupRoomRecording.getSid(),
            researchSession.getStartedAt(),
            researchSession.getEndedAt(),
            recording.getCompositionLayout(),
            recording.getAudioChannel()));
  }

  /**
   * Updates recording status to ERROR and start/end dates to now. Publishes recording processing
   * error event.
   *
   * @param recording Failed recording.
   */
  private void handleRecordingProcessingFailed(Recording recording) {
    var conference = recording.getConference();
    var researchSession = conference.getResearchSession();

    recording
        .setProcessingStartedAt(ofNullable(recording.getProcessingStartedAt())
            .orElseGet(Instant::now))
        .setProcessingFinishedAt(now())
        .setState(ERROR);

    recordingProcessingErrorEventPublisher.publish(
        new RecordingProcessingErrorEvent(
            researchSession.getId(),
            researchSession.getPlatformId(),
            conference.getRoomSid(),
            recording.getCompositionLayout(),
            researchSession.getStartedAt(),
            researchSession.getEndedAt(),
            recording.getAudioChannel()));

    repository.save(recording);
  }

  private Integer calculateRecordingOffset(
      ResearchSession researchSession, GroupRoom groupRoom, GroupRoomRecording groupRoomRecording) {
    long conferenceOffset =
        between(researchSession.getStartedAt(), ofEpochMilli(groupRoom.getDateCreatedMillisGmt()))
            .toMillis()
            % (60 * 60);
    int roomDuration = resolveRoomDuration(researchSession, groupRoom);
    int offset = (roomDuration - groupRoomRecording.getDuration()) + (int) conferenceOffset;

    return Math.max(offset, 0);
  }

  private int resolveRoomDuration(ResearchSession researchSession, GroupRoom groupRoom) {
    int duration;
    List<RoomStateLog> logs =
        roomStateProvider.getLastRoomFinishedAndParticipantOfflineLogs(researchSession.getId());
    if (isRoomBroken(logs)) {
      duration =
          (int) (logs.get(1).getAddedAt().toEpochMilli() - groupRoom.getDateCreatedMillisGmt());
    } else {
      duration = groupRoom.getDuration();
    }
    return duration;
  }

  private boolean isRoomBroken(List<RoomStateLog> logs) {
    return logs.get(0).isRoomFinished()
        && logs.get(0).getAddedAt().toEpochMilli() - logs.get(1).getAddedAt().toEpochMilli()
        > MAX_LAST_ROOM_EVENTS_RANGE_MS;
  }

  private void updateRecording(Recording recording, Conference conference, State state,
                               CompositionLayout layout, AudioChannel audioChannel) {
    repository.save(recording
        .setConference(conference)
        .setState(state)
        .setCompositionLayout(layout)
        .setAudioChannel(audioChannel));
  }
}
