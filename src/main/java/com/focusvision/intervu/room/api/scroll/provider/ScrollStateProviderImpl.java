package com.focusvision.intervu.room.api.scroll.provider;

import com.focusvision.intervu.room.api.scroll.mapper.ScrollLogMapper;
import com.focusvision.intervu.room.api.scroll.model.ScrollLogDto;
import com.focusvision.intervu.room.api.scroll.service.ScrollService;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ScrollStateProvider}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ScrollStateProviderImpl implements ScrollStateProvider {

  private final ScrollService scrollService;
  private final ScrollLogMapper scrollLogMapper;

  @Override
  public Optional<ScrollLogDto> getCurrentState(UUID roomId, UUID contextId) {
    log.debug("Fetching current scroll log for room {} and context {}.", roomId, contextId);

    return scrollService.getCurrentRoomScrollLog(roomId, contextId).map(scrollLogMapper::mapToDto);
  }
}
