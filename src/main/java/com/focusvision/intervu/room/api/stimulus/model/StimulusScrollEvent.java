package com.focusvision.intervu.room.api.stimulus.model;

import java.util.UUID;

/**
 * DTO representing the stimulus scroll event.
 */
public record StimulusScrollEvent(UUID roomId,
                                  UUID stimulusId,
                                  UUID participantId,
                                  String content,
                                  Long offset) {

}
