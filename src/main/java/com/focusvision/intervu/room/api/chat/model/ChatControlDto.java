package com.focusvision.intervu.room.api.chat.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpMethod;

/**
 * DTO representing the chat control metadata.
 */
@Getter
@Builder
@ApiModel("Chat Controls Metadata")
public class ChatControlDto {

  @ApiModelProperty("Enable Chat Control")
  private ControlDto enable;

  @ApiModelProperty("Disable Chat Control")
  private ControlDto disable;

  /**
   * Control DTO.
   */
  @Getter
  @Builder
  @ApiModel("Chat Control Data")
  public static class ControlDto {

    @ApiModelProperty("HTTP method indicator")
    private final HttpMethod method;

    @ApiModelProperty("Action URL")
    private final String url;
  }

}
