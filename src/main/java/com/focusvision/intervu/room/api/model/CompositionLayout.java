package com.focusvision.intervu.room.api.model;

/**
 * Enum that represents composition layout.
 */
public enum CompositionLayout {

  /**
   * Grid layout.
   */
  GRID,

  /**
   * One row grid layout.
   */
  ONE_ROW_GRID,

  /**
   * Picture in picture layout.
   */
  PICTURE_IN_PICTURE,

  /**
   * Main video with bottom row layout.
   */
  MAIN_VIDEO_WITH_BOTTOM_ROW,

  /**
   * Picture in picture with column layout.
   */
  PICTURE_IN_PICTURE_WITH_COLUMN,

  /**
   * Mosaic layout.
   */
  MOSAIC,

  /**
   * Chess table layout.
   */
  CHESS_TABLE
}
