package com.focusvision.intervu.room.api.model.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Entity representing conference details.
 *
 * @author Branko Ostojic
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "conferences")
@Getter
@Setter
@Accessors(chain = true)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Conference extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private String roomSid;
  @Column
  private boolean completed;
  @Column
  private Integer recordingOffset = 0;

  @OneToOne
  @JoinColumn(name = "research_session_id", referencedColumnName = "id")
  private ResearchSession researchSession;

  @OneToMany(
      fetch = FetchType.LAZY,
      mappedBy = "conference",
      cascade = CascadeType.ALL,
      orphanRemoval = true)
  private List<Recording> recordings = new ArrayList<>();

}
