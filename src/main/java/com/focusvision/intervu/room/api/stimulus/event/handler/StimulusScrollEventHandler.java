package com.focusvision.intervu.room.api.stimulus.event.handler;

import com.focusvision.intervu.room.api.stimulus.model.StimulusScrollEvent;

/**
 * Handler contract for stimulus scroll events.
 */
public interface StimulusScrollEventHandler {

  /**
   * Handles the provided stimulus scroll event.
   *
   * @param event Scroll event.
   */
  void handle(StimulusScrollEvent event);
}
