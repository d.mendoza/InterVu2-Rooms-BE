package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * DTO representing the participant connected with phone event.
 *
 * @author Branko Ostojic
 */
public record ParticipantConnectedWithPhoneEvent(UUID roomId,
                                                 UUID participantId) {

}
