package com.focusvision.intervu.room.api.stimulus.snapshot;

import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link StimulusSnapshot} entity.
 *
 * @author Branko Ostojic
 */
public interface StimulusSnapshotRepository extends JpaRepository<StimulusSnapshot, UUID> {

  /**
   * Finds all stimulus snapshots belonging to the specified room.
   *
   * @param researchSession Research session.
   * @return Matching stimulus snapshots
   */
  List<StimulusSnapshot> findAllByResearchSession(ResearchSession researchSession);

  /**
   * Counts all stimulus snapshots belonging to the specified stimulus.
   *
   * @param stimulus Stimulus.
   * @return Counts matching stimulus snapshots
   */
  Integer countStimulusSnapshotByStimulus(Stimulus stimulus);
}
