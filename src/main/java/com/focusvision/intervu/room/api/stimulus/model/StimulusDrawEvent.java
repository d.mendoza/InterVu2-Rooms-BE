package com.focusvision.intervu.room.api.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus draw event.
 *
 * @author Branko Ostojic
 */
public record StimulusDrawEvent(UUID roomId,
                                UUID stimulusId,
                                UUID participantId,
                                String content,
                                Long offset) {

}
