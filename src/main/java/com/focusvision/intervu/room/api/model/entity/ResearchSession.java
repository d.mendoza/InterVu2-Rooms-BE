package com.focusvision.intervu.room.api.model.entity;

import static com.focusvision.intervu.room.api.advisor.Lockable.Group.RESEARCH_SESSION;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.CANCELED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.EXPIRED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;

import com.focusvision.intervu.room.api.advisor.Lockable;
import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing research session.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "research_sessions")
@Getter
@Setter
@Accessors(chain = true)
public class ResearchSession extends Auditable implements Lockable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column private String platformId;
  @Column private String name;

  @Column
  @Enumerated(EnumType.STRING)
  private ResearchSessionState state;

  @Column private Instant startsAt;
  @Column private Instant endsAt;
  @Column private Integer version;
  @Column private Instant startedAt;
  @Column private Instant endedAt;
  @Column private boolean useWebcams = true;
  @Column private boolean privacy = false;

  @Column private String listenerPin;

  @Column private String speakerPin;

  @Column private String operatorPin;

  @Column
  @Enumerated(EnumType.STRING)
  private AudioChannel audioChannel;

  @ManyToOne private Project project;

  @OneToOne(mappedBy = "researchSession")
  private Conference conference;

  @OneToMany(
      fetch = FetchType.LAZY,
      mappedBy = "researchSession",
      cascade = CascadeType.ALL,
      orphanRemoval = true)
  private Set<Participant> participants = new HashSet<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      mappedBy = "researchSession",
      cascade = CascadeType.ALL,
      orphanRemoval = true)
  private List<Stimulus> stimuli = new ArrayList<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      mappedBy = "researchSession",
      cascade = CascadeType.ALL,
      orphanRemoval = true)
  private List<Bookmark> bookmarks = new ArrayList<>();

  public void addParticipant(Participant participant) {
    this.participants.add(participant);
    participant.setResearchSession(this);
  }

  public void addStimulus(Stimulus stimulus) {
    this.stimuli.add(stimulus);
    stimulus.setResearchSession(this);
  }

  public boolean isPending() {
    return SCHEDULED.equals(state);
  }

  public boolean isStarted() {
    return IN_PROGRESS.equals(state);
  }

  public boolean isCanceled() {
    return CANCELED.equals(state);
  }

  public boolean isExpired() {
    return EXPIRED.equals(state);
  }

  public boolean isFinished() {
    return FINISHED.equals(state);
  }

  public boolean isCompleted() {
    return Set.of(FINISHED, CANCELED, EXPIRED).contains(state);
  }

  @Override
  public String lockKey() {
    return platformId;
  }

  @Override
  public Group lockGroup() {
    return RESEARCH_SESSION;
  }
}
