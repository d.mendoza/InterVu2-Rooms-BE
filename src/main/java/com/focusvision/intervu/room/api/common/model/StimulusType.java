package com.focusvision.intervu.room.api.common.model;

/**
 * Enum that represents stimulus type.
 *
 * @author Branko Ostojic
 */
public enum StimulusType {

  /**
   * Poll stimulus.
   */
  POLL,
  /**
   * Drag and drop stimulus.
   */
  DRAG_AND_DROP,
  /**
   * Document sharing stimulus.
   */
  DOCUMENT_SHARING

}
