package com.focusvision.intervu.room.api.chat.mapper;

import com.focusvision.intervu.room.api.chat.model.ChatMessageDto;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Mapper for {@link ChatMessage} entity.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring")
public interface ChatMessageMapper {

  /**
   * Maps chat message to DTO.
   *
   * @param chatMessage Chat message to be mapped.
   * @return Resulting DTO.
   */
  @Mappings(value = {
      @Mapping(source = "senderDisplayName", target = "senderName")
  })
  ChatMessageDto mapToDto(ChatMessage chatMessage);

}
