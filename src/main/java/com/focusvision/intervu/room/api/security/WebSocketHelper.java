package com.focusvision.intervu.room.api.security;

import static com.focusvision.intervu.room.api.chat.service.BrokerChatCommunicationChannelService.CHAT_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.DRAW_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.PARTICIPANT_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.ROOM_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.STIMULUS_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.STIMULUS_GLOBAL_CHANNEL;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Helper class for WebSocket related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Component
// TODO: Refactor this to proper module
public class WebSocketHelper {

  /**
   * Extracts subscription info from destination.
   *
   * @param simpDestination Subscription destination.
   * @return Subscription info.
   */
  public SubscriptionInfo getSubscriptionInfo(String simpDestination) {
    log.debug("Extracting subscription info from {}.", simpDestination);

    if (simpDestination.startsWith(ROOM_CHANNEL)) {
      return new SubscriptionInfo(SubscriptionInfo.Type.ROOM, destination(simpDestination));
    } else if (simpDestination.startsWith(PARTICIPANT_CHANNEL)) {
      return new SubscriptionInfo(SubscriptionInfo.Type.PARTICIPANT, destination(simpDestination));
    } else if (simpDestination.startsWith(CHAT_CHANNEL)) {
      return new SubscriptionInfo(SubscriptionInfo.Type.CHAT, destination(simpDestination));
    } else if (simpDestination.startsWith(DRAW_CHANNEL)) {
      return new SubscriptionInfo(SubscriptionInfo.Type.DRAW, destination(simpDestination));
    } else if (simpDestination.startsWith(STIMULUS_CHANNEL)) {
      return new SubscriptionInfo(SubscriptionInfo.Type.STIMULUS, destination(simpDestination));
    } else if (simpDestination.startsWith(STIMULUS_GLOBAL_CHANNEL)) {
      return new SubscriptionInfo(SubscriptionInfo.Type.STIMULUS_GLOBAL,
          destination(simpDestination));
    }

    return new SubscriptionInfo(SubscriptionInfo.Type.UNKNOWN, simpDestination);
  }

  private String destination(String simpDestination) {
    return simpDestination.substring(simpDestination.lastIndexOf(".") + 1);
  }

  /**
   * Subscription info model.
   */
  @Getter
  @Setter
  @AllArgsConstructor
  public static class SubscriptionInfo {
    private Type type;
    private String destination;

    /**
     * Subscription ype.
     */
    public enum Type {
      ROOM,
      CHAT,
      PARTICIPANT,
      DRAW,
      STIMULUS,
      STIMULUS_GLOBAL,
      UNKNOWN
    }
  }
}
