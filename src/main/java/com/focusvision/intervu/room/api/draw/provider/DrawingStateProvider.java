package com.focusvision.intervu.room.api.draw.provider;

import com.focusvision.intervu.room.api.draw.model.DrawingLogDto;
import java.util.List;
import java.util.UUID;

/**
 * Provider for drawing state logs.
 *
 * @author Branko Ostojic
 */
public interface DrawingStateProvider {

  /**
   * Gets all drawing logs representing the current state for the specified context.
   *
   * @param roomId    Room ID.
   * @param contextId Drawing context ID.
   * @return Collection representing current drawing state on the specified context.
   */
  List<DrawingLogDto> getCurrentState(UUID roomId, UUID contextId);
}
