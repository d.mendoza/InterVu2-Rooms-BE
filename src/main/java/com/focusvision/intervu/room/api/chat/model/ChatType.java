package com.focusvision.intervu.room.api.chat.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumeration representing chat type.
 *
 * @author Branko Ostojic
 */
@Getter
@RequiredArgsConstructor
public enum ChatType {
  /**
   * Internal (backroom) chat message.
   */
  INTERNAL("internal"),
  /**
   * Respondents chat message.
   */
  RESPONDENTS("respondents"),
  /**
   * Private (direct) chat message.
   */
  PRIVATE("private");

  /**
   * Chat type representation.
   */
  private final String type;

}
