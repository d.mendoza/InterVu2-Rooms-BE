package com.focusvision.intervu.room.api.state;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.Project;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.state.model.ParticipantState;
import com.focusvision.intervu.room.api.state.model.RoomState;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/** Room state operator. */
public interface RoomStateOperator {

  RoomState create(
      UUID roomId, Project project, ResearchSession researchSession, Set<Participant> participants);

  Optional<RoomState> update(
      UUID roomId,
      Project project,
      ResearchSession researchSession,
      Set<Participant> participants,
      List<Stimulus> stimuli);

  // TODO: this should be cached by roomId, and eviction will be done on each other method
  RoomState getCurrentRoomState(UUID roomId);

  Optional<ParticipantState> getCurrentParticipantState(UUID roomId, UUID participantId);

  /**
   * Marks participant as present over WEB (browser). This will mark participant as online if it was
   * offline previously.
   *
   * @param roomId Room ID.
   * @param participant Participant.
   * @return New room state if it is changed.
   */
  Optional<RoomState> markParticipantAsPresentOverWeb(UUID roomId, Participant participant);

  /**
   * Marks participant as present over phone. This will mark participant as online if it was offline
   * previously.
   *
   * @param roomId Room ID.
   * @param participant Participant.
   * @return New room state if it is changed.
   */
  Optional<RoomState> markParticipantAsPresentOverPhone(UUID roomId, Participant participant);

  /**
   * Marks participant as NOT present (offline) over WEB. This will mark participant as offline if
   * it was online previously.
   *
   * @param roomId Room ID.
   * @param participantId Participant ID.
   * @return New room state if it is changed.
   */
  Optional<RoomState> markParticipantAsNotPresentOverWeb(UUID roomId, UUID participantId);

  /**
   * Marks participant as NOT present (offline) over phone. This will mark participant as offline if
   * it was online previously.
   *
   * @param roomId Room ID.
   * @param participantId Participant ID.
   * @return New room state if it is changed.
   */
  Optional<RoomState> markParticipantAsNotPresentOverPhone(UUID roomId, UUID participantId);

  /**
   * Marks participant as banned.
   *
   * @param roomId Room ID.
   * @param participantId Participant ID.
   * @return New room state if it is changed.
   */
  Optional<RoomState> markParticipantAsBanned(UUID roomId, UUID participantId);

  Optional<RoomState> markParticipantAsReady(UUID roomId, UUID participantId);

  Optional<RoomState> markRoomAsStarted(
      UUID roomId, Instant startedAt, Set<Participant> participants);

  Optional<RoomState> markRoomAsFinished(UUID roomId, Instant finishedAt);

  Optional<RoomState> markRoomAsCanceled(UUID roomId);

  Optional<RoomState> markRoomAsExpired(UUID roomId);

  Optional<RoomState> activateStimulus(UUID roomId, Stimulus stimulus);

  Optional<RoomState> deactivateStimulus(UUID roomId);

  Optional<RoomState> enableStimulusInteraction(UUID roomId);

  Optional<RoomState> disableStimulusInteraction(UUID roomId);

  /**
   * Marks that stimulus is in focus, if it is not already marked.
   *
   * @param roomId Room ID.
   * @return RoomState if changed, or empty {@link Optional}.
   */
  Optional<RoomState> enableStimulusFocus(UUID roomId);

  /**
   * Marks that stimulus is not in focus, if it is not already marked.
   *
   * @param roomId Room ID.
   * @return RoomState if changed, or empty {@link Optional}.
   */
  Optional<RoomState> disableStimulusFocus(UUID roomId);

  /**
   * Marks that drawing sync is ON. That will indicate that participants drawings are not visible to
   * others.
   *
   * @param roomId Room ID.
   * @return RoomState if changed, or empty {@link Optional}.
   */
  Optional<RoomState> enableDrawingSync(UUID roomId);

  /**
   * Marks that drawing sync is OFF. That will indicate that participants drawings are visible to
   * others.
   *
   * @param roomId Room ID.
   * @return RoomState if changed, or empty {@link Optional}.
   */
  Optional<RoomState> disableDrawingSync(UUID roomId);

  /**
   * Disable drawing for provided participants.
   *
   * @param roomId Room ID.
   * @param participants List of participants IDs.
   * @return Updated participants states.
   */
  List<ParticipantState> enableParticipantDrawing(UUID roomId, List<UUID> participants);

  /**
   * Disable drawing for provided participants.
   *
   * @param roomId Room ID.
   * @param participants List of participants IDs.
   * @return Updated participants states.
   */
  List<ParticipantState> disableParticipantDrawing(UUID roomId, List<UUID> participants);

  /**
   * Enable stimulus broadcast results.
   *
   * @param roomId Room ID.
   * @return RoomState if present, or empty {@link Optional}.
   */
  Optional<RoomState> enableStimulusBroadcastResults(UUID roomId);

  /**
   * Disable stimulus broadcast results.
   *
   * @param roomId Room ID.
   * @return RoomState if present, or empty {@link Optional}.
   */
  Optional<RoomState> disableStimulusBroadcastResults(UUID roomId);

  /**
   * Reset stimulus in the given room.
   *
   * @param roomId Room ID.
   * @return RoomState if present, or empty {@link Optional}.
   */
  Optional<RoomState> resetStimulus(UUID roomId);

  /**
   * Enables the respondents chat.
   *
   * @param roomId Room ID.
   * @return RoomState if changed, or empty {@link Optional}.
   */
  Optional<RoomState> enableRespondentsChat(UUID roomId);

  /**
   * Disables the respondents chat.
   *
   * @param roomId Room ID.
   * @return RoomState if changed, or empty {@link Optional}.
   */
  Optional<RoomState> disableRespondentsChat(UUID roomId);

  ParticipantState constructParticipantState(UUID roomId, Participant participant);

  /**
   * Enables active stimulus sync.
   *
   * @param roomId Room ID.
   * @return Room state if present, or empty {@link Optional}.
   */
  Optional<RoomState> enableStimulusSync(UUID roomId);

  /**
   * Disables stimulus sync.
   *
   * @param roomId Room ID.
   * @return Room state if present, or empty {@link Optional}.
   */
  Optional<RoomState> disableStimulusSync(UUID roomId);
}
