package com.focusvision.intervu.room.api.model.messaging;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the chat message.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class ChatMessageData {

  /**
   * Chat message content.
   */
  @ToString.Exclude
  private String message;

  /**
   * Chat message sender platform ID.
   */
  private String senderPlatformId;

  /**
   * Chat message sender display name.
   */
  private String senderDisplayName;

  /**
   * Chat message sent time.
   */
  private Instant sentAt;
}
