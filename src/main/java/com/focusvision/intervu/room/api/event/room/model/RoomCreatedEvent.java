package com.focusvision.intervu.room.api.event.room.model;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.Project;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import java.util.Set;
import java.util.UUID;

/**
 * Model for room created event.
 *
 * @author Branko Ostojic
 */
public record RoomCreatedEvent(UUID roomId,
                               Project project,
                               ResearchSession researchSession,
                               Set<Participant> participants) {

}
