package com.focusvision.intervu.room.api.model.messaging;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the project sanitise request.
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ProjectSanitiseRequest {

  /**
   * Project platform ID.
   */
  private Long platformId;
}
