package com.focusvision.intervu.room.api.event.room.handler;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;

import com.focusvision.intervu.room.api.adapter.ResearchSessionStateUpdateSenderAdapter;
import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomStartedEvent;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionStateData;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingErrorEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingFinishedEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingStartedEvent;
import com.focusvision.intervu.room.api.service.ChatMessagesNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Handler responsible for sending notifications about session state change.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomStateUpdateHandler implements
    RoomStartedEventHandler,
    RoomFinishedEventHandler,
    RoomRecordingFinishedEventHandler,
    RoomRecordingErrorEventHandler {

  private final ResearchSessionStateUpdateSenderAdapter senderAdapter;
  private final ChatMessagesNotificationService chatMessagesNotificationService;

  @Override
  @EventListener
  public void handle(RoomStartedEvent event) {
    log.info("Sending room {} started event.", event.roomId());

    var stateUpdate = new ResearchSessionStateData()
        .setId(event.roomPlatformId())
        .setState(IN_PROGRESS)
        .setStartedAt(event.startedAt())
        .setConferenceId(event.conferenceId());

    senderAdapter.send(stateUpdate);
  }

  @Override
  @EventListener
  public void handle(RoomFinishedEvent event) {
    log.info("Sending room {} finished event.", event.roomId());

    var stateUpdate = new ResearchSessionStateData()
        .setId(event.roomPlatformId())
        .setState(FINISHED)
        .setStartedAt(event.startedAt())
        .setEndedAt(event.finishedAt())
        .setConferenceId(event.conferenceId());

    senderAdapter.send(stateUpdate);
    chatMessagesNotificationService.sendChatMessages(event.roomId());
  }

  @Override
  @EventListener
  public void handle(RecordingProcessingFinishedEvent event) {
    log.info("Sending room recording processing finished event (roomId: {})", event.roomId());

    var stateUpdate = new ResearchSessionStateData()
        .setId(event.roomPlatformId())
        .setState(FINISHED)
        .setStartedAt(event.startedAt())
        .setEndedAt(event.finishedAt())
        .setConferenceId(event.conferenceId())
        .setCompositionLayout(event.compositionLayout())
        .setAudioChannel(event.audioChannel())
        .setRecordingId(event.recordingId());

    senderAdapter.send(stateUpdate);
  }

  /**
   * Handles recording processing started event.
   *
   * @param event Event data.
   */
  @EventListener
  public void handle(RecordingProcessingStartedEvent event) {
    log.info("Sending room recording processing started event (roomId: {})", event.roomId());

    // TODO: Send message to proper RMQ queue
  }

  @Override
  @EventListener
  public void handle(RecordingProcessingErrorEvent event) {
    log.info("Sending room recording processing failed event (roomId: {})", event.roomId());

    var stateUpdate = new ResearchSessionStateData()
        .setId(event.roomPlatformId())
        .setState(FINISHED)
        .setStartedAt(event.startedAt())
        .setEndedAt(event.finishedAt())
        .setConferenceId(event.conferenceId())
        .setCompositionLayout(event.layout())
        .setAudioChannel(event.audioChannel())
        .setError(true);

    senderAdapter.send(stateUpdate);
  }
}
