package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.AudioChannel.NATIVE;
import static com.focusvision.intervu.room.api.common.model.AudioChannel.TRANSLATOR;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_ACTIVATED;
import static com.focusvision.intervu.room.api.model.CompositionLayout.GRID;
import static com.focusvision.intervu.room.api.model.CompositionLayout.PICTURE_IN_PICTURE_WITH_COLUMN;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.CompositionParameters;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.focusvision.intervu.room.api.service.CompositionParametersService;
import com.focusvision.intervu.room.api.service.StreamingService;
import com.focusvision.intervu.room.api.state.RoomStateProvider;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link CompositionParametersService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CompositionLayoutServiceImpl implements CompositionParametersService {

  private final RoomStateProvider roomStateProvider;
  private final StreamingService streamingService;

  @Override
  public List<CompositionParameters> resolveCompositionParams(
      Conference conference) {
    List<CompositionParameters> compositionParams = new ArrayList<>();
    var audioChannels = resolveAudioChannels(conference);

    resolveLayouts(conference).forEach(layout -> {
      audioChannels.forEach(
          audioChannel -> compositionParams.add(new CompositionParameters(layout, audioChannel)));
    });

    return compositionParams;
  }

  @Override
  public List<AudioChannel> resolveAudioChannels(Conference conference) {
    return switch (conference.getResearchSession().getAudioChannel()) {
      case NATIVE -> List.of(NATIVE);
      case TRANSLATOR, NATIVE_AND_TRANSLATOR -> hasTranslator(conference.getRoomSid())
          ? List.of(NATIVE, TRANSLATOR) : List.of(NATIVE);
    };
  }

  private List<CompositionLayout> resolveLayouts(Conference conference) {
    var roomId = conference.getResearchSession().getId();
    if (hasStimuli(roomId) || hasScreenshare(conference.getRoomSid())) {
      return List.of(PICTURE_IN_PICTURE_WITH_COLUMN, GRID);
    }

    return List.of(GRID);
  }

  private boolean hasTranslator(String roomSid) {
    return streamingService.getParticipantsRecordings(roomSid).stream()
        .anyMatch(ParticipantGroupRoomRecording::isTranslator);
  }

  private boolean hasStimuli(UUID roomId) {
    return roomStateProvider.getLogsForRoom(roomId).stream()
        .map(RoomStateLog::getChangeType)
        .anyMatch(STIMULUS_ACTIVATED::equals);
  }

  private boolean hasScreenshare(String roomSid) {
    return streamingService.getParticipantsRecordings(roomSid).stream()
        .anyMatch(ParticipantGroupRoomRecording::isScreenShare);
  }
}
