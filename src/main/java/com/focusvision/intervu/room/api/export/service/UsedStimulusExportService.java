package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.model.UsedStimulusExportDto;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.List;
import java.util.Optional;

/**
 * Service for exporting used {@link Stimulus} data.
 */
public interface UsedStimulusExportService {

  /**
   * Exports used stimuli data for given room.
   *
   * @param platformId Room platform ID.
   * @return List of used stimuli or empty {@link Optional} if room can't be found.
   */
  Optional<List<UsedStimulusExportDto>> export(String platformId);
}
