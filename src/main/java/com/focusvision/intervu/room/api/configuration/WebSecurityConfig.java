package com.focusvision.intervu.room.api.configuration;

import static com.focusvision.intervu.room.api.security.IntervuAuthority.INTERVU_USER;
import static com.focusvision.intervu.room.api.security.IntervuAuthority.ROOM_USER;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.exception.handler.CustomResponseEntityExceptionHandler;
import com.focusvision.intervu.room.api.security.IntervuUserAuthenticationFilter;
import com.focusvision.intervu.room.api.security.RestAuthenticationEntryPoint;
import com.focusvision.intervu.room.api.security.RoomAuthenticationFilter;
import com.focusvision.intervu.room.api.security.TokenProvider;
import com.focusvision.intervu.room.api.security.service.AuthenticationDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Security configuration class.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;
  private final TokenProvider tokenProvider;
  private final AuthenticationDetailsService authenticationDetailsService;
  private final ObjectMapper mapper;
  private final CustomResponseEntityExceptionHandler exceptionHandler;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf()
        .disable()
        .headers()
        .frameOptions()
        .sameOrigin()
        .and()
        .authorizeRequests()
        .antMatchers("/", "/error")
        .permitAll()
        .antMatchers("/actuator/**")
        .permitAll()
        .antMatchers("/twilio/**")
        .permitAll()
        .antMatchers("/waiting-room/chat")
        .authenticated()
        .antMatchers("/dashboard/**")
        .hasAuthority(INTERVU_USER.name())
        .antMatchers("/waiting-room/session/**")
        .hasAuthority(INTERVU_USER.name())
        .antMatchers("/recordings/**")
        .hasAuthority(INTERVU_USER.name())
        .antMatchers("/export/participants/**")
        .hasAuthority(INTERVU_USER.name())
        .antMatchers("/export/room/**")
        .hasAuthority(INTERVU_USER.name())
        .antMatchers("/room/**")
        .hasAuthority(ROOM_USER.name())
        .antMatchers("/participants/**")
        .hasAuthority(ROOM_USER.name())
        .antMatchers("/wip/**", "/waiting-room/*")
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .exceptionHandling()
        .authenticationEntryPoint(restAuthenticationEntryPoint)
        .and()
        .addFilterBefore(
            new RoomAuthenticationFilter(
                mapper, tokenProvider, authenticationDetailsService, exceptionHandler),
            BasicAuthenticationFilter.class)
        .addFilterBefore(
            new IntervuUserAuthenticationFilter(tokenProvider), RoomAuthenticationFilter.class);
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring()
        .antMatchers(
            "/",
            "/home",
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui/**",
            "/webjars/**");
  }
}
