package com.focusvision.intervu.room.api.state.stimulus.handler;

import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;

/**
 * Handler for the {@link PollStimulusVoteEvent}.
 */
public interface PollStimulusVoteEventHandler {

  /**
   * Handles the provided stimulus vote event.
   *
   * @param event Stimulus vote event.
   */
  void handle(PollStimulusVoteEvent event);
}
