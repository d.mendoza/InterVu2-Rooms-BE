package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.service.RoomStateNotificationService;
import com.focusvision.intervu.room.api.state.RoomStateChangedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Room state event handler.
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class RoomStateEventHandler implements RoomStateChangedEventHandler {

  private final RoomStateNotificationService roomStateNotificationService;

  /**
   * Handles room state change event.
   *
   * @param event Event data.
   */
  @EventListener
  public void handle(RoomStateChangedEvent event) {
    log.info("Handling room state changed event (event: {})", event);

    roomStateNotificationService.sendRoomStateChangeNotification(
        event.roomState(),
        event.eventType());
  }
}
