package com.focusvision.intervu.room.api.stimulus.snapshot;

import com.focusvision.intervu.room.api.common.event.EventPublisher;
import com.focusvision.intervu.room.api.common.provider.ResearchSessionProvider;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service used for stimulus snapshot related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusSnapshotService {

  private final EventPublisher eventPublisher;
  private final ResearchSessionProvider researchSessionProvider;
  private final RoomOffsetProvider roomOffsetProvider;
  private final ParticipantService participantService;
  private final StimulusService stimulusService;
  private final StimulusSnapshotRepository stimulusSnapshotRepository;

  /**
   * Adds a stimulus snapshot for specified stimulus.
   *
   * @param caller    Participant adding the snapshot.
   * @param name      Snapshot name.
   * @param data      Snapshot data.
   * @param mimeType  Snapshot data mime type.
   * @param timestamp Bookmark timestamp.
   * @return Created snapshot.
   */
  @Transactional
  public StimulusSnapshotDto addSnapshot(AuthenticatedParticipant caller,
                                         UUID stimulusId,
                                         String name,
                                         byte[] data,
                                         String mimeType,
                                         Long timestamp) {
    log.debug("Adding a snapshot (stimulusId: {}, roomId: {}, name: {})",
        stimulusId,
        caller.getRoomId(),
        name);

    var room = researchSessionProvider.get(caller.getRoomId());
    if (!room.isStarted()) {
      throw new IntervuRoomException("Room not running.");
    }
    var stimulus = stimulusService.find(stimulusId, room.getId())
        .orElseThrow(ResourceNotFoundException::new);
    var participant = participantService.get(caller.getId());

    var snapshot = createStimulusSnapshot(room,
        participant,
        stimulus,
        name,
        mimeType,
        data,
        timestamp);

    var snapshotAddedEvent = new SnapshotAddedEvent(room.getId(), caller, snapshot.getName());
    eventPublisher.publish(snapshotAddedEvent);

    return new StimulusSnapshotDto()
        .setId(snapshot.getId())
        .setName(snapshot.getName())
        .setTimestamp(timestamp);
  }

  private StimulusSnapshot createStimulusSnapshot(ResearchSession room,
                                                  Participant participant,
                                                  Stimulus stimulus,
                                                  String name,
                                                  String mimeType,
                                                  byte[] data,
                                                  Long timestamp) {
    var snapshot = new StimulusSnapshot()
        .setName(constructSnapshotNumberedName(name, stimulus))
        .setMimeType(mimeType)
        .setData(data)
        .setStartOffset(roomOffsetProvider.calculateOffset(room.getId(), timestamp))
        .setResearchSession(room)
        .setParticipant(participant)
        .setStimulus(stimulus);

    return stimulusSnapshotRepository.save(snapshot);
  }

  private String constructSnapshotNumberedName(String name, Stimulus stimulus) {
    var snapshotsCount = stimulusSnapshotRepository.countStimulusSnapshotByStimulus(stimulus);

    return String.format("%s - %s", name, ++snapshotsCount);
  }
}
