package com.focusvision.intervu.room.api.service.impl;

import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import com.focusvision.intervu.room.api.service.RoomStateService;
import com.focusvision.intervu.room.api.state.RoomStateOperator;
import com.focusvision.intervu.room.api.state.mapper.RoomStateMapper;
import com.focusvision.intervu.room.api.state.model.RoomState;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomStateService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomStateServiceImpl implements RoomStateService {

  private final RoomStateOperator roomStateOperator;
  private final RoomStateMapper roomStateMapper;

  @Override
  public RoomStateDto getRoomState(UUID roomId) {
    log.debug("Getting room {} state details.", roomId);

    RoomState roomState = roomStateOperator.getCurrentRoomState(roomId);

    return roomStateMapper.mapToDto(roomState);
  }
}
