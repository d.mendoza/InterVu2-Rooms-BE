package com.focusvision.intervu.room.api.scroll.repository;

import com.focusvision.intervu.room.api.scroll.model.ScrollLog;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for {@link ScrollLog} entity.
 */
public interface ScrollRepository extends JpaRepository<ScrollLog, UUID> {

  /**
   * Gets scroll logs for specified room ordered by the offset and creation date.
   *
   * @param roomId Room ID.
   * @return Matching scroll logs.
   */
  List<ScrollLog> findAllByRoomIdOrderByOffsetAscAddedAtAsc(UUID roomId);

  /**
   * Gets the latest scroll log for the specified room for the specified context.
   *
   * @param roomId    Room ID.
   * @param contextId Context ID.
   * @return Matching scroll log or empty {@link Optional}.
   */
  Optional<ScrollLog> findFirstByRoomIdAndContextIdOrderByOffsetDescAddedAtDesc(UUID roomId,
                                                                                UUID contextId);
}
