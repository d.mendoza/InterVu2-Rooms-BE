package com.focusvision.intervu.room.api.state.mapper;

import com.focusvision.intervu.room.api.model.dto.RoomStateLogDto;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import org.mapstruct.Mapper;

/**
 * Mapper for {@link RoomStateLog} entity.
 */
@Mapper(componentModel = "spring", uses = {RoomStateMapper.class})
public interface RoomStateLogMapper {

  /**
   * Maps room state log to DTO.
   *
   * @param roomStateLog Room state log to be mapped.
   * @return Resulting DTO.
   */
  RoomStateLogDto map(RoomStateLog roomStateLog);

}
