package com.focusvision.intervu.room.api.export.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Value;

/**
 * DTO representing the respondent room data for export (chat messages and recordings).
 */
@Value
@ApiModel("Participant Data Export")
public class ParticipantDataExportDto {

  @ApiModelProperty("Session ID")
  String sessionId;
  @ApiModelProperty("Participant's chat messages")
  List<ChatMessageExportDto> chatMessages;
  @ApiModelProperty("Participant's room recordings")
  List<ParticipantRecordingExportDto> roomRecordings;
}
