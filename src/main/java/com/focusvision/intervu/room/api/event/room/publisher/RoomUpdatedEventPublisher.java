package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.room.model.RoomUpdatedEvent;

/**
 * Room updated event publisher.
 */
public interface RoomUpdatedEventPublisher {

  void publish(RoomUpdatedEvent event);
}
