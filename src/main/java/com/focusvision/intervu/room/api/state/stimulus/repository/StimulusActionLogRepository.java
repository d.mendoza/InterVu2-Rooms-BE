package com.focusvision.intervu.room.api.state.stimulus.repository;

import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionLog;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for {@link StimulusActionLog} entity.
 */
public interface StimulusActionLogRepository extends JpaRepository<StimulusActionLog, UUID> {

  /**
   * Finds all stimulus action logs for provided stimulus.
   *
   * @param stimulusId Stimulus ID.
   * @return List of matching stimulus action logs.
   */
  List<StimulusActionLog> findByStimulusIdOrderByOffsetAscAddedAtAsc(UUID stimulusId);

  /**
   * Finds last action in log on stimulus with provided id.
   *
   * @param stimulusId Stimulus ID.
   * @return Matching action in log or empty {@link Optional}.
   */
  Optional<StimulusActionLog> findTop1ByStimulusIdOrderByOffsetDescAddedAtDesc(UUID stimulusId);
}
