package com.focusvision.intervu.room.api.stimulus.event.handler;

import com.focusvision.intervu.room.api.scroll.service.ScrollService;
import com.focusvision.intervu.room.api.state.RoomLeaderService;
import com.focusvision.intervu.room.api.stimulus.model.StimulusScrollEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * {@link ApplicationEvent} specific implementation of a stimulus scroll events handler for
 * persisting the scroll logs.
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class ApplicationStimulusScrollEventPersistenceHandler implements
    StimulusScrollEventHandler {

  private final ScrollService scrollService;
  private final RoomLeaderService roomLeaderService;

  @Override
  @EventListener
  public void handle(StimulusScrollEvent event) {
    log.debug("Handling stimulus scroll event: [{}].", event);

    if (roomLeaderService.isRoomLeader(event.roomId(), event.participantId())) {
      scrollService.saveStimulusScrollAction(
          event.roomId(), event.participantId(), event.stimulusId(), event.offset(),
          event.content());
    }
  }

}
