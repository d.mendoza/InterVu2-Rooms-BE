package com.focusvision.intervu.room.api.scroll.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the scroll log.
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ScrollLogDto {

  /**
   * Scroll action content.
   */
  private String content;
}
