package com.focusvision.intervu.room.api.model.event;

import static com.focusvision.intervu.room.api.model.event.EventType.PARTICIPANT_MICROPHONE_TOGGLE;

import com.focusvision.intervu.room.api.model.dto.ParticipantMicrophoneToggleDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the participant's microphone toggle event.
 *
 * @author Stefan Backovic (sbackovic@itekako.com)
 */
@Getter
@Setter
@ToString
@ApiModel("ParticipantMicrophoneToggleEvent")
public class ParticipantMicrophoneToggleEvent extends StateEvent<ParticipantMicrophoneToggleDto> {

  @ApiModelProperty("Event Type")
  private final EventType eventType = PARTICIPANT_MICROPHONE_TOGGLE;

  public ParticipantMicrophoneToggleEvent(ParticipantMicrophoneToggleDto data, String destination) {
    super(data, destination);
  }
}
