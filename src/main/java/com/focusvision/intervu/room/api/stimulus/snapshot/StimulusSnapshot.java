package com.focusvision.intervu.room.api.stimulus.snapshot;

import com.focusvision.intervu.room.api.model.entity.Auditable;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing stimulus snapshot.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "stimulus_snapshots")
@Getter
@Setter
@Accessors(chain = true)
public class StimulusSnapshot extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private String name;

  @Column
  private String mimeType;

  @Column
  private byte[] data;

  @Column
  @OrderBy
  private Long startOffset;

  @ManyToOne
  private Stimulus stimulus;

  @ManyToOne
  private Participant participant;

  @ManyToOne
  private ResearchSession researchSession;
}
