package com.focusvision.intervu.room.api.tracking.api;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;

import com.focusvision.intervu.room.api.tracking.model.RoomStateAckMessage;
import com.focusvision.intervu.room.api.tracking.service.RoomStateTrackingService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

/**
 * Controller responsible for room state tracking operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class RoomStateTrackingMessagingController {

  private final RoomStateTrackingService trackingService;

  /**
   * Handles room state ACK message.
   *
   * @param message Room state ACK message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("tracking/room-state/acknowledgment")
  public void ackRoomState(
      Principal principal,
      @Payload @Validated RoomStateAckMessage message) {
    log.debug("Receiving room state ack (message {}, sent by {})", message, principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    trackingService.ackRoomState(sender.getId(), sender.getRoomId(), message.getVersion());
  }
}
