package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantRemovedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomCanceledEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomCreatedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomExpiredEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomStartedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomUpdatedEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingErrorEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingErrorEventPublisher;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingFinishedEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingFinishedEventPublisher;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingStartedEvent;
import com.focusvision.intervu.room.api.recording.event.RecordingProcessingStartedEventPublisher;
import com.focusvision.intervu.room.api.state.RoomStateChangedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * Room event publisher.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomEventPublisher implements
    RoomStateChangedEventPublisher,
    RoomCreatedEventPublisher,
    RoomUpdatedEventPublisher,
    RoomCanceledEventPublisher,
    RoomStartedEventPublisher,
    RoomFinishedEventPublisher,
    RoomExpiredEventPublisher,
    RecordingProcessingStartedEventPublisher,
    RecordingProcessingFinishedEventPublisher,
    RecordingProcessingErrorEventPublisher,
    ParticipantsRemovedEventPublisher {

  private final ApplicationEventPublisher publisher;

  @Override
  public void publish(RoomStateChangedEvent event) {
    log.debug("Publishing room state changed event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RoomCreatedEvent event) {
    log.debug("Publishing room created event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RoomStartedEvent event) {
    log.debug("Publishing room started  event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RoomFinishedEvent event) {
    log.debug("Publishing room finished event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RoomCanceledEvent event) {
    log.debug("Publishing room canceled event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RoomExpiredEvent event) {
    log.debug("Publishing room expired event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RoomUpdatedEvent event) {
    log.debug("Publishing room updated event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RecordingProcessingStartedEvent event) {
    log.debug("Publishing recording processing started event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RecordingProcessingFinishedEvent event) {
    log.debug("Publishing recording processing finished event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(RecordingProcessingErrorEvent event) {
    log.debug("Publishing recording processing error event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantRemovedEvent event) {
    log.debug("Publishing participants removed event: {}", event);

    publisher.publishEvent(event);
  }
}
