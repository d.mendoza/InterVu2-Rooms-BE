package com.focusvision.intervu.room.api.model.entity;

import com.focusvision.intervu.room.api.common.model.ChatType;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing chat message tracking.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "chat_messages_tracking")
@Getter
@Setter
@Accessors(chain = true)
public class ChatMessageTrack {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  @Column
  @Enumerated(EnumType.STRING)
  private ChatMessage.ChatSource source;
  @Column
  @Enumerated(EnumType.STRING)
  private ChatType type;
  @Column
  private UUID chatMessageId;
  @Column
  private UUID handle;
  @Column
  private UUID participantId;
  @Column
  private Instant seenAt;

}
