package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CREATED;

import com.focusvision.intervu.room.api.adapter.StateNotificationSenderAdapter;
import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.model.dto.ParticipantMicrophoneToggleDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.model.event.ParticipantMicrophoneToggleEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRemovedEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRoomStateEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantStateEvent;
import com.focusvision.intervu.room.api.model.event.RoomStateEvent;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.service.RoomStateNotificationService;
import com.focusvision.intervu.room.api.state.mapper.RoomStateMapper;
import com.focusvision.intervu.room.api.state.model.ParticipantState;
import com.focusvision.intervu.room.api.state.model.RoomState;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomStateNotificationService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomStateNotificationServiceImpl implements RoomStateNotificationService {

  private final StateNotificationSenderAdapter sender;
  private final CommunicationChannelService communicationChannelService;
  private final RoomStateMapper roomStateMapper;
  private final ParticipantStateService participantStateService;

  @Override
  public void sendRoomStateChangeNotification(RoomState roomState, RoomStateChangeType type) {
    log.debug("Sending room {} state change notification.", roomState.getRoomId());

    try {
      var roomStateDetails = roomStateMapper.mapToDto(roomState);
      sender.send(RoomStateEvent.builder()
          .eventType(type)
          .destination(roomState.getRoomChannel())
          .data(roomStateDetails)
          .build());
    } catch (Exception e) {
      log.error("Failed to send room state notification for room {}", roomState.getRoomId(), e);
    }
  }

  @Override
  public void sendParticipantStateNotification(ParticipantState participantState) {
    log.debug("Sending participant state notification to participant {}.",
        participantState.getId());

    var participantChannel =
        communicationChannelService.getParticipantChannel(participantState.getId());
    var participantStateDto = participantStateService.getParticipantState(participantState);
    var stateEvent = new ParticipantStateEvent(participantStateDto, participantChannel);
    sender.send(stateEvent);
  }

  @Override
  public void sendParticipantMicrophoneToggleNotification(UUID participantId) {
    log.debug("Sending participant microphone toggle notification to participant {}.",
        participantId);

    var data = new ParticipantMicrophoneToggleDto();
    var participantChannel = communicationChannelService.getParticipantChannel(participantId);
    sender.send(new ParticipantMicrophoneToggleEvent(data, participantChannel));
  }

  @Override
  public void sendNewRoomNotification(RoomState roomState,
                                      List<ParticipantState> participantStates) {
    log.debug("Sending new room {} details notification.", roomState.getRoomId());

    var roomStateData = roomStateMapper.mapToDto(roomState);

    participantStates.stream()
        .map(roomStateMapper::mapToDto)
        .map(participantStateDto -> new ParticipantRoomStateDto()
            .setRoomState(roomStateData)
            .setParticipantState(participantStateDto))
        .forEach(data -> {
          var channel = communicationChannelService
              .getParticipantGlobalChannel(data.getParticipantState().getParticipantPlatformId());
          sender.send(ParticipantRoomStateEvent.builder()
              .eventType(ROOM_CREATED)
              .destination(channel)
              .data(data)
              .build());
        });
  }

  @Override
  public void sendParticipantRemovedNotification(UUID roomId, UUID participantId,
                                                 String participantPlatformId) {
    log.debug("Sending participant removed notification for room {} and participant {}", roomId,
        participantId);

    var destination =
        communicationChannelService.getParticipantGlobalChannel(participantPlatformId);
    sender.send(ParticipantRemovedEvent.builder()
        .roomId(roomId)
        .participantId(participantId)
        .destination(destination)
        .build());
  }
}
