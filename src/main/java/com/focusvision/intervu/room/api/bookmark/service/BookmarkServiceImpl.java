package com.focusvision.intervu.room.api.bookmark.service;

import static com.focusvision.intervu.room.api.common.model.BookmarkType.NOTE;
import static com.focusvision.intervu.room.api.common.model.BookmarkType.PII;

import com.focusvision.intervu.room.api.bookmark.event.model.BookmarkAddedEvent;
import com.focusvision.intervu.room.api.bookmark.event.model.PiiBookmarkAddedEvent;
import com.focusvision.intervu.room.api.bookmark.mapper.BookmarkMapper;
import com.focusvision.intervu.room.api.common.event.EventPublisher;
import com.focusvision.intervu.room.api.common.model.BookmarkType;
import com.focusvision.intervu.room.api.common.provider.ResearchSessionProvider;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.entity.Bookmark;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.repository.BookmarkRepository;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link BookmarkService}.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BookmarkServiceImpl implements BookmarkService {

  private final BookmarkRepository bookmarkRepository;
  private final ResearchSessionProvider researchSessionProvider;
  private final ParticipantService participantService;
  private final EventPublisher eventPublisher;
  private final RoomOffsetProvider roomOffsetProvider;
  private final BookmarkMapper bookmarkMapper;

  @Override
  public Bookmark addBookmark(AuthenticatedParticipant caller,
                              String note,
                              Long timestamp) {
    log.debug("Adding a bookmark {} for room {} by {}", note, caller.getRoomId(), caller.getId());

    var room = researchSessionProvider.get(caller.getRoomId());
    var participant = participantService.findById(caller.getId()).orElseThrow();
    if (!room.isStarted()) {
      throw new IntervuRoomException("Room not running.");
    }

    var newBookmark = createBookmark(room, participant, note, timestamp, NOTE);

    var bookmarkDetails = bookmarkMapper.mapToDetails(newBookmark);
    var bookmarkAddedEvent = new BookmarkAddedEvent(room.getId(), caller, bookmarkDetails);
    eventPublisher.publish(bookmarkAddedEvent);

    return newBookmark;
  }

  @Override
  public List<Bookmark> getAllBookmarks(UUID roomId) {
    log.debug("Getting all bookmarks for room {}.", roomId);

    var room = researchSessionProvider.get(roomId);
    return bookmarkRepository.findAllByResearchSession(room);
  }

  @Override
  public List<Bookmark> getAllBookmarksWithAlignedOffset(ResearchSession researchSession,
                                                         Integer recordingOffset) {
    log.debug("Getting all bookmarks for room {} with aligned offset.", researchSession.getId());

    return bookmarkRepository.findAllByResearchSession(researchSession).stream()
        .map(bookmark -> this.alignWithRecordingOffset(bookmark, recordingOffset))
        .toList();
  }

  @Override
  public Bookmark addPiiBookmark(AuthenticatedParticipant caller, String note, Long timestamp) {
    log.debug("Adding PII bookmark {} for room {} by {}", note, caller.getRoomId(), caller.getId());

    var room = researchSessionProvider.get(caller.getRoomId());
    var participant = participantService.get(caller.getId());
    if (!room.isStarted()) {
      throw new IntervuRoomException("Room not running.");
    }

    var bookmark = createBookmark(room, participant, note, timestamp, PII);
    var piiBookmarkDetails = bookmarkMapper.mapToPiiDetails(bookmark);
    var piiBookmarkAddedEvent = new PiiBookmarkAddedEvent(room.getId(), caller, piiBookmarkDetails);

    eventPublisher.publish(piiBookmarkAddedEvent);

    return bookmark;
  }

  private Bookmark alignWithRecordingOffset(Bookmark bookmark,
                                            Integer recordingOffset) {
    long alignedOffset = roomOffsetProvider
        .alignWithRecordingOffset(bookmark.getStartOffset(), recordingOffset);

    return bookmark.setStartOffset(alignedOffset);
  }

  private Bookmark createBookmark(ResearchSession room,
                                  Participant participant,
                                  String note,
                                  Long timestamp,
                                  BookmarkType type) {
    var bookmark = new Bookmark()
        .setNote(note)
        .setStartOffset(roomOffsetProvider.calculateOffset(room.getId(), timestamp))
        .setResearchSession(room)
        .setParticipant(participant)
        .setType(type);

    return bookmarkRepository.save(bookmark);
  }

}
