package com.focusvision.intervu.room.api.model.event;

import static com.focusvision.intervu.room.api.model.event.EventType.PARTICIPANT_ROLE_CHANGED;

import com.focusvision.intervu.room.api.model.dto.ParticipantMicrophoneToggleDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoleChangeDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the participant's role changed event.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@ApiModel("ParticipantRoleChangedEvent")
public class ParticipantRoleChangedEvent extends StateEvent<ParticipantRoleChangeDto> {

  @ApiModelProperty("Event Type")
  private final EventType eventType = PARTICIPANT_ROLE_CHANGED;

  public ParticipantRoleChangedEvent(ParticipantRoleChangeDto data, String destination) {
    super(data, destination);
  }
}
