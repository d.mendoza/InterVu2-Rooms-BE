package com.focusvision.intervu.room.api.model.messaging;

import static java.time.Instant.now;

import java.time.Instant;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * DTO representing the research session data processing results.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@Accessors(chain = true)
public class ResearchSessionProcessResult {

  /**
   * Processing status.
   */
  public enum Status {
    SUCCESS, FAILED
  }

  private ResearchSessionData researchSessionData;
  private Status status;
  private String message;
  private Instant processedAt;

  /**
   * Construct success response.
   *
   * @param researchSessionData Research session data.
   * @return Processing result.
   */
  public static ResearchSessionProcessResult success(ResearchSessionData researchSessionData) {
    return new ResearchSessionProcessResult()
        .setResearchSessionData(researchSessionData)
        .setStatus(Status.SUCCESS)
        .setProcessedAt(now());
  }

  /**
   * Construct failure response.
   *
   * @param researchSessionData Research session data.
   * @return Processing result.
   */
  public static ResearchSessionProcessResult fail(ResearchSessionData researchSessionData,
                                                  String message) {
    return new ResearchSessionProcessResult()
        .setResearchSessionData(researchSessionData)
        .setStatus(Status.FAILED)
        .setMessage(message)
        .setProcessedAt(now());
  }

}
