package com.focusvision.intervu.room.api.state;

import static java.lang.Math.max;
import static java.time.Instant.now;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomStateProvider} and {@link RoomOffsetProvider}.
 * It acts as a proxy service for {@link RoomStateLogService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomStateProviderImpl implements RoomStateProvider, RoomOffsetProvider {

  private final RoomStateLogService roomStateLogService;

  @Override
  public RoomStateLog getCurrentForRoom(UUID roomId) {
    log.debug("Getting room {} current state.", roomId);

    return roomStateLogService.getCurrentForRoom(roomId);
  }

  @Override
  public Optional<RoomStateLog> findCurrentForRoom(UUID roomId) {
    log.debug("Finding room {} current state.", roomId);

    return roomStateLogService.findCurrentForRoom(roomId);
  }

  @Override
  public RoomStateLog save(RoomState roomState, RoomStateChangeType changeType) {
    log.debug("Saving room state (roomState: {})", roomState);

    long offset = this.calculateOffset(roomState.getRoomId());

    return roomStateLogService.save(roomState, changeType, offset);
  }

  @Override
  public List<RoomStateLog> getLogsForRoom(UUID roomId) {
    log.debug("Getting room {} change logs.", roomId);

    return roomStateLogService.getLogsForRoom(roomId);
  }

  @Override
  public List<RoomStateLog> getLastRoomFinishedAndParticipantOfflineLogs(UUID roomId) {
    log.debug(
        "Getting the last two ROOM_FINISHED and PARTICIPANT_OFFLINE room state logs for room {}",
        roomId);

    return roomStateLogService.getLastRoomFinishedAndParticipantOfflineLogs(roomId);
  }

  @Override
  public Long calculateOffset(UUID roomId) {
    return this.calculate(roomId, now().toEpochMilli());
  }

  @Override
  public Long calculateOffset(UUID roomId, long timestamp) {
    return this.calculate(roomId, timestamp);
  }

  private Long calculate(UUID roomId, long timestamp) {
    return roomStateLogService.findRoomStartedLogForRoom(roomId)
        .map(RoomStateLog::getAddedAt)
        .map(Instant::toEpochMilli)
        .map(addedAt -> timestamp - addedAt)
        .orElse(0L);
  }

  @Override
  public Long alignWithRecordingOffset(Long startOffset, Integer recordingOffset) {
    return max(startOffset - recordingOffset, 0);
  }

}
