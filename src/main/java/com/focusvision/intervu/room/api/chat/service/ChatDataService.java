package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.chat.model.ChatHistoryDto;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.List;
import java.util.UUID;

/**
 * Service used for chat messaging related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatDataService {

  /**
   * Saves waiting room private chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveWaitingRoomPrivateMessage(String message,
                                            UUID senderId,
                                            String senderPlatformId,
                                            String senderDisplayName,
                                            UUID handle,
                                            UUID roomId);

  /**
   * Saves waiting room respondents chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveWaitingRoomRespondentsMessage(String message,
                                                UUID senderId,
                                                String senderPlatformId,
                                                String senderDisplayName,
                                                UUID handle,
                                                UUID roomId);

  /**
   * Saves dashboard internal chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveWaitingRoomInternalMessage(String message,
                                             UUID senderId,
                                             String senderPlatformId,
                                             String senderDisplayName,
                                             UUID handle,
                                             UUID roomId);

  /**
   * Saves meeting room private chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveMeetingRoomPrivateMessage(String message,
                                            UUID senderId,
                                            String senderPlatformId,
                                            String senderDisplayName,
                                            UUID handle,
                                            UUID roomId);

  /**
   * Saves meeting room internal chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveMeetingRoomInternalMessage(String message,
                                             UUID senderId,
                                             String senderPlatformId,
                                             String senderDisplayName,
                                             UUID handle,
                                             UUID roomId);

  /**
   * Saves meeting room respondents chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveMeetingRoomRespondentsMessage(String message,
                                                UUID senderId,
                                                String senderPlatformId,
                                                String senderDisplayName,
                                                UUID handle,
                                                UUID roomId);

  /**
   * Returns a collection of all waiting room internal chat messages for provided handle.
   *
   * @param caller Participant requesting the data.
   * @return Chat history.
   */
  ChatHistoryDto getWaitingRoomInternalChatMessages(AuthenticatedParticipant caller);

  /**
   * Returns a collection of all meeting room internal chat messages for provided handle.
   *
   * @param caller Participant requesting the data.
   * @return Chat history.
   */
  ChatHistoryDto getMeetingRoomInternalChatMessages(AuthenticatedParticipant caller);

  /**
   * Returns a collection of all waiting room respondents type chat messages for provided handle.
   *
   * @param caller Participant requesting the data.
   * @return Chat history.
   */
  ChatHistoryDto getWaitingRoomRespondentsChatMessages(AuthenticatedParticipant caller);

  /**
   * Returns a collection of all meeting room respondents type chat messages for provided handle.
   *
   * @param caller Participant requesting the data.
   * @return Chat history.
   */
  ChatHistoryDto getMeetingRoomRespondentsChatMessages(AuthenticatedParticipant caller);

  /**
   * Returns a collection of all meeting room private chat messages for specified participants.
   *
   * @param caller            Participant requesting the data.
   * @param secondParticipant Second participant.
   * @return Chat history.
   */
  ChatHistoryDto getMeetingRoomPrivateChatMessages(AuthenticatedParticipant caller,
                                                   UUID secondParticipant);

  /**
   * Returns a collection of all waiting room private chat messages for specified participants.
   *
   * @param caller            Participant requesting the data.
   * @param secondParticipant Second participant.
   * @return Chat history.
   */
  ChatHistoryDto getWaitingRoomPrivateChatMessages(AuthenticatedParticipant caller,
                                                   UUID secondParticipant);

  /**
   * Deletes the chat history of a participant.
   *
   * @param participant Participant.
   */
  void clearUserHistory(Participant participant);

  /**
   * Returns a collection of all chat messages for provided handle and sender platform ID.
   *
   * @param handle           Chat messages handle.
   * @param senderPlatformId Sender platform ID.
   * @return Collection of matching chat messages.
   */
  List<ChatMessage> getRespondentChatMessages(UUID handle, String senderPlatformId);

  /**
   * Saves meeting room internal bookmark chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveMeetingRoomBookmarkMessage(String message,
                                             UUID senderId,
                                             String senderPlatformId,
                                             String senderDisplayName,
                                             UUID handle,
                                             UUID roomId);

  /**
   * Saves meeting room internal PII chat message.
   *
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveMeetingRoomPiiMessage(UUID senderId,
                                        String senderPlatformId,
                                        String senderDisplayName,
                                        UUID handle,
                                        UUID roomId);

  /**
   * Saves meeting room internal snapshot chat message.
   *
   * @param message           Chat message content.
   * @param senderId          Sender ID.
   * @param senderPlatformId  Chat message sender platform ID.
   * @param senderDisplayName Chat message sender display name.
   * @param handle            Chat message handle.
   * @param roomId            Room ID.
   * @return Persisted chat message.
   */
  ChatMessage saveMeetingRoomSnapshotMessage(String message,
                                             UUID senderId,
                                             String senderPlatformId,
                                             String senderDisplayName,
                                             UUID handle,
                                             UUID roomId);
}
