package com.focusvision.intervu.room.api.cache.room.log.event;

import java.util.UUID;

/**
 * Model for clear room log cache event.
 *
 * @author Branko Ostojic
 */
public record ClearRoomLogCacheEvent(UUID roomId) {

}
