package com.focusvision.intervu.room.api.export.api;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.export.model.StimulusActionLogExportDto;
import com.focusvision.intervu.room.api.export.model.StimulusSnapshotExportDto;
import com.focusvision.intervu.room.api.export.model.UsedStimulusExportDto;
import com.focusvision.intervu.room.api.export.service.StimulusActionExportService;
import com.focusvision.intervu.room.api.export.service.StimulusSnapshotExportService;
import com.focusvision.intervu.room.api.export.service.UsedStimulusExportService;
import com.focusvision.intervu.room.api.security.IntervuUser;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for stimuli data export operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "export/room", produces = MediaType.APPLICATION_JSON_VALUE)
public class StimulusExportController {

  private final StimulusActionExportService stimulusActionExportService;
  private final StimulusSnapshotExportService stimulusSnapshotExportService;
  private final UsedStimulusExportService usedStimulusExportService;

  /**
   * Exports stimulus action log.
   *
   * @param platformId Room platform ID.
   * @param caller     Authenticated user.
   * @return Stimulus actions log.
   */
  @GetMapping("{platformId}/stimuli")
  @ApiOperation(
      value = "Gets stimulus action log.",
      notes = "Gets stimulus action log.")
  public List<StimulusActionLogExportDto> stimulusActionLogs(
      @PathVariable String platformId,
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Exporting room {} stimuli action log data.", platformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return stimulusActionExportService.export(platformId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  /**
   * Exports stimulus snapshots.
   *
   * @param roomPlatformId Room platform ID
   * @param caller         Authenticated user.
   * @return Stimuli snapshots.
   */
  @GetMapping("{roomPlatformId}/stimuli/snapshots")
  @ApiOperation(
      value = "Gets stimuli snapshots.",
      notes = "Gets stimuli snapshots.")
  public List<StimulusSnapshotExportDto> stimuliSnapshots(
      @PathVariable String roomPlatformId,
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Exporting stimulus snapshots (roomPlatformId: {})", roomPlatformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return stimulusSnapshotExportService.export(roomPlatformId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  /**
   * Exports used stimuli data for given room.
   *
   * @param platformId Room platform ID.
   * @param caller     Authenticated user.
   * @return Matching stimuli data.
   */
  @GetMapping("{platformId}/stimuli/used")
  @ApiOperation(
      value = "Gets used stimuli.",
      notes = "Gets used stimuli.")
  public List<UsedStimulusExportDto> usedStimuli(
      @PathVariable String platformId,
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Exporting used stimuli for room platform ID: {}.", platformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return usedStimulusExportService.export(platformId)
        .orElseThrow(ResourceNotFoundException::new);
  }
}
