package com.focusvision.intervu.room.api.model.messaging;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the banned participant.
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString
public class BannedParticipantData {

  /**
   * Participant platform ID.
   */
  private String platformId;

  /**
   * Participant role.
   */
  private String role;

  /**
   * Research session platform ID.
   */
  private String sessionPlatformId;
}
