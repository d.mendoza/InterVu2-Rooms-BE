package com.focusvision.intervu.room.api.recording.infra;

import static java.lang.String.format;
import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.advisor.ClusterLock;
import com.focusvision.intervu.room.api.model.messaging.ProcessPendingRecordingRequest;
import com.focusvision.intervu.room.api.recording.service.RecordingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ specific implementation of {@link ProcessPendingRecordingRequest} consumer.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProcessPendingRecordingRequestConsumer {

  private final RecordingService recordingService;

  /**
   * Handles  process pending recording request message.
   *
   * @param request Event payload.
   */
  @ClusterLock
  @RabbitListener(
      queues = "#{processPendingRecordingQueue.name}",
      containerFactory = "researchSessionContainerFactory")
  public void consume(ProcessPendingRecordingRequest request) {
    log.debug("Handling process pending recording request (request: {})", request);

    try {
      recordingService.startProcessing(fromString(request.getRecordingId()));
    } catch (Exception e) {
      log.error(format("Error Handling process pending recording request %s", request), e);
    }
  }
}
