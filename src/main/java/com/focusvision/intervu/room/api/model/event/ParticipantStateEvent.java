package com.focusvision.intervu.room.api.model.event;

import static com.focusvision.intervu.room.api.model.event.EventType.PARTICIPANT_STATE;

import com.focusvision.intervu.room.api.model.dto.ParticipantStateDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the participant state event.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@ApiModel("ParticipantStateEvent")
public class ParticipantStateEvent extends StateEvent<ParticipantStateDto> {

  @ApiModelProperty("Event Type")
  private final EventType eventType = PARTICIPANT_STATE;

  public ParticipantStateEvent(ParticipantStateDto data, String destination) {
    super(data, destination);
  }
}
