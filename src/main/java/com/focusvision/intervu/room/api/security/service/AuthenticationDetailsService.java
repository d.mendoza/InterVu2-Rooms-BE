package com.focusvision.intervu.room.api.security.service;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.Optional;

/**
 * Service for authenticating participants.
 *
 * @author Branko Ostojic
 */
public interface AuthenticationDetailsService {

  /**
   * Gets the details for authenticating participant.
   *
   * @param id Participant ID.
   * @return Authenticated participant details
   */
  Optional<AuthenticatedParticipant> getDetails(String id);
}
