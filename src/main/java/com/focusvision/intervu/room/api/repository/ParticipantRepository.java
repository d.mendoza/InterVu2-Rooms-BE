package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.model.entity.Participant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link Participant} entity.
 *
 * @author Branko Ostojic
 */
public interface ParticipantRepository extends JpaRepository<Participant, UUID> {

  /**
   * Finds participant by the invitation token.
   *
   * @param invitationToken Participant's Invitation token.
   * @return Participant matching the token or empty {@link Optional}.
   */
  Optional<Participant> findByInvitationToken(String invitationToken);

  /**
   * Finds participant with provided platform ID and belonging to the provided research session.
   *
   * @param researchSessionId Research Session ID.
   * @param platformId        Participant's platform ID.
   * @return Matching participant or empty {@link Optional}.
   */
  Optional<Participant> findByResearchSessionIdAndPlatformId(UUID researchSessionId,
                                                             String platformId);

  /**
   * Finds participant with provided platform ID and belonging to the provided research session.
   *
   * @param researchSessionPlatformId Research Session platform ID.
   * @param platformId                Participant's platform ID.
   * @return Matching participant or empty {@link Optional}.
   */
  Optional<Participant> findByResearchSessionPlatformIdAndPlatformId(
      String researchSessionPlatformId, String platformId);

  /**
   * Finds participant with provided ID and belonging to the provided research session.
   *
   * @param researchSessionId Research Session ID.
   * @param id                Participant's ID.
   * @return Matching participant or empty {@link Optional}.
   */
  Optional<Participant> findByResearchSessionIdAndId(UUID researchSessionId, UUID id);

  /**
   * Finds participants belonging to the provided research session.
   *
   * @param researchSessionId Research Session ID.
   * @return Matching participants.
   */
  List<Participant> findByResearchSessionId(UUID researchSessionId);

  /**
   * Finds participant by platform ID.
   *
   * @param platformId Participant platform ID.
   * @return Matching participant or empty {@link Optional}.
   */
  List<Participant> findByPlatformId(String platformId);

  /**
   * Finds participant by its speaker PIN.
   *
   * @param pin Participant speaker PIN.
   * @return Participant details or empty {@link Optional}.
   */
  Optional<Participant> findBySpeakerPin(String pin);
}
