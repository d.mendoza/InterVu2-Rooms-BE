package com.focusvision.intervu.room.api.tracking.model.entity;

import com.focusvision.intervu.room.api.model.entity.Auditable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing room state ACK.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "room_state_acknowledgments")
@Getter
@Setter
@Accessors(chain = true)
public class RoomStateAcknowledgement extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  private UUID roomId;
  private UUID participantId;
  private Long version;

  @Column(name = "start_offset")
  private Long offset;
}
