package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.event.room.model.RoomExpiredEvent;

/**
 * Room expired event handler.
 */
public interface RoomExpiredEventHandler {

  void handle(RoomExpiredEvent event);
}
