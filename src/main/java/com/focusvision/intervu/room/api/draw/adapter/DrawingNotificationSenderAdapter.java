package com.focusvision.intervu.room.api.draw.adapter;

import com.focusvision.intervu.room.api.draw.model.DrawingNotificationMessage;

/**
 * Adapter for sending drawing notifications.
 *
 * @author Branko Ostojic
 */
public interface DrawingNotificationSenderAdapter {

  /**
   * Sends the provided drawing notification to specified destination.
   *
   * @param destination Message destination.
   * @param message     Drawing notification message to be sent.
   */
  void send(String destination, DrawingNotificationMessage message);
}
