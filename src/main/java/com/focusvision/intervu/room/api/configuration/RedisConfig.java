package com.focusvision.intervu.room.api.configuration;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Redis configuration.
 */
@Configuration
public class RedisConfig {

  @Value("${spring.redis.ssl}")
  private boolean redisSsl;
  @Value("${spring.redis.host}")
  private String redisHost;
  @Value("${spring.redis.port}")
  private Integer redisPort;
  @Value("${spring.redis.password}")
  private String redisPassword;

  @Bean
  RedissonClient redissonClient() {
    Config config = new Config();
    config.useSingleServer()
        .setAddress(
            String.format("%s://%s:%s", redisSsl ? "rediss" : "redis", redisHost, redisPort))
        .setPassword(redisPassword);

    return Redisson.create(config);
  }

}
