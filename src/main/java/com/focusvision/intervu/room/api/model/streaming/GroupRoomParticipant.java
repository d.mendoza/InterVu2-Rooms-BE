package com.focusvision.intervu.room.api.model.streaming;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Model for group room participant.
 *
 * @author Branko Ostojic
 */
@Getter
@RequiredArgsConstructor
public class GroupRoomParticipant {

  /**
   * Group room participant SID.
   */
  private final String sid;

  /**
   * Group room participant token.
   */
  private final String token;
}
