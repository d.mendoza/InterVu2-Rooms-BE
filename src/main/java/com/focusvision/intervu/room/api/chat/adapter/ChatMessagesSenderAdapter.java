package com.focusvision.intervu.room.api.chat.adapter;

import com.focusvision.intervu.room.api.model.messaging.ResearchSessionChatMessagesData;

/**
 * Adapter for sending research session's chat messages.
 */
public interface ChatMessagesSenderAdapter {

  /**
   * Sends research session's chat messages.
   *
   * @param researchSessionChatMessagesData Research session chat messages data.
   */
  void send(ResearchSessionChatMessagesData researchSessionChatMessagesData);
}
