package com.focusvision.intervu.room.api.draw.model;

import com.focusvision.intervu.room.api.common.model.DrawingActionType;
import com.focusvision.intervu.room.api.common.model.DrawingContextType;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing room state.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "drawing_logs")
@Getter
@Setter
@Accessors(chain = true)
public class DrawingLog {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  private UUID roomId;

  private UUID participantId;

  @Column(name = "start_offset")
  private Long offset;

  @Enumerated(EnumType.STRING)
  private DrawingActionType actionType;

  @Enumerated(EnumType.STRING)
  private DrawingContextType contextType;

  private UUID contextId;

  private String content;

  private Instant addedAt;
}
