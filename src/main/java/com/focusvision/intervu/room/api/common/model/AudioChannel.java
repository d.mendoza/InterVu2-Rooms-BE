package com.focusvision.intervu.room.api.common.model;

/**
 * Enum that represents audio channel type.
 *
 * @author Branko Ostojic
 */
public enum AudioChannel {
  /**
   * Native audio channel.
   */
  NATIVE,
  /**
   * Translator audio channel.
   */
  TRANSLATOR,
  /**
   * Native and translator audio channel.
   */
  NATIVE_AND_TRANSLATOR,

}
