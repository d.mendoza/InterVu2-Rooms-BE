package com.focusvision.intervu.room.api.controller;

import static java.util.UUID.fromString;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.RoomService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for participant related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/participants", produces = MediaType.APPLICATION_JSON_VALUE)
public class ParticipantController {

  private final RoomService roomService;

  /**
   * Removes participant from the session.
   *
   * @param id     Participant ID.
   * @param caller Authenticated participant.
   */
  @DeleteMapping("{id}")
  @ResponseStatus(code = NO_CONTENT)
  @ApiOperation(
      value = "Removes participant from session",
      notes = "Removes participant from session in progress")
  @PreAuthorize("hasAuthority('PARTICIPANT_CONTROL')")
  public void banParticipant(@ApiParam("participant ID") @PathVariable String id,
                             @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Banning participant {} from session {}", id, caller.getRoomId());

    roomService.banParticipant(caller, fromString(id));
  }

  /**
   * Toggle participant's microphone.
   *
   * @param id     Participant ID.
   * @param caller Authenticated participant.
   */
  @PutMapping("{id}")
  @ResponseStatus(code = NO_CONTENT)
  @ApiOperation(
      value = "Toggle participant's microphone",
      notes = "Toggle participant's microphone in the running session")
  @PreAuthorize("hasAuthority('PARTICIPANT_CONTROL')")
  public void toggleParticipantMicrophone(@ApiParam("participant ID") @PathVariable String id,
                                          @ApiIgnore @AuthenticationPrincipal
                                              AuthenticatedParticipant caller) {
    log.debug("Toggle microphone of participant {} in the session {}", id, caller.getRoomId());

    roomService.toggleParticipantMicrophone(caller, fromString(id));
  }
}
