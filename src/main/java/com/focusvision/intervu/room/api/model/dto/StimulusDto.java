package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.StimulusType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the stimulus.
 */
@Getter
@Setter
@ToString
@ApiModel("Stimulus")
public class StimulusDto {

  @ApiModelProperty("Stimulus ID")
  private UUID id;
  @ApiModelProperty("Stimulus platform ID")
  private String platformId;
  @ApiModelProperty("Stimulus name")
  private String name;
  @ApiModelProperty("Stimulus data")
  private String data;
  @ApiModelProperty("Stimulus position")
  private Integer position;
  @ApiModelProperty("Stimulus type")
  private StimulusType type;
  @ApiModelProperty("Broadcast results")
  private boolean broadcastResults;
  @ApiModelProperty("Stimulus interaction flag")
  private Boolean interactionEnabled;
  @ApiModelProperty("Stimulus sync flag")
  private Boolean syncEnabled;
}
