package com.focusvision.intervu.room.api.twilio.service;

import static java.lang.String.format;
import static java.util.Optional.of;
import static java.util.UUID.randomUUID;

import java.util.Optional;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Twilio participant's identity provider.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
public class TwilioIdentityProvider {

  private static final String SPEAKER_PREFIX = "speaker_";
  private static final String LISTENER_PREFIX = "listener_";
  private static final String OPERATOR_PREFIX = "operator_";
  private static final String ANONYMOUS_IDENTITY_FORMAT = "%s%s_%s";

  /**
   * Generates anonymous listener identity. Each time generate is invoked it will result in a new
   * identity, even for the same sessionId.
   *
   * @param sessionId Session ID.
   * @return Generated identity
   */
  public String generateAnonymousListenerIdentity(UUID sessionId) {
    return format(ANONYMOUS_IDENTITY_FORMAT, LISTENER_PREFIX, sessionId, randomUUID());
  }

  /**
   * Generates anonymous speaker identity. Each time generate is invoked it will result in a new
   * identity, even for the same sessionId.
   *
   * @param sessionId Session ID.
   * @return Generated identity
   */
  public String generateAnonymousSpeakerIdentity(UUID sessionId) {
    return format(ANONYMOUS_IDENTITY_FORMAT, SPEAKER_PREFIX, sessionId, randomUUID());
  }

  /**
   * Generates anonymous speaker identity. Each time generate is invoked it will result in a new
   * identity, even for the same sessionId.
   *
   * @param sessionId Session ID.
   * @return Generated identity
   */
  public String generateAnonymousOperatorIdentity(UUID sessionId) {
    return format(ANONYMOUS_IDENTITY_FORMAT, OPERATOR_PREFIX, sessionId, randomUUID());
  }

  /**
   * Generates speaker identity. Invoking generate for specific speakerId each time will produce
   * same identity.
   *
   * @param speakerId Speaker ID.
   * @return Generated identity
   */
  public String generateSpeakerIdentity(UUID speakerId) {
    return format("%s%s", SPEAKER_PREFIX, speakerId);
  }

  /**
   * Extracts phone participant ID from the Twilio identity. Ignores all other types of
   * participants. It will return empty {@link Optional} if the participant is anonymous:
   *
   * @see #generateAnonymousSpeakerIdentity(UUID)
   * @see #generateAnonymousListenerIdentity(UUID)
   * @see #generateAnonymousOperatorIdentity(UUID)
   * @param participantIdentity Twilio participant identity.
   * @return Participant ID or empty {@link Optional}.
   */
  public Optional<UUID> extractSpeakerIdentity(String participantIdentity) {
    return of(participantIdentity)
        .filter(i -> i.startsWith(SPEAKER_PREFIX))
        .filter(i -> i.lastIndexOf("_") == 7)
        .map(i -> i.replace(SPEAKER_PREFIX, ""))
        .map(UUID::fromString);
  }

  /**
   * Checks is provided participant identity belonging to a listener participant.
   *
   * @see #generateAnonymousListenerIdentity(UUID)
   * @param participantIdentity Twilio participant identity.
   * @return True if participant is listener, false otherwise.
   */
  public boolean isListener(String participantIdentity) {
    return participantIdentity.startsWith(LISTENER_PREFIX);
  }

  /**
   * Checks if provided participant identity belongs to a participant connected over phone.
   *
   * @param participantIdentity Twilio participant identity.
   * @return True if participant is connected over phone, false otherwise.
   */
  public boolean isPhoneParticipant(String participantIdentity) {
    return participantIdentity.startsWith(LISTENER_PREFIX)
        || participantIdentity.startsWith(SPEAKER_PREFIX)
        || participantIdentity.startsWith(OPERATOR_PREFIX);
  }
}
