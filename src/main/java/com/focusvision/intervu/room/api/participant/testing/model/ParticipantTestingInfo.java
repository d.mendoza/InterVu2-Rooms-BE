package com.focusvision.intervu.room.api.participant.testing.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model representing the participant testing info.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Participant Testing Info")
public class ParticipantTestingInfo {

  @ApiModelProperty("Webcams Usage")
  private final boolean useWebcams;
}
