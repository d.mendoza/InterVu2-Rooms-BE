package com.focusvision.intervu.room.api.export.model;

import com.focusvision.intervu.room.api.common.model.ProjectType;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.model.dto.RoomParticipantDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the room state.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@ApiModel("RoomState")
public class RoomStateExportDto {

  @ApiModelProperty("Room ID")
  private UUID roomId;
  @ApiModelProperty("Room display name")
  private String roomName;
  @ApiModelProperty("Project display name")
  private String projectName;
  @ApiModelProperty("Indicates room current state")
  private ResearchSessionState state;
  @ApiModelProperty("Room's start time")
  private Instant startsAt;
  @ApiModelProperty("Room's end time")
  private Instant endsAt;
  @ApiModelProperty("Room's webcams usage")
  private boolean useWebcams;
  @ApiModelProperty("Project number")
  private String projectNumber;
  @ApiModelProperty("Room's privacy usage")
  private boolean privacy;
  @ApiModelProperty("Project type")
  private ProjectType projectType;

  @ApiModelProperty("Participants in the room")
  private List<RoomParticipantDto> participants;
  @ApiModelProperty("Active stimulus in the room")
  private StimulusExportDto stimulus;
  @ApiModelProperty("Stimuli focus in the room")
  private boolean stimulusInFocus;
  @ApiModelProperty("Drawing sync indicator")
  private boolean drawingInSync;
  @ApiModelProperty("Respondents chat enabled/disabled indicator")
  private boolean respondentsChatEnabled;
}
