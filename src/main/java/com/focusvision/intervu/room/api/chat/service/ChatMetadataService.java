package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.chat.model.ChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ParticipantChatMetadataDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.List;

/**
 * Service used for chat metadata related operations. Chat metadata is used to create chat UI.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatMetadataService {

  /**
   * Returns chat metadata for waiting room. Chat metadata is participant specific. Some
   * participants may have limited chat options due to their role.
   *
   * @param caller Participant requesting chat metadata.
   * @return Waiting room chat metadata for participant.
   */
  ChatMetadataDto getWaitingRoomChatMetadata(AuthenticatedParticipant caller);

  /**
   * Returns chat metadata for meeting room. Chat metadata is participant specific. Some
   * participants may have limited chat options due to their role.
   *
   * @param caller Participant requesting chat metadata.
   * @return Meeting room chat metadata for participant.
   */
  ChatMetadataDto getMeetingRoomChatMetadata(AuthenticatedParticipant caller);

  /**
   * Gets the participants metadata for caller's waiting room.
   *
   * @param caller Participant requesting chat metadata.
   * @return Participants chat metadata.
   */
  List<ParticipantChatMetadataDto> getWaitingRoomParticipantsChatMetadata(
      AuthenticatedParticipant caller);

  /**
   * Gets the participants metadata for caller's meeting room.
   *
   * @param caller Participant requesting chat metadata.
   * @return Participants chat metadata.
   */
  List<ParticipantChatMetadataDto> getMeetingRoomParticipantsChatMetadata(
      AuthenticatedParticipant caller);
}
