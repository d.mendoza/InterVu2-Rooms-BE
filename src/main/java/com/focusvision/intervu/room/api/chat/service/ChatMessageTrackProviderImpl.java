package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessageTrack;
import com.focusvision.intervu.room.api.repository.ChatMessageTrackRepository;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ChatMessageTrackProvider}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ChatMessageTrackProviderImpl implements ChatMessageTrackProvider {

  private final ChatMessageTrackRepository repository;

  //Cache
  @Override
  public Optional<ChatMessageTrack> findLatestTrack(ChatMessage.ChatSource source,
                                                    ChatType type,
                                                    UUID participantId,
                                                    UUID handle) {
    return repository.findOneBySourceAndTypeAndParticipantIdAndHandle(source, type, participantId,
        handle);
  }

  //Cache
  @Override
  public ChatMessageTrack addTrack(ChatMessageTrack chatMessageTrack) {
    return repository.save(chatMessageTrack);
  }
}
