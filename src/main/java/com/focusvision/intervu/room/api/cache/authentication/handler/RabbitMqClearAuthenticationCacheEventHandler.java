package com.focusvision.intervu.room.api.cache.authentication.handler;

import com.focusvision.intervu.room.api.cache.authentication.event.ClearAuthenticationCacheEvent;
import com.focusvision.intervu.room.api.cache.authentication.service.AuthenticationCachingService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * {@link RabbitListener} specific implementation of a {@link ClearAuthenticationCacheEventHandler}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqClearAuthenticationCacheEventHandler
    implements ClearAuthenticationCacheEventHandler {

  private final AuthenticationCachingService authenticationCachingService;

  @Override
  @RabbitListener(queues = "#{clearAuthenticationCacheQueue.name}")
  public void handle(ClearAuthenticationCacheEvent event) {
    log.debug("Handling clear auth cache event: {}", event);

    event.participantIds().stream()
        .map(UUID::toString)
        .forEach(authenticationCachingService::clearAuthenticationCache);
  }
}
