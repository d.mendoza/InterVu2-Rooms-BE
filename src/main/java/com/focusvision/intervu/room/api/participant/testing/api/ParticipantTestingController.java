package com.focusvision.intervu.room.api.participant.testing.api;

import com.focusvision.intervu.room.api.participant.testing.application.ParticipantTestingService;
import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingInfo;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller for participant testing related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "participants/testing", produces = MediaType.APPLICATION_JSON_VALUE)
public class ParticipantTestingController {

  private final ParticipantTestingService participantTestingService;

  /**
   * Reports participant testing failure.
   *
   * @param caller Authenticated participant.
   */
  @PostMapping("fail")
  @ApiOperation(
      value = "Report testing failure",
      notes = "Report testing failure for a participant with a specified invitation token")
  @PreAuthorize("hasAuthority('PARTICIPANT_TESTING_REPORT')")
  public void testingFail(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Participant {} failed testing.", caller.getId());

    participantTestingService.reportFail(caller.getPlatformId(), caller.getRoomPlatformId());
  }

  /**
   * Reports participant testing success.
   *
   * @param caller Authenticated participant.
   */
  @PostMapping("pass")
  @ApiOperation(
      value = "Report testing success",
      notes = "Report testing success of a participant")
  @PreAuthorize("hasAuthority('PARTICIPANT_TESTING_REPORT')")
  public void testingSuccess(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Participant {} passed testing.", caller.getId());

    participantTestingService.reportSuccess(caller.getPlatformId(), caller.getRoomPlatformId());
  }

  /**
   * Gets participant testing info.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping("info")
  @ApiOperation(
      value = "Get information about participant's research session",
      notes = "Get information about participant's research session")
  @PreAuthorize("hasAuthority('PARTICIPANT_TESTING_READ')")
  public ParticipantTestingInfo testingInfo(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Getting participant research session testing info {}", caller.getId());

    return participantTestingService.getInfo(caller.getRoomId());
  }

}
