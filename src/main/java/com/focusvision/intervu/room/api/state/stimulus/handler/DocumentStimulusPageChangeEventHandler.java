package com.focusvision.intervu.room.api.state.stimulus.handler;

import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;

/**
 * Handler for {@link DocumentStimulusPageChangeEvent}.
 */
public interface DocumentStimulusPageChangeEventHandler {

  /**
   * Handles the provided document stimulus page change event.
   *
   * @param event Document stimulus page change event.
   */
  void handle(DocumentStimulusPageChangeEvent event);
}
