package com.focusvision.intervu.room.api.service.impl;

import com.focusvision.intervu.room.api.chat.service.ChatCommunicationChannelService;
import com.focusvision.intervu.room.api.model.dto.ChannelDto;
import com.focusvision.intervu.room.api.security.IntervuUser;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link CommunicationChannelService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CommunicationChannelServiceImpl implements CommunicationChannelService {

  /**
   * Represents the destination prefix for the room topic.
   */
  public static final String ROOM_CHANNEL = "/topic/room";
  /**
   * Represents the destination prefix for the participant topic.
   */
  public static final String PARTICIPANT_CHANNEL = "/topic/participant";
  /**
   * Represents the destination prefix for the draw topic.
   */
  public static final String DRAW_CHANNEL = "/topic/draw";
  /**
   * Represents the destination prefix for the stimulus topic.
   */
  public static final String STIMULUS_CHANNEL = "/topic/stimulus";
  /**
   * Represents the destination prefix for the stimulus global topic.
   */
  public static final String STIMULUS_GLOBAL_CHANNEL = "/topic/global-stimulus";

  private final ChatCommunicationChannelService chatCommunicationChannelService;

  @Override
  public ChannelDto getParticipantChannelData(UUID participantId, UUID roomId) {
    log.debug("Getting channel details for participant {} and room {}.", participantId, roomId);

    return new ChannelDto()
        .setParticipant(this.getParticipantChannel(participantId))
        .setRoom(this.getRoomChannel(roomId))
        .setDraw(this.getDrawChannel(roomId))
        .setChat(this.chatCommunicationChannelService.getParticipantChatChannel(participantId))
        .setStimulus(this.getStimulusChannel(roomId))
        .setGlobalStimulus(this.getStimulusGlobalChannel(roomId));
  }

  @Override
  public String getParticipantGlobalChannel(String participantPlatformId) {
    log.debug("Getting global channel details for participant {}.", participantPlatformId);

    return this.getParticipantChannel(participantPlatformId);
  }

  @Override
  public String getParticipantGlobalChannel(IntervuUser intervuUser) {
    log.debug("Getting global channel details for participant {}.", intervuUser.getPlatformId());

    return this.getParticipantChannel(intervuUser.getPlatformId());
  }

  @Override
  public String getParticipantChannel(UUID participantId) {
    log.debug("Getting channel details for participant {}.", participantId);

    return this.getParticipantChannel(participantId.toString());
  }

  private String getParticipantChannel(String handle) {
    return PARTICIPANT_CHANNEL + "." + handle;
  }

  @Override
  public String getRoomChannel(UUID roomId) {
    log.debug("Getting room {} channel details.", roomId);

    return this.getRoomChannel(roomId.toString());
  }

  private String getRoomChannel(String handle) {
    return ROOM_CHANNEL + "." + handle;
  }

  @Override
  public String getDrawChannel(UUID roomId) {
    log.debug("Getting room {} draw channel details.", roomId);

    return this.getDrawChannel(roomId.toString());
  }

  private String getDrawChannel(String handle) {
    return DRAW_CHANNEL + "." + handle;
  }

  @Override
  public String getStimulusChannel(UUID roomId) {
    log.debug("Getting stimulus {} channel details.", roomId);

    return this.getStimulusChannel(roomId.toString());
  }

  private String getStimulusChannel(String handle) {
    return STIMULUS_CHANNEL + "." + handle;
  }

  @Override
  public String getStimulusGlobalChannel(UUID roomId) {
    log.debug("Getting stimulus {} respondent channel details.", roomId);

    return this.getStimulusGlobalChannel(roomId.toString());
  }

  private String getStimulusGlobalChannel(String handle) {
    return STIMULUS_GLOBAL_CHANNEL + "." + handle;
  }

}
