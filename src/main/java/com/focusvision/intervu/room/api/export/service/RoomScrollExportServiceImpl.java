package com.focusvision.intervu.room.api.export.service;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import com.focusvision.intervu.room.api.export.mapper.RoomScrollLogExportMapper;
import com.focusvision.intervu.room.api.export.model.RoomScrollLogExportDto;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.scroll.model.ScrollLog;
import com.focusvision.intervu.room.api.scroll.service.ScrollService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomScrollExportService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomScrollExportServiceImpl implements RoomScrollExportService {

  private final ScrollService scrollService;
  private final RoomOffsetProvider roomOffsetProvider;
  private final ResearchSessionService researchSessionService;
  private final RoomScrollLogExportMapper scrollLogExportMapper;
  private final StimulusService stimulusService;

  @Override
  public Optional<List<RoomScrollLogExportDto>> export(String platformId) {
    log.debug("Exporting drawing logs for room with platform ID {}.", platformId);

    return researchSessionService.fetch(platformId)
        .map(this::getLogs);
  }

  private List<RoomScrollLogExportDto> getLogs(ResearchSession room) {
    var stimulusMap = stimulusService.getAll(room.getId()).stream()
        .collect(toMap(Stimulus::getId, Stimulus::getPlatformId));

    return scrollService.getRoomScrollLogs(room.getId()).stream()
        .map(log -> map(
            log,
            room.getConference().getRecordingOffset(),
            stimulusMap.get(log.getContextId())))
        .collect(toList());
  }

  private RoomScrollLogExportDto map(
      ScrollLog scrollLog,
      Integer roomRecordingOffset,
      String stimulusPlatformId) {
    var alignedOffset = roomOffsetProvider
        .alignWithRecordingOffset(scrollLog.getOffset(), roomRecordingOffset);

    return scrollLogExportMapper.mapToDto(scrollLog, alignedOffset, stimulusPlatformId);
  }
}
