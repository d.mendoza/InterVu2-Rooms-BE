package com.focusvision.intervu.room.api.chat.model;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model representing incoming chat message.
 *
 * @author Branko Ostojic
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IncomingChatMessage {

  /**
   * Message content.
   */
  @NotEmpty
  private String message;
}
