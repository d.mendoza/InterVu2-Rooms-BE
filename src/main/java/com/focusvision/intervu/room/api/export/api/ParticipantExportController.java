package com.focusvision.intervu.room.api.export.api;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.export.model.ParticipantDataExportDto;
import com.focusvision.intervu.room.api.export.service.ParticipantExportService;
import com.focusvision.intervu.room.api.security.IntervuUser;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for exporting participant room personal data.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "export/participants", produces = MediaType.APPLICATION_JSON_VALUE)
public class ParticipantExportController {

  private final ParticipantExportService participantExportService;

  /**
   * Exports participant room data.
   *
   * @param caller     Authenticated participant.
   * @param platformId Participant platform ID.
   * @return Participant export data.
   */
  @GetMapping("{platformId}")
  @ApiOperation(
      value = "Gets participant export data",
      notes = "Gets participant export data")
  public List<ParticipantDataExportDto> export(
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller,
      @PathVariable String platformId) {
    log.debug("Exporting room personal data for participant {}", platformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return participantExportService.export(platformId);
  }
}
