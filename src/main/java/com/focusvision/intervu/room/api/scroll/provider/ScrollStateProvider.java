package com.focusvision.intervu.room.api.scroll.provider;

import com.focusvision.intervu.room.api.scroll.model.ScrollLogDto;
import java.util.Optional;
import java.util.UUID;

/**
 * Provider for scroll state logs.
 */
public interface ScrollStateProvider {

  /**
   * Gets the scroll log representing the current state for the specified context.
   *
   * @param roomId    Room ID.
   * @param contextId Drawing context ID.
   * @return Current scroll state on the specified context or empty {@link Optional}.
   */
  Optional<ScrollLogDto> getCurrentState(UUID roomId, UUID contextId);
}
