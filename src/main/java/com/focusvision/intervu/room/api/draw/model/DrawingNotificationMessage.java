package com.focusvision.intervu.room.api.draw.model;

import com.focusvision.intervu.room.api.common.model.DrawingActionType;
import com.focusvision.intervu.room.api.common.model.DrawingContextType;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;

/**
 * Model representing draw action notification message.
 *
 * @author Branko Ostojic
 */
@Getter
@Builder
public class DrawingNotificationMessage {

  /**
   * ID of the participant who is drawing.
   */
  private final UUID participantId;

  /**
   * Drawing context type (e.g. stimulus).
   */
  private final DrawingContextType contextType;

  /**
   * ID of the context (e.g. stimulus) on which drawing is performed.
   */
  private final UUID contextId;

  /**
   * Drawing action type (e.g. draw, undo, redo, clear).
   */
  private final DrawingActionType type;

  /**
   * Drawing action content.
   */
  private final String content;

  /**
   * Drawing action offset.
   */
  private final Long offset;
}
