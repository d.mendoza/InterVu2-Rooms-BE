package com.focusvision.intervu.room.api.chat.service;

import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.common.model.ChatType.PRIVATE;
import static com.focusvision.intervu.room.api.common.model.ChatType.RESPONDENTS;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.ChatSource.MEETING_ROOM;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.ChatSource.WAITING_ROOM;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.BOOKMARK;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.PII;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.REGULAR;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.MessageType.SNAPSHOT;
import static java.time.Instant.now;
import static java.util.List.of;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;

import com.focusvision.intervu.room.api.chat.mapper.ChatMessageMapper;
import com.focusvision.intervu.room.api.chat.model.ChatHistoryDto;
import com.focusvision.intervu.room.api.chat.model.ChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.GroupChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ParticipantChatMetadataDto;
import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.repository.ChatMessageRepository;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ChatDataService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ChatDataServiceImpl implements ChatDataService {

  private final ChatMessageRepository chatMessageRepository;
  private final ChatMetadataService chatMetadataService;
  private final ChatMessageTrackingService chatTrackingService;
  private final ChatMessageMapper chatMessageMapper;

  @Override
  public ChatMessage saveWaitingRoomPrivateMessage(String message,
                                                   UUID senderId,
                                                   String senderPlatformId,
                                                   String senderDisplayName,
                                                   UUID handle,
                                                   UUID roomId) {
    log.debug("Saving waiting room private chat message sent by [{}] to [{}]", senderId, handle);

    var chatMessage = new ChatMessage()
        .setSource(WAITING_ROOM)
        .setType(PRIVATE)
        .setMessageType(REGULAR)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveWaitingRoomRespondentsMessage(String message,
                                                       UUID senderId,
                                                       String senderPlatformId,
                                                       String senderDisplayName,
                                                       UUID handle,
                                                       UUID roomId) {
    log.debug("Saving waiting room respondents chat message sent by [{}] to [{}]",
        senderId, handle);

    var chatMessage = new ChatMessage()
        .setSource(WAITING_ROOM)
        .setType(RESPONDENTS)
        .setMessageType(REGULAR)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveWaitingRoomInternalMessage(String message,
                                                    UUID senderId,
                                                    String senderPlatformId,
                                                    String senderDisplayName,
                                                    UUID handle,
                                                    UUID roomId) {
    log.debug("Saving waiting room internal chat message sent by [{}] to [{}]", senderId, handle);

    var chatMessage = new ChatMessage()
        .setSource(WAITING_ROOM)
        .setType(INTERNAL)
        .setMessageType(REGULAR)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveMeetingRoomPrivateMessage(String message,
                                                   UUID senderId,
                                                   String senderPlatformId,
                                                   String senderDisplayName,
                                                   UUID handle,
                                                   UUID roomId) {
    log.debug("Saving meeting room private chat message sent by [{}] to [{}]", senderId, handle);

    var chatMessage = new ChatMessage()
        .setSource(MEETING_ROOM)
        .setType(PRIVATE)
        .setMessageType(REGULAR)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveMeetingRoomInternalMessage(String message,
                                                    UUID senderId,
                                                    String senderPlatformId,
                                                    String senderDisplayName,
                                                    UUID handle,
                                                    UUID roomId) {
    log.debug("Saving meeting room internal chat message sent by [{}] in room [{}]",
        senderId, handle);

    var chatMessage = new ChatMessage()
        .setSource(MEETING_ROOM)
        .setType(INTERNAL)
        .setMessageType(REGULAR)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveMeetingRoomRespondentsMessage(String message,
                                                       UUID senderId,
                                                       String senderPlatformId,
                                                       String senderDisplayName,
                                                       UUID handle,
                                                       UUID roomId) {
    log.debug("Saving meeting room respondents chat message sent by [{}] in room [{}]",
        senderId, handle);

    var chatMessage = new ChatMessage()
        .setSource(MEETING_ROOM)
        .setType(RESPONDENTS)
        .setMessageType(REGULAR)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveMeetingRoomBookmarkMessage(String message,
                                                    UUID senderId,
                                                    String senderPlatformId,
                                                    String senderDisplayName,
                                                    UUID handle,
                                                    UUID roomId) {
    log.debug("Saving meeting room internal bookmark chat message sent by [{}] to [{}]",
        senderId, handle);

    var chatMessage = new ChatMessage()
        .setSource(MEETING_ROOM)
        .setType(INTERNAL)
        .setMessageType(BOOKMARK)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveMeetingRoomPiiMessage(UUID senderId,
                                               String senderPlatformId,
                                               String senderDisplayName,
                                               UUID handle,
                                               UUID roomId) {
    log.debug("Saving meeting room internal PII chat message sent by [{}] to [{}]", senderId,
        handle);

    var chatMessage = new ChatMessage()
        .setSource(MEETING_ROOM)
        .setType(INTERNAL)
        .setMessageType(PII)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatMessage saveMeetingRoomSnapshotMessage(String message,
                                                    UUID senderId,
                                                    String senderPlatformId,
                                                    String senderDisplayName,
                                                    UUID handle,
                                                    UUID roomId) {
    log.debug("Saving meeting room internal snapshot chat message (senderId: {}, handle: {}",
        senderId,
        handle);

    var chatMessage = new ChatMessage()
        .setSource(MEETING_ROOM)
        .setType(INTERNAL)
        .setMessageType(SNAPSHOT)
        .setMessage(message)
        .setSenderId(senderId)
        .setSenderDisplayName(senderDisplayName)
        .setSenderPlatformId(senderPlatformId)
        .setHandle(handle)
        .setResearchSessionId(roomId)
        .setSentAt(now());

    return chatMessageRepository.save(chatMessage);
  }

  @Override
  public ChatHistoryDto getWaitingRoomInternalChatMessages(AuthenticatedParticipant caller) {
    log.debug("Fetching all waiting room internal chat messages for participant {}",
        caller.getId());

    var metadata = chatMetadataService.getWaitingRoomChatMetadata(caller);
    var handle = ofNullable(metadata.getInternal())
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    return getChatHistory(
        () -> chatMessageRepository
            .findAllByHandleAndTypeAndSourceIsInOrderBySentAtAsc(handle, INTERNAL,
                of(WAITING_ROOM)),
        () -> chatTrackingService
            .getLastSeenWaitingRoomInternalMessage(caller.getId(), handle),
        caller);
  }

  @Override
  public ChatHistoryDto getMeetingRoomInternalChatMessages(AuthenticatedParticipant caller) {
    log.debug("Fetching all meeting room internal chat messages for participant {}",
        caller.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(caller);
    var handle = ofNullable(metadata.getInternal())
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    return getChatHistory(
        () -> chatMessageRepository
            .findAllByHandleAndTypeAndSourceIsInOrderBySentAtAsc(handle, INTERNAL,
                of(MEETING_ROOM)),
        () -> chatTrackingService
            .getLastSeenMeetingRoomInternalMessage(caller.getId(), handle),
        caller);
  }

  @Override
  public ChatHistoryDto getWaitingRoomRespondentsChatMessages(AuthenticatedParticipant caller) {
    log.debug("Fetching all waiting room respondents chat messages for participant {}",
        caller.getId());

    var metadata = chatMetadataService.getWaitingRoomChatMetadata(caller);
    var handle = ofNullable(metadata.getRespondents())
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    return getChatHistory(
        () -> chatMessageRepository
            .findAllByHandleAndTypeAndSourceIsInOrderBySentAtAsc(handle, RESPONDENTS,
                of(WAITING_ROOM)),
        () -> chatTrackingService
            .getLastSeenWaitingRoomRespondentsMessage(caller.getId(), handle),
        caller);
  }

  @Override
  public ChatHistoryDto getMeetingRoomRespondentsChatMessages(AuthenticatedParticipant caller) {
    log.debug("Fetching all meeting room respondents chat messages for participant {}",
        caller.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(caller);
    var handle = ofNullable(metadata.getRespondents())
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    return getChatHistory(
        () -> chatMessageRepository
            .findAllByHandleAndTypeAndSourceIsInOrderBySentAtAsc(handle, RESPONDENTS,
                of(MEETING_ROOM)),
        () -> chatTrackingService
            .getLastSeenMeetingRoomRespondentsMessage(caller.getId(), handle),
        caller);
  }

  @Override
  public ChatHistoryDto getMeetingRoomPrivateChatMessages(AuthenticatedParticipant caller,
                                                          UUID secondParticipant) {
    log.debug("Fetching all meeting room private chat messages for {} and {}.",
        caller.getId(), secondParticipant);

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(caller);
    var handle = this.getDirectChatHandle(metadata, secondParticipant);

    return getChatHistory(
        () -> chatMessageRepository
            .findAll(caller.getId(), secondParticipant, PRIVATE, of(MEETING_ROOM)),
        () -> chatTrackingService
            .getLastSeenMeetingRoomPrivateMessage(caller.getId(), handle),
        caller);
  }

  private ChatHistoryDto getHistory(List<ChatMessage> messages,
                                    UUID lastSeenMessageId,
                                    long unreadMessages) {
    var messagesDto = messages.stream()
        .map(chatMessageMapper::mapToDto)
        .toList();

    return new ChatHistoryDto()
        .setLatestSeenMessageId(lastSeenMessageId)
        .setUnreadMessages(unreadMessages)
        .setMessages(messagesDto);
  }

  @Override
  public ChatHistoryDto getWaitingRoomPrivateChatMessages(AuthenticatedParticipant caller,
                                                          UUID secondParticipant) {
    log.debug("Fetching all meeting room private chat messages for {} and {}.",
        caller.getId(), secondParticipant);

    var metadata = chatMetadataService.getWaitingRoomChatMetadata(caller);
    var handle = this.getDirectChatHandle(metadata, secondParticipant);

    return getChatHistory(
        () -> chatMessageRepository.findAll(caller.getId(), secondParticipant, PRIVATE,
            of(WAITING_ROOM)),
        () -> chatTrackingService.getLastSeenWaitingRoomPrivateMessage(caller.getId(), handle),
        caller);
  }

  @Override
  public void clearUserHistory(Participant participant) {
    log.debug("Deleting the chat history of participant {}", participant.getPlatformId());

    chatMessageRepository.deleteAllBySenderPlatformIdAndResearchSessionId(
        participant.getPlatformId(),
        participant.getResearchSession().getId()
    );
  }

  @Override
  public List<ChatMessage> getRespondentChatMessages(UUID handle, String senderPlatformId) {
    log.debug("Getting chat messages for handle {} and sender {}", handle, senderPlatformId);

    return chatMessageRepository
        .findAllByHandleAndSenderPlatformIdAndMessageType(handle, senderPlatformId, REGULAR);
  }

  private UUID getDirectChatHandle(ChatMetadataDto metadata,
                                   UUID secondParticipant) {
    return ofNullable(metadata.getDirect())
        .stream()
        .flatMap(Collection::stream)
        .map(ParticipantChatMetadataDto::getHandle)
        .filter(recipient -> recipient.equals(secondParticipant))
        .findFirst()
        .orElseThrow(this::chatForbiddenException);
  }

  private ChatHistoryDto getChatHistory(Supplier<List<ChatMessage>> messageSupplier,
                                        Supplier<Optional<UUID>> lastSeenMessageIdSupplier,
                                        AuthenticatedParticipant caller) {
    var messages = messageSupplier.get();
    var latestSeenMessageId = lastSeenMessageIdSupplier.get();
    var unreadMessages = latestSeenMessageId
        .map(id -> countUnseenMessages(messages, id, caller.getId()))
        .orElseGet(() -> countOtherParticipantsMessages(messages, caller.getId()));

    return this.getHistory(messages, latestSeenMessageId.orElse(null), unreadMessages);
  }


  private long countUnseenMessages(List<ChatMessage> messages,
                                   UUID latestSeenMessageId,
                                   UUID callerId) {
    return messages.stream()
        .dropWhile(not(message -> message.getId().equals(latestSeenMessageId)))
        .skip(1)
        .filter(not(message -> message.getSenderId().equals(callerId)))
        .count();
  }

  private long countOtherParticipantsMessages(List<ChatMessage> messages,
                                              UUID callerId) {
    return messages.stream()
        .filter(not(message -> message.getSenderId().equals(callerId)))
        .count();
  }

  private IntervuRoomException chatForbiddenException() {
    return new ActionForbiddenException("Reading chat data forbidden.");
  }
}
