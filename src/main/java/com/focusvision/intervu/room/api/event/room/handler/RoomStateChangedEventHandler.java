package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.state.RoomStateChangedEvent;

/**
 * Room state changed event handler.
 */
public interface RoomStateChangedEventHandler {

  void handle(RoomStateChangedEvent event);
}
