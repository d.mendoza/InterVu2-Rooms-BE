package com.focusvision.intervu.room.api.model.streaming;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.RecordingType;
import java.time.Instant;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Model for group room recording.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@Accessors(chain = true)
public class ParticipantGroupRoomRecording {

  /**
   * Group room recording SID.
   */
  private String sid;
  /**
   * Group room recording track name.
   */
  private String trackName;
  /**
   * Group room recording duration.
   */
  private Integer duration;

  /**
   * Group room recording creation time.
   */
  private Instant startedAt;
  /**
   * Group room recording stream type.
   */
  private RecordingType recordingType;
  /**
   * Group room recording participant type.
   */
  private String participantType;
  /**
   * Group room recording participant ID.
   */
  private String participantId;

  public boolean isVideo() {
    return RecordingType.VIDEO.equals(this.recordingType);
  }

  public boolean isAudio() {
    return RecordingType.AUDIO.equals(this.recordingType);
  }

  public boolean isScreenShare() {
    return RecordingType.SCREENSHARE.equals(this.recordingType);
  }

  public boolean isRespondent() {
    return ParticipantRole.RESPONDENT.name().equalsIgnoreCase(participantType);
  }

  public boolean isModerator() {
    return ParticipantRole.MODERATOR.name().equalsIgnoreCase(participantType);
  }

  public boolean isObserver() {
    return ParticipantRole.OBSERVER.name().equalsIgnoreCase(participantType);
  }

  public boolean isTranslator() {
    return ParticipantRole.TRANSLATOR.name().equalsIgnoreCase(participantType);
  }

}
