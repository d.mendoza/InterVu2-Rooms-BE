package com.focusvision.intervu.room.api.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;

/**
 * Interceptor for outbound WebSocket messages.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RequiredArgsConstructor
public class WebSocketOutboundChannelInterceptor implements ChannelInterceptor {

  @Override
  public Message<?> preSend(Message<?> message, MessageChannel channel) {
    StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
    if (StompCommand.MESSAGE.equals(headerAccessor.getCommand())) {
      // TODO: Pretty print message.
      // Track sending of messages somehow maybe.
      log.info("Sending WS message: {}", message);
    }
    return message;
  }

}
