package com.focusvision.intervu.room.api.state.stimulus.service;

import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service for handling stimulus state operations.
 */
public interface StimulusStateService {

  /**
   * Finds stimulus state for provided stimulus ID and participant ID.
   *
   * @param stimulusId    Stimulus which state is changed.
   * @param participantId Room participant performing stimulus state action.
   */
  Optional<StimulusActionState> getStimulusStateForParticipant(UUID stimulusId, UUID participantId);

  /**
   * Finds stimulus state for provided stimulus ID.
   *
   * @param stimulusId Stimulus which state is changed.
   * @return Matching results.
   */
  List<StimulusActionState> getStimulusState(UUID stimulusId);

  /**
   * Persists stimulus state.
   *
   * @param stimulusActionState Stimulus action state to save.
   * @return Persisted stimulus action state.
   */
  StimulusActionState save(StimulusActionState stimulusActionState);

  /**
   * Resets stimulus state.
   *
   * @param stimulusId Stimulus.
   */
  void resetState(UUID stimulusId);
}
