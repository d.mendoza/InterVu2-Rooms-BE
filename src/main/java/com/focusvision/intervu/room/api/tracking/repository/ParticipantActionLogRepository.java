package com.focusvision.intervu.room.api.tracking.repository;

import com.focusvision.intervu.room.api.tracking.model.entity.ParticipantActionLog;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link ParticipantActionLog} entity.
 *
 * @author Branko Ostojic
 */
public interface ParticipantActionLogRepository extends JpaRepository<ParticipantActionLog, UUID> {}
