package com.focusvision.intervu.room.api.controller;

import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.model.dto.StimulusStateDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.stimulus.StimulusActionOperator;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room document stimuli action state related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/stimuli/document", produces = MediaType.APPLICATION_JSON_VALUE)
public class DocumentSharingStimulusStateController {

  private final StimulusActionOperator stimulusActionOperator;
  private final StimulusService stimulusService;

  /**
   * Get stimulus current state.
   *
   * @param id     Stimulus ID.
   * @param caller User performing action.
   * @return Stimulus state.
   */
  @GetMapping("/{id}/state")
  @PreAuthorize("hasAuthority('STIMULI_READ')")
  public StimulusStateDto lastAction(
      @ApiParam("Stimulus ID") @PathVariable String id,
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Get stimulus {} state for participant {}", id, caller.getId());

    var stimulusId = fromString(id);
    stimulusService.findDocumentSharingStimulus(stimulusId, caller.getRoomId())
        .orElseThrow(() -> new ActionForbiddenException("Fetching states forbidden."));

    return stimulusActionOperator.getLastAction(stimulusId);
  }

}
