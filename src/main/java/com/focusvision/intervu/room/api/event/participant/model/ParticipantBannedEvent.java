package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * Model for participant banned event.
 *
 * @author Branko Ostojic
 */
public record ParticipantBannedEvent(UUID roomId, UUID participantId) {

}
