package com.focusvision.intervu.room.api.event.participant.publisher;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedToRoomEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedWithPhoneEvent;

/**
 * Participant presence event publisher.
 *
 * @author Branko Ostojic
 */
public interface ParticipantPresenceEventPublisher {

  /**
   * Publishes provided participant connected event.
   *
   * @param event {@link ParticipantConnectedEvent}.
   */
  void publish(ParticipantConnectedEvent event);

  /**
   * Publishes provided participant disconnected event.
   *
   * @param event {@link ParticipantDisconnectedEvent}.
   */
  void publish(ParticipantDisconnectedEvent event);

  /**
   * Publishes provided participant connected with phone event.
   *
   * @param event {@link ParticipantConnectedWithPhoneEvent}.
   */
  void publish(ParticipantConnectedWithPhoneEvent event);

  /**
   * Publishes provided participant disconnected with phone event.
   *
   * @param event {@link ParticipantDisconnectedWithPhoneEvent}.
   */
  void publish(ParticipantDisconnectedWithPhoneEvent event);

  /**
   * Publishes provided participant connected to the room event.
   *
   * @param event {@link ParticipantConnectedToRoomEvent}.
   * @deprecated We should switch to use only {@link ParticipantConnectedEvent}.
   */
  @Deprecated
  void publish(ParticipantConnectedToRoomEvent event);
}
