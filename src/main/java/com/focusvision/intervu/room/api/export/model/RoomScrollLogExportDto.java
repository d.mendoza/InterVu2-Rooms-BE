package com.focusvision.intervu.room.api.export.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Value;

/**
 * DTO for room scroll log export.
 */
@Value
@ApiModel("Room Scroll Log Export")
public class RoomScrollLogExportDto {

  @ApiModelProperty("Event offset from session start")
  Long offset;

  @ApiModelProperty("Context ID")
  UUID contextId;

  @ApiModelProperty("Stimulus platform ID")
  String stimulusPlatformId;

  @ApiModelProperty("Participant ID")
  UUID participantId;

  @ApiModelProperty("Action content")
  String content;
}
