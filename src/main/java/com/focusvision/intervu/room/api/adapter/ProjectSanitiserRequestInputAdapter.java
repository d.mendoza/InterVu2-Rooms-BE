package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.ProjectSanitiseRequest;

/**
 * Adapter for handling the project sanitise request.
 */
public interface ProjectSanitiserRequestInputAdapter {

  /**
   * Processes the project sanitise request.
   *
   * @param projectSanitiseRequest Project sanitise request.
   */
  void process(ProjectSanitiseRequest projectSanitiseRequest);
}
