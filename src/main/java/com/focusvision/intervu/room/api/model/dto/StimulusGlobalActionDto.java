package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.StimulusActionType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the stimulus global action.
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@ApiModel("StimulusGlobalActionDto")
public class StimulusGlobalActionDto {

  @ApiModelProperty("Room Id")
  private UUID roomId;

  @ApiModelProperty("Stimulus Id")
  private UUID stimulusId;

  @ApiModelProperty("Stimulus Action type")
  private StimulusActionType type;

  @ApiModelProperty("Action content")
  private String content;
}
