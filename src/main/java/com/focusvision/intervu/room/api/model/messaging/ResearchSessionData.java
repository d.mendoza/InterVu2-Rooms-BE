package com.focusvision.intervu.room.api.model.messaging;

import static com.focusvision.intervu.room.api.advisor.Lockable.Group.RESEARCH_SESSION;

import com.focusvision.intervu.room.api.advisor.Lockable;
import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.ProjectType;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.common.model.StimulusType;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the incoming research session data.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ResearchSessionData implements Lockable {

  private String id;
  private String name;
  private ResearchSessionState state;
  private Integer version;
  private Instant startsAt;
  private Instant endsAt;
  private boolean useWebcams = true;
  private boolean privacy = false;
  private CompositionLayout compositionLayout;
  private String listenerPin;
  private String speakerPin;
  private String operatorPin;
  private AudioChannel audioChannel = AudioChannel.NATIVE;

  private Project project;
  private List<Participant> participants = new ArrayList<>();
  private List<Stimulus> stimuli = new ArrayList<>();

  @Override
  public String lockKey() {
    return this.id;
  }

  @Override
  public Group lockGroup() {
    return RESEARCH_SESSION;
  }

  /** Project data. */
  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @ToString
  public static class Project {
    private String id;
    private String name;
    private String projectNumber;
    private ProjectType type;
  }

  /** Participant data. */
  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @ToString
  public static class Participant {
    private String id;
    private String name;
    private String invitationToken;
    private boolean admin = false;
    private boolean banable = true;
    private boolean moderator = false;
    private boolean guest = false;
    private boolean projectManager = false;
    private boolean projectOperator = false;
    private ParticipantRole role;
    private String speakerPin;
  }

  /** Stimulus data. */
  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @ToString
  public static class Stimulus {
    private String id;
    private String name;
    private String thumbnail;
    private String data;
    private Integer position;
    private StimulusType type;
  }
}
