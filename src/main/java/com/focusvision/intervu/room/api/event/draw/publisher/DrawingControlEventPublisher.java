package com.focusvision.intervu.room.api.event.draw.publisher;

import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncDisabledEvent;
import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncEnabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingDisabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingEnabledEvent;

/**
 * Publisher for the drawing control related evens.
 *
 * @author Branko Ostojic
 */
public interface DrawingControlEventPublisher {

  /**
   * Publishes the provided drawing sync enabled event.
   *
   * @param event Drawing sync enabled event.
   */
  void publish(DrawingSyncEnabledEvent event);

  /**
   * Publishes the provided drawing sync disabled event.
   *
   * @param event Drawing sync disabled event.
   */
  void publish(DrawingSyncDisabledEvent event);

  /**
   * Publish {@link ParticipantDrawingEnabledEvent}.
   *
   * @param event {@link ParticipantDrawingEnabledEvent}.
   */
  void publish(ParticipantDrawingEnabledEvent event);

  /**
   * Publish {@link ParticipantDrawingDisabledEvent}.
   *
   * @param event {@link ParticipantDrawingDisabledEvent}.
   */
  void publish(ParticipantDrawingDisabledEvent event);
}
