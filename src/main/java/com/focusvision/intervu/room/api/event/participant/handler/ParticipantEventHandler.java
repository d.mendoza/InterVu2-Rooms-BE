package com.focusvision.intervu.room.api.event.participant.handler;

import com.focusvision.intervu.room.api.adapter.StateNotificationSenderAdapter;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantMicrophoneToggleEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantRemovedEvent;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoleChangeDto;
import com.focusvision.intervu.room.api.model.event.ParticipantRoleChangedEvent;
import com.focusvision.intervu.room.api.role.RoleChangedEvent;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.service.RoomStateNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/** Participant events handler. */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
// TODO: Refactor participant events
public class ParticipantEventHandler {

  private final RoomStateNotificationService roomStateNotificationService;
  private final StateNotificationSenderAdapter sender;
  private final CommunicationChannelService communicationChannelService;

  /**
   * Handles participant microphone toggle event.
   *
   * @param event Event data.
   */
  @EventListener
  public void handle(ParticipantMicrophoneToggleEvent event) {
    log.info("Handling participant microphone toggle event: {}", event);

    roomStateNotificationService.sendParticipantMicrophoneToggleNotification(event.participantId());
  }

  /**
   * Handles participant removed event.
   *
   * @param event Event data.
   */
  @Deprecated
  @EventListener
  public void handle(ParticipantRemovedEvent event) {
    log.info("Handling participant removed event: [{}]", event);

    roomStateNotificationService.sendParticipantRemovedNotification(
        event.roomId(), event.participantId(), event.platformId());
  }

  /**
   * Handles participant role changed event.
   *
   * @param event Event data.
   */
  @EventListener
  public void handle(RoleChangedEvent event) {
    var data = new ParticipantRoleChangeDto(event.role());
    var channel = communicationChannelService.getParticipantChannel(event.participantId());
    sender.send(new ParticipantRoleChangedEvent(data, channel));
  }
}
