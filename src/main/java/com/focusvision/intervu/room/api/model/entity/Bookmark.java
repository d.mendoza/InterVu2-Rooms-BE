package com.focusvision.intervu.room.api.model.entity;

import com.focusvision.intervu.room.api.common.model.BookmarkType;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing bookmark.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "bookmarks")
@Getter
@Setter
@Accessors(chain = true)
public class Bookmark extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  @Column
  private String note;
  @Column
  @OrderBy
  private Long startOffset;
  @Enumerated(EnumType.STRING)
  private BookmarkType type;

  @ManyToOne
  private Participant participant;
  @ManyToOne
  private ResearchSession researchSession;

  public boolean isPii() {
    return BookmarkType.PII.equals(type);
  }

}
