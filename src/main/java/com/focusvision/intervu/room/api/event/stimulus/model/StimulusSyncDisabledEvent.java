package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus sync disabled event.
 */
public record StimulusSyncDisabledEvent(UUID roomId, UUID stimulusId) {

}
