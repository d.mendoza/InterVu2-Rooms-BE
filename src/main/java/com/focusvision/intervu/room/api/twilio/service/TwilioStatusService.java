package com.focusvision.intervu.room.api.twilio.service;

import java.util.Map;

/**
 * Service used for Twilio status handling related operations.
 *
 * @author Branko Ostojic
 */
public interface TwilioStatusService {

  /**
   * Process Twilio status update event.
   *
   * @param status Status parameters.
   */
  void processStatus(Map<String, String> status);
}
