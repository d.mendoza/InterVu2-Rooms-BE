package com.focusvision.intervu.room.api.exception;

import static com.focusvision.intervu.room.api.exception.ErrorCode.FORBIDDEN;

import lombok.Getter;

/**
 * Exception class used for action forbidden case.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Getter
public class ActionForbiddenException extends IntervuRoomException {

  public ActionForbiddenException() {
    super(FORBIDDEN);
  }

  public ActionForbiddenException(String message) {
    super(FORBIDDEN, message);
  }

  public ActionForbiddenException(ErrorCode code, String message) {
    super(code, message);
  }

}
