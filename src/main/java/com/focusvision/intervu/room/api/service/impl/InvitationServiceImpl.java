package com.focusvision.intervu.room.api.service.impl;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.repository.ParticipantRepository;
import com.focusvision.intervu.room.api.service.InvitationService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link InvitationService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class InvitationServiceImpl implements InvitationService {

  private final ParticipantRepository participantRepository;

  @Override
  public Optional<Participant> findInvitationDetails(String invitationToken) {
    log.debug("Fetching invitation for invitation token {}.", invitationToken);

    return participantRepository.findByInvitationToken(invitationToken);
  }

}
