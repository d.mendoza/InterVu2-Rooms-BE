package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model stimuli focus disabled event.
 */
public record StimuliFocusDisabledEvent(UUID roomId) {

}
