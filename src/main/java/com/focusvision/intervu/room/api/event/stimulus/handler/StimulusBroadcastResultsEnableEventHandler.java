package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusBroadcastResultsEnableEvent;

/**
 * Handler for the {@link StimulusBroadcastResultsEnableEvent}.
 */
public interface StimulusBroadcastResultsEnableEventHandler {

  /**
   * Handles the provided {@link StimulusBroadcastResultsEnableEvent}.
   *
   * @param event Stimulus broadcast results enable event.
   */
  void handle(StimulusBroadcastResultsEnableEvent event);

}
