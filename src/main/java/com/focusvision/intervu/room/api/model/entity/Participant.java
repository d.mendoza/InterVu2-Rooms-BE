package com.focusvision.intervu.room.api.model.entity;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing participant.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "participants")
@Getter
@Setter
@Accessors(chain = true)
public class Participant extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  @Column
  private String platformId;
  @Column
  private String invitationToken;
  @Column
  private String displayName;
  @Column
  @Enumerated(EnumType.STRING)
  private ParticipantRole role;
  @Column
  private String conferenceToken;
  @Column
  private boolean banned = false;
  @Column
  private boolean admin = false;
  @Column
  private boolean banable = true;
  @Column
  private Boolean moderator = false;
  @Column
  private boolean guest = false;
  @Column
  private boolean projectManager = false;
  @Column
  private boolean projectOperator = false;
  @Column
  private String speakerPin;

  @ManyToOne
  private ResearchSession researchSession;

  public boolean isRespondent() {
    return RESPONDENT.equals(role);
  }

  public boolean isModerator() {
    return MODERATOR.equals(role);
  }

  public boolean isObserver() {
    return OBSERVER.equals(role);
  }

  public boolean canModerate() {
    return MODERATOR.equals(role) || moderator;
  }

  public boolean isInternal() {
    return !isRespondent();
  }

  public boolean isForstaStaffMember() {
    return admin || projectManager || projectOperator;
  }

  public boolean isTranslator() {
    return TRANSLATOR.equals(role);
  }

}
