package com.focusvision.intervu.room.api.stimulus.snapshot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

/**
 * DTO representing stimulus snapshot.
 */
@Data
@Accessors(chain = true)
@ApiModel("Add Stimulus Snapshot")
public class AddStimulusSnapshotDto {

  @NotNull
  @Length(max = 200)
  @ApiModelProperty("Snapshot name")
  private String name;

  @NotNull
  @ApiModelProperty("Snapshot data")
  private byte[] data;

  @NotNull
  @Length(max = 30)
  @ApiModelProperty("Snapshot mime type")
  private String mimeType;

  @NotNull
  @ApiModelProperty("Snapshot timestamp")
  private Long timestamp;
}
