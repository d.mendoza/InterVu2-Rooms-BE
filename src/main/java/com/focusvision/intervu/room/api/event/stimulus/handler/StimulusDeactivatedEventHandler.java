package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusDeactivatedEvent;

/**
 * Stimulus event handler.
 */
public interface StimulusDeactivatedEventHandler {

  /**
   * Handles stimulus deactivated event.
   *
   * @param event Event data.
   */
  void handle(StimulusDeactivatedEvent event);
}
