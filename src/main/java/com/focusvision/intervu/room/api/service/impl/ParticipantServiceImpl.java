package com.focusvision.intervu.room.api.service.impl;

import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.repository.ParticipantRepository;
import com.focusvision.intervu.room.api.service.ParticipantService;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ParticipantService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantServiceImpl implements ParticipantService {

  private final ParticipantRepository participantRepository;

  @Override
  public Optional<Participant> findById(UUID id) {
    log.debug("Finding participant {} details.", id);

    return participantRepository.findById(id);
  }

  @Override
  public Participant updateConferenceToken(UUID id, String conferenceToken) {
    log.debug("Updating participant {} conference token {}.", id, conferenceToken);

    var participant = participantRepository.getById(id);
    participant.setConferenceToken(conferenceToken);
    return participantRepository.save(participant);
  }

  @Override
  public Optional<Participant> findBySpeakerPin(String pin) {
    log.debug("Finding participant by speaker pin {}.", pin);

    return participantRepository.findBySpeakerPin(pin);
  }

  @Override
  public Optional<Participant> findRoomParticipant(UUID roomId, String platformId) {
    log.debug("Finding room {} participant with platform ID {}", roomId, platformId);

    return participantRepository.findByResearchSessionIdAndPlatformId(roomId, platformId);
  }

  @Override
  public Optional<Participant> findRoomParticipant(String roomPlatformId, String platformId) {
    log.debug("Finding room {} participant with platform ID {}", roomPlatformId, platformId);

    return participantRepository.findByResearchSessionPlatformIdAndPlatformId(roomPlatformId,
        platformId);
  }

  @Override
  public Optional<Participant> findRoomParticipant(UUID roomId, UUID participantId) {
    log.debug("Finding room {} participant with ID {}", roomId, participantId);

    return participantRepository.findByResearchSessionIdAndId(roomId, participantId);
  }

  @Override
  public List<Participant> findRoomParticipants(UUID roomId) {
    log.debug("Finding room {} participants.", roomId);

    return participantRepository.findByResearchSessionId(roomId);
  }

  @Override
  public void banParticipant(UUID participantId) {
    log.debug("Marking participant {} as banned", participantId);

    var participant = participantRepository.getById(participantId);
    participant.setBanned(true);

    participantRepository.save(participant);
  }

  @Override
  public Participant get(UUID id) {
    log.debug("Getting participant {} details.", id);

    return participantRepository.getById(id);
  }

}
