package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusActivatedEvent;

/**
 * Stimulus activated event handler.
 */
public interface StimulusActivatedEventHandler {

  void handle(StimulusActivatedEvent event);
}
