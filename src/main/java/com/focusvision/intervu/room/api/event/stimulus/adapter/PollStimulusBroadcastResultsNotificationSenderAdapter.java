package com.focusvision.intervu.room.api.event.stimulus.adapter;

import com.focusvision.intervu.room.api.model.event.PollStimulusBroadcastResultsDataEvent;

/**
 * Adapter for sending {@link PollStimulusBroadcastResultsDataEvent}.
 */
public interface PollStimulusBroadcastResultsNotificationSenderAdapter {

  /**
   * Sends the provided poll stimulus broadcast results data.
   *
   * @param event Event.
   */
  void send(PollStimulusBroadcastResultsDataEvent event);
}
