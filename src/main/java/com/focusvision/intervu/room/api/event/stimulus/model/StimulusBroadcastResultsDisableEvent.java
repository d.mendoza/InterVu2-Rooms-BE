package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus broadcast results disable event.
 */
public record StimulusBroadcastResultsDisableEvent(UUID roomId,
                                                   UUID stimulusId) {

}
