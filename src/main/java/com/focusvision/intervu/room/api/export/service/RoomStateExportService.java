package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.model.RoomStateLogExportDto;
import java.util.List;
import java.util.Optional;

/**
 * Service for exporting room state log data.
 *
 * @author Branko Ostojic
 */
public interface RoomStateExportService {

  /**
   * Gets the export data for provided room ID.
   *
   * @param platformId Room platform ID.
   * @return List of room state logs or empty {@link Optional} if room can't be found.
   */
  Optional<List<RoomStateLogExportDto>> export(String platformId);
}
