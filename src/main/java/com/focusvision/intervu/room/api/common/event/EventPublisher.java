package com.focusvision.intervu.room.api.common.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * Application specific event publisher.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class EventPublisher {

  private final ApplicationEventPublisher publisher;

  /**
   * Publishes provided event.
   *
   * @param event Event to publish
   */
  public void publish(Object event) {
    log.debug("Publishing event (event: {})", event);

    publisher.publishEvent(event);
  }
}
