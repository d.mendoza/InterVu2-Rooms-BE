package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.common.provider.ResearchSessionProvider;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service used for research session related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ResearchSessionService extends ResearchSessionProvider {

  /**
   * Fetch research session details by the platform ID.
   *
   * @param platformId ResearchSession platform ID.
   * @return ResearchSession details or empty {@link Optional}.
   */
  Optional<ResearchSession> fetch(String platformId);

  /**
   * Marks research session as started/in progress. Method is also responsible to produce any
   * required event for the rest of the system.
   *
   * @param id ResearchSession ID.
   * @return Updated research session.
   */
  ResearchSession markAsStarted(UUID id);

  /**
   * Marks research session as finished. Method is also responsible to produce any required event
   * for the rest of the system.
   *
   * @param id ResearchSession ID.
   * @return Updated research session.
   */
  ResearchSession markAsFinished(UUID id);

  /**
   * Creates new or updates existing research session data based on the input data. If the existing
   * research session data has bigger or equal version number as the one provided, it will throw an
   * exception.
   *
   * @param researchSessionData Research session data.
   * @throws IntervuRoomException if the session is not editable..
   */
  void createOrUpdate(ResearchSessionData researchSessionData) throws IntervuRoomException;

  /**
   * Get today's upcoming research sessions that Intervu user is involved in.
   *
   * @param platformId Intervu user platform ID.
   * @return List of today's upcoming sessions with participant involved.
   */
  List<ResearchSession> getTodayUpcomingSessions(String platformId);

  /**
   * Gets research sessions by provided participant platform ID.
   *
   * @param participantPlatformId Participant's platform ID.
   * @return List of matching research sessions.
   */
  List<ResearchSession> getResearchSessionsForParticipant(String participantPlatformId);

  /**
   * Validates composition layout that will be used for research session.
   *
   * @param layout Composition layout.
   */
  void validateCompositionLayout(CompositionLayout layout);
}
