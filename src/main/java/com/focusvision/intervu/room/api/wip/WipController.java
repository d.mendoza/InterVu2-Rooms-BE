package com.focusvision.intervu.room.api.wip;

import static java.time.Instant.now;

import com.focusvision.intervu.room.api.configuration.domain.JwtProperties;
import com.focusvision.intervu.room.api.model.dto.AuthDto;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.repository.ParticipantRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * WIP controller.
 */
@Slf4j
@RestController
@Profile(value = {"!production & !staging"})
@RequiredArgsConstructor
public class WipController {

  private final ParticipantRepository participantRepository;
  private final JwtProperties jwtProperties;
  private final DbInitializer dbInitializer;

  @GetMapping("/wip/init")
  @ResponseStatus(HttpStatus.OK)
  public void init(@RequestParam(defaultValue = "100") int cnt) {
    dbInitializer.init(cnt);
  }

  @PostMapping("/wip/log")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void logUiMessage(@RequestBody String message) {
    log.info("UI {}", message);
  }

  /**
   * Generate token.
   *
   * @param platformId Platform ID.
   * @return Auth details.
   */
  @GetMapping("/wip/token/{platformId}")
  public AuthDto generate(@PathVariable String platformId) {

    Participant participant = participantRepository.findAll().stream()
        .filter(p -> p.getPlatformId().equals(platformId))
        .findFirst()
        .orElseThrow();

    Map<String, Object> claims = new HashMap<>();
    claims.put("platformId", participant.getPlatformId());
    claims.put("email", participant.getId().toString());
    claims.put("firstName", participant.getDisplayName());
    claims.put("lastName", participant.getDisplayName());

    var token = Jwts.builder()
        .setSubject(participant.getId().toString())
        .addClaims(claims)
        .setIssuedAt(new Date())
        .setExpiration(new Date(
            now().plusMillis(jwtProperties.getTokenExpirationHours() * 60 * 60 * 1000)
                .toEpochMilli()))
        .signWith(SignatureAlgorithm.HS512, jwtProperties.getTokenSecret())
        .compact();

    return new AuthDto().setToken(token);
  }

  @GetMapping("/wip/refresh")
  public void populate(@RequestParam(defaultValue = "100") int cnt) {
    dbInitializer.repopulate(cnt);
  }
}
