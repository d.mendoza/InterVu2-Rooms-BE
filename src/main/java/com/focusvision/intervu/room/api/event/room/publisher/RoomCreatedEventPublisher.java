package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.room.model.RoomCreatedEvent;

/**
 * Room created event publisher.
 */
public interface RoomCreatedEventPublisher {

  void publish(RoomCreatedEvent event);
}
