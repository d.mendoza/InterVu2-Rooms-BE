package com.focusvision.intervu.room.api.bookmark.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.bookmark.model.BookmarkDetails;
import com.focusvision.intervu.room.api.bookmark.model.BookmarkDetailsDto;
import com.focusvision.intervu.room.api.bookmark.model.PiiBookmarkDetails;
import com.focusvision.intervu.room.api.model.entity.Bookmark;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Mapper for {@link Bookmark} entity.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface BookmarkMapper {

  /**
   * Maps bookmark to DTO.
   *
   * @param bookmark Bookmark to be mapped.
   * @return Resulting DTO.
   */
  @Mappings(value = {
      @Mapping(source = "startOffset", target = "offset"),
      @Mapping(source = "createdAt", target = "addedAt"),
      @Mapping(source = "participant.platformId", target = "addedBy"),
      @Mapping(source = "participant.displayName", target = "addedByDisplayName"),
      @Mapping(target = "note", expression = "java(mapNote(bookmark))")
  })
  BookmarkDetailsDto mapToDetailsDto(Bookmark bookmark);

  /**
   * Maps bookmark to DTO.
   *
   * @param bookmark Bookmark to be mapped.
   * @return Resulting DTO.
   */
  @Mappings(value = {
      @Mapping(source = "startOffset", target = "offset"),
      @Mapping(source = "createdAt", target = "addedAt"),
      @Mapping(source = "participant.platformId", target = "addedBy"),
      @Mapping(source = "participant.displayName", target = "addedByDisplayName")
  })
  BookmarkDetails mapToDetails(Bookmark bookmark);

  /**
   * Maps bookmark to PII DTO.
   *
   * @param bookmark Bookmark to be mapped.
   * @return Resulting DTO.
   */
  @Mappings(value = {
      @Mapping(source = "startOffset", target = "offset"),
      @Mapping(source = "createdAt", target = "addedAt"),
      @Mapping(source = "participant.platformId", target = "addedBy"),
      @Mapping(source = "participant.displayName", target = "addedByDisplayName")
  })
  PiiBookmarkDetails mapToPiiDetails(Bookmark bookmark);

  default String mapNote(Bookmark bookmark) {
    return bookmark.isPii() ? null : bookmark.getNote();
  }

}
