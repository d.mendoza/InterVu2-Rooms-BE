package com.focusvision.intervu.room.api.twilio.service.impl;

import com.focusvision.intervu.room.api.common.provider.ResearchSessionProvider;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.twilio.api.TwilioDialInRequest;
import com.focusvision.intervu.room.api.twilio.model.DialInPin;
import com.focusvision.intervu.room.api.twilio.service.TwilioDialInService;
import com.focusvision.intervu.room.api.twilio.service.TwilioIdentityProvider;
import com.twilio.twiml.VoiceResponse;
import com.twilio.twiml.voice.Connect;
import com.twilio.twiml.voice.Gather;
import com.twilio.twiml.voice.Room;
import com.twilio.twiml.voice.Say;
import java.util.Optional;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link TwilioDialInService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TwilioDialInServiceImpl implements TwilioDialInService {

  private final ResearchSessionProvider researchSessionProvider;
  private final ParticipantService participantService;
  private final TwilioIdentityProvider identityProvider;

  private static final String DIAL_IN_ACTION_URL = "/twilio/dial-in/pin";
  private static final String JOIN_CONFERENCE_MESSAGE = "You will be joined to the conference now.";
  private static final String CONFERENCE_NOT_STARTED_MESSAGE =
      "The conference has not started yet. Please try again later.";
  private static final String INVALID_PIN_MESSAGE = "You've entered invalid conference PIN number.";
  private static final String FINISH_ON_KEY = "#";

  @Override
  public String processDialIn(TwilioDialInRequest request) {
    log.debug("Processing dial-in request (request: {})", request);

    Say sayPin = new Say
        .Builder("Welcome. Please enter the conference PIN followed by #.").build();
    Say sayBye = new Say
        .Builder("We didn't receive any input. Goodbye!").build();
    Gather gather = new Gather.Builder()
        .action(DIAL_IN_ACTION_URL)
        .finishOnKey(FINISH_ON_KEY)
        .inputs(Gather.Input.DTMF)
        .say(sayPin)
        .timeout(10)
        .build();

    return new VoiceResponse.Builder().gather(gather).say(sayBye).build().toXml();
  }

  @Override
  public String processDialInPin(DialInPin dialInPin) {
    log.debug("Processing dial-in PIN request (dialInPin: {})", dialInPin);

    return listenerResponse(dialInPin)
        .or(() -> speakerResponse(dialInPin))
        .or(() -> operatorResponse(dialInPin))
        .or(() -> speakerParticipantResponse(dialInPin))
        .orElse(processInvalidPin());
  }

  private Optional<String> listenerResponse(DialInPin dialInPin) {
    return Optional.ofNullable(dialInPin.pin())
        .flatMap(researchSessionProvider::findByListenerPin)
        .map(this::processListenerParticipant);
  }

  private Optional<String> speakerResponse(DialInPin dialInPin) {
    return Optional.ofNullable(dialInPin.pin())
        .flatMap(researchSessionProvider::findBySpeakerPin)
        .map(this::processSpeakerParticipant);
  }

  private Optional<String> operatorResponse(DialInPin dialInPin) {
    return Optional.ofNullable(dialInPin.pin())
        .flatMap(researchSessionProvider::findByOperatorPin)
        .map(this::processOperatorParticipant);
  }

  private Optional<String> speakerParticipantResponse(DialInPin dialInPin) {
    return Optional.ofNullable(dialInPin.pin())
        .flatMap(participantService::findBySpeakerPin)
        .map(this::processSpeakerParticipant);
  }

  private String processInvalidPin() {
    var say = new Say.Builder(INVALID_PIN_MESSAGE).build();

    return new VoiceResponse.Builder()
        .say(say)
        .build()
        .toXml();
  }

  private String processListenerParticipant(ResearchSession researchSession) {
    return processDialInParticipant(
        researchSession.getConference(),
        () -> identityProvider.generateAnonymousListenerIdentity(researchSession.getId()));
  }

  private String processSpeakerParticipant(ResearchSession researchSession) {
    return processDialInParticipant(
        researchSession.getConference(),
        () -> identityProvider.generateAnonymousSpeakerIdentity(researchSession.getId()));
  }

  private String processSpeakerParticipant(Participant participant) {
    return processDialInParticipant(
        participant.getResearchSession().getConference(),
        () -> identityProvider.generateSpeakerIdentity(participant.getId()));
  }

  private String processOperatorParticipant(ResearchSession researchSession) {
    return processDialInParticipant(
        researchSession.getConference(),
        () -> identityProvider.generateAnonymousOperatorIdentity(researchSession.getId()));
  }

  private String processDialInParticipant(Conference conference,
                                          Supplier<String> participantIdentityProvider) {
    if (isConferenceStarted(conference)) {
      var say = new Say.Builder(JOIN_CONFERENCE_MESSAGE).build();
      var room = new Room.Builder(conference.getRoomSid())
          .participantIdentity(participantIdentityProvider.get()).build();
      var connect = new Connect.Builder()
          .room(room)
          .build();

      return new VoiceResponse.Builder()
          .say(say)
          .connect(connect)
          .build()
          .toXml();
    }

    var say = new Say.Builder(CONFERENCE_NOT_STARTED_MESSAGE).build();

    return new VoiceResponse.Builder()
        .say(say)
        .build()
        .toXml();
  }

  private boolean isConferenceStarted(Conference conference) {
    return conference != null
        && conference.getRoomSid() != null
        && !conference.isCompleted();
  }

}
