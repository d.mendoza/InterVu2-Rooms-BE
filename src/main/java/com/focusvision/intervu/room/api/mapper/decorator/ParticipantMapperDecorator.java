package com.focusvision.intervu.room.api.mapper.decorator;

import com.focusvision.intervu.room.api.configuration.domain.TwilioProperties;
import com.focusvision.intervu.room.api.mapper.ParticipantMapper;
import com.focusvision.intervu.room.api.model.dto.DialInInfoDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantDialInInfoDto;
import com.focusvision.intervu.room.api.model.entity.Participant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Decorator for {@link ParticipantMapper}.
 */
public abstract class ParticipantMapperDecorator implements ParticipantMapper {

  @Autowired
  @Qualifier("delegate")
  private ParticipantMapper delegate;

  @Autowired
  private TwilioProperties twilioProperties;

  @Override
  public DialInInfoDto mapToDialInInfoDto(Participant participant) {
    return delegate.mapToDialInInfoDto(participant)
        .setDialInNumbers(twilioProperties.getDialInNumbers());
  }

  @Override
  public ParticipantDialInInfoDto mapToParticipantDialInInfoDto(Participant participant) {
    return delegate.mapToParticipantDialInInfoDto(participant)
        .setDialInNumbers(twilioProperties.getDialInNumbers());
  }

}
