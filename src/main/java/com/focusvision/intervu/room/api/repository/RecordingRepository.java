package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.entity.Recording.State;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link Recording} entity.
 */
public interface RecordingRepository extends JpaRepository<Recording, UUID> {

  /**
   * Fetches all recordings with provided state.
   *
   * @param state Recording state.
   * @return List of recordings.
   */
  List<Recording> findByState(State state);

  /**
   * Finds conference recording with specific layout and audio channel.
   *
   * @param conference        Conference.
   * @param compositionLayout Composition layout.
   * @param audioChannel      AudioChannel.
   * @return Matching recording or empty {@link Optional}.
   */
  Optional<Recording> findByConferenceAndCompositionLayoutAndAudioChannel(
      Conference conference,
      CompositionLayout compositionLayout,
      AudioChannel audioChannel);
}
