package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * Model for participant microphone toggle event.
 *
 * @author Branko Ostojic
 */
public record ParticipantMicrophoneToggleEvent(UUID roomId,
                                               UUID participantId) {

}
