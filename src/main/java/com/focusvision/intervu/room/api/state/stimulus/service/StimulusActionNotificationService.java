package com.focusvision.intervu.room.api.state.stimulus.service;

import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;

/**
 * Service for sending stimulus action notification.
 */
public interface StimulusActionNotificationService {

  /**
   * Sends poll stimulus vote notification.
   *
   * @param event Poll stimulus vote event.
   */
  void sendPollStimulusVoteNotification(PollStimulusVoteEvent event);

  /**
   * Sends poll stimulus ranking notification.
   *
   * @param event Poll stimulus ranking event.
   */
  void sendPollStimulusRankingNotification(PollStimulusRankingEvent event);

  /**
   * Sends poll stimulus text answer notification.
   *
   * @param event Poll stimulus text answer event.
   */
  void sendPollStimulusTextAnswerNotification(PollStimulusAnswerEvent event);

  /**
   * Sends document stimulus page change notification.
   *
   * @param event Document stimulus page change event.
   */
  void sendDocumentStimulusPageChangeNotification(DocumentStimulusPageChangeEvent event);
}
