package com.focusvision.intervu.room.api.bookmark.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

/**
 * DTO representing the add bookmark details.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Bookmark")
public class AddBookmarkDto {

  @NotNull
  @Length(max = 1000)
  @ApiModelProperty("Bookmark note")
  private String note;
  @NotNull
  @ApiModelProperty("Bookmark timestamp")
  private Long timestamp;
}
