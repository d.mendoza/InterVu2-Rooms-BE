package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.UUID;

/**
 * Service used for chat related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatHandlerService {

  /**
   * Handles (persist/send) internal meeting room chat message.
   *
   * @param sender  Message sender.
   * @param message Message.
   */
  void handleMeetingRoomInternalMessage(AuthenticatedParticipant sender,
                                        String message);

  /**
   * Handles (persist/send) meeting room bookmark chat message.
   *
   * @param sender  Message sender.
   * @param message Message.
   */
  void handleMeetingRoomBookmarkMessage(AuthenticatedParticipant sender,
                                        String message);

  /**
   * Handles (persist/send) meeting room PII bookmark chat message.
   *
   * @param sender Message sender.
   */
  void handleMeetingRoomPiiMessage(AuthenticatedParticipant sender);

  /**
   * Handles (persist/send) meeting room snapshot chat message.
   *
   * @param sender  Message sender.
   * @param message Message.
   */
  void handleMeetingRoomSnapshotMessage(AuthenticatedParticipant sender,
                                        String message);

  /**
   * Handles (persist/send) respondents meeting room chat message.
   *
   * @param sender  Message sender.
   * @param message Message.
   */
  void handleMeetingRoomRespondentsMessage(AuthenticatedParticipant sender,
                                           String message);

  /**
   * Handles (persist/send) internal waiting room chat message.
   *
   * @param sender  Message sender.
   * @param message Message.
   */
  void handleWaitingRoomInternalMessage(AuthenticatedParticipant sender,
                                        String message);

  /**
   * Handles (persist/send) respondents waiting room chat message.
   *
   * @param sender  Message sender.
   * @param message Message.
   */
  void handleWaitingRoomRespondentsMessage(AuthenticatedParticipant sender,
                                           String message);

  /**
   * Handles (persist/send) private waiting room chat message.
   *
   * @param sender  Message sender.
   * @param handle  Message (recipient) handle.
   * @param message Message.
   */
  void handleWaitingRoomPrivateMessage(AuthenticatedParticipant sender,
                                       UUID handle,
                                       String message);

  /**
   * Handles (persist/send) private meeting room chat message.
   *
   * @param sender  Message sender.
   * @param handle  Message (recipient) handle.
   * @param message Message.
   */
  void handleMeetingRoomPrivateMessage(AuthenticatedParticipant sender,
                                       UUID handle,
                                       String message);

  /**
   * Handles seen meeting room internal chat message.
   *
   * @param sender    Message sender.
   * @param messageId Latest seen chat message ID.
   */
  void handleSeenMeetingRoomInternalMessage(UUID messageId,
                                            AuthenticatedParticipant sender);

  /**
   * Handles seen meeting room respondents chat message.
   *
   * @param sender    Message sender.
   * @param messageId Latest seen chat message ID.
   */
  void handleSeenMeetingRoomRespondentsMessage(UUID messageId,
                                               AuthenticatedParticipant sender);

  /**
   * Handles seen meeting room private chat message.
   *
   * @param sender    Message sender.
   * @param handle    Private chat handle.
   * @param messageId Latest seen chat message ID.
   */
  void handleSeenMeetingRoomPrivateMessage(UUID messageId,
                                           UUID handle,
                                           AuthenticatedParticipant sender);

  /**
   * Handles seen waiting room internal chat message.
   *
   * @param sender    Message sender.
   * @param messageId Latest seen chat message ID.
   */
  void handleSeenWaitingRoomInternalMessage(UUID messageId,
                                            AuthenticatedParticipant sender);

  /**
   * Handles seen waiting room respondents chat message.
   *
   * @param sender    Message sender.
   * @param messageId Latest seen chat message ID.
   */
  void handleSeenWaitingRoomRespondentsMessage(UUID messageId,
                                               AuthenticatedParticipant sender);

  /**
   * Handles seen waiting room private chat message.
   *
   * @param sender    Message sender.
   * @param handle    Private chat handle.
   * @param messageId Latest seen chat message ID.
   */
  void handleSeenWaitingRoomPrivateMessage(UUID messageId,
                                           UUID handle,
                                           AuthenticatedParticipant sender);
}
