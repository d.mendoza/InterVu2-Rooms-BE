package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.BannedParticipantData;

/**
 * Adapter used for sending notifications to admin app about banned participants.
 */
public interface BannedParticipantNotificationSenderAdapter {

  /**
   * Sends event upon participant ban.
   *
   * @param bannedParticipantData Banned participant data.
   */
  void send(BannedParticipantData bannedParticipantData);
}
