package com.focusvision.intervu.room.api.state.stimulus.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing stimulus action state.
 */
@Entity
@Table(name = "stimulus_action_states")
@Getter
@Setter
@Accessors(chain = true)
public class StimulusActionState {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private UUID participantId;

  @Column
  private UUID stimulusId;

  @Column
  private String content;

}
