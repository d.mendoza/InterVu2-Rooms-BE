package com.focusvision.intervu.room.api.state.stimulus.adapter;

import com.focusvision.intervu.room.api.model.event.StimulusGlobalActionDataEvent;

/**
 * Adapter for sending stimulus global action notifications.
 */
public interface StimulusGlobalActionNotificationSenderAdapter {
  /**
   * Sends the provided stimulus action notification event to specified destination.
   *
   * @param event Stimulus global action notification event to be sent.
   */
  void send(StimulusGlobalActionDataEvent event);
}
