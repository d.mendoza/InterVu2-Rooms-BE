package com.focusvision.intervu.room.api.security.service;

import com.focusvision.intervu.room.api.cache.authentication.service.AuthenticationCachingService;
import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.ErrorCode;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.dto.AuthDto;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.security.PermissionResolver;
import com.focusvision.intervu.room.api.security.TokenProvider;
import com.focusvision.intervu.room.api.service.InvitationService;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.waitingroom.authentication.service.ParticipantAuthenticationService;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link AuthenticationDetailsService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
@CacheConfig(cacheResolver = "cacheResolver")
public class AuthenticationDetailsServiceImpl
    implements AuthenticationDetailsService, AuthenticationCachingService,
    ParticipantAuthenticationService {

  private final ParticipantService participantService;
  private final InvitationService invitationService;
  private final TokenProvider tokenProvider;

  @Override
  @Cacheable(
      condition = "@cacheConfiguration.authenticationCachingEnabled",
      unless = "#result==null")
  public Optional<AuthenticatedParticipant> getDetails(String id) {
    return participantService.findById(UUID.fromString(id))
        .map(this::ensureRoomIsNotFinished)
        .map(this::ensureRoomIsNotCanceled)
        .map(this::ensureRoomIsNotExpired)
        .map(participant -> AuthenticatedParticipant.builder()
            .id(participant.getId())
            .platformId(participant.getPlatformId())
            .displayName(participant.getDisplayName())
            .roomId(participant.getResearchSession().getId())
            .roomPlatformId(participant.getResearchSession().getPlatformId())
            .roomName(participant.getResearchSession().getName())
            .role(participant.getRole())
            .canModerate(participant.getModerator())
            .admin(participant.isAdmin())
            .banable(participant.isBanable())
            .guest(participant.isGuest())
            .projectManager(participant.isProjectManager())
            .projectOperator(participant.isProjectOperator())
            .banned(participant.isBanned())
            .authorities(PermissionResolver.resolve(participant))
            .build());
  }

  @Override
  public AuthDto authenticate(String token) {
    return invitationService.findInvitationDetails(token)
        .map(this::ensureRoomIsNotFinished)
        .map(this::ensureRoomIsNotCanceled)
        .map(this::ensureRoomIsNotExpired)
        .map(participant -> tokenProvider.createToken(participant.getId().toString()))
        .map(jwtToken -> new AuthDto().setToken(jwtToken))
        .orElseThrow(() -> new IntervuRoomException("Error authenticating."));
  }

  @Override
  public AuthDto authenticate(String sessionPlatformId, String participantPlatformId) {
    return participantService.findRoomParticipant(sessionPlatformId, participantPlatformId)
        .map(this::ensureRoomIsNotFinished)
        .map(this::ensureRoomIsNotCanceled)
        .map(this::ensureRoomIsNotExpired)
        .map(participant -> tokenProvider.createToken(participant.getId().toString()))
        .map(jwtToken -> new AuthDto().setToken(jwtToken))
        .orElseThrow(() -> new IntervuRoomException("Error authenticating."));
  }

  @Override
  @CacheEvict
  public void clearAuthenticationCache(String participantId) {
    log.debug("Clearing authentication cache for participant {}.", participantId);
  }

  private Participant ensureRoomIsNotFinished(Participant participant) {
    if (participant.getResearchSession().isFinished()) {
      throw new ActionForbiddenException(ErrorCode.ROOM_FINISHED, "Room is finished.");
    }

    return participant;
  }

  private Participant ensureRoomIsNotCanceled(Participant participant) {
    if (participant.getResearchSession().isCanceled()) {
      throw new ActionForbiddenException(ErrorCode.ROOM_CANCELED, "Room is canceled.");
    }

    return participant;
  }

  private Participant ensureRoomIsNotExpired(Participant participant) {
    if (participant.getResearchSession().isExpired()) {
      throw new ActionForbiddenException(ErrorCode.ROOM_EXPIRED, "Room has expired.");
    }

    return participant;
  }
}
