package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.export.model.RoomScrollLogExportDto;
import com.focusvision.intervu.room.api.scroll.model.ScrollLog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link ScrollLog} entity.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface RoomScrollLogExportMapper {

  /**
   * Maps scroll log to export DTO.
   *
   * @param scrollLog          Scroll log to be mapped.
   * @param offset             Log offset.
   * @param stimulusPlatformId Stimulus platform ID.
   * @return Resulting DTO.
   */
  @Mapping(source = "offset", target = "offset")
  @Mapping(source = "stimulusPlatformId", target = "stimulusPlatformId")
  RoomScrollLogExportDto mapToDto(ScrollLog scrollLog, Long offset, String stimulusPlatformId);

}
