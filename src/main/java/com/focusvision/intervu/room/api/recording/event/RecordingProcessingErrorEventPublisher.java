package com.focusvision.intervu.room.api.recording.event;

/**
 * Room recording processing error event publisher.
 */
public interface RecordingProcessingErrorEventPublisher {

  /**
   * Publishes provided event data.
   *
   * @param event Event data.
   */
  void publish(RecordingProcessingErrorEvent event);
}
