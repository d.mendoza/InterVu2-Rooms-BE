package com.focusvision.intervu.room.api.controller;

import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.scroll.model.ScrollLogDto;
import com.focusvision.intervu.room.api.scroll.provider.ScrollStateProvider;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room scroll control related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/scroll", produces = MediaType.APPLICATION_JSON_VALUE)
public class ScrollController {

  private final ScrollStateProvider scrollStateProvider;

  /**
   * Gets scroll state for the specified context.
   *
   * @param contextId Context ID.
   * @param caller    Authenticated user.
   * @return Scroll state.
   */
  @GetMapping("state/{contextId}")
  @ApiOperation(
      value = "Retrieves the scroll state",
      notes = "Retrieves the scroll state for specific context")
  @PreAuthorize("hasAuthority('SCROLL_READ')")
  public ScrollLogDto state(@PathVariable String contextId,
                            @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Retrieving current scroll state for context {}.", contextId);

    return scrollStateProvider.getCurrentState(caller.getRoomId(), fromString(contextId))
        .orElse(new ScrollLogDto());
  }
}
