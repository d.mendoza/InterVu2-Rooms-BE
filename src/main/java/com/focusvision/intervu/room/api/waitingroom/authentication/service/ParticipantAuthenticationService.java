package com.focusvision.intervu.room.api.waitingroom.authentication.service;

import com.focusvision.intervu.room.api.model.dto.AuthDto;

/**
 * Service for participant authentication.
 */
public interface ParticipantAuthenticationService {

  /**
   * Performs checks and authentication of a participant based on the provided invitation token.
   *
   * @param token Invitation token.
   * @return Authentication details.
   */
  AuthDto authenticate(String token);

  /**
   * Performs checks and authentication of a participant based on the provided session and
   * participant platform id.
   *
   * @param sessionPlatformId     Session platform id.
   * @param participantPlatformId Participant platform id.
   * @return Authentication details.
   */
  AuthDto authenticate(String sessionPlatformId, String participantPlatformId);
}
