package com.focusvision.intervu.room.api.participant.testing.application;

/**
 * Service used for participant testing notification.
 *
 * @author Branko Ostojic
 */
public interface ParticipantTestNotificationService {

  /**
   * Notifies about participant testing failure.
   *
   * @param participantPlatformId Participant platform ID.
   * @param roomPlatformId        Room platform ID.
   */
  void notifyTestingFailure(String participantPlatformId, String roomPlatformId);

  /**
   * Notifies about participant testing success.
   *
   * @param participantPlatformId Participant platform ID.
   * @param roomPlatformId        Room platform ID.
   */
  void notifyTestingSuccess(String participantPlatformId, String roomPlatformId);

}
