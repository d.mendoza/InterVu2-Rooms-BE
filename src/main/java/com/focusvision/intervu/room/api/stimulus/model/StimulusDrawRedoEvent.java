package com.focusvision.intervu.room.api.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus draw redo event.
 *
 * @author Branko Ostojic
 */
public record StimulusDrawRedoEvent(UUID roomId,
                                    UUID stimulusId,
                                    UUID participantId,
                                    String content,
                                    Long offset) {

}
