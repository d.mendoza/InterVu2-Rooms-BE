package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.adapter.ChatMessageSenderAdapter;
import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ChatNotificationService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ChatNotificationServiceImpl implements ChatNotificationService {

  private final ChatMessageSenderAdapter sender;

  @Override
  public void send(ChatMessage chatMessage, UUID recipient) {
    log.debug("Sending chat message [{}] to {}", chatMessage, recipient);

    var message = new OutgoingChatMessage()
        .setId(chatMessage.getId())
        .setMessage(chatMessage.getMessage())
        .setSenderId(chatMessage.getSenderId().toString())
        .setSource(chatMessage.getSource())
        .setType(chatMessage.getType())
        .setMessageType(chatMessage.getMessageType())
        .setSenderName(chatMessage.getSenderDisplayName())
        .setHandle(chatMessage.getHandle())
        .setSentAt(chatMessage.getSentAt().toEpochMilli());

    sender.send(message, recipient);
  }
}
