package com.focusvision.intervu.room.api.tracking.repository;

import com.focusvision.intervu.room.api.tracking.model.entity.RoomStateAcknowledgement;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link RoomStateAcknowledgement} entity.
 *
 * @author Branko Ostojic
 */
public interface RoomStateAcknowledgementRepository
    extends JpaRepository<RoomStateAcknowledgement, UUID> {}
