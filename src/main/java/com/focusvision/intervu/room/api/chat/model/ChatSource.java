package com.focusvision.intervu.room.api.chat.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumeration representing chat source.
 *
 * @author Branko Ostojic
 */
@Getter
@RequiredArgsConstructor
public enum ChatSource {
  /**
   * Waiting room chat.
   */
  WAITING_ROOM("waiting-room"),

  /**
   * Meeting room chat.
   */
  MEETING_ROOM("meeting-room");

  /**
   * Chat source representation.
   */
  private final String type;

}
