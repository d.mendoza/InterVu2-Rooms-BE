package com.focusvision.intervu.room.api.common.model;

/**
 * Enum that represents ResearchSession state.
 *
 * @author Branko Ostojic
 */
public enum ResearchSessionState {
  /**
   * Representing an active session.
   */
  SCHEDULED,
  /**
   * Representing a session in progress.
   */
  IN_PROGRESS,
  /**
   * Representing a canceled session.
   */
  CANCELED,
  /**
   * Representing a expired session.
   */
  EXPIRED,
  /**
   * Representing a finished session.
   */
  FINISHED
}
