package com.focusvision.intervu.room.api.export.service;

import static java.util.stream.Collectors.toMap;

import com.focusvision.intervu.room.api.export.mapper.StimulusActionLogMapper;
import com.focusvision.intervu.room.api.export.model.StimulusActionLogExportDto;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionLog;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusActionLogService;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link StimulusActionExportService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusActionExportServiceImpl implements StimulusActionExportService {

  private final StimulusActionLogService stimulusActionLogService;
  private final RoomOffsetProvider roomOffsetProvider;
  private final ResearchSessionService researchSessionService;
  private final StimulusActionLogMapper stimulusActionLogMapper;
  private final StimulusService stimulusService;
  private final ParticipantService participantService;

  @Override
  public Optional<List<StimulusActionLogExportDto>> export(String platformId) {
    log.debug("Exporting stimulus action logs for room wit platform ID {}", platformId);

    return researchSessionService.fetch(platformId)
        .map(this::getLogs);
  }

  private List<StimulusActionLogExportDto> getLogs(ResearchSession room) {
    var stimulusMap = stimulusService.getAll(room.getId()).stream()
        .collect(toMap(Stimulus::getId, Stimulus::getPlatformId));
    var participantMap = participantService.findRoomParticipants(room.getId()).stream()
        .collect(toMap(Participant::getId, Participant::getPlatformId));

    return room.getStimuli().stream()
        .map(Stimulus::getId)
        .map(stimulusActionLogService::getLogsForStimulus)
        .flatMap(Collection::stream)
        .map(log -> mapToDto(
            log,
            room.getConference().getRecordingOffset(),
            stimulusMap.get(log.getStimulusId()),
            participantMap.get(log.getParticipantId())))
        .toList();
  }

  private StimulusActionLogExportDto mapToDto(
      StimulusActionLog stimulusActionLog,
      Integer roomRecordingOffset,
      String stimulusPlatformId,
      String participantPlatformId) {
    var alignedOffset = roomOffsetProvider
        .alignWithRecordingOffset(stimulusActionLog.getOffset(), roomRecordingOffset);

    return stimulusActionLogMapper.mapToDto(stimulusActionLog, alignedOffset, stimulusPlatformId,
        participantPlatformId);
  }
}
