package com.focusvision.intervu.room.api.security;

import static io.netty.handler.codec.http.HttpHeaders.Values.APPLICATION_JSON;
import static java.util.Optional.ofNullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.handler.CustomResponseEntityExceptionHandler;
import com.focusvision.intervu.room.api.security.service.AuthenticationDetailsService;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Custom room token authorization filter.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@RequiredArgsConstructor
public class RoomAuthenticationFilter extends OncePerRequestFilter {

  private final ObjectMapper mapper;
  private final TokenProvider tokenProvider;
  private final AuthenticationDetailsService authenticationDetailsService;
  private final CustomResponseEntityExceptionHandler exceptionHandler;

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {
    return request.getRequestURI().startsWith("/dashboard")
        || request.getRequestURI().startsWith("/waiting-room/session");
  }

  @Override
  public void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    try {
      getJwtFromRequest(request)
          .ifPresent(
              jwt ->
                  tokenProvider
                      .getSubjectFromToken(jwt)
                      .flatMap(authenticationDetailsService::getDetails)
                      .ifPresent(
                          authenticatedParticipant -> {
                            var authentication =
                                new ParticipantTokenBasedAuthentication(
                                    authenticatedParticipant, jwt);
                            SecurityContextHolder.getContext().setAuthentication(authentication);
                            this.setMdcContext(authenticatedParticipant);
                          }));
    } catch (ActionForbiddenException ex) {
      var errorResponse = exceptionHandler.handleForbiddenException(ex, null);
      response.setStatus(errorResponse.getStatusCode().value());
      response.setContentType(APPLICATION_JSON);
      response.getWriter().write(mapper.writeValueAsString(errorResponse.getBody()));
      return;
    }

    chain.doFilter(request, response);
  }

  private Optional<String> getJwtFromRequest(HttpServletRequest request) {
    return ofNullable(request.getHeader("X-Room-Token"))
        .filter(StringUtils::hasText)
        .or(() -> ofNullable(request.getParameter("x-room-token")));
  }

  private void setMdcContext(AuthenticatedParticipant participant) {
    MDC.put("user", "user:PARTICIPANT");
    MDC.put("participantId", "participantId:" + participant.getId());
    MDC.put("participantPlatformId", "participantPlatformId:" + participant.getPlatformId());
    MDC.put("roomPlatformId", "roomPlatformId:" + participant.getRoomPlatformId());
    MDC.put("roomDisplayName", "roomDisplayName:" + participant.getRoomName());
  }
}
