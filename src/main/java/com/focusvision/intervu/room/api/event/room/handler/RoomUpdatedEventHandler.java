package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.event.room.model.RoomUpdatedEvent;

/**
 * Room updated event publisher.
 */
public interface RoomUpdatedEventHandler {

  void handle(RoomUpdatedEvent event);
}
