package com.focusvision.intervu.room.api.tracking.model;

/**
 * Model representing participant action types.
 *
 * @author Branko Ostojic
 */
public enum ParticipantActionType {

  MIC_ON,
  MIC_OFF,
  CAMERA_ON,
  CAMERA_OFF,
  SCREEN_SHARE_ON,
  SCREEN_SHARE_OFF,
  TRANSLATOR_ON,
  TRANSLATOR_OFF
}
