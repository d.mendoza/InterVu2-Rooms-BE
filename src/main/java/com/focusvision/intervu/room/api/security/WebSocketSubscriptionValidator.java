package com.focusvision.intervu.room.api.security;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractIntervuUser;
import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;
import static java.util.UUID.fromString;
import static java.util.function.Predicate.not;

import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.repository.ResearchSessionRepository;
import com.focusvision.intervu.room.api.security.WebSocketHelper.SubscriptionInfo;
import java.security.Principal;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Validator for WebSocket subscriptions and connections.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class WebSocketSubscriptionValidator {

  private final ResearchSessionRepository researchSessionRepository;
  private final WebSocketHelper wsHelper;

  /**
   * Validates whether subscription is allowed.
   *
   * @param destination Subscription destination.
   * @param caller      Participant invoking subscription.
   */
  public void validateSubscription(String destination, Principal caller) {
    log.info("Validating subscribing user {} to subscription {}", caller.getName(), destination);

    boolean isAllowed;

    SubscriptionInfo subscriptionInfo = wsHelper.getSubscriptionInfo(destination);
    isAllowed = switch (subscriptionInfo.getType()) {
      case ROOM -> this.isRoomSubscriptionAllowedForParticipant(caller, subscriptionInfo)
          || this.isRoomSubscriptionAllowedForIntervuUser(caller, subscriptionInfo);
      case PARTICIPANT ->
          this.isParticipantSubscriptionAllowedForParticipant(caller, subscriptionInfo)
              || this.isParticipantSubscriptionAllowedForIntervuUser(caller, subscriptionInfo);
      case CHAT -> this.isChatSubscriptionAllowedForIntervuUser(caller, subscriptionInfo)
          || this.isChatSubscriptionAllowedForParticipant(caller, subscriptionInfo)
          || this.isChatSubscriptionAllowedForRoom(caller, subscriptionInfo);
      case DRAW -> this.isDrawSubscriptionAllowedForParticipant(caller, subscriptionInfo);
      case STIMULUS -> this.isStimulusSubscriptionAllowedForParticipant(caller, subscriptionInfo);
      case STIMULUS_GLOBAL -> this.isStimulusGlobalSubscriptionAllowedForParticipant(caller,
          subscriptionInfo);
      default -> false;
    };

    if (!isAllowed) {
      log.warn("Denying subscription to {} for caller {}.", destination, caller.getName());
      throw new IntervuRoomException("Subscription not permitted.");
    }

    log.info("Allowing subscription to {} for caller {}.", destination, caller.getName());
  }

  private boolean isParticipantSubscriptionAllowedForParticipant(
      Principal caller,
      SubscriptionInfo subscriptionInfo) {
    return extractParticipant(caller)
        .map(AuthenticatedParticipant::getId)
        .filter(id -> id.equals(fromString(subscriptionInfo.getDestination())))
        .isPresent();
  }

  private boolean isParticipantSubscriptionAllowedForIntervuUser(
      Principal caller,
      SubscriptionInfo subscriptionInfo) {
    return extractIntervuUser(caller)
        .map(IntervuUser::getPlatformId)
        .filter(platformId -> platformId.equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

  private boolean isRoomSubscriptionAllowedForIntervuUser(Principal caller,
                                                          SubscriptionInfo subscriptionInfo) {
    return extractIntervuUser(caller)
        .map(IntervuUser::getPlatformId)
        .map(platformId ->
            researchSessionRepository.findById(fromString(subscriptionInfo.getDestination()))
                .stream()
                .flatMap(researchSession -> researchSession.getParticipants().stream())
                .anyMatch(participant -> participant.getPlatformId().equals(platformId)))
        .orElse(false);
  }

  private boolean isRoomSubscriptionAllowedForParticipant(Principal caller,
                                                          SubscriptionInfo subscriptionInfo) {
    return extractParticipant(caller)
        .map(AuthenticatedParticipant::getRoomId)
        .filter(id -> id.toString().equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

  private boolean isChatSubscriptionAllowedForIntervuUser(Principal caller,
                                                          SubscriptionInfo subscriptionInfo) {
    return extractIntervuUser(caller)
        .map(IntervuUser::getPlatformId)
        .filter(platformId -> platformId.equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

  private boolean isChatSubscriptionAllowedForParticipant(Principal caller,
                                                          SubscriptionInfo subscriptionInfo) {
    return extractParticipant(caller)
        .map(participant -> participant.getId().toString())
        .filter(id -> id.equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

  private boolean isChatSubscriptionAllowedForRoom(Principal caller,
                                                   SubscriptionInfo subscriptionInfo) {
    return extractParticipant(caller)
        .filter(not(AuthenticatedParticipant::isRespondent))
        .map(AuthenticatedParticipant::getRoomId)
        .map(UUID::toString)
        .filter(id -> id.equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

  private boolean isDrawSubscriptionAllowedForParticipant(Principal caller,
                                                          SubscriptionInfo subscriptionInfo) {
    return extractParticipant(caller)
        .map(AuthenticatedParticipant::getRoomId)
        .filter(id -> id.toString().equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

  private boolean isStimulusSubscriptionAllowedForParticipant(Principal caller,
                                                              SubscriptionInfo subscriptionInfo) {
    return extractParticipant(caller)
        .filter(participant -> participant.canModerate() || participant.isObserver()
            || participant.isTranslator())
        .map(AuthenticatedParticipant::getRoomId)
        .map(UUID::toString)
        .filter(uuid -> uuid.equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

  private boolean isStimulusGlobalSubscriptionAllowedForParticipant(
      Principal caller,
      SubscriptionInfo subscriptionInfo) {
    return extractParticipant(caller)
        .filter(AuthenticatedParticipant::isRespondent)
        .map(AuthenticatedParticipant::getRoomId)
        .map(UUID::toString)
        .filter(uuid -> uuid.equals(subscriptionInfo.getDestination()))
        .isPresent();
  }

}
