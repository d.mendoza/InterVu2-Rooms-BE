package com.focusvision.intervu.room.api.model.entity;

import com.focusvision.intervu.room.api.common.model.ChatType;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Entity representing chat message.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "chat_messages")
@Getter
@Setter
@Accessors(chain = true)
@ToString
public class ChatMessage {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  @Column
  @Enumerated(EnumType.STRING)
  private ChatSource source;
  @Column
  @Enumerated(EnumType.STRING)
  private ChatType type;
  @Column
  @Enumerated(EnumType.STRING)
  private MessageType messageType;
  @Column
  @ToString.Exclude
  private String message;
  @Column
  private UUID senderId;
  @Column
  private String senderPlatformId;
  @Column
  private String senderDisplayName;
  @Column
  private UUID handle;
  @Column
  private UUID researchSessionId;
  @Column
  private Instant sentAt;

  /**
   * Enumeration representing chat message source.
   */
  public enum ChatSource {
    /**
     * Waiting room chat message.
     */
    WAITING_ROOM,

    /**
     * Meeting room chat message.
     */
    MEETING_ROOM
  }

  /**
   * Enumeration representing chat message type.
   */
  public enum MessageType {
    /**
     * Regular chat message.
     */
    REGULAR,
    /**
     * Bookmark message.
     */
    BOOKMARK,
    /**
     * PII message.
     */
    PII,
    /**
     * Snapshot message.
     */
    SNAPSHOT,
  }

}
