package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Poll stimulus respondent result DTO.
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel("PollStimulusRespondentResultDto")
public class PollStimulusRespondentResultDto {

  private UUID stimulusId;
  private List<String> results;
}
