package com.focusvision.intervu.room.api.participant.testing.application;

import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingInfo;
import java.util.UUID;

/**
 * Service for participant testing operations.
 *
 * @author Branko Ostojic
 */
public interface ParticipantTestingService {

  /**
   * Gets participant testing info for specified room (research session).
   *
   * @param roomId Room ID.
   * @return Participant testing info.
   */
  ParticipantTestingInfo getInfo(UUID roomId);

  /**
   * Reports that participant failed testing.
   *
   * @param participantPlatformId Participant platform ID.
   * @param roomPlatformId        Room platform ID.
   */
  void reportSuccess(String participantPlatformId, String roomPlatformId);

  /**
   * Reports that participant passed testing.
   *
   * @param participantPlatformId Participant platform ID.
   * @param roomPlatformId        Room platform ID.
   */
  void reportFail(String participantPlatformId, String roomPlatformId);
}
