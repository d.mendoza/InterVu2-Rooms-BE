package com.focusvision.intervu.room.api.state.model;

import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.CANCELED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.EXPIRED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.FINISHED;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.IN_PROGRESS;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static java.util.Optional.ofNullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.ProjectType;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the state of room.
 */
@Data
@Builder
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomState {

  // TODO: check all what is used here.
  //  It looks like not all things are room state properties really.
  private Long version;
  private Long previousVersion;
  private UUID roomId;
  private String roomName;
  private String projectName;
  private ResearchSessionState state;
  private Instant startsAt;
  private Instant endsAt;
  private Instant startedAt;
  private Instant endedAt;
  private boolean useWebcams;
  private String projectNumber;
  private boolean privacy;
  private String roomChannel;
  private List<ParticipantState> participants;
  private List<StimulusState> stimuli;
  private boolean stimulusInFocus;
  private boolean drawingInSync;
  private boolean respondentsChatEnabled;
  private ProjectType projectType;
  private AudioChannel audioChannel;

  @JsonIgnore
  public boolean isPending() {
    return SCHEDULED.equals(state);
  }

  @JsonIgnore
  public boolean isRunning() {
    return IN_PROGRESS.equals(state);
  }

  /**
   * Mark state as started.
   *
   * @param startedAt Start time.
   * @return Room state.
   */
  @JsonIgnore
  public RoomState markAsStarted(Instant startedAt) {
    this.state = IN_PROGRESS;
    this.startedAt = startedAt;
    return this;
  }

  /**
   * Mark state as finished.
   *
   * @param finishedAt Finish time.
   * @return Room state.
   */
  @JsonIgnore
  public RoomState markAsFinished(Instant finishedAt) {
    this.state = FINISHED;
    this.endedAt = finishedAt;
    return this;
  }

  @JsonIgnore
  public RoomState markAsCanceled() {
    this.state = CANCELED;
    return this;
  }

  @JsonIgnore
  public RoomState markAsExpired() {
    this.state = EXPIRED;
    return this;
  }

  @JsonIgnore
  public RoomState enableStimulusFocus() {
    this.stimulusInFocus = true;
    return this;
  }

  @JsonIgnore
  public RoomState disableStimulusFocus() {
    this.stimulusInFocus = false;
    return this;
  }

  @JsonIgnore
  public RoomState enableDrawingSync() {
    this.drawingInSync = true;
    return this;
  }

  @JsonIgnore
  public RoomState disableDrawingSync() {
    this.drawingInSync = false;
    return this;
  }

  @JsonIgnore
  public RoomState enableRespondentsChat() {
    this.respondentsChatEnabled = true;
    return this;
  }

  @JsonIgnore
  public RoomState disableRespondentsChat() {
    this.respondentsChatEnabled = false;
    return this;
  }

  /**
   * Gets active stimulus if present.
   *
   * @return Active stimulus or empty {@code Optional}.
   */
  @JsonIgnore
  public Optional<StimulusState> getActiveStimulus() {
    return ofNullable(stimuli)
        .stream()
        .flatMap(Collection::stream)
        .filter(StimulusState::isActive)
        .findFirst();
  }
}
