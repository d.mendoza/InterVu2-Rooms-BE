package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the stimuli action state.
 */
@Data
@Accessors(chain = true)
@ApiModel("StimulusState")
public class StimulusStateDto {

  @ApiModelProperty("ID of the stimulus on which action is performed.")
  private UUID stimulusId;

  @ApiModelProperty("ID of the participant who performed action.")
  private UUID participantId;

  @ApiModelProperty("Content of action.")
  private String content;
}
