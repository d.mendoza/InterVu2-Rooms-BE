package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import java.util.UUID;

/**
 * Service used for room state related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface RoomStateService {

  /**
   * Prepares the current room global data.
   *
   * @param roomId Room ID.
   * @return Room state data.
   */
  RoomStateDto getRoomState(UUID roomId);

}
