package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.export.model.RoomStateExportDto;
import com.focusvision.intervu.room.api.export.model.RoomStateLogExportDto;
import com.focusvision.intervu.room.api.export.model.StimulusExportDto;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import com.focusvision.intervu.room.api.state.model.StimulusState;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link RoomStateLog} entity.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface RoomStateLogExportMapper {

  /**
   * Maps room state log to DTO for export.
   *
   * @param roomStateLog Room state log to be mapped.
   * @param offset       Log offset.
   * @return Resulting DTO.
   */
  @Mapping(source = "offset", target = "offset")
  RoomStateLogExportDto mapToExportDto(RoomStateLog roomStateLog, Long offset);

  /**
   * Maps room state to DTO.
   *
   * @param roomState Room state to be mapped.
   * @return Resulting DTO.
   */
  @Mapping(target = "stimulus", expression = "java(map(extractActiveStimulusState(roomState)))")
  RoomStateExportDto map(RoomState roomState);

  /**
   * Maps stimuli state to DTO.
   *
   * @param stimulusState Room state to be mapped.
   * @return Resulting DTO.
   */
  StimulusExportDto map(StimulusState stimulusState);

  default StimulusState extractActiveStimulusState(RoomState roomState) {
    return roomState.getActiveStimulus().orElse(null);
  }

}
