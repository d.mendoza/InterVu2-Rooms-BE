package com.focusvision.intervu.room.api.model.entity;

import static com.focusvision.intervu.room.api.common.model.ProjectType.SELF_SERVICE;

import com.focusvision.intervu.room.api.common.model.ProjectType;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing research project.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "projects")
@Getter
@Setter
@Accessors(chain = true)
public class Project extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  @Column
  private String platformId;
  @Column
  private String name;
  @Column
  private String projectNumber;
  @Column
  @Enumerated(EnumType.STRING)
  private ProjectType type;

  @OneToMany(
      fetch = FetchType.LAZY,
      mappedBy = "project",
      cascade = CascadeType.ALL,
      orphanRemoval = true)
  private Set<ResearchSession> researchSessions = new HashSet<>();

  public boolean isSelfService() {
    return SELF_SERVICE.equals(type);
  }

}
