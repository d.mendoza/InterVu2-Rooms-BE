package com.focusvision.intervu.room.api.state.stimulus.service;

import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import com.focusvision.intervu.room.api.state.stimulus.repository.StimulusStateRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation for {@link StimulusStateService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusStateServiceImpl implements StimulusStateService {

  private final StimulusStateRepository stimulusStateRepository;

  @Override
  public Optional<StimulusActionState> getStimulusStateForParticipant(UUID stimulusId,
                                                                      UUID participantId) {
    log.debug("Finding current stimulus {} state for participant {}.", stimulusId, participantId);

    return stimulusStateRepository.findByStimulusIdAndParticipantId(stimulusId, participantId);
  }

  @Override
  public List<StimulusActionState> getStimulusState(UUID stimulusId) {
    log.debug("Finding current stimulus {} state for all participants.", stimulusId);

    return stimulusStateRepository.findByStimulusId(stimulusId);
  }

  @Override
  public StimulusActionState save(StimulusActionState stimulusActionState) {
    log.debug("Saving stimulus state [{}]", stimulusActionState);

    return stimulusStateRepository.save(stimulusActionState);
  }

  @Override
  @Transactional
  public void resetState(UUID stimulusId) {
    log.debug("Resetting stimulus state [{}]", stimulusId);

    stimulusStateRepository.deleteByStimulusId(stimulusId);
  }
}
