package com.focusvision.intervu.room.api.controller;

import static java.util.Objects.nonNull;
import static java.util.UUID.fromString;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.mapper.StimulusMapper;
import com.focusvision.intervu.room.api.model.dto.StimuliThumbnailsDto;
import com.focusvision.intervu.room.api.model.dto.StimulusDto;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.StimulusService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room stimuli related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/stimuli", produces = MediaType.APPLICATION_JSON_VALUE)
public class StimuliController {

  private final StimulusService stimulusService;
  private final StimulusMapper stimulusMapper;

  /**
   * Gets room stimuli data.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping
  @ApiOperation(
      value = "Gets room stimuli data.",
      notes = "Gets room stimuli data.")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public List<StimulusDto> list(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching room {} stimuli data by {}.", caller.getRoomId(), caller.getId());

    return stimulusService.getAll(caller.getRoomId()).stream()
        .map(stimulusMapper::mapToDto)
        .toList();
  }

  /**
   * Gets room stimulus data.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @GetMapping("{id}")
  @ApiOperation(
      value = "Retrieve details of a stimulus.",
      notes = "Returns details for room stimulus.")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public StimulusDto details(@ApiParam("stimulus ID") @PathVariable String id,
                             @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching room {} stimuli {} data by {}", caller.getRoomId(), id, caller.getId());

    return stimulusService.find(fromString(id), caller.getRoomId())
        .map(stimulusMapper::mapToDto)
        .orElseThrow(ResourceNotFoundException::new);
  }

  /**
   * Activates room stimulus.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @PutMapping("{id}/activate")
  @ApiOperation(
      value = "Activate a stimulus",
      notes = "Activates room stimuli")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void activate(@ApiParam("Stimulus ID") @PathVariable String id,
                       @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Activating room {} stimulus {} by {}.", caller.getRoomId(), id, caller.getId());

    stimulusService.activate(fromString(id), caller.getRoomId());
  }

  /**
   * Deactivates room stimulus.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @PutMapping("{id}/deactivate")
  @ApiOperation(
      value = "Deactivate a stimulus",
      notes = "Deactivates room stimuli")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void deactivate(@ApiParam("Stimulus ID") @PathVariable String id,
                         @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Deactivating room {} stimulus {} by {}.", caller.getRoomId(), id, caller.getId());

    stimulusService.deactivate(fromString(id), caller.getRoomId());
  }

  /**
   * Enables interaction with active stimulus.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("interaction/enable")
  @ApiOperation(
      value = "Enable interaction with current stimulus",
      notes = "Enables interaction with current room stimuli")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void start(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enabling interaction with room {} current stimulus by {}.", caller.getRoomId(),
        caller.getId());

    stimulusService.enableStimulusInteraction(caller.getRoomId());
  }

  /**
   * Disables interaction with active stimulus.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("interaction/disable")
  @ApiOperation(
      value = "Disable interaction with current stimulus",
      notes = "Disables interaction with current room stimuli")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void stop(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Disabling interaction with room {} current stimulus by {}.", caller.getRoomId(),
        caller.getId());

    stimulusService.disableStimulusInteraction(caller.getRoomId());
  }

  /**
   * Enables focus on active stimulus.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("enable")
  @ApiOperation(
      value = "Enables focus",
      notes = "Enables stimuli focus")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void enableFocus(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enabling stimuli focus by {}", caller.getId());

    stimulusService.enableFocus(caller.getRoomId());
  }

  /**
   * Disables focus on active stimulus.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("disable")
  @ApiOperation(
      value = "Disables focus",
      notes = "Disables stimuli focus")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void disableFocus(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Disabling stimuli focus by {}", caller.getId());

    stimulusService.disableFocus(caller.getRoomId());
  }

  /**
   * Reset active stimulus state.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("reset")
  @ApiOperation(
      value = "Reset current stimulus",
      notes = "Resets current room stimulus")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void reset(@ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Resetting room {} current stimulus by {}.", caller.getRoomId(), caller.getId());

    stimulusService.resetStimulus(caller.getRoomId());
  }

  /**
   * Gets thumbnails for all stimulus.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping("thumbnails")
  @ApiOperation(
      value = "Retrieve thumbnails of all stimulus in room.",
      notes = "Returns thumbnails of of all room stimulus.")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public StimuliThumbnailsDto thumbnails(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching room {} thumbnails by {}", caller.getRoomId(), caller.getId());

    return map(caller.getRoomId());
  }

  private StimuliThumbnailsDto map(UUID roomId) {
    return new StimuliThumbnailsDto()
        .setThumbnails(stimulusService.getAll(roomId).stream()
            .filter(stimulus -> nonNull(stimulus.getThumbnail()))
            .collect(toMap(Stimulus::getId, Stimulus::getThumbnail)));
  }
}
