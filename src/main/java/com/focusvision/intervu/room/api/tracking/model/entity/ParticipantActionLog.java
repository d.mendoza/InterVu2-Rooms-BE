package com.focusvision.intervu.room.api.tracking.model.entity;

import com.focusvision.intervu.room.api.model.entity.Auditable;
import com.focusvision.intervu.room.api.tracking.model.ParticipantActionType;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing participant actions log.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "participant_action_logs")
@Getter
@Setter
@Accessors(chain = true)
public class ParticipantActionLog extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  private UUID roomId;
  private UUID participantId;
  @Enumerated(EnumType.STRING)
  private ParticipantActionType type;

  @Column(name = "start_offset")
  private Long offset;
}
