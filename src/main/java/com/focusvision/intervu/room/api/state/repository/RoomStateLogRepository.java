package com.focusvision.intervu.room.api.state.repository;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for {@link RoomStateLog} entity.
 *
 * @author Branko Ostojic
 */
public interface RoomStateLogRepository extends JpaRepository<RoomStateLog, UUID> {

  /**
   * Gets the latest record for specified room ID.
   *
   * @param roomId Room ID.
   * @return Matching record.
   */
  RoomStateLog getTopByRoomIdOrderByVersionDesc(UUID roomId);

  /**
   * Finds the latest record for specified room ID.
   *
   * @param roomId Room ID.
   * @return Matching record or empty {@link Optional}.
   */
  Optional<RoomStateLog> findTopByRoomIdOrderByVersionDesc(UUID roomId);

  /**
   * Finds the record for specified room ID and change type.
   *
   * @param roomId     Room ID.
   * @param changeType Change type.
   * @return Matching record or empty {@link Optional}.
   */
  Optional<RoomStateLog> findOneByRoomIdAndChangeType(UUID roomId, RoomStateChangeType changeType);

  /**
   * Finds all state change logs for specified room, ordered by offset and creation timestamp.
   *
   * @param roomId Room ID.
   * @return List of room state change logs.
   */
  List<RoomStateLog> findAllByRoomIdOrderByOffsetAscAddedAtAsc(UUID roomId);

  /**
   * Gets the top 2 room state logs with provided change types ordered by version descending.
   *
   * @param roomId      Room ID.
   * @param changeTypes Room state change types.
   * @return List of two matching room state logs.
   */
  List<RoomStateLog> getTop2ByRoomIdAndChangeTypeInOrderByVersionDesc(
      UUID roomId,
      List<RoomStateChangeType> changeTypes);
}
