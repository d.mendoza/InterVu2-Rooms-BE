package com.focusvision.intervu.room.api.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

/**
 * Abstract class representing the state event.
 *
 * @author Branko Ostojic
 */
@Getter
public abstract class StateEvent<T> {

  /**
   * Event destination.
   */
  @ApiModelProperty("Event destination")
  protected final String destination;
  /**
   * Event data payload.
   */
  @ApiModelProperty("Event data")
  protected final T data;

  @JsonCreator
  public StateEvent(T data, String destination) {
    this.data = data;
    this.destination = destination;
  }

  /**
   * Gets the event type.
   *
   * @return Event type.
   */
  public abstract EventType getEventType();

}
