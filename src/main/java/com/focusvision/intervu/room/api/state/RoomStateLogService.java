package com.focusvision.intervu.room.api.state;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_OFFLINE;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_FINISHED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_STARTED;
import static java.time.Instant.now;

import com.focusvision.intervu.room.api.cache.room.log.service.RoomLogCachingService;
import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import com.focusvision.intervu.room.api.state.repository.RoomStateLogRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Service for room state log operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
@CacheConfig(cacheResolver = "cacheResolver")
public class RoomStateLogService implements RoomLogCachingService {

  private final RoomStateLogRepository roomStateLogRepository;

  /**
   * Gets the current (last) room state change log data.
   *
   * @param roomId Room ID.
   * @return Current room state change log data.
   */
  public RoomStateLog getCurrentForRoom(UUID roomId) {
    log.debug("Getting room {} current state.", roomId);

    return roomStateLogRepository.getTopByRoomIdOrderByVersionDesc(roomId);
  }

  /**
   * Finds current (last) room state change log data.
   *
   * @param roomId Room ID.
   * @return Current room state change log data or empty {@link Optional}.
   */
  public Optional<RoomStateLog> findCurrentForRoom(UUID roomId) {
    log.debug("Finding room {} current state.", roomId);

    return roomStateLogRepository.findTopByRoomIdOrderByVersionDesc(roomId);
  }

  /**
   * Finds room state change log data representing the start of room.
   * Results are cached for performance gain.
   *
   * @param roomId Room ID.
   * @return Matching room state change log data or empty {@link Optional}.
   */
  @Cacheable(condition = "@cacheConfiguration.roomLogCachingEnabled", unless = "#result==null")
  public Optional<RoomStateLog> findRoomStartedLogForRoom(UUID roomId) {
    log.debug("Finding room {} start change state.", roomId);

    return roomStateLogRepository.findOneByRoomIdAndChangeType(roomId, ROOM_STARTED);
  }

  /**
   * Persists the new room state and change info like offset and change type.
   *
   * @param roomState  Current room state data.
   * @param changeType Change type.
   * @return Saved room state change log.
   */
  public RoomStateLog save(RoomState roomState, RoomStateChangeType changeType, Long offset) {
    log.debug("Saving room state (roomState: {}, changeType: {}, offset: {})",
        roomState, changeType, offset);

    roomState.setPreviousVersion(roomState.getVersion());
    roomState.setVersion(now().toEpochMilli());

    var roomStateLog =
        new RoomStateLog()
            .setRoomId(roomState.getRoomId())
            .setOffset(offset)
            .setChangeType(changeType)
            .setRoomState(roomState)
            .setVersion(roomState.getVersion())
            .setPreviousVersion(roomState.getPreviousVersion())
            .setAddedAt(now());

    return roomStateLogRepository.save(roomStateLog);
  }

  /**
   * Gets the list of room state change logs ordered by offset.
   *
   * @param roomId Room ID.
   * @return List of room state change logs.
   */
  public List<RoomStateLog> getLogsForRoom(UUID roomId) {
    log.debug("Getting room {} change logs.", roomId);

    return roomStateLogRepository.findAllByRoomIdOrderByOffsetAscAddedAtAsc(roomId);
  }

  /**
   * Gets tha last two ROOM_FINISHED and PARTICIPANT_OFFLINE room state logs for the given room.
   *
   * @param roomId Room ID.
   * @return List of two matching room state logs.
   */
  public List<RoomStateLog> getLastRoomFinishedAndParticipantOfflineLogs(UUID roomId) {
    log.debug(
        "Getting the last two ROOM_FINISHED and PARTICIPANT_OFFLINE room state logs for room {}",
        roomId);

    return roomStateLogRepository.getTop2ByRoomIdAndChangeTypeInOrderByVersionDesc(
        roomId, List.of(ROOM_FINISHED, PARTICIPANT_OFFLINE));
  }

  @Override
  @CacheEvict
  public void clearRoomLogCache(UUID roomId) {
    log.info("Clearing room log cache (roomId: {})", roomId);
  }
}
