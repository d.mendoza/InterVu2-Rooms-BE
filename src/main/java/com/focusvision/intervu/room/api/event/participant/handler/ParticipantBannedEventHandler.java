package com.focusvision.intervu.room.api.event.participant.handler;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantBannedEvent;

/**
 * Participant ban event handler.
 */
public interface ParticipantBannedEventHandler {

  /**
   * Handles participant ban event.
   *
   * @param event Event data.
   */
  void handle(ParticipantBannedEvent event);
}
