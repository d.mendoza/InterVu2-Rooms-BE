package com.focusvision.intervu.room.api.state.stimulus.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.model.event.StimulusGlobalActionDataEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionNotificationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * {@link SimpMessagingTemplate} specific implementation of a {@link
 * StimulusActionNotificationSenderAdapter}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SimpleMessagingStimulusActionNotificationSender implements
    StimulusActionNotificationSenderAdapter,
    StimulusGlobalActionNotificationSenderAdapter {

  private final SimpMessagingTemplate messagingTemplate;

  @Override
  public void send(String destination, StimulusActionNotificationMessage message) {
    log.debug("Sending stimulus action event [{}]", message);

    try {
      messagingTemplate.convertAndSend(destination, message);
    } catch (MessagingException e) {
      log.error(format("Error sending stimulus action event %s", message), e);
    }
  }

  @Override
  public void send(StimulusGlobalActionDataEvent event) {
    log.debug("Sending stimulus global action event [{}]", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending stimulus global action event [%s]", event), e);
    }
  }
}
