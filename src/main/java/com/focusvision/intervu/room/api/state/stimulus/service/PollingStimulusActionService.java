package com.focusvision.intervu.room.api.state.stimulus.service;

import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import java.util.UUID;

/**
 * Service for handling stimulus actions.
 */
public interface PollingStimulusActionService {

  /**
   * Handle poll stimulus vote action.
   *
   * @param roomId                Room ID.
   * @param participantId         Room participant making vote action.
   * @param stimulusActionMessage Stimulus action message.
   */
  void vote(UUID roomId, UUID participantId, PollStimulusActionMessage stimulusActionMessage);

  /**
   * Handle poll stimulus ranking action.
   *
   * @param roomId                Room ID.
   * @param participantId         Room participant making ranking action.
   * @param stimulusActionMessage Stimulus action message.
   */
  void rank(UUID roomId, UUID participantId, PollStimulusActionMessage stimulusActionMessage);

  /**
   * Handle poll stimulus answer action.
   *
   * @param roomId                Room ID.
   * @param participantId         Room participant making answer action.
   * @param stimulusActionMessage Stimulus action message.
   */
  void answer(UUID roomId, UUID participantId, PollStimulusActionMessage stimulusActionMessage);
}
