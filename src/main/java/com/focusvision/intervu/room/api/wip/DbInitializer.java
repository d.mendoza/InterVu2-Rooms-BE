package com.focusvision.intervu.room.api.wip;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;
import static com.focusvision.intervu.room.api.common.model.ProjectType.FULL_SERVICE;
import static com.focusvision.intervu.room.api.common.model.ResearchSessionState.SCHEDULED;
import static com.focusvision.intervu.room.api.common.model.StimulusType.DOCUMENT_SHARING;
import static com.focusvision.intervu.room.api.common.model.StimulusType.DRAG_AND_DROP;
import static com.focusvision.intervu.room.api.common.model.StimulusType.POLL;
import static java.lang.String.format;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.HOURS;
import static net.bytebuddy.utility.RandomString.make;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.common.model.StimulusType;
import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData.Participant;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData.Stimulus;
import com.focusvision.intervu.room.api.repository.BookmarkRepository;
import com.focusvision.intervu.room.api.repository.ChatMessageRepository;
import com.focusvision.intervu.room.api.repository.ConferenceRepository;
import com.focusvision.intervu.room.api.repository.ParticipantRepository;
import com.focusvision.intervu.room.api.repository.ProjectRepository;
import com.focusvision.intervu.room.api.repository.ResearchSessionRepository;
import com.focusvision.intervu.room.api.repository.StimulusRepository;
import com.focusvision.intervu.room.api.service.InvitationService;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link InvitationService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Profile(value = {"!production & !staging"})
public class DbInitializer {

  private static final int PARTICIPANT_GROUP_CNT = 10;

  private final ParticipantRepository participantRepository;
  private final ResearchSessionRepository researchSessionRepository;
  private final ConferenceRepository conferenceRepository;
  private final StimulusRepository stimulusRepository;
  private final ProjectRepository projectRepository;
  private final ChatMessageRepository chatMessageRepository;
  private final BookmarkRepository bookmarkRepository;
  private final RabbitTemplate rabbitTemplate;
  private final MessagingProperties messagingProperties;

  private static final String STIMULI_DRAG_AND_DROP_TEMPLATE =
      "{\"dragAndDropSections\":[{\"title\":\"Section 1\",\"pictureUrl\":null},"
          + "{\"title\":\"Section 2\",\"pictureUrl\":null}],"
          + "\"dragAndDropCards\":[{\"id\":%d,\"text\":\"Small card\",\"pictureUrl\":null},"
          + "{\"id\":%d,\"text\":\"Lorem ipsum dolor sit amet, "
          + "consectetur adipiscing elit.\",\"pictureUrl\":null},"
          + "{\"id\":%d,\"text\":\"Picture card\",\"pictureUrl\":\"%s\"}]}";

  private static final String STIMULI_POLL_VOTE_TEMPLATE =
      "{\"title\":\"Poll vote\","
          + "\"pictureUrl\":\"%s\","
          + "\"type\":\"MULTIPLE_ANSWER\",\"answers\":[{\"id\":%d,\"text\":\"Short answer\"},"
          + "{\"id\":%d,\"text\":\"Short answer 2\"},"
          + "{\"id\":%d,\"text\":\"Loremipsumdolorsitamet,consecteturadipiscingelit.\"}]}";

  private static final String STIMULI_POLL_TEXT_ANSWER_TEMPLATE =
      "{\"title\":\"Poll text answer\","
          + "\"pictureUrl\":\"%s\","
          + "\"type\":\"TEXT_ANSWER\",\"answers\":null}";

  private static final String STIMULI_POLL_RANKING_TEMPLATE =
      "{\"title\":\"Poll ranking\","
          + "\"pictureUrl\":\"%s\","
          + "\"type\":\"RANKED_LIST\",\"answers\":[{\"id\":%d,\"text\":\"Short answer\"},"
          + "{\"id\":%d,\"text\":\"Short answer 2\"},"
          + "{\"id\":%d,\"text\":\"Loremipsumdolorsitamet,consecteturadipiscingelit.\"}]}";

  private static final String STIMULI_POLL_MULTIPLE_CHOICE_TEMPLATE =
      "{\"title\":\"Poll multiple answers\","
          + "\"pictureUrl\":\"%s\","
          + "\"type\":\"MULTIPLE_CHOICE\",\"answers\":[{\"id\":%d,\"text\":\"Short answer\"},"
          + "{\"id\":%d,\"text\":\"Short answer 2\"},"
          + "{\"id\":%d,\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\"}]}";

  private static final String STIMULI_DOCUMENT_SHARING_TEMPLATE =
      "{\"fileType\":\"image\"," + "\"fileSubType\":\"png\"," + "\"fileUrl\":\"%s\"}";

  /**
   * Repopulates session data.
   *
   * @param cnt Number of generated sessions.
   */
  public void repopulate(int cnt) {
    log.debug("Starting repopulating db");
    bookmarkRepository.findAll().forEach(bookmarkRepository::delete);
    bookmarkRepository.flush();
    chatMessageRepository.findAll().forEach(chatMessageRepository::delete);
    chatMessageRepository.flush();
    conferenceRepository.findAll().forEach(conferenceRepository::delete);
    conferenceRepository.flush();
    stimulusRepository.findAll().forEach(stimulusRepository::delete);
    stimulusRepository.flush();
    participantRepository.findAll().forEach(participantRepository::delete);
    participantRepository.flush();
    researchSessionRepository.findAll().forEach(researchSessionRepository::delete);
    researchSessionRepository.flush();
    projectRepository.findAll().forEach(projectRepository::delete);
    projectRepository.flush();

    init(cnt);
  }

  /**
   * Initialize data.
   *
   * @param sessionCount Number of generated sessions.
   */
  public void init(int sessionCount) {
    log.debug("Starting initializing db");
    var offset = 0;

    for (int i = 1; i <= sessionCount; i++) {

      var participants = new ArrayList<Participant>();
      for (int j = 0; j < PARTICIPANT_GROUP_CNT; j++) {
        participants.add(moderator(i, j));
        participants.add(respondent(i, j));
        participants.add(translator(i, j));
        participants.add(observer(i, j));
        participants.add(admin(i, j));
      }

      var project = new ResearchSessionData.Project();
      project.setId("project" + i);
      project.setProjectNumber(project.getId());
      project.setName("Project " + i);
      project.setType(FULL_SERVICE);

      var stimuli = List.of(
          votePollStimulus(3, "t1"),
          rankPollStimulus(3, "t1"),
          textAnswerPollStimulus(3, "t1"),
          multipleChoicePollStimulus(3, "t1"),
          dragAndDropStimulus(2, "t2"),
          documentSharingStimulus(1, "t3"));

      var start = now().plus(offset, HOURS).truncatedTo(HOURS);
      var end = now().plus(offset + 1, HOURS).truncatedTo(HOURS);
      offset++;
      var researchSessionData =
          new ResearchSessionData()
              .setCompositionLayout(CompositionLayout.GRID)
              .setName("Session " + i)
              .setProject(project)
              .setId(format("%d", i))
              .setStartsAt(start)
              .setEndsAt(end)
              .setVersion(1)
              .setState(SCHEDULED)
              .setUseWebcams(true)
              .setListenerPin(format("00%s", i))
              .setOperatorPin(format("11%s", i))
              .setSpeakerPin(format("22%s", i))
              .setAudioChannel(AudioChannel.NATIVE_AND_TRANSLATOR)
              .setParticipants(participants)
              .setStimuli(stimuli);

      rabbitTemplate.convertAndSend(
          messagingProperties.getResearchSessionUpdatesQueue(), researchSessionData);
    }
  }

  private Participant observer(int sessionId, int participantId) {
    return participant(OBSERVER,
        format("40%s", System.nanoTime()),
        format("o%s_%s", sessionId, participantId))
        .setInvitationToken(format("of%s_%s", sessionId, participantId));
  }

  private Participant admin(int sessionId, int participantId) {
    return participant(OBSERVER,
        format("50%s", System.nanoTime()),
        format("of%s_%s", sessionId, participantId))
        .setName(format("of%s_%s", sessionId, participantId))
        .setInvitationToken(format("a%s_%s", sessionId, participantId))
        .setModerator(true);
  }

  private Participant translator(int sessionId, int participantId) {
    return participant(TRANSLATOR,
        format("30%s", System.nanoTime()),
        format("t%s_%s", sessionId, participantId))
        .setInvitationToken(format("t%s_%s", sessionId, participantId));
  }

  private Participant moderator(int sessionId, int participantId) {
    return participant(MODERATOR,
        format("10%s", System.nanoTime()),
        format("m%s_%s", sessionId, participantId))
        .setAdmin(true)
        .setModerator(true)
        .setBanable(false);
  }

  private Participant respondent(int sessionId, int participantId) {
    return participant(RESPONDENT,
        format("20%s", System.nanoTime()),
        format("r%s_%s", sessionId, participantId))
        .setInvitationToken(format("r%s_%s", sessionId, participantId));
  }

  private Participant participant(ParticipantRole role, String pin, String id) {
    return new Participant()
        .setRole(role)
        .setSpeakerPin(pin)
        .setName(format("%s %s", role, id))
        .setId(id);
  }

  private Stimulus documentSharingStimulus(int position, String thumbnail) {
    return new Stimulus()
        .setPosition(position)
        .setId(make())
        .setName("Stimulus document sharing " + make())
        .setType(DOCUMENT_SHARING)
        .setThumbnail(thumbnail)
        .setData(documentSharing());
  }

  private Stimulus dragAndDropStimulus(int position, String thumbnail) {
    return new Stimulus()
        .setPosition(position)
        .setId(make())
        .setName("Stimulus drag and drop " + make())
        .setType(DRAG_AND_DROP)
        .setThumbnail("t2")
        .setData(dragAndDrop());
  }

  private Stimulus multipleChoicePollStimulus(int position, String thumbnail) {
    return stimulus(position,
        POLL,
        "Stimulus poll multiple choice " + make(),
        thumbnail, multipleChoicePoll());
  }

  private Stimulus textAnswerPollStimulus(int position, String thumbnail) {
    return stimulus(position,
        POLL,
        "Stimulus poll text answer " + make(),
        "t1",
        textAnswerPoll());
  }

  private Stimulus rankPollStimulus(int position, String thumbnail) {
    return stimulus(position,
        POLL,
        "Stimulus poll ranking " + make(),
        thumbnail,
        rankingPoll());
  }

  private Stimulus votePollStimulus(int position, String thumbnail) {
    return stimulus(position,
        POLL,
        "Stimulus poll vote " + make(),
        thumbnail,
        votingPoll());
  }

  private String documentSharing() {
    return format(
        STIMULI_DOCUMENT_SHARING_TEMPLATE,
        "https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMT"
            + "Y3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY1200_CR90,0,630,1200_AL_.jpg");
  }

  private String dragAndDrop() {
    return format(
        STIMULI_DRAG_AND_DROP_TEMPLATE,
        1,
        2,
        3,
        "https://vignette.wikia.nocookie.net/filmguide/images/f/f2/Lord_of_the_rings_the_two_"
            + "towers_2002_intl_original_film_art_2000x.jpg/revision/latest?cb=20190813215951");
  }

  private String multipleChoicePoll() {
    return format(
        STIMULI_POLL_MULTIPLE_CHOICE_TEMPLATE,
        "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTct"
            + "MzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg",
        1,
        2,
        3);
  }

  private String textAnswerPoll() {
    return format(
        STIMULI_POLL_TEXT_ANSWER_TEMPLATE,
        "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOT"
            + "djZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg");
  }

  private String rankingPoll() {
    return format(
        STIMULI_POLL_RANKING_TEMPLATE,
        "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NT"
            + "ctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg",
        1,
        2,
        3);
  }

  private String votingPoll() {
    return format(
        STIMULI_POLL_VOTE_TEMPLATE,
        "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0"
            + "NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg",
        1,
        2,
        3);
  }

  private Stimulus stimulus(int position,
                            StimulusType type,
                            String name, String thumbnail,
                            String data) {
    return new Stimulus()
        .setPosition(position)
        .setId(make())
        .setName(name)
        .setType(type)
        .setThumbnail(thumbnail)
        .setData(data);
  }
}
