package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.streaming.GroupRoom;
import com.focusvision.intervu.room.api.model.streaming.GroupRoomParticipant;
import com.focusvision.intervu.room.api.model.streaming.GroupRoomRecording;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.twilio.rest.video.v1.room.Participant;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Service used for streaming related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface StreamingService {

  /**
   * Creates a group room capable of streaming audio/video data.
   *
   * @param id Group room unique identification.
   * @return Created group room data.
   */
  GroupRoom createGroupRoom(UUID id);

  /**
   * Get details of a specified group room.
   *
   * @param roomSid Group room unique identification.
   * @return Group room data.
   */
  GroupRoom getGroupRoom(String roomSid);

  /**
   * Completes the specified group room.
   *
   * @param roomSid Group room SID.
   */
  void completeGroupRoom(String roomSid);

  /**
   * Creates a participant for conference group room.
   *
   * @param participantId Participant ID.
   * @param roomSid       Group room SID.
   * @return Group room participant data.
   */
  GroupRoomParticipant createGroupRoomParticipant(UUID participantId, String roomSid);

  /**
   * Kicks/removes the specified participant from the specified room.
   *
   * @param roomSid       Room SID.
   * @param participantId Participant ID.
   */
  void kickParticipantFromRoom(String roomSid, UUID participantId);

  /**
   * Creates a group room recording for the specified room, if there are video/audio sources
   * available.
   *
   * @param roomSid      Room SID.
   * @param isPrivacyOn  Is privacy ON.
   * @param layout       Composition layout.
   * @param audioChannel Audio channel.
   * @return Group room recording data.
   */
  GroupRoomRecording createGroupRoomRecording(String roomSid,
                                              boolean isPrivacyOn,
                                              CompositionLayout layout,
                                              AudioChannel audioChannel);

  /**
   * Returns a specified recording data.
   *
   * @param recordingSid Recording SID
   * @return Recording data.
   * @throws IntervuRoomException in case of any error.
   */
  GroupRoomRecording getGroupRoomRecording(String recordingSid) throws IntervuRoomException;

  /**
   * Returns a specified recording media link.
   *
   * @param recordingSid Recording SID
   * @return Recording link.
   * @throws IntervuRoomException in case of any error.
   */
  String getGroupRoomRecordingLink(String recordingSid) throws IntervuRoomException;

  /**
   * Deletes all room recordings.
   *
   * @param roomSid Room SID.
   */
  void deleteGroupRoomRecordings(String roomSid);

  /**
   * Gets all participants room recordings for specified room.
   *
   * @param roomSid Room SID.
   * @return List of all recordings.
   */
  List<ParticipantGroupRoomRecording> getParticipantsRecordings(String roomSid);

  /**
   * Gets all room recordings for specified room and participant.
   *
   * @param roomSid       Room SID.
   * @param participantId Participant ID.
   * @return List of all recordings.
   */
  List<ParticipantGroupRoomRecording> getParticipantRecordings(String roomSid, UUID participantId);

  /**
   * Returns all participants for specific room. In case of ended room it will return all
   * participants. In case of ongoing room - it will return all connected participants.
   *
   * @param roomSid Room SID.
   * @return Room participants.
   */
  Set<Participant> getRoomParticipants(String roomSid);

}
