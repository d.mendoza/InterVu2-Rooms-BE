package com.focusvision.intervu.room.api.stimulus.snapshot;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.UUID;

/**
 * Model representing the stimulus snapshot added event.
 *
 * @author Branko Ostojic
 */
public record SnapshotAddedEvent(
    UUID roomId,
    AuthenticatedParticipant participant,
    String snapshotName) {

}
