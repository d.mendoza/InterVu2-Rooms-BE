package com.focusvision.intervu.room.api.service.impl;

import com.focusvision.intervu.room.api.chat.service.ChatDataService;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.Project;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.repository.ProjectRepository;
import com.focusvision.intervu.room.api.service.ProjectService;
import java.util.Collection;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of a {@link ProjectService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

  private final ProjectRepository projectRepository;
  private final ChatDataService chatDataService;

  @Override
  public Optional<Project> findByPlatformId(String platformId) {
    log.debug("Finding project by platform ID {}.", platformId);

    return projectRepository.findByPlatformId(platformId);
  }

  @Override
  public Project save(Project project) {
    log.debug("Saving project {}.", project);

    return projectRepository.save(project);
  }

  @Override
  @Transactional
  public void sanitiseProject(String projectPlatformId) {
    log.debug("Sanitising project with platform ID {}.", projectPlatformId);

    projectRepository.findByPlatformId(projectPlatformId)
        .ifPresentOrElse(this::sanitiseProject,
            () -> log.error("Sanitising project with platform Id {} failed - project is missing.",
                projectPlatformId));
  }

  private void sanitiseProject(Project project) {
    project.getResearchSessions().stream()
        .map(ResearchSession::getParticipants)
        .flatMap(Collection::stream)
        .filter(Participant::isRespondent)
        .forEach(chatDataService::clearUserHistory);
  }
}
