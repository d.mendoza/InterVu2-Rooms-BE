package com.focusvision.intervu.room.api.scroll.service;

import static java.time.Instant.now;

import com.focusvision.intervu.room.api.scroll.model.ScrollLog;
import com.focusvision.intervu.room.api.scroll.repository.ScrollRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ScrollService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ScrollServiceImpl implements ScrollService {

  private final ScrollRepository scrollRepository;

  @Override
  public ScrollLog saveStimulusScrollAction(UUID roomId,
                                            UUID participantId,
                                            UUID stimulusId,
                                            Long offset,
                                            String content) {
    log.debug("Persisting stimulus {} scroll action: {}", stimulusId, content);

    var scroll = new ScrollLog()
        .setRoomId(roomId)
        .setParticipantId(participantId)
        .setOffset(offset)
        .setContextId(stimulusId)
        .setContent(content)
        .setAddedAt(now());

    return scrollRepository.save(scroll);
  }

  @Override
  public List<ScrollLog> getRoomScrollLogs(UUID roomId) {
    log.debug("Fetching all scroll logs for room {}.", roomId);

    return scrollRepository.findAllByRoomIdOrderByOffsetAscAddedAtAsc(roomId);
  }

  @Override
  public Optional<ScrollLog> getCurrentRoomScrollLog(UUID roomId, UUID contextId) {
    log.debug("Fetching the current scroll log for room {} and context {}.", roomId, contextId);

    return scrollRepository.findFirstByRoomIdAndContextIdOrderByOffsetDescAddedAtDesc(roomId,
        contextId);
  }
}
