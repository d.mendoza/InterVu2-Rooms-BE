package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;

/**
 * Adapter for sending recording state check event.
 */
public interface RecordingCheckSenderAdapter {

  /**
   * Sends event for checking processing recording state.
   *
   * @param conference Processing recording to check.
   */
  void send(ProcessingRecordingCheck conference);

}
