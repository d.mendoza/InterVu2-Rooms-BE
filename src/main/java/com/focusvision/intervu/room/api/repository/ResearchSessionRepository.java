package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for the {@link ResearchSession} entity.
 *
 * @author Branko Ostojic
 */
public interface ResearchSessionRepository extends JpaRepository<ResearchSession, UUID> {

  /**
   * Finds research session by the platform ID.
   *
   * @param platformId Research session platform ID.
   * @return Research session if found or empty {@link Optional}.
   */
  Optional<ResearchSession> findByPlatformId(String platformId);

  /**
   * Searches for research session which are in progress or scheduled within provided date range and
   * containing participant with matching platform ID.
   *
   * @param platformId Participant platform ID.
   * @param from Start of search period range.
   * @param to End of search period range.
   * @return Matching research sessions.
   */
  @Query(
      "SELECT rs FROM ResearchSession rs JOIN rs.participants p WHERE p.platformId = :platformId "
          + "AND p.banned = FALSE AND (rs.state = 'IN_PROGRESS' OR "
          + "(rs.state = 'SCHEDULED' AND "
          + "((rs.startsAt BETWEEN :from AND :to) OR (:from BETWEEN rs.startsAt AND rs.endsAt)))) "
          + "ORDER BY rs.startsAt")
  List<ResearchSession> findParticipantSessions(
      @Param("platformId") String platformId, @Param("from") Instant from, @Param("to") Instant to);

  /**
   * Finds research sessions by provided participant platform ID.
   *
   * @param platformId Participant platform ID.
   * @param state Research session state.
   * @return LIst of matching research sessions.
   */
  List<ResearchSession> findByParticipantsPlatformIdAndState(
      String platformId, ResearchSessionState state);

  /**
   * Finds research session for provided listener PIN.
   *
   * @param listenerPin Listener PIN.
   * @return Matching research session or empty {@link Optional}.
   */
  Optional<ResearchSession> findByListenerPin(String listenerPin);

  /**
   * Finds research session for provided speaker PIN.
   *
   * @param speakerPin Speaker PIN.
   * @return Matching research session or empty {@link Optional}.
   */
  Optional<ResearchSession> findBySpeakerPin(String speakerPin);

  /**
   * Finds research session for provided operator PIN.
   *
   * @param operatorPin Operator PIN.
   * @return Matching research session or empty {@link Optional}.
   */
  Optional<ResearchSession> findByOperatorPin(String operatorPin);
}
