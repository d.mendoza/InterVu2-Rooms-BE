package com.focusvision.intervu.room.api.common.model;

/**
 * Enum that represents recording type.
 *
 * @author Branko Ostojic
 */
public enum RecordingType {
  /**
   * Audio recording.
   */
  AUDIO,
  /**
   * Video recording.
   */
  VIDEO,
  /**
   * Screen share recording.
   */
  SCREENSHARE,
  /**
   * Data recording.
   */
  DATA
}
