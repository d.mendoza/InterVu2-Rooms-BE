package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.event.room.model.RoomStartedEvent;

/**
 * Room started event handler.
 */
public interface RoomStartedEventHandler {

  void handle(RoomStartedEvent event);
}
