package com.focusvision.intervu.room.api.role;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.TRANSLATOR;
import static java.util.Set.of;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.Participant;
import java.util.Set;

/**
 * Service for resolving participant alternative roles.
 *
 * @author Branko Ostojic
 */
public class ParticipantAlternativeRoleResolver {

  /**
   * Resolves alternative roles for provided participant.
   *
   * @param participant Participant.
   * @return Set of alternative roles.
   */
  public static Set<ParticipantRole> resolve(Participant participant) {
    return getAlternativeRoles(participant.getRole());
  }

  private static Set<ParticipantRole> getAlternativeRoles(ParticipantRole role) {
    return switch (role) {
      case MODERATOR, TRANSLATOR -> of(OBSERVER);
      case OBSERVER -> of(MODERATOR);
      default -> of();
    };
  }
}
