package com.focusvision.intervu.room.api.state.handler;

import com.focusvision.intervu.room.api.chat.event.handler.ChatControlEventHandler;
import com.focusvision.intervu.room.api.chat.event.model.RespondentsChatDisabledEvent;
import com.focusvision.intervu.room.api.chat.event.model.RespondentsChatEnabledEvent;
import com.focusvision.intervu.room.api.event.draw.handler.DrawingControlEventHandler;
import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncDisabledEvent;
import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncEnabledEvent;
import com.focusvision.intervu.room.api.event.participant.handler.ParticipantBannedEventHandler;
import com.focusvision.intervu.room.api.event.participant.handler.ParticipantPresenceEventHandler;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantBannedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedToRoomEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingDisabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingEnabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantReadyEvent;
import com.focusvision.intervu.room.api.event.room.handler.RoomCanceledEventHandler;
import com.focusvision.intervu.room.api.event.room.handler.RoomCreatedEventHandler;
import com.focusvision.intervu.room.api.event.room.handler.RoomExpiredEventHandler;
import com.focusvision.intervu.room.api.event.room.handler.RoomFinishedEventHandler;
import com.focusvision.intervu.room.api.event.room.handler.RoomStartedEventHandler;
import com.focusvision.intervu.room.api.event.room.handler.RoomUpdatedEventHandler;
import com.focusvision.intervu.room.api.event.room.model.RoomCanceledEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomCreatedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomExpiredEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomStartedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomUpdatedEvent;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimuliFocusDisabledEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimuliFocusEnabledEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusActivatedEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusBroadcastResultsDisableEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusBroadcastResultsEnableEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusDeactivatedEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusInteractionDisabledEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusInteractionEnabledEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusResetEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusSyncDisabledEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.handler.StimulusSyncEnabledEventHandler;
import com.focusvision.intervu.room.api.event.stimulus.model.StimuliFocusDisabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimuliFocusEnabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusActivatedEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusBroadcastResultsDisableEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusBroadcastResultsEnableEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusDeactivatedEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusInteractionDisabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusInteractionEnabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusResetEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusSyncDisabledEvent;
import com.focusvision.intervu.room.api.event.stimulus.model.StimulusSyncEnabledEvent;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.RoomStateNotificationService;
import com.focusvision.intervu.room.api.state.RoomStateOperator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * {@link org.springframework.context.ApplicationEvent} specific implementation of room state change
 * events handler.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomStateChangeEventHandler
    implements RoomCreatedEventHandler,
        RoomUpdatedEventHandler,
        RoomCanceledEventHandler,
        RoomExpiredEventHandler,
        RoomStartedEventHandler,
        RoomFinishedEventHandler,
        ParticipantPresenceEventHandler,
        ParticipantBannedEventHandler,
        DrawingControlEventHandler,
        ChatControlEventHandler,
        StimulusActivatedEventHandler,
        StimulusDeactivatedEventHandler,
        StimuliFocusEnabledEventHandler,
        StimuliFocusDisabledEventHandler,
        StimulusInteractionEnabledEventHandler,
        StimulusInteractionDisabledEventHandler,
        StimulusResetEventHandler,
        StimulusSyncEnabledEventHandler,
        StimulusSyncDisabledEventHandler,
        StimulusBroadcastResultsEnableEventHandler,
        StimulusBroadcastResultsDisableEventHandler {

  private final RoomStateOperator roomStateOperator;
  private final ParticipantService participantService;
  private final RoomStateNotificationService roomStateNotificationService;

  @Override
  @EventListener
  public void handle(RoomCreatedEvent event) {
    log.info("Handling room created event (event: {})", event);

    var roomState =
        roomStateOperator.create(
            event.roomId(), event.project(), event.researchSession(), event.participants());
    var participantStates =
        event.participants().stream()
            .map(p -> roomStateOperator.constructParticipantState(event.roomId(), p))
            .toList();

    roomStateNotificationService.sendNewRoomNotification(roomState, participantStates);
  }

  @Override
  @EventListener
  public void handle(RoomUpdatedEvent event) {
    log.info("Handling room updated event (event: {})", event);

    roomStateOperator.update(
        event.roomId(),
        event.project(),
        event.researchSession(),
        event.participants(),
        event.stimuli());
  }

  @Override
  @EventListener
  public void handle(RoomCanceledEvent event) {
    log.info("Handling room canceled event (event: {})", event);

    roomStateOperator.markRoomAsCanceled(event.roomId());
  }

  @Override
  @EventListener
  public void handle(RoomExpiredEvent event) {
    log.info("Handling room expired event (event: {})", event);

    roomStateOperator.markRoomAsExpired(event.roomId());
  }

  @Override
  @EventListener
  public void handle(RoomStartedEvent event) {
    log.info("Handling room started event (event: {})", event);

    roomStateOperator
        .markRoomAsStarted(event.roomId(), event.startedAt(), event.participants())
        .stream()
        .flatMap(roomState -> roomState.getParticipants().stream())
        .forEach(roomStateNotificationService::sendParticipantStateNotification);
  }

  @Override
  @EventListener
  public void handle(RoomFinishedEvent event) {
    log.info("Handling room finished event (event: {})", event);

    roomStateOperator.markRoomAsFinished(event.roomId(), event.finishedAt());
  }

  @Override
  @EventListener
  @Deprecated
  public void handle(ParticipantConnectedEvent event) {
    log.info("Handling participant connected event (event: {})", event);
    // For now, we are just logging event
    // roomStateOperator.getCurrentParticipantState(event.getRoomId(), event.getParticipantId())
    //      .ifPresentOrElse(roomStateNotificationService::sendParticipantStateNotification,
    //       () -> log.error("Participant {} state not found.", event.getParticipantId()));
  }

  @Override
  @EventListener
  public void handle(ParticipantReadyEvent event) {
    log.info("Handling participant ready event (event: {})", event);

    roomStateOperator.markParticipantAsReady(event.roomId(), event.participantId()).stream()
        .flatMap(roomState -> roomState.getParticipants().stream())
        .filter(participantState -> participantState.getId().equals(event.participantId()))
        .findFirst()
        .ifPresent(roomStateNotificationService::sendParticipantStateNotification);
  }

  @Override
  @EventListener
  public void handle(ParticipantBannedEvent event) {
    log.info("Handling participant banned event (event: {})", event);

    roomStateOperator.markParticipantAsBanned(event.roomId(), event.participantId()).stream()
        .flatMap(roomState -> roomState.getParticipants().stream())
        .filter(participantState -> participantState.getId().equals(event.participantId()))
        .findFirst()
        .ifPresent(roomStateNotificationService::sendParticipantStateNotification);
  }

  @Override
  @EventListener
  public void handle(ParticipantDrawingEnabledEvent event) {
    log.info("Handling participant drawing enabled event (event: {})", event);

    roomStateOperator
        .enableParticipantDrawing(event.roomId(), event.participants())
        .forEach(roomStateNotificationService::sendParticipantStateNotification);
  }

  @Override
  @EventListener
  public void handle(ParticipantDrawingDisabledEvent event) {
    log.info("Handling participant drawing enabled event (event: {})", event);

    roomStateOperator
        .disableParticipantDrawing(event.roomId(), event.participants())
        .forEach(roomStateNotificationService::sendParticipantStateNotification);
  }

  @Override
  @EventListener
  public void handle(DrawingSyncEnabledEvent event) {
    log.info("Handling drawing sync enabled event (event: {})", event);

    roomStateOperator.enableDrawingSync(event.roomId());
  }

  @Override
  @EventListener
  public void handle(DrawingSyncDisabledEvent event) {
    log.info("Handling drawing sync disabled event (event: {})", event);

    roomStateOperator.disableDrawingSync(event.roomId());
  }

  @Override
  @EventListener
  public void handle(RespondentsChatEnabledEvent event) {
    log.info("Handling respondents chat enabled event (event: {})", event);

    roomStateOperator.enableRespondentsChat(event.roomId());
  }

  @Override
  @EventListener
  public void handle(RespondentsChatDisabledEvent event) {
    log.info("Handling respondents chat disabled event (event: {})", event);

    roomStateOperator.disableRespondentsChat(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusActivatedEvent event) {
    log.info("Handling stimulus activated event (event: {})", event);

    roomStateOperator.activateStimulus(event.roomId(), event.stimulus());
  }

  @Override
  @EventListener
  public void handle(StimulusDeactivatedEvent event) {
    log.info("Handling stimulus deactivated event (event: {})", event);

    roomStateOperator.deactivateStimulus(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusInteractionEnabledEvent event) {
    log.info("Handling stimulus interaction enabled event (event: {})", event);

    roomStateOperator.enableStimulusInteraction(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusInteractionDisabledEvent event) {
    log.info("Handling stimulus interaction disabled event (event: {})", event);

    roomStateOperator.disableStimulusInteraction(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimuliFocusEnabledEvent event) {
    log.info("Handling stimuli focus enabled event (event: {})", event);

    roomStateOperator.enableStimulusFocus(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimuliFocusDisabledEvent event) {
    log.info("Handling stimuli focus disabled event (event: {})", event);

    roomStateOperator.disableStimulusFocus(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusBroadcastResultsEnableEvent event) {
    log.info("Handling polling stimulus broadcast results enable event (event: {})", event);

    roomStateOperator.enableStimulusBroadcastResults(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusBroadcastResultsDisableEvent event) {
    log.info("Handling polling stimulus broadcast disable results event (event: {})", event);

    roomStateOperator.disableStimulusBroadcastResults(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusResetEvent event) {
    log.info("Handling stimulus reset event (event: {})", event);

    roomStateOperator.resetStimulus(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusSyncEnabledEvent event) {
    log.info("Handling stimulus sync enabled event (event: {})", event);

    roomStateOperator.enableStimulusSync(event.roomId());
  }

  @Override
  @EventListener
  public void handle(StimulusSyncDisabledEvent event) {
    log.info("Handling stimulus sync disabled event (event: {})", event);

    roomStateOperator.disableStimulusSync(event.roomId());
  }

  @Override
  @EventListener
  public void handle(ParticipantConnectedToRoomEvent event) {
    log.info("Handling participant connected to room event (event: {})", event);

    participantService
        .findById(event.participantId())
        .ifPresent(
            participant ->
                roomStateOperator.markParticipantAsPresentOverWeb(event.roomId(), participant));
  }

  @Override
  @EventListener
  public void handle(ParticipantConnectedWithPhoneEvent event) {
    log.debug("Handling participant connected with phone event (event: {})", event);

    participantService
        .findById(event.participantId())
        .ifPresent(
            participant ->
                roomStateOperator.markParticipantAsPresentOverPhone(event.roomId(), participant));
  }

  @Override
  @EventListener
  public void handle(ParticipantDisconnectedEvent event) {
    log.info("Handling participant disconnected event (event: {})", event);

    roomStateOperator.markParticipantAsNotPresentOverWeb(event.roomId(), event.participantId());
  }

  @Override
  public void handle(ParticipantDisconnectedWithPhoneEvent event) {
    log.debug("Handling participant disconnected with phone event (event: {})", event);

    roomStateOperator.markParticipantAsNotPresentOverPhone(event.roomId(), event.participantId());
  }
}
