package com.focusvision.intervu.room.api.security;

import static com.focusvision.intervu.room.api.security.IntervuAuthority.INTERVU_USER;

import java.util.Collection;
import java.util.List;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Model representing the InterVu authenticated user details.
 *
 * @author Branko Ostojic
 */
@Data
@ToString
@Accessors(chain = true)
public class IntervuUser implements UserDetails {

  private String platformId;
  @ToString.Exclude
  private String email;
  @ToString.Exclude
  private String firstName;
  @ToString.Exclude
  private String lastName;
  private boolean systemUser;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return List.of(new SimpleGrantedAuthority(INTERVU_USER.name()));
  }

  @Override
  public String getPassword() {
    return null;
  }

  @Override
  public String getUsername() {
    return platformId;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
