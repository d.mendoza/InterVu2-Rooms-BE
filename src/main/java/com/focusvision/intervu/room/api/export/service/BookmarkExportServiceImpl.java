package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.bookmark.service.BookmarkService;
import com.focusvision.intervu.room.api.export.mapper.BookmarkExportMapper;
import com.focusvision.intervu.room.api.export.model.BookmarkExportDto;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.service.ConferenceService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link BookmarkExportService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BookmarkExportServiceImpl implements BookmarkExportService {

  private final BookmarkService bookmarkService;
  private final BookmarkExportMapper bookmarkExportMapper;
  private final ConferenceService conferenceService;

  @Override
  public Optional<List<BookmarkExportDto>> export(String platformId) {
    log.debug("Exporting bookmarks for room with platform ID {}.", platformId);

    return conferenceService.findByResearchSession(platformId)
        .map(this::getBookmarks);
  }

  private List<BookmarkExportDto> getBookmarks(Conference conference) {
    return bookmarkService.getAllBookmarksWithAlignedOffset(conference.getResearchSession(),
            conference.getRecordingOffset())
        .stream()
        .map(bookmarkExportMapper::mapToDto)
        .toList();
  }
}
