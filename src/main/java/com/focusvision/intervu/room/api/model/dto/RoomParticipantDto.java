package com.focusvision.intervu.room.api.model.dto;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.OBSERVER;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the participants brief details.
 */
@Data
@Accessors(chain = true)
@ApiModel("Participant")
public class RoomParticipantDto {

  @ApiModelProperty("Participant's ID")
  private UUID id;
  @ApiModelProperty("Participant's display name")
  private String displayName;
  @ApiModelProperty("Indicates whether the participant is online")
  private boolean online;
  @ApiModelProperty("Participant role")
  private ParticipantRole role;
  @ApiModelProperty("Indicates whether participant is banned from room")
  private boolean banned;
  @ApiModelProperty("Indicates whether participant admin or not")
  private boolean admin;
  @ApiModelProperty("Indicates whether participant can draw in room")
  private boolean drawingEnabled;
  @ApiModelProperty("Indicates whether participant can moderate")
  private boolean moderator;
  @ApiModelProperty("Indicates whether participant can be banned by non-admins")
  private boolean banable;
  @ApiModelProperty("Indicates whether participant is a guest")
  private boolean guest;
  @ApiModelProperty("Indicates whether participant is project manager")
  private boolean projectManager;
  @ApiModelProperty("Indicates whether participant is project operator")
  private boolean projectOperator;

  @JsonIgnore
  public boolean isRespondent() {
    return RESPONDENT.equals(role);
  }

  @JsonIgnore
  public boolean isModeratorParticipant() {
    return MODERATOR.equals(role);
  }

  @JsonIgnore
  public boolean isObserver() {
    return OBSERVER.equals(role);
  }

  @JsonIgnore
  public boolean isInternal() {
    return !isRespondent();
  }
}
