package com.focusvision.intervu.room.api.task;

import com.focusvision.intervu.room.api.adapter.RecordingCheckSenderAdapter;
import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.recording.service.RecordingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduled task for publishing unprocessed recordings for checkup.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(
    prefix = "app.tasks.check-processing-recordings",
    value = "enabled",
    havingValue = "true")
public class RecordingProcessingTask {

  private final RecordingCheckSenderAdapter adapter;
  private final RecordingService recordingService;

  /**
   * Triggers processing recordings check.
   */
  @Scheduled(
      initialDelayString = "${app.tasks.check-processing-recordings.initial-delay:10000}",
      fixedRateString = "${app.tasks.check-processing-recordings.period}")
  @SchedulerLock(
      name = "RecordingProcessingTask",
      lockAtLeastFor = "${app.tasks.check-processing-recordings.lock-period-min}",
      lockAtMostFor = "${app.tasks.check-processing-recordings.lock-period-max}")
  public void run() {
    log.info("Starting processing recordings check task.");

    var recordings = recordingService.getAllUnprocessedRecordings();
    log.info("Found {} processing recordings to check.", recordings.size());

    recordings.stream()
        .map(this::map)
        .forEach(this.adapter::send);

    log.info("Finishing processing recordings check task.");
  }

  private ProcessingRecordingCheck map(Recording recording) {
    return new ProcessingRecordingCheck()
        .setId(recording.getId().toString());
  }
}
