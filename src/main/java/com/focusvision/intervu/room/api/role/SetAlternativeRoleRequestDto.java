package com.focusvision.intervu.room.api.role;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the set alternative role request.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Set alternative role request DTO")
public class SetAlternativeRoleRequestDto {

  @NotNull
  @ApiModelProperty("Alternative role")
  private ParticipantRole role;
}
