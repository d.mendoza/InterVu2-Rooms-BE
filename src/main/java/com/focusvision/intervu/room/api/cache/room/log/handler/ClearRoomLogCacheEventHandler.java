package com.focusvision.intervu.room.api.cache.room.log.handler;

import com.focusvision.intervu.room.api.cache.room.log.event.ClearRoomLogCacheEvent;

/**
 * Handler contract for room log cache eviction event.
 *
 * @author Branko Ostojic
 */
public interface ClearRoomLogCacheEventHandler {

  /**
   * Handles the provided room log cache eviction event.
   *
   * @param event Clear room log cache event.
   */
  void handle(ClearRoomLogCacheEvent event);
}
