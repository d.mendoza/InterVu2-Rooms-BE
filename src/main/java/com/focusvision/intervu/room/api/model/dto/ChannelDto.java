package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the communication channel details.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Channel")
public class ChannelDto {

  @ApiModelProperty("Channel for global room communication")
  private String room;

  @ApiModelProperty("Channel for global room draw communication")
  private String draw;

  @ApiModelProperty("Channel for participant specific communication")
  private String participant;

  @ApiModelProperty("Channel for stimulus specific communication for moderators")
  private String stimulus;

  @ApiModelProperty("Channel for stimulus specific communication for all participants")
  private String globalStimulus;

  @ApiModelProperty("Channel for chat communication")
  private String chat;

}
