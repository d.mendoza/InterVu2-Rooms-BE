package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service used for stimuli related operations.
 *
 * @author Branko Ostojic
 */
public interface StimulusService {

  /**
   * Gets all room stimuli.
   *
   * @param roomId Room ID.
   * @return All stimuli belonging to the room.
   */
  List<Stimulus> getAll(UUID roomId);

  /**
   * Finds stimulus by ID and belonging room ID.
   *
   * @param id     Stimulus ID.
   * @param roomId Room ID.
   * @return Found stimulus or empty {@link Optional}.
   */
  Optional<Stimulus> find(UUID id, UUID roomId);

  /**
   * Finds stimulus by ID with broadcast results enabled.
   *
   * @param id Stimulus ID.
   * @return Found stimulus or empty {@link Optional}.
   */
  Optional<Stimulus> findStimulusWithBroadcastResults(UUID id);

  /**
   * Activates the specified stimulus.
   *
   * @param id     Stimulus ID.
   * @param roomId Room ID.
   */
  void activate(UUID id, UUID roomId);

  /**
   * Deactivates the specified stimulus.
   *
   * @param id     Stimulus ID.
   * @param roomId Room ID.
   */
  void deactivate(UUID id, UUID roomId);

  /**
   * Enables interaction with the current room stimulus.
   *
   * @param roomId Room ID.
   */
  void enableStimulusInteraction(UUID roomId);

  /**
   * Disables interaction with the current room stimulus.
   *
   * @param roomId Room ID.
   */
  void disableStimulusInteraction(UUID roomId);

  /**
   * Checks if stimulus is active in room.
   *
   * @param id Stimulus ID.
   * @return True if stimulus is active in room. False otherwise.
   */
  boolean isStimulusActive(UUID id);

  /**
   * Enables stimuli focus for provided room.
   *
   * @param roomId Room ID.
   */
  void enableFocus(UUID roomId);

  /**
   * Disables stimuli focus for provided room.
   *
   * @param roomId Room ID.
   */
  void disableFocus(UUID roomId);

  /**
   * Resets current stimulus for provided room.
   *
   * @param roomId Room ID.
   */
  void resetStimulus(UUID roomId);

  /**
   * Enable broadcast results for active polling stimulus in room.
   *
   * @param roomId     Room ID.
   * @param stimulusId Stimulus ID.
   */
  void enableBroadcastResults(UUID roomId, UUID stimulusId);

  /**
   * Disable broadcast results for active polling stimulus in room.
   *
   * @param roomId     Room ID.
   * @param stimulusId Stimulus ID.
   */
  void disableBroadcastResults(UUID roomId, UUID stimulusId);

  boolean isStimulusActiveInRoom(UUID roomId, UUID stimulusId);

  /**
   * Enable stimulus sync with the current stimulus in room.
   *
   * @param roomId     Room ID.
   * @param stimulusId Stimulus ID.
   */
  void enableStimulusSync(UUID roomId, UUID stimulusId);

  /**
   * Disable stimulus sync with the current stimulus in room.
   *
   * @param roomId     Room ID.
   * @param stimulusId Stimulus ID.
   */
  void disableStimulusSync(UUID roomId, UUID stimulusId);

  /**
   * Finds document sharing stimulus by its ID and belonging research session ID.
   *
   * @param stimulusId        Stimulus ID.
   * @param researchSessionId Research session ID.
   * @return Matching stimulus or empty {@link Optional}.
   */
  Optional<Stimulus> findDocumentSharingStimulus(UUID stimulusId, UUID researchSessionId);
}
