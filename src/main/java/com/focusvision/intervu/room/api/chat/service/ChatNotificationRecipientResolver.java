package com.focusvision.intervu.room.api.chat.service;

import com.focusvision.intervu.room.api.chat.model.ChatSource;
import com.focusvision.intervu.room.api.chat.model.ChatType;
import com.focusvision.intervu.room.api.chat.model.ParticipantChatMetadataDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.List;
import java.util.UUID;

/**
 * Service for chat notiicaiton recipients resolving operations.
 *
 * @author Branko Ostojic
 */
public interface ChatNotificationRecipientResolver {

  /**
   * Resolves and returns a list of chat notification recipients.
   *
   * @param sender    Chat message sender.
   * @param recipient Recipient handle.
   * @param source    Chat source.
   * @param type      Chat type.
   * @return List of chat message notificaion recipients.
   */
  List<ParticipantChatMetadataDto> resolveRecipients(AuthenticatedParticipant sender,
                                                     UUID recipient,
                                                     ChatSource source,
                                                     ChatType type);
}
