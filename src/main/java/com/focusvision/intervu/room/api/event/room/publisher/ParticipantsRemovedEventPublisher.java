package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantRemovedEvent;

/**
 * Publisher for {@link com.focusvision.intervu.room.api.model.event.ParticipantRemovedEvent}.
 */
public interface ParticipantsRemovedEventPublisher {

  /**
   * Publishes the participants removed event.
   *
   * @param event Participants removed event.
   */
  void publish(ParticipantRemovedEvent event);
}
