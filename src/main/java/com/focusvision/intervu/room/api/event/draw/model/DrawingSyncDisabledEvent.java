package com.focusvision.intervu.room.api.event.draw.model;

import java.util.UUID;

/**
 * Model drawing sync disabled event.
 */
public record DrawingSyncDisabledEvent(UUID roomId) {

}
