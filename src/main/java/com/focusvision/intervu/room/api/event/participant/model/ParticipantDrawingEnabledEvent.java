package com.focusvision.intervu.room.api.event.participant.model;

import java.util.List;
import java.util.UUID;

/**
 * Model for participant drawing enabled event.
 */
public record ParticipantDrawingEnabledEvent(UUID roomId,
                                             List<UUID> participants) {

}
