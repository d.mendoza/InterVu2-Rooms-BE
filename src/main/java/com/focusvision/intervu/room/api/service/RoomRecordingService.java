package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.dto.RoomRecordingDto;
import java.util.Optional;

/**
 * Service used for room recording related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface RoomRecordingService {

  /**
   * Gets the recording data for room with specified platform ID.
   *
   * @param roomPlatformId Research session platformId.
   * @return Recording data or empty {@link Optional}.
   */
  Optional<RoomRecordingDto> getRecordingData(String roomPlatformId);

  /**
   * Deletes the recording data for room with specified platform ID.
   *
   * @param roomPlatformId Research session platformId.
   */
  void deleteRecordingData(String roomPlatformId);
}
