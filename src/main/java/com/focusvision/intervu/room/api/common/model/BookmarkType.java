package com.focusvision.intervu.room.api.common.model;

/**
 * Enum that represents bookmark type.
 */
public enum BookmarkType {
  /**
   * Standard bookmark.
   */
  NOTE,
  /**
   * Bookmark for personally identifiable information breach.
   */
  PII
}
