package com.focusvision.intervu.room.api.chat.service;

import java.util.Optional;
import java.util.UUID;

/**
 * Service used for chat tracking related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatMessageTrackingService {

  /**
   * Marks meeting room private chat message as seen.
   *
   * @param messageId     Chat message ID.
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   */
  void markAsSeenMeetingRoomPrivateMessage(UUID messageId,
                                           UUID participantId,
                                           UUID handle);

  /**
   * Marks meeting room internal chat message as seen.
   *
   * @param messageId     Chat message ID.
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   */
  void markAsSeenMeetingRoomInternalMessage(UUID messageId,
                                            UUID participantId,
                                            UUID handle);

  /**
   * Marks meeting room respondents chat message as seen.
   *
   * @param messageId     Chat message ID.
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   */
  void markAsSeenMeetingRoomRespondentsMessage(UUID messageId,
                                               UUID participantId,
                                               UUID handle);

  /**
   * Marks waiting room private chat message as seen.
   *
   * @param messageId     Chat message ID.
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   */
  void markAsSeenWaitingRoomPrivateMessage(UUID messageId,
                                           UUID participantId,
                                           UUID handle);

  /**
   * Marks waiting room internal chat message as seen.
   *
   * @param messageId     Chat message ID.
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   */
  void markAsSeenWaitingRoomInternalMessage(UUID messageId,
                                            UUID participantId,
                                            UUID handle);

  /**
   * Marks waiting room respondents chat message as seen.
   *
   * @param messageId     Chat message ID.
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   */
  void markAsSeenWaitingRoomRespondentsMessage(UUID messageId,
                                               UUID participantId,
                                               UUID handle);

  /**
   * Gets latest seen meeting room internal chat message ID.
   *
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   * @return Latest seen message ID.
   */
  Optional<UUID> getLastSeenMeetingRoomInternalMessage(UUID participantId,
                                                       UUID handle);

  /**
   * Gets latest seen meeting room respondents chat message ID.
   *
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   * @return Latest seen message ID.
   */
  Optional<UUID> getLastSeenMeetingRoomRespondentsMessage(UUID participantId,
                                                          UUID handle);

  /**
   * Gets latest seen meeting room private chat message ID.
   *
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   * @return Latest seen message ID.
   */
  Optional<UUID> getLastSeenMeetingRoomPrivateMessage(UUID participantId,
                                                      UUID handle);

  /**
   * Gets latest seen waiting room internal chat message ID.
   *
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   * @return Latest seen message ID.
   */
  Optional<UUID> getLastSeenWaitingRoomInternalMessage(UUID participantId,
                                                       UUID handle);

  /**
   * Gets latest seen waiting room respondents chat message ID.
   *
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   * @return Latest seen message ID.
   */
  Optional<UUID> getLastSeenWaitingRoomRespondentsMessage(UUID participantId,
                                                          UUID handle);

  /**
   * Gets latest seen waiting room private chat message ID.
   *
   * @param participantId Participant ID.
   * @param handle        Chat handle.
   * @return Latest seen message ID.
   */
  Optional<UUID> getLastSeenWaitingRoomPrivateMessage(UUID participantId,
                                                      UUID handle);
}
