package com.focusvision.intervu.room.api.configuration.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * Retry configuration properties.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 **/
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "app.retry")
public class RetryProperties {

  /**
   * Retry configuration for streaming service operations.
   */
  @NestedConfigurationProperty
  private final Config streamingServiceOperations;

  /**
   * Retry configuration.
   */
  @Getter
  @RequiredArgsConstructor
  @ConstructorBinding
  public static class Config {

    /**
     * Number of max attempts to perform the operation.
     */
    private final Integer maxAttempts;

    /**
     * Back off interval in milliseconds.
     */
    private final Long backOffIntervalMillis;
  }
}
