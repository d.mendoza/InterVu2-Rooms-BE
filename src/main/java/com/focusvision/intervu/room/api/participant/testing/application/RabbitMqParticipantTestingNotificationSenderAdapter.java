package com.focusvision.intervu.room.api.participant.testing.application;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ specific implementation of {@link RespondentTestingNotificationSenderAdapter}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqParticipantTestingNotificationSenderAdapter
    implements RespondentTestingNotificationSenderAdapter {

  private final RabbitTemplate rabbitTemplate;
  private final MessagingProperties messagingProperties;

  @Override
  public void send(ParticipantTestingResult results) {
    log.debug("Sending notification of participant {} with testing results",
        results.getRespondentId());

    try {
      rabbitTemplate.convertAndSend(messagingProperties.getRespondentTestingQueue(), results);
    } catch (MessagingException e) {
      log.error(format("Error sending test results for participant %s.", results.getRespondentId()),
          e);
    }
  }
}
