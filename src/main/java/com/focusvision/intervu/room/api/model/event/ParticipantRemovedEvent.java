package com.focusvision.intervu.room.api.model.event;

import static com.focusvision.intervu.room.api.model.event.EventType.PARTICIPANT_REMOVED;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the participants removed event.
 */
@Getter
@Setter
@Builder
@ToString
@ApiModel("ParticipantsRemovedEvent")
public class ParticipantRemovedEvent {

  @ApiModelProperty("Room ID")
  private final UUID roomId;
  @ApiModelProperty("Participant ID")
  private final UUID participantId;
  @ApiModelProperty("Event Type")
  private final EventType eventType = PARTICIPANT_REMOVED;
  @ApiModelProperty("Event destination")
  private final String destination;
}
