package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.event.room.model.RoomExpiredEvent;

/**
 * Room expired event publisher.
 */
public interface RoomExpiredEventPublisher {

  void publish(RoomExpiredEvent event);
}
