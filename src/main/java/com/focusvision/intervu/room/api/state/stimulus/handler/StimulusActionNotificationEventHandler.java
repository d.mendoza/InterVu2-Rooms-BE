package com.focusvision.intervu.room.api.state.stimulus.handler;

import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusActionNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * {@link ApplicationEvent} specific implementation of a stimulus interaction events handler.
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class StimulusActionNotificationEventHandler implements
    PollStimulusVoteEventHandler,
    PollStimulusRankingEventHandler,
    PollStimulusAnswerEventHandler,
    DocumentStimulusPageChangeEventHandler {

  private final StimulusActionNotificationService stimulusActionNotificationService;

  @Override
  @EventListener
  public void handle(PollStimulusVoteEvent event) {
    log.info("Handling poll stimulus vote event: {}.", event);

    stimulusActionNotificationService.sendPollStimulusVoteNotification(event);
  }

  @Override
  @EventListener
  public void handle(PollStimulusRankingEvent event) {
    log.info("Handling poll stimulus ranking event: {}.", event);

    stimulusActionNotificationService.sendPollStimulusRankingNotification(event);
  }

  @Override
  @EventListener
  public void handle(PollStimulusAnswerEvent event) {
    log.info("Handling poll stimulus text answer event: {}.", event);

    stimulusActionNotificationService.sendPollStimulusTextAnswerNotification(event);
  }

  @Override
  @EventListener
  public void handle(DocumentStimulusPageChangeEvent event) {
    log.info("Handling document stimulus page change event: {}.", event);

    stimulusActionNotificationService.sendDocumentStimulusPageChangeNotification(event);
  }
}
