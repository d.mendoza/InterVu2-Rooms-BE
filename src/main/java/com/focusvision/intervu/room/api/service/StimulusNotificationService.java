package com.focusvision.intervu.room.api.service;


import java.util.UUID;

/**
 * Service for sending stimulus notifications.
 */
public interface StimulusNotificationService {

  /**
   * Sends stimulus broadcast results notification.
   *
   * @param roomId     Room Id.
   * @param stimulusId Stimulus Id.
   */
  void sendStimulusBroadcastResultsNotification(UUID roomId, UUID stimulusId);
}
