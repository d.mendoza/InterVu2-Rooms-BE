package com.focusvision.intervu.room.api.messaging;

import com.focusvision.intervu.room.api.adapter.ConferenceCheckDataInputAdapter;
import com.focusvision.intervu.room.api.adapter.ProjectSanitiserRequestInputAdapter;
import com.focusvision.intervu.room.api.adapter.RecordingCheckDataInputAdapter;
import com.focusvision.intervu.room.api.adapter.ResearchSessionDataInputAdapter;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.model.messaging.ProjectSanitiseRequest;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult;
import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * Consumer for messages coming from RabbitMQ message broker.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class RabbitMqConsumer {

  private final ResearchSessionDataInputAdapter researchSessionDataInputAdapter;
  private final ConferenceCheckDataInputAdapter conferenceCheckDataInputAdapter;
  private final RecordingCheckDataInputAdapter recordingCheckDataInputAdapter;
  private final ProjectSanitiserRequestInputAdapter projectSanitiserRequestInputAdapter;

  /**
   * Handles research session update.
   *
   * @param researchSessionData Research session data.
   * @return Processing result.
   */
  @RabbitListener(
      queues = "#{researchSessionUpdatesQueue.name}",
      containerFactory = "researchSessionContainerFactory")
  @SendTo("#{researchSessionProcessedQueue.name}")
  public ResearchSessionProcessResult receiveResearchSession(
      ResearchSessionData researchSessionData) {
    log.debug("Received research session data message: {}", researchSessionData);

    return researchSessionDataInputAdapter.process(researchSessionData);
  }

  /**
   * Handles processing conference check request.
   *
   * @param processingRecordingCheck Message payload.
   */
  @RabbitListener(
      queues = "#{processingConferencesQueue.name}",
      containerFactory = "researchSessionContainerFactory")
  public void receiveProcessingConferenceCheckRequest(
      ProcessingRecordingCheck processingRecordingCheck) {
    log.debug("Received processing conference check message: {}", processingRecordingCheck);

    recordingCheckDataInputAdapter.process(processingRecordingCheck);
  }

  /**
   * Handles running conferences check request.
   *
   * @param runningConferenceCheck Message payload.
   */
  @RabbitListener(
      queues = "#{runningConferencesQueue.name}",
      containerFactory = "researchSessionContainerFactory")
  public void receiveRunningConferenceCheckRequest(RunningConferenceCheck runningConferenceCheck) {
    log.debug("Received running conference check message: {}", runningConferenceCheck);

    conferenceCheckDataInputAdapter.process(runningConferenceCheck);
  }

  /**
   * Handles project sanitise request.
   *
   * @param projectSanitiseRequest Message payload.
   */
  @RabbitListener(
      queues = "#{projectSanitiseQueue.name}",
      containerFactory = "researchSessionContainerFactory")
  public void receiveProjectSanitiseRequest(ProjectSanitiseRequest projectSanitiseRequest) {
    log.debug("Received project sanitise request: {}", projectSanitiseRequest);

    projectSanitiserRequestInputAdapter.process(projectSanitiseRequest);
  }

}
