package com.focusvision.intervu.room.api.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus draw clear event.
 *
 * @author Branko Ostojic
 */
public record StimulusDrawClearEvent(UUID roomId,
                                     UUID stimulusId,
                                     UUID participantId,
                                     Long offset) {

}
