package com.focusvision.intervu.room.api.cache.authentication.publisher;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.cache.authentication.event.ClearAuthenticationCacheEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ specific implementation of a {@link AuthenticationCacheEvictionEventPublisher}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqAuthenticationCacheEvictionEventPublisher
    implements AuthenticationCacheEvictionEventPublisher {

  private final RabbitTemplate template;
  private final FanoutExchange clearAuthenticationCacheExchange;

  @Override
  public void publish(ClearAuthenticationCacheEvent event) {
    log.debug("Publishing clear authentication cache event: {}", event);

    try {
      template.convertAndSend(clearAuthenticationCacheExchange.getName(), "", event);
    } catch (MessagingException e) {
      log.error(format("Error sending clear authentication cache event %s", event), e);
    }
  }
}
