package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;

/**
 * Adapter for handling the conference check data.
 *
 * @author Branko Ostojic
 */
public interface ConferenceCheckDataInputAdapter {

  /**
   * Check the state of the conference room for running sessions. If the conference room is
   * completed - session should be marked as finished.
   *
   * @param runningConferenceCheck Running conference check data.
   */
  void process(RunningConferenceCheck runningConferenceCheck);
}
