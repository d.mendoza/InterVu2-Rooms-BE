package com.focusvision.intervu.room.api.controller;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;

import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import com.focusvision.intervu.room.api.state.stimulus.service.PollingStimulusActionService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller responsible for room stimuli action related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class PollingStimulusActionController {

  private final PollingStimulusActionService stimulusActionService;

  /**
   * Handle poll stimulus voting action.
   *
   * @param message   Action message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("stimulus.poll.vote")
  public void vote(@Payload @Validated Message<PollStimulusActionMessage> message,
                   Principal principal) {
    log.debug("Receiving poll stimulus action event [{}], from [{}].", message,
        principal.getName());

    var caller = extractParticipant(principal).orElseThrow();

    stimulusActionService.vote(caller.getRoomId(), caller.getId(), message.getPayload());
  }

  /**
   * Handle poll stimulus ranking action.
   *
   * @param message   Action message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("stimulus.poll.ranking")
  public void ranking(@Payload @Validated Message<PollStimulusActionMessage> message,
                      Principal principal) {
    log.debug("Receiving poll stimulus action event [{}], from [{}].", message,
        principal.getName());

    var caller = extractParticipant(principal).orElseThrow();

    stimulusActionService.rank(caller.getRoomId(), caller.getId(), message.getPayload());
  }

  /**
   * Handle poll stimulus answer action.
   *
   * @param message   Action message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("stimulus.poll.answer")
  public void answer(@Payload @Validated PollStimulusActionMessage message,
                     Principal principal) {
    log.debug("Receiving poll stimulus action event [{}], from [{}].", message,
        principal.getName());

    var caller = extractParticipant(principal).orElseThrow();

    stimulusActionService.answer(caller.getRoomId(), caller.getId(), message);
  }

}
