package com.focusvision.intervu.room.api.service.impl;

import com.focusvision.intervu.room.api.mapper.ParticipantMapper;
import com.focusvision.intervu.room.api.model.dto.DialInInfoDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantDialInInfoDto;
import com.focusvision.intervu.room.api.service.ParticipantDialInService;
import com.focusvision.intervu.room.api.service.ParticipantService;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ParticipantDialInService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantDialInServiceImpl implements ParticipantDialInService {

  private final ParticipantService participantService;
  private final ParticipantMapper participantMapper;

  @Override
  public DialInInfoDto getDialInInfo(UUID id) {
    log.debug("Getting dial-in information for participant {}.", id);

    var participant = participantService.get(id);

    return participantMapper.mapToDialInInfoDto(participant);
  }

  @Override
  public List<ParticipantDialInInfoDto> getDialInfoForParticipants(UUID roomId) {
    log.debug("Getting dial-in information for participants in room {}", roomId);

    return participantService.findRoomParticipants(roomId).stream()
        .map(participantMapper::mapToParticipantDialInInfoDto)
        .collect(Collectors.toList());
  }
}
