package com.focusvision.intervu.room.api.event.stimulus.model;

/**
 * Enumeration representing all available data types present in events sent on stimulus global
 * channel.
 */
public enum StimulusGlobalDataType {
  BROADCAST,
  ACTION
}
