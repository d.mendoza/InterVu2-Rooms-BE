package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.CompositionParameters;
import com.focusvision.intervu.room.api.model.entity.Conference;
import java.util.List;

/**
 * Service for {@link  CompositionLayout} and {@link AudioChannel} related operations.
 */
public interface CompositionParametersService {

  /**
   * Resolves which composition layouts and audio channels will be used
   * for creating recording based on stimuli, screenshare usage,
   * translator presence and research session audio channel property.
   *
   * @param conference Conference.
   * @return List of correspondent composition layouts.
   */
  List<CompositionParameters> resolveCompositionParams(Conference conference);

  /**
   * Resolves which audio channels will be used for creating twilio conference based
   * on translator presence and research session audio channel property.
   *
   * @param conference Conference.
   * @return List of used audio channels.
   */
  List<AudioChannel> resolveAudioChannels(Conference conference);
}
