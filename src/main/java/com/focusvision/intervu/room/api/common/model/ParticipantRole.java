package com.focusvision.intervu.room.api.common.model;

/**
 * Enumeration representing participant's role in the session.
 *
 * @author Branko Ostojic
 */
public enum ParticipantRole {
  /**
   * Moderator drives the whole session flow.
   */
  MODERATOR,
  /**
   * Observer observers the sessions.
   */
  OBSERVER,
  /**
   * Translator is responsible for translating within session.
   */
  TRANSLATOR,
  /**
   * Respondent is a participant in session.
   */
  RESPONDENT
}
