package com.focusvision.intervu.room.api.scroll.service;

import com.focusvision.intervu.room.api.scroll.model.ScrollLog;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service for handling scroll related actions.
 */
public interface ScrollService {

  /**
   * Persists the scroll action on stimulus.
   *
   * @param roomId        Room ID.
   * @param participantId Participant ID.
   * @param stimulusId    Stimulus ID.
   * @param offset        Action offset from the room start.
   * @param content       Scroll action content.
   * @return Persisted scroll action log.
   */
  ScrollLog saveStimulusScrollAction(UUID roomId,
                                     UUID participantId,
                                     UUID stimulusId,
                                     Long offset,
                                     String content);

  /**
   * Gets the list of all scroll logs ordered by the <code>offset</code>.
   *
   * @param roomId Room ID.
   * @return List of all scroll logs.
   */
  List<ScrollLog> getRoomScrollLogs(UUID roomId);

  /**
   * Gets the current scroll log for specific room and context.
   *
   * @param roomId    Room ID.
   * @param contextId Context ID.
   * @return Current scroll log for specific context or empty {@link Optional}.
   */
  Optional<ScrollLog> getCurrentRoomScrollLog(UUID roomId, UUID contextId);
}
