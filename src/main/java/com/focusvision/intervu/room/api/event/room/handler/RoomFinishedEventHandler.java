package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;

/**
 * Room finished event handler.
 */
public interface RoomFinishedEventHandler {

  void handle(RoomFinishedEvent event);
}
