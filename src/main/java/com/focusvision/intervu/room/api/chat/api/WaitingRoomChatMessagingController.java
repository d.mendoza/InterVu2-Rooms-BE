package com.focusvision.intervu.room.api.chat.api;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;
import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.chat.model.IncomingChatMessage;
import com.focusvision.intervu.room.api.chat.model.SeenChatMessage;
import com.focusvision.intervu.room.api.chat.service.ChatHandlerService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

/**
 * Controller responsible for waiting room chat messaging operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class WaitingRoomChatMessagingController {

  private final ChatHandlerService chatHandlerService;

  /**
   * Handles waiting room respondents chat message.
   *
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("waiting-room/chat/respondents")
  public void respondents(@Payload @Validated IncomingChatMessage message,
                          Principal principal) {
    log.debug("Receiving waiting room respondents chat message {}, sent by {}", message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var text = message.getMessage();

    chatHandlerService.handleWaitingRoomRespondentsMessage(sender, text);
  }

  /**
   * Handles waiting room internal chat message.
   *
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("waiting-room/chat/internal")
  public void internal(@Payload @Validated IncomingChatMessage message,
                       Principal principal) {
    log.debug("Receiving waiting room internal chat message, sent by {}", principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var text = message.getMessage();

    chatHandlerService.handleWaitingRoomInternalMessage(sender, text);
  }

  /**
   * Handles waiting room private chat message.
   *
   * @param handle    Chat handle.
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("waiting-room/chat/private/{handle}")
  public void privateChat(@DestinationVariable String handle,
                          @Payload @Validated IncomingChatMessage message,
                          Principal principal) {
    log.debug("Receiving waiting room private chat message, sent by {}", principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var text = message.getMessage();

    chatHandlerService.handleWaitingRoomPrivateMessage(sender, fromString(handle), text);
  }

  /**
   * Marks waiting room internal chat message as seen.
   *
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("waiting-room/chat/internal/seen")
  public void internalSeen(@Payload @Validated SeenChatMessage message,
                           Principal principal) {
    log.debug("Receiving seen waiting room internal chat message {}, sent by {}", message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var chatMessageId = message.getId();
    chatHandlerService.handleSeenWaitingRoomInternalMessage(chatMessageId, sender);
  }

  /**
   * Marks waiting room respondents chat message as seen.
   *
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("waiting-room/chat/respondents/seen")
  public void respondentsSeen(@Payload @Validated SeenChatMessage message,
                              Principal principal) {
    log.debug("Receiving seen waiting room respondents chat message {}, sent by {}", message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var chatMessageId = message.getId();
    chatHandlerService.handleSeenWaitingRoomRespondentsMessage(chatMessageId, sender);
  }

  /**
   * Marks waiting room private chat message as seen.
   *
   * @param handle    Chat handle.
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("waiting-room/chat/private/{handle}/seen")
  public void privateSeen(@DestinationVariable String handle,
                          @Payload @Validated SeenChatMessage message,
                          Principal principal) {
    log.debug("Receiving seen waiting room private chat message {}, sent by {}", message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var chatMessageId = message.getId();
    chatHandlerService.handleSeenWaitingRoomPrivateMessage(chatMessageId, fromString(handle),
        sender);
  }
}
