package com.focusvision.intervu.room.api.event.room.handler;

import com.focusvision.intervu.room.api.recording.event.RecordingProcessingErrorEvent;

/**
 * Room recording error event handler.
 */
public interface RoomRecordingErrorEventHandler {

  void handle(RecordingProcessingErrorEvent event);
}
