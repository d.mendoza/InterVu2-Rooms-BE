package com.focusvision.intervu.room.api.stimulus.snapshot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

/**
 * DTO representing stimulus snapshot details.
 */
@Data
@Accessors(chain = true)
@ApiModel("Stimulus Snapshot")
public class StimulusSnapshotDto {

  @ApiModelProperty("Snapshot ID")
  private UUID id;

  @ApiModelProperty("Snapshot name")
  private String name;

  @NotNull
  @ApiModelProperty("Snapshot timestamp")
  private Long timestamp;
}
