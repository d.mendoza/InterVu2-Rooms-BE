package com.focusvision.intervu.room.api.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for the RabbitMQ message broker.
 *
 * @author Branko Ostojic
 */
@Configuration
@RequiredArgsConstructor
public class RabbitMqConfig {

  private final MessagingProperties messagingConfiguration;
  private final ObjectMapper objectMapper;

  /**
   * Creates RabbitMQ template.
   *
   * @param connectionFactory Connection facctory.
   * @return RabbitMQ template.
   */
  @Bean
  public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
    final var rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
    return rabbitTemplate;
  }

  /**
   * Creates RMQ message listener container.
   *
   * @param rabbitConnectionFactory Connection factory.
   * @return Message listener container.
   */
  @Bean("researchSessionContainerFactory")
  public RabbitListenerContainerFactory<SimpleMessageListenerContainer> factory(
      ConnectionFactory rabbitConnectionFactory) {
    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
    factory.setConnectionFactory(rabbitConnectionFactory);
    factory.setPrefetchCount(1);
    factory.setMessageConverter(producerJackson2MessageConverter());
    return factory;
  }

  @Bean
  public Queue researchSessionUpdatesQueue() {
    return new Queue(messagingConfiguration.getResearchSessionUpdatesQueue(), true, false, false);
  }

  @Bean
  public Queue researchSessionProcessedQueue() {
    return new Queue(messagingConfiguration.getResearchSessionProcessedQueue(), true, false, false);
  }

  @Bean
  public Queue researchSessionStateUpdatesQueue() {
    return new Queue(messagingConfiguration.getResearchSessionStateUpdatesQueue(), true, false,
        false);
  }

  @Bean
  public Queue respondentTestingQueue() {
    return new Queue(messagingConfiguration.getRespondentTestingQueue(), true, false, false);
  }

  @Bean
  public Queue processingConferencesQueue() {
    return new Queue(messagingConfiguration.getConferencesProcessingQueue(), true, false, false);
  }

  @Bean
  public Queue processPendingRecordingQueue() {
    return new Queue(messagingConfiguration.getProcessPendingRecordingQueue(), true, false, false);
  }

  @Bean
  public Queue runningConferencesQueue() {
    return new Queue(messagingConfiguration.getConferencesRunningQueue(), true, false, false);
  }

  @Bean
  public Queue projectSanitiseQueue() {
    return new Queue(messagingConfiguration.getProjectSanitiseQueue(), true, false, false);
  }

  @Bean
  public Queue researchSessionChatMessagesQueue() {
    return new Queue(messagingConfiguration.getResearchSessionChatMessagesQueue(), true, false,
        false);
  }

  @Bean
  public Queue bannedParticipantQueue() {
    return new Queue(messagingConfiguration.getParticipantBannedQueue(), true, false, false);
  }

  @Bean
  public FanoutExchange clearAuthenticationCacheExchange() {
    return new FanoutExchange(messagingConfiguration.getClearAuthenticationCacheExchange());
  }

  @Bean
  public Queue clearAuthenticationCacheQueue() {
    return new AnonymousQueue();
  }

  @Bean
  public Binding clearAuthenticationCacheQueueBinding(
      FanoutExchange clearAuthenticationCacheExchange,
      Queue clearAuthenticationCacheQueue) {
    return BindingBuilder.bind(clearAuthenticationCacheQueue).to(clearAuthenticationCacheExchange);
  }

  @Bean
  public Binding clearRoomLogCacheQueueBinding(FanoutExchange clearRoomLogCacheExchange,
                                               Queue clearRoomLogCacheQueue) {
    return BindingBuilder.bind(clearRoomLogCacheQueue).to(clearRoomLogCacheExchange);
  }

  @Bean
  public FanoutExchange clearRoomLogCacheExchange() {
    return new FanoutExchange(messagingConfiguration.getClearRoomLogCacheExchange());
  }

  @Bean
  public Queue clearRoomLogCacheQueue() {
    return new AnonymousQueue();
  }

  @Bean
  public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
    return new Jackson2JsonMessageConverter(objectMapper);
  }
}
