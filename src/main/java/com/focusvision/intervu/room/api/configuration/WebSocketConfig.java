package com.focusvision.intervu.room.api.configuration;

import static com.focusvision.intervu.room.api.chat.service.BrokerChatCommunicationChannelService.CHAT_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.DRAW_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.PARTICIPANT_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.ROOM_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.STIMULUS_CHANNEL;
import static com.focusvision.intervu.room.api.service.impl.CommunicationChannelServiceImpl.STIMULUS_GLOBAL_CHANNEL;

import com.focusvision.intervu.room.api.configuration.domain.CorsProperties;
import com.focusvision.intervu.room.api.configuration.domain.MessageBrokerProperties;
import com.focusvision.intervu.room.api.security.WebSocketInboundChannelInterceptor;
import com.focusvision.intervu.room.api.security.WebSocketOutboundChannelInterceptor;
import com.focusvision.intervu.room.api.security.WebSocketSubscriptionValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

/**
 * WebSocket support configuration.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Configuration
@RequiredArgsConstructor
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

  private final WebSocketSubscriptionValidator wsSubscriptionValidator;
  private final MessageBrokerProperties configuration;
  private final CorsProperties corsProperties;

  @Bean
  public WebSocketInboundChannelInterceptor wsInboundChannelInterceptor() {
    return new WebSocketInboundChannelInterceptor(wsSubscriptionValidator);
  }

  @Bean
  public WebSocketOutboundChannelInterceptor wsOutboundChannelInterceptor() {
    return new WebSocketOutboundChannelInterceptor();
  }

  @Override
  public void configureWebSocketTransport(WebSocketTransportRegistration registry) {
    registry.setMessageSizeLimit(configuration.getTransport().getMessageSizeLimit());
  }

  @Override
  public void configureClientInboundChannel(ChannelRegistration registration) {
    registration.interceptors(wsInboundChannelInterceptor());
  }

  @Override
  public void configureClientOutboundChannel(ChannelRegistration registration) {
    registration.interceptors(wsOutboundChannelInterceptor());
  }

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry
        .addEndpoint("/ws")
        .setAllowedOriginPatterns(corsProperties.getAllowedOrigins())
        .withSockJS()
        .setSupressCors(true);
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry registry) {
    registry.setApplicationDestinationPrefixes("/app");
    registry.enableStompBrokerRelay(ROOM_CHANNEL, PARTICIPANT_CHANNEL, CHAT_CHANNEL, DRAW_CHANNEL,
            STIMULUS_CHANNEL, STIMULUS_GLOBAL_CHANNEL)
        .setRelayHost(configuration.getHost())
        .setRelayPort(configuration.getRelayPort())
        .setClientLogin(configuration.getUsername())
        .setClientPasscode(configuration.getPassword())
        .setSystemLogin(configuration.getUsername())
        .setSystemPasscode(configuration.getPassword());
    registry.setPathMatcher(new AntPathMatcher("."));
  }
}
