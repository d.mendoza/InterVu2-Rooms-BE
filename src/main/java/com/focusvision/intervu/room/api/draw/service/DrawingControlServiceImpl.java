package com.focusvision.intervu.room.api.draw.service;

import static java.util.List.of;
import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncDisabledEvent;
import com.focusvision.intervu.room.api.event.draw.model.DrawingSyncEnabledEvent;
import com.focusvision.intervu.room.api.event.draw.publisher.DrawingControlEventPublisher;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingDisabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingEnabledEvent;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.service.ParticipantService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link DrawingControlService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DrawingControlServiceImpl implements DrawingControlService {

  private final DrawingControlEventPublisher drawingControlEventPublisher;
  private final ParticipantService participantService;

  @Override
  public void enableSync(UUID roomId) {
    log.debug("Processing drawing sync enabled (roomId: {})", roomId);

    drawingControlEventPublisher.publish(new DrawingSyncEnabledEvent(roomId));
  }

  @Override
  public void disableSync(UUID roomId) {
    log.debug("Processing drawing sync disabled (roomId: {})", roomId);

    drawingControlEventPublisher.publish(new DrawingSyncDisabledEvent(roomId));
  }

  @Override
  public void enableDrawingForParticipant(UUID roomId, UUID participantId) {
    log.debug("Enabling drawing (roomId: {}, participantId: {})", roomId, participantId);

    var participant = participantService.findRoomParticipant(roomId, participantId)
        .orElseThrow(() -> new IntervuRoomException("Participant not in room."));

    drawingControlEventPublisher
        .publish(new ParticipantDrawingEnabledEvent(roomId, of(participant.getId())));
  }

  @Override
  public void disableDrawingForParticipant(UUID roomId, UUID participantId) {
    log.debug("Disabling drawing (roomId: {}, participantId: {})", roomId, participantId);

    var participant = participantService.findRoomParticipant(roomId, participantId)
        .orElseThrow(() -> new IntervuRoomException("Participant not in room."));

    drawingControlEventPublisher
        .publish(new ParticipantDrawingDisabledEvent(roomId, of(participant.getId())));
  }

  @Override
  public void enableDrawingForParticipants(UUID roomId) {
    log.debug("Enabling drawing for all participants (roomId: {})", roomId);

    var participants = participantService.findRoomParticipants(roomId).stream()
        .map(Participant::getId)
        .toList();

    drawingControlEventPublisher
        .publish(new ParticipantDrawingEnabledEvent(roomId, participants));
  }

  @Override
  public void disableDrawingForParticipants(UUID roomId) {
    log.debug("Disable drawing for all participants (roomId: {})", roomId);

    var participants = participantService.findRoomParticipants(roomId).stream()
        .map(Participant::getId)
        .toList();

    drawingControlEventPublisher
        .publish(new ParticipantDrawingDisabledEvent(roomId, participants));
  }
}
