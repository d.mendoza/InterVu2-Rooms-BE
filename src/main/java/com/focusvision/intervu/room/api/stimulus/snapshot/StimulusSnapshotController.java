package com.focusvision.intervu.room.api.stimulus.snapshot;

import static org.springframework.http.HttpStatus.CREATED;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for stimulus snapshot operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/stimuli", produces = MediaType.APPLICATION_JSON_VALUE)
public class StimulusSnapshotController {

  private final StimulusSnapshotService stimulusSnapshotService;

  /**
   * Create stimulus snapshot.
   *
   * @param dto    Request data.
   * @param caller Authenticated user.
   * @return Created stimulus snapshot.
   */
  @PostMapping("{id}/snapshot")
  @ResponseStatus(CREATED)
  @ApiOperation(
      value = "Creates a stimulus snapshot",
      notes = "Creates a stimulus snapshot data")
  @PreAuthorize("hasAuthority('STIMULI_SNAPSHOT_ADD')")
  public StimulusSnapshotDto createSnapshot(@ApiParam("Stimulus ID") @PathVariable String id,
                                            @RequestBody @Valid AddStimulusSnapshotDto dto,
                                            @ApiIgnore @AuthenticationPrincipal
                                                AuthenticatedParticipant caller) {
    log.debug("Creating stimulus snapshot {stimulusId: {}}", id);

    return stimulusSnapshotService.addSnapshot(caller,
        UUID.fromString(id),
        dto.getName(),
        dto.getData(),
        dto.getMimeType(),
        dto.getTimestamp());
  }
}
