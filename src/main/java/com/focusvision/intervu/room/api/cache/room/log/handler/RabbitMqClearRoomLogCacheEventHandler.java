package com.focusvision.intervu.room.api.cache.room.log.handler;

import com.focusvision.intervu.room.api.cache.room.log.event.ClearRoomLogCacheEvent;
import com.focusvision.intervu.room.api.cache.room.log.service.RoomLogCachingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * {@link RabbitListener} specific implementation of a {@link ClearRoomLogCacheEventHandler}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqClearRoomLogCacheEventHandler implements ClearRoomLogCacheEventHandler {

  private final RoomLogCachingService roomLogCachingService;

  @Override
  @RabbitListener(queues = "#{clearRoomLogCacheQueue.name}")
  public void handle(ClearRoomLogCacheEvent event) {
    log.debug("Handling clear room log cache event: {}", event);

    roomLogCachingService.clearRoomLogCache(event.roomId());
  }
}
