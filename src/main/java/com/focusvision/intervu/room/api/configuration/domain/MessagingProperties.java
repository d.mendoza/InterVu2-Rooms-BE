package com.focusvision.intervu.room.api.configuration.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * Messaging properties.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 **/
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "app.messaging")
public class MessagingProperties {
  /**
   * Messaging queue for research sessions data updates.
   */
  private final String researchSessionUpdatesQueue;
  /**
   * Messaging queue for processed research sessions data.
   */
  private final String researchSessionProcessedQueue;
  /**
   * Messaging queue for research sessions state updates.
   */
  private final String researchSessionStateUpdatesQueue;
  /**
   * Messaging queue for respondent testing results.
   */
  private final String respondentTestingQueue;
  /**
   * Messaging queue for processing conference check events.
   */
  private final String conferencesProcessingQueue;
  /**
   * Messaging queue for running conference check events.
   */
  private final String conferencesRunningQueue;
  /**
   * Messaging queue for sanitise project requests.
   */
  private final String projectSanitiseQueue;
  /**
   * Messaging queue for research sessions chat messages.
   */
  private final String researchSessionChatMessagesQueue;
  /**
   * Messaging queue for banned participant events.
   */
  private final String participantBannedQueue;
  /**
   * Messaging exchange for authentication cache eviction events.
   */
  private final String clearAuthenticationCacheExchange;
  /**
   * Messaging exchange for room log cache eviction events.
   */
  private final String clearRoomLogCacheExchange;
  /**
   * Messaging queue for processing pending recording events.
   */
  private final String processPendingRecordingQueue;
}
