package com.focusvision.intervu.room.api.exception.handler;

import static com.focusvision.intervu.room.api.exception.ErrorCode.FORBIDDEN;
import static com.focusvision.intervu.room.api.exception.ErrorCode.INVALID_REQUEST_DATA;
import static com.focusvision.intervu.room.api.exception.ErrorCode.STREAMING_API_ERROR;
import static com.focusvision.intervu.room.api.exception.ErrorCode.UNDEFINED;
import static org.springframework.util.StringUtils.isEmpty;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.exception.StreamingApiException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Custom exception handler for REST endpoints.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@ControllerAdvice
@RestController
@Getter
@Slf4j
@RequiredArgsConstructor
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  private final ConversionService conversionService;

  /**
   * Handles IntervuRoomException.
   *
   * @param ex      Exception.
   * @param request Web request.
   * @return Handled response.
   */
  @ExceptionHandler(IntervuRoomException.class)
  public final ResponseEntity<ErrorResponse> handleIntervuException(IntervuRoomException ex,
                                                                    WebRequest request) {
    var message = isEmpty(ex.getMessage()) ? ex.getCode().name() : ex.getMessage();

    log.error("Handling Intervu Room exception: {}", message);

    ErrorResponse exceptionResponse = new ErrorResponse(ex.getCode(), message);
    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handles ResourceNotFoundException.
   *
   * @param ex      Exception.
   * @param request Web request.
   * @return Handled response.
   */
  @ExceptionHandler(ResourceNotFoundException.class)
  public final ResponseEntity<ErrorResponse> handleResourceNotFoundException(
      ResourceNotFoundException ex,
      WebRequest request) {
    log.error("Handling resource not found exception", ex);

    ErrorResponse exceptionResponse = new ErrorResponse(ex.getCode(), ex.getCode().name());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  /**
   * Handles ActionForbiddenException.
   *
   * @param ex      Exception.
   * @param request Web request.
   * @return Handled response.
   */
  @ExceptionHandler(ActionForbiddenException.class)
  public final ResponseEntity<ErrorResponse> handleForbiddenException(ActionForbiddenException ex,
                                                                      WebRequest request) {
    var message = isEmpty(ex.getMessage()) ? ex.getCode().name() : ex.getMessage();
    log.error("Handling action forbidden exception: {}", message);

    ErrorResponse exceptionResponse = new ErrorResponse(ex.getCode(), message);
    return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
  }

  /**
   * Handles AccessDeniedException.
   *
   * @param ex      Exception.
   * @param request Web request.
   * @return Handled response.
   */
  @ExceptionHandler(AccessDeniedException.class)
  public final ResponseEntity<ErrorResponse> handleAccessDeniedException(AccessDeniedException ex,
                                                                         WebRequest request) {
    log.error("Handling access denied exception", ex);

    ErrorResponse exceptionResponse = new ErrorResponse(FORBIDDEN, ex.getMessage());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
  }

  /**
   * Handles StreamingApiException.
   *
   * @param ex      Exception.
   * @param request Web request.
   * @return Handled response.
   */
  @ExceptionHandler(StreamingApiException.class)
  public final ResponseEntity<ErrorResponse> handleStreaminApiExceptions(StreamingApiException ex,
                                                                         WebRequest request) {
    log.error("Handling streaming api exception", ex);

    ErrorResponse exceptionResponse = new ErrorResponse(STREAMING_API_ERROR, ex.getMessage());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.SERVICE_UNAVAILABLE);
  }

  /**
   * Handles Exception.
   *
   * @param ex      Exception.
   * @param request Web request.
   * @return Handled response.
   */
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception ex, WebRequest request) {
    log.error("Handling exception", ex);

    ErrorResponse exceptionResponse = new ErrorResponse(UNDEFINED, ex.getMessage());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handles HttpMessageNotReadableException.
   *
   * @param ex      Exception.
   * @param headers Request headers.
   * @param status  Request status.
   * @param request Web request.
   * @return Handled response.
   */
  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
    log.debug("Handling json convert to object validation exception: {}", ex.getMessage());

    ErrorResponse exceptionResponse =
        new ErrorResponse(INVALID_REQUEST_DATA, "Invalid request data");

    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }
}
