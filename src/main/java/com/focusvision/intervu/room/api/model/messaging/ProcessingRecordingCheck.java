package com.focusvision.intervu.room.api.model.messaging;

import static com.focusvision.intervu.room.api.advisor.Lockable.Group.RECORDING;

import com.focusvision.intervu.room.api.advisor.Lockable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the processing recording check data.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ProcessingRecordingCheck implements Lockable {

  private String id;

  @Override
  public String lockKey() {
    return id;
  }

  @Override
  public Group lockGroup() {
    return RECORDING;
  }
}
