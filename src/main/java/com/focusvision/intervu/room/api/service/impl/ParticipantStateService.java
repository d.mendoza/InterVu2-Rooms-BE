package com.focusvision.intervu.room.api.service.impl;

import static java.util.Optional.of;

import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.dto.ParticipantStateDto;
import com.focusvision.intervu.room.api.participant.ParticipantPermissionResolver;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.state.RoomStateOperator;
import com.focusvision.intervu.room.api.state.mapper.RoomStateMapper;
import com.focusvision.intervu.room.api.state.model.ParticipantState;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Service used for participant state related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantStateService {

  private final RoomStateOperator roomStateOperator;
  private final CommunicationChannelService communicationChannelService;
  private final ParticipantService participantService;
  private final RoomStateMapper roomStateMapper;

  /**
   * Collects and returns the current participant state. If the state for participant is not present
   * - it will be created.
   *
   * @param participantId Participant ID.
   * @param roomId        Room ID.
   * @return Participant state.
   */
  public ParticipantStateDto getOrCreateParticipantState(UUID participantId, UUID roomId) {
    return roomStateOperator.getCurrentParticipantState(roomId, participantId)
        .or(() -> of(roomStateOperator.constructParticipantState(roomId,
            participantService.get(participantId))))
        .map(this::getParticipantState)
        .orElseThrow(() -> new IntervuRoomException("Participant state not found."));
  }

  /**
   * Collects and returns the current participant state data.
   *
   * @param participantState Participant state.
   * @return Participant state data.
   */
  public ParticipantStateDto getParticipantState(ParticipantState participantState) {
    var participantId = participantState.getId();
    var participant = participantService.get(participantId);
    var roomId = participant.getResearchSession().getId();
    var channelData = communicationChannelService.getParticipantChannelData(participantId, roomId);
    var permissions = ParticipantPermissionResolver.resolve(participant);
    return roomStateMapper.mapToDto(participantState)
        .setChannel(channelData)
        .setPermissions(permissions);
  }

}
