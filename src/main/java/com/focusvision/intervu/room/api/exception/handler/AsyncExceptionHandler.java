package com.focusvision.intervu.room.api.exception.handler;

import static java.lang.String.format;

import java.lang.reflect.Method;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

/**
 * Custom exception handler for Async operations exceptions.
 *
 * @author Branko Ostojic
 */
@Slf4j
public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

  @Override
  public void handleUncaughtException(Throwable ex, Method method, Object... params) {
    log.error(format("Error executing async method %s.", method.getName()), ex);
  }
}
