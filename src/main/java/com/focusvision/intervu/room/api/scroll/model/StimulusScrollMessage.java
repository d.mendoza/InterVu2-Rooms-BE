package com.focusvision.intervu.room.api.scroll.model;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Model representing stimulus scroll action message.
 */
@Data
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class StimulusScrollMessage {

  /**
   * Scroll action content.
   */
  private String content;

  /**
   * Scroll action UTC timestamp.
   */
  @NotNull
  private Long timestamp;
}
