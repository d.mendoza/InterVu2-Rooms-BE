package com.focusvision.intervu.room.api.recording.event;

/**
 * Room recording processing finished event publisher.
 *
 * @author Branko Ostojic
 */
public interface RecordingProcessingFinishedEventPublisher {

  /**
   * Publishes provided event data.
   *
   * @param event Event data.
   */
  void publish(RecordingProcessingFinishedEvent event);
}
