package com.focusvision.intervu.room.api.cache.authentication.handler;

import com.focusvision.intervu.room.api.cache.authentication.event.ClearAuthenticationCacheEvent;

/**
 * Handler contract for authentication cache eviction event.
 *
 * @author Branko Ostojic
 */
public interface ClearAuthenticationCacheEventHandler {

  /**
   * Handles the provided authentication cache eviction event.
   *
   * @param event Clear authentication cache event.
   */
  void handle(ClearAuthenticationCacheEvent event);
}
