package com.focusvision.intervu.room.api.model.messaging;

import static com.focusvision.intervu.room.api.advisor.Lockable.Group.RECORDING;

import com.focusvision.intervu.room.api.advisor.Lockable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the process pending recording request.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ProcessPendingRecordingRequest implements Lockable {

  private String recordingId;

  @Override
  public String lockKey() {
    return recordingId;
  }

  @Override
  public Group lockGroup() {
    return RECORDING;
  }
}
