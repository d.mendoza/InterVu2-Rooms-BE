package com.focusvision.intervu.room.api.state.model;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.dto.ChannelDto;
import com.focusvision.intervu.room.api.model.dto.ConferenceDto;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the participants state in room.
 */
@Data
@Builder
@Accessors(chain = true)
public class ParticipantState {

  private UUID id;
  private String platformId;
  private String displayName;
  private boolean online;
  private boolean presentOverWeb;
  private boolean presentOverPhone;
  private ParticipantRole role;
  private boolean banned;
  private boolean admin;
  private boolean moderator;
  private boolean banable;
  private boolean guest;
  private boolean projectManager;
  private boolean projectOperator;
  private boolean ready;
  private boolean drawingEnabled;
  private Long firstEntryOffset;
  private ConferenceDto conference;
  /**
   * This property is no longer required in the room state object.
   *
   * @deprecated This should be removed since it is static and not changing.
   */
  @Deprecated
  private ChannelDto channel;

  /**
   * Marks participant state as present over WEB. It also marks participant as online.
   *
   * @return Participant state.
   */
  @JsonIgnore
  public ParticipantState markAsPresentOverWeb() {
    this.presentOverWeb = true;
    this.online = true;
    return this;
  }

  /**
   * Marks participant state as present over phone. It also marks participant as online.
   *
   * @return Participant state.
   */
  @JsonIgnore
  public ParticipantState markAsPresentOverPhone() {
    this.presentOverPhone = true;
    this.online = true;
    return this;
  }

  /**
   * Marks participant state as not present over WEB. It also marks participant as offline if
   * participant is not present over phone.
   *
   * @return Participant state.
   */
  @JsonIgnore
  public ParticipantState markAsNotPresentOverWeb() {
    this.presentOverWeb = false;
    this.online = presentOverPhone;
    return this;
  }

  /**
   * Marks participant state as not present over phone. It also marks participant as offline if
   * participant is not present over WEB.
   *
   * @return Participant state.
   */
  @JsonIgnore
  public ParticipantState markAsNotPresentOverPhone() {
    this.presentOverPhone = false;
    this.online = presentOverWeb;
    return this;
  }

  @JsonIgnore
  public ParticipantState markAsBanned() {
    this.banned = true;
    return this;
  }

  @JsonIgnore
  public ParticipantState markAsReady() {
    this.ready = true;
    return this;
  }

  @JsonIgnore
  public boolean isInternal() {
    return !RESPONDENT.equals(role);
  }

  @JsonIgnore
  public boolean isModeratorRole() {
    return MODERATOR.equals(role);
  }
}
