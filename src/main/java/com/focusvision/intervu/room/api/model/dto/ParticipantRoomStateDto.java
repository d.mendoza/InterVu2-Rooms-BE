package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the participant room state.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@ApiModel("ParticipantRoomState")
public class ParticipantRoomStateDto {

  @ApiModelProperty("Room state data")
  private RoomStateDto roomState;
  @ApiModelProperty("Participant state data")
  private ParticipantStateDto participantState;
  @ApiModelProperty("Dial-in info")
  private DialInInfoDto dialInInfo;
}
