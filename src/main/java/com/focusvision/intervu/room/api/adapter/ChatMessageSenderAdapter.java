package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import java.util.UUID;

/**
 * Adapter used for sending chat messages.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ChatMessageSenderAdapter {

  /**
   * Sends the provided chat message to the specified recipient (destination).
   *
   * @param message   Chat message to be sent.
   * @param recipient Recipient of the message.
   */
  void send(OutgoingChatMessage message, UUID recipient);

}
