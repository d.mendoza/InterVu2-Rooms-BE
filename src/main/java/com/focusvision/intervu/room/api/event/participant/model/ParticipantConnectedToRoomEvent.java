package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * DTO representing the participant connected to the room event.
 *
 * @author Branko Ostojic
 */
public record ParticipantConnectedToRoomEvent(UUID roomId,
                                              UUID participantId) {

}
