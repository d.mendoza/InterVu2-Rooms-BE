package com.focusvision.intervu.room.api.draw.service;

import java.util.UUID;

/**
 * Service for handling drawing control actions.
 *
 * @author Branko Ostojic
 */
public interface DrawingControlService {

  /**
   * Enables the drawing sync.
   *
   * @param roomId Room ID.
   */
  void enableSync(UUID roomId);

  /**
   * Disables the drawing sync.
   *
   * @param roomId Room ID.
   */
  void disableSync(UUID roomId);

  /**
   * Enable drawing for participant in research session.
   *
   * @param roomId        Room ID.
   * @param participantId Participant UUID.
   */
  void enableDrawingForParticipant(UUID roomId, UUID participantId);

  /**
   * Disable drawing for participant in research session.
   *
   * @param roomId        Room ID.
   * @param participantId Participant UUID.
   */
  void disableDrawingForParticipant(UUID roomId, UUID participantId);

  /**
   * Enable drawing for all participants in research session.
   *
   * @param roomId Room ID.
   */
  void enableDrawingForParticipants(UUID roomId);

  /**
   * Disable drawing for all participants in research session.
   *
   * @param roomId Room ID.
   */
  void disableDrawingForParticipants(UUID roomId);
}
