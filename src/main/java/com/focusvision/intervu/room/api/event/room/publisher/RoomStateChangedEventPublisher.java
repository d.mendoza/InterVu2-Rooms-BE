package com.focusvision.intervu.room.api.event.room.publisher;

import com.focusvision.intervu.room.api.state.RoomStateChangedEvent;

/**
 * Room state changed event publisher.
 */
public interface RoomStateChangedEventPublisher {

  void publish(RoomStateChangedEvent event);
}
