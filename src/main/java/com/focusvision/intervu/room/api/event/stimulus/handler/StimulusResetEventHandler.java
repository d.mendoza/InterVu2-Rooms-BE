package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusResetEvent;

/**
 * Handler for the {@link StimulusResetEvent}.
 */
public interface StimulusResetEventHandler {

  /**
   * Handles the provided {@link StimulusResetEvent}.
   *
   * @param event Stimulus reset event.
   */
  void handle(StimulusResetEvent event);
}
