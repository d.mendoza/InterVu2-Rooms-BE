package com.focusvision.intervu.room.api.scroll.mapper;

import com.focusvision.intervu.room.api.scroll.model.ScrollLog;
import com.focusvision.intervu.room.api.scroll.model.ScrollLogDto;
import org.mapstruct.Mapper;

/**
 * Mapper for {@link ScrollLog} entity.
 */
@Mapper(componentModel = "spring")
public interface ScrollLogMapper {

  /**
   * Maps scroll log to DTO.
   *
   * @param scrollLog Scroll log to be mapped.
   * @return Resulting DTO.
   */
  ScrollLogDto mapToDto(ScrollLog scrollLog);

}
