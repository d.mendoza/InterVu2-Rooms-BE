package com.focusvision.intervu.room.api.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Intervu user token authentication implementation of {@link Authentication}.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public class IntervuUserTokenBasedAuthentication extends AbstractAuthenticationToken {
  /**
   * Serial version UID.
   */
  private static final long serialVersionUID = 7910953044599712941L;

  private final String token;
  private final IntervuUser principle;

  /**
   * Constructor.
   *
   * @param principle Principle.
   * @param token     Token.
   */
  public IntervuUserTokenBasedAuthentication(final IntervuUser principle, final String token) {
    super(principle.getAuthorities());
    this.principle = principle;
    this.token = token;
  }

  @Override
  public boolean isAuthenticated() {
    return true;
  }

  @Override
  public Object getCredentials() {
    return token;
  }

  @Override
  public UserDetails getPrincipal() {
    return principle;
  }

}
