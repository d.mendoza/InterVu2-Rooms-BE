package com.focusvision.intervu.room.api.stimulus.event.publisher;

import com.focusvision.intervu.room.api.stimulus.model.StimulusScrollEvent;

/**
 * Publisher contract for stimulus scroll events.
 */
public interface StimulusScrollEventPublisher {

  /**
   * Publish the provided stimulus scroll event.
   *
   * @param event Scroll event.
   */
  void publish(StimulusScrollEvent event);
}
