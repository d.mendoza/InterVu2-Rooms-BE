package com.focusvision.intervu.room.api.recording.task;

import static java.time.Duration.between;
import static java.time.Instant.now;

import com.focusvision.intervu.room.api.model.entity.Recording;
import com.focusvision.intervu.room.api.model.messaging.ProcessPendingRecordingRequest;
import com.focusvision.intervu.room.api.recording.infra.ProcessPendingRecordingRequestPublisher;
import com.focusvision.intervu.room.api.recording.service.RecordingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduled task for publishing pending recordings for processing.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(
    prefix = "app.tasks.check-pending-recordings",
    value = "enabled",
    havingValue = "true")
public class PendingRecordingTask {

  private final ProcessPendingRecordingRequestPublisher publisher;
  private final RecordingService recordingService;
  @Value("${app.tasks.check-pending-recordings.period}")
  private Long taskPeriod;

  /**
   * Triggers pending recordings check.
   */
  @Scheduled(
      initialDelayString = "${app.tasks.check-pending-recordings.initial-delay:10000}",
      fixedRateString = "${app.tasks.check-pending-recordings.period}")
  @SchedulerLock(
      name = "PendingRecordingTask",
      lockAtLeastFor = "${app.tasks.check-pending-recordings.lock-period-min}",
      lockAtMostFor = "${app.tasks.check-pending-recordings.lock-period-max}")
  public void run() {
    log.info("Starting pending recordings task.");

    var recordings = recordingService.getAllPendingRecordings();
    log.info("Found {} pending recordings to process.", recordings.size());

    recordings.stream()
        .filter(this::isRecordingReadyForProcessing)
        .map(this::map)
        .forEach(this.publisher::publish);

    log.info("Finishing pending recordings task.");
  }

  /**
   * Checks whether recording is ready to be processed. Since we need a small buffer before
   * conference ending and all recording data to be present for now let's use one task interval as a
   * threshold.
   *
   * @param recording Recording to check.
   * @return True if recording is ready to be processed. False otherwise.
   */
  private boolean isRecordingReadyForProcessing(Recording recording) {
    return between(now(), recording.getCreatedAt()).abs().toMillis() > taskPeriod;
  }

  private ProcessPendingRecordingRequest map(Recording recording) {
    return new ProcessPendingRecordingRequest()
        .setRecordingId(recording.getId().toString());
  }
}
