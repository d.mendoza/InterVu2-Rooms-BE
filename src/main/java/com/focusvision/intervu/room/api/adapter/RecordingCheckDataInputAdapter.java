package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;

/**
 * Adapter for handling the recording check data.
 */
public interface RecordingCheckDataInputAdapter {

  /**
   * Processes the recording data. It should result in updating a recording
   * if all conditions are met.
   *
   * @param processingConferenceCheck Processing recording check data.
   */
  void process(ProcessingRecordingCheck processingConferenceCheck);
}
