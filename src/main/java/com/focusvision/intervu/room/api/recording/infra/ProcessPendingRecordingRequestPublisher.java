package com.focusvision.intervu.room.api.recording.infra;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.messaging.RabbitMqProducer;
import com.focusvision.intervu.room.api.model.messaging.ProcessPendingRecordingRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ specific implementation of {@link ProcessPendingRecordingRequest} publisher.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProcessPendingRecordingRequestPublisher {

  private final MessagingProperties messagingProperties;
  private final RabbitMqProducer rabbitMqProducer;

  /**
   * Publishes process pending recording request.
   *
   * @param request Request data.
   */
  public void publish(ProcessPendingRecordingRequest request) {
    log.debug("Sending process pending recording event (request: {})", request);

    try {
      rabbitMqProducer.send(messagingProperties.getProcessPendingRecordingQueue(), request);
    } catch (MessagingException e) {
      log.error(format("Error sending process pending recording event: %s.", request), e);
    }
  }
}
