package com.focusvision.intervu.room.api.event.participant.publisher;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantMicrophoneToggleEvent;

/**
 * Participant microphone toggle event publisher.
 */
public interface ParticipantMicrophoneToggleEventPublisher {

  void publish(ParticipantMicrophoneToggleEvent event);
}
