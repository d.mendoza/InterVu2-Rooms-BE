package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.mapper.UsedStimulusExportMapper;
import com.focusvision.intervu.room.api.export.model.UsedStimulusExportDto;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.StimulusService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link UsedStimulusExportService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UsedStimulusExportServiceImpl implements UsedStimulusExportService {

  private final ResearchSessionService researchSessionService;
  private final StimulusService stimulusService;
  private final UsedStimulusExportMapper usedStimulusExportMapper;

  @Override
  public Optional<List<UsedStimulusExportDto>> export(String platformId) {
    log.debug("Exporting used stimuli for room with platform ID {}.", platformId);

    return researchSessionService.fetch(platformId)
        .map(this::getUsedStimuli);
  }

  private List<UsedStimulusExportDto> getUsedStimuli(ResearchSession researchSession) {
    return stimulusService.getAll(researchSession.getId()).stream()
        .map(usedStimulusExportMapper::mapToDto)
        .toList();
  }
}
