package com.focusvision.intervu.room.api.twilio.service.impl;

import static com.twilio.type.Rule.Type.EXCLUDE;
import static com.twilio.type.Rule.Type.INCLUDE;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.publisher.ParticipantPresenceEventPublisher;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.twilio.service.TwilioIdentityProvider;
import com.focusvision.intervu.room.api.twilio.service.TwilioStatusService;
import com.twilio.rest.video.v1.room.Participant;
import com.twilio.rest.video.v1.room.participant.SubscribeRules;
import com.twilio.type.SubscribeRule;
import com.twilio.type.SubscribeRulesUpdate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link TwilioStatusService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TwilioStatusServiceImpl implements TwilioStatusService {

  private static final String PROP_STATUS_CALLBACK_EVENT_TYPE = "StatusCallbackEvent";
  private static final String PROP_PARTICIPANT_IDENTITY = "ParticipantIdentity";
  private static final String PROP_ROOM_SID = "RoomSid";
  private static final String ROOM_STARTED = "room-started";
  private static final String ROOM_ENDED = "room-ended";
  private static final String PARTICIPANT_CONNECTED = "participant-connected";
  private static final String PARTICIPANT_DISCONNECTED = "participant-disconnected";
  private static final String COMPOSITION_AVAILABLE = "composition-available";
  private static final String RECORDING_STARTED = "recording-started";

  private final ParticipantService participantService;
  private final TwilioIdentityProvider twilioIdentityProvider;
  private final ParticipantPresenceEventPublisher presenceEventPublisher;

  @Override
  public void processStatus(Map<String, String> status) {
    switch (status.get(PROP_STATUS_CALLBACK_EVENT_TYPE)) {
      case ROOM_STARTED -> log.info("Room started (status: {})", status);
      case ROOM_ENDED -> log.info("Room ended (status: {})", status);
      case PARTICIPANT_CONNECTED -> handleParticipantConnected(status);
      case PARTICIPANT_DISCONNECTED -> handleParticipantDisconnected(status);
      case COMPOSITION_AVAILABLE -> log.info("Composition available (status: {})", status);
      case RECORDING_STARTED -> log.info("Recording started (status: {})", status);
      default -> log.info("Unhandled event (status: {})", status);
    }
  }

  private void handleParticipantConnected(Map<String, String> status) {
    log.info("Participant connected (status: {})", status);

    var roomSid = status.get(PROP_ROOM_SID);
    var identity = status.get(PROP_PARTICIPANT_IDENTITY);
    var participantId = twilioIdentityProvider.extractSpeakerIdentity(identity);
    var connectedParticipants = getConnectedParticipants(roomSid);
    var phoneParticipants = connectedParticipants.stream()
        .map(Participant::getIdentity)
        .filter(twilioIdentityProvider::isPhoneParticipant)
        .toList();
    var listeners = connectedParticipants.stream()
        .map(Participant::getIdentity)
        .filter(twilioIdentityProvider::isListener)
        .toList();

    phoneParticipants.forEach(participantIdentity ->
        updateParticipantSubscriptionRules(roomSid, participantIdentity, listeners));

    participantId
        .flatMap(participantService::findById)
        .map(participant -> new ParticipantConnectedWithPhoneEvent(
            participant.getResearchSession().getId(),
            participant.getId()))
        .ifPresent(presenceEventPublisher::publish);
  }

  private void handleParticipantDisconnected(Map<String, String> status) {
    log.info("Participant disconnected (status: {})", status);

    var identity = status.get(PROP_PARTICIPANT_IDENTITY);
    var participantId = twilioIdentityProvider.extractSpeakerIdentity(identity);
    participantId
        .flatMap(participantService::findById)
        .map(participant -> new ParticipantDisconnectedWithPhoneEvent(
            participant.getResearchSession().getId(),
            participant.getId()))
        .ifPresent(presenceEventPublisher::publish);
  }

  private List<Participant> getConnectedParticipants(String roomSid) {
    var participantsReader = Participant.reader(roomSid)
        .setStatus(Participant.Status.CONNECTED)
        .read();

    var participants = new ArrayList<Participant>();
    participantsReader.forEach(participants::add);

    return participants;
  }

  private void updateParticipantSubscriptionRules(String roomSid,
                                                  String participantSid,
                                                  List<String> excludedParticipants) {
    log.debug("Updating subscription rules (roomSid: {}, participantSid: {}, "
        + "excludedParticipants: {})", roomSid, participantSid, excludedParticipants);

    var rules = new ArrayList<SubscribeRule>();
    rules.add(SubscribeRule.builder()
        .withType(INCLUDE).withAll()
        .build());

    excludedParticipants.stream()
        .map(publisher -> SubscribeRule.builder()
            .withType(EXCLUDE)
            .withPublisher(publisher)
            .build())
        .forEach(rules::add);

    var rulesUpdate = new SubscribeRulesUpdate(rules);

    SubscribeRules
        .updater(roomSid, participantSid)
        .setRules(new ObjectMapper().convertValue(rulesUpdate, Map.class))
        .update();
  }
}
