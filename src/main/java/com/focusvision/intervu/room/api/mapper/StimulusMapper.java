package com.focusvision.intervu.room.api.mapper;

import com.focusvision.intervu.room.api.model.dto.StimulusBarDataDto;
import com.focusvision.intervu.room.api.model.dto.StimulusDto;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import org.mapstruct.Mapper;

/**
 * Mapper for {@link Stimulus} entity.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring")
public interface StimulusMapper {

  /**
   * Maps stimulus to DTO.
   *
   * @param stimulus Stimulus to be mapped.
   * @return Resulting DTO.
   */
  StimulusBarDataDto map(Stimulus stimulus);

  /**
   * Maps stimulus data to DTO.
   *
   * @param stimulus Stimulus to be mapped.
   * @return Resulting DTO.
   */
  StimulusDto mapToDto(Stimulus stimulus);
}
