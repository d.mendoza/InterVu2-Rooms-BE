package com.focusvision.intervu.room.api.state.stimulus.service;

import static java.util.Optional.ofNullable;

import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import com.focusvision.intervu.room.api.model.dto.StimulusDto;
import com.focusvision.intervu.room.api.service.RoomStateService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusActionMessage;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;
import com.focusvision.intervu.room.api.state.stimulus.publisher.PollStimulusVoteEventPublisher;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link PollingStimulusActionService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PollingStimulusActionServiceImpl implements PollingStimulusActionService {

  private final RoomOffsetProvider roomOffsetProvider;
  private final PollStimulusVoteEventPublisher pollStimulusVoteEventPublisher;
  private final RoomStateService roomStateService;

  @Override
  public void vote(UUID roomId, UUID participantId,
                   PollStimulusActionMessage stimulusActionMessage) {
    log.debug("Handling poll stimulus vote action: {}", stimulusActionMessage);

    var canVote = ofNullable(roomStateService.getRoomState(roomId))
        .map(RoomStateDto::getStimulus)
        .filter(StimulusDto::getInteractionEnabled)
        .isPresent();

    if (canVote) {
      String content = stimulusActionMessage.getContent();
      UUID stimulusId = stimulusActionMessage.getStimulusId();
      Long offset =
          roomOffsetProvider.calculateOffset(roomId, stimulusActionMessage.getTimestamp());

      pollStimulusVoteEventPublisher
          .publish(PollStimulusVoteEvent.builder()
              .roomId(roomId)
              .stimulusId(stimulusId)
              .participantId(participantId)
              .content(content)
              .offset(offset)
              .build());
    }
  }

  @Override
  public void rank(UUID roomId, UUID participantId,
                   PollStimulusActionMessage stimulusActionMessage) {
    log.debug("Handling poll stimulus rank action: {}", stimulusActionMessage);

    var canRank = ofNullable(roomStateService.getRoomState(roomId))
        .map(RoomStateDto::getStimulus)
        .filter(StimulusDto::getInteractionEnabled)
        .isPresent();

    if (canRank) {
      String content = stimulusActionMessage.getContent();
      UUID stimulusId = stimulusActionMessage.getStimulusId();
      Long offset =
          roomOffsetProvider.calculateOffset(roomId, stimulusActionMessage.getTimestamp());

      pollStimulusVoteEventPublisher
          .publish(PollStimulusRankingEvent.builder()
              .roomId(roomId)
              .stimulusId(stimulusId)
              .participantId(participantId)
              .content(content)
              .offset(offset)
              .build());
    }
  }

  @Override
  public void answer(UUID roomId, UUID participantId,
                     PollStimulusActionMessage stimulusActionMessage) {
    log.debug("Handling poll stimulus answer action action: {}", stimulusActionMessage);

    var canAnswer = ofNullable(roomStateService.getRoomState(roomId))
        .map(RoomStateDto::getStimulus)
        .filter(StimulusDto::getInteractionEnabled)
        .isPresent();

    if (canAnswer) {
      String content = stimulusActionMessage.getContent();
      UUID stimulusId = stimulusActionMessage.getStimulusId();
      Long offset =
          roomOffsetProvider.calculateOffset(roomId, stimulusActionMessage.getTimestamp());

      pollStimulusVoteEventPublisher
          .publish(PollStimulusAnswerEvent.builder()
              .roomId(roomId)
              .stimulusId(stimulusId)
              .participantId(participantId)
              .content(content)
              .offset(offset)
              .build());
    }
  }
}
