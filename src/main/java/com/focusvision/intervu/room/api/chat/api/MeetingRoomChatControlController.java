package com.focusvision.intervu.room.api.chat.api;

import com.focusvision.intervu.room.api.chat.service.ChatControlService;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for meeting room chat control related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "meeting-room/chat", produces = MediaType.APPLICATION_JSON_VALUE)
public class MeetingRoomChatControlController {

  private final ChatControlService chatControlService;

  /**
   * Enables meeting room respondents chat.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("respondents/control/enable")
  @ApiOperation(
      value = "Enable meeting room respondents chat",
      notes = "Enable meeting room respondents chat")
  public void enable(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Participant {} enabling meeting room respondents chat.", caller.getId());

    chatControlService.enableMeetingRoomRespondentsChat(caller);
  }

  /**
   * Disables meeting room respondents chat.
   *
   * @param caller Authenticated participant.
   */
  @PutMapping("respondents/control/disable")
  @ApiOperation(
      value = "Disable meeting room respondents chat",
      notes = "Disable meeting room respondents chat")
  public void disable(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Participant {} disabling meeting room respondents chat.", caller.getId());

    chatControlService.disableMeetingRoomRespondentsChat(caller);
  }

}
