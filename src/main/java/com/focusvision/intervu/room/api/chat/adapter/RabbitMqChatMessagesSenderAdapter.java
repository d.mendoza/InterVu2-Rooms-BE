package com.focusvision.intervu.room.api.chat.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.messaging.RabbitMqProducer;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionChatMessagesData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ specific implementation of {@link ChatMessagesSenderAdapter}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqChatMessagesSenderAdapter implements ChatMessagesSenderAdapter {

  private final MessagingProperties messagingProperties;
  private final RabbitMqProducer rabbitMqProducer;

  @Override
  public void send(ResearchSessionChatMessagesData researchSessionChatMessagesData) {
    log.debug("Sending chat messages of research session: [{}]",
        researchSessionChatMessagesData.getId());

    try {
      rabbitMqProducer.send(messagingProperties.getResearchSessionChatMessagesQueue(),
          researchSessionChatMessagesData);
    } catch (MessagingException e) {
      log.error(format("Error sending chat messages for research session: %s.",
          researchSessionChatMessagesData.getId()), e);
    }
  }
}
