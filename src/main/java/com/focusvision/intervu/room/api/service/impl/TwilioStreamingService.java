package com.focusvision.intervu.room.api.service.impl;

import static com.focusvision.intervu.room.api.common.model.AudioChannel.TRANSLATOR;
import static com.focusvision.intervu.room.api.common.model.RecordingType.SCREENSHARE;
import static com.focusvision.intervu.room.api.common.model.RecordingType.VIDEO;
import static com.focusvision.intervu.room.api.twilio.util.TwilioTrackNamingUtil.RESPONDENT_CAMERA_WILDCARD;
import static com.focusvision.intervu.room.api.twilio.util.TwilioTrackNamingUtil.isOldTrackNamingUsed;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.function.Predicate.not;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.RecordingType;
import com.focusvision.intervu.room.api.configuration.domain.TwilioProperties;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.streaming.GroupRoom;
import com.focusvision.intervu.room.api.model.streaming.GroupRoomParticipant;
import com.focusvision.intervu.room.api.model.streaming.GroupRoomRecording;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import com.focusvision.intervu.room.api.model.streaming.RecordingLayoutData;
import com.focusvision.intervu.room.api.service.StreamingService;
import com.focusvision.intervu.room.api.twilio.service.TwilioLayoutService;
import com.twilio.base.ResourceSet;
import com.twilio.http.HttpMethod;
import com.twilio.http.NetworkHttpClient;
import com.twilio.http.Request;
import com.twilio.http.Response;
import com.twilio.http.TwilioRestClient;
import com.twilio.jwt.accesstoken.AccessToken;
import com.twilio.jwt.accesstoken.VideoGrant;
import com.twilio.rest.Domains;
import com.twilio.rest.video.v1.Composition;
import com.twilio.rest.video.v1.CompositionCreator;
import com.twilio.rest.video.v1.Recording;
import com.twilio.rest.video.v1.Room;
import com.twilio.rest.video.v1.room.Participant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * Twilio specific implementation of a {@link StreamingService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "app.conference", value = "provider", havingValue = "TWILIO")
public class TwilioStreamingService implements StreamingService {

  private static final int VIDEO_HEIGHT = 720;
  private static final int VIDEO_WIDTH = 1280;
  private static final String LISTENER_TRACK_NAME_PATTERN = "listener_.+";
  private static final String OPERATOR_TRACK_NAME_PATTERN = "operator_.+";
  private static final String TRANSLATOR_TRACK_NAME_PREFIX = "TRANSLATOR_*";
  private static final String OBSERVER_TRACK_NAME_PREFIX = "OBSERVER_*";

  private final TwilioProperties configuration;
  private final TwilioLayoutService twilioLayoutService;

  @Override
  public GroupRoom createGroupRoom(UUID id) {
    log.info("Creating Twilio room {}.", id);

    Room room = Room.creator()
        .setRecordParticipantsOnConnect(true)
        .setStatusCallback(configuration.getCallbackUrl())
        .setType(Room.RoomType.GROUP)
        .setUniqueName(id.toString())
        .create();

    log.info("Created Twilio room {}.", room.getSid());

    return new GroupRoom()
        .setSid(room.getSid())
        .setDateCreatedMillisGmt(room.getDateCreated().toInstant().toEpochMilli())
        .setStatus(room.getStatus());
  }

  @Override
  public GroupRoom getGroupRoom(String roomSid) {
    log.info("Getting Twilio room {}.", roomSid);

    Room room = Room.fetcher(roomSid).fetch();

    return new GroupRoom()
        .setSid(room.getSid())
        .setDateCreatedMillisGmt(room.getDateCreated().toInstant().toEpochMilli())
        .setDuration(room.getDuration() != null ? room.getDuration() * 1000 : 0)
        .setStatus(room.getStatus());
  }

  @Override
  public void completeGroupRoom(String roomSid) {
    log.debug("Completing Twilio room with SID {}.", roomSid);

    try {
      Room.updater(roomSid, Room.RoomStatus.COMPLETED)
          .update();
    } catch (Exception e) {
      log.error(String.format("Error completing Twilio room with SID %s", roomSid), e);
    }
  }

  @Override
  public GroupRoomParticipant createGroupRoomParticipant(UUID participantId, String roomSid) {
    log.debug("Generating access token for participant {} in Twilio room with SID {}.",
        participantId, roomSid);

    VideoGrant grant = new VideoGrant().setRoom(roomSid);
    AccessToken accessToken = new AccessToken.Builder(
        configuration.getAccountSid(),
        configuration.getKeySid(),
        configuration.getKeySecret())
        .identity(participantId.toString())
        .ttl(14400)
        .grant(grant)
        .build();
    return new GroupRoomParticipant(accessToken.getId(), accessToken.toJwt());
  }

  @Override
  public GroupRoomRecording createGroupRoomRecording(String roomSid,
                                                     boolean isPrivacyOn,
                                                     CompositionLayout layout,
                                                     AudioChannel audioChannel) {
    log.debug("Creating group room composition (roomSid: {}, isPrivacyOn: {}, layout: {})",
        roomSid,
        isPrivacyOn,
        layout);

    deleteNotNeededRecordings(roomSid);

    var recordings = getParticipantsRecordings(roomSid);
    var excludedVideos = extractExcludedVideos(recordings, isPrivacyOn);
    var audioSources = getAudioSources(recordings, audioChannel);

    var layoutData = twilioLayoutService
        .createLayoutData(recordings, layout, excludedVideos);
    var videoLayoutData = layoutData.map(RecordingLayoutData::getLayout).orElse(null);
    var screenShareBounds = layoutData.map(RecordingLayoutData::getScreenshareBounds)
        .orElse(null);

    var composition = createGroupRoomRecording(
        roomSid,
        videoLayoutData,
        audioSources);

    return new GroupRoomRecording()
        .setSid(composition.getSid())
        .setDuration(composition.getDuration() * 1000)
        .setProcessed(Composition.Status.COMPLETED.equals(composition.getStatus()))
        .setScreenshareBounds(screenShareBounds);
  }

  private Composition createGroupRoomRecording(String roomSid,
                                               Map<String, Object> videoLayoutData,
                                               Set<String> audioSources) {
    var compositionCreator = Composition.creator(roomSid)
        .setVideoLayout(videoLayoutData)
        .setResolution(format("%sx%s", VIDEO_WIDTH, VIDEO_HEIGHT))
        .setFormat(Composition.Format.MP4)
        .setTrim(false)
        .setAudioSources(audioSources.stream().toList());

    var composition = compositionCreator.create();

    log.info("Finished creating Twilio composition (roomSid: {}, compositionSid: {})",
        roomSid,
        composition.getSid());

    return composition;
  }

  @Override
  public GroupRoomRecording getGroupRoomRecording(String recordingSid) throws IntervuRoomException {
    log.info("Getting Twilio room {} recording composition.", recordingSid);

    Composition composition = Composition.fetcher(recordingSid).fetch();

    return new GroupRoomRecording()
        .setSid(composition.getSid())
        .setDuration(composition.getDuration() * 1000)
        .setProcessed(Composition.Status.COMPLETED.equals(composition.getStatus()))
        .setError(Composition.Status.FAILED.equals(composition.getStatus())
            || Composition.Status.DELETED.equals(composition.getStatus()));
  }

  @Override
  public String getGroupRoomRecordingLink(String recordingSid) throws IntervuRoomException {
    log.debug("Getting Twilio room recording {} link.", recordingSid);

    HttpClientBuilder clientBuilder = HttpClientBuilder.create();
    clientBuilder.disableRedirectHandling();

    TwilioRestClient restClient = new TwilioRestClient
        .Builder(configuration.getAccountSid(), configuration.getApiKey())
        .httpClient(new NetworkHttpClient(clientBuilder))
        .build();

    Request request = new Request(
        HttpMethod.GET,
        Domains.VIDEO.toString(),
        "/v1/Compositions/" + recordingSid + "/Media?Ttl=3600",
        restClient.getRegion());

    try {
      Response response = restClient.request(request);
      JSONObject json = new JSONObject(response.getContent());
      return json.getString("redirect_to");
    } catch (JSONException e) {
      log.error(format("Error fetching recording %s", recordingSid), e);
      throw new IntervuRoomException(
          format("Error getting recording %s media link.", recordingSid));
    }
  }

  @Override
  public void deleteGroupRoomRecordings(String roomSid) {
    log.debug("Deleting all Twilio room {} recordings and compositions.", roomSid);

    getAllRoomRecordings(roomSid).forEach(this::deleteRecordingSilently);
    getAllRoomCompositions(roomSid).forEach(this::deleteComposition);
  }

  @Override
  public List<ParticipantGroupRoomRecording> getParticipantsRecordings(String roomSid) {
    log.debug("Getting Twilio room {} participants recordings.", roomSid);

    var participantGroupRoomRecordings = new ArrayList<ParticipantGroupRoomRecording>();
    var recordings = Recording.reader().setGroupingSid(roomSid).read();

    for (var recording : recordings) {
      participantGroupRoomRecordings.add(extractParticipantGroupRoomRecording(recording));
    }

    return participantGroupRoomRecordings;
  }

  @Override
  public List<ParticipantGroupRoomRecording> getParticipantRecordings(String roomSid,
                                                                      UUID participantId) {
    log.debug("Getting recordings for respondent {} and room {}", participantId, roomSid);

    var roomParticipant = findRoomParticipant(roomSid, participantId);

    return roomParticipant
        .map(participant -> getParticipantsRecordings(roomSid).stream()
            .filter(recording -> recording.getParticipantId().equals(participant.getIdentity()))
            .toList())
        .orElseGet(List::of);
  }

  @Override
  public void kickParticipantFromRoom(String roomSid, UUID participantId) {
    log.info("Kicking participant {} from Twilio room with SID {}", participantId, roomSid);

    findConnectedRoomParticipant(participantId, roomSid)
        .ifPresentOrElse(participant -> {
          try {
            Participant
                .updater(roomSid, participant.getSid())
                .setStatus(Participant.Status.DISCONNECTED)
                .update();
          } catch (Exception e) {
            log.error(
                format("Error kicking participant %s from Twilio room %s.", participantId, roomSid),
                e);
          }
        }, () -> log.info("Connected participant {} to Twilio room {} not found", participantId,
            roomSid));
  }

  @Override
  public Set<Participant> getRoomParticipants(String roomSid) {
    log.debug("Getting room participants (room: {})", roomSid);

    Set<Participant> participants = new HashSet<>();
    Participant.reader(roomSid)
        .read().forEach(participants::add);

    return participants;
  }

  private void deleteParticipantRecordings(String participantIdentity) {
    log.info("Deleting recordings (participant: {})", participantIdentity);

    var recordings = Recording.reader()
        .setGroupingSid(List.of(participantIdentity))
        .limit(MAX_VALUE)
        .read();
    try {
      for (Recording recording : recordings) {
        deleteRecording(recording.getSid());
      }
    } catch (Exception e) {
      log.error(format("Error deleting Twilio recording (participant: %s)", participantIdentity),
          e);
      throw e;
    }
  }

  /**
   * Deletes all unneeded recordings of listener and operator participants. Once deleted, those
   * participant recordings won't be available again.
   *
   * @param roomSid Room SID.
   */
  private void deleteNotNeededRecordings(String roomSid) {
    var participants = getRoomParticipants(roomSid).stream()
        .filter(
            participant ->
                participant.getIdentity().matches(LISTENER_TRACK_NAME_PATTERN)
                    || participant.getIdentity().matches(OPERATOR_TRACK_NAME_PATTERN))
        .map(Participant::getSid)
        .distinct().toList();

    for (String participant : participants) {
      deleteParticipantRecordings(participant);
    }
  }

  private Set<String> extractExcludedVideos(List<ParticipantGroupRoomRecording> recordings,
                                            boolean isPrivacyOn) {
    Predicate<ParticipantGroupRoomRecording> isRespondentVideo =
        recording -> recording.isVideo() && recording.isRespondent();
    // Observer track prefix
    // Respondent video track prefix
    var excludedVideos = new HashSet<String>();
    excludedVideos.add(OBSERVER_TRACK_NAME_PREFIX);
    excludedVideos.add(TRANSLATOR_TRACK_NAME_PREFIX);
    if (isPrivacyOn) {
      if (isOldTrackNamingUsed(recordings)) {
        recordings.stream()
            .filter(isRespondentVideo)
            .map(ParticipantGroupRoomRecording::getTrackName)
            .forEach(excludedVideos::add);
      } else {
        excludedVideos.add(RESPONDENT_CAMERA_WILDCARD);
      }
    }

    return excludedVideos;
  }

  /**
   * Extracting participant audio recordings that will be present in the composition. If composition
   * is translated only translator audio should be included. Otherwise, participants with observer
   * or translator role should be excluded by {@link CompositionCreator#audioSourcesExcluded}.
   * Observers room role will be applied to participants, that are coming from control panel as:
   * observers, guests, admins, project managers or ipos.
   *
   * @param recordings   All audio recording from participants in room ( both web and phone )
   * @param audioChannel Recording audio channel.
   * @return Filtered list of audio recordings
   */
  private Set<String> getAudioSources(List<ParticipantGroupRoomRecording> recordings,
                                      AudioChannel audioChannel) {
    if (TRANSLATOR.equals(audioChannel)) {
      return Set.of(TRANSLATOR_TRACK_NAME_PREFIX);
    } else {
      return recordings.stream()
          .filter(ParticipantGroupRoomRecording::isAudio)
          .filter(not(ParticipantGroupRoomRecording::isObserver))
          .filter(not(ParticipantGroupRoomRecording::isTranslator))
          .map(ParticipantGroupRoomRecording::getTrackName)
          .collect(Collectors.toSet());
    }
  }

  private ResourceSet<Participant> connectedParticipants(String sid) {
    log.debug("Listing room {} connected participants.", sid);

    return Participant.reader(sid)
        .setStatus(Participant.Status.CONNECTED)
        .read();
  }

  private Optional<Participant> findConnectedRoomParticipant(UUID participantId, String roomSid) {
    ResourceSet<Participant> participants = this.connectedParticipants(roomSid);
    for (Participant participant : participants) {
      if (participant.getIdentity().equals(participantId.toString())) {
        return of(participant);
      }
    }

    return empty();
  }

  private List<String> getAllRoomRecordings(String roomSid) {
    log.debug("Getting all recordings data for Twilio room {}.", roomSid);

    var recordings = new ArrayList<String>();

    Recording.reader()
        .setGroupingSid(roomSid)
        .limit(100)
        .read()
        .forEach(recording -> recordings.add(recording.getSid()));

    return recordings;
  }

  private List<String> getAllRoomCompositions(String roomSid) {
    log.debug("Getting all compositions data for Twilio room {}.", roomSid);

    var compositions = new ArrayList<String>();

    Composition.reader()
        .setRoomSid(roomSid)
        .read()
        .forEach(composition -> compositions.add(composition.getSid()));

    return compositions;
  }

  private void deleteRecordingSilently(String recordingSid) {
    log.info("Deleting Twilio recording {}.", recordingSid);

    try {
      deleteRecording(recordingSid);
    } catch (Exception e) {
      // ignore
    }
  }

  private void deleteRecording(String recordingSid) {
    log.info("Deleting Twilio recording {}.", recordingSid);

    try {
      Recording.deleter(recordingSid).delete();
    } catch (Exception e) {
      log.error(format("Error deleting Twilio recording %s.", recordingSid), e);
      throw e;
    }
  }

  private void deleteComposition(String compositionSid) {
    log.info("Deleting Twilio composition {}.", compositionSid);

    try {
      Composition.deleter(compositionSid).delete();
    } catch (Exception e) {
      log.error(format("Error deleting Twilio composition %s.", compositionSid), e);
    }
  }

  private ParticipantGroupRoomRecording extractParticipantGroupRoomRecording(Recording recording) {
    String[] trackDetails = this.getTrackDetails(recording);

    var participantGroupRoomRecording = new ParticipantGroupRoomRecording();
    if (trackDetails.length > 2) {
      boolean oldTrackNamingUsed = isOldTrackNamingUsed(recording.getTrackName());

      switch (trackDetails[oldTrackNamingUsed ? 2 : 1]) {
        case "camera" -> participantGroupRoomRecording.setRecordingType(VIDEO);
        case "microphone" -> participantGroupRoomRecording.setRecordingType(RecordingType.AUDIO);
        case "screenshare" -> participantGroupRoomRecording.setRecordingType(SCREENSHARE);
        default -> participantGroupRoomRecording.setRecordingType(RecordingType.DATA);
      }

      participantGroupRoomRecording.setParticipantType(trackDetails[0]);
      participantGroupRoomRecording.setParticipantId(trackDetails[oldTrackNamingUsed ? 1 : 2]);
    }

    if (participantGroupRoomRecording.getRecordingType() == null) {
      if (recording.getType().equals(Recording.Type.VIDEO)) {
        participantGroupRoomRecording.setRecordingType(VIDEO);
      } else if (recording.getType().equals(Recording.Type.AUDIO)) {
        participantGroupRoomRecording.setRecordingType(RecordingType.AUDIO);
      } else {
        participantGroupRoomRecording.setRecordingType(RecordingType.DATA);
      }
    }

    participantGroupRoomRecording.setSid(recording.getSid());
    participantGroupRoomRecording.setTrackName(recording.getTrackName());
    participantGroupRoomRecording.setStartedAt(recording.getDateCreated().toInstant());
    participantGroupRoomRecording.setDuration(recording.getDuration());

    return participantGroupRoomRecording;
  }

  private String[] getTrackDetails(Recording recording) {
    return recording.getTrackName().split("_");
  }

  private ResourceSet<Participant> roomParticipants(String roomSid) {
    return Participant.reader(roomSid)
        .read();
  }

  private Optional<Participant> findRoomParticipant(String roomSid, UUID participantId) {
    ResourceSet<Participant> participants = this.roomParticipants(roomSid);
    for (Participant participant : participants) {
      if (participant.getIdentity().equals(participantId.toString())) {
        return of(participant);
      }
    }

    return empty();
  }
}
