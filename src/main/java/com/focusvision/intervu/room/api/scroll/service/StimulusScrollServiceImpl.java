package com.focusvision.intervu.room.api.scroll.service;

import com.focusvision.intervu.room.api.scroll.model.StimulusScrollMessage;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import com.focusvision.intervu.room.api.stimulus.event.publisher.StimulusScrollEventPublisher;
import com.focusvision.intervu.room.api.stimulus.model.StimulusScrollEvent;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link StimulusScrollService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusScrollServiceImpl implements StimulusScrollService {

  private final RoomOffsetProvider roomOffsetProvider;
  private final StimulusScrollEventPublisher publisher;

  @Override
  public void scroll(UUID roomId,
                     UUID stimulusId,
                     UUID participantId,
                     StimulusScrollMessage stimulusScrollMessage) {
    log.debug("Handling stimulus {} scroll action: {}", stimulusId, stimulusScrollMessage);

    String content = stimulusScrollMessage.getContent();
    Long offset = roomOffsetProvider.calculateOffset(roomId, stimulusScrollMessage.getTimestamp());

    publisher.publish(new StimulusScrollEvent(roomId, stimulusId, participantId, content, offset));
  }
}
