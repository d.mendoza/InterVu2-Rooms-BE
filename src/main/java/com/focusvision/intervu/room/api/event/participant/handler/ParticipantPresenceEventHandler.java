package com.focusvision.intervu.room.api.event.participant.handler;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedToRoomEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantReadyEvent;

/**
 * Participant presence event handler.
 */
public interface ParticipantPresenceEventHandler {

  /**
   * Handles participant connected event.
   *
   * @param event Event data.
   */
  void handle(ParticipantConnectedEvent event);

  /**
   * Handles participant ready over WEB (browser) event.
   *
   * @param event Event data.
   */
  void handle(ParticipantReadyEvent event);

  /**
   * Handles participant connected to room over WEB (browser) event.
   *
   * @param event Event data.
   */
  void handle(ParticipantConnectedToRoomEvent event);

  /**
   * Handles participant connected with phone event.
   *
   * @param event Event data.
   */
  void handle(ParticipantConnectedWithPhoneEvent event);

  /**
   * Handles participant disconnected over WEB (browser) event.
   *
   * @param event Event data.
   */
  void handle(ParticipantDisconnectedEvent event);

  /**
   * Handles participant disconnected with phone event.
   *
   * @param event Event data.
   */
  void handle(ParticipantDisconnectedWithPhoneEvent event);
}
