package com.focusvision.intervu.room.api.stimulus.event.publisher;

import com.focusvision.intervu.room.api.stimulus.model.StimulusScrollEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * {@link ApplicationEventPublisher} specific implementation of a stimulus scroll events publisher.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ApplicationStimulusScrollEventPublisher implements StimulusScrollEventPublisher {

  private final ApplicationEventPublisher publisher;

  @Override
  public void publish(StimulusScrollEvent event) {
    log.debug("Publishing stimulus scroll event: [{}].", event);

    publisher.publishEvent(event);
  }

}
