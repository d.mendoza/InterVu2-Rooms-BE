package com.focusvision.intervu.room.api.state;

import java.util.UUID;

/**
 * Provider for room state change log data.
 *
 * @author Branko Ostojic
 */
public interface RoomOffsetProvider {

  /**
   * Calculates the current offset for the specified room. If the room has not started, offset will
   * always be 0.
   *
   * @param roomId Room ID.
   * @return Offset from the room start time or 0 if the room has not started.
   */
  Long calculateOffset(UUID roomId);

  /**
   * Calculates the current offset for the specified room and provided timestamp. If the room has
   * not started, offset will always be 0.
   *
   * @param roomId    Room ID.
   * @param timestamp Timestamp for which to calculate the offset.
   * @return Offset from the room start time or 0 if the room has not started.
   */
  Long calculateOffset(UUID roomId, long timestamp);

  /**
   * Aligns start offset with the conference recording offset for better events sync.
   *
   * @param startOffset     Start offset.
   * @param recordingOffset Conference recording offset.
   * @return Aligned offset.
   */
  Long alignWithRecordingOffset(Long startOffset, Integer recordingOffset);

}
