package com.focusvision.intervu.room.api.state.stimulus.repository;

import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for {@link StimulusActionState} entity.
 */
public interface StimulusStateRepository extends JpaRepository<StimulusActionState, UUID> {

  /**
   * Finds the action state for stimulus ID and participant ID.
   *
   * @param stimulusId    Stimulus ID.
   * @param participantId Participant ID.
   * @return Matching record or empty {@link Optional}.
   */
  Optional<StimulusActionState> findByStimulusIdAndParticipantId(UUID stimulusId,
                                                                 UUID participantId);

  /**
   * Finds the action state for stimulus ID and participant ID.
   *
   * @param stimulusId Stimulus ID.
   * @return Matching records.
   */
  List<StimulusActionState> findByStimulusId(UUID stimulusId);

  /**
   * Deletes the action states for stimulus ID.
   *
   * @param stimulusId Stimulus ID.
   */
  void deleteByStimulusId(UUID stimulusId);
}
