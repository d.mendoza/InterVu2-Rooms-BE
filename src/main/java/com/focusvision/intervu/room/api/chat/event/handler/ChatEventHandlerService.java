package com.focusvision.intervu.room.api.chat.event.handler;

import com.focusvision.intervu.room.api.bookmark.event.model.BookmarkAddedEvent;
import com.focusvision.intervu.room.api.bookmark.event.model.PiiBookmarkAddedEvent;
import com.focusvision.intervu.room.api.chat.service.ChatHandlerService;
import com.focusvision.intervu.room.api.stimulus.snapshot.SnapshotAddedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Chat related events handler.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ChatEventHandlerService {

  private final ChatHandlerService chatHandlerService;

  /**
   * Handles provided bookmark added event.
   *
   * @param event Bookmark added event.
   */
  @EventListener
  public void handle(BookmarkAddedEvent event) {
    log.info("Handling bookmark added event (event: {})", event);

    chatHandlerService.handleMeetingRoomBookmarkMessage(
        event.participant(), event.bookmark().note());
  }

  /**
   * Handles provided PII bookmark added event.
   *
   * @param event PII Bookmark added event.
   */
  @EventListener
  public void handle(PiiBookmarkAddedEvent event) {
    log.info("Handling PII bookmark added event (event: {})", event);

    chatHandlerService.handleMeetingRoomPiiMessage(event.participant());
  }

  /**
   * Handles provided snapshot added event.
   *
   * @param event Snapshot added event.
   */
  @EventListener
  public void handle(SnapshotAddedEvent event) {
    log.info("Handling snapshot added event (event: {})", event);

    chatHandlerService.handleMeetingRoomSnapshotMessage(
        event.participant(), event.snapshotName());
  }
}
