package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.entity.Participant;
import java.util.Optional;

/**
 * Service used for invitation related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface InvitationService {

  /**
   * Finds invitation details for provided invitation link.
   *
   * @param invitationLink Invitation link.
   * @return Matching invitation details or empty {@code Optional}
   */
  Optional<Participant> findInvitationDetails(String invitationLink);

}
