package com.focusvision.intervu.room.api.event.stimulus.handler;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusSyncEnabledEvent;

/**
 * Handler for {@link StimulusSyncEnabledEvent}.
 */
public interface StimulusSyncEnabledEventHandler {

  /**
   * Handles the provided {@link StimulusSyncEnabledEvent}.
   *
   * @param event Stimulus sync enabled event.
   */
  void handle(StimulusSyncEnabledEvent event);
}
