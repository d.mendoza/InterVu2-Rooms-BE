package com.focusvision.intervu.room.api.stimulus.document.service;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.UUID;

/**
 * Service for handling document stimulus actions.
 */
public interface DocumentStimulusActionService {

  /**
   * Handle document stimulus set active page action. Action content is provided as string because
   * API is simply persisting/publishing it and not taking any action with the content itself.
   *
   * @param stimulusId Stimulus ID.
   * @param content    Action content.
   * @param timestamp  Action timestamp in UTC.
   * @param caller     Participant performing action.
   */
  void setActivePage(UUID stimulusId,
                     String content,
                     Long timestamp,
                     AuthenticatedParticipant caller);

}
