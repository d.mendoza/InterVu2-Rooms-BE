package com.focusvision.intervu.room.api.common.provider;

import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import java.util.Optional;
import java.util.UUID;

/**
 * Research session data provider.
 *
 * @author Branko Ostojic
 */
public interface ResearchSessionProvider {

  /**
   * Get research session details by the ID.
   *
   * @param id ResearchSession ID.
   * @return ResearchSession details.
   */
  ResearchSession get(UUID id);

  /**
   * Finds research session for provided listener PIN.
   *
   * @param listenerPin Listener PIN.
   * @return Matching research session or empty {@link Optional}.
   */
  Optional<ResearchSession> findByListenerPin(String listenerPin);

  /**
   * Finds research session for provided operator PIN.
   *
   * @param operatorPin Operator PIN.
   * @return Matching research session or empty {@link Optional}.
   */
  Optional<ResearchSession> findByOperatorPin(String operatorPin);

  /**
   * Finds research session for provided speaker PIN.
   *
   * @param speakerPin Speaker PIN.
   * @return Matching research session or empty {@link Optional}.
   */
  Optional<ResearchSession> findBySpeakerPin(String speakerPin);
}
