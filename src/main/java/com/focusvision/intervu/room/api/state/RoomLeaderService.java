package com.focusvision.intervu.room.api.state;

import java.util.UUID;

/**
 * Service for room leader related operations.
 */
public interface RoomLeaderService {

  /**
   * Checks if the participant is the room leader of the given room.
   *
   * @param roomId        Room id.
   * @param participantId Participant id.
   */
  boolean isRoomLeader(UUID roomId, UUID participantId);
}
