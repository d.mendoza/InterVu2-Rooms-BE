package com.focusvision.intervu.room.api.model.event;

import static com.focusvision.intervu.room.api.event.stimulus.model.StimulusGlobalDataType.BROADCAST;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusGlobalDataType;
import com.focusvision.intervu.room.api.model.dto.PollStimulusRespondentResultDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DTO representing the poll stimulus broadcast results event.
 */
@Getter
@Setter
@Builder
@ToString
@ApiModel("PollStimulusBroadcastResultsDataEvent")
public class PollStimulusBroadcastResultsDataEvent {

  @ApiModelProperty("Event Type")
  private final StimulusGlobalDataType eventType = BROADCAST;
  /**
   * Event destination.
   */
  @ApiModelProperty("Event destination")
  private final String destination;
  /**
   * Event data payload.
   */
  @ApiModelProperty("Event data")
  private final PollStimulusRespondentResultDto data;

}
