package com.focusvision.intervu.room.api.state.stimulus.publisher;

import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;

/**
 * {@link StimulusActionEventPublisher} specific implementation of a poll vote events publisher.
 */
public interface PollStimulusVoteEventPublisher {

  /**
   * Publishes the provided poll stimulus vote event.
   *
   * @param event Drawing sync enabled event.
   */
  void publish(PollStimulusVoteEvent event);

  /**
   * Publishes the provided poll stimulus ranking event.
   *
   * @param event Poll stimulus ranking event.
   */
  void publish(PollStimulusRankingEvent event);

  /**
   * Publishes the provided poll stimulus text answer event.
   *
   * @param event Poll stimulus text answer event.
   */
  void publish(PollStimulusAnswerEvent event);
}
