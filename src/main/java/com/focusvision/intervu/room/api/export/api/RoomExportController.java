package com.focusvision.intervu.room.api.export.api;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.export.model.RoomDrawingLogExportDto;
import com.focusvision.intervu.room.api.export.model.RoomScrollLogExportDto;
import com.focusvision.intervu.room.api.export.model.RoomStateLogExportDto;
import com.focusvision.intervu.room.api.export.service.RoomDrawingExportService;
import com.focusvision.intervu.room.api.export.service.RoomScrollExportService;
import com.focusvision.intervu.room.api.export.service.RoomStateExportService;
import com.focusvision.intervu.room.api.security.IntervuUser;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room data export operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "export/room", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomExportController {

  private final RoomStateExportService roomStateExportService;
  private final RoomDrawingExportService roomDrawingExportService;
  private final RoomScrollExportService roomScrollExportService;

  /**
   * Exports room state data.
   *
   * @param caller     Authenticated participant.
   * @param platformId Room platform ID.
   * @return Room state export data.
   */
  @GetMapping("{platformId}/state")
  @ApiOperation(
      value = "Gets room state change log.",
      notes = "Gets room state change log.")
  public List<RoomStateLogExportDto> exportRoomStateLogs(
      @PathVariable String platformId,
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Exporting room {} state log data.", platformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return roomStateExportService.export(platformId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  /**
   * Exports room drawing data.
   *
   * @param caller     Authenticated participant.
   * @param platformId Room platform ID.
   * @return Room state export data.
   */
  @GetMapping("{platformId}/drawings")
  @ApiOperation(
      value = "Gets room drawing logs.",
      notes = "Gets room drawing logs.")
  public List<RoomDrawingLogExportDto> exportRoomDrawingLogs(
      @PathVariable String platformId,
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Exporting room {} drawing log data.", platformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return roomDrawingExportService.export(platformId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  /**
   * Exports room scroll data.
   *
   * @param caller     Authenticated participant.
   * @param platformId Room platform ID.
   * @return Room state export data.
   */
  @GetMapping("{platformId}/scroll")
  @ApiOperation(
      value = "Gets room scroll logs.",
      notes = "Gets room scroll logs.")
  public List<RoomScrollLogExportDto> exportRoomScrollLogs(
      @PathVariable String platformId,
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Exporting room {} drawing log data.", platformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return roomScrollExportService.export(platformId)
        .orElseThrow(ResourceNotFoundException::new);
  }

}
