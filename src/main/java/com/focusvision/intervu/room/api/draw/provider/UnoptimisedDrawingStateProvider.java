package com.focusvision.intervu.room.api.draw.provider;

import static com.focusvision.intervu.room.api.common.model.DrawingActionType.CLEAR;
import static java.util.Comparator.comparing;
import static java.util.function.BinaryOperator.maxBy;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import com.focusvision.intervu.room.api.draw.mapper.DrawingLogMapper;
import com.focusvision.intervu.room.api.draw.model.DrawingLog;
import com.focusvision.intervu.room.api.draw.model.DrawingLogDto;
import com.focusvision.intervu.room.api.draw.service.DrawingService;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Unoptimised implementation of a {@link DrawingStateProvider}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UnoptimisedDrawingStateProvider implements DrawingStateProvider {

  private final DrawingService drawingService;
  private final DrawingLogMapper drawingLogMapper;

  @Override
  public List<DrawingLogDto> getCurrentState(UUID roomId, UUID contextId) {
    log.debug("Fetching all drawing logs for room {} and context {}.", roomId, contextId);

    var logs = drawingService.getRoomDrawingLogs(roomId, contextId);
    var mostRecentClear = logs.stream()
        .filter(log -> CLEAR.equals(log.getActionType()))
        .collect(toMap(DrawingLog::getParticipantId, identity(),
            maxBy(comparing(DrawingLog::getOffset))));

    return logs.stream()
        .filter(log -> isAfterClearEvent(log, mostRecentClear))
        .map(drawingLogMapper::mapToDto)
        .toList();
  }

  private boolean isAfterClearEvent(DrawingLog log, Map<UUID, DrawingLog> mostRecentClear) {
    return !mostRecentClear.containsKey(log.getParticipantId())
        || log.getOffset() >= mostRecentClear.get(log.getParticipantId()).getOffset();
  }
}
