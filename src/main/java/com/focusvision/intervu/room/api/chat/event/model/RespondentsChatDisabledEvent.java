package com.focusvision.intervu.room.api.chat.event.model;

import java.util.UUID;

/**
 * Model representing the respondents chat disabled event.
 *
 * @author Branko Ostojic
 */
public record RespondentsChatDisabledEvent(UUID roomId) {
}
