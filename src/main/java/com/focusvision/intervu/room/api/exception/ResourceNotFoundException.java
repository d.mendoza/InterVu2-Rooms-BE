package com.focusvision.intervu.room.api.exception;

import static com.focusvision.intervu.room.api.exception.ErrorCode.NOT_FOUND;

import lombok.Getter;

/**
 * Exception class used for resources not found case.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Getter
public class ResourceNotFoundException extends IntervuRoomException {

  public ResourceNotFoundException() {
    super(NOT_FOUND);
  }

}
