package com.focusvision.intervu.room.api.stimulus.event.handler;

import static com.focusvision.intervu.room.api.common.model.DrawingActionType.CLEAR;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.DRAW;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.REDO;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.UNDO;

import com.focusvision.intervu.room.api.draw.service.DrawingService;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawClearEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawRedoEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawUndoEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * {@link ApplicationEvent} specific implementation of a stimulus drawing events handler for
 * persisting the drawing logs.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class ApplicationStimulusDrawEventPersistenceHandler implements StimulusDrawEventHandler {

  private final DrawingService drawingService;

  @Override
  @EventListener
  public void handle(StimulusDrawEvent event) {
    log.debug("Handling stimulus draw event: [{}].", event);

    drawingService.saveStimulusDrawingAction(
        event.roomId(), event.participantId(), event.stimulusId(),
        DRAW, event.offset(), event.content());
  }

  @Override
  @EventListener
  public void handle(StimulusDrawUndoEvent event) {
    log.debug("Handling stimulus draw undo event: [{}].", event);

    drawingService.saveStimulusDrawingAction(
        event.roomId(), event.participantId(), event.stimulusId(),
        UNDO, event.offset(), event.content());
  }

  @Override
  @EventListener
  public void handle(StimulusDrawRedoEvent event) {
    log.debug("Handling stimulus draw redo event: [{}].", event);

    drawingService.saveStimulusDrawingAction(
        event.roomId(), event.participantId(), event.stimulusId(),
        REDO, event.offset(), event.content());
  }

  @Override
  @EventListener
  public void handle(StimulusDrawClearEvent event) {
    log.debug("Handling stimulus draw clear event: [{}].", event);

    drawingService.saveStimulusDrawingAction(
        event.roomId(), event.participantId(), event.stimulusId(),
        CLEAR, event.offset(), null);
  }

}
