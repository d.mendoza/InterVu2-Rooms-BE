package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.model.entity.Project;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for the {@link Project} entity.
 *
 * @author Branko Ostojic
 */
public interface ProjectRepository extends JpaRepository<Project, UUID> {

  /**
   * Finds project by it's platform ID.
   *
   * @param platformId Project platform ID.
   * @return Matching project or empty {@link Optional}.
   */
  Optional<Project> findByPlatformId(String platformId);
}
