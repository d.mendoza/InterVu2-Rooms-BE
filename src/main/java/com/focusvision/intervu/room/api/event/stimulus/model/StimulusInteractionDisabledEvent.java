package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus interaction disabled event.
 */
public record StimulusInteractionDisabledEvent(UUID roomId) {

}
