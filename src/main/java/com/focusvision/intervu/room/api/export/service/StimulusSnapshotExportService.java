package com.focusvision.intervu.room.api.export.service;

import com.focusvision.intervu.room.api.export.model.StimulusSnapshotExportDto;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.stimulus.snapshot.StimulusSnapshot;
import com.focusvision.intervu.room.api.stimulus.snapshot.StimulusSnapshotRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

/**
 * Service for stimulus snapshot export operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusSnapshotExportService {

  private final ResearchSessionService researchSessionService;
  private final StimulusSnapshotRepository stimulusSnapshotRepository;

  /**
   * Gets all room stimuli snapshots for export.
   *
   * @param roomPlatformId Room platform ID
   * @return All room stimulus snapshots.
   */
  public Optional<List<StimulusSnapshotExportDto>> export(String roomPlatformId) {
    log.debug("Exporting stimulus snapshots (roomPlatformId: {})", roomPlatformId);

    return researchSessionService.fetch(roomPlatformId)
        .map(this::getSnapshots);
  }

  private List<StimulusSnapshotExportDto> getSnapshots(ResearchSession room) {
    return stimulusSnapshotRepository.findAllByResearchSession(room).stream()
        .map(this::map)
        .toList();
  }

  private StimulusSnapshotExportDto map(StimulusSnapshot stimulusSnapshot) {
    return new StimulusSnapshotExportDto(
        stimulusSnapshot.getStimulus().getId(),
        stimulusSnapshot.getName(),
        stimulusSnapshot.getMimeType(),
        Base64.encodeBase64String(stimulusSnapshot.getData()),
        stimulusSnapshot.getStartOffset()
    );
  }

}
