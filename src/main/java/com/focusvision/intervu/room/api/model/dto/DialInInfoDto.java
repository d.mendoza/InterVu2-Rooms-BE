package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing telephony info.
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString
@ApiModel("Dial-in Info")
public class DialInInfoDto {

  @ApiModelProperty("Dial-in numbers")
  @ToString.Exclude
  private List<String> dialInNumbers;
  @ApiModelProperty("Operator PIN")
  private String operatorPin;
  @ApiModelProperty("Listener PIN")
  private String listenerPin;
  @ApiModelProperty("Speaker PIN")
  private String speakerPin;

}
