package com.focusvision.intervu.room.api.configuration;

import static java.util.List.of;
import static java.util.Objects.requireNonNull;

import com.focusvision.intervu.room.api.cache.authentication.service.AuthenticationCachingService;
import com.focusvision.intervu.room.api.cache.room.log.service.RoomLogCachingService;
import com.focusvision.intervu.room.api.configuration.domain.CachingProperties;
import java.util.Collection;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Simple caching configuration class.
 *
 * @author Branko Ostojic
 */
@Configuration
@EnableCaching
@RequiredArgsConstructor
public class CacheConfiguration {

  private final CachingProperties cachingProperties;

  /**
   * Provides CacheManager configuration.
   *
   * @param properties Caching properties.
   * @return CacheManager configuration.
   */
  @Bean
  public CacheManager simpleCacheManager(CachingProperties properties) {
    SimpleCacheManager cacheManager = new SimpleCacheManager();
    cacheManager.setCaches(of(
        new ConcurrentMapCache(properties.getAuthentication().getName()),
        new ConcurrentMapCache(properties.getRoomLog().getName())
    ));
    return cacheManager;
  }

  @Bean
  public CacheResolver cacheResolver(CacheManager simpleCacheManager,
                                     CachingProperties properties) {
    return new SimpleCacheResolver(properties, simpleCacheManager);
  }

  public boolean isAuthenticationCachingEnabled() {
    return cachingProperties.getAuthentication().isEnabled();
  }

  public boolean isRoomLogCachingEnabled() {
    return cachingProperties.getRoomLog().isEnabled();
  }

  private static class SimpleCacheResolver implements CacheResolver {
    private final List<Cache> authenticationCache;
    private final List<Cache> roomLogCache;

    public SimpleCacheResolver(CachingProperties properties,
                               CacheManager simpleCacheManager) {
      this.authenticationCache = of(
          requireNonNull(simpleCacheManager.getCache(properties.getAuthentication().getName())));
      this.roomLogCache = of(
          requireNonNull(simpleCacheManager.getCache(properties.getRoomLog().getName())));
    }

    @Override
    public Collection<? extends Cache> resolveCaches(CacheOperationInvocationContext<?> context) {
      if (context.getTarget() instanceof AuthenticationCachingService) {
        return authenticationCache;
      } else if (context.getTarget() instanceof RoomLogCachingService) {
        return roomLogCache;
      }

      return of();
    }
  }
}
