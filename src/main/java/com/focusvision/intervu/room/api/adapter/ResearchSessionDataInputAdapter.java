package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult;

/**
 * Adapter for processing the research session data.
 *
 * @author Branko Ostojic
 */
public interface ResearchSessionDataInputAdapter {

  /**
   * Processes the research session data. It should result in creating a new research session in the
   * system or updating the existing one if all conditions are met.
   *
   * @param researchSessionData Research session data.
   * @return Processing result.
   */
  ResearchSessionProcessResult process(ResearchSessionData researchSessionData);
}
