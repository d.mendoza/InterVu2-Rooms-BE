package com.focusvision.intervu.room.api.event.participant.model;

import java.util.List;
import java.util.UUID;

/**
 * Model for participant drawing disabled event.
 */
public record ParticipantDrawingDisabledEvent(UUID roomId,
                                              List<UUID> participants) {

}
