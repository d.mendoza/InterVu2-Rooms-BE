package com.focusvision.intervu.room.api.state.stimulus.service;

import com.focusvision.intervu.room.api.common.model.StimulusActionType;
import com.focusvision.intervu.room.api.model.dto.StimulusGlobalActionDto;
import com.focusvision.intervu.room.api.model.event.StimulusGlobalActionDataEvent;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.state.RoomStateOperator;
import com.focusvision.intervu.room.api.state.model.StimulusState;
import com.focusvision.intervu.room.api.state.stimulus.adapter.StimulusActionNotificationSenderAdapter;
import com.focusvision.intervu.room.api.state.stimulus.adapter.StimulusGlobalActionNotificationSenderAdapter;
import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionNotificationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation for {@link StimulusActionNotificationService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusActionNotificationServiceImpl implements StimulusActionNotificationService {

  private final StimulusActionNotificationSenderAdapter senderAdapter;
  private final StimulusGlobalActionNotificationSenderAdapter globalSenderAdapter;
  private final CommunicationChannelService communicationChannelService;
  private final RoomStateOperator roomStateOperator;

  @Override
  public void sendPollStimulusVoteNotification(PollStimulusVoteEvent event) {
    log.debug("Sending poll stimulus vote notification [{}]", event);

    var destination = communicationChannelService.getStimulusChannel(event.getRoomId());

    senderAdapter.send(destination, StimulusActionNotificationMessage
        .builder()
        .roomId(event.getRoomId())
        .stimulusId(event.getStimulusId())
        .participantId(event.getParticipantId())
        .type(StimulusActionType.POLL_VOTED)
        .content(event.getContent())
        .build());

    roomStateOperator.getCurrentRoomState(event.getRoomId())
        .getActiveStimulus()
        .filter(StimulusState::isBroadcastResults)
        .ifPresent(stimulusState ->
            globalSenderAdapter.send(StimulusGlobalActionDataEvent.builder()
                .destination(
                    communicationChannelService.getStimulusGlobalChannel(event.getRoomId()))
                .data(new StimulusGlobalActionDto()
                    .setRoomId(event.getRoomId())
                    .setStimulusId(event.getStimulusId())
                    .setType(StimulusActionType.POLL_VOTED)
                    .setContent(event.getContent()))
                .build()));
  }

  @Override
  public void sendPollStimulusRankingNotification(PollStimulusRankingEvent event) {
    log.debug("Sending poll stimulus ranking notification [{}]", event);

    var destination = communicationChannelService.getStimulusChannel(event.getRoomId());

    senderAdapter.send(destination, StimulusActionNotificationMessage
        .builder()
        .roomId(event.getRoomId())
        .stimulusId(event.getStimulusId())
        .participantId(event.getParticipantId())
        .type(StimulusActionType.POLL_RANKED)
        .content(event.getContent())
        .build());

    roomStateOperator.getCurrentRoomState(event.getRoomId())
        .getActiveStimulus()
        .filter(StimulusState::isBroadcastResults)
        .ifPresent(stimulusState ->
            globalSenderAdapter.send(StimulusGlobalActionDataEvent.builder()
                .destination(
                    communicationChannelService.getStimulusGlobalChannel(event.getRoomId()))
                .data(new StimulusGlobalActionDto()
                    .setRoomId(event.getRoomId())
                    .setStimulusId(event.getStimulusId())
                    .setType(StimulusActionType.POLL_RANKED)
                    .setContent(event.getContent()))
                .build()));
  }

  @Override
  public void sendPollStimulusTextAnswerNotification(PollStimulusAnswerEvent event) {
    log.debug("Sending poll stimulus text answer notification [{}]", event);

    var destination = communicationChannelService.getStimulusChannel(event.getRoomId());

    senderAdapter.send(destination, StimulusActionNotificationMessage
        .builder()
        .roomId(event.getRoomId())
        .stimulusId(event.getStimulusId())
        .participantId(event.getParticipantId())
        .type(StimulusActionType.POLL_ANSWERED)
        .content(event.getContent())
        .build());

    roomStateOperator.getCurrentRoomState(event.getRoomId())
        .getActiveStimulus()
        .filter(StimulusState::isBroadcastResults)
        .ifPresent(stimulusState ->
            globalSenderAdapter.send(StimulusGlobalActionDataEvent.builder()
                .destination(
                    communicationChannelService.getStimulusGlobalChannel(event.getRoomId()))
                .data(new StimulusGlobalActionDto()
                    .setRoomId(event.getRoomId())
                    .setStimulusId(event.getStimulusId())
                    .setType(StimulusActionType.POLL_RANKED)
                    .setContent(event.getContent()))
                .build()));
  }

  @Override
  public void sendDocumentStimulusPageChangeNotification(DocumentStimulusPageChangeEvent event) {
    log.debug("Sending document stimulus page change notification [{}]", event);

    var destination = communicationChannelService.getStimulusChannel(event.getRoomId());

    roomStateOperator.getCurrentRoomState(event.getRoomId()).getActiveStimulus()
        .filter(StimulusState::isSyncEnabled)
        .ifPresent(stimulusState -> {
          senderAdapter.send(destination, StimulusActionNotificationMessage
              .builder()
              .roomId(event.getRoomId())
              .stimulusId(event.getStimulusId())
              .participantId(event.getParticipantId())
              .type(StimulusActionType.DOCUMENT_PAGE_CHANGED)
              .content(event.getContent())
              .build());

          globalSenderAdapter.send(StimulusGlobalActionDataEvent.builder()
              .destination(communicationChannelService.getStimulusGlobalChannel(event.getRoomId()))
              .data(new StimulusGlobalActionDto()
                  .setRoomId(event.getRoomId())
                  .setStimulusId(event.getStimulusId())
                  .setType(StimulusActionType.DOCUMENT_PAGE_CHANGED)
                  .setContent(event.getContent()))
              .build());
        });
  }
}
