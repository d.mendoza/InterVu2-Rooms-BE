package com.focusvision.intervu.room.api.exception;

/**
 * Exception class used for streaming api errors.
 */
public class StreamingApiException extends RuntimeException {

  public StreamingApiException(String message) {
    super(message);
  }
}
