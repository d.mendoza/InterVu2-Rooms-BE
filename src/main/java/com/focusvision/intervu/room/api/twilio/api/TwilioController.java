package com.focusvision.intervu.room.api.twilio.api;

import com.focusvision.intervu.room.api.twilio.model.DialInPin;
import com.focusvision.intervu.room.api.twilio.service.TwilioDialInService;
import com.focusvision.intervu.room.api.twilio.service.TwilioStatusService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/** Controller responsible for Twilio related callbacks. */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "twilio")
public class TwilioController {

  private final TwilioDialInService twilioDialInService;
  private final TwilioStatusService twilioStatusService;

  /**
   * Handles dial in (phone) request.
   *
   * @param twilioDialInRequest Dial in request data.
   * @return Dial in response (answer).
   */
  @PostMapping(value = "dial-in", produces = MediaType.TEXT_HTML_VALUE)
  public String dialIn(TwilioDialInRequest twilioDialInRequest) {
    log.info("Dial-in request from Twilio (request: {})", twilioDialInRequest);

    return twilioDialInService.processDialIn(twilioDialInRequest);
  }

  /**
   * Handles dial in (phone) PIN verification.
   *
   * @param twilioDialInRequest Dial in request data.
   * @return Dial in response (answer).
   */
  @PostMapping(value = "dial-in/pin", produces = MediaType.TEXT_HTML_VALUE)
  public String pin(TwilioDialInRequest twilioDialInRequest) {
    log.info("Dial-in PIN request from Twilio (request: {})", twilioDialInRequest);

    var dialInPin = new DialInPin(twilioDialInRequest.getDigits());
    return twilioDialInService.processDialInPin(dialInPin);
  }

  /**
   * Handles Twilio status update request.
   *
   * @param request Twilio status update data.
   */
  @PostMapping(
      value = "status",
      consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
  public void status(@RequestParam MultiValueMap<String, String> request) {
    log.info("Handling status callback from Twilio (request: {})", request);

    twilioStatusService.processStatus(request.toSingleValueMap());
  }
}
