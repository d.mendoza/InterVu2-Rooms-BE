package com.focusvision.intervu.room.api.state;

import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.DRAWING_SYNC_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.DRAWING_SYNC_ENABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_BANNED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_DRAWING_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_DRAWING_ENABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_OFFLINE;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_ONLINE;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.PARTICIPANT_READY;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.RESPONDENTS_CHAT_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.RESPONDENTS_CHAT_ENABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CANCELED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_CREATED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_EXPIRED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_FINISHED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_STARTED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.ROOM_UPDATED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_ACTIVATED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_BROADCAST_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_BROADCAST_ENABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_DEACTIVATED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_FOCUS_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_FOCUS_ENABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_INTERACTION_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_INTERACTION_ENABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_RESET;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_SYNC_DISABLED;
import static com.focusvision.intervu.room.api.common.model.RoomStateChangeType.STIMULUS_SYNC_ENABLED;
import static java.util.Comparator.comparing;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.event.room.publisher.RoomStateChangedEventPublisher;
import com.focusvision.intervu.room.api.model.dto.ConferenceDto;
import com.focusvision.intervu.room.api.model.entity.Participant;
import com.focusvision.intervu.room.api.model.entity.Project;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.state.model.ParticipantState;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import com.focusvision.intervu.room.api.state.model.StimulusState;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusStateService;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Implementation of a {@link RoomStateOperator}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
//TODO: Add room state lock here for all methods
public class RoomStateOperatorImpl implements RoomStateOperator {

  private final RoomStateProvider roomStateProvider;
  private final CommunicationChannelService communicationChannelService;
  private final StimulusStateService stimulusStateService;
  private final RoomStateChangedEventPublisher roomStateChangedEventPublisher;
  private final RoomOffsetProvider roomOffsetProvider;

  @Override
  public RoomState create(UUID roomId,
                          Project project,
                          ResearchSession researchSession,
                          Set<Participant> participants) {
    log.info("Creating new room state (roomId: {})", roomId);

    var stimulusStates = researchSession.getStimuli().stream()
        .map(this::constructStimulusState)
        .sorted(comparing(StimulusState::getId))
        .toList();

    var roomChannel = communicationChannelService.getRoomChannel(roomId);
    var roomState = RoomState.builder()
        .roomId(roomId)
        .roomChannel(roomChannel)
        .roomName(researchSession.getName())
        .projectName(project.getName())
        .state(researchSession.getState())
        .startsAt(researchSession.getStartsAt())
        .endsAt(researchSession.getEndsAt())
        .useWebcams(researchSession.isUseWebcams())
        .projectNumber(project.getProjectNumber())
        .privacy(researchSession.isPrivacy())
        .participants(List.of())
        .stimuli(stimulusStates)
        .drawingInSync(false)
        .stimulusInFocus(false)
        .respondentsChatEnabled(false)
        .projectType(project.getType())
        .audioChannel(researchSession.getAudioChannel())
        .build();

    return this.saveAndNotify(roomState, ROOM_CREATED);
  }

  @Override
  public Optional<RoomState> update(UUID roomId,
                                    Project project,
                                    ResearchSession researchSession,
                                    Set<Participant> participants,
                                    List<Stimulus> stimuli) {
    log.info("Updating room state (roomId: {})", roomId);

    RoomState roomState =
        roomStateProvider.getCurrentForRoom(researchSession.getId()).getRoomState();

    participants.forEach(participant -> this.findParticipantState(roomState, participant.getId())
        .ifPresent(
            participantState -> this.updateParticipantState(participantState, participant)));

    var stimuliStates = stimuli.stream()
        .map(stimulus -> this.findStimulusState(roomState, stimulus.getId())
            .map(stimulusState -> this.updateStimulusState(stimulusState, stimulus))
            .orElse(this.constructStimulusState(stimulus)))
        .sorted(comparing(StimulusState::getId))
        .toList();

    roomState
        .setRoomName(researchSession.getName())
        .setProjectName(project.getName())
        .setStartsAt(researchSession.getStartsAt())
        .setEndsAt(researchSession.getEndsAt())
        .setUseWebcams(researchSession.isUseWebcams())
        .setProjectNumber(project.getProjectNumber())
        .setPrivacy(researchSession.isPrivacy())
        .setStimuli(stimuliStates)
        .setProjectType(project.getType())
        .setAudioChannel(researchSession.getAudioChannel());

    return of(this.saveAndNotify(roomState, ROOM_UPDATED));
  }

  @Override
  public RoomState getCurrentRoomState(UUID roomId) {
    log.info("Getting room state (roomId: {})", roomId);

    return roomStateProvider.getCurrentForRoom(roomId).getRoomState();
  }

  @Override
  public Optional<ParticipantState> getCurrentParticipantState(UUID roomId, UUID participantId) {
    log.info("Finding participant state (roomId: {}, participantId: {})", roomId, participantId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .flatMap(roomState -> this.findParticipantState(roomState, participantId));
  }

  @Override
  public Optional<RoomState> markParticipantAsPresentOverWeb(UUID roomId,
                                                             Participant participant) {
    log.debug("Marking participant as present over web (roomId: {}, participantId: {})",
        roomId, participant.getId());

    var roomState = roomStateProvider.getCurrentForRoom(roomId).getRoomState();
    var participantState = findParticipantState(roomState, participant.getId());

    boolean participantStateChanged;
    if (participantState.isPresent()) {
      participantStateChanged = participantState
          .filter(not(ParticipantState::isOnline)
              .or(not(ParticipantState::isPresentOverWeb)))
          .map(ParticipantState::markAsPresentOverWeb)
          .isPresent();
    } else {
      long offset = roomOffsetProvider.calculateOffset(roomState.getRoomId());
      var initialParticipantState = this.constructParticipantState(roomId, participant)
          .setFirstEntryOffset(offset)
          .markAsPresentOverWeb();
      roomState.getParticipants().add(initialParticipantState);
      participantStateChanged = true;
    }

    return participantStateChanged
        ? of(this.saveAndNotify(roomState, PARTICIPANT_ONLINE))
        : empty();
  }

  @Override
  public Optional<RoomState> markParticipantAsPresentOverPhone(UUID roomId,
                                                               Participant participant) {
    log.debug("Marking participant as present over phone (roomId: {}, participantId: {})",
        roomId, participant.getId());

    var roomState = roomStateProvider.getCurrentForRoom(roomId).getRoomState();
    var participantState = findParticipantState(roomState, participant.getId());

    boolean participantStateChanged;
    if (participantState.isPresent()) {
      participantStateChanged = participantState
          .filter(not(ParticipantState::isOnline)
              .or(not(ParticipantState::isPresentOverPhone)))
          .map(ParticipantState::markAsPresentOverPhone)
          .isPresent();
    } else {
      var initialParticipantState = this.constructParticipantState(roomId, participant)
          .markAsPresentOverPhone();
      roomState.getParticipants().add(initialParticipantState);
      participantStateChanged = true;
    }

    return participantStateChanged
        ? of(this.saveAndNotify(roomState, PARTICIPANT_ONLINE))
        : empty();
  }

  @Override
  public Optional<RoomState> markParticipantAsNotPresentOverWeb(UUID roomId, UUID participantId) {
    log.info("Marking participant as offline (roomId: {}, participantId: {})",
        roomId, participantId);

    RoomState roomState = roomStateProvider.getCurrentForRoom(roomId).getRoomState();
    boolean participantStateChanged = findParticipantState(roomState, participantId)
        .filter(ParticipantState::isPresentOverWeb)
        .map(ParticipantState::markAsNotPresentOverWeb)
        .filter(not(ParticipantState::isOnline))
        .isPresent();

    return participantStateChanged
        ? of(this.saveAndNotify(roomState, PARTICIPANT_OFFLINE))
        : empty();
  }

  @Override
  public Optional<RoomState> markParticipantAsNotPresentOverPhone(UUID roomId,
                                                                  UUID participantId) {
    log.info("Marking participant as not present over phone (roomId: {}, participantId: {})",
        roomId, participantId);

    RoomState roomState = roomStateProvider.getCurrentForRoom(roomId).getRoomState();
    boolean participantStateChanged = findParticipantState(roomState, participantId)
        .filter(ParticipantState::isPresentOverPhone)
        .map(ParticipantState::markAsNotPresentOverPhone)
        .filter(not(ParticipantState::isOnline))
        .isPresent();

    return participantStateChanged
        ? of(this.saveAndNotify(roomState, PARTICIPANT_OFFLINE))
        : empty();
  }

  @Override
  public Optional<RoomState> markParticipantAsBanned(UUID roomId, UUID participantId) {
    log.info("Marking participant as banned (roomId: {}, participantId: {})",
        roomId, participantId);

    RoomState roomState = roomStateProvider.getCurrentForRoom(roomId).getRoomState();
    boolean participantStateChanged = findParticipantState(roomState, participantId)
        .filter(not(ParticipantState::isBanned))
        .map(ParticipantState::markAsBanned)
        .isPresent();

    return participantStateChanged
        ? of(this.saveAndNotify(roomState, PARTICIPANT_BANNED))
        : empty();
  }

  @Override
  public Optional<RoomState> markParticipantAsReady(UUID roomId, UUID participantId) {
    log.info("Marking participant as ready (roomId: {}, participantId: {})",
        roomId, participantId);

    RoomState roomState = roomStateProvider.getCurrentForRoom(roomId).getRoomState();
    boolean participantStateChanged = findParticipantState(roomState, participantId)
        .filter(not(ParticipantState::isReady))
        .map(ParticipantState::markAsReady)
        .isPresent();

    return participantStateChanged
        ? of(this.saveAndNotify(roomState, PARTICIPANT_READY))
        : empty();
  }

  @Override
  public Optional<RoomState> markRoomAsStarted(UUID roomId,
                                               Instant startedAt,
                                               Set<Participant> participants) {
    log.info("Marking room as started. (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(RoomState::isPending)
        .map(roomState -> roomState.markAsStarted(startedAt))
        .map(roomState -> {
          var participantStates = participants.stream()
              .flatMap(participant -> this.findParticipantState(roomState, participant.getId())
                  .map(participantState -> this.updateParticipantState(participantState,
                      participant))
                  .stream())
              .sorted(comparing(ParticipantState::getId))
              .toList();
          roomState.setParticipants(participantStates);
          return roomState;
        })
        .map(roomState -> this.saveAndNotify(roomState, ROOM_STARTED));
  }

  @Override
  public Optional<RoomState> markRoomAsFinished(UUID roomId, Instant finishedAt) {
    log.info("Marking room as finished (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(RoomState::isRunning)
        .map(roomState -> roomState.markAsFinished(finishedAt))
        .map(roomState -> this.saveAndNotify(roomState, ROOM_FINISHED));
  }

  @Override
  public Optional<RoomState> markRoomAsCanceled(UUID roomId) {
    log.info("Marking room as canceled (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(RoomState::isPending)
        .map(RoomState::markAsCanceled)
        .map(roomState -> this.saveAndNotify(roomState, ROOM_CANCELED));
  }

  @Override
  public Optional<RoomState> markRoomAsExpired(UUID roomId) {
    log.info("Marking room as expired (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(RoomState::isPending)
        .map(RoomState::markAsExpired)
        .map(roomState -> this.saveAndNotify(roomState, ROOM_EXPIRED));
  }

  @Override
  public Optional<RoomState> activateStimulus(UUID roomId, Stimulus stimulus) {
    log.info("Activating stimulus (roomId: {}, stimulusId: {})", roomId, stimulus.getId());

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .map(roomState -> this.activateStimulus(roomState, stimulus))
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_ACTIVATED));
  }

  private RoomState activateStimulus(RoomState roomState, Stimulus stimulus) {
    roomState.getStimuli()
        .forEach(stimulusState -> stimulusState.setActive(
            stimulus.getId().equals(stimulusState.getId())));
    return roomState;
  }

  @Override
  public Optional<RoomState> deactivateStimulus(UUID roomId) {
    log.info("Deactivating stimulus (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .map(this::deactivateStimulus)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_DEACTIVATED));
  }

  private RoomState deactivateStimulus(RoomState roomState) {
    roomState.getStimuli()
        .forEach(stimulusState -> stimulusState.setActive(false));
    return roomState;
  }

  @Override
  public Optional<RoomState> enableStimulusInteraction(UUID roomId) {
    log.info("Enabling stimulus interaction (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .flatMap(this::enableStimulusInteraction)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_INTERACTION_ENABLED));
  }

  private Optional<RoomState> enableStimulusInteraction(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(activeStimulusState -> {
          activeStimulusState.setInteractionEnabled(true);
          return roomState;
        });
  }

  @Override
  public Optional<RoomState> disableStimulusInteraction(UUID roomId) {
    log.info("Disabling stimulus interaction (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .flatMap(this::disableStimulusInteraction)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_INTERACTION_DISABLED));
  }

  private Optional<RoomState> disableStimulusInteraction(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(activeStimulusState -> {
          activeStimulusState.setInteractionEnabled(false);
          return roomState;
        });
  }

  @Override
  public Optional<RoomState> enableStimulusFocus(UUID roomId) {
    log.info("Enabling stimulus focus (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(not(RoomState::isStimulusInFocus))
        .map(RoomState::enableStimulusFocus)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_FOCUS_ENABLED));
  }

  @Override
  public Optional<RoomState> disableStimulusFocus(UUID roomId) {
    log.info("Disabling stimulus focus (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(RoomState::isStimulusInFocus)
        .map(RoomState::disableStimulusFocus)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_FOCUS_DISABLED));
  }

  @Override
  public Optional<RoomState> enableDrawingSync(UUID roomId) {
    log.info("Enabling drawing sync (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(not(RoomState::isDrawingInSync))
        .map(RoomState::enableDrawingSync)
        .map(roomState -> this.saveAndNotify(roomState, DRAWING_SYNC_ENABLED));
  }

  @Override
  public Optional<RoomState> disableDrawingSync(UUID roomId) {
    log.info("Disable drawing sync (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(RoomState::isDrawingInSync)
        .map(RoomState::disableDrawingSync)
        .map(roomState -> this.saveAndNotify(roomState, DRAWING_SYNC_DISABLED));
  }

  @Override
  public List<ParticipantState> enableParticipantDrawing(UUID roomId,
                                                         List<UUID> participants) {
    log.info("Enabling drawing for participants (roomId: {}, participants: {})",
        roomId, participants);

    var roomState = getCurrentRoomState(roomId);
    var updatedParticipantStateList = participants.stream()
        .map(participantId -> findParticipantState(roomState, participantId)
            .filter(not(ParticipantState::isDrawingEnabled))
            .map(participantState -> participantState.setDrawingEnabled(true)))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .toList();

    if (!updatedParticipantStateList.isEmpty()) {
      this.saveAndNotify(roomState, PARTICIPANT_DRAWING_ENABLED);
    }

    return updatedParticipantStateList;
  }

  @Override
  public List<ParticipantState> disableParticipantDrawing(UUID roomId,
                                                          List<UUID> participants) {
    log.info("Disabling drawing for participants (roomId: {}, participants: {})",
        roomId, participants);

    var roomState = getCurrentRoomState(roomId);
    var updatedParticipantStateList = participants.stream()
        .map(participantId -> findParticipantState(roomState, participantId)
            .filter(ParticipantState::isDrawingEnabled)
            .map(participantState -> participantState.setDrawingEnabled(false)))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .toList();

    if (!updatedParticipantStateList.isEmpty()) {
      this.saveAndNotify(roomState, PARTICIPANT_DRAWING_DISABLED);
    }

    return updatedParticipantStateList;
  }

  @Override
  public Optional<RoomState> enableRespondentsChat(UUID roomId) {
    log.info("Enabling respondents chat (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(not(RoomState::isRespondentsChatEnabled))
        .map(RoomState::enableRespondentsChat)
        .map(roomState -> this.saveAndNotify(roomState, RESPONDENTS_CHAT_ENABLED));
  }

  @Override
  public Optional<RoomState> disableRespondentsChat(UUID roomId) {
    log.info("Disabling respondents chat (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(RoomState::isRespondentsChatEnabled)
        .map(RoomState::disableRespondentsChat)
        .map(roomState -> this.saveAndNotify(roomState, RESPONDENTS_CHAT_DISABLED));
  }

  @Override
  public Optional<RoomState> enableStimulusBroadcastResults(UUID roomId) {
    log.info("Enabling stimulus broadcast results (roomId: {})", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(not(this::isBroadcastResultsEnabled))
        .flatMap(this::enableStimulusBroadcastResults)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_BROADCAST_ENABLED));
  }

  private Optional<RoomState> enableStimulusBroadcastResults(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(activeStimulusState -> {
          activeStimulusState.setBroadcastResults(true);
          return roomState;
        });
  }

  @Override
  public Optional<RoomState> disableStimulusBroadcastResults(UUID roomId) {
    log.info("Disabling stimulus broadcast results in room {}", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .filter(this::isBroadcastResultsEnabled)
        .flatMap(this::disableStimulusBroadcastResults)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_BROADCAST_DISABLED));
  }

  private Optional<RoomState> disableStimulusBroadcastResults(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(activeStimulusState -> {
          activeStimulusState.setBroadcastResults(false);
          return roomState;
        });
  }

  private boolean isBroadcastResultsEnabled(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(StimulusState::isBroadcastResults)
        .orElse(false);
  }

  @Override
  public Optional<RoomState> resetStimulus(UUID roomId) {
    log.info("Resetting room {} stimulus.", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .flatMap(this::resetStimulus)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_RESET));
  }

  private Optional<RoomState> resetStimulus(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(activeStimulusState -> {
          stimulusStateService.resetState(activeStimulusState.getId());
          return roomState;
        });
  }

  @Override
  public Optional<RoomState> enableStimulusSync(UUID roomId) {
    log.debug("Enabling stimulus sync in room {}", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .flatMap(this::enableStimulusSync)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_SYNC_ENABLED));
  }

  private Optional<RoomState> enableStimulusSync(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(activeStimulusState -> {
          activeStimulusState.setSyncEnabled(true);
          return roomState;
        });
  }

  @Override
  public Optional<RoomState> disableStimulusSync(UUID roomId) {
    log.debug("Disabling stimulus sync in room {}", roomId);

    return roomStateProvider.findCurrentForRoom(roomId)
        .map(RoomStateLog::getRoomState)
        .flatMap(this::disableStimulusSync)
        .map(roomState -> this.saveAndNotify(roomState, STIMULUS_SYNC_DISABLED));
  }

  private Optional<RoomState> disableStimulusSync(RoomState roomState) {
    return roomState.getActiveStimulus()
        .map(activeStimulusState -> {
          activeStimulusState.setSyncEnabled(false);
          return roomState;
        });
  }

  /**
   * Saves new room state and publishes event with new state and change type.
   *
   * @param roomState  Current room state.
   * @param changeType State change type.
   * @return New room state.
   */
  private RoomState saveAndNotify(RoomState roomState, RoomStateChangeType changeType) {
    var newRoomState = roomStateProvider.save(roomState, changeType).getRoomState();
    roomStateChangedEventPublisher.publish(new RoomStateChangedEvent(changeType, newRoomState));

    return newRoomState;
  }

  private Optional<ParticipantState> findParticipantState(RoomState roomState,
                                                          UUID participantId) {
    return roomState.getParticipants().stream()
        .filter(participant -> participant.getId().equals(participantId))
        .findFirst();
  }

  @Override
  public ParticipantState constructParticipantState(UUID roomId, Participant participant) {
    var channel =
        communicationChannelService.getParticipantChannelData(participant.getId(), roomId);
    var conference = StringUtils.hasText(participant.getConferenceToken())
        ? new ConferenceDto().setToken(participant.getConferenceToken())
        : null;

    return ParticipantState.builder()
        .id(participant.getId())
        .platformId(participant.getPlatformId())
        .displayName(participant.getDisplayName())
        .online(false)
        .ready(false)
        .role(participant.getRole())
        .admin(participant.isAdmin())
        .moderator(participant.isModerator())
        .banable(participant.isBanable())
        .guest(participant.isGuest())
        .projectManager(participant.isProjectManager())
        .projectOperator(participant.isProjectOperator())
        .banned(participant.isBanned())
        .channel(channel)
        .conference(conference)
        .build();
  }

  private ParticipantState updateParticipantState(ParticipantState participantState,
                                                  Participant participant) {
    participantState
        .setDisplayName(participant.getDisplayName())
        .setRole(participant.getRole())
        .setAdmin(participant.isAdmin())
        .setModerator(participant.isModerator())
        .setBanable(participant.isBanable())
        .setGuest(participant.isGuest())
        .setProjectManager(participant.isProjectManager())
        .setProjectOperator(participant.isProjectOperator())
        .setBanned(participant.isBanned());

    ofNullable(participant.getConferenceToken())
        .filter(StringUtils::hasText)
        .map(token -> new ConferenceDto().setToken(token))
        .ifPresent(participantState::setConference);

    return participantState;
  }

  private Optional<StimulusState> findStimulusState(RoomState roomState, UUID stimulusId) {
    return roomState.getStimuli().stream()
        .filter(stimulusState -> stimulusState.getId().equals(stimulusId))
        .findFirst();
  }

  private StimulusState constructStimulusState(Stimulus stimulus) {
    return StimulusState.builder()
        .id(stimulus.getId())
        .platformId(stimulus.getPlatformId())
        .name(stimulus.getName())
        .data(stimulus.getData())
        .type(stimulus.getType())
        .syncEnabled(stimulus.isDocumentSharing())
        .broadcastResults(false)
        .interactionEnabled(false)
        .active(false)
        .build();
  }

  private StimulusState updateStimulusState(StimulusState stimulusState, Stimulus stimulus) {
    return StimulusState.builder()
        .id(stimulus.getId())
        .platformId(stimulus.getPlatformId())
        .name(stimulus.getName())
        .data(stimulus.getData())
        .type(stimulus.getType())
        .broadcastResults(stimulusState.isBroadcastResults())
        .interactionEnabled(stimulusState.isInteractionEnabled())
        .syncEnabled(stimulusState.isSyncEnabled())
        .active(stimulusState.isActive())
        .build();
  }

}
