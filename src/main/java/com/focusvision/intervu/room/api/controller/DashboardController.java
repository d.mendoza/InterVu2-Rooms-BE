package com.focusvision.intervu.room.api.controller;

import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.dto.AuthDto;
import com.focusvision.intervu.room.api.model.dto.IntervuUserDto;
import com.focusvision.intervu.room.api.model.dto.ParticipantRoomStateDto;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.security.IntervuUser;
import com.focusvision.intervu.room.api.security.TokenProvider;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.RoomStateService;
import com.focusvision.intervu.room.api.service.impl.ParticipantStateService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller for InterVu users dashboard operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "dashboard", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class DashboardController {

  private final ResearchSessionService researchSessionService;
  private final ParticipantService participantService;
  private final TokenProvider tokenProvider;
  private final CommunicationChannelService communicationChannelService;
  private final RoomStateService roomStateService;
  private final ParticipantStateService participantStateService;

  /**
   * Gets authenticated user details.
   *
   * @param caller User performing action.
   * @return User data.
   */
  @GetMapping
  @ApiOperation(
      value = "InterVu Room dashboard user data",
      notes = "Returns details for authorized user (e.g. moderators, observers, ...)")
  public IntervuUserDto dashboard(@ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Getting InterVu user {} details sessions.", caller.getPlatformId());

    return new IntervuUserDto()
        .setPlatformId(caller.getPlatformId())
        .setEmail(caller.getEmail())
        .setFirstName(caller.getFirstName())
        .setLastName(caller.getLastName())
        .setChannel(communicationChannelService.getParticipantGlobalChannel(caller));
  }

  /**
   * Get available rooms data.
   *
   * @param caller User performing action.
   * @return Rooms data.
   */
  @GetMapping("rooms")
  @ApiOperation(
      value = "InterVu Room dashboard data",
      notes = "Returns today's upcoming sessions details for "
          + "authorized users (e.g. moderators, observers, ...)")
  public List<ParticipantRoomStateDto> rooms(
      @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Getting InterVu user {} upcoming sessions.", caller.getPlatformId());

    return researchSessionService.getTodayUpcomingSessions(caller.getPlatformId())
        .stream()
        .map(rs -> this.map(rs, caller.getPlatformId()))
        .toList();
  }

  /**
   * Authenticate for specific room.
   *
   * @param roomId Room ID.
   * @param caller User performing action.
   * @return Authentication details.
   */
  @Deprecated
  @GetMapping("rooms/{roomId}")
  @ApiOperation(
      value = "Generate authentication token for specified room",
      notes = "Returns generated JWT token if user has access to specified room")
  public AuthDto authenticate(@PathVariable String roomId,
                              @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Authenticating user {} for room {}", caller.getPlatformId(), roomId);

    return participantService.findRoomParticipant(fromString(roomId), caller.getPlatformId())
        .map(participant -> tokenProvider.createToken(participant.getId().toString()))
        .map(jwtToken -> new AuthDto().setToken(jwtToken))
        .orElseThrow(() -> new IntervuRoomException("Error generating room token."));
  }

  private ParticipantRoomStateDto map(ResearchSession rs, String platformId) {
    var roomId = rs.getId();
    var participantRoomStateDto = new ParticipantRoomStateDto()
        .setRoomState(roomStateService.getRoomState(roomId));
    rs.getParticipants().stream()
        .filter(p -> p.getPlatformId().equals(platformId))
        .findFirst()
        .map(
            participant -> participantStateService.getOrCreateParticipantState(participant.getId(),
                roomId))
        .ifPresent(participantRoomStateDto::setParticipantState);

    return participantRoomStateDto;
  }

}
