package com.focusvision.intervu.room.api.state.stimulus.publisher;

import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * Publisher for the stimulus action events.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusActionEventPublisher implements
    PollStimulusVoteEventPublisher, DocumentStimulusPageChangeEventPublisher {

  private final ApplicationEventPublisher publisher;

  @Override
  public void publish(PollStimulusVoteEvent event) {
    log.debug("Publishing poll vote event: [{}].", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(PollStimulusRankingEvent event) {
    log.debug("Publishing poll ranking event: [{}].", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(PollStimulusAnswerEvent event) {
    log.debug("Publishing poll text answer event: [{}].", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(DocumentStimulusPageChangeEvent event) {
    log.debug("Publishing document stimulus page change event [{}]", event);

    publisher.publishEvent(event);
  }
}
