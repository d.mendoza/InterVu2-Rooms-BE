package com.focusvision.intervu.room.api.state.stimulus.adapter;

import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionNotificationMessage;

/**
 * Adapter for sending stimulus action notifications.
 */
public interface StimulusActionNotificationSenderAdapter {

  /**
   * Sends the provided stimulus action notification to specified destination.
   *
   * @param destination Message destination.
   * @param message     Stimulus action notification message to be sent.
   */
  void send(String destination, StimulusActionNotificationMessage message);
}
