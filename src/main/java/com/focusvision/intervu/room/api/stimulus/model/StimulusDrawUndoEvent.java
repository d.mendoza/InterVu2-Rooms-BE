package com.focusvision.intervu.room.api.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus draw undo event.
 *
 * @author Branko Ostojic
 */
public record StimulusDrawUndoEvent(UUID roomId,
                                    UUID stimulusId,
                                    UUID participantId,
                                    String content,
                                    Long offset) {

}
