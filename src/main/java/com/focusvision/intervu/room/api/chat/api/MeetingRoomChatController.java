package com.focusvision.intervu.room.api.chat.api;

import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.chat.model.ChatHistoryDto;
import com.focusvision.intervu.room.api.chat.model.ChatMetadataDto;
import com.focusvision.intervu.room.api.chat.service.ChatDataService;
import com.focusvision.intervu.room.api.chat.service.ChatMetadataService;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room chat data related operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "meeting-room/chat", produces = MediaType.APPLICATION_JSON_VALUE)
public class MeetingRoomChatController {

  private final ChatDataService chatDataService;
  private final ChatMetadataService chatMetadataService;

  /**
   * Gets meeting room chat metadata.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping("meta")
  @ApiOperation(
      value = "Gets meeting room chat metadata",
      notes = "Gets meeting room chat metadata used for creating chat UI")
  public ChatMetadataDto chatMetadata(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching room chat metadata for participant {}.", caller.getId());

    return chatMetadataService.getMeetingRoomChatMetadata(caller);
  }

  /**
   * Gets meeting room internal chat messages.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping("internal")
  @ApiOperation(
      value = "Gets meeting room internal chat messages",
      notes = "Gets meeting room internal chat messages history")
  public ChatHistoryDto internalHistory(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching meeting room internal chat history by {}", caller.getId());

    return chatDataService.getMeetingRoomInternalChatMessages(caller);
  }

  /**
   * Gets meeting room respondents chat messages.
   *
   * @param caller Authenticated participant.
   */
  @GetMapping("respondents")
  @ApiOperation(
      value = "Gets meeting room respondents chat messages",
      notes = "Gets meeting room respondents chat messages history")
  public ChatHistoryDto respondentChatMessages(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching meeting room respondents chat history.");

    return chatDataService.getMeetingRoomRespondentsChatMessages(caller);
  }

  /**
   * Gets meeting room private chat messages.
   *
   * @param handle Chat handle.
   * @param caller Authenticated participant.
   */
  @GetMapping("private/{handle}")
  @ApiOperation(
      value = "Gets meeting room private chat messages",
      notes = "Gets meeting room private chat messages history")
  public ChatHistoryDto privateChatMessages(
      @ApiParam("Chat messages handle.") @PathVariable String handle,
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Fetching meeting room private chat messages.");

    return chatDataService.getMeetingRoomPrivateChatMessages(caller, fromString(handle));
  }
}
