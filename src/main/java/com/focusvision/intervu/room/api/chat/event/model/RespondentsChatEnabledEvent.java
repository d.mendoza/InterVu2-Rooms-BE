package com.focusvision.intervu.room.api.chat.event.model;

import java.util.UUID;

/**
 * Model representing the respondents chat enabled event.
 *
 * @author Branko Ostojic
 */
public record RespondentsChatEnabledEvent(UUID roomId) {
}
