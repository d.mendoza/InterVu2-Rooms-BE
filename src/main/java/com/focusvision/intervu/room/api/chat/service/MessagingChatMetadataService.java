package com.focusvision.intervu.room.api.chat.service;

import static com.focusvision.intervu.room.api.chat.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.chat.model.ChatType.PRIVATE;
import static com.focusvision.intervu.room.api.chat.model.ChatType.RESPONDENTS;
import static java.lang.String.format;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.empty;
import static org.springframework.http.HttpMethod.PUT;

import com.focusvision.intervu.room.api.chat.model.ChatControlDto;
import com.focusvision.intervu.room.api.chat.model.ChatControlDto.ControlDto;
import com.focusvision.intervu.room.api.chat.model.ChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ChatType;
import com.focusvision.intervu.room.api.chat.model.GroupChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ParticipantChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ParticipantDto;
import com.focusvision.intervu.room.api.model.dto.RoomParticipantDto;
import com.focusvision.intervu.room.api.model.dto.RoomStateDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.RoomStateService;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Messaging specific implementation of a {@link ChatMetadataService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MessagingChatMetadataService implements ChatMetadataService {
  private static final String WAITING_ROOM_GROUP_CHAT_DEST = "/app/waiting-room/chat/%s";
  private static final String MEETING_ROOM_GROUP_CHAT_DEST = "/app/meeting-room/chat/%s";
  private static final String WAITING_ROOM_GROUP_CHAT_DEST_SEEN = "/app/waiting-room/chat/%s/seen";
  private static final String MEETING_ROOM_GROUP_CHAT_DEST_SEEN = "/app/meeting-room/chat/%s/seen";
  private static final String WAITING_ROOM_PRIVATE_CHAT_DEST = "/app/waiting-room/chat/%s/%s";
  private static final String MEETING_ROOM_PRIVATE_CHAT_DEST = "/app/meeting-room/chat/%s/%s";
  private static final String WAITING_ROOM_PRIVATE_CHAT_DEST_SEEN =
      "/app/waiting-room/chat/%s/%s/seen";
  private static final String MEETING_ROOM_PRIVATE_CHAT_DEST_SEEN =
      "/app/meeting-room/chat/%s/%s/seen";
  private static final String WAITING_ROOM_GROUP_CHAT_MESSAGES_URL = "/waiting-room/chat/%s";
  private static final String MEETING_ROOM_GROUP_CHAT_MESSAGES_URL = "/meeting-room/chat/%s";
  private static final String WAITING_ROOM_PRIVATE_CHAT_MESSAGES_URL = "/waiting-room/chat/%s/%s";
  private static final String MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL = "/meeting-room/chat/%s/%s";

  private static final ChatControlDto meetingRoomRespondentsChatControl = ChatControlDto.builder()
      .enable(
          ControlDto.builder()
              .method(PUT)
              .url("/meeting-room/chat/respondents/control/enable")
              .build())
      .disable(
          ControlDto.builder()
              .method(PUT)
              .url("/meeting-room/chat/respondents/control/disable")
              .build())
      .build();

  private final RoomStateService roomStateService;

  @Override
  public ChatMetadataDto getWaitingRoomChatMetadata(AuthenticatedParticipant caller) {
    log.debug("Getting waiting room chat metadata for participant {}.", caller.getId());

    var metadata = ChatMetadataDto.builder();
    var roomState = roomStateService.getRoomState(caller.getRoomId());

    if (caller.isInternal()) {
      metadata.internal(getWaitingRoomInternalChatMetadata(caller, roomState));
    }

    if (canReadPublicChat(caller)) {
      metadata.respondents(getWaitingRoomRespondentsChatMetadata(caller, roomState));
    }

    var direct = this.getWaitingRoomPrivateChatParticipants(caller)
        .map(this::getWaitingRoomPrivateChatMetadata)
        .toList();
    metadata.direct(direct);

    return metadata.build();
  }

  @Override
  public ChatMetadataDto getMeetingRoomChatMetadata(AuthenticatedParticipant caller) {
    log.debug("Getting meeting room chat metadata for participant {}.", caller.getId());

    var metadata = ChatMetadataDto.builder();
    var roomState = roomStateService.getRoomState(caller.getRoomId());

    if (caller.isInternal()) {
      metadata.internal(getMeetingRoomInternalChatMetadata(caller, roomState));
    }

    if (canReadPublicChat(caller)) {
      metadata.respondents(getMeetingRoomRespondentsChatMetadata(caller, roomState));
    }

    var direct = this.getMeetingRoomPrivateChatParticipants(caller, roomState)
        .map(this::getMeetingRoomPrivateChatMetadata)
        .toList();
    metadata.direct(direct);

    return metadata.build();
  }

  @Override
  public List<ParticipantChatMetadataDto> getMeetingRoomParticipantsChatMetadata(
      AuthenticatedParticipant caller) {
    log.debug("Getting meeting room participants chat metadata for room {}", caller.getRoomId());

    var roomState = roomStateService.getRoomState(caller.getRoomId());
    return this.getParticipantsChatMetadata(roomState)
        .stream()
        .map(this::getMeetingRoomPrivateChatMetadata)
        .toList();
  }

  @Override
  public List<ParticipantChatMetadataDto> getWaitingRoomParticipantsChatMetadata(
      AuthenticatedParticipant caller) {
    log.debug("Getting waiting room participants chat metadata for room {}", caller.getRoomId());

    var roomState = roomStateService.getRoomState(caller.getRoomId());
    return this.getParticipantsChatMetadata(roomState)
        .stream()
        .map(this::getWaitingRoomPrivateChatMetadata)
        .toList();
  }

  private GroupChatMetadataDto getWaitingRoomRespondentsChatMetadata(
      AuthenticatedParticipant caller,
      RoomStateDto roomState) {
    log.debug("Getting waiting room respondents chat metadata for room {}", caller.getRoomId());

    var handle = caller.getRoomId();
    var participants = getParticipantsForRespondentsChat(roomState)
        .map(this::getWaitingRoomPrivateChatMetadata)
        .toList();

    return getWaitingRoomGroupChatMetadata(RESPONDENTS, handle)
        .setParticipants(participants)
        .setEnabled(true)
        .setReadOnly(!canWriteInPublicChat(caller));
  }

  private GroupChatMetadataDto getWaitingRoomInternalChatMetadata(AuthenticatedParticipant caller,
                                                                  RoomStateDto roomState) {
    log.debug("Getting waiting room internal chat metadata for room {}", caller.getRoomId());

    var handle = caller.getRoomId();
    var participants = getParticipantsForInternalChat(caller, roomState)
        .map(this::getWaitingRoomPrivateChatMetadata)
        .toList();

    return getWaitingRoomGroupChatMetadata(INTERNAL, handle)
        .setParticipants(participants)
        .setEnabled(true)
        .setReadOnly(false);
  }

  private GroupChatMetadataDto getWaitingRoomGroupChatMetadata(ChatType chatType, UUID handle) {
    return new GroupChatMetadataDto()
        .setHandle(handle)
        .setType(chatType)
        .setDestination(format(WAITING_ROOM_GROUP_CHAT_DEST, chatType.getType()))
        .setDestinationForSeen(format(WAITING_ROOM_GROUP_CHAT_DEST_SEEN, chatType.getType()))
        .setMessagesUrl(format(WAITING_ROOM_GROUP_CHAT_MESSAGES_URL, chatType.getType()));
  }

  private GroupChatMetadataDto getMeetingRoomGroupChatMetadata(ChatType chatType, UUID handle) {
    return new GroupChatMetadataDto()
        .setHandle(handle)
        .setType(chatType)
        .setDestination(format(MEETING_ROOM_GROUP_CHAT_DEST, chatType.getType()))
        .setDestinationForSeen(format(MEETING_ROOM_GROUP_CHAT_DEST_SEEN, chatType.getType()))
        .setMessagesUrl(format(MEETING_ROOM_GROUP_CHAT_MESSAGES_URL, chatType.getType()));
  }

  private ParticipantChatMetadataDto getWaitingRoomPrivateChatMetadata(
      RoomParticipantDto participant) {
    log.debug("Getting waiting room private chat metadata for participant {}.",
        participant.getId());

    return this.getPrivateChatMetadata(
        participant,
        WAITING_ROOM_PRIVATE_CHAT_DEST,
        WAITING_ROOM_PRIVATE_CHAT_DEST_SEEN,
        WAITING_ROOM_PRIVATE_CHAT_MESSAGES_URL);
  }

  private ParticipantChatMetadataDto getMeetingRoomPrivateChatMetadata(
      RoomParticipantDto participant) {
    log.debug("Getting meeting room private chat metadata for participant {}.",
        participant.getId());

    return this.getPrivateChatMetadata(
        participant,
        MEETING_ROOM_PRIVATE_CHAT_DEST,
        MEETING_ROOM_PRIVATE_CHAT_DEST_SEEN,
        MEETING_ROOM_PRIVATE_CHAT_MESSAGES_URL);
  }

  private GroupChatMetadataDto getMeetingRoomRespondentsChatMetadata(
      AuthenticatedParticipant caller,
      RoomStateDto roomState) {
    log.debug("Getting meeting room respondents chat metadata for room {}", caller.getRoomId());

    var handle = caller.getRoomId();
    var participants = getParticipantsForRespondentsChat(roomState)
        .map(this::getMeetingRoomPrivateChatMetadata)
        .toList();


    var metadata = getMeetingRoomGroupChatMetadata(RESPONDENTS, handle)
        .setParticipants(participants)
        .setEnabled(roomState.isRespondentsChatEnabled())
        .setReadOnly(!canWriteInPublicChat(caller));

    if (canControlPublicChat(caller)) {
      metadata.setControl(meetingRoomRespondentsChatControl);
    }

    return metadata;
  }

  private GroupChatMetadataDto getMeetingRoomInternalChatMetadata(AuthenticatedParticipant caller,
                                                                  RoomStateDto roomState) {
    log.debug("Getting meeting room internal chat metadata for room {}", caller.getRoomId());

    var handle = caller.getRoomId();
    var participants = getParticipantsForInternalChat(caller, roomState)
        .map(this::getMeetingRoomPrivateChatMetadata)
        .toList();

    return getMeetingRoomGroupChatMetadata(INTERNAL, handle)
        .setParticipants(participants)
        .setEnabled(true)
        .setReadOnly(false);
  }

  private ParticipantChatMetadataDto getPrivateChatMetadata(RoomParticipantDto participant,
                                                            String destination,
                                                            String destinationForSeen,
                                                            String messagesUrl) {
    var handle = participant.getId();
    var participantDto = this.convert(participant);
    return new ParticipantChatMetadataDto()
        .setParticipant(participantDto)
        .setHandle(handle)
        .setType(PRIVATE)
        .setReadOnly(false)
        .setEnabled(true)
        .setDestination(format(destination, PRIVATE.getType(), handle))
        .setDestinationForSeen(format(destinationForSeen, PRIVATE.getType(), handle))
        .setMessagesUrl(format(messagesUrl, PRIVATE.getType(), handle));
  }

  private ParticipantDto convert(RoomParticipantDto participant) {
    return new ParticipantDto()
        .setId(participant.getId())
        .setName(participant.getDisplayName())
        .setRole(participant.getRole())
        .setOnline(participant.isOnline())
        .setAdmin(participant.isAdmin())
        .setProjectManager(participant.isProjectManager())
        .setProjectOperator(participant.isProjectOperator());
  }

  private Stream<RoomParticipantDto> getWaitingRoomPrivateChatParticipants(
      AuthenticatedParticipant caller) {
    log.debug("Waiting room private chat is not allowed.");

    return empty();
  }

  private Stream<RoomParticipantDto> getMeetingRoomPrivateChatParticipants(
      AuthenticatedParticipant caller,
      RoomStateDto roomState) {
    log.debug("Getting meeting room {} private chat participants.", caller.getRoomId());

    return getParticipantsForPrivateChat(caller, roomState);
  }

  private Stream<RoomParticipantDto> getParticipantsForPrivateChat(
      AuthenticatedParticipant caller,
      RoomStateDto roomState) {
    var participants = roomState
        .getParticipants()
        .stream()
        .filter(skipSelf(caller));

    if (caller.isModeratorRole()) {
      return participants;
    } else if (caller.isRespondent()) {
      return participants
          .filter(RoomParticipantDto::isModeratorParticipant);
    } else if (caller.isInternal()) {
      return participants
          .filter(not(RoomParticipantDto::isRespondent));
    }

    return empty();
  }

  private Stream<RoomParticipantDto> getParticipantsForInternalChat(
      AuthenticatedParticipant caller,
      RoomStateDto roomState) {
    return roomState
        .getParticipants()
        .stream()
        .filter(RoomParticipantDto::isInternal);
  }

  private Stream<RoomParticipantDto> getParticipantsForRespondentsChat(RoomStateDto roomState) {
    return roomState.getParticipants()
        .stream()
        .filter(this::canReadPublicChat);
  }

  private List<RoomParticipantDto> getParticipantsChatMetadata(RoomStateDto roomState) {
    return roomState.getParticipants();
  }

  private Predicate<RoomParticipantDto> skipSelf(AuthenticatedParticipant caller) {
    return not(participant -> participant.getId().equals(caller.getId()));
  }

  private boolean canWriteInPublicChat(AuthenticatedParticipant caller) {
    return caller.isModeratorRole()
        || caller.isRespondent()
        || caller.isAdmin()
        || caller.isProjectManager()
        || caller.isProjectOperator();
  }

  private boolean canReadPublicChat(AuthenticatedParticipant caller) {
    return caller.isModeratorRole()
        || caller.isRespondent()
        || caller.isAdmin()
        || caller.isProjectManager()
        || caller.isProjectOperator();
  }

  private boolean canReadPublicChat(RoomParticipantDto roomParticipant) {
    return roomParticipant.isModeratorParticipant()
        || roomParticipant.isRespondent()
        || roomParticipant.isAdmin()
        || roomParticipant.isProjectManager()
        || roomParticipant.isProjectOperator();
  }

  private boolean canControlPublicChat(AuthenticatedParticipant caller) {
    return caller.isModeratorRole()
        || caller.isAdmin()
        || caller.isProjectManager()
        || caller.isProjectOperator();
  }
}
