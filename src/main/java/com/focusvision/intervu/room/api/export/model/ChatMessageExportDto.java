package com.focusvision.intervu.room.api.export.model;

import com.focusvision.intervu.room.api.common.model.ChatType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.UUID;
import lombok.Value;

/**
 * DTO representing the chat message details for export.
 *
 * @author Branko Ostojic
 */
@Value
@ApiModel("Chat Message")
public class ChatMessageExportDto {

  @ApiModelProperty("Chat message content")
  String message;

  @ApiModelProperty("Chat message sender platform ID")
  String senderId;

  @ApiModelProperty("Chat message sender name")
  String senderName;

  @ApiModelProperty("Chat message handle")
  UUID handle;

  @ApiModelProperty("Chat type")
  ChatType type;

  @ApiModelProperty("Chat message sent time")
  Instant sentAt;

}
