package com.focusvision.intervu.room.api.bookmark.model;

import com.focusvision.intervu.room.api.common.model.BookmarkType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.util.UUID;
import lombok.Value;

/**
 * DTO representing the bookmark details.
 *
 * @author Branko Ostojic
 */
@Value
@ApiModel("Bookmark Details")
public class BookmarkDetailsDto {

  @ApiModelProperty("Bookmark ID")
  UUID id;
  @ApiModelProperty("Bookmark note")
  String note;
  @ApiModelProperty("Bookmark creator platform ID")
  String addedBy;
  @ApiModelProperty("Bookmark creation date")
  Instant addedAt;
  @ApiModelProperty("Bookmark offset in milliseconds from start of the session")
  Long offset;
  @ApiModelProperty("Bookmark creator display name ID")
  String addedByDisplayName;
  @ApiModelProperty("Bookmark type")
  BookmarkType type;
}
