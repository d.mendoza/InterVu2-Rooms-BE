package com.focusvision.intervu.room.api.draw.service;

import com.focusvision.intervu.room.api.common.model.DrawingActionType;
import com.focusvision.intervu.room.api.draw.model.DrawingLog;
import java.util.List;
import java.util.UUID;

/**
 * Service for handling drawing related actions.
 *
 * @author Branko Ostojic
 */
public interface DrawingService {

  /**
   * Persists the drawing action on stimulus.
   *
   * @param roomId        Room ID.
   * @param participantId Participant ID.
   * @param stimulusId    Stimulus ID.
   * @param actionType    Type of drawing action.
   * @param offset        Action offset from the room start.
   * @param content       Drawing action content.
   * @return Persisted drawing action log.
   */
  DrawingLog saveStimulusDrawingAction(UUID roomId,
                                       UUID participantId,
                                       UUID stimulusId,
                                       DrawingActionType actionType,
                                       Long offset,
                                       String content);

  /**
   * Gets the list of all drawing logs ordered by the <code>offset</code>.
   *
   * @param roomId Room ID.
   * @return List of all drawing logs.
   */
  List<DrawingLog> getRoomDrawingLogs(UUID roomId);

  /**
   * Gets the list of all drawing logs for specific context ordered by the <code>offset</code>.
   *
   * @param roomId    Room ID.
   * @param contextId Drawing context ID.
   * @return List of all drawing logs for specific context.
   */
  List<DrawingLog> getRoomDrawingLogs(UUID roomId, UUID contextId);
}
