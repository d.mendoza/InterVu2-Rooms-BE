package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.BookmarkType;
import com.focusvision.intervu.room.api.export.model.BookmarkExportDto;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.entity.Recording;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * DTO representing the Room recording details.
 */
@Getter
@Setter
@Accessors(chain = true)
public class RoomRecordingDto {

  private List<RecordingDto> recordings;

  /**
   * Recording model.
   */
  @Getter
  @Setter
  @Accessors(chain = true)
  public static class RecordingDto {

    private String recordingUrl;
    private Integer recordingDuration;
    private Recording.State state;
    private CompositionLayout layout;
    private ScreenshareBoundsDto screensharePosition;
    private AudioChannel audioChannel;
  }

  /**
   * Screenshare position DTO.
   */
  @Getter
  @Setter
  @Accessors(chain = true)
  public static class ScreenshareBoundsDto {

    private ScreenAreaDto screenshareArea;
    private List<ScreenAreaDto> excludedAreas;

  }

  /**
   * Screenshare position DTO.
   */
  @Getter
  @Setter
  @Accessors(chain = true)
  public static class ScreenAreaDto {

    private Long offsetX;
    private Long offsetY;
    private Long width;
    private Long height;
  }
}
