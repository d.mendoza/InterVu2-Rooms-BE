package com.focusvision.intervu.room.api.state;

import com.focusvision.intervu.room.api.common.model.RoomStateChangeType;
import com.focusvision.intervu.room.api.state.model.RoomState;
import com.focusvision.intervu.room.api.state.model.RoomStateLog;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Provider for room state change log data.
 *
 * @author Branko Ostojic
 */
public interface RoomStateProvider {

  /**
   * Persists the new room state and change info like offset and change type.
   *
   * @param roomState  Current room state data.
   * @param changeType Change type.
   * @return Saved room state change log.
   */
  RoomStateLog save(RoomState roomState, RoomStateChangeType changeType);

  /**
   * Gets the current (last) room state change log data.
   *
   * @param roomId Room ID.
   * @return Current room state change log data.
   */
  RoomStateLog getCurrentForRoom(UUID roomId);

  /**
   * Finds current (last) room state change log data.
   *
   * @param roomId Room ID.
   * @return Current room state change log data or empty {@link Optional}.
   */
  Optional<RoomStateLog> findCurrentForRoom(UUID roomId);

  /**
   * Gets the list of room state change logs ordered by offset.
   *
   * @param roomId Room ID.
   * @return List of room state change logs.
   */
  List<RoomStateLog> getLogsForRoom(UUID roomId);

  /**
   * Gets tha last two ROOM_FINISHED and PARTICIPANT_OFFLINE room state logs for the given room.
   *
   * @param roomId Room ID.
   * @return List of two matching room state logs.
   */
  List<RoomStateLog> getLastRoomFinishedAndParticipantOfflineLogs(UUID roomId);
}
