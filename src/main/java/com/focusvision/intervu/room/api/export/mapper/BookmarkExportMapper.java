package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.export.model.BookmarkExportDto;
import com.focusvision.intervu.room.api.model.entity.Bookmark;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link Bookmark} entity.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface BookmarkExportMapper {

  /**
   * Maps bookmark to export DTO.
   *
   * @param bookmark Bookmark to be mapped.
   * @return Bookmark DTO.
   */
  @Mapping(source = "startOffset", target = "offset")
  @Mapping(source = "participant.platformId", target = "createdBy")
  BookmarkExportDto mapToDto(Bookmark bookmark);
}
