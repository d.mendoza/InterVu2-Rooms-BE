package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.export.model.ParticipantRecordingExportDto;
import com.focusvision.intervu.room.api.model.streaming.ParticipantGroupRoomRecording;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link ParticipantRecordingExportDto} model.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface ParticipantRecordingExportMapper {

  /**
   * Maps participant recording data to export DTO.
   *
   * @param recording Participant recording to be mapped.
   * @return Resulting DTO.
   */
  @Mapping(source = "recordingType", target = "type")
  ParticipantRecordingExportDto mapToDto(ParticipantGroupRoomRecording recording);

}
