package com.focusvision.intervu.room.api.state.stimulus.service;

import com.focusvision.intervu.room.api.common.model.StimulusActionType;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionLog;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service for handling stimulus action log data.
 */
public interface StimulusActionLogService {

  /**
   * Persists the new stimulus action state and change info like offset and action type.
   *
   * @param stimulusActionState Current stimulus action state data.
   * @param actionType          Stimulus action type.
   * @param offset              Change offset.
   * @return Saved stimulus action change log.
   */
  StimulusActionLog save(StimulusActionState stimulusActionState, StimulusActionType actionType,
                         Long offset);

  /**
   * Finds last action in log on stimulus with provided id.
   *
   * @param stimulusId Stimulus ID.
   * @return Matching action in log or empty {@link Optional}.
   */
  Optional<StimulusActionLog> findLastAction(UUID stimulusId);

  /**
   * Finds all stimulus action logs for specific stimulus.
   *
   * @param stimulusId Stimulus ID.
   * @return List of matching stimulus action logs.
   */
  List<StimulusActionLog> getLogsForStimulus(UUID stimulusId);
}
