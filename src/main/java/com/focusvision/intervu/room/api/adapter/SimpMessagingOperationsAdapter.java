package com.focusvision.intervu.room.api.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.chat.model.OutgoingChatMessage;
import com.focusvision.intervu.room.api.chat.service.ChatCommunicationChannelService;
import com.focusvision.intervu.room.api.model.event.ParticipantMicrophoneToggleEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRemovedEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRoleChangedEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRoomStateEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantStateEvent;
import com.focusvision.intervu.room.api.model.event.RoomStateEvent;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * Implementation for various message sender adapters.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SimpMessagingOperationsAdapter implements
    StateNotificationSenderAdapter,
    ChatMessageSenderAdapter {

  private final SimpMessagingTemplate messagingTemplate;
  private final ChatCommunicationChannelService chatCommunicationChannelService;

  @Override
  public void send(ParticipantStateEvent event) {
    log.debug("Sending participant state event {}.", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending participant state event %s.", event), e);
    }
  }

  @Override
  public void send(ParticipantRoomStateEvent event) {
    log.debug("Sending participant room state event {}.", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending participant room state event %s.", event), e);
    }
  }

  @Override
  public void send(ParticipantMicrophoneToggleEvent event) {
    log.debug("Sending participant microphone toggle event {}.", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending participant microphone toggle event %s.", event), e);
    }
  }

  @Override
  public void send(ParticipantRoleChangedEvent event) {
    log.debug("Sending participant role changed event {}.", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending participant role changed event %s.", event), e);
    }
  }

  @Override
  public void send(RoomStateEvent event) {
    log.debug("Sending room state event {}.", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending room state event %s.", event), e);
    }
  }

  @Override
  public void send(OutgoingChatMessage message, UUID recipient) {
    log.debug("Sending chat message [{}] to {}.", message, recipient);

    try {
      messagingTemplate.convertAndSend(chatCommunicationChannelService.getChatChannel(recipient),
          message);
    } catch (MessagingException e) {
      log.error(format("Error sending chat message %s to %s.", message, recipient), e);
    }
  }

  @Override
  public void send(ParticipantRemovedEvent event) {
    log.debug("Sending participants removed event {}.", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending participants removed event %s.", event), e);
    }
  }
}
