package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the InterVu user.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("IntervuUser")
public class IntervuUserDto {

  @ApiModelProperty("User platform ID")
  private String platformId;
  @ApiModelProperty("User email")
  private String email;
  @ApiModelProperty("User first name")
  private String firstName;
  @ApiModelProperty("User last name")
  private String lastName;
  @ApiModelProperty("User's communication channel")
  private String channel;
}
