package com.focusvision.intervu.room.api.configuration.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * Caching properties.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 **/
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "app.caching")
public class CachingProperties {

  @NestedConfigurationProperty
  private final CachingProperties.Config authentication;
  @NestedConfigurationProperty
  private final CachingProperties.Config roomLog;

  /**
   * Caching configuratiton.
   */
  @Getter
  @RequiredArgsConstructor
  @ConstructorBinding
  public static class Config {

    /**
     * Caching enabled indicator.
     */
    private final boolean enabled;

    /**
     * Cache name.
     */
    private final String name;
  }
}
