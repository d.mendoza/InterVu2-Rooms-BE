package com.focusvision.intervu.room.api.export.model;

import com.focusvision.intervu.room.api.common.model.BookmarkType;
import io.swagger.annotations.ApiModel;

/**
 * Dto representing bookmark model for export.
 */
@ApiModel("Bookmark Export")
public record BookmarkExportDto(Long offset, String note, String createdBy, BookmarkType type) {

}
