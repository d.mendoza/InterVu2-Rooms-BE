package com.focusvision.intervu.room.api.common.model;

/**
 * Enumeration representing chat type.
 *
 * @author Branko Ostojic
 */
public enum ChatType {
  /**
   * Internal chat message.
   */
  INTERNAL,
  /**
   * Respondents chat message.
   */
  RESPONDENTS,
  /**
   * Private chat message.
   */
  PRIVATE,
}
