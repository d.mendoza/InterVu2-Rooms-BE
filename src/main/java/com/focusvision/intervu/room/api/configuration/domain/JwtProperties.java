package com.focusvision.intervu.room.api.configuration.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * JWT token properties.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 **/
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@ConfigurationProperties(prefix = "app.jwt")
public class JwtProperties {
  /**
   * Secret used for encoding the token.
   */
  private final String tokenSecret;
  /**
   * Token validity period in hours.
   */
  private final Integer tokenExpirationHours;
}
