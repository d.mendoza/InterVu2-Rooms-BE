package com.focusvision.intervu.room.api.draw.mapper;

import com.focusvision.intervu.room.api.draw.model.DrawingLog;
import com.focusvision.intervu.room.api.draw.model.DrawingLogDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link DrawingLog} entity.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Mapper(componentModel = "spring")
public interface DrawingLogMapper {

  /**
   * Maps drawing log to DTO.
   *
   * @param drawingLog Drawing log to be mapped.
   * @return Resulting DTO.
   */
  @Mapping(source = "actionType", target = "type")
  DrawingLogDto mapToDto(DrawingLog drawingLog);

}
