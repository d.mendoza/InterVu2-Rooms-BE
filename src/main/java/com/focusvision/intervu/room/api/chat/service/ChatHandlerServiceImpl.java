package com.focusvision.intervu.room.api.chat.service;

import static com.focusvision.intervu.room.api.chat.model.ChatSource.MEETING_ROOM;
import static com.focusvision.intervu.room.api.chat.model.ChatSource.WAITING_ROOM;
import static com.focusvision.intervu.room.api.chat.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.chat.model.ChatType.PRIVATE;
import static com.focusvision.intervu.room.api.chat.model.ChatType.RESPONDENTS;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;

import com.focusvision.intervu.room.api.chat.model.ChatSource;
import com.focusvision.intervu.room.api.chat.model.ChatType;
import com.focusvision.intervu.room.api.chat.model.GroupChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ParticipantChatMetadataDto;
import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.Collection;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ChatHandlerService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ChatHandlerServiceImpl implements ChatHandlerService {

  private final ChatDataService chatDataService;
  private final ChatMessageTrackingService chatTrackingService;
  private final ChatMetadataService chatMetadataService;
  private final ChatNotificationService chatNotificationService;
  private final ChatNotificationRecipientResolver notificationRecipientResolver;
  private final ChatHandleResolver chatHandleResolver;

  @Override
  public void handleMeetingRoomInternalMessage(AuthenticatedParticipant sender,
                                               String message) {
    log.debug("Handling meeting room internal chat message from {}.", sender.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    var handle = ofNullable(metadata.getInternal())
        .filter(not(GroupChatMetadataDto::isReadOnly))
        .filter(GroupChatMetadataDto::isEnabled)
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveMeetingRoomInternalMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, MEETING_ROOM, INTERNAL, chatMessage);
  }

  @Override
  public void handleMeetingRoomBookmarkMessage(AuthenticatedParticipant sender,
                                               String message) {
    log.debug("Handling meeting room bookmark chat message from {}.", sender.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    var handle = ofNullable(metadata.getInternal())
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveMeetingRoomBookmarkMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, MEETING_ROOM, INTERNAL, chatMessage);
  }

  @Override
  public void handleMeetingRoomPiiMessage(AuthenticatedParticipant sender) {
    log.debug("Handling meeting room PII chat message from {}.", sender.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    var handle = ofNullable(metadata.getInternal())
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveMeetingRoomPiiMessage(
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, MEETING_ROOM, INTERNAL, chatMessage);
  }

  @Override
  public void handleMeetingRoomSnapshotMessage(AuthenticatedParticipant sender,
                                               String message) {
    log.debug("Handling meeting room snapshot chat message (senderId: {}, message: {})",
        sender.getId(),
        message);

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    var handle = ofNullable(metadata.getInternal())
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveMeetingRoomSnapshotMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, MEETING_ROOM, INTERNAL, chatMessage);
  }

  @Override
  public void handleMeetingRoomRespondentsMessage(AuthenticatedParticipant sender,
                                                  String message) {
    log.debug("Handling meeting room respondents chat message from {}.", sender.getId());

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    var handle = ofNullable(metadata.getRespondents())
        .filter(not(GroupChatMetadataDto::isReadOnly))
        .filter(GroupChatMetadataDto::isEnabled)
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveMeetingRoomRespondentsMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, MEETING_ROOM, RESPONDENTS, chatMessage);
  }

  @Override
  public void handleMeetingRoomPrivateMessage(AuthenticatedParticipant sender,
                                              UUID handle,
                                              String message) {
    log.debug("Handling meeting room private chat message from {} sent to {}.",
        sender.getId(), handle);

    var metadata = chatMetadataService.getMeetingRoomChatMetadata(sender);
    ofNullable(metadata.getDirect())
        .stream()
        .flatMap(Collection::stream)
        .filter(not(ParticipantChatMetadataDto::isReadOnly))
        .filter(ParticipantChatMetadataDto::isEnabled)
        .map(ParticipantChatMetadataDto::getHandle)
        .filter(recipient -> recipient.equals(handle))
        .findFirst()
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveMeetingRoomPrivateMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, MEETING_ROOM, PRIVATE, chatMessage);
  }

  @Override
  public void handleWaitingRoomInternalMessage(AuthenticatedParticipant sender,
                                               String message) {
    log.debug("Handling waiting room internal chat message from {}.", sender.getId());

    var metadata = chatMetadataService.getWaitingRoomChatMetadata(sender);
    var handle = ofNullable(metadata.getInternal())
        .filter(not(GroupChatMetadataDto::isReadOnly))
        .filter(GroupChatMetadataDto::isEnabled)
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveWaitingRoomInternalMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, WAITING_ROOM, INTERNAL, chatMessage);
  }

  @Override
  public void handleWaitingRoomRespondentsMessage(AuthenticatedParticipant sender,
                                                  String message) {
    log.debug("Handling waiting room respondents chat message from {}.", sender.getId());

    var metadata = chatMetadataService.getWaitingRoomChatMetadata(sender);
    var handle = ofNullable(metadata.getRespondents())
        .filter(not(GroupChatMetadataDto::isReadOnly))
        .filter(GroupChatMetadataDto::isEnabled)
        .map(GroupChatMetadataDto::getHandle)
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveWaitingRoomRespondentsMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, WAITING_ROOM, RESPONDENTS, chatMessage);
  }

  @Override
  public void handleWaitingRoomPrivateMessage(AuthenticatedParticipant sender,
                                              UUID handle,
                                              String message) {
    log.debug("Handling meeting room private chat message from {} sent to {}.", sender.getId(),
        handle);

    var metadata = chatMetadataService.getWaitingRoomChatMetadata(sender);
    ofNullable(metadata.getDirect())
        .stream()
        .flatMap(Collection::stream)
        .filter(not(ParticipantChatMetadataDto::isReadOnly))
        .filter(ParticipantChatMetadataDto::isEnabled)
        .map(ParticipantChatMetadataDto::getHandle)
        .filter(recipient -> recipient.equals(handle))
        .findFirst()
        .orElseThrow(this::chatForbiddenException);

    ChatMessage chatMessage = chatDataService.saveWaitingRoomPrivateMessage(
        message,
        sender.getId(),
        sender.getPlatformId(),
        sender.getDisplayName(),
        handle,
        sender.getRoomId());

    this.sendChatNotification(sender, handle, WAITING_ROOM, PRIVATE, chatMessage);
  }

  @Override
  public void handleSeenMeetingRoomInternalMessage(UUID messageId,
                                                   AuthenticatedParticipant sender) {
    log.debug("Handling seen meeting room internal chat message {} from {}.", messageId,
        sender.getId());

    var handle = chatHandleResolver.resolveMeetingRoomInternalChatHandle(sender)
        .orElseThrow(this::chatForbiddenException);

    chatTrackingService.markAsSeenMeetingRoomInternalMessage(messageId, sender.getId(), handle);
  }

  @Override
  public void handleSeenMeetingRoomRespondentsMessage(UUID messageId,
                                                      AuthenticatedParticipant sender) {
    log.debug("Handling seen meeting room respondents chat message {} from {}.", messageId,
        sender.getId());

    var handle = chatHandleResolver.resolveMeetingRoomRespondentsChatHandle(sender)
        .orElseThrow(this::chatForbiddenException);

    chatTrackingService.markAsSeenMeetingRoomRespondentsMessage(messageId, sender.getId(), handle);
  }

  @Override
  public void handleSeenMeetingRoomPrivateMessage(UUID messageId,
                                                  UUID recipient,
                                                  AuthenticatedParticipant sender) {
    log.debug("Handling seen meeting room private chat message {} from {}.", messageId,
        sender.getId());

    var handle = chatHandleResolver.resolveMeetingRoomPrivateChatHandle(sender, recipient)
        .orElseThrow(this::chatForbiddenException);

    chatTrackingService.markAsSeenMeetingRoomPrivateMessage(messageId, sender.getId(), handle);
  }

  @Override
  public void handleSeenWaitingRoomInternalMessage(UUID messageId,
                                                   AuthenticatedParticipant sender) {
    log.debug("Handling seen waiting room internal chat message {} from {}.", messageId,
        sender.getId());

    var handle = chatHandleResolver.resolveWaitingRoomInternalChatHandle(sender)
        .orElseThrow(this::chatForbiddenException);

    chatTrackingService.markAsSeenWaitingRoomInternalMessage(messageId, sender.getId(), handle);
  }

  @Override
  public void handleSeenWaitingRoomRespondentsMessage(UUID messageId,
                                                      AuthenticatedParticipant sender) {
    log.debug("Handling seen waiting room respondents chat message {} from {}.", messageId,
        sender.getId());

    var handle = chatHandleResolver.resolveWaitingRoomRespondentsChatHandle(sender)
        .orElseThrow(this::chatForbiddenException);

    chatTrackingService.markAsSeenWaitingRoomRespondentsMessage(messageId, sender.getId(), handle);
  }

  @Override
  public void handleSeenWaitingRoomPrivateMessage(UUID messageId,
                                                  UUID recipient,
                                                  AuthenticatedParticipant sender) {
    log.debug("Handling seen waiting room private chat message {} from {}.", messageId,
        sender.getId());

    var handle = chatHandleResolver.resolveWaitingRoomPrivateChatHandle(sender, recipient)
        .orElseThrow(this::chatForbiddenException);

    chatTrackingService.markAsSeenWaitingRoomPrivateMessage(messageId, sender.getId(), handle);
  }

  private void sendChatNotification(AuthenticatedParticipant sender,
                                    UUID handle,
                                    ChatSource source,
                                    ChatType type,
                                    ChatMessage chatMessage) {
    notificationRecipientResolver
        .resolveRecipients(sender, handle, source, type)
        .forEach(participant -> this.sendChatNotification(chatMessage, participant));
  }

  private void sendChatNotification(ChatMessage chatMessage,
                                    ParticipantChatMetadataDto participantMetadata) {
    chatNotificationService.send(chatMessage, participantMetadata.getHandle());
  }

  private IntervuRoomException chatForbiddenException() {
    return new ActionForbiddenException("Chat action forbidden.");
  }
}
