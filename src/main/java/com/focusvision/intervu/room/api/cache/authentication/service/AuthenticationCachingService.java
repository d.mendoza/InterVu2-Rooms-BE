package com.focusvision.intervu.room.api.cache.authentication.service;

/**
 * Contract for service providing authentication cache eviction functionality. Implementor may or
 * may not implement caching as well, but it will be responsible for eviction.
 *
 * @author Branko Ostojic
 */
public interface AuthenticationCachingService {

  /**
   * Clears authentication cache belonging participant.
   *
   * @param participantId Participant ID.
   */
  void clearAuthenticationCache(String participantId);
}
