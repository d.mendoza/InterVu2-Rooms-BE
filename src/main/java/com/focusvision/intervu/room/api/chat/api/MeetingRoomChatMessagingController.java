package com.focusvision.intervu.room.api.chat.api;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;
import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.chat.model.IncomingChatMessage;
import com.focusvision.intervu.room.api.chat.model.SeenChatMessage;
import com.focusvision.intervu.room.api.chat.service.ChatHandlerService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

/**
 * Controller responsible for meeting room chat messaging operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class MeetingRoomChatMessagingController {

  private final ChatHandlerService chatHandlerService;

  /**
   * Handles meeting room internal chat messages.
   *
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("meeting-room/chat/internal")
  public void internal(@Payload @Validated IncomingChatMessage message,
                       Principal principal) {
    log.debug("Receiving meeting room internal chat message, sent by {}",
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var text = message.getMessage();
    chatHandlerService.handleMeetingRoomInternalMessage(sender, text);
  }

  /**
   * Handles meeting room respondents chat messages.
   *
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("meeting-room/chat/respondents")
  public void respondents(@Payload @Validated IncomingChatMessage message,
                          Principal principal) {
    log.debug("Receiving meeting room respondents chat, sent by {}",
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var text = message.getMessage();
    chatHandlerService.handleMeetingRoomRespondentsMessage(sender, text);
  }

  /**
   * Handles meeting room private chat messages.
   *
   * @param handle    Chat handle.
   * @param message   Chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("meeting-room/chat/private/{handle}")
  public void privateChat(@DestinationVariable String handle,
                          @Payload @Validated IncomingChatMessage message,
                          Principal principal) {
    log.debug("Receiving meeting room private chat message, sent by {}",
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var text = message.getMessage();

    chatHandlerService.handleMeetingRoomPrivateMessage(sender, fromString(handle), text);
  }

  /**
   * Marks meeting room internal chat message as seen.
   *
   * @param message   Seen chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("meeting-room/chat/internal/seen")
  public void internalSeen(@Payload @Validated SeenChatMessage message,
                           Principal principal) {
    log.debug("Receiving seen meeting room internal chat message {}, sent by {}", message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var chatMessageId = message.getId();
    chatHandlerService.handleSeenMeetingRoomInternalMessage(chatMessageId, sender);
  }

  /**
   * Marks meeting room respondents chat message as seen.
   *
   * @param message   Seen chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("meeting-room/chat/respondents/seen")
  public void respondentsSeen(@Payload @Validated SeenChatMessage message,
                              Principal principal) {
    log.debug("Receiving seen meeting room respondents chat message {}, sent by {}", message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var chatMessageId = message.getId();
    chatHandlerService.handleSeenMeetingRoomRespondentsMessage(chatMessageId, sender);
  }

  /**
   * Marks meeting room private chat message as seen.
   *
   * @param handle    Chat handle.
   * @param message   Seen chat message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("meeting-room/chat/private/{handle}/seen")
  public void privateSeen(@DestinationVariable String handle,
                          @Payload @Validated SeenChatMessage message,
                          Principal principal) {
    log.debug("Receiving seen meeting room private chat message {}, sent by {}", message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    var chatMessageId = message.getId();
    chatHandlerService.handleSeenMeetingRoomPrivateMessage(chatMessageId, fromString(handle),
        sender);
  }

}
