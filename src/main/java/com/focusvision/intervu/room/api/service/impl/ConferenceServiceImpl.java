package com.focusvision.intervu.room.api.service.impl;

import static com.twilio.rest.video.v1.Room.RoomStatus.IN_PROGRESS;
import static java.lang.String.format;

import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.recording.service.RecordingService;
import com.focusvision.intervu.room.api.repository.ConferenceRepository;
import com.focusvision.intervu.room.api.service.CompositionParametersService;
import com.focusvision.intervu.room.api.service.ConferenceService;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.StreamingService;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ConferenceService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ConferenceServiceImpl implements ConferenceService {

  private final ConferenceRepository conferenceRepository;
  private final StreamingService streamingService;
  private final ParticipantService participantService;
  private final ResearchSessionService researchSessionService;
  private final RecordingService recordingService;
  private final CompositionParametersService compositionParametersService;

  @Override
  public Conference start(ResearchSession researchSession) {
    log.info("Starting conference for session {}.", researchSession.getId());

    var conferenceRoom = streamingService.createGroupRoom(researchSession.getId());
    researchSession
        .getParticipants()
        .forEach(
            p -> {
              var groupRoomParticipant =
                  streamingService.createGroupRoomParticipant(p.getId(), conferenceRoom.getSid());
              participantService.updateConferenceToken(p.getId(), groupRoomParticipant.getToken());
            });

    var conference =
        new Conference().setRoomSid(conferenceRoom.getSid()).setResearchSession(researchSession);
    return conferenceRepository.save(conference);
  }

  @Override
  public Conference finish(UUID id) {
    log.info("Finishing conference (id: {})", id);

    var conference = conferenceRepository.getById(id);
    streamingService.completeGroupRoom(conference.getRoomSid());
    compositionParametersService.resolveCompositionParams(conference).forEach(
        param -> recordingService.createPending(conference, param.layout(), param.audioChannel())
    );

    return conferenceRepository.save(conference.setCompleted(true));
  }

  @Override
  public void regenerateGroupRoomRecordings(
      Conference conference, CompositionLayout layout) {
    log.info(
        "Regenerating room recording (conferenceId: {}, layout: {}",
        conference.getId(),
        layout);

    try {
      researchSessionService.validateCompositionLayout(layout);
      compositionParametersService.resolveAudioChannels(conference)
          .forEach(audioChannel ->
              recordingService.markAsPendingOrCreateNew(conference, layout, audioChannel));
    } catch (Exception e) {
      log.error(
          format("Error regenerating group room recording for conference %s.", conference.getId()),
          e);
      throw e;
    }
  }

  @Override
  public void regenerateGroupRoomRecordings(String roomPlatformId) {
    log.debug("Regenerating group room recordings for platform ID {}", roomPlatformId);

    var conference = findByResearchSession(roomPlatformId)
        .orElseThrow(ResourceNotFoundException::new);

    try {
      for (var param : compositionParametersService.resolveCompositionParams(conference)) {
        researchSessionService.validateCompositionLayout(param.layout());
        recordingService.markAsPendingOrCreateNew(conference, param.layout(), param.audioChannel());
      }
    } catch (Exception e) {
      log.error(
          format("Error regenerating group room recordings for conference %s.", conference.getId()),
          e);
      throw e;
    }
  }


  @Override
  public void kickParticipantFromRoom(Conference conference, UUID participantId) {
    log.info("Kicking participant {} from conference {}.", participantId, conference.getId());

    streamingService.kickParticipantFromRoom(conference.getRoomSid(), participantId);
  }

  @Override
  public void deleteRecordings(UUID id) {
    log.info("Deleting all conference {} recordings.", id);

    var conference = conferenceRepository.getById(id);
    streamingService.deleteGroupRoomRecordings(conference.getRoomSid());
  }

  @Override
  public List<Conference> getAllRunningConferences() {
    log.debug("Fetching all running conferences.");

    return conferenceRepository.findByCompleted(false);
  }

  @Override
  public boolean shouldConferenceBeClosed(UUID id) {
    log.debug("Checking should conference {} be closed.", id);

    var conference = conferenceRepository.getById(id);
    var inProgress =
        streamingService.getGroupRoom(conference.getRoomSid()).getStatus().equals(IN_PROGRESS);
    return !conference.isCompleted() && !inProgress;
  }

  @Override
  public Conference get(UUID id) {
    log.debug("Getting conference {} details.", id);

    return conferenceRepository.getById(id);
  }

  @Override
  public Optional<Conference> findByResearchSession(String platformId) {
    log.debug("Finding conference by research session ID {}", platformId);

    return conferenceRepository.findByResearchSessionPlatformId(platformId);
  }

}
