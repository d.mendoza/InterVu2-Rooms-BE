package com.focusvision.intervu.room.api.tracking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model representing room state ACK message.
 *
 * @author Branko Ostojic
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomStateAckMessage {

  /**
   * Room state version to acknowledge.
   */
  private Long version;
}
