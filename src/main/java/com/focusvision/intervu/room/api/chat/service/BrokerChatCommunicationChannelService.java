package com.focusvision.intervu.room.api.chat.service;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Broker specific implementation of a {@link ChatCommunicationChannelService}.
 */
@Slf4j
@Service
public class BrokerChatCommunicationChannelService implements ChatCommunicationChannelService {

  /**
   * Represents the destination prefix for the chat topic.
   */
  public static final String CHAT_CHANNEL = "/topic/chat";

  @Override
  public String getParticipantChatChannel(UUID participantId) {
    log.debug("Getting chat channel details for participant {}.", participantId);

    return this.getChatChannel(participantId);
  }

  @Override
  public String getChatChannel(UUID handle) {
    return CHAT_CHANNEL + "." + handle;
  }
}
