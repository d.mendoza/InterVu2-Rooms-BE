package com.focusvision.intervu.room.api.adapter.impl;

import com.focusvision.intervu.room.api.adapter.BannedParticipantNotificationSenderAdapter;
import com.focusvision.intervu.room.api.configuration.domain.MessagingProperties;
import com.focusvision.intervu.room.api.messaging.RabbitMqProducer;
import com.focusvision.intervu.room.api.model.messaging.BannedParticipantData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link BannedParticipantNotificationSenderAdapter}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BannedParticipantNotificationSenderAdapterImpl
    implements BannedParticipantNotificationSenderAdapter {

  private final MessagingProperties messagingProperties;
  private final RabbitMqProducer rabbitMqProducer;

  @Override
  public void send(BannedParticipantData bannedParticipantData) {
    log.debug("Sending banned participant data [{}]", bannedParticipantData);

    try {
      rabbitMqProducer.send(messagingProperties.getParticipantBannedQueue(), bannedParticipantData);
    } catch (MessagingException e) {
      log.error("Error sending banned participant event for [{}]", bannedParticipantData);
    }

  }
}
