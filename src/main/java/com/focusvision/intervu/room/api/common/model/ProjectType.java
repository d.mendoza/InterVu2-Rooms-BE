package com.focusvision.intervu.room.api.common.model;

/**
 * Enum that represents project type.
 *
 * @author Branko Ostojic
 */
public enum ProjectType {

  /**
   * Representing full type of project.
   */
  FULL_SERVICE,

  /**
   * Representing self service type of project.
   */
  SELF_SERVICE

}
