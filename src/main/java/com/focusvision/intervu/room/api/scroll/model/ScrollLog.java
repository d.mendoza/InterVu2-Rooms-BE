package com.focusvision.intervu.room.api.scroll.model;

import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing scroll log entity.
 */
@Entity
@Table(name = "scroll_logs")
@Getter
@Setter
@Accessors(chain = true)
public class ScrollLog {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private UUID roomId;

  @Column
  private UUID participantId;

  @Column(name = "start_offset")
  private Long offset;

  @Column
  private UUID contextId;

  @Column
  private String content;

  @Column
  private Instant addedAt;
}
