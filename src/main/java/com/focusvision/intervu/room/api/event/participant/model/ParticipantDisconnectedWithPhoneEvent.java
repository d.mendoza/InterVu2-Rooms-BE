package com.focusvision.intervu.room.api.event.participant.model;

import java.util.UUID;

/**
 * DTO representing the participant disconnected with phone event.
 *
 * @author Branko Ostojic
 */
public record ParticipantDisconnectedWithPhoneEvent(UUID roomId,
                                                    UUID participantId) {

}
