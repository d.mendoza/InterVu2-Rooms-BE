package com.focusvision.intervu.room.api.model.entity;

import static com.focusvision.intervu.room.api.common.model.StimulusType.DOCUMENT_SHARING;

import com.focusvision.intervu.room.api.common.model.StimulusType;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing stimulus.
 *
 * @author Branko Ostojic
 */
@Entity
@Table(name = "stimuli")
@Getter
@Setter
@Accessors(chain = true)
public class Stimulus extends Auditable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  @Column
  private String platformId;
  @Column
  private String name;
  @Column
  @Enumerated(EnumType.STRING)
  private StimulusType type;
  @Column
  private String thumbnail;
  @Column
  private String data;
  @Column
  @OrderBy
  private Integer position;

  @ManyToOne
  private ResearchSession researchSession;

  public boolean isDocumentSharing() {
    return DOCUMENT_SHARING.equals(type);
  }

}
