package com.focusvision.intervu.room.api.model;

import lombok.Builder;
import lombok.Getter;

/**
 * Class that contains screenshare position details.
 */
@Getter
@Builder
public class ScreenArea {

  private Long offsetX;

  private Long offsetY;

  private Long width;

  private Long height;

}
