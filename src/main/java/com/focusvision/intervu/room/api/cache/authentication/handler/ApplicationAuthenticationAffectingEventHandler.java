package com.focusvision.intervu.room.api.cache.authentication.handler;

import static java.util.List.of;
import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.cache.authentication.event.ClearAuthenticationCacheEvent;
import com.focusvision.intervu.room.api.cache.authentication.publisher.AuthenticationCacheEvictionEventPublisher;
import com.focusvision.intervu.room.api.event.participant.handler.ParticipantBannedEventHandler;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantBannedEvent;
import com.focusvision.intervu.room.api.event.room.handler.RoomFinishedEventHandler;
import com.focusvision.intervu.room.api.event.room.handler.RoomUpdatedEventHandler;
import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomUpdatedEvent;
import com.focusvision.intervu.room.api.model.entity.Participant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * {@link EventListener} specific implementation of a {@link ClearAuthenticationCacheEventHandler}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ApplicationAuthenticationAffectingEventHandler
    implements ParticipantBannedEventHandler, RoomUpdatedEventHandler, RoomFinishedEventHandler {

  private final AuthenticationCacheEvictionEventPublisher publisher;

  @Override
  @EventListener
  public void handle(ParticipantBannedEvent event) {
    log.debug("Handling participant banned event (event: {})", event);

    publisher.publish(new ClearAuthenticationCacheEvent(event.roomId(), of(event.participantId())));
  }

  @Override
  @EventListener
  public void handle(RoomUpdatedEvent event) {
    log.debug("Handling room updated event (event: {})", event);

    var participantIds = event.participants().stream().map(Participant::getId).toList();
    publisher.publish(new ClearAuthenticationCacheEvent(event.roomId(), participantIds));
  }

  @Override
  @EventListener
  public void handle(RoomFinishedEvent event) {
    log.debug("Handling room finished event (event: {})", event);

    var participantIds = event.participants().stream().map(Participant::getId).toList();
    publisher.publish(new ClearAuthenticationCacheEvent(event.roomId(), participantIds));
  }
}
