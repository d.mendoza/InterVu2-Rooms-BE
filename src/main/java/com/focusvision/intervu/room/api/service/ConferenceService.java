package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.CompositionLayout;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service used for conference related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface ConferenceService {

  /**
   * Create and starts a conference for the provided room/session.
   *
   * @param researchSession Room/ResearchSession.
   * @return Conference details.
   */
  Conference start(ResearchSession researchSession);

  /**
   * Marks conference as finished and creates all necessary recording data for further processing.
   * Recordings processing will be done as a separate process.
   *
   * @param id Conference ID.
   * @return Updated conference data.
   */
  Conference finish(UUID id);

  /**
   * Kicks/removes the specified participant from the specified conference.
   *
   * @param conference    Conference.
   * @param participantId Participant ID.
   */
  void kickParticipantFromRoom(Conference conference, UUID participantId);

  /**
   * Deletes all conference recordings.
   *
   * @param id Conference ID.
   */
  void deleteRecordings(UUID id);

  /**
   * Gets all in progress conferences.
   *
   * @return List of in progress conferences.
   */
  List<Conference> getAllRunningConferences();

  /**
   * Check whether specified conference should be closed. If it is running and corresponding room on
   * streaming service is closed/expired, it would mean that this conference should be marked as
   * closed/finished.
   *
   * @param id Conference ID.
   */
  boolean shouldConferenceBeClosed(UUID id);

  /**
   * Gets the conference with corresponding ID.
   *
   * @param id Conference ID.
   * @return Matching conference.
   */
  Conference get(UUID id);

  /**
   * Finds a conference by provided research session platform ID.
   *
   * @param platformId Research session platform ID.
   * @return Matching conference, or empty {@link Optional}.
   */
  Optional<Conference> findByResearchSession(String platformId);

  /**
   * Regenerates a group room recording with a new layout for the provided conference.
   *
   * @param conference Conference.
   * @param layout     New composition layout.
   */
  void regenerateGroupRoomRecordings(Conference conference, CompositionLayout layout);

  /**
   * Regenerates all group room recordings for the provided room platform ID.
   *
   * @param platformId Room platform ID.
   */
  void regenerateGroupRoomRecordings(String platformId);
}
