package com.focusvision.intervu.room.api.service.impl;

import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.common.model.StimulusType;
import com.focusvision.intervu.room.api.event.stimulus.adapter.PollStimulusBroadcastResultsNotificationSenderAdapter;
import com.focusvision.intervu.room.api.model.dto.PollStimulusRespondentResultDto;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.model.event.PollStimulusBroadcastResultsDataEvent;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.service.StimulusNotificationService;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.stimulus.model.StimulusActionState;
import com.focusvision.intervu.room.api.state.stimulus.service.StimulusStateService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation for {@link StimulusNotificationService}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StimulusNotificationServiceImpl implements StimulusNotificationService {

  private final PollStimulusBroadcastResultsNotificationSenderAdapter senderAdapter;
  private final CommunicationChannelService communicationChannelService;
  private final StimulusStateService stimulusStateService;
  private final StimulusService stimulusService;

  @Override
  public void sendStimulusBroadcastResultsNotification(UUID roomId, UUID stimulusId) {
    log.debug("Sending stimulus {} broadcast results notification in room {}.", stimulusId, roomId);

    if (stimulusService.isStimulusActiveInRoom(stimulusId, roomId)) {
      stimulusService.find(stimulusId, roomId)
          .map(Stimulus::getType)
          .ifPresent(type -> processStimulusBroadcast(roomId, stimulusId, type));
    } else {
      log.warn("Broadcasting results for non active stimulus {} in room {}", stimulusId, roomId);
    }
  }

  private void processStimulusBroadcast(UUID roomId, UUID stimulusId, StimulusType type) {
    var destination = communicationChannelService.getStimulusGlobalChannel(roomId);

    if (type == StimulusType.POLL) {
      sendPollStimulusBroadcastResultsNotification(stimulusId, destination);
    }
  }

  private void sendPollStimulusBroadcastResultsNotification(UUID stimulusId, String destination) {
    log.debug("Sending poll stimulus {} broadcast results notification to {}", stimulusId,
        destination);

    var results = stimulusStateService.getStimulusState(stimulusId)
        .stream()
        .map(StimulusActionState::getContent)
        .toList();

    var event = PollStimulusBroadcastResultsDataEvent.builder()
        .destination(destination)
        .data(new PollStimulusRespondentResultDto()
            .setStimulusId(stimulusId)
            .setResults(results))
        .build();

    senderAdapter.send(event);
  }
}
