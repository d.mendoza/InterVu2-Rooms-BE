package com.focusvision.intervu.room.api.service;

import com.focusvision.intervu.room.api.model.dto.ChannelDto;
import com.focusvision.intervu.room.api.security.IntervuUser;
import java.util.UUID;

/**
 * Service used for communication channel related operations.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public interface CommunicationChannelService {

  /**
   * Fetch participant communication channels details.
   *
   * @param participantId Participant ID.
   * @param roomId        Room ID.
   * @return Communication channel details.
   */
  ChannelDto getParticipantChannelData(UUID participantId, UUID roomId);

  /**
   * Returns a global channel for a specified participant.
   *
   * @param participantPlatformId Participant platform ID.
   * @return Global channel data.
   */
  String getParticipantGlobalChannel(String participantPlatformId);

  /**
   * Returns a global channel for a participant for specified InterVu user.
   *
   * @param intervuUser InterVu user.
   * @return Global channel data.
   */
  String getParticipantGlobalChannel(IntervuUser intervuUser);

  /**
   * Returns a channel for a specified participant ID.
   *
   * @param participantId Participant ID.
   * @return Participant communication channel.
   */
  String getParticipantChannel(UUID participantId);

  /**
   * Gets the room channel.
   *
   * @param roomId Room ID.
   * @return Room channel.
   */
  String getRoomChannel(UUID roomId);

  /**
   * Gets the room draw channel.
   *
   * @param roomId Room ID.
   * @return Draw channel.
   */
  String getDrawChannel(UUID roomId);

  /**
   * Gets the room stimulus action channel.
   *
   * @param roomId Room ID.
   * @return Stimulus action channel.
   */
  String getStimulusChannel(UUID roomId);

  /**
   * Gets the room stimulus respondent channel. This channel is used for sending other respondents
   * research data without exposing respondent PII data.
   *
   * @param roomId Room ID.
   * @return Stimulus respondent channel.
   */
  String getStimulusGlobalChannel(UUID roomId);
}
