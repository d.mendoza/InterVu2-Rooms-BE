package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.event.ParticipantMicrophoneToggleEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRemovedEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRoleChangedEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantRoomStateEvent;
import com.focusvision.intervu.room.api.model.event.ParticipantStateEvent;
import com.focusvision.intervu.room.api.model.event.RoomStateEvent;

/**
 * Adapter for sending room and participant state notifications.
 *
 * @author Branko Ostojic
 */
public interface StateNotificationSenderAdapter {

  /**
   * Sends the provided participant state event to specified destination.
   *
   * @param participantStateEvent Participant state event to be sent.
   */
  void send(ParticipantStateEvent participantStateEvent);

  /**
   * Sends the provided participant room state event to specified destination.
   *
   * @param participantRoomStateEvent Participant room state event to be sent.
   */
  void send(ParticipantRoomStateEvent participantRoomStateEvent);

  /**
   * Sends the provided room details event to specified destination.
   *
   * @param roomStateEvent Room state event to be sent.
   */
  void send(RoomStateEvent roomStateEvent);

  /**
   * Sends the provided microphone toggle event to specified destination.
   *
   * @param participantMicrophoneToggleEvent Participant microphone toggle event to be sent.
   */
  void send(ParticipantMicrophoneToggleEvent participantMicrophoneToggleEvent);

  /**
   * Sends the provided participant removed event to specified destination.
   *
   * @param participantRemovedEvent Participant removed event to be send.
   */
  void send(ParticipantRemovedEvent participantRemovedEvent);

  /**
   * Sends the provided role change event to specified destination.
   *
   * @param participantRoleChangedEvent Participant role change event to be sent.
   */
  void send(ParticipantRoleChangedEvent participantRoleChangedEvent);

}
