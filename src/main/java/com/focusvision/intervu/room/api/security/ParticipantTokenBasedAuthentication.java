package com.focusvision.intervu.room.api.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Token authentication implementation of <code>Authentication</code>.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
public class ParticipantTokenBasedAuthentication extends AbstractAuthenticationToken {

  private static final long serialVersionUID = 7910953044599712941L;

  private final String token;
  private final AuthenticatedParticipant principle;

  /**
   * Constructs authentication details.
   *
   * @param principle Principal user.
   * @param token     Token.
   */
  public ParticipantTokenBasedAuthentication(final AuthenticatedParticipant principle,
                                             final String token) {
    super(principle.getAuthorities());
    this.principle = principle;
    this.token = token;
  }

  @Override
  public boolean isAuthenticated() {
    return principle.isAuthenticated();
  }

  @Override
  public Object getCredentials() {
    return token;
  }

  @Override
  public UserDetails getPrincipal() {
    return principle;
  }

}
