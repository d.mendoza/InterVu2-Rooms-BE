package com.focusvision.intervu.room.api.adapter;

import com.focusvision.intervu.room.api.model.messaging.ResearchSessionStateData;

/**
 * Adapter for sending research session and conference state update data.
 */
public interface ResearchSessionStateUpdateSenderAdapter {

  /**
   * Sends the provided research session and conference state update data.
   *
   * @param researchSessionStateData Research session state to be sent.
   */
  void send(ResearchSessionStateData researchSessionStateData);
}
