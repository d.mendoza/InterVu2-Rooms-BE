package com.focusvision.intervu.room.api.cache.authentication.event;

import java.util.List;
import java.util.UUID;

/**
 * Model for clear authentication cache event.
 *
 * @author Branko Ostojic
 */
public record ClearAuthenticationCacheEvent(UUID roomId,
                                            List<UUID> participantIds) {

}
