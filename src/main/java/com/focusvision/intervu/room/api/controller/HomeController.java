package com.focusvision.intervu.room.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller for Swagger.
 *
 * @author Branko Ostojic (bostojic@itekako.com)
 */
@Controller
@ApiIgnore
public class HomeController {

  @GetMapping("home")
  public String home() {
    return "redirect:/swagger-ui/";
  }
}
