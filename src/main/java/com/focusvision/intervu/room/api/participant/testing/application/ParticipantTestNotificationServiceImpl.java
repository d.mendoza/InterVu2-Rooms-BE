package com.focusvision.intervu.room.api.participant.testing.application;

import com.focusvision.intervu.room.api.participant.testing.model.ParticipantTestingResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link ParticipantTestNotificationService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantTestNotificationServiceImpl implements ParticipantTestNotificationService {

  private final RespondentTestingNotificationSenderAdapter sender;

  @Override
  public void notifyTestingFailure(String participantPlatformId, String roomPlatformId) {
    log.debug("Sending testing failure data of a participant {}.", participantPlatformId);

    var participantTestingResult = ParticipantTestingResult.builder()
        .success(false)
        .respondentId(participantPlatformId)
        .sessionId(roomPlatformId)
        .build();
    sender.send(participantTestingResult);
  }

  @Override
  public void notifyTestingSuccess(String participantPlatformId, String roomPlatformId) {
    log.debug("Sending testing success data of a participant {}.", participantPlatformId);

    var participantTestingResult = ParticipantTestingResult.builder()
        .success(true)
        .respondentId(participantPlatformId)
        .sessionId(roomPlatformId)
        .build();

    sender.send(participantTestingResult);
  }
}
