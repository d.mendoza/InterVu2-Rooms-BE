package com.focusvision.intervu.room.api.tracking.api;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;

import com.focusvision.intervu.room.api.tracking.model.ParticipantActionTrackingMessage;
import com.focusvision.intervu.room.api.tracking.service.ParticipantTrackingService;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

/**
 * Controller responsible for participant actions tracking operations.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class ParticipantTrackingMessagingController {

  private final ParticipantTrackingService trackingService;

  /**
   * Handles participant action tracking message.
   *
   * @param message Participant action tracking message.
   * @param principal Authenticated participant.
   */
  @MessageMapping("tracking/participant")
  public void track(Principal principal,
                    @Payload @Validated ParticipantActionTrackingMessage message) {
    log.debug(
        "Receiving participant action tracking message (message: {}, participant: {})",
        message,
        principal.getName());

    var sender = extractParticipant(principal).orElseThrow();
    trackingService.track(sender.getId(), sender.getRoomId(), message.getType());
  }
}
