package com.focusvision.intervu.room.api.recording.event;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import java.time.Instant;
import java.util.UUID;

/**
 * Model for room recording processing error event.
 */
public record RecordingProcessingErrorEvent(UUID roomId,
                                            String roomPlatformId,
                                            String conferenceId,
                                            CompositionLayout layout,
                                            Instant startedAt,
                                            Instant finishedAt,
                                            AudioChannel audioChannel) {

}
