package com.focusvision.intervu.room.api.chat.service;

import static com.focusvision.intervu.room.api.common.model.ChatType.INTERNAL;
import static com.focusvision.intervu.room.api.common.model.ChatType.PRIVATE;
import static com.focusvision.intervu.room.api.common.model.ChatType.RESPONDENTS;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.ChatSource.MEETING_ROOM;
import static com.focusvision.intervu.room.api.model.entity.ChatMessage.ChatSource.WAITING_ROOM;
import static java.time.Instant.now;
import static java.util.function.Predicate.not;

import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import com.focusvision.intervu.room.api.model.entity.ChatMessageTrack;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Only last seen message tracking implementation of a {@link ChatMessageTrackingService}.
 *
 * @author Branko Ostojic¬
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LatestSeenChatMessageTrackingService implements ChatMessageTrackingService {

  private final ChatMessageTrackProvider provider;

  @Override
  public void markAsSeenMeetingRoomPrivateMessage(UUID messageId,
                                                  UUID participantId,
                                                  UUID handle) {
    log.debug("Marking {} message as seen by {}.", messageId, participantId);

    this.markAsSeen(MEETING_ROOM, PRIVATE, messageId, participantId, handle);
  }

  @Override
  public void markAsSeenMeetingRoomInternalMessage(UUID messageId,
                                                   UUID participantId,
                                                   UUID handle) {
    log.debug("Marking {} message as seen by {}.", messageId, participantId);

    this.markAsSeen(MEETING_ROOM, INTERNAL, messageId, participantId, handle);
  }

  @Override
  public void markAsSeenMeetingRoomRespondentsMessage(UUID messageId,
                                                      UUID participantId,
                                                      UUID handle) {
    log.debug("Marking {} message as seen by {}.", messageId, participantId);

    this.markAsSeen(MEETING_ROOM, RESPONDENTS, messageId, participantId, handle);
  }

  @Override
  public void markAsSeenWaitingRoomPrivateMessage(UUID messageId,
                                                  UUID participantId,
                                                  UUID handle) {
    log.debug("Marking {} message as seen by {}.", messageId, participantId);

    this.markAsSeen(WAITING_ROOM, PRIVATE, messageId, participantId, handle);
  }

  @Override
  public void markAsSeenWaitingRoomInternalMessage(UUID messageId,
                                                   UUID participantId,
                                                   UUID handle) {
    log.debug("Marking {} message as seen by {}.", messageId, participantId);

    this.markAsSeen(WAITING_ROOM, INTERNAL, messageId, participantId, handle);
  }

  @Override
  public void markAsSeenWaitingRoomRespondentsMessage(UUID messageId,
                                                      UUID participantId,
                                                      UUID handle) {
    log.debug("Marking {} message as seen by {}.", messageId, participantId);

    this.markAsSeen(WAITING_ROOM, RESPONDENTS, messageId, participantId, handle);
  }

  @Override
  public Optional<UUID> getLastSeenMeetingRoomInternalMessage(UUID participantId, UUID handle) {
    log.debug("Getting last seen meeting room internal {} message for {}.", handle, participantId);

    return provider.findLatestTrack(MEETING_ROOM, INTERNAL, participantId, handle)
        .map(ChatMessageTrack::getChatMessageId);
  }

  @Override
  public Optional<UUID> getLastSeenMeetingRoomRespondentsMessage(UUID participantId, UUID handle) {
    log.debug("Getting last seen meeting room respondents {} message for {}.", handle,
        participantId);

    return provider.findLatestTrack(MEETING_ROOM, RESPONDENTS, participantId, handle)
        .map(ChatMessageTrack::getChatMessageId);
  }

  @Override
  public Optional<UUID> getLastSeenMeetingRoomPrivateMessage(UUID participantId, UUID handle) {
    log.debug("Getting last seen meeting room private {} message for {}.", handle, participantId);

    return provider.findLatestTrack(MEETING_ROOM, PRIVATE, participantId, handle)
        .map(ChatMessageTrack::getChatMessageId);
  }

  @Override
  public Optional<UUID> getLastSeenWaitingRoomInternalMessage(UUID participantId, UUID handle) {
    log.debug("Getting last seen waiting room internal {} message for {}.", handle, participantId);

    return provider.findLatestTrack(WAITING_ROOM, INTERNAL, participantId, handle)
        .map(ChatMessageTrack::getChatMessageId);
  }

  @Override
  public Optional<UUID> getLastSeenWaitingRoomRespondentsMessage(UUID participantId, UUID handle) {
    log.debug("Getting last seen waiting room respondents {} message for {}.", handle,
        participantId);

    return provider.findLatestTrack(WAITING_ROOM, RESPONDENTS, participantId, handle)
        .map(ChatMessageTrack::getChatMessageId);
  }

  @Override
  public Optional<UUID> getLastSeenWaitingRoomPrivateMessage(UUID participantId, UUID handle) {
    log.debug("Getting last seen waiting room private {} message for {}.", handle, participantId);

    return provider.findLatestTrack(WAITING_ROOM, PRIVATE, participantId, handle)
        .map(ChatMessageTrack::getChatMessageId);
  }

  private void markAsSeen(ChatMessage.ChatSource source,
                          ChatType type,
                          UUID messageId,
                          UUID participantId,
                          UUID handle) {
    var chatMessageTrack = provider
        .findLatestTrack(source, type, participantId, handle)
        .filter(not(track -> track.getChatMessageId().equals(messageId)))
        .map(track -> track.setChatMessageId(messageId))
        .orElseGet(() -> new ChatMessageTrack()
            .setSource(source)
            .setType(type)
            .setParticipantId(participantId)
            .setHandle(handle)
            .setChatMessageId(messageId))
        .setSeenAt(now());

    provider.addTrack(chatMessageTrack);
  }
}
