package com.focusvision.intervu.room.api.configuration;

import static java.lang.String.join;

import com.focusvision.intervu.room.api.configuration.domain.CorsProperties;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * CORS filter.
 *
 * @author Branko Ostojic
 */
@Component
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter implements Filter {

  private final CorsProperties corsProperties;

  @Override
  public void doFilter(ServletRequest req,
                       ServletResponse res,
                       FilterChain chain) throws IOException, ServletException {
    if ("OPTIONS".equalsIgnoreCase(((HttpServletRequest) req).getMethod())) {
      final HttpServletResponse response = (HttpServletResponse) res;
      response.setHeader("Access-Control-Allow-Origin",
          join(",", corsProperties.getAllowedOrigins()));
      response.setHeader("Access-Control-Allow-Methods", "POST, PUT, PATCH, GET, OPTIONS, DELETE");
      response.setHeader("Access-Control-Allow-Headers",
          join(",", corsProperties.getAllowedHeaders()));
      response.setHeader("Access-Control-Max-Age", "3600");
      response.setStatus(HttpServletResponse.SC_OK);
    } else {
      chain.doFilter(req, res);
    }
  }

  @Override
  public void destroy() {
  }

  @Override
  public void init(FilterConfig config) throws ServletException {
  }
}
