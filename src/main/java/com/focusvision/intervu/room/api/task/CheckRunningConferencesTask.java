package com.focusvision.intervu.room.api.task;

import com.focusvision.intervu.room.api.adapter.ConferenceCheckSenderAdapter;
import com.focusvision.intervu.room.api.model.entity.Conference;
import com.focusvision.intervu.room.api.model.messaging.RunningConferenceCheck;
import com.focusvision.intervu.room.api.service.ConferenceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduled task for publishing running conferences for checkup.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(
    prefix = "app.tasks.check-running-conferences",
    value = "enabled",
    havingValue = "true")
public class CheckRunningConferencesTask {

  private final ConferenceCheckSenderAdapter adapter;
  private final ConferenceService conferenceService;

  /**
   * Triggers running conferences check task.
   */
  @Scheduled(
      initialDelayString = "${app.tasks.check-running-conferences.initial-delay:10000}",
      fixedRateString = "${app.tasks.check-running-conferences.period}")
  @SchedulerLock(
      name = "CheckRunningConferencesTask",
      lockAtLeastFor = "${app.tasks.check-running-conferences.lock-period-min}",
      lockAtMostFor = "${app.tasks.check-running-conferences.lock-period-max}")
  public void run() {
    log.info("Starting running conferences check task.");

    var conferences = conferenceService.getAllRunningConferences();
    log.info("Found {} running conferences to check.", conferences.size());

    conferences.stream()
        .map(this::map)
        .forEach(this.adapter::send);

    log.info("Finishing running conferences check task.");
  }

  private RunningConferenceCheck map(Conference conference) {
    return new RunningConferenceCheck()
        .setId(conference.getId().toString());
  }
}
