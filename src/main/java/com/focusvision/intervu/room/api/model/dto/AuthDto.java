package com.focusvision.intervu.room.api.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the authentication token details.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("Authentication")
public class AuthDto {

  @ApiModelProperty("JWT authentication token")
  private String token;
}
