package com.focusvision.intervu.room.api.state.stimulus.model;

import com.focusvision.intervu.room.api.common.model.StimulusActionType;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Entity representing stimulus action log.
 */
@Entity
@Table(name = "stimulus_action_logs")
@Getter
@Setter
@Accessors(chain = true)
public class StimulusActionLog {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private UUID stimulusId;

  @Column
  private UUID participantId;

  @Column(name = "start_offset")
  private Long offset;

  @Column
  private Long version;

  @Column
  @Enumerated(EnumType.STRING)
  private StimulusActionType actionType;

  @Column
  private String content;

  @Column
  private Instant addedAt;
}
