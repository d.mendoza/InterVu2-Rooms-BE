package com.focusvision.intervu.room.api.draw.service;

import static com.focusvision.intervu.room.api.common.model.DrawingActionType.CLEAR;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.DRAW;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.REDO;
import static com.focusvision.intervu.room.api.common.model.DrawingActionType.UNDO;
import static com.focusvision.intervu.room.api.common.model.DrawingContextType.STIMULUS;

import com.focusvision.intervu.room.api.draw.adapter.DrawingNotificationSenderAdapter;
import com.focusvision.intervu.room.api.draw.model.DrawingNotificationMessage;
import com.focusvision.intervu.room.api.service.CommunicationChannelService;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawClearEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawRedoEvent;
import com.focusvision.intervu.room.api.stimulus.model.StimulusDrawUndoEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link DrawingNotificationService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DrawingNotificationServiceImpl implements DrawingNotificationService {

  private final DrawingNotificationSenderAdapter senderAdapter;
  private final CommunicationChannelService communicationChannelService;

  @Override
  public void sendDrawNotification(StimulusDrawEvent event) {
    log.debug("Sending stimulus draw notification: {}", event);

    var drawChannel = communicationChannelService.getDrawChannel(event.roomId());
    senderAdapter.send(
        drawChannel,
        DrawingNotificationMessage.builder()
            .participantId(event.participantId())
            .contextId(event.stimulusId())
            .contextType(STIMULUS)
            .type(DRAW)
            .content(event.content())
            .offset(event.offset())
            .build());
  }

  @Override
  public void sendDrawNotification(StimulusDrawUndoEvent event) {
    log.debug("Sending stimulus draw undo notification: {}", event);

    var drawChannel = communicationChannelService.getDrawChannel(event.roomId());
    senderAdapter.send(
        drawChannel,
        DrawingNotificationMessage.builder()
            .participantId(event.participantId())
            .contextId(event.stimulusId())
            .contextType(STIMULUS)
            .type(UNDO)
            .content(event.content())
            .offset(event.offset())
            .build());
  }

  @Override
  public void sendDrawNotification(StimulusDrawRedoEvent event) {
    log.debug("Sending stimulus draw redo notification: {}", event);

    var drawChannel = communicationChannelService.getDrawChannel(event.roomId());
    senderAdapter.send(
        drawChannel,
        DrawingNotificationMessage.builder()
            .participantId(event.participantId())
            .contextId(event.stimulusId())
            .contextType(STIMULUS)
            .type(REDO)
            .content(event.content())
            .offset(event.offset())
            .build());
  }

  @Override
  public void sendDrawNotification(StimulusDrawClearEvent event) {
    log.debug("Sending stimulus draw clear notification: {}", event);

    var drawChannel = communicationChannelService.getDrawChannel(event.roomId());
    senderAdapter.send(
        drawChannel,
        DrawingNotificationMessage.builder()
            .participantId(event.participantId())
            .contextId(event.stimulusId())
            .contextType(STIMULUS)
            .type(CLEAR)
            .content(null)
            .offset(event.offset())
            .build());
  }
}
