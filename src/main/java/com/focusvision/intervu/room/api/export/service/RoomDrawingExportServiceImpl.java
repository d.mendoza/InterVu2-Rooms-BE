package com.focusvision.intervu.room.api.export.service;

import static java.util.stream.Collectors.toMap;

import com.focusvision.intervu.room.api.draw.model.DrawingLog;
import com.focusvision.intervu.room.api.draw.service.DrawingService;
import com.focusvision.intervu.room.api.export.mapper.RoomDrawingLogExportMapper;
import com.focusvision.intervu.room.api.export.model.RoomDrawingLogExportDto;
import com.focusvision.intervu.room.api.model.entity.ResearchSession;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.StimulusService;
import com.focusvision.intervu.room.api.state.RoomOffsetProvider;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of a {@link RoomDrawingExportService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomDrawingExportServiceImpl implements RoomDrawingExportService {

  private final DrawingService drawingService;
  private final RoomOffsetProvider roomOffsetProvider;
  private final ResearchSessionService researchSessionService;
  private final RoomDrawingLogExportMapper drawingLogExportMapper;
  private final StimulusService stimulusService;

  @Override
  public Optional<List<RoomDrawingLogExportDto>> export(String platformId) {
    log.debug("Exporting drawing logs for room with platform ID {}.", platformId);

    return researchSessionService.fetch(platformId)
        .map(this::getLogs);
  }

  private List<RoomDrawingLogExportDto> getLogs(ResearchSession room) {
    var stimulusMap = stimulusService.getAll(room.getId()).stream()
        .collect(toMap(Stimulus::getId, Stimulus::getPlatformId));

    return drawingService.getRoomDrawingLogs(room.getId()).stream()
        .map(log -> map(
            log,
            room.getConference().getRecordingOffset(),
            stimulusMap.get(log.getContextId())))
        .toList();
  }

  private RoomDrawingLogExportDto map(
      DrawingLog drawingLog,
      Integer roomRecordingOffset,
      String stimulusPlatformId) {
    var alignedOffset = roomOffsetProvider
        .alignWithRecordingOffset(drawingLog.getOffset(), roomRecordingOffset);

    return drawingLogExportMapper.mapToDto(drawingLog, alignedOffset, stimulusPlatformId);
  }
}
