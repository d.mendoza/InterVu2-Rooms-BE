package com.focusvision.intervu.room.api.model.event;

/**
 * Enumeration representing all available types of events.
 *
 * @author Branko Ostojic
 */
@Deprecated
public enum EventType {
  /**
   * Represents the participant state related event.
   */
  PARTICIPANT_STATE,
  /**
   * Represents the participant microphone toggle event.
   */
  PARTICIPANT_MICROPHONE_TOGGLE,
  /**
   * Represents the participant removed event.
   */
  PARTICIPANT_REMOVED,
  /**
   * Represents the participant role changed event.
   */
  PARTICIPANT_ROLE_CHANGED,
}
