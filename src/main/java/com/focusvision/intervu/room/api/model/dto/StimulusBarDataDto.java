package com.focusvision.intervu.room.api.model.dto;

import com.focusvision.intervu.room.api.common.model.StimulusType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the stimulus brief data used for stimulus navigation bar.
 *
 * @author Branko Ostojic
 */
@Data
@Accessors(chain = true)
@ApiModel("StimulusBarData")
public class StimulusBarDataDto {

  @ApiModelProperty("Stimulus ID")
  private UUID id;
  @ApiModelProperty("Stimulus name")
  private String name;
  @ApiModelProperty("Stimulus type")
  private StimulusType type;
  @ApiModelProperty("Stimulus thumbnail")
  private String thumbnail;
}
