package com.focusvision.intervu.room.api.state.stimulus.model;

import com.focusvision.intervu.room.api.common.model.StimulusActionType;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;

/**
 * Model representing stimulus action notification message.
 */
@Getter
@Builder
public class StimulusActionNotificationMessage {

  /**
   * ID of room in witch action was made on stimulus.
   */
  private final UUID roomId;
  /**
   * ID of the stimulus on which action is made.
   */
  private final UUID stimulusId;
  /**
   * ID of the participant who made stimulus action.
   */
  private final UUID participantId;
  /**
   * Stimulus action type.
   */
  private final StimulusActionType type;
  /**
   * Content of action.
   */
  private final String content;
}
