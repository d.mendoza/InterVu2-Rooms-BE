package com.focusvision.intervu.room.api.event.stimulus.model;

import com.focusvision.intervu.room.api.model.entity.Stimulus;
import java.util.UUID;

/**
 * Modle for stimulus activated event.
 *
 * @author Branko Ostojic
 */
public record StimulusActivatedEvent(UUID roomId,
                                     Stimulus stimulus) {

}
