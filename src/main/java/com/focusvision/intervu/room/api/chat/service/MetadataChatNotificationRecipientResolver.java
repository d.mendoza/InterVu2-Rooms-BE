package com.focusvision.intervu.room.api.chat.service;

import static java.util.stream.Collectors.toList;

import com.focusvision.intervu.room.api.chat.model.ChatMetadataDto;
import com.focusvision.intervu.room.api.chat.model.ChatSource;
import com.focusvision.intervu.room.api.chat.model.ChatType;
import com.focusvision.intervu.room.api.chat.model.ParticipantChatMetadataDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Chat metadata backed implementation of a {@link ChatNotificationRecipientResolver}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MetadataChatNotificationRecipientResolver
    implements ChatNotificationRecipientResolver {

  private final ChatMetadataService chatMetadataService;

  @Override
  public List<ParticipantChatMetadataDto> resolveRecipients(AuthenticatedParticipant sender,
                                                            UUID recipient,
                                                            ChatSource source,
                                                            ChatType type) {
    log.debug("Resolving {} {} chat notification recipients for sender {} and recipient {}.",
        source, type, sender, recipient);

    return switch (type) {
      case INTERNAL -> getChatMetadataForSource(sender, source).getInternal().getParticipants();
      case RESPONDENTS -> getChatMetadataForSource(sender, source).getRespondents()
          .getParticipants();
      case PRIVATE -> getChatParticipantsMetaForSource(sender, source)
          .stream()
          .filter(isSenderOrRecipient(sender, recipient))
          .toList();
    };
  }

  private ChatMetadataDto getChatMetadataForSource(
      AuthenticatedParticipant caller,
      ChatSource source) {
    return switch (source) {
      case WAITING_ROOM -> chatMetadataService.getWaitingRoomChatMetadata(caller);
      case MEETING_ROOM -> chatMetadataService.getMeetingRoomChatMetadata(caller);
    };
  }

  private List<ParticipantChatMetadataDto> getChatParticipantsMetaForSource(
      AuthenticatedParticipant caller,
      ChatSource source) {
    return switch (source) {
      case WAITING_ROOM -> chatMetadataService.getWaitingRoomParticipantsChatMetadata(caller);
      case MEETING_ROOM -> chatMetadataService.getMeetingRoomParticipantsChatMetadata(caller);
    };
  }

  private Predicate<ParticipantChatMetadataDto> isSenderOrRecipient(AuthenticatedParticipant caller,
                                                                    UUID recipient) {
    return metadata -> metadata.getParticipant().getId().equals(caller.getId())
        || metadata.getParticipant().getId().equals(recipient);
  }
}
