package com.focusvision.intervu.room.api.twilio.service;

import com.focusvision.intervu.room.api.twilio.api.TwilioDialInRequest;
import com.focusvision.intervu.room.api.twilio.model.DialInPin;

/**
 * Service used for Twilio dial in related operations.
 *
 * @author Branko Ostojic
 */
public interface TwilioDialInService {

  /**
   * Process Twilio dial-in request.
   *
   * @param twilioDialInRequest Twilio dial-in request data.
   * @return Dial-in response.
   */
  String processDialIn(TwilioDialInRequest twilioDialInRequest);

  /**
   * Process Twilio dial-in PIN request.
   *
   * @param dialInPin Dial-in PIN data.
   * @return Dial-in PIN response.
   */
  String processDialInPin(DialInPin dialInPin);

}
