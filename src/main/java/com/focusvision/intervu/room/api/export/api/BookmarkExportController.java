package com.focusvision.intervu.room.api.export.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.focusvision.intervu.room.api.exception.ActionForbiddenException;
import com.focusvision.intervu.room.api.exception.ResourceNotFoundException;
import com.focusvision.intervu.room.api.export.model.BookmarkExportDto;
import com.focusvision.intervu.room.api.export.service.BookmarkExportService;
import com.focusvision.intervu.room.api.security.IntervuUser;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for room bookmark data export operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "export/room", produces = APPLICATION_JSON_VALUE)
public class BookmarkExportController {

  private final BookmarkExportService bookmarkExportService;

  /**
   * Exports bookmark room data.
   *
   * @param platformId Room platform ID.
   * @param caller     Authenticated participant.
   * @return Room bookmark export data.
   */
  @GetMapping("{platformId}/bookmarks")
  @ApiOperation(
      value = "Gets room bookmarks.",
      notes = "Gets room bookmarks.")
  public List<BookmarkExportDto> export(@PathVariable String platformId,
                                        @ApiIgnore @AuthenticationPrincipal IntervuUser caller) {
    log.debug("Exporting room {} bookmark data.", platformId);

    if (!caller.isSystemUser()) {
      throw new ActionForbiddenException("Caller is not a system user");
    }

    return bookmarkExportService.export(platformId).orElseThrow(ResourceNotFoundException::new);
  }
}
