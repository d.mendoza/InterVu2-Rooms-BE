package com.focusvision.intervu.room.api.state.stimulus.handler;

import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;

/**
 * Handler for the {@link PollStimulusRankingEvent}.
 */
public interface PollStimulusRankingEventHandler {

  /**
   * Handles the provided stimulus ranking event.
   *
   * @param event Stimulus ranking event.
   */
  void handle(PollStimulusRankingEvent event);
}
