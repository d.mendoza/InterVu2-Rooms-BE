package com.focusvision.intervu.room.api.bookmark.api;

import static org.springframework.http.HttpStatus.CREATED;

import com.focusvision.intervu.room.api.bookmark.mapper.BookmarkMapper;
import com.focusvision.intervu.room.api.bookmark.model.AddBookmarkDto;
import com.focusvision.intervu.room.api.bookmark.model.BookmarkDetailsDto;
import com.focusvision.intervu.room.api.bookmark.service.BookmarkService;
import com.focusvision.intervu.room.api.model.dto.AddPiiBookmarkDto;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for bookmarks related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/bookmarks", produces = MediaType.APPLICATION_JSON_VALUE)
public class BookmarkController {

  private final BookmarkService bookmarkService;
  private final BookmarkMapper bookmarkMapper;

  /**
   * Create bookmark.
   *
   * @param dto    Request data.
   * @param caller Authenticated user.
   * @return Created bookmark details.
   */
  @PostMapping
  @ResponseStatus(CREATED)
  @ApiOperation(
      value = "Creates a bookmark",
      notes = "Creates a bookmark for the running session")
  @PreAuthorize("hasAuthority('BOOKMARK_REGULAR_ADD')")
  public BookmarkDetailsDto create(@RequestBody @Valid AddBookmarkDto dto,
                                   @ApiIgnore @AuthenticationPrincipal
                                   AuthenticatedParticipant caller) {
    log.debug("Creating a bookmark by participant {}", caller.getId());

    var bookmark = bookmarkService
        .addBookmark(caller, dto.getNote(), dto.getTimestamp());
    return bookmarkMapper.mapToDetailsDto(bookmark);
  }

  /**
   * Get all bookmarks.
   *
   * @param caller Authenticated user.
   * @return List of all bookmarks.
   */
  @GetMapping
  @ApiOperation(
      value = "Gets all room bookmarks",
      notes = "Gets all bookmarks for the specific meeting session")
  @PreAuthorize("hasAuthority('BOOKMARK_READ')")
  public List<BookmarkDetailsDto> list(
      @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Getting all bookmarks for session {}", caller.getRoomId());

    return bookmarkService.getAllBookmarks(caller.getRoomId()).stream()
        .map(bookmarkMapper::mapToDetailsDto)
        .toList();
  }

  /**
   * Create PII bookmark.
   *
   * @param dto    Request data.
   * @param caller Authenticated user.
   * @return Created PII bookmark details.
   */
  @PostMapping("pii")
  @ResponseStatus(CREATED)
  @ApiOperation(
      value = "Creates a bookmark",
      notes = "Creates PII bookmark for the running session")
  @PreAuthorize("hasAuthority('BOOKMARK_PII_ADD')")
  public BookmarkDetailsDto createPiiBookmark(@RequestBody @Valid AddPiiBookmarkDto dto,
                                              @ApiIgnore @AuthenticationPrincipal
                                              AuthenticatedParticipant caller) {
    log.debug("Creating PII bookmark by participant {}", caller.getId());

    var bookmark = bookmarkService.addPiiBookmark(caller, dto.getNote(), dto.getTimestamp());

    return bookmarkMapper.mapToDetailsDto(bookmark);
  }

}
