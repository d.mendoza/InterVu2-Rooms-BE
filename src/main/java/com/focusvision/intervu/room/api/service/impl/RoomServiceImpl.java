package com.focusvision.intervu.room.api.service.impl;

import com.focusvision.intervu.room.api.advisor.ClusterLock;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantBannedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantMicrophoneToggleEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantReadyEvent;
import com.focusvision.intervu.room.api.event.participant.publisher.ParticipantBannedEventPublisher;
import com.focusvision.intervu.room.api.event.participant.publisher.ParticipantMicrophoneToggleEventPublisher;
import com.focusvision.intervu.room.api.event.participant.publisher.ParticipantReadyEventPublisher;
import com.focusvision.intervu.room.api.event.room.model.RoomFinishedEvent;
import com.focusvision.intervu.room.api.event.room.model.RoomStartedEvent;
import com.focusvision.intervu.room.api.event.room.publisher.RoomFinishedEventPublisher;
import com.focusvision.intervu.room.api.event.room.publisher.RoomStartedEventPublisher;
import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.ConferenceService;
import com.focusvision.intervu.room.api.service.ParticipantService;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import com.focusvision.intervu.room.api.service.RoomService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of a {@link RoomService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

  private final ParticipantService participantService;
  private final ResearchSessionService researchSessionService;
  private final ConferenceService conferenceService;
  private final ParticipantReadyEventPublisher participantReadyEventPublisher;
  private final ParticipantBannedEventPublisher participantBannedEventPublisher;
  private final ParticipantMicrophoneToggleEventPublisher participantMicrophoneToggleEventPublisher;
  private final RoomStartedEventPublisher roomStartedEventPublisher;
  private final RoomFinishedEventPublisher roomFinishedEventPublisher;

  @Override
  @ClusterLock
  @Transactional
  public void startRoom(UUID roomId, UUID participantId) {
    log.debug("Starting room {} by participant {}", roomId, participantId);

    var room = researchSessionService.get(roomId);
    if (!room.isPending()) {
      throw new IntervuRoomException("Can't start the room.");
    }

    room = researchSessionService.markAsStarted(room.getId());
    var conference = conferenceService.start(room);
    room.setConference(conference);

    roomStartedEventPublisher.publish(new RoomStartedEvent(
        room.getId(),
        room.getPlatformId(),
        room.getConference().getRoomSid(),
        room.getStartedAt(),
        room.getParticipants()));
  }

  @Override
  @ClusterLock
  @Transactional
  public void finish(UUID roomId, UUID participantId) {
    log.debug("Participant {} is finishing room {}.", participantId, roomId);

    this.finish(roomId);
  }

  private void finish(UUID roomId) {
    var room = researchSessionService.markAsFinished(roomId);
    conferenceService.finish(room.getConference().getId());

    roomFinishedEventPublisher.publish(new RoomFinishedEvent(
        room.getId(),
        room.getPlatformId(),
        room.getConference().getRoomSid(),
        room.getStartedAt(),
        room.getEndedAt(),
        room.getParticipants()));
  }

  @Override
  @ClusterLock
  @Transactional
  public void finishIfBroken(UUID roomId) {
    log.debug("Finishing room {} if broken.", roomId);

    var room = researchSessionService.get(roomId);
    if (room.isStarted()
        && conferenceService.shouldConferenceBeClosed(room.getConference().getId())) {
      log.info("Finishing broken room {}.", room.getId());
      this.finish(roomId);
    } else {
      log.debug("Room {} is not broken.", room.getId());
    }
  }

  @Override
  public void joinRoom(UUID roomId, UUID participantId) {
    log.debug("Joining room {} by participant {}", roomId, participantId);

    participantReadyEventPublisher.publish(new ParticipantReadyEvent(roomId, participantId));
  }

  @Override
  @Transactional
  public void banParticipant(AuthenticatedParticipant moderator, UUID participantId) {
    log.debug("Removing participant {} from room {}.", participantId, moderator.getRoomId());

    var roomId = moderator.getRoomId();
    var participant = participantService.findRoomParticipant(roomId, participantId)
        .orElseThrow(
            () -> new IntervuRoomException("Moderator and participant are not in the same room."));

    if (!moderator.isAdmin() && !participant.isBanable()) {
      throw new IntervuRoomException("Only administrators can ban other administrators.");
    }

    participantService.banParticipant(participant.getId());
    var researchSession = researchSessionService.get(roomId);
    if (researchSession.getConference() != null) {
      conferenceService.kickParticipantFromRoom(researchSession.getConference(), participantId);
    }

    participantBannedEventPublisher.publish(new ParticipantBannedEvent(roomId, participantId));
  }

  @Override
  public void toggleParticipantMicrophone(AuthenticatedParticipant moderator, UUID participantId) {
    log.debug("Toggle participant's microphone (roomId: {}, participantId: {})",
        moderator.getRoomId(), participantId);

    var roomId = moderator.getRoomId();
    participantService.findRoomParticipant(roomId, participantId)
        .orElseThrow(
            () -> new IntervuRoomException("Moderator and participant are not in the same room."));

    participantMicrophoneToggleEventPublisher
        .publish(new ParticipantMicrophoneToggleEvent(roomId, participantId));
  }

}
