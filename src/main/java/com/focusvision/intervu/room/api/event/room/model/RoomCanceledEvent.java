package com.focusvision.intervu.room.api.event.room.model;

import java.util.UUID;

/**
 * Model for room canceled event.
 *
 * @author Branko Ostojic
 */
public record RoomCanceledEvent(UUID roomId) {

}
