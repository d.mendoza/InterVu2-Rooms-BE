package com.focusvision.intervu.room.api.listener;

import static com.focusvision.intervu.room.api.security.PrincipalHelper.extractParticipant;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedToRoomEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedEvent;
import com.focusvision.intervu.room.api.event.participant.publisher.ParticipantPresenceEventPublisher;
import com.focusvision.intervu.room.api.security.WebSocketHelper;
import com.focusvision.intervu.room.api.security.WebSocketHelper.SubscriptionInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

/**
 * Service responsible for handling various WS events.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class WebSocketEventListener {

  private final WebSocketHelper wsHelper;
  private final ParticipantPresenceEventPublisher participantPresenceEventPublisher;

  /**
   * Handles session connected event.
   *
   * @param event Session connected event data.
   */
  @EventListener
  public void handleWebSocketConnectionListener(SessionConnectedEvent event) {
    log.info("Received a new web socket connection: {}", event);
  }

  /**
   * Handles session disconnected event.
   *
   * @param event Session disconnected event data.
   */
  @EventListener
  public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
    log.info("Received a socket disconnect event: {}", event);

    extractParticipant(event.getUser())
        .map(
            participant ->
                new ParticipantDisconnectedEvent(participant.getRoomId(), participant.getId()))
        .ifPresent(participantPresenceEventPublisher::publish);
  }

  /**
   * Handles session subscribed event.
   *
   * @param event Session subscribed event data.
   */
  @EventListener
  public void handleWebSocketSubscribeListener(SessionSubscribeEvent event) {
    log.debug("Received a new web socket subscribe request: {}", event);

    StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
    SubscriptionInfo subscriptionInfo =
        wsHelper.getSubscriptionInfo(headerAccessor.getDestination());
    switch (subscriptionInfo.getType()) {
      case ROOM:
        extractParticipant(event.getUser())
            .map(
                participant ->
                    new ParticipantConnectedToRoomEvent(
                        participant.getRoomId(), participant.getId()))
            .ifPresent(participantPresenceEventPublisher::publish);
        break;
      case PARTICIPANT:
        // TODO: check is this event needed or we can notify only on room subscription
        extractParticipant(event.getUser())
            .map(
                participant ->
                    new ParticipantConnectedEvent(participant.getRoomId(), participant.getId()))
            .ifPresent(participantPresenceEventPublisher::publish);
        break;
      case CHAT:
      case DRAW:
      case STIMULUS:
      case STIMULUS_GLOBAL:
      case UNKNOWN:
      default:
    }
    // TODO: log subscription details ...
    // TODO: maybe add some table for keeping indication of participants subscriptions?
  }

  /**
   * Handles session unsubscribed event.
   *
   * @param event Session unsubscribed event data.
   */
  @EventListener
  public void handleWebSocketUnsubscribeListener(SessionUnsubscribeEvent event) {
    log.debug("Received a web socket unsubscribe request: {}", event);

    // TODO: log subscription details ...
    // TODO: maybe add some table for keeping indication of participants subscriptions?
  }
}
