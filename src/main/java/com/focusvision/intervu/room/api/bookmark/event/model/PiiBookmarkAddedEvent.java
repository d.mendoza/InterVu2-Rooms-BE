package com.focusvision.intervu.room.api.bookmark.event.model;

import com.focusvision.intervu.room.api.bookmark.model.PiiBookmarkDetails;
import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import java.util.UUID;

/**
 * Model representing the PII bookmark added event.
 */
public record PiiBookmarkAddedEvent(
    UUID roomId,
    AuthenticatedParticipant participant,
    PiiBookmarkDetails bookmark) {
}
