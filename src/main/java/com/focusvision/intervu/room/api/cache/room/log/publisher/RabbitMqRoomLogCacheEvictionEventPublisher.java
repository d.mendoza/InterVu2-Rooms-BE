package com.focusvision.intervu.room.api.cache.room.log.publisher;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.cache.room.log.event.ClearRoomLogCacheEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ specific implementation of a {@link RoomLogCacheEvictionEventPublisher}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqRoomLogCacheEvictionEventPublisher
    implements RoomLogCacheEvictionEventPublisher {

  private final RabbitTemplate template;
  private final FanoutExchange clearRoomLogCacheExchange;

  @Override
  public void publish(ClearRoomLogCacheEvent event) {
    log.debug("Publishing clear room log cache event: {}", event);

    try {
      template.convertAndSend(clearRoomLogCacheExchange.getName(), "", event);
    } catch (MessagingException e) {
      log.error(format("Error sending clear room log cache event %s", event), e);
    }
  }
}
