package com.focusvision.intervu.room.api.messaging;

import static java.lang.String.format;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

/**
 * Producer for messages going to RabbitMQ message broker.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class RabbitMqProducer {

  private final RabbitTemplate rabbitTemplate;

  /**
   * Sends provided message to RabbitMQ.
   *
   * @param destination Message destination.
   * @param message     Message payload.
   */
  public void send(String destination, Object message) {
    log.debug("Sending message {} to {}", message, destination);

    try {
      rabbitTemplate.convertAndSend(destination, message);
    } catch (MessagingException e) {
      log.error(format("Error sending message [%s] to %s.", message, destination), e);
    }
  }

}
