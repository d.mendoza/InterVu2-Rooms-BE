package com.focusvision.intervu.room.api.repository;

import com.focusvision.intervu.room.api.common.model.ChatType;
import com.focusvision.intervu.room.api.model.entity.ChatMessage;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for the {@link ChatMessage} entity.
 *
 * @author Branko Ostojic
 */
public interface ChatMessageRepository extends JpaRepository<ChatMessage, UUID> {

  /**
   * Gets chat messages by handle.
   *
   * @param handle Chat message handle.
   * @return Matching chat messages.
   */
  List<ChatMessage> findAllByHandle(UUID handle);

  /**
   * Gets chat messages by handle, type and sources.
   *
   * @param handle  Chat message handle.
   * @param type    Chat message type.
   * @param sources Chat message sources.s
   * @return Matching chat messages.
   */
  List<ChatMessage> findAllByHandleAndTypeAndSourceIsInOrderBySentAtAsc(
      UUID handle,
      ChatType type,
      List<ChatMessage.ChatSource> sources);

  /**
   * Gets private chat messages by type and sources and participants.
   *
   * @param firstParticipant  First participant.
   * @param secondParticipant Second participant.
   * @param type              Chat message type.
   * @param sources           Chat message sources.s
   * @return Matching chat messages.
   */
  @Query("""
      SELECT cm FROM ChatMessage cm
      WHERE (cm.senderId = :firstParticipant AND cm.handle = :secondParticipant)
      OR (cm.senderId = :secondParticipant AND cm.handle = :firstParticipant)
      AND cm.type = :type
      AND cm.source IN :sources
      ORDER BY cm.sentAt ASC
      """)
  List<ChatMessage> findAll(@Param("firstParticipant") UUID firstParticipant,
                            @Param("secondParticipant") UUID secondParticipant,
                            @Param("type") ChatType type,
                            @Param("sources") List<ChatMessage.ChatSource> sources);

  /**
   * Gets chat messages by recipient platform id.
   *
   * @param id Recipient platform id.
   * @return Matching chat messages.
   */
  List<ChatMessage> findAllBySenderPlatformId(String id);

  /**
   * Deletes all research session chat messages for specific sender.
   *
   * @param senderPlatformId  Sender platform ID.
   * @param researchSessionId Research session ID.
   */
  void deleteAllBySenderPlatformIdAndResearchSessionId(String senderPlatformId,
                                                       UUID researchSessionId);

  /**
   * Gets chat messages by handle and sender platform ID.
   *
   * @param handle      Chat handle.
   * @param platformId  Sender platform ID.
   * @param messageType Message type.
   * @return All matching messages.
   */
  List<ChatMessage> findAllByHandleAndSenderPlatformIdAndMessageType(
      UUID handle,
      String platformId,
      ChatMessage.MessageType messageType);
}
