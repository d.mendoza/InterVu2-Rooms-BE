package com.focusvision.intervu.room.api.participant.testing.model;

import lombok.Builder;
import lombok.Getter;

/**
 * Model representing the participant testing results.
 */
@Getter
@Builder
public class ParticipantTestingResult {

  /**
   * Testing outcome indication.
   */
  private final boolean success;
  /**
   * Participant platform ID.
   */
  private final String respondentId;
  /**
   * Research session platform ID.
   */
  private final String sessionId;

}
