package com.focusvision.intervu.room.api.draw.model;

import com.focusvision.intervu.room.api.common.model.DrawingActionType;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * DTO representing the drawing log.
 *
 * @author Branko Ostojic
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class DrawingLogDto {

  /**
   * ID of the participant who is drawing.
   */
  private UUID participantId;

  /**
   * Drawing action type (e.g. draw, undo, redo, clear).
   */
  private DrawingActionType type;

  /**
   * Drawing action content.
   */
  private String content;
}
