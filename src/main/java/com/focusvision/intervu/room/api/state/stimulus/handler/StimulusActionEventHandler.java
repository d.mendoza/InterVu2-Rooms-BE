package com.focusvision.intervu.room.api.state.stimulus.handler;

import com.focusvision.intervu.room.api.state.stimulus.StimulusActionOperator;
import com.focusvision.intervu.room.api.state.stimulus.model.DocumentStimulusPageChangeEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusRankingEvent;
import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusVoteEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * {@link ApplicationEvent} specific implementation of a stimulus action events handler.
 */
@Slf4j
@Async
@Service
@RequiredArgsConstructor
public class StimulusActionEventHandler implements
    PollStimulusVoteEventHandler,
    PollStimulusRankingEventHandler,
    PollStimulusAnswerEventHandler,
    DocumentStimulusPageChangeEventHandler {

  private final StimulusActionOperator stimulusStateOperator;

  @Override
  @EventListener
  public void handle(PollStimulusVoteEvent event) {
    log.info("Handling poll stimulus vote event: {}.", event);

    stimulusStateOperator.vote(event.getRoomId(),
        event.getParticipantId(),
        event.getStimulusId(),
        event.getContent(),
        event.getOffset());
  }

  @Override
  @EventListener
  public void handle(PollStimulusRankingEvent event) {
    log.info("Handling poll stimulus ranking event: {}.", event);

    stimulusStateOperator.rank(event.getRoomId(),
        event.getParticipantId(),
        event.getStimulusId(),
        event.getContent(),
        event.getOffset());
  }

  @Override
  @EventListener
  public void handle(PollStimulusAnswerEvent event) {
    log.info("Handling poll stimulus answer event: {}.", event);

    stimulusStateOperator.answer(event.getRoomId(),
        event.getParticipantId(),
        event.getStimulusId(),
        event.getContent(),
        event.getOffset());
  }

  @Override
  @EventListener
  public void handle(DocumentStimulusPageChangeEvent event) {
    log.info("Handling document stimulus page change event [{}]", event);

    stimulusStateOperator.changePage(event.getRoomId(),
        event.getParticipantId(),
        event.getStimulusId(),
        event.getContent(),
        event.getOffset());
  }
}
