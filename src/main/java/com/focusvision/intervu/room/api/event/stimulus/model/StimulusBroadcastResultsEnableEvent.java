package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimulus broadcast results enable event.
 */
public record StimulusBroadcastResultsEnableEvent(UUID roomId,
                                                  UUID stimulusId) {

}
