package com.focusvision.intervu.room.api.event.participant.publisher;

import com.focusvision.intervu.room.api.event.participant.model.ParticipantBannedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedToRoomEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantConnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDisconnectedWithPhoneEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingDisabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantDrawingEnabledEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantMicrophoneToggleEvent;
import com.focusvision.intervu.room.api.event.participant.model.ParticipantReadyEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * {@link ApplicationEventPublisher} specific implementation of a participant event publisher.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ParticipantEventPublisher implements
        ParticipantPresenceEventPublisher,
        ParticipantReadyEventPublisher,
        ParticipantBannedEventPublisher,
        ParticipantMicrophoneToggleEventPublisher {

  private final ApplicationEventPublisher publisher;

  @Override
  public void publish(ParticipantConnectedEvent event) {
    log.debug("Publishing participant connected event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantConnectedToRoomEvent event) {
    log.debug("Publishing participant connected to room event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantDisconnectedEvent event) {
    log.debug("Publishing participant disconnected event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantConnectedWithPhoneEvent event) {
    log.debug("Publishing participant connected with phone event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantDisconnectedWithPhoneEvent event) {
    log.debug("Publishing participant disconnected with phone event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantBannedEvent event) {
    log.debug("Publishing participant banned event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantReadyEvent event) {
    log.debug("Publishing participant ready event: {}.", event);

    publisher.publishEvent(event);
  }

  @Override
  public void publish(ParticipantMicrophoneToggleEvent event) {
    log.debug("Publishing participant microphone toggle event: {}.", event);

    publisher.publishEvent(event);
  }

}
