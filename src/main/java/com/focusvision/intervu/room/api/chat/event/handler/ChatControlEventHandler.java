package com.focusvision.intervu.room.api.chat.event.handler;

import com.focusvision.intervu.room.api.chat.event.model.RespondentsChatDisabledEvent;
import com.focusvision.intervu.room.api.chat.event.model.RespondentsChatEnabledEvent;

/**
 * Handler for the {@link RespondentsChatEnabledEvent}.
 *
 * @author Branko Ostojic
 */
public interface ChatControlEventHandler {

  /**
   * Handles the provided respondents chat enabled event.
   *
   * @param event Respondents chat enabled event.
   */
  void handle(RespondentsChatEnabledEvent event);

  /**
   * Handles the provided respondents chat disabled event.
   *
   * @param event Respondents chat disabled event.
   */
  void handle(RespondentsChatDisabledEvent event);
}
