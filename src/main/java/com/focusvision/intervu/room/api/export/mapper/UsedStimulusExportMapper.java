package com.focusvision.intervu.room.api.export.mapper;

import static org.mapstruct.ReportingPolicy.ERROR;

import com.focusvision.intervu.room.api.export.model.UsedStimulusExportDto;
import com.focusvision.intervu.room.api.model.entity.Stimulus;
import org.mapstruct.Mapper;

/**
 * Mapper for {@link UsedStimulusExportDto}.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ERROR)
public interface UsedStimulusExportMapper {

  /**
   * Maps {@link Stimulus} to {@link UsedStimulusExportDto} for export.
   *
   * @param stimulus Stimulus.
   * @return Resulting DTO.
   */
  UsedStimulusExportDto mapToDto(Stimulus stimulus);
}
