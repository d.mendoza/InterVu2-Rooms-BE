package com.focusvision.intervu.room.api.adapter.impl;

import static java.lang.String.format;
import static java.util.UUID.fromString;

import com.focusvision.intervu.room.api.adapter.RecordingCheckDataInputAdapter;
import com.focusvision.intervu.room.api.advisor.ClusterLock;
import com.focusvision.intervu.room.api.model.messaging.ProcessingRecordingCheck;
import com.focusvision.intervu.room.api.recording.service.RecordingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link RecordingCheckDataInputAdapter}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RecordingCheckDataInputAdapterImpl implements RecordingCheckDataInputAdapter {

  private final RecordingService recordingService;

  @Override
  @ClusterLock
  public void process(ProcessingRecordingCheck processingRecordingCheck) {
    log.debug("Processing recording check incoming data: [{}]", processingRecordingCheck);

    try {
      recordingService.checkProcessingRecording(fromString(processingRecordingCheck.getId()));
    } catch (Exception e) {
      log.error(format("Error processing recording check %s", processingRecordingCheck), e);
    }
  }
}
