package com.focusvision.intervu.room.api.event.stimulus.model;

import java.util.UUID;

/**
 * Model for stimuli focus enabled event.
 */
public record StimuliFocusEnabledEvent(UUID roomId) {

}
