package com.focusvision.intervu.room.api.state.stimulus.handler;

import com.focusvision.intervu.room.api.state.stimulus.model.PollStimulusAnswerEvent;

/**
 * Handler for the {@link PollStimulusAnswerEvent}.
 */
public interface PollStimulusAnswerEventHandler {

  /**
   * Handles the provided stimulus answer event.
   *
   * @param event Stimulus answer event.
   */
  void handle(PollStimulusAnswerEvent event);
}
