package com.focusvision.intervu.room.api.role;

import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import com.focusvision.intervu.room.api.model.entity.Participant;
import java.util.Set;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;

/**
 * Participant alternative roles data.
 */
@Getter
@Builder
public class ParticipantAlternativeRolesDto {

  /**
   * Participant ID.
   */
  private final UUID participantId;

  /**
   * Participant display name.
   */
  private final String displayName;

  /**
   * Participant current role.
   */
  private final ParticipantRole currentRole;

  /**
   * Participant alternative roles.
   */
  private final Set<ParticipantRole> alternativeRoles;

}
