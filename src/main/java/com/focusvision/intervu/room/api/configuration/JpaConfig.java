package com.focusvision.intervu.room.api.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Configuration for JPA.
 */
@Configuration
@EnableJpaAuditing
public class JpaConfig {
}
