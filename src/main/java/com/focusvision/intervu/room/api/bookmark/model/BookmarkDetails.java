package com.focusvision.intervu.room.api.bookmark.model;

import java.time.Instant;
import java.util.UUID;

/**
 * Model representing the bookmark details.
 *
 * @author Branko Ostojic
 */
public record BookmarkDetails(
    UUID id,
    String note,
    String addedBy,
    Instant addedAt,
    Long offset,
    String addedByDisplayName
) {
}
