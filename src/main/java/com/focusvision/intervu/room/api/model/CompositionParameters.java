package com.focusvision.intervu.room.api.model;

import com.focusvision.intervu.room.api.common.model.AudioChannel;

/**
 * Data representing parameters needed for twilio composition creation.
 *
 * @param layout       Composition Layout.
 * @param audioChannel Audio channel.
 */
public record CompositionParameters(CompositionLayout layout, AudioChannel audioChannel) {

}
