package com.focusvision.intervu.room.api.model.event;

import static com.focusvision.intervu.room.api.event.stimulus.model.StimulusGlobalDataType.ACTION;

import com.focusvision.intervu.room.api.event.stimulus.model.StimulusGlobalDataType;
import com.focusvision.intervu.room.api.model.dto.StimulusGlobalActionDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Model representing the stimulus global action event.
 */
@Getter
@Setter
@Builder
@ToString
@ApiModel("StimulusGlobalActionDataEvent")
public class StimulusGlobalActionDataEvent {

  @ApiModelProperty("Event Type")
  private final StimulusGlobalDataType eventType = ACTION;
  /**
   * Event destination.
   */
  @ApiModelProperty("Event destination")
  private final String destination;
  /**
   * Event data payload.
   */
  @ApiModelProperty("Event data")
  private final StimulusGlobalActionDto data;

}
