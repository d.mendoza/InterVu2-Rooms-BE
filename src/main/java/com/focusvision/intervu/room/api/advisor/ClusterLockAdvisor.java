package com.focusvision.intervu.room.api.advisor;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.exception.IntervuRoomException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

/**
 * Implementation of a {@link ClusterLock}. If method annotated with the {@link ClusterLock} has
 * argument implementing {@link Lockable} then that arg will be used for group level locking. If no
 * such argument is provided, then the method will be locked on a cluster level.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class ClusterLockAdvisor {

  private final RedissonClient redissonClient;

  /**
   * Applies a cluster lock.
   *
   * @param joinPoint Join point.
   * @return Resulting object.
   * @throws Throwable in case of anny errors.
   */
  @Around("@annotation(ClusterLock)")
  public Object applyLock(ProceedingJoinPoint joinPoint) throws Throwable {
    String lockKey = this.extractLockKey(joinPoint);

    final RLock lock = redissonClient.getFairLock(lockKey);
    try {
      log.debug("Try to lock {}", lockKey);
      boolean acquired = lock.tryLock(5, 5, TimeUnit.SECONDS);
      if (!acquired) {
        throw new IntervuRoomException("Error waiting for the lock");
      }

      log.debug("Locked {}", lockKey);
      return joinPoint.proceed();
    } finally {
      if (lock.isHeldByCurrentThread()) {
        lock.unlock();
        log.debug("Unlocked {}", lockKey);
      }
    }
  }

  private String extractLockKey(ProceedingJoinPoint joinPoint) {
    return Arrays.stream(joinPoint.getArgs())
        .filter(arg -> arg instanceof Lockable)
        .map(arg -> (Lockable) arg)
        .map(lockable -> format("%s-%s", lockable.lockGroup(), lockable.lockKey()))
        .findFirst()
        .orElse(
            format("%s-%s",
                joinPoint.getSignature().getDeclaringType().getSimpleName(),
                joinPoint.getSignature().getName()));
  }

}
