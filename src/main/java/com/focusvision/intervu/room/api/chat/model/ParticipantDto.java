package com.focusvision.intervu.room.api.chat.model;

import static com.focusvision.intervu.room.api.common.model.ParticipantRole.MODERATOR;
import static com.focusvision.intervu.room.api.common.model.ParticipantRole.RESPONDENT;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.focusvision.intervu.room.api.common.model.ParticipantRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DTO representing the participant chat metadata.
 */
@Data
@Accessors(chain = true)
@ApiModel("Participant data")
public class ParticipantDto {
  @ApiModelProperty("Participant ID")
  private UUID id;
  @ApiModelProperty("Participant name for chat identification")
  private String name;
  @ApiModelProperty("Participant role for chat identification")
  private ParticipantRole role;
  @ApiModelProperty("Is participant online")
  private boolean online;
  @ApiModelProperty("Is participant admin")
  private boolean admin;
  @ApiModelProperty("Is participant a project manager")
  private boolean projectManager;
  @ApiModelProperty("Is participant a project operator")
  private boolean projectOperator;

  @JsonIgnore
  public boolean isRespondent() {
    return RESPONDENT.equals(role);
  }

  @JsonIgnore
  public boolean isModerator() {
    return MODERATOR.equals(role);
  }

}
