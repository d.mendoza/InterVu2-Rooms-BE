package com.focusvision.intervu.room.api.adapter.impl;

import static com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult.fail;
import static com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult.success;
import static java.lang.String.format;

import com.focusvision.intervu.room.api.adapter.ResearchSessionDataInputAdapter;
import com.focusvision.intervu.room.api.advisor.ClusterLock;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionData;
import com.focusvision.intervu.room.api.model.messaging.ResearchSessionProcessResult;
import com.focusvision.intervu.room.api.service.ResearchSessionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ResearchSessionDataInputAdapter}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ResearchSessionDataInputAdapterImpl implements ResearchSessionDataInputAdapter {

  private final ResearchSessionService researchSessionService;

  @Override
  @ClusterLock
  public ResearchSessionProcessResult process(ResearchSessionData researchSessionData) {
    log.debug("Processing research session incoming data: {}", researchSessionData);

    try {
      researchSessionService.createOrUpdate(researchSessionData);
      return success(researchSessionData);
    } catch (Exception e) {
      log.error(format("Error processing research session data %s", researchSessionData), e);
      return fail(researchSessionData, e.toString());
    }
  }
}
