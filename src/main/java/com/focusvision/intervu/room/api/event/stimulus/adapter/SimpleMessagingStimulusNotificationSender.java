package com.focusvision.intervu.room.api.event.stimulus.adapter;

import static java.lang.String.format;

import com.focusvision.intervu.room.api.model.event.PollStimulusBroadcastResultsDataEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * {@link SimpMessagingTemplate} specific implementation of a {@link
 * PollStimulusBroadcastResultsNotificationSenderAdapter}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SimpleMessagingStimulusNotificationSender
    implements PollStimulusBroadcastResultsNotificationSenderAdapter {

  private final SimpMessagingTemplate messagingTemplate;

  @Override
  public void send(PollStimulusBroadcastResultsDataEvent event) {
    log.debug("Sending stimulus broadcast results event [{}].", event);

    try {
      messagingTemplate.convertAndSend(event.getDestination(), event);
    } catch (MessagingException e) {
      log.error(format("Error sending stimulus broadcast results message [%s]", event), e);
    }
  }
}
