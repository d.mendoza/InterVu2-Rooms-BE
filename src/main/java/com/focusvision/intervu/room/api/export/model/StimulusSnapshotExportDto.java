package com.focusvision.intervu.room.api.export.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.Value;

/**
 * DTO representing the stimulus snapshot export.
 */
@Value
@ApiModel("Stimulus Snapshot Export")
public class StimulusSnapshotExportDto {

  @ApiModelProperty("Stimulus ID")
  UUID stimulusId;

  @ApiModelProperty("Snapshot name")
  String name;

  @ApiModelProperty("Snapshot data mime type")
  String mimeType;

  @ApiModelProperty("Snapshot data - Base64 encoded")
  String data;

  @ApiModelProperty("Snapshot offset form session start")
  Long offset;

}
