package com.focusvision.intervu.room.api.state.stimulus.model;

import java.util.UUID;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Model representing poll stimulus action message.
 */
@Data
@ToString
@NoArgsConstructor
@Accessors(chain = true)
public class PollStimulusActionMessage {

  /**
   * ID of the stimulus on which action is performed.
   */
  @NotNull
  private UUID stimulusId;

  /**
   * Poll stimulus action content.
   */
  @NotEmpty
  private String content;

  /**
   * Poll stimulus action UTC timestamp.
   */
  @NotNull
  private Long timestamp;
}
