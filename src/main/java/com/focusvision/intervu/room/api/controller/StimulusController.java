package com.focusvision.intervu.room.api.controller;

import com.focusvision.intervu.room.api.security.AuthenticatedParticipant;
import com.focusvision.intervu.room.api.service.StimulusService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controller responsible for stimuli related operations.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "room/stimuli/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
public class StimulusController {

  private final StimulusService stimulusService;

  /**
   * Enables stimulus broadcast.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @PutMapping("broadcast/enable")
  @ApiOperation(
      value = "Enable broadcast stimulus results",
      notes = "Enable broadcast stimulus results")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void enableBroadcast(@ApiParam("Stimulus ID") @PathVariable String id,
                              @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enable broadcast stimulus {} results.", id);

    stimulusService.enableBroadcastResults(caller.getRoomId(), UUID.fromString(id));
  }

  /**
   * Disable stimulus broadcast.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @PutMapping("broadcast/disable")
  @ApiOperation(
      value = "Disable broadcast stimulus results",
      notes = "Disable broadcast stimulus results")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void disableBroadcast(@ApiParam("Stimulus ID") @PathVariable String id,
                               @ApiIgnore @AuthenticationPrincipal
                                   AuthenticatedParticipant caller) {
    log.debug("Disable broadcast stimulus {} results.", id);

    stimulusService.disableBroadcastResults(caller.getRoomId(), UUID.fromString(id));
  }

  /**
   * Enables stimulus sync.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated partcipant.
   */
  @PutMapping("sync/enable")
  @ApiOperation(
      value = "Enable stimulus sync",
      notes = "Enable stimulus sync")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void enableSync(@ApiParam("Stimulus ID") @PathVariable String id,
                         @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Enable stimulus {} sync.", id);

    stimulusService.enableStimulusSync(caller.getRoomId(), UUID.fromString(id));
  }

  /**
   * Disables stimulus sync.
   *
   * @param id     Stimulus ID.
   * @param caller Authenticated participant.
   */
  @PutMapping("sync/disable")
  @ApiOperation(
      value = "Disable stimulus sync",
      notes = "Disable stimulus sync")
  @PreAuthorize("hasAuthority('STIMULI_CONTROL')")
  public void disableSync(@ApiParam("Stimulus ID") @PathVariable String id,
                          @ApiIgnore @AuthenticationPrincipal AuthenticatedParticipant caller) {
    log.debug("Disable stimulus {} sync.", id);

    stimulusService.disableStimulusSync(caller.getRoomId(), UUID.fromString(id));
  }

}
