package com.focusvision.intervu.room.api.model.messaging;

import com.focusvision.intervu.room.api.common.model.AudioChannel;
import com.focusvision.intervu.room.api.common.model.ResearchSessionState;
import com.focusvision.intervu.room.api.model.CompositionLayout;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * DTO representing the outgoing research session state data.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ResearchSessionStateData {

  /**
   * Research session platform ID.
   */
  private String id;
  /**
   * Research session state.
   */
  private ResearchSessionState state;
  /**
   * Research session start time.
   */
  private Instant startedAt;
  /**
   * Research session end time.
   */
  private Instant endedAt;
  /**
   * Research session conference ID.
   */
  private String conferenceId;
  /**
   * Research session conference recording ID.
   */
  private String recordingId;
  /**
   * Research session conference recording ID.
   */
  private boolean error = false;
  /**
   * Research session conference composition layout.
   */
  private CompositionLayout compositionLayout;
  /**
   * Research session conference audio channel.
   */
  private AudioChannel audioChannel;

}
