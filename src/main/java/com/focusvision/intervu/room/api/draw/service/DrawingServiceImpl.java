package com.focusvision.intervu.room.api.draw.service;

import static java.time.Instant.now;

import com.focusvision.intervu.room.api.common.model.DrawingActionType;
import com.focusvision.intervu.room.api.common.model.DrawingContextType;
import com.focusvision.intervu.room.api.draw.model.DrawingLog;
import com.focusvision.intervu.room.api.draw.repository.DrawingRepository;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link DrawingService}.
 *
 * @author Branko Ostojic
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DrawingServiceImpl implements DrawingService {

  private final DrawingRepository drawingRepository;

  @Override
  public DrawingLog saveStimulusDrawingAction(UUID roomId,
                                              UUID participantId,
                                              UUID stimulusId,
                                              DrawingActionType actionType,
                                              Long offset,
                                              String content) {
    log.debug("Persisting stimulus {} draw {} action: {}", stimulusId, actionType, content);

    var drawing = new DrawingLog()
        .setRoomId(roomId)
        .setParticipantId(participantId)
        .setOffset(offset)
        .setActionType(actionType)
        .setContextType(DrawingContextType.STIMULUS)
        .setContextId(stimulusId)
        .setContent(content)
        .setAddedAt(now());

    return drawingRepository.save(drawing);
  }

  @Override
  public List<DrawingLog> getRoomDrawingLogs(UUID roomId) {
    log.debug("Fetching all drawing logs for room {}.", roomId);

    return drawingRepository.findAllByRoomIdOrderByOffsetAscAddedAtAsc(roomId);
  }

  @Override
  public List<DrawingLog> getRoomDrawingLogs(UUID roomId, UUID contextId) {
    log.debug("Fetching all drawing logs for room {} and context {}.", roomId, contextId);

    return drawingRepository.findAllByRoomIdAndContextIdOrderByOffsetAscAddedAtAsc(roomId,
        contextId);
  }
}
