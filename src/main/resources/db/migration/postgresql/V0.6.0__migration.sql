-- DB migration for application version 0.6.0

ALTER TABLE conferences
    ADD COLUMN room_duration      INT                   NOT NULL          DEFAULT 0,
    ADD COLUMN recording_state    CHARACTER VARYING(20) NOT NULL
        CHECK (recording_state IN ('STARTED', 'PROCESSING', 'COMPLETED')) DEFAULT 'STARTED',
    ADD COLUMN recording_offset   INT                   NOT NULL          DEFAULT 0,
    ADD COLUMN recording_duration INT                   NOT NULL          DEFAULT 0;

UPDATE conferences
SET recording_state = 'PROCESSING'
FROM research_sessions
WHERE conferences.research_session_id = research_sessions.id AND research_sessions.ended_at IS NOT NULL;

ALTER TABLE research_sessions
    ADD COLUMN privacy BOOLEAN NOT NULL DEFAULT FALSE;
