-- DB rollback for application version 0.19.0

ALTER TABLE participants
    DROP COLUMN guest,
    DROP COLUMN project_manager,
    DROP COLUMN project_operator;

DELETE
FROM flyway_schema_history
WHERE version = '0.19.0';
