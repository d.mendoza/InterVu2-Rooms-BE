-- Rollback for DI-971 Stimulus snapshot model

DROP TABLE stimulus_snapshots;

DELETE
FROM flyway_schema_history
WHERE version = '0.45.0';
