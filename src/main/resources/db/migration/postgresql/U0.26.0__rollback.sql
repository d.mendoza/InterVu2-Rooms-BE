-- DB rollback for application version 0.26.0

ALTER TABLE chat_messages
    DROP CONSTRAINT chat_messages_message_type_check,
    ADD CONSTRAINT chat_messages_message_type_check CHECK (message_type IN ('REGULAR', 'BOOKMARK'));

DELETE FROM bookmarks WHERE type = 'PII';
ALTER TABLE bookmarks DROP COLUMN type;

DELETE
FROM flyway_schema_history
WHERE version = '0.26.0';
