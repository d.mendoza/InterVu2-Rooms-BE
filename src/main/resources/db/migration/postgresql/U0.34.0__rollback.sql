-- Rollback for IV-4059 Define composition layout based on stimuli usage in room

ALTER TABLE research_sessions
  ADD COLUMN composition_layout CHARACTER VARYING(50)
    CHECK (composition_layout IN ('GRID', 'ONE_ROW_GRID', 'PICTURE_IN_PICTURE',
    'MAIN_VIDEO_WITH_BOTTOM_ROW', 'PICTURE_IN_PICTURE_WITH_COLUMN', 'MOSAIC', 'CHESS_TABLE'));
UPDATE research_sessions rs
    SET composition_layout =
    (SELECT composition_layout FROM conferences c WHERE c.research_session_id = rs.id);
UPDATE research_sessions
  SET composition_layout = 'GRID' WHERE composition_layout IS NULL;
ALTER TABLE research_sessions
  ALTER COLUMN composition_layout SET NOT NULL;
ALTER TABLE conferences
  DROP COLUMN composition_layout;

DELETE
FROM flyway_schema_history
WHERE version = '0.34.0';
