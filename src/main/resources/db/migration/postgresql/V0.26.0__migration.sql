-- DB migration for application version 0.26.0

ALTER TABLE bookmarks ADD COLUMN type CHARACTER VARYING (20);
UPDATE bookmarks SET type = 'NOTE';
ALTER TABLE bookmarks ALTER COLUMN type SET NOT NULL;
ALTER TABLE bookmarks
    ADD CONSTRAINT bookmarks_type_check
        CHECK (type IN ('NOTE', 'PII'));

ALTER TABLE chat_messages
    DROP CONSTRAINT chat_messages_message_type_check,
    ADD CONSTRAINT chat_messages_message_type_check CHECK (message_type IN ('REGULAR', 'BOOKMARK', 'PII'));
