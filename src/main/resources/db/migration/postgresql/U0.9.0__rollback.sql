-- DB rollback for application version 0.9.0

DELETE
FROM flyway_schema_history
WHERE version = '0.9.0';
