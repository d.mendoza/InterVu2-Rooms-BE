-- DB rollback for application version 0.16.0

DROP TABLE stimulus_action_states;
DROP TABLE stimulus_action_logs;

ALTER TABLE room_state_logs
    DROP CONSTRAINT room_state_logs_change_type_check;

DELETE FROM room_state_logs
WHERE change_type IN (
            'STIMULUS_BROADCAST_ENABLED',
            'STIMULUS_BROADCAST_DISABLED',
            'STIMULUS_INTERACTION_ENABLED',
            'STIMULUS_INTERACTION_DISABLED',
            'STIMULUS_RESET'
        );

ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED'));

DELETE
FROM flyway_schema_history
WHERE version = '0.16.0';
