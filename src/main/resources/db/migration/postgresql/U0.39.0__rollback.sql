-- Rollback for DI-509 implement recording layout resolver

ALTER TABLE conferences
ADD COLUMN room_duration      INT                                              NOT NULL DEFAULT 0,
ADD COLUMN recording_duration INT                                              NOT NULL DEFAULT 0,
ADD COLUMN recording_state    CHARACTER VARYING(50)
    CHECK (recording_state IN ('STARTED', 'PROCESSING', 'COMPLETED', 'ERROR')) NOT NULL DEFAULT 'STARTED';

ALTER TABLE conferences DROP COLUMN completed;

UPDATE conferences AS conf
SET room_duration      = rec.room_duration,
    recording_duration = rec.recording_duration,
    recording_state    = rec.state
FROM recordings rec
WHERE conf.id = rec.conference_id;

DELETE
FROM flyway_schema_history
WHERE version = '0.39.0';
