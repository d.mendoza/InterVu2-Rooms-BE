-- DB rollback for migration version 0.31.0

ALTER TABLE research_sessions DROP COLUMN speaker_pin;

DELETE
FROM flyway_schema_history
WHERE version = '0.31.0';
