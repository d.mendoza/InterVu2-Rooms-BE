-- DB rollback for application version 0.29.0

ALTER TABLE participants DROP COLUMN speaker_pin;
ALTER TABLE research_sessions DROP COLUMN operator_pin;
ALTER TABLE research_sessions DROP COLUMN listener_pin;

DELETE
FROM flyway_schema_history
WHERE version = '0.30.0';
