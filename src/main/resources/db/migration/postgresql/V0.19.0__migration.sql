-- DB migration for application version 0.19.0

ALTER TABLE participants
    ADD COLUMN guest            BOOLEAN default false,
    ADD COLUMN project_manager  BOOLEAN default false,
    ADD COLUMN project_operator BOOLEAN default false;
