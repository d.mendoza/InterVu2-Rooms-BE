-- DI-832 Audio channel type

ALTER TABLE research_sessions
    ADD COLUMN audio_channel CHARACTER VARYING(50)
        CHECK (audio_channel IN
               ('NATIVE', 'TRANSLATOR', 'NATIVE_AND_TRANSLATOR')) NOT NULL DEFAULT 'NATIVE';

UPDATE room_state_logs SET room_state = jsonb_set(
        room_state::jsonb,
        '{audioChannel}',
        '"NATIVE"',
        true);
