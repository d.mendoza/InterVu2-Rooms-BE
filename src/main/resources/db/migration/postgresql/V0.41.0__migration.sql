-- DI-12 keep track of scroll actions

CREATE TABLE scroll_logs
(
    id             uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id        uuid                  NOT NULL,
    participant_id uuid                  NOT NULL,
    start_offset   BIGINT                NOT NULL,
    context_id     uuid                  NOT NULL,
    content        TEXT                  NULL,
    added_at       TIMESTAMP             NOT NULL
);
