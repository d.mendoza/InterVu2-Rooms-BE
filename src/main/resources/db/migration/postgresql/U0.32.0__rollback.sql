-- IV-3852 send screensre position to admin

ALTER TABLE conferences DROP COLUMN screenshare_position;

DELETE
FROM flyway_schema_history
WHERE version = '0.32.0';
