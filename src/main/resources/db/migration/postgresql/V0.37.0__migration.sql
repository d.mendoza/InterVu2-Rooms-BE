-- DI-405 saving recording data per layout

CREATE TABLE recordings (
  id                 uuid                   PRIMARY KEY DEFAULT uuid_generate_v4(),
  recording_sid      CHARACTER VARYING(100) NULL,
  conference_id      uuid                   NOT NULL    REFERENCES conferences (id),
  screenshare_bounds jsonb,
  composition_layout CHARACTER VARYING(50)
      CHECK (composition_layout IN ('GRID', 'ONE_ROW_GRID', 'PICTURE_IN_PICTURE',
      'MAIN_VIDEO_WITH_BOTTOM_ROW', 'PICTURE_IN_PICTURE_WITH_COLUMN', 'MOSAIC', 'CHESS_TABLE')),
  UNIQUE (conference_id, composition_layout));

INSERT INTO recordings (conference_id, recording_sid, composition_layout, screenshare_bounds)
    (SELECT id, recording_sid, composition_layout, screenshare_bounds FROM conferences);

ALTER TABLE conferences DROP COLUMN recording_sid;
ALTER TABLE conferences DROP COLUMN composition_layout;
ALTER TABLE conferences DROP COLUMN screenshare_bounds;
