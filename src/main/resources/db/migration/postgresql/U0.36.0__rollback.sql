-- Rollback for IV-4190 removing unused roles

ALTER TABLE participants
    DROP CONSTRAINT participants_role_check;

ALTER TABLE participants
    ADD CONSTRAINT participants_role_check
        CHECK (role IN ('MODERATOR', 'RESEARCH_COORDINATOR', 'OBSERVER', 'TRANSLATOR', 'RESPONDENT',
                        'RECRUITER'));

DELETE
FROM flyway_schema_history
WHERE version = '0.36.0';
