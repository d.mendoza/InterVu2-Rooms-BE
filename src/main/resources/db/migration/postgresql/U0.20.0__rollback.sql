-- DB rollback for application version 0.20.0

DELETE
FROM flyway_schema_history
WHERE version = '0.20.0';
