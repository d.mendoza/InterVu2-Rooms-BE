-- DB migration for application version 0.29.0

ALTER TABLE research_sessions ADD COLUMN listener_pin CHARACTER VARYING(20) NULL;
ALTER TABLE research_sessions ADD COLUMN operator_pin CHARACTER VARYING(20) NULL;
ALTER TABLE research_sessions ADD CONSTRAINT research_sessions_listener_pin_uq UNIQUE (listener_pin);
ALTER TABLE research_sessions ADD CONSTRAINT research_sessions_operator_pin_uq UNIQUE (operator_pin);

ALTER TABLE participants ADD COLUMN speaker_pin CHARACTER VARYING(20) NULL;
ALTER TABLE participants ADD CONSTRAINT participants_speaker_pin_uq UNIQUE (speaker_pin);
