-- DB migration for application version 0.21.0
ALTER TABLE room_state_logs
    ADD COLUMN previous_version BIGINT NULL;

