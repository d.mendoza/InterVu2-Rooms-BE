-- DB migration for application version 0.3.0

ALTER TABLE participants ADD COLUMN banned BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE conferences ADD COLUMN recording_sid CHARACTER VARYING(100) NULL;
