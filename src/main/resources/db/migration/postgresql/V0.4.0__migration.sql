-- DB migration for application version 0.4.0

CREATE TABLE bookmarks
(
    id                  uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    research_session_id uuid                    NOT NULL REFERENCES research_sessions (id),
    participant_id      uuid                    NOT NULL REFERENCES participants (id),
    note                CHARACTER VARYING(1000) NOT NULL,
    start_offset        BIGINT                  NOT NULL,
    created_at          TIMESTAMP               NOT NULL
);

ALTER TABLE research_sessions
    ADD COLUMN started_at TIMESTAMP NULL;
ALTER TABLE research_sessions
    ADD COLUMN ended_at TIMESTAMP NULL;
ALTER TABLE research_sessions
    DROP CONSTRAINT research_sessions_state_check;

ALTER TABLE research_sessions
    ADD CONSTRAINT research_sessions_state_check
        CHECK (state IN ('SCHEDULED', 'IN_PROGRESS', 'FINISHED', 'EXPIRED', 'CANCELED'));

ALTER TABLE research_sessions ADD COLUMN use_webcams BOOLEAN DEFAULT TRUE;
