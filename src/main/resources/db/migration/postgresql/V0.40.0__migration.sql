-- DI-571 Refactoring recording processing

ALTER TABLE recordings DROP CONSTRAINT recordings_state_check;
ALTER TABLE recordings
    ADD CONSTRAINT recordings_state_check
        CHECK (state IN ('PENDING', 'PROCESSING', 'COMPLETED', 'ERROR'));

ALTER TABLE recordings
    ALTER COLUMN state SET DEFAULT 'PENDING',
    ADD COLUMN processing_started_at    TIMESTAMP NULL,
    ADD COLUMN processing_finished_at   TIMESTAMP NULL,
    ADD COLUMN created_at               TIMESTAMP NULL,
    ADD COLUMN modified_at              TIMESTAMP NULL;

UPDATE recordings AS rec
SET created_at = conf.modified_at FROM conferences conf
WHERE conf.id = rec.conference_id;
