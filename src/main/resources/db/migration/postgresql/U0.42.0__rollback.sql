-- Rollback for DI-737 track translator switch

ALTER TABLE participant_action_logs
    DROP CONSTRAINT participant_action_logs_type_check;

DELETE
FROM participant_action_logs
WHERE type IN ('TRANSLATOR_ON', 'TRANSLATOR_OFF');

ALTER TABLE participant_action_logs
    ADD CONSTRAINT participant_action_logs_type_check
        CHECK (type IN ('MIC_ON', 'MIC_OFF', 'CAMERA_ON', 'CAMERA_OFF', 'SCREEN_SHARE_ON',
                        'SCREEN_SHARE_OFF'));

DELETE
FROM flyway_schema_history
WHERE version = '0.42.0';
