-- DB rollback for application version 0.2.0

ALTER TABLE research_sessions
    DROP CONSTRAINT research_sessions_state_check;

ALTER TABLE research_sessions
    ADD CONSTRAINT research_sessions_state_check
        CHECK (state IN ('SCHEDULED', 'IN_PROGRESS', 'FINISHED'));

DELETE
FROM flyway_schema_history
WHERE version = '0.2.0';
