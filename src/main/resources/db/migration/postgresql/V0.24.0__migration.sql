-- DB migration for application version 0.24.0

ALTER TABLE chat_messages
    DROP CONSTRAINT chat_messages_source_check;

UPDATE chat_messages
SET source = 'MEETING_ROOM'
WHERE source = 'BACKROOM';

ALTER TABLE chat_messages
    ADD CONSTRAINT chat_messages_source_check CHECK (source IN ('WAITING_ROOM', 'MEETING_ROOM'));

ALTER TABLE chat_messages
    DROP CONSTRAINT chat_messages_type_check;
ALTER TABLE chat_messages
    ADD CONSTRAINT chat_messages_type_check CHECK (type IN ('INTERNAL', 'RESPONDENTS', 'PRIVATE'));

ALTER TABLE chat_messages
    ADD COLUMN sender_id uuid NULL REFERENCES participants (id);
UPDATE chat_messages
SET sender_id = p.id
FROM participants p
WHERE p.research_session_id = chat_messages.research_session_id
  AND p.platform_id = chat_messages.sender_platform_id;
ALTER TABLE chat_messages
    ALTER COLUMN sender_id SET NOT NULL;

ALTER TABLE chat_messages
    DROP COLUMN handle;
ALTER TABLE chat_messages
    ADD COLUMN handle uuid NULL;
UPDATE chat_messages
SET handle = research_session_id;
ALTER TABLE chat_messages
    ALTER COLUMN handle SET NOT NULL;

ALTER TABLE room_state_logs
    DROP CONSTRAINT room_state_logs_change_type_check;
ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED',
                               'STIMULUS_INTERACTION_ENABLED',
                               'STIMULUS_INTERACTION_DISABLED',
                               'STIMULUS_RESET',
                               'RESPONDENTS_CHAT_ENABLED',
                               'RESPONDENTS_CHAT_DISABLED'));

CREATE TABLE chat_messages_tracking
(
    id              uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    source          CHARACTER VARYING(20) NOT NULL
        CHECK (source IN ('WAITING_ROOM', 'MEETING_ROOM')),
    type            CHARACTER VARYING(20) NOT NULL
        CHECK (type IN ('INTERNAL', 'RESPONDENTS', 'PRIVATE')),
    handle          uuid                  NOT NULL,
    participant_id  uuid                  NOT NULL,
    chat_message_id uuid                  NOT NULL,
    seen_at         TIMESTAMP             NOT NULL,
    UNIQUE (source, type, handle, participant_id)
);

ALTER TABLE chat_messages
    ADD COLUMN message_type CHARACTER VARYING(20)  NOT NULL DEFAULT 'REGULAR';
ALTER TABLE chat_messages
    ADD CONSTRAINT chat_messages_message_type_check CHECK (message_type IN ('REGULAR', 'BOOKMARK'));

ALTER TABLE projects
    ADD COLUMN type CHARACTER VARYING(20)
        CHECK (type IN ('FULL_SERVICE', 'SELF_SERVICE'));

UPDATE projects SET type = 'FULL_SERVICE';

ALTER TABLE projects ALTER COLUMN type SET NOT NULL;

UPDATE room_state_logs SET room_state = jsonb_set(
    room_state::jsonb,
    '{projectType}',
    '"FULL_SERVICE"',
    true);

ALTER TABLE stimulus_action_logs DROP CONSTRAINT stimulus_action_logs_action_type_check;
ALTER TABLE stimulus_action_logs ADD CONSTRAINT stimulus_action_logs_action_type_check
        CHECK (action_type IN ('POLL_VOTED', 'POLL_RANKED', 'POLL_ANSWERED', 'DOCUMENT_PAGE_CHANGED'));

ALTER TABLE room_state_logs DROP CONSTRAINT room_state_logs_change_type_check;
ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED',
                               'STIMULUS_INTERACTION_ENABLED',
                               'STIMULUS_INTERACTION_DISABLED',
                               'STIMULUS_RESET',
                               'RESPONDENTS_CHAT_ENABLED',
                               'RESPONDENTS_CHAT_DISABLED',
                               'STIMULUS_SYNC_ENABLED',
                               'STIMULUS_SYNC_DISABLED'));
