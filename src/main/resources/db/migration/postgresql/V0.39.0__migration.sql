-- DI-509 Implement recording layout resolver

ALTER TABLE recordings
    ADD COLUMN room_duration      INT                    NOT NULL DEFAULT 0,
    ADD COLUMN recording_duration INT                    NOT NULL DEFAULT 0,
    ADD COLUMN state              CHARACTER VARYING(20)  NOT NULL
        CHECK (state IN ('PROCESSING', 'COMPLETED', 'ERROR')) DEFAULT 'PROCESSING';

UPDATE recordings as rec
SET room_duration      = conf.room_duration,
    recording_duration = conf.recording_duration,
    state              = conf.recording_state
FROM conferences conf
WHERE conf.id = rec.conference_id
AND conf.recording_state <> 'STARTED';

ALTER TABLE conferences ADD COLUMN completed BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE conferences SET completed = true
    WHERE recording_state <> 'PROCESSING'
    AND recording_state <> 'STARTED';

ALTER TABLE conferences
    DROP COLUMN room_duration,
    DROP COLUMN recording_duration,
    DROP COLUMN recording_state;
