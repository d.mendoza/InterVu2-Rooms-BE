-- Rollback for IV-4015 ack room state

DROP TABLE room_state_acknowledgments;

DELETE
FROM flyway_schema_history
WHERE version = '0.33.0';
