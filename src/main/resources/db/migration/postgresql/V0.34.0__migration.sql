--IV-4059 Define composition layout based on stimuli usage in room

ALTER TABLE conferences
  ADD COLUMN composition_layout CHARACTER VARYING(50)
    CHECK (composition_layout IN ('GRID', 'ONE_ROW_GRID', 'PICTURE_IN_PICTURE',
    'MAIN_VIDEO_WITH_BOTTOM_ROW', 'PICTURE_IN_PICTURE_WITH_COLUMN', 'MOSAIC', 'CHESS_TABLE'));

UPDATE conferences c
    SET composition_layout =
    (SELECT composition_layout FROM research_sessions rs WHERE rs.id = c.research_session_id);
ALTER TABLE research_sessions
  DROP COLUMN composition_layout;
