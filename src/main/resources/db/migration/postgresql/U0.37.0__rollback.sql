-- Rollback for DI-405 saving recording data per layout

ALTER TABLE conferences ADD COLUMN recording_sid      CHARACTER VARYING(100);
ALTER TABLE conferences ADD COLUMN screenshare_bounds jsonb;
ALTER TABLE conferences ADD COLUMN composition_layout CHARACTER VARYING(50)
  CHECK (composition_layout IN ('GRID', 'ONE_ROW_GRID', 'PICTURE_IN_PICTURE',
      'MAIN_VIDEO_WITH_BOTTOM_ROW', 'PICTURE_IN_PICTURE_WITH_COLUMN', 'MOSAIC', 'CHESS_TABLE'));

UPDATE conferences AS conf
SET recording_sid = rec.recording_sid,
   composition_layout = rec.composition_layout,
   screenshare_bounds = rec.screenshare_bounds
FROM recordings rec
WHERE conf.id = rec.conference_id;

DROP TABLE recordings;

DELETE
FROM flyway_schema_history
WHERE version = '0.37.0';
