-- DB rollback for application version 0.17.0

UPDATE room_state_logs SET room_state = room_state #- '{stimulus,platformId}';

DELETE
FROM flyway_schema_history
WHERE version = '0.17.0';
