-- Rollback for DI-12 keep track of scroll actions

DROP TABLE scroll_logs;

DELETE
FROM flyway_schema_history
WHERE version = '0.41.0';
