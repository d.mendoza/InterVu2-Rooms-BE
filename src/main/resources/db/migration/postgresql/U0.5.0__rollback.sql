-- DB rollback for application version 0.5.0

ALTER TABLE projects DROP COLUMN project_number;

DELETE
FROM flyway_schema_history
WHERE version = '0.5.0';
