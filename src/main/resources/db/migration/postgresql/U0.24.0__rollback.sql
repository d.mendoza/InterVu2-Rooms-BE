-- DB rollback for application version 0.24.0

ALTER TABLE room_state_logs DROP CONSTRAINT room_state_logs_change_type_check;
ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED',
                               'STIMULUS_INTERACTION_ENABLED',
                               'STIMULUS_INTERACTION_DISABLED',
                               'STIMULUS_RESET',
                               'RESPONDENTS_CHAT_ENABLED',
                               'RESPONDENTS_CHAT_DISABLED'));

ALTER TABLE stimulus_action_logs DROP CONSTRAINT stimulus_action_logs_action_type_check;
ALTER TABLE stimulus_action_logs ADD CONSTRAINT stimulus_action_logs_action_type_check
        CHECK (action_type IN ('POLL_VOTED', 'POLL_RANKED', 'POLL_ANSWERED'));

UPDATE room_state_logs SET room_state = room_state::jsonb - 'projectType';

ALTER TABLE projects DROP COLUMN type;

ALTER TABLE chat_messages DROP CONSTRAINT chat_messages_message_type_check;
ALTER TABLE chat_messages DROP COLUMN message_type;

DROP TABLE chat_messages_tracking;

ALTER TABLE room_state_logs DROP CONSTRAINT room_state_logs_change_type_check;
DELETE FROM room_state_logs
WHERE change_type IN (
                      'RESPONDENTS_CHAT_ENABLED',
                      'RESPONDENTS_CHAT_DISABLED');

ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED',
                               'STIMULUS_INTERACTION_ENABLED',
                               'STIMULUS_INTERACTION_DISABLED',
                               'STIMULUS_RESET'));

ALTER TABLE chat_messages DROP COLUMN handle;
ALTER TABLE chat_messages ADD COLUMN handle CHARACTER VARYING(100) NULL;
UPDATE chat_messages SET handle = research_session_id::text;
ALTER TABLE chat_messages ALTER COLUMN handle SET NOT NULL;

ALTER TABLE chat_messages DROP COLUMN sender_id;

DELETE FROM chat_messages WHERE type = 'PRIVATE';
ALTER TABLE chat_messages DROP CONSTRAINT chat_messages_type_check;
ALTER TABLE chat_messages ADD CONSTRAINT chat_messages_type_check CHECK (type IN ('INTERNAL', 'RESPONDENTS'));

ALTER TABLE chat_messages DROP CONSTRAINT chat_messages_source_check;

UPDATE chat_messages SET source = 'BACKROOM'
WHERE source = 'MEETING_ROOM';

ALTER TABLE chat_messages ADD CONSTRAINT chat_messages_source_check CHECK (source IN ('WAITING_ROOM', 'BACKROOM'));

DELETE
FROM flyway_schema_history
WHERE version = '0.24.0';
