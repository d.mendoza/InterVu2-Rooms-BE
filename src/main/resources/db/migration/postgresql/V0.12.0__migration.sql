-- DB migration for application version 0.12.0

CREATE TABLE stimulus_state_logs
(
    id                  uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_sate_log_id    uuid                    NOT NULL REFERENCES room_state_logs (id),
    participant_id      uuid                    NOT NULL,
    content             text                    NOT NULL
);

ALTER TABLE room_state_logs
    DROP CONSTRAINT room_state_logs_change_type_check;

ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_DRAW',
                               'STIMULUS_DRAW_UNDO',
                               'STIMULUS_DRAW_REDO',
                               'STIMULUS_DRAW_CLEAR'));
