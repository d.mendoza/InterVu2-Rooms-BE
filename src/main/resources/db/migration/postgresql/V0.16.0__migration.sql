-- DB migration for application version 0.16.0

CREATE TABLE stimulus_action_logs
(
    id              uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    stimulus_id     uuid                  NOT NULL,
    participant_id  uuid                  NOT NULL,
    start_offset    BIGINT                NOT NULL,
    action_type     CHARACTER VARYING(30) NOT NULL
        CHECK (action_type IN ('POLL_VOTED')),
    content         TEXT                  NULL,
    version         BIGINT                NOT NULL,
    added_at        TIMESTAMP             NOT NULL
);

CREATE TABLE stimulus_action_states
(
    id              uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    stimulus_id     uuid                  NOT NULL,
    participant_id  uuid                  NOT NULL,
    content         TEXT                  NOT NULL
);

ALTER TABLE room_state_logs
    DROP CONSTRAINT room_state_logs_change_type_check;

ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED'));

ALTER TABLE stimulus_action_logs DROP CONSTRAINT stimulus_action_logs_action_type_check;
ALTER TABLE stimulus_action_logs ADD CONSTRAINT stimulus_action_logs_action_type_check
        CHECK (action_type IN ('POLL_VOTED', 'POLL_RANKED'));

ALTER TABLE stimulus_action_logs DROP CONSTRAINT stimulus_action_logs_action_type_check;
ALTER TABLE stimulus_action_logs ADD CONSTRAINT stimulus_action_logs_action_type_check
        CHECK (action_type IN ('POLL_VOTED', 'POLL_RANKED', 'POLL_ANSWERED'));

ALTER TABLE room_state_logs DROP CONSTRAINT room_state_logs_change_type_check;
ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED',
                               'STIMULUS_INTERACTION_ENABLED',
                               'STIMULUS_INTERACTION_DISABLED'));

ALTER TABLE room_state_logs DROP CONSTRAINT room_state_logs_change_type_check;
ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED',
                               'STIMULUS_INTERACTION_ENABLED',
                               'STIMULUS_INTERACTION_DISABLED',
                               'STIMULUS_RESET'));
