-- DB rollback for application version 0.21.0
ALTER TABLE room_state_logs
    DROP COLUMN previous_version;


DELETE
FROM flyway_schema_history
WHERE version = '0.21.0';
