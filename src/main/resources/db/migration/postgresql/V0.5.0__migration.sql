-- DB migration for application version 0.5.0

ALTER TABLE projects ADD COLUMN project_number CHARACTER VARYING (100);
UPDATE projects SET project_number = platform_id;
ALTER TABLE projects ALTER COLUMN project_number SET NOT NULL;
ALTER TABLE projects ADD UNIQUE (project_number);
