-- DB rollback for application version 0.3.0

ALTER TABLE conferences DROP COLUMN recording_sid;
ALTER TABLE participants DROP COLUMN banned;

DELETE
FROM flyway_schema_history
WHERE version = '0.3.0';
