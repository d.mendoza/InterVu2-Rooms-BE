-- DB migration for application version 0.11.0

CREATE TABLE room_state_logs
(
    id           uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id      uuid                  NOT NULL,
    start_offset BIGINT                NOT NULL,
    change_type  CHARACTER VARYING(30) NOT NULL
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED')),
    room_state   jsonb                 NOT NULL,
    version      BIGINT                NOT NULL,
    added_at     TIMESTAMP             NOT NULL
);

ALTER TABLE participants
    DROP COLUMN present;
ALTER TABLE participants
    DROP COLUMN ready;

ALTER TABLE stimuli
    ALTER COLUMN thumbnail TYPE TEXT;
ALTER TABLE stimuli
    DROP CONSTRAINT stimuli_platform_id_key;
