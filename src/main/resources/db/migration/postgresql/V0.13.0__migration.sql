-- DB migration for application version 0.13.0

ALTER TABLE participants ADD COLUMN banable BOOLEAN default true;
ALTER TABLE participants ADD COLUMN moderator BOOLEAN default false;

ALTER TABLE room_state_logs
    DROP CONSTRAINT room_state_logs_change_type_check;

DROP TABLE stimulus_state_logs;

DELETE
FROM room_state_logs
WHERE
    change_type in ('STIMULUS_DRAW',
                    'STIMULUS_DRAW_UNDO',
                    'STIMULUS_DRAW_REDO',
                    'STIMULUS_DRAW_CLEAR');

ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED'));

CREATE TABLE drawing_logs
(
    id             uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id        uuid                  NOT NULL,
    participant_id uuid                  NOT NULL,
    start_offset   BIGINT                NOT NULL,
    action_type    CHARACTER VARYING(10) NOT NULL
        CHECK (action_type IN ('DRAW', 'UNDO', 'REDO', 'CLEAR')),
    context_type   CHARACTER VARYING(10) NOT NULL
        CHECK (context_type IN ('STIMULUS')),
    context_id     uuid                  NOT NULL,
    content        TEXT                  NULL,
    added_at       TIMESTAMP             NOT NULL
);
