-- DB rollback for application version 0.5.1

ALTER TABLE participants DROP COLUMN admin;

DELETE
FROM flyway_schema_history
WHERE version = '0.5.1';
