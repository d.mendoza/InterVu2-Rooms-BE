-- DB rollback for application version 0.1.0

DROP TABLE chat_messages;
DROP TABLE conferences;
DROP TABLE stimuli;
DROP TABLE participants;
DROP TABLE research_sessions;
DROP TABLE projects;

DELETE
FROM flyway_schema_history
WHERE version = '0.1.0';
