-- Rollback for IV-3989 track participant actions

DROP TABLE participant_action_logs;

DELETE
FROM flyway_schema_history
WHERE version = '0.35.0';
