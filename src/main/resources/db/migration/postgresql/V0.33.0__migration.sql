-- IV-4015 ack room state

CREATE TABLE room_state_acknowledgments
(
    id             uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id        uuid                  NOT NULL,
    participant_id uuid                  NOT NULL,
    start_offset   BIGINT                NOT NULL,
    version        BIGINT                NOT NULL,
    created_at     TIMESTAMP             NOT NULL,
    modified_at    TIMESTAMP             NULL
);
