-- DB migration for application version 0.2.0

ALTER TABLE research_sessions
    DROP CONSTRAINT research_sessions_state_check;

ALTER TABLE research_sessions
    ADD CONSTRAINT research_sessions_state_check
        CHECK (state IN ('SCHEDULED', 'IN_PROGRESS', 'FINISHED', 'CANCELED'));
