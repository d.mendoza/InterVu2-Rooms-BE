-- DI-553 ws event for expired session

ALTER TABLE room_state_logs DROP CONSTRAINT room_state_logs_change_type_check;
ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_EXPIRED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED',
                               'STIMULUS_FOCUS_ENABLED',
                               'STIMULUS_FOCUS_DISABLED',
                               'DRAWING_SYNC_ENABLED',
                               'DRAWING_SYNC_DISABLED',
                               'PARTICIPANT_DRAWING_ENABLED',
                               'PARTICIPANT_DRAWING_DISABLED',
                               'STIMULUS_BROADCAST_ENABLED',
                               'STIMULUS_BROADCAST_DISABLED',
                               'STIMULUS_INTERACTION_ENABLED',
                               'STIMULUS_INTERACTION_DISABLED',
                               'STIMULUS_RESET',
                               'RESPONDENTS_CHAT_ENABLED',
                               'RESPONDENTS_CHAT_DISABLED',
                               'STIMULUS_SYNC_ENABLED',
                               'STIMULUS_SYNC_DISABLED'));
