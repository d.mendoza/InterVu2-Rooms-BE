-- DI-976 Snapshot chat notification

DELETE FROM chat_messages WHERE message_type = 'SNAPSHOT';

ALTER TABLE chat_messages
    DROP CONSTRAINT chat_messages_message_type_check,
    ADD CONSTRAINT chat_messages_message_type_check CHECK (message_type IN ('REGULAR', 'BOOKMARK', 'PII'));

DELETE
FROM flyway_schema_history
WHERE version = '0.44.0';
