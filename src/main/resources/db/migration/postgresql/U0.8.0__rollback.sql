-- DB rollback for application version 0.8.0

ALTER TABLE research_sessions DROP COLUMN composition_layout;
ALTER TABLE stimuli DROP COLUMN created_at, DROP COLUMN modified_at;
ALTER TABLE research_sessions DROP COLUMN created_at, DROP COLUMN modified_at;
ALTER TABLE projects DROP COLUMN created_at, DROP COLUMN modified_at;
ALTER TABLE participants DROP COLUMN created_at, DROP COLUMN modified_at;
ALTER TABLE conferences DROP COLUMN created_at, DROP COLUMN modified_at;
ALTER TABLE bookmarks DROP COLUMN modified_at;

DELETE
FROM flyway_schema_history
WHERE version = '0.8.0';
