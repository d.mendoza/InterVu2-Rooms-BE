-- IV-3876 Research session speaker PIN

ALTER TABLE research_sessions ADD COLUMN speaker_pin CHARACTER VARYING(20) NULL;
ALTER TABLE research_sessions ADD CONSTRAINT research_sessions_speaker_pin_uq UNIQUE (speaker_pin);
