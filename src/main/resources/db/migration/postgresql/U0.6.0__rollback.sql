-- DB rollback for application version 0.6.0

ALTER TABLE research_sessions
    DROP COLUMN privacy;

ALTER TABLE conferences
    DROP COLUMN room_duration,
    DROP COLUMN recording_state,
    DROP COLUMN recording_duration,
    DROP COLUMN recording_offset;

DELETE
FROM flyway_schema_history
WHERE version = '0.6.0';
