-- DB rollback for application version 0.11.0

ALTER TABLE stimuli ADD CONSTRAINT stimuli_platform_id_key UNIQUE (platform_id);
ALTER TABLE stimuli ALTER COLUMN thumbnail TYPE CHARACTER VARYING (250);

DROP TABLE room_state_logs;

ALTER TABLE participants
    ADD COLUMN present BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE participants
    ADD COLUMN ready BOOLEAN NOT NULL DEFAULT FALSE;

DELETE
FROM flyway_schema_history
WHERE version = '0.11.0';
