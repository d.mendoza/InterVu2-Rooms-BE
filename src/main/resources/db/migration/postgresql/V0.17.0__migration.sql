-- DB migration for application version 0.17.0

WITH sq AS(
	SELECT s.id, s.platform_id::jsonb
	FROM stimuli AS s
	WHERE id IN
		(SELECT DISTINCT (room_state->'stimulus'->>'id')::uuid FROM room_state_logs))
UPDATE room_state_logs
	SET room_state = jsonb_set(room_state, '{stimulus, platformId}', sq.platform_id)
	FROM sq
	WHERE sq.id::text = room_state_logs.room_state->'stimulus'->>'id';
