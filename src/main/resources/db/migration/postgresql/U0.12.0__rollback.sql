-- DB rollback for application version 0.12.0
ALTER TABLE room_state_logs
    DROP CONSTRAINT room_state_logs_change_type_check;

ALTER TABLE room_state_logs
    ADD CONSTRAINT room_state_logs_change_type_check
        CHECK (change_type IN (
                               'ROOM_CREATED',
                               'ROOM_UPDATED',
                               'ROOM_CANCELED',
                               'ROOM_STARTED',
                               'ROOM_FINISHED',
                               'PARTICIPANT_ONLINE',
                               'PARTICIPANT_OFFLINE',
                               'PARTICIPANT_BANNED',
                               'PARTICIPANT_READY',
                               'STIMULUS_ACTIVATED',
                               'STIMULUS_DEACTIVATED'));

DROP TABLE stimulus_state_logs;

DELETE
FROM flyway_schema_history
WHERE version = '0.12.0';
