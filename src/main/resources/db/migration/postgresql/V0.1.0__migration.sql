-- DB migration for application version 0.1.0
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE projects
(
    id          uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    platform_id CHARACTER VARYING(100) NOT NULL UNIQUE,
    name        CHARACTER VARYING(50)  NOT NULL
);

CREATE TABLE research_sessions
(
    id          uuid PRIMARY KEY                DEFAULT uuid_generate_v4(),
    platform_id CHARACTER VARYING(100) NOT NULL UNIQUE,
    project_id  uuid                   NOT NULL REFERENCES projects (id),
    name        CHARACTER VARYING(50)  NOT NULL,
    state       CHARACTER VARYING(20)  NOT NULL
        CHECK (state IN ('SCHEDULED', 'IN_PROGRESS', 'FINISHED')),
    starts_at   TIMESTAMP              NOT NULL,
    ends_at     TIMESTAMP              NOT NULL,
    version     INT                    NOT NULL DEFAULT 1
);

CREATE TABLE participants
(
    id                  uuid PRIMARY KEY                 DEFAULT uuid_generate_v4(),
    platform_id         CHARACTER VARYING(100)  NOT NULL,
    research_session_id uuid                    NOT NULL REFERENCES research_sessions (id),
    invitation_token    CHARACTER VARYING(100)  NULL UNIQUE,
    display_name        CHARACTER VARYING(100)  NOT NULL,
    role                CHARACTER VARYING(20)   NOT NULL
        CHECK (role IN ('MODERATOR', 'RESEARCH_COORDINATOR', 'OBSERVER', 'TRANSLATOR', 'RESPONDENT', 'RECRUITER')),
    present             BOOLEAN                 NOT NULL DEFAULT FALSE,
    ready               BOOLEAN                 NOT NULL DEFAULT FALSE,
    conference_token    CHARACTER VARYING(1000) NULL
);

CREATE TABLE stimuli
(
    id                  uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    platform_id         CHARACTER VARYING(100) NOT NULL UNIQUE,
    research_session_id uuid                   NOT NULL REFERENCES research_sessions (id),
    name                CHARACTER VARYING(50)  NOT NULL,
    type                CHARACTER VARYING(20)  NOT NULL
        CHECK (type IN ('POLL', 'DRAG_AND_DROP', 'DOCUMENT_SHARING')),
    thumbnail           CHARACTER VARYING(250) NULL,
    data                text                   NOT NULL,
    position            INT                    NOT NULL
);

CREATE TABLE conferences
(
    id                  uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    research_session_id uuid                   NOT NULL REFERENCES research_sessions (id),
    room_sid            CHARACTER VARYING(100) NOT NULL
);

CREATE TABLE chat_messages
(
    id                  uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    source              CHARACTER VARYING(20)   NOT NULL
        CHECK (source IN ('WAITING_ROOM', 'DASHBOARD', 'BACKROOM')),
    type                CHARACTER VARYING(20)   NOT NULL
        CHECK (type IN ('INTERNAL', 'RESPONDENTS')),
    message             CHARACTER VARYING(1000) NULL,
    sender_platform_id  CHARACTER VARYING(100)  NOT NULL,
    sender_display_name CHARACTER VARYING(100)  NOT NULL,
    handle              CHARACTER VARYING(100)  NOT NULL,
    research_session_id uuid                    NOT NULL REFERENCES research_sessions (id),
    sent_at             TIMESTAMP               NOT NULL
);
