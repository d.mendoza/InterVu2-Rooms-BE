-- Rollback for DI-571 Refactoring recording processing

UPDATE recordings SET state = 'PROCESSING' WHERE state = 'PENDING';

ALTER TABLE recordings DROP CONSTRAINT recordings_state_check;
ALTER TABLE recordings
    ADD CONSTRAINT recordings_state_check
        CHECK (state IN ('PROCESSING', 'COMPLETED', 'ERROR'));

ALTER TABLE recordings
    ALTER COLUMN state SET DEFAULT 'PROCESSING',
    DROP COLUMN processing_started_at,
    DROP COLUMN processing_finished_at,
    DROP COLUMN created_at,
    DROP COLUMN modified_at;

DELETE
FROM flyway_schema_history
WHERE version = '0.40.0';
