-- DI-976 Snapshot chat notification

ALTER TABLE chat_messages
    DROP CONSTRAINT chat_messages_message_type_check,
    ADD CONSTRAINT chat_messages_message_type_check CHECK (message_type IN ('REGULAR', 'BOOKMARK', 'PII', 'SNAPSHOT'));
