-- DB migration for application version 0.7.0

ALTER TABLE conferences
    DROP CONSTRAINT conferences_recording_state_check;

ALTER TABLE conferences
    ADD CONSTRAINT conferences_recording_state_check
        CHECK (recording_state IN ('STARTED', 'PROCESSING', 'COMPLETED', 'ERROR'));
