-- DI-989 Add translated flag to recordings table

ALTER TABLE recordings ADD COLUMN audio_channel CHARACTER VARYING(50)
  CHECK (audio_channel IN ('NATIVE', 'TRANSLATOR')) NOT NULL DEFAULT 'NATIVE';

ALTER TABLE recordings DROP CONSTRAINT recordings_conference_id_composition_layout_key;
ALTER TABLE recordings ADD CONSTRAINT recordings_conference_id_composition_layout_audio_channel_key
  UNIQUE(conference_id, composition_layout, audio_channel);
