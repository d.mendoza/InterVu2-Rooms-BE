-- DI-971 Stimulus snapshot model

CREATE TABLE stimulus_snapshots
(
    id                  uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    research_session_id uuid                   NOT NULL REFERENCES research_sessions (id),
    participant_id      uuid                   NOT NULL REFERENCES participants (id),
    stimulus_id         uuid                   NOT NULL REFERENCES stimuli (id),
    name                CHARACTER VARYING(200) NOT NULL,
    mime_type           CHARACTER VARYING(30)  NOT NULL,
    data                bytea                  NOT NULL,
    start_offset        BIGINT                 NOT NULL,
    created_by          CHARACTER VARYING(50)  NULL,
    created_at          TIMESTAMP              NULL,
    modified_by         CHARACTER VARYING(50)  NULL,
    modified_at         TIMESTAMP              NULL
);
