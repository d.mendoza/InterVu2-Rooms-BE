-- DB rollback for application version 0.4.0

DROP TABLE bookmarks;

ALTER TABLE research_sessions
    DROP COLUMN started_at;
ALTER TABLE research_sessions
    DROP COLUMN ended_at;
ALTER TABLE research_sessions
    DROP CONSTRAINT research_sessions_state_check;

ALTER TABLE research_sessions
    ADD CONSTRAINT research_sessions_state_check
        CHECK (state IN ('SCHEDULED', 'IN_PROGRESS', 'FINISHED', 'CANCELED'));

ALTER TABLE research_sessions DROP COLUMN use_webcams;

DELETE
FROM flyway_schema_history
WHERE version = '0.4.0';
