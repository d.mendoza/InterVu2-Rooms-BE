-- IV-3989 track participant actions

CREATE TABLE participant_action_logs
(
    id             uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id        uuid                  NOT NULL,
    participant_id uuid                  NOT NULL,
    start_offset   BIGINT                NOT NULL,
    type           CHARACTER VARYING(20) NOT NULL
        CHECK (type IN ('MIC_ON', 'MIC_OFF', 'CAMERA_ON', 'CAMERA_OFF', 'SCREEN_SHARE_ON',
                        'SCREEN_SHARE_OFF')),
    created_at     TIMESTAMP             NOT NULL,
    modified_at    TIMESTAMP             NULL
);
