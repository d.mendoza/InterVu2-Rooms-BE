-- Rollback for DI-989 Add translated flag to recordings table

ALTER TABLE recordings DROP CONSTRAINT recordings_conference_id_composition_layout_audio_channel_key;
ALTER TABLE recordings ADD CONSTRAINT recordings_conference_id_composition_layout_key
  UNIQUE(conference_id, composition_layout);

ALTER TABLE recordings DROP COLUMN audio_channel;

DELETE
FROM flyway_schema_history
WHERE version = '0.46.0';
