-- IV-4190 removing unused roles

ALTER TABLE participants
DROP CONSTRAINT participants_role_check;

ALTER TABLE participants
    ADD CONSTRAINT participants_role_check
        CHECK (role IN ('MODERATOR', 'OBSERVER', 'TRANSLATOR', 'RESPONDENT'));
