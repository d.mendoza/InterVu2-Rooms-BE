-- DB migration for application version 0.8.0

ALTER TABLE bookmarks ADD COLUMN modified_at TIMESTAMP NULL;
ALTER TABLE conferences ADD COLUMN created_at TIMESTAMP NULL;
ALTER TABLE conferences ADD COLUMN modified_at TIMESTAMP NULL;
ALTER TABLE participants ADD COLUMN created_at TIMESTAMP NULL;
ALTER TABLE participants ADD COLUMN modified_at TIMESTAMP NULL;
ALTER TABLE projects ADD COLUMN created_at TIMESTAMP NULL;
ALTER TABLE projects ADD COLUMN modified_at TIMESTAMP NULL;
ALTER TABLE research_sessions ADD COLUMN created_at TIMESTAMP NULL;
ALTER TABLE research_sessions ADD COLUMN modified_at TIMESTAMP NULL;
ALTER TABLE stimuli ADD COLUMN created_at TIMESTAMP NULL;
ALTER TABLE stimuli ADD COLUMN modified_at TIMESTAMP NULL;
ALTER TABLE research_sessions ADD COLUMN composition_layout CHARACTER VARYING(50) NOT NULL
    CHECK (composition_layout IN ('GRID', 'ONE_ROW_GRID', 'PICTURE_IN_PICTURE', 'MAIN_VIDEO_WITH_BOTTOM_ROW',
                                  'PICTURE_IN_PICTURE_WITH_COLUMN', 'MOSAIC','CHESS_TABLE')) DEFAULT 'GRID';
