-- Rollback for DI-832 Audio channel type

UPDATE room_state_logs SET room_state = room_state::jsonb - 'audioChannel';

ALTER TABLE research_sessions
    DROP COLUMN audio_channel;

DELETE
FROM flyway_schema_history
WHERE version = '0.43.0';
